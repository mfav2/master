<?php  echo $this->element('Components/breadcrumb'); ?>
<div class="control-group">
    <div class="row-fluid">
        <div class="span6">
             <p class="f_legend" >
                <?php echo __('Menu Management'); ?>
            </p>
            <div id="showmenu" class="demo"></div>
          
        </div>
        <div class="span6" id="DivMenuFormAdd">

        </div>
        <div  class="span6" id="DivMenuFormEdit">

        </div>  	
    </div>
</div>

<script>
$(document).ready(function() {
    $("#showmenu").jstree({
                "contextmenu": {
                    "items": {
                        "create": {
                            "label": "Add",
                            "action": function (obj) {
                                var level=parseInt(obj.attr('level'));
                                addmenu(obj.attr('id'),level);
                                
                            }, "_disabled": false,
                            "_class": "add",
                            "separator_before": false,
                            "separator_after": false,
                            "icon": false
                        }, 
                        "rename": {
                            "label": "Edit",
                            "action": function (obj) {
                                 
                                
                            }, "_disabled": false,
                            "_class": "edit",
                            "separator_before": false,
                            "separator_after": false,
                            "icon": false
                        }, 
                        "remove": {
                            "label": "Delete",
                            "action": function (obj) {
                                if(obj.attr('level')!='-1'){
                                    if (confirm("Are you sure you want to delete")) {
                                       
                                    }
                                    else{
                                    
                                    }
                                }else{
                                    alert('This organization con not delete!');
                                }
                            
                               
                            }, "_disabled": false,
                            "_class": "delete",
                            "separator_before": true,
                            "separator_after": false,
                            "icon": false
                        }, "ccp": false
                    }
                },
                "core" : {
                     "initially_open" : [ 1 ]
                },
                "plugins": ["themes", "json_data", "ui","contextmenu", "sort"],
                "json_data": {
                    "data" : [
                        {
                            "data" : {
                                "title":'Menu Management',
                                'attr':{
                                    "id":1,
                                    "href" : "#",
                                    'level':-1
                                  
                                }
                            },
                "metadata" : { id : 1 },
                            "state" : "closed",
                            "attr":{
                                "id":1,
                                'level':-1,
                                'parent_id':-1
                            }
                        }
                    ],
                    "ajax": {
    
                        url: function (node) { 
                        
                            var nodeId = node.attr('id'); 
                            if(node!=-1){
                                return "/Menu/showmenu/" + nodeId;
                            }
                            else{
                                return "/Menu/showmenu/";
                            }
                        },
                        type: "POST",
                        contentType: "application/json;charset=utf-8",
                        dataType: "json",
                        data: function (n) {
                            return { id: n.attr ? n.attr("id") : "0",level:n.attr('level') };
                        },
                        success: function (data, textstatus, xhr) {
                            // alert(data)
                        },
                        error: function (xhr, textstatus, errorThrown) {
                            // alert(textstatus);
                        }
                    }
                }
            });
        

});

function addmenu(parent_id, level){
	alert(parent_id+ ' , '+ level);
    //$('#UserInOrganization').hide();
        $('#DivMenuFormEdit').hide();
        $('#DivMenuFormAdd').load("/Menu/add?_="+Math.random(),function(data) {
            $('#DivMenuFormAdd').show(1000);
            $('#DivMenuFormAdd').html(data);
           x
            /*if(level<3){
                window.location="#";
                $('input[name=organization_short_name]').val(null);
                $('input[name=organization_name_th]').val(null);
                $('input[name=organization_name_en]').val(null);
                $('input[name=organization_code]').val(null);
                $('input[name=organization_book_no]').val(null);
                $('input[name=organization_code]').attr('readonly', false);
                $('input[name=organization_book_no]').attr('readonly', false);
                $('#OrganizationForm').attr('action', "/organizations/add");
                $('input[name=organization_level]').val(level+1);
                $('#OrganizationParentId').val(parent_id);
                
                $('#DivOrganizationFormAdd').show(1000);
                                          
            }else{
                                    
                alert('<?php __('Sorry can not add child because this organization is the lowest level.'); ?>');
            }*/
        });
}
</script>