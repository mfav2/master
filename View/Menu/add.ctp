<?php
    echo $this->Form->create('Menu', array('action' => "add", 'class' => 'form-horizontal well form_validation_reg'));
?>
    <fieldset>
        <p class="f_legend" id="HeadOfForm"><?php echo __("Add Menu"); ?></p>
        <div class="control-group">
            <div class="row-fluid">

            </div>
        </div>
        <div class="control-group">
            <div class="row-fluid">
                <div class="span12">
                    <label class="control-label">Menu Name (Local)</label>
                    <div class="controls">

                    </div>
                </div>
                <div class="span12">
                    <label class="control-label">Menu Name (English)</label>
                    <div class="controls">
                        
                    </div>
                </div>
                <div class="span12">
                    <label class="control-label">Controller</label>
                    <div class="controls">
                        
                    </div>
                </div>
                <div class="span12">
                    <label class="control-label">Action</label>
                    <div class="controls">
                        
                    </div>
                </div>
                <div class="span12">
                    <label class="control-label">Params</label>
                    <div class="controls">
                        
                    </div>
                </div>
                <div class="span12">
                    <label class="control-label">Menu Icon</label>
                    <div class="controls">
                        
                    </div>
                </div>
                <div class="span12">
                    <label class="control-label">Sort Order</label>
                    <div class="controls">
                        
                    </div>
                </div>
            </div>
        </div>    
    </fieldset>
<?php    
    echo $this->Form->end();

?>