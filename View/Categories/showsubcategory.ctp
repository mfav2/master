<table class='table table-striped table-bordered table-condensed'>
	<thead>
		<tr>
			<th><?php echo __('Detail'); ?></th>
			<th><?php echo __('Group'); ?></th>
			<th><i class="icon-trash"></i></th>
		</tr>
	</thead> 
	<tbody>
		<?php foreach($getCateId['MainCategoryDescription'] as $maindesc): ?>
			<?php foreach($getCateId['SubCategoryDescription'] as $subdesc){?>
				
				<?php if($subdesc['parent_id'] == $maindesc['category_id']){ ?>
				<tr id="cat<?php echo $subdesc['category_id']; ?>">
					<td><?php echo $subdesc['description']; ?></td>
					<td><?php echo $maindesc['description']; ?></td>
					<td><i class="icon-trash" style="cursor:pointer;" onclick="deleted(<?php echo $subdesc['category_id']; ?>);"></i></td>
				</tr>
				<?php } ?>
			<?php } ?>
		<?php endforeach;?>
	</tbody>

</table>
