<?php
//pr($getCateId);
	if(!empty($getcategory)){
		foreach($getcategory as $description){
			$listgroup[$description['category']['category_id']] = $description['category']['description'];
		}
	}
?>

<div class="row-fluid">
		<div class="span12">
			<input type="hidden" id="maincate" value="<?php echo $main_category; ?>" />
			 <table calss='table span12' width="100%">
				<tr>
					<td align="right"><?php echo __("Sub Group Name"); ?>&nbsp;:&nbsp;</td>
					<td><?php echo $this->Form->input('group_name', array('type' => 'text', 'label'=>false, '')); ?></td>
				</tr>
				<tr>
					<td align="right"><?php echo __("Group"); ?>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $this->form->input('group', array(
							'type' => 'select',
							'label' => false,
							'div' => false,
							'style' => 'width:auto;',
							'default'=> $default,
							'options' => $listgroup,
						)); ?>
					</td>
				</tr>
			 </table>
		</div>

		<div class="span12" align="center" style="margin:5px 0;">
			<button id="saveGroup" class="button"><?php echo __('Save'); ?></button>
		</div>

		<div id="showcategory" class="span12" style="margin:5px 0 0 0;">
			<table class='table table-striped table-bordered table-condensed'>
				<thead>
					<tr>
						<th><?php echo __('Detail'); ?></th>
						<th><?php echo __('Group'); ?></th>
						<th><i class="icon-trash"></i></th>
					</tr>
				</thead> 
				<tbody>
					<?php foreach($getCateId['MainCategoryDescription'] as $maindesc): ?>
						<?php foreach($getCateId['SubCategoryDescription'] as $subdesc){?>
							
							<?php if($subdesc['parent_id'] == $maindesc['category_id']){ ?>
							<tr id="cat<?php echo $subdesc['category_id']; ?>">
								<td><?php echo $subdesc['description']; ?></td>
								<td><?php echo $maindesc['description']; ?></td>
								<td><i class="icon-trash" style="cursor:pointer;" onclick="deleted(<?php echo $subdesc['category_id']; ?>);"></i></td>
							</tr>
							<?php } ?>
						<?php } ?>
					<?php endforeach;?>
				</tbody>
				<!--<tbody>
					
					<?php foreach($getCateId['SubCategoryDescription'] as $subdesc) : ?>
							<tr>
								<td><?php echo $subdesc['description']; ?></td>
								<td>
									
									<?php foreach($getCateId['MainCategoryDescription'] as $maindesc){?>
										<?php
											if($subdesc['parent_id'] == $maindesc['category_id']){
												echo $maindesc['description'];
											}
										?>
									<?php } ?>
								</td>
							</tr>
					<?php endforeach;?>
				</tbody>-->
			</table>
		</div>
</div>

<script>
$('#saveGroup').click(function(){
	var main_cate = $('#maincate').val();
	var sub_group_name = $('#group_name').val();
	var group = $('#group option:selected').val();
	$.ajax({
			type: "POST",
			cache: false,
			url: '/Categories/saveGroup?_='+Math.random(),
			data: {'category_name':sub_group_name, 'parent_id':group},
			success: function(data){
				if(data==1){
					$('#showcategory').empty();
					$.ajax({
						type: "POST",
						cache: false,
						url: '/Categories/showsubcategory/'+main_cate+'?_='+Math.random(),
						data: {'category_id':main_cate},
						success: function(show){
							$('#showcategory').append(show);
						}
					});
				}else{
					alert(data);
				}
			}
	});
});

function deleted(cat_id){
	if(confirm('<?php echo __('Confirm to delete'); ?> ? ')){
        $.ajax({
          cache: false,
          url: "/Categories/categorydeleted",
          type: "post",
          data: {'id':cat_id},
          success:function(data){
			  if(data == 1){
                  //location.reload();
				  $('#cat'+cat_id).remove();
              }else{
                  alert(data);
              }
			  //alert(data);
          }
        });    
    }else{
        return false;   
    }
}
</script>

<style>
.modal {
    background-clip: padding-box;
    background-color: #FFFFFF;
    border: 8px solid rgba(0, 0, 0, 0.5);
    border-radius: 6px 6px 6px 6px;
    box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
    left: 50%;
    margin: -300px 0 0 -600px;
    overflow: auto;
    position: fixed;
    top: 50%;
    width: 1200px;

    z-index: 1050;
}

.modal-body {
    min-height: 450px;
    max-height: 450px;
    overflow-y: auto;
}
</style>