<?php echo $this->element('Components/breadcrumb');  ?>
<div class="control-group">
    <div class="row-fluid">
        <div class="span6">
             <p class="f_legend" >
                <?php echo __('Category'); ?>
            </p>
            <div id="category_tree" class="demo"></div>
          
        </div>    
    </div>
</div>

<script>
$(document).ready(function() {
    $("#category_tree").jstree({
                "contextmenu": {
                    "items": {
                        "create": {
                            "label": "Add",
                            "action": function (obj) {
                               
                                
                            }, "_disabled": false,
                            "_class": "add",
                            "separator_before": false,
                            "separator_after": false,
                            "icon": false
                        }, 
                        "rename": {
                            "label": "Edit",
                            "action": function (obj) {
                                 
                                
                            }, "_disabled": false,
                            "_class": "edit",
                            "separator_before": false,
                            "separator_after": false,
                            "icon": false
                        }, 
                        "remove": {
                            "label": "Delete",
                            "action": function (obj) {
                                if(obj.attr('level')!='-1'){
                                    if (confirm("Are you sure you want to delete")) {
                                       
                                    }
                                    else{
                                    
                                    }
                                }else{
                                    alert('This organization con not delete!');
                                }
                            
                               
                            }, "_disabled": false,
                            "_class": "delete",
                            "separator_before": true,
                            "separator_after": false,
                            "icon": false
                        }, "ccp": false
                    }
                },
                "core" : {
                     "initially_open" : [ 1 ]
                },
                "plugins": ["themes", "json_data", "ui","contextmenu"],
                "json_data": {
                    "data" : [
                        {
                            "data" : {
                                "title":'<?php echo Configure::read('Application.MinistryName');?>',
                                'attr':{
                                    "id":1,
                                    "href" : "#",
                                    'level':-1
                                  
                                }
                            },
                "metadata" : { id : 1 },
                            "state" : "closed",
                            "attr":{
                                "id":1,
                                'level':-1,
                                'parent_id':-1
                            }
                        }
                    ],
                    "ajax": {
    
                        url: function (node) { 
                        
                            var nodeId = node.attr('id'); 
                            if(node!=-1){
                                return "/Categories/showallcategory/" + nodeId;
                            }
                            else{
                                return "/Categories/showallcategory/";
                            }
                        },
                        type: "POST",
                        contentType: "application/json;charset=utf-8",
                        dataType: "json",
                        data: function (n) {
                            return { id: n.attr ? n.attr("id") : "0",level:n.attr('level') };
                        },
                        success: function (data, textstatus, xhr) {
                            // alert(data)
                        },
                        error: function (xhr, textstatus, errorThrown) {
                            // alert(textstatus);
                        }
                    }
                }
            });
        

});
</script>