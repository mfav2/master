<?php  //echo $this->element('Components/breadcrumb'); ?>
<?php
   $current_date = date("Y-m-d");

    $timestamp = mktime(0, 0, 0, $selected_month, 1, $selected_year);
    $days_in_month = date("t", $timestamp);

    $tmp_month_list = range(1, 12);
    $month_list = array();
    foreach($tmp_month_list AS $i => $m){
        $month_list[$m] = date("M", mktime(0, 0, 0, $m, 1, $selected_year));
    }
    
    $tmp_years_list = range(date("Y") - 1, date("Y") + 2);
    foreach($tmp_years_list AS $i => $y){
        $years_list[$y] = $y;
    }
    
?>    

<div class="row-fluid">
    <div class="span12">
        <div align="right">
            <form style="margin-bottom:0px; margin-top:0px" method="post" action="<?php echo $this->Portal->makeUrl($this->name, 'view'); ?>">
            <?php
                echo $this->form->input('selected_year', array('type'=>'select', 'label'=>false, 'div'=>false, 'options'=>$years_list, 'selected'=>$selected_year, 'style'=>'width:100px; margin-top:8px', 'onchange'=>'this.form.submit()'));
                echo $this->form->input('selected_month', array('type'=>'select', 'label'=>false, 'div'=>false, 'options'=>$month_list, 'selected'=>$selected_month, 'style'=>'width:100px; margin-top:8px', 'onchange'=>'this.form.submit()'));
                echo $this->form->input('user_id', array('type'=>'hidden', 'value'=>$user_id));
            ?>
            </form>
        </div>
    </div>
    <div class="row-fluid">
        <div id="agendaAllMonth" class="span12">
            <table class="table table-bordered table-striped table_vam">
                <tr>
                    <th style='padding:5px;'><?php echo $user[$user_id]['detail']; ?></th>
                </tr>
                <?php 
                    if(!empty($appointments['event_date'])){ 
                        for($i = 1; $i <= $days_in_month; $i++){
                            $i = $i < 10 ? "0$i" : $i;
                            $this_date = "$selected_year-$selected_month-$i";
                            $day_of_week = date("w", strtotime($this_date));

                            if($day_of_week == 0){
                                $colorevent = "#ffbad2";
                            }else if($day_of_week == 6){
                                $colorevent = "#8f5e99";
                            }else{
                                $colorevent = "#F3F3F3;";
                            }

                            if($this_date == $current_date){
                                $colorevent = "#F2F5A9";
                            }
                            $this_day_event_title = '';
                            foreach($appointments['event_date'] AS $app_id => $event_date){
                                $time = mktime(0, 0, 0, $selected_month, $i, $selected_year);
                                $datetime_today = date("Y-m-d H:i:s", $time);
                                $datetime_tomorrow = date("Y-m-d 23:59:00", $time);

                                $event_datetime_start = $event_date['start_date'];
                                $event_datetime_end = $event_date['end_date'];

                                if(empty($event_date['appointment_name'])){
                                $event_date['appointment_name'] = $event_date['detail'];
                                }

                                $start_time = substr($event_datetime_start, 11, 5);
                                $end_time = substr($event_datetime_end, 11, 5);

                                if($event_datetime_start <= $datetime_today && $event_datetime_end >= $datetime_tomorrow){
                                $event_title = "<div class='app-detail'>All day {$event_date['appointment_name']}</div>";
                                }else if($event_datetime_start <= $datetime_today && $event_datetime_end < $datetime_tomorrow && $event_datetime_end >= $datetime_today){
                                $event_title = "<div class='app-detail'>{$start_time} - {$end_time} {$event_date['appointment_name']}</div>";
                                }else if($event_datetime_start >= $datetime_today && $event_datetime_end <= $datetime_tomorrow){
                                $event_title = "<div class='app-detail'>{$start_time} - {$end_time} {$event_date['appointment_name']}</div>";
                                }else{
                                $event_title = '';
                                }

                                $this_day_event_title .= $event_title;
                            }    
                            echo "<tr>";
                            echo " <td style='background-color:" . $colorevent . "'>" . date("D d M y", $time) . "<br />";
                            echo "     <div class='app-detail'>".$this_day_event_title."</div>";
                            echo " </td>";
                            echo "</tr>";
                        }
                    }else{
                        for($i = 1; $i <= $days_in_month; $i++){
                            $i = $i < 10 ? "0$i" : $i;
                            $this_date = "$selected_year-$selected_month-$i";
                            $day_of_week = date("w", strtotime($this_date));
                            $time = mktime(0, 0, 0, $selected_month, $i, $selected_year);         
                            if($day_of_week == 0){
                                $colorevent = "#ffbad2";
                            }else if($day_of_week == 6){
                                $colorevent = "#8f5e99";
                            }else{
                                $colorevent = "#F3F3F3;";
                            }

                            if($this_date == $current_date){
                                $colorevent = "#F2F5A9";
                            }
                            $this_day_event_title = '';

                            echo "<tr><td style='background-color:" . $colorevent . "'>";
                            echo "<div class='app-date'>" . date("D d M y", $time) . "</div>";
                            echo $this_day_event_title;
                            echo "</td></tr>";
                        }
                    }
                    
                ?>
                
            </table>
            
        </div>
    </div>
</div>    

<style>
.table th, .table td {
    border-top: 1px solid #DDDDDD;
    line-height: 15px;
    padding: 3px;
    text-align: left;
    vertical-align: top;
}


.app-date{
    text-align: right;
    width:100px;
}

.app-detail{
    margin: 5px 0px 0px 40px;
}
</style>