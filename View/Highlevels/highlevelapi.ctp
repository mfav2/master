<?php
    $current_date = date("Y-m-d");

    $timestamp = mktime(0, 0, 0, $selected_month, 1, $selected_year);
    $days_in_month = date("t", $timestamp);

    $tmp_month_list = range(1, 12);
    $month_list = array();
    foreach($tmp_month_list AS $i => $m){
        $month_list[$m] = date("M", mktime(0, 0, 0, $m, 1, $selected_year));
    }
    
    $tmp_years_list = range(date("Y") - 1, date("Y") + 1);
    foreach($tmp_years_list AS $i => $y){
        $years_list[$y] = $y;
    }

    
    if(!empty($appointments['event_date'])){
        echo '<table width="100%" class="table table-bordered table-striped table_vam">';
            
            $this_day_event_title = '';
            foreach($appointments['event_date'] AS $app_id => $event_date){
                //pr($appointments);
                $time = mktime(0, 0, 0, $selected_month, $selected_date, $selected_year);
                $datetime_today = date("Y-m-d H:i:s", $time);
                $datetime_tomorrow = date("Y-m-d 23:59:00", $time);
                
                $event_datetime_start = $event_date['start_date'];
                $event_datetime_end = $event_date['end_date'];
                
                //if(empty($event_date['appointment_name'])){
                    $event_date['appointment_name'] = $event_date['detail'];
                //}
                
                $start_time = substr($event_datetime_start, 11, 5);
                $end_time = substr($event_datetime_end, 11, 5);
                
                if($event_datetime_start <= $datetime_today && $event_datetime_end >= $datetime_tomorrow){
                    $event_title = "<div class='app-detail'>All day {$event_date['appointment_name']}</div>";
                }else if($event_datetime_start <= $datetime_today && $event_datetime_end < $datetime_tomorrow && $event_datetime_end >= $datetime_today){
                    $event_title = "<div class='app-detail'>{$start_time} - {$end_time} {$event_date['appointment_name']}</div>";
                }else if($event_datetime_start >= $datetime_today && $event_datetime_end <= $datetime_tomorrow){
                    $event_title = "<div class='app-detail'>{$start_time} - {$end_time} {$event_date['appointment_name']}</div>";
                }else{
                    $event_title = '';
                }
                
                $this_day_event_title .= $event_title;
                
            }
                echo "<tr><td>";
                echo "<div class='app-date'>" . date("D d M y", $time) . "</div>";
                echo $this_day_event_title;
                echo "</td></tr>";
            
        
        echo "</table>";
    }
    
    
?>

<?php //if(!empty($appointments)){ pr($appointments); } ?>

<style>
/*.table th, .table td {
    border-top: 1px solid #DDDDDD;
    line-height: 15px;
    padding: 3px;
    text-align: left;
    vertical-align: top;
}


.app-date{
    text-align: right;
    width:100px;
}

.app-detail{
    margin-left: 50px
}*/
</style>