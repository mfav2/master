<?php
    $currentUser = $this->session->read('AuthUser'); 
    $permiss =  $currentUser['PermissionGroup'];
    $permisssubscribe = $currentUser['PermissionSubscribe'];
    
    $showmanage  = 'N';
    foreach($permiss as $permission){
        if($permission['PermissionGroup']['function_id'] ==  2){
            if($permission['PermissionGroup']['is_add'] == 'Y'){
                $showmanage = 'Y'; 
            }   
        }
    }
    

    if(empty($selected_year)){ $selected_year = date("Y"); }
    if(empty($selected_month)){ $selected_month = date("m"); }
    
    if(!empty($this->request->data)){
	$high_level_group = $this->request->data['Highlevels']['high_level_group'];
    }else{
            $high_level_group = $this->request->query['high_level_group'];
    }
    
    $current_date = date("Y-m-d");
     
    /***************************** Month *************************************/ 
    $timestamp = mktime(0, 0, 0, $selected_month, 1, $selected_year);
    $days_in_month = date("t", $timestamp);

    $tmp_month_list = range(1, 12);
    $month_list = array();
    foreach($tmp_month_list AS $i => $m){
    //    $month_list[$m]['num_month'] = $m;
    //    $month_list[$m]['month_short_name'] = date("M", mktime(0, 0, 0, $m, 1, $selected_year));
        $month_list[$m] = date("M", mktime(0, 0, 0, $m, 1, $selected_year));
    }
    
    /***************************** Year **************************************/
    
    $tmp_years_list = range(date("Y") - 1, date("Y") + 1);
    foreach($tmp_years_list AS $i => $y){
        $years_list[$y] = $y;
    }
    $mission = __('Mission Mode');
    
    /*************************************************************************/
?>
<?php  echo $this->element('Components/breadcrumb'); ?>
<?php  //echo $this->element('Components/highleveltableform');      ?>
<div class="row-fluid">
    <div class="span12" align="right">
        <div class="boxbar_content">
          <?php  
                echo $this->form->create('Highlevels', array('action'=>'highlevelindex'));
                echo $this->form->input('selected_year', array('type'=>'select', 'label'=>false, 'div'=>false, 'options'=>$years_list, 'selected'=>$selected_year, 'style'=>'width:100px; margin:8px 5px 5px 0;'));
                echo $this->form->input('selected_month', array('type'=>'select', 'label'=>false, 'div'=>false, 'options'=>$month_list, 'selected'=>$selected_month, 'style'=>'width:100px; margin:8px 5px 5px 0;'));
                echo $this->form->input('high_level_group', array('type'=>'select', 'options'=>$highlevel_group, 'label'=>false, 'default'=>$high_level_group, 'div'=>false, 'style'=>' margin:8px 5px 5px 0;'));
                //echo "<button type='button' onclick='showmission(".$high_level_group.")'  id='missionmode' class='btn' style='margin:8px 5px 5px 0; padding:2px 5px;'>".$mission."</button>"; 
                if($showmanage == 'Y'){
                    echo "<a href='{$this->Portal->makeUrl('Highlevels', 'highlevelgroupindex')}' class='btn' style='margin:8px 0 5px 0; padding:2px 5px;'>".__('High Level Group Management')."</a>";
                }
                echo $this->form->end();
          ?>
            <div id="showajax">
                    <!-- Start Create Table High Level -->
                    <table class="table table-bordered table-striped table_vam" >
                          <tr style="background-color:#0695ff;">
                              <th align="center" class="textdatehigh" style="width:280px; text-align:center"><?= __($month_list[(int)$selected_month]) . "&nbsp;" . __($selected_year) ?></th>
                              <?php
                                  for ($i = 1; $i <= $days_in_month; $i++) {
                                      $Selecteddate = $selected_year . "-" . $selected_month . "-" . $i;
                                      $DayOfWeek = date("w", strtotime($Selecteddate));

                                      if ($DayOfWeek == 0){ //sunday
                                          if($i < 10){
                                              echo "<th style='background:#ffbad2; color:#fff; cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>0{$i}</th>";
                                          }else{
                                              echo "<th style='background:#ffbad2; color:#fff; cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>{$i}</th>";
                                          }
                                      }else if ($DayOfWeek == 6){ //saturday
                                          if($i < 10){
                                              echo "<th style='background:#8f5e99; color:#fff; cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>0{$i}</th>";
                                          }else{
                                              echo "<th style='background:#8f5e99; color:#fff; cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>{$i}</th>";
                                          }
                                      }else {
                                          if($Selecteddate == $current_date){
                                              if($i < 10){
                                                  echo "<th style='background:#F2F5A9; color:#fff; cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>0{$i}</th>";
                                              }else{
                                                  echo "<th style='background:#F2F5A9; color:#fff; cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>{$i}</th>";
                                              }
                                          }else{
                                              if($i < 10){
                                                  echo "<th style='cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>0{$i}</th>";
                                              }else{
                                                  echo "<th style='cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>{$i}</th>";
                                              }
                                          }
                                      }
                                  }
                              ?>
                          </tr>
                        <?php
                            if(!empty($user_list)){ 
                                foreach($user_list as $list){
                                    
                                   echo "<tr style='height:6px'>";
                                   echo "<td><a href='".$this->Portal->makeUrl('Highlevels', 'view', 'user_id=' . $list['Highlevel']['user_id'] . '&selected_year=' . $selected_year . '&selected_month=' . $selected_month)."'>".$list['Highlevel']['display_name']."<br /> [ ".$list['Highlevel']['position_name']." ]".$list['Highlevel']['organization_name']."</a></td>";
                                   
                                   for ($i = 1; $i <= $days_in_month; $i++) {
                                        $i = $i < 10 ? "0$i" : $i;
                                        
                                        $this_date = "$selected_year-$selected_month-$i";
                                        //echo $this_date;
                                        $day_of_week = date("w", strtotime($this_date));

                                        if($day_of_week == 0){
                                            $colorevent = "#ffbad2";
                                        }else if($day_of_week == 6){
                                            $colorevent = "#8f5e99";
                                        }else{
                                            $colorevent = "#F3F3F3;";
                                        }


                                        if($this_date == $current_date){
                                            $colorevent = "#F2F5A9";
                                        }
                                        
                                        echo "<td align='center' valign='middle' style='background-color:" . $colorevent . "'>";
                                        
                                        foreach($appointments AS $user_id => $ap){
                                            $location = '';
                                            if($user_id == $list['Highlevel']['user_id']){
                                                foreach($ap['event_date'] AS $app_id => $event_date){ 
                                                    $event_start_date = $event_date['start_date'];
                                                    $event_end_date = $event_date['end_date'];
                                                    $this_day_start_time = $this_date . " 00:00:00";
                                                    $this_day_end_time =  $this_date . " 23:59:59";

                                                    
                                                    if(empty($event_date['appointment_name'])){
                                                        $event_date['appointment_name'] = $event_date['detail'];
                                                    }

                                                    $arr_location = array(
                                                        //1=>'<img src="../img/icon/B_trans1.gif" class="personPopupTrigger" rel="' . $event_date['appointment_name'] . '" />',
                                                        //2=>'<img src="../img/icon/bus_trans1.gif" class="personPopupTrigger" rel="' . $event_date['appointment_name'] . '" />',
                                                        //3=>'<img src="../img/icon/airplane_trans1.gif" class="personPopupTrigger" rel="' . $event_date['appointment_name'] . '" />'
                                                        1=>'<div class="personPopupTrigger" rel="'.$ap['user_id'].','.$i.','.$selected_month.','.$selected_year.'"><img src="../img/icon/B_trans1.gif" style="cursor:pointer;" onclick="showevent('.$app_id.','.$ap['user_id'].','.$i.','.$selected_month.','.$selected_year.')" /></div>',
                                                        2=>'<div class="personPopupTrigger" rel="'.$ap['user_id'].','.$i.','.$selected_month.','.$selected_year.'" ><img src="../img/icon/bus_trans1.gif" style="cursor:pointer;" onclick="showevent('.$app_id.','.$ap['user_id'].','.$i.','.$selected_month.','.$selected_year.')" /></div>',
                                                        3=>'<div class="personPopupTrigger" rel="'.$ap['user_id'].','.$i.','.$selected_month.','.$selected_year.'" ><img src="../img/icon/airplane_trans1.gif" style="cursor:pointer;" onclick="showevent('.$app_id.','.$ap['user_id'].','.$i.','.$selected_month.','.$selected_year.')"  /></div>',
                                                    );

                                                    if($this_day_start_time >= $event_start_date && $this_day_start_time <= $event_end_date){
                                                        
                                                        $location = $arr_location[$event_date['location_type_id']];
                                                    }else if($event_start_date >= $this_day_start_time && $event_start_date <= $this_day_end_time){
                                                        $location = $arr_location[$event_date['location_type_id']];
                                                    }else if($event_start_date >= $this_day_start_time && $event_end_date <= $this_day_end_time){
                                                        $location = $arr_location[$event_date['location_type_id']];
                                                    }
                                                } 
                                                /**********************************/
                                            }
                                          echo $this_day_end_time;
                                           echo $location; 
                                        }
                                        
                                        
                                        
                                        echo "</td>";
                                   }
                                   
                                   echo "</tr>";
                                }
                            }else{
                                if(!empty($this->data)){
                                    echo "<tr><td></td><td colspan='{$days_in_month}'>".__('No planed for this month')."!!!</td></tr>";
                                }else{
                                    echo "<tr><td></td><td colspan='{$days_in_month}'></td></tr>";
                                }
                            }
                        ?>
                    </table>      
                    <!-- End Create Table High Level -->
            </div>
            
        </div>
    </div>
</div>    






<script src="/lib/qtip2/jquery.qtip.min.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        var reload = '<?php echo $this->Portal->makeUrl('Highlevels','highlevelindex'); ?>';
        var highlevelgroup = '<?php echo $this->Portal->makeUrl('Highlevels','highlevelgroupindex'); ?>';
        $('#thismonth').click(function(){
            window.location.href=reload;
        });
        $('#highlevelgroup').click(function(){
            window.location.href=highlevelgroup;
        });
    })
    
    $('div.personPopupTrigger').each(function() {
            $currentLink = $(this);
            var settings = $currentLink.attr('rel').split(',');
            var eventid = 0;
            var userId = settings[0];
            var selectday = settings[1];
            var selectmonth = settings[2];
            var selectyear = settings[3];
            $currentLink.qtip({
                content: {
                    text: 'Loading...',
                    ajax: {
                        url: '/Highlevels/highlevelapi/'+eventid+'/'+userId+'/'+selectday+'/'+selectmonth+'/'+selectyear,
                        loading: false
                    }
                },

                position: {
                        my: 'top left',
                        target: 'mouse',
                        viewport: $(window), // Keep it on-screen at all times if possible
                        adjust: {
                        x: 10,  y: 10
                        }
                },
                hide: {
                        fixed: true // Helps to prevent the tooltip from hiding ocassionally when tracking!
                },
                style: 'qtip-shadow',
                show: 'mouseover',
                hide: 'mouseout'
            });
    }); 
    
    $('#HighlevelsSelectedYear').change(function(){
            var year = $('#HighlevelsSelectedYear option:selected').val();
            var month = $('#HighlevelsSelectedMonth option:selected').val();
            var group = $('#HighlevelsHighLevelGroup option:selected').val(); 
            //alert(year+' : '+month+' : '+group);
            $.ajax({
                    cache: false,
                    url: "/Highlevels/highlevelajax",
                    type: "post",
                    data: {'high_level_group': group, 'selected_year': year, 'selected_month': month},
                    success:function(data){
                            $('#showajax').empty().append(data);
                            //alert(data);
                    }
            });
    });

    $('#HighlevelsSelectedMonth').change(function(){
            var year = $('#HighlevelsSelectedYear option:selected').val();
            var month = $('#HighlevelsSelectedMonth option:selected').val();
            var group = $('#HighlevelsHighLevelGroup option:selected').val(); 
            //alert(year+' : '+month+' : '+group);
            $.ajax({
                    cache: false,
                    url: "/Highlevels/highlevelajax",
                    type: "post",
                    data: {'high_level_group': group, 'selected_year': year, 'selected_month': month},
                success:function(data){
                            $('#showajax').empty().append(data);
                    }
            });
    });

    $('#HighlevelsHighLevelGroup').change(function(){
            var year = $('#HighlevelsSelectedYear option:selected').val();
            var month = $('#HighlevelsSelectedMonth option:selected').val();
            var group = $('#HighlevelsHighLevelGroup option:selected').val(); 
            $.ajax({
                    cache: false,
                    url: "/Highlevels/highlevelajax",
                    type: "post",
                    data: {'high_level_group': group, 'selected_year': year, 'selected_month': month},
                success:function(data){
                            $('#showajax').empty().append(data);
                    }
            });
    });

    function showtimeline(selectday, selectmonth, selectyear){
        var alluser  = $('#alluser').val();
        var group = $('#HighlevelsHighLevelGroup option:selected').val();
            $('#myModal').modal('show');
            $('#customModal').empty();
            $('#customModalHeader').html('<?=__('Timeline')?>');
            $('#customModalAction').html('<?=__('Close')?>');
         $.ajax({
            cache: false, 
            url: "/Highlevels/timeline",
            type: "post", 
            data: {'high_level_group': group, 'selecteddate':selectday, 'selecteddate':selectday, 'selectedmonth':selectmonth, 'selectedyear':selectyear },
            success:function(data){
                $('#customModal').html(data);
            }
         });

    }
    function showevent(event_id, userid, selectday, selectmonth, selectyear){
        $('#myModal').modal('show');
        $('#customModal').empty();
        $('#customModalHeader').html('<?=__('Appointment')?>');
        $('#customModalAction').html('<?=__('Next')?>');
        $('#customModal').load("/Highlevels/highlevelapi/"+event_id+'/'+userid+'/'+selectday+'/'+selectmonth+'/'+selectyear,function(data) {
            //$('#customModal').html(data);
        });
    }


    function showmission(group_id){
            var group = $('#HighlevelsHighLevelGroup option:selected').val(); 
            if(group != ""){
                    var group_id = group
            }

            var url = "/Highlevels/chkredirect/"+group_id;
            //alert(url);
             window.location.href=url;
    }

    function modalAction(){

    }   
    
</script>


<?php //pr($this->request); ?>

<style type="text/css">
    a.user:hover{
        text-decoration: underline;
        color: yellow;
    }
    .table th, .table td {
        border-top: 1px solid #DDDDDD;
        line-height: 15px;
        padding: 3px;
        text-align: left;
        vertical-align: top;
    } 
</style>