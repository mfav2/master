<?php  echo $this->element('Components/breadcrumb'); ?>

<?php if(!empty($highlevel)){ ?>
    <table class="table table-bordered  table_vam" id="highlevelgroup">
        <thead>
            <tr>
                <th><?php echo __('Display Name'); ?></th>
                <th><?php echo __('Position'); ?></th>
                <th><?php echo __('Organization'); ?></th>
		<th><?php echo __('Sort'); ?></th>
		<th><?php echo __('Action'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($highlevel as $high) { ?>
            <tr>
                <td><?php echo $high['Highlevel']['display_name']; ?></td>
                <td><?php echo $high['Highlevel']['position_name']; ?></td>
                <td><?php echo $high['Highlevel']['organization_name']; ?></td>
                <td><?php echo $high['Highlevel']['sort_order']; ?></td>
                <td>
		<a href="<?php echo $this->Portal->makeUrl('Highlevels', 'edithighlevel', 'id='.$high['Highlevel']['id'].'&group_id='.$this->request->query['highlevel_group_id']); ?>">	
			<i class="icon-pencil" style="cursor:pointer;"></i></a>&nbsp;
			<i class="icon-trash" onclick="deleteusergroup(<?php echo $high['Highlevel']['id']; ?>)" style="cursor:pointer;"></i>
		</td>
            </tr>
            <?php } ?>
        </tbody>
    </table>   
<?php }else{ 
        echo __("Not found any user in this group!!!");
      } 
?>
<script type="text/javascript">
    function clickAuthUser(id){
        $.ajax({ 
            cache: false,
            url : '/Highlevels/HighLevelSearchDate',
            type:'POST',
            data: {'useid': id},
            success:function(data){
                $('#popupAuthUser').empty().append(data);
                $("#popupAuthUser").dialog({width:400, height:400}); 
            }
			
        });
    }
    $(document).ready(function(){
        $('#thismonth').click(function(){
            //alert("test");
            window.location.reload();
        });
    })

	function deleteusergroup(id){
		
		$.ajax({
			cache: false,
            url : '/Highlevels/deleteuserinhighlevel',
            type:'POST',
            data: {'id': id},
            success:function(data){
                if(data == 1){
				  window.location.reload();
				}else{
				  alert(data);
				}
            }
		
		});
	}
    
	
</script>
