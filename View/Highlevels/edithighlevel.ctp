<?php  echo $this->element('Components/breadcrumb'); ?>
<?php
    
   echo $this->form->create($model, array('action'=>'edithighlevel', 'class'=>'form-horizontal well'));
   echo $this->form->input('id', array('type'=>'hidden','label'=> false, 'div'=>false));
?>
<table border="0" cellspacing="2" cellpadding="5">
	<tr>
		<td><?php echo __('Name'); ?></td>
		<td>
                        <?php echo $this->form->input('highlevel_group_id', array('type'=>'hidden','label'=> false, 'div'=>false)); ?>
			<?php echo $this->form->input('name', array('label'=> false, 'readonly'=>'readonly')); ?>
			<?php echo $this->form->input('user_id', array('type'=>'hidden','label'=> false, 'div'=>false)); ?>
                        <?php echo $this->form->input('new_name', array('type'=>'hidden','label'=> false, 'div'=>false)); ?>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>
                    <div>
                         <a id="EditHighlevelUser" class="btn" href="#myModal" data-backdrop="static" data-toggle="modal"><?php echo __('Choose User'); ?></a>
                    </div>
		</td>
	</tr>
	<tr>
		<td><?php echo __('Order'); ?></td>
		<td><?php echo $this->form->input('sort_order', array('label'=> false, 'onKeyPress'=>'CheckNum();')); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><?php echo $this->form->submit(__('Submit'), array('class'=>'btn')); ?></td>
	</tr>
</table>
<?php
			
   //echo $this->form->end();
?>
</form>
<script>
$(document).ready(function(){
    $('#customModalAction').removeAttr('data-dismiss');
});
$('#closechatbox').click(function(){
    $('#customModalAction').attr('data-dismiss','modal');
});

function CheckNum(){
	e_k=event.keyCode
	if (e_k != 13 && (e_k < 48) || (e_k > 57)) {
		event.returnValue = false;
	}
}

</script>
