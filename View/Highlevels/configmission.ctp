<?php  echo $this->element('Components/breadcrumb'); ?>
<?php
	$selected_month = date('m');
	$selected_year = date('Y');
	$timestamp = mktime(0, 0, 0, $selected_month, 1, $selected_year);
	$days_in_month = date("t", $timestamp);
?>
<div id="missionTable" class="row-fluid mousescroll">
    <div class=" span12" align="right">
        <div class="boxbar_content">
            <form  method="post" action="<?php echo $this->Portal->makeUrl('Highlevels', 'mission', 'group_id='.$this->request->query['group_id']); ?>">
                     <table class="table table-bordered table-striped table_vam">
                            <tr style="background-color:#0695ff;">
                                    <th  align="center">Date <br />
                                            <input type="checkbox" name="all" id="checkall" />
                                    </th>
                                    <th>


                                            <div class="span6">	
                                                    From <div class="input-append date" id="dp_start">
                                                                      <input name="from_date" class="span6" type="text" readonly="readonly" value="<?php echo '01/'.$selected_month.'/'.$selected_year; ?>" /><span class="add-on"><i class="splashy-calendar_day_up"></i></span>
                                                    </div>
                                            </div>

                                            <div class="span6">	 
                                                    To <div class="input-append date" id="dp_end">
                                                                    <input name="to_date" class="span6" type="text" readonly="readonly" value="<?php echo $days_in_month.'/'.$selected_month.'/'.$selected_year; ?>" /><span class="add-on"><i class="splashy-calendar_day_down"></i></span>
                                                    </div>
                                            </div>	
                                    </th>
                            </tr>
                            <?php
                                    if(!empty($user_list)){  
                                            foreach($user_list as $list){
                                                echo "<tr>";
                                                echo "<th align='center'><input type='checkbox' name='data[chooseuser][]' value='".$list['Highlevel']['user_id']."' ></th>";
                                                echo "<th>".$list['Highlevel']['display_name']." [ ".$list['Highlevel']['position_name']." ]</th>";
                                                echo "</tr>";
                                                    /*foreach($user_detail as $high_user){
                                                            if($high_user['id'] == $list){
                                                                    echo "<tr>";
                                                                    echo "<th align='center'><input type='checkbox' name='data[chooseuser][]' value='".$high_user['id']."' ></th>";
                                                                    echo "<th>".$high_user['detail']."</th>";
                                                                    echo "</tr>";
                                                            }
                                                    }*/
                                            }
                                    }
                            ?>
                            <tr>
                                    <td colspan="2"><input type="submit" value="<?php echo __('Submit');?>" /></td>
                            </tr>
                     </table>
            </form>	 
        </div>
    </div>
</div>

<style>
div#missionTable {
    
    height: 375px;
}
div.mousescroll {

    overflow: hidden;
}
div.mousescroll:hover {
    overflow: scroll;
}
.table {
  width: 100%;
  margin-bottom: 0px;
}
</style>
<script>
function configmission(group_id){
	var url = "/Highlevels/configmission/"+group_id;
	//alert(url);
	 window.location.href=url;
}
</script>

<script src="lib/slimScroll/jquery.slimscroll.min.js"></script>

<script>
$(document).ready(function(){
	$('#missionTable').slimscroll({
	  color: '#000',
	  size: '10px',

	  height: '370px'                  
	});

	
/************************************/
//set default
 
  
        $('#dp_start').datepicker({format: "dd/mm/yyyy"}).on('changeDate', function(ev){
                var dateText = $(this).data('date');

                var endDateTextBox = $('#dp_end input');
                if (endDateTextBox.val() != '') {
                        var testStartDate = new Date(dateText);
                        var testEndDate = new Date(endDateTextBox.val());
                        if (testStartDate > testEndDate) {
                                endDateTextBox.val(dateText);
                        }
                }
                else {
                        endDateTextBox.val(dateText);
                };
                $('#dp_end').datepicker('setStartDate', dateText);
                $('#dp_start').datepicker('hide');
        });
        $('#dp_end').datepicker({format: "dd/mm/yyyy"}).on('changeDate', function(ev){
                var dateText = $(this).data('date');
                var startDateTextBox = $('#dp_start input');
                var startdate = $('#dp_start input').val();
                
                if (startdate != '') {
                    
                        var start = convertdate(startdate);
                        var end = convertdate(dateText);
                        
                        var testStartDate = new Date(start);
                        var testEndDate = new Date(end);
                        if (testEndDate > testStartDate) {
                              $('#dp_end').datepicker('setStartDate', startdate);
                        }
                }
                else {
                        //startDateTextBox.val(dateText);
                }
                $('#dp_start').datepicker('setEndDate', dateText);
                $('#dp_end').datepicker('hide');
        }); 

});
$('#checkall:checkbox').change(function () {
    if($(this).attr("checked")) $('input:checkbox').attr('checked','checked');
    else $('input:checkbox').removeAttr('checked');
});

function convertdate(date_str){
    var datearray = date_str.split("/");
    var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
    return newdate;
}		
</script>