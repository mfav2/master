<!--<script src="js/custom/highlevelgroup_index.js"> </script>-->
<script>
function showdel(id){
    $("#deletegroup").modal("show");
    $("#deldata").val(id);
}
function new_highlevel(id){
    $("#addhighlevel_ingroup").modal("show");
    $("#dataid").val(id);
}
</script>
<?php
//pr($allgroup);
echo $this->element('Components/breadcrumb');
?>
<div class="row-fluid">
    <div class="btn-group sepH_b">
        <button data-toggle="dropdown" class="btn dropdown-toggle"><?=__('Action')?> <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <li>
                <a class="addgroup" data-tableid="smpl_tbl" data-toggle="modal" data-backdrop="static" href="#addgroup">
                    <i class="splashy-add_small"></i><?=__('Add Group')?></a>
            </li>
        </ul>
    </div>
    <table class="table table-bordered  table_vam" id="highlevelgroup">
        <thead>
            <tr>
                <th width="90%"><?=__('High Level Group')?></th>
                <th><i class="splashy-group_blue"></i></th>
                <th><i class="icon-pencil"></i></th>
                <th><i class="icon-trash"></i></th>
            </tr>
        </thead>
        <tbody>
            <?php
            //pr($allgroup);
            foreach ($allgroup as $showall) :
                echo '<tr>';
                echo '<td><a href="' . $this->Portal->makeUrl('Highlevels', 'highlevelview', 'highlevel_group_id=' . $showall['HighlevelGroup']['id']) . '">' . $showall['HighlevelGroup']['highlevel_group_name'] . '</a></td>';
                /*echo '<td>' . 
                '<i class="splashy-group_blue_add" style="cursor:pointer;" onclick="new_highlevel('.$showall['HighlevelGroup']['id'].')"></i>' 
                . '</td>';*/
                echo '<td>' . 
                '<a class="add_user" class="label ttip_b" highlevel_group_id="' . $showall['HighlevelGroup']['id'] . '" href="#myModal" data-backdrop="static" data-toggle="modal" title="Add User"><i class="splashy-group_blue_add" style="cursor:pointer;"></i></a>' 
                . '</td>';
                echo '<td>' .
                '<a class="editgroup" data-toggle="modal" data-backdrop="static" href="#' . $showall['HighlevelGroup']['id'] . 'g' . '">' .
                '<i class="icon-pencil"></i></a>';
                        
                echo'</td>';
                echo '<td>' .
                '<i class="icon-trash" style="cursor:pointer;" onclick="showdel('.$showall['HighlevelGroup']['id'].')"></i>' .
                '</td>';
                
                $gname = $showall['HighlevelGroup']['highlevel_group_name'];
                echo $this->Form->create($this->name, array('action' => 'editgroup'));
                ?>
            <div class="modal hide fade" id="<?= $showall['HighlevelGroup']['id'] . 'g'; ?>">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">×</button>
                    <h3><?=__('Edit Group')?></h3>
                </div>
                <div class="modal-body">
                    <div align="center">
                        <?=__('Group Name')?>: <input type="text" name="editgroup"  value='<?php echo $gname; ?>' /> 
                        <input type="hidden" name="idgroup" value='<?php echo $showall['HighlevelGroup']['id']; ?>' />
                    </div>
                </div>
                <div class="modal-footer">
                    <div align="right"><input type="submit" class="btn" value="<?=__('SAVE')?>"></div>
                </div> 
            </div>
            <?=
            $this->Form->end();
            echo '</tr>';
        endforeach;
        ?>
        </tbody>
    </table>
</div>




<?= $this->Form->create($this->name, array('action' => 'addgroup')); ?>
<div class="modal hide fade" id="addgroup">
    <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>
        <h3><?=__('New High Level Group')?></h3>
    </div>
    <div class="modal-body">
        <div align="center">
            <?=__('Group Name')?> : <input id="grouphigh" type="text" class="text" name="group"> 
            <!--<label> Group Name : </label>-->
<?php // echo $this->Form->input('highlevel_group_name',array('id' => 'groupHigh','type'=>'text','label' =>false,'class'=>'control-label'));      ?>

        </div>
    </div>
    <div class="modal-footer">
        <div align="right"><input type="submit" class="btn" value="<?=__('SAVE')?>"></div>
    </div> 
</div>
<?= $this->Form->end(); ?>

<?= $this->Form->create($this->name, array('action' => 'deletegroup')); ?>
<div class="modal hide" id="deletegroup">
    <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>
        <h3><?=__('Delete Group')?></h3>
    </div>
    <div class="modal-body">
        <div align="center">
            <?=__('Are you sure delete')?>?
            <input id="deldata" name="idgroup" type="hidden" value="">
        </div>
    </div>
    <div class="modal-footer">
        <div align="right"><input type="submit" class="btn" value="Delete">
        <input type="button" class="btn" value="Cancel" data-dismiss="modal">
        </div>
    </div> 
</div>
<?= $this->Form->end(); ?>

<?= $this->Form->create($this->name, array('action' => 'addhighlevel_ingroup')); ?>
<div class="modal hide" id="addhighlevel_ingroup">
    <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>
        <h3><?=__('New High level')?></h3>
    </div>
    <div class="modal-body">
        <div align="center">
            <input id="newhigh" type="text" class="text" name="highlevel">
            <input id="dataid" name="idgroup" type="hidden" value="">
        </div>
    </div>
    <div class="modal-footer">
        <div align="right"><input type="submit" class="btn" value="Add High Level">
        <input type="button" class="btn" value="Cancel" data-dismiss="modal">
        </div>
    </div> 
</div>
<?= $this->Form->end(); ?>
<div id="highlevel_group_id" style="display:none"></div>


<!--<script type="text/javascript" src="/js/custom/highlevel.js"></script>-->
<script type="text/javascript">
$(document).ready(function() {
    $(".add_user").click(function(event){
        $('#highlevel_group_id').text($(this).attr('highlevel_group_id'));
        
        $('#customModalHeader').html('User Directory');
        $('#customModalAction').html('Next');
        $('#customModal').load("/Widget/userDirectory/highlevel", function(data) {
            //$('#customModal').html(data);
        });
    });
});
</script>
