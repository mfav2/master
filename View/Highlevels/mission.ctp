<?php  echo $this->element('Components/breadcrumb'); ?>
<div class="row-fluid">
	
	<span><a href="<?php echo $this->Portal->makeUrl('Highlevels', 'configmission', 'group_id='.$this->request->query['group_id']); ?>" class="btn"><?php echo __('Config Mission Mode');?> </a></span>
	<span style="margin: 0 10px 0 10px; font-weight:bold;">From&nbsp;:&nbsp;<?php echo $this->Time->format('d M y', $from_dat); ?></span>
	<span style="font-weight:bold;">To&nbsp;:&nbsp;<?php echo $this->Time->format('d M y', $to_dat); ?></span>

</div>
<div id="missionTable" class="row-fluid mousescroll">
    <div class="span12" align="right" >
        <div class="boxbar_content">
            <table class="table table-bordered table-striped table_vam" >
                <tr style="background-color:#fff;">
                    <th width="200">Date<br/><img src="/img/mission_bar_200.jpg"></th>
                    <th width="200">Mission<br/><img src="/img/mission_bar_200.jpg"></th>
                    <?php   
                        if(!empty($user_list)){
                            foreach($user_list as $list){
                                if(!empty($list['Highlevel']['user_id']) || $list['Highlevel']['user_id'] != 0){
                                        echo "<th style='font-weight:normal; font-size:12px; line-height:15px;'>";
                                        echo $list['Highlevel']['display_name'].'<br />[ '.$list['Highlevel']['position_name'].' ]';
                                        echo "<br/><img src='/img/mission_bar_200.jpg'></th>";
                                }
                            }
                        } 
                    ?>
                </tr>
                <?php 
                      foreach($appointments as $appointment){
                            $start = $this->Time->format('d M y', $appointment['Appointment']['start_date']); 
                            $end = $this->Time->format('d M y', $appointment['Appointment']['end_date']);
                            $stime = $this->Time->format('H:i', $appointment['Appointment']['start_date']); 
                            $etime = $this->Time->format('H:i', $appointment['Appointment']['end_date']);
                            $app_user_id = $appointment['Appointment']['user_id'];
                             if($stime == '00:00' && $etime == '23:59'){
                                $stime = '';
                                $etime = '';
                             }
                             $begin = $start." ".$stime;
                             $last = $end." ".$etime;
                             $detail =  $appointment['Appointment']['detail'];
                             if($appointment['Appointment']['detail'] == '-' || empty($appointment['Appointment']['detail'])){
                                $detail = $appointment['Appointment']['private_note'];
                             }
                ?>
                        <tr>
                            <td><?php echo $begin; ?> - <?php echo $last; ?></td>
                            <td><?php echo $detail; ?></td>
                            <?php 
                                if(!empty($user_list)){
                                    foreach($user_list as $list){
                                        echo "<td style='text-align:center;'>";
                                        if($list['Highlevel']['user_id'] == $app_user_id){
                                            echo  "&nbsp;<i class='icon-ok'></i>";
                                        }    
                                        echo "</td>";
                                    }
                                }
                            ?>    
                        </tr>
                <?php } ?>
            </table>
	</div>
    </div>
</div>

<style>
div#missionTable {
    /*width: 50px;*/
    max-height: 500px;
	/*border:1px solid #dddddd;*/

}
div.mousescroll {
    overflow: hidden;
}
div.mousescroll:hover {
    overflow: scroll;
}

/***** Table ******/
.table {
  width: 100%;
  margin-bottom: 0px;
}

.table_vam th{vertical-align: middle; text-align:center;}


</style>


<!--<script src="lib/slimScroll/jquery.slimscroll.min.js"></script>

<script>
$(document).ready(function(){
	$('#missionTable').slimscroll({
	  color: '#000',
	  size: '10px',
	  height: '200px'                  
	});


});
</script>-->