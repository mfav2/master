<?php $currentUser = $this->Session->read('AuthUser');?>
<div class="sepH_c">
    <div class="chat_box">
        <div class="row-fluid">
            <div class="span12 chat_sidebar">
                <div class="chat_heading clearfix">
                    <div class="btn-group pull-right">
                        <a id="addContact" class="btn btn-mini ttip_t" href="#myModal" data-backdrop="static" data-toggle="modal" title="Add a Contact"><i class="splashy-contact_blue_add"></i></a>                    
                        <a id="refreshList" href="#" class="btn btn-mini ttip_t" title="Refresh list"><i class="icon-refresh"></i></a>
                    </div>
                    <?php echo __('Contact list'); ?>
                </div>
              <ul class="chat_user_list" style ="background:#FFF; height:464px; overflow-y: auto;">
<!--                    <li>
                        <a href="javascript:void(0)">
                            <?php 
                                $my_img_key = $this->Session->read('AuthUser.AuthUserProfile.0.image_key'); 
                                if(empty($my_img_key)){
                                    $my_img_key = 1;
                                }
                            ?>
                            <img src="<?php echo $this->FileStorageHelper->urlByKey($my_img_key);?>" style="width:30px; height: 30px" />
                            <?php echo  $this->Session->read('AuthUser.AuthUserProfile.0.first_name_th') . ' ' . 
                                        $this->Session->read('AuthUser.AuthUserProfile.0.last_name_th'); 
                            ?>
                                    <span>(you)</span>
                        </a>
                    </li>                                            -->
                    <?php
                        if(!empty($messageGroup)){
                            foreach($messageGroup AS $i => $group){
                                $g = $group['MessageGroup'];
				$groupuser = $group['MessageGroupUser'];
                                if(!empty($g['image_key'])){
                                    $img = '<img src="' . $this->FileStorageHelper->urlByKey($g['image_key']) . '" style="width:30px; height: 30px" />';
                                }else{
                                    $img = '<img src="img/user_avatar.png" alt="" class="user_avatar">';
                                }
							if(!empty($CountMessage['Group'])){
                                foreach($CountMessage['Group'] as $group_id=>$Message){
                                     if($g['id'] == $group_id){
					  //pr($groupuser);
                                            echo "
                                                <li id='UserContact{$g['id']}' style='margin:0px; padding:0px;'>
                                                <span class='pull-left'>
                                                <a id='contactlist' group_id='{$g['id']}' class='contactlist' href='#myModal' data-backdrop='static' data-toggle='modal'>
                                                   {$img}
						   <span id='changegroup{$g['id']}'>
							{$g['group_name']}
						   </span>
                                                </a>
                                                </span>";
                                               
                                            echo "<span id='showcountmsg' class='pull-right label label-important' style='margin:15px 5px 0 0;'>
                                                 
                                                    ".$Message."  
                                                 </span>"; 
											
                                            echo "<span class='pull-right' style='margin:15px 5px 0 0; cursor:pointer;' onclick='deleteGroup({$g['id']})'><i class='splashy-group_blue_remove'></i></span>";
											if($g['contact_type'] == 2){
												foreach($groupuser as $msggroup){
									
													if($msggroup['deleted'] == 'N'){
														if($msggroup['is_admin'] == 'Y'){
															if($currentUser['AuthUser']['id'] == $msggroup['user_id']){	
															  echo "<span class='pull-right' style='margin:15px 5px 0 0; cursor:pointer;' onclick='editGroup({$g['id']})'><i class='splashy-group_blue_edit'></i></span>";
															}
														}else{
															if($currentUser['AuthUser']['id'] == $msggroup['user_id']){	
															 echo "<span class='pull-right' style='margin:15px 5px 0 0; cursor:pointer;' onclick='viewGroup({$g['id']})'><i class='splashy-group_blue'></i></span>";
															}
														}

													}

												}
												/*foreach($groupuser as $msggroup){
													if($currentUser['AuthUser']['id'] == $msggroup['user_id'] && $msggroup['is_admin'] == 'Y'){
													  echo "<span class='pull-right' style='margin:15px 5px 0 0; cursor:pointer;' onclick='editGroup({$g['id']})'><i class='splashy-group_blue_edit'></i></span>"; 
													}else{
														echo "<span class='pull-right' style='margin:15px 5px 0 0; cursor:pointer;' onclick='editGroup({$g['id']})'><i class='splashy-group_blue'></i></span>";
													}
												}*/
											}

                                            echo "</li>";
                                     }
                                }
							  } // #Check CountMessage 	
                                
                            }
                        }
                    ?>
                </ul>

            </div>
        </div>
    </div>    
</div>



<!--<i id="btn_upload_file" class="icon-adt_atach"></i>-->
<span id="addGroup"></span>
<input id="user_id" class="chkuser_id" type="hidden" value="<?php echo $currentUser['AuthUser']['id'];?>" />
<!--<input id="active_contact_id" type="hidden" value="" />
<input id="last_message_time" type="hidden" value="" />
<input id="is_fetch_data_completed" type="hidden" value="0" />-->

<textarea id="txtarea" style="display:none"></textarea>

<!-- chat functions -->
<script src="js/custom/widget/mini_chat.js"></script>
<script>
function deleteGroup(group_id){
     if(confirm('<?php echo __('Confirm to delete'); ?> ?')==true){
        $.ajax({
            type: "POST",
            url: "Messages/deletedGroup",
            cache: false,
            data: {'group_id': group_id},
            success: function(data){
                $('#UserContact'+group_id).remove();
            }
        });  
    }
}  

function editGroup(group_id){
		$('#myModal').modal('show');
		$('#customModal').empty();
        $('#customModalHeader').html('Edit Group Name');
        $('#customModalAction').html('Save');
        $('#customModal').load("/Messages/editgroup/"+group_id+"/edit_group_name?_="+Math.random(),function(data) {
            //$('#customModal').html(data);
            /*********************************/
            
            /*********************************/
        });

	
}

function viewGroup(group_id){
		$('#myModal').modal('show');
		$('#customModal').empty();
        $('#customModalHeader').html('View User In Group');
        $('#customModalAction').html('Close');
        $('#customModal').load("/Messages/viewgroup/"+group_id+"/edit_group_name?_="+Math.random(),function(data) {
            //$('#customModal').html(data);
            /*********************************/
        });
}
</script>