<script src="js/gebo_tables.js"></script>
<script>
    //    $('#otherProfile').click(function(){
    //        $('#profileBox').load('/profiles/otherprofile');
    //    });
</script>
<style>
    .splashy-check:hover {
        background-position: -42px -1314px;
    }
    .splashy-mail_light_new_2:hover {
        background-position: -42px -1398px;
    }
</style>
<a id="smoke_alert" class="btn" href="#">Alert</a>
<div>
    <h3 class="heading">MFA Connects</h3>
    <div class="tabbable tabbable-bordered">
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#tab_br1">Received Message</a>
            </li>
            <li>
                <a data-toggle="tab" href="#tab_br2">Send Message</a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="tab_br1" class="tab-pane active">
                <div class="row-fluid">
                    <div class="span5">
                        <a class="btn">Delete selected messages</a>
                    </div>
                    <div class="span7" style="text-align: right;">
                        Delete history in all message
                        <select>
                            <option>Older than 1 month</option>
                            <option>Older than 2 months</option>
                            <option>Older than 6 months</option>                    
                        </select>
                        <a class="btn">Enter</a>
                    </div>
                </div>
                <table id="rec_tbl" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="table_checkbox">
                                <input class="select_rows" type="checkbox" data-tableid="rec_tbl" name="select_rows">
                            </th>
                            <th width="200px">From (Received on)</th>
                            <th width="10px"><i class="splashy-mail_light_new_2"></i></th>
                            <th>Message</th>
                            <th width="10px"><i class="icon-adt_atach"></i></th>
                            <th width="10px"><i class="icon-adt_atach"></i></th>
                            <th width="10px"><i class="icon-adt_atach"></i></th>
                            <th width="10px">CC</th>
                            <th width="10px"><i class="splashy-mail_light_left"></i></th>
                            <th width="10px"><i class="splashy-mail_light_right"></i></th>
                            <th width="10px"><i class="icon-adt_trash"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($recMes as $mes) : ?>
                            <tr>
                                <td>
                                    <input type="checkbox" name="row_sel">
                                </td>
                                <td><a href="#box" data-backdrop="static" data-toggle="modal" onclick="showuser(<?= $mes['sender_id'] ?>)"><?= $mes['sender']; ?></a><span class="help-block"><?= $mes['time']; ?></span></td>
                                <td><a href="#" title=<?= $mes['isnew'] == 1 ? '"Unread"><i class="splashy-mail_light_new_2"></i>' : '"Opened"<i class="splashy-mail_light_stuffed"></i>'; ?></a></td>
                                <td><?= $mes['message'] ?></td>
                                <td><?= $mes['att1'] == null ? '' : '<a title="' . $mes['att1'] . '" href="' . $mes['att1'] . '"><i class="icon-adt_atach"></i></a>'; ?></td>
                                <td><?= $mes['att2'] == null ? '' : '<a title="' . $mes['att2'] . '" href="' . $mes['att2'] . '"><i class="icon-adt_atach"></i></a>'; ?></td>
                                <td><?= $mes['att3'] == null ? '' : '<a title="' . $mes['att3'] . '" href="' . $mes['att3'] . '"><i class="icon-adt_atach"></i></a>'; ?></td>
                                <td><a href="#"<?= $mes['iscc'] == 1 ? 'CC' : ''; ?></a></td>
                                <td><a title="Reply" href="#"><?= $mes['isrep'] == 1 ? '<i class="splashy-check">' : '<i class="splashy-mail_light_left"></i>'; ?></a></td>
                                <td><a title="Forward" href="#"><i class="splashy-mail_light_right"></i></a></td>
                                <td><a title="Delete" href="#"><i class="icon-adt_trash"></i></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <div id="tab_br2" class="tab-pane">
                <div class="row-fluid">
                    <div class="span5">
                        <a class="btn">Delete selected messages</a>
                    </div>
                    <div class="span7" style="text-align: right;">
                        Delete history in all message
                        <select>
                            <option>Older than 1 month</option>
                            <option>Older than 2 months</option>
                            <option>Older than 6 months</option>                    
                        </select>
                        <a class="btn">Enter</a>
                    </div>
                </div>
                <table id="sen_tbl" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="table_checkbox">
                                <input class="select_rows" type="checkbox" data-tableid="sen_tbl" name="select_rows">
                            </th>
                            <th width="200px">To (Sent on)</th>
                            <th width="10px"><i class="splashy-mail_light_new_2"></i></th>
                            <th>Message</th>
                            <th width="10px"><i class="icon-adt_atach"></i></th>
                            <th width="10px"><i class="icon-adt_atach"></i></th>
                            <th width="10px"><i class="icon-adt_atach"></i></th>
                            <th width="10px">CC</th>
                            <th width="10px"><i class="splashy-mail_light_right"></i></th>
                            <th width="10px"><i class="icon-adt_trash"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($recMes as $mes) : ?>
                            <tr>
                                <td>
                                    <input type="checkbox" name="row_sel">
                                </td>
                                <td><a href="#"><?= $mes['sender']; ?></a><span class="help-block"><?= $mes['time']; ?></span></td>
                                <td><a href="#" title=<?= $mes['isnew'] == 1 ? '"Unread"><i class="splashy-mail_light_new_2"></i>' : '"Opened"<i class="splashy-mail_light_stuffed"></i>'; ?></a></td>
                                <td><?= $mes['message'] ?></td>
                                <td><?= $mes['att1'] == null ? '' : '<a title="' . $mes['att1'] . '" href="' . $mes['att1'] . '"><i class="icon-adt_atach"></i></a>'; ?></td>
                                <td><?= $mes['att2'] == null ? '' : '<a title="' . $mes['att2'] . '" href="' . $mes['att2'] . '"><i class="icon-adt_atach"></i></a>'; ?></td>
                                <td><?= $mes['att3'] == null ? '' : '<a title="' . $mes['att3'] . '" href="' . $mes['att3'] . '"><i class="icon-adt_atach"></i></a>'; ?></td>
                                <td><a href="#"<?= $mes['iscc'] == 1 ? 'CC' : ''; ?></a></td>
                                <td><a title="Forward" href="#"><i class="splashy-mail_light_right"></i></a></td>
                                <td><a title="Delete" href="#"><i class="icon-adt_trash"></i></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>