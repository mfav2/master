<div class="row-fluid">

	<div class="span12">

	    <label><?php echo __('Group Name');?>&nbsp;:</label>
	    <input id="groupname" type="text" value="<?php echo $showgroup ;?>">
		<input id="groupid" type="hidden" value="<?php echo $groupId ;?>">
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		 <label><?php echo __('User List');?><br />
		 <small class="s_color sl_email">(<?php echo __('Select User when you want to take off your group.');?>)</small></label>
		 <div id="showuser">
			<ul style="list-style:none; margin:5px 5px;">
			<?php foreach($showuser as $show) { ?>
				<li>
					<?php if($show['MessageGroupUser']['is_admin'] == 'N') { ?>
							
				
							<input id="<?php echo $show['MessageGroupUser']['id'] ?>" class="select_row" type="checkbox" name="rowname" /> &nbsp; <span><?php echo $show['MessageGroupUser']['show_user']." [ ".$show['MessageGroupUser']['organization_description']['description']." - ".$show['MessageGroupUser']['organization_belong']." ]"; ?></span>
					<?php } ?>
					
				    
				</li>
			<?php } ?>
			</ul>
		 </div>
	</div>
</div>
<?php
if(empty($event)) $event = 'click_select';
?>
<script src="js/custom/event/<?php echo $event;?>.js"></script>
<script>
$(document).ready(function(){
    $('#customModalAction').removeAttr('data-dismiss');
});
$('#closechatbox').click(function(){
    $('#customModalAction').attr('data-dismiss','modal');
});
</script>
<style>
.modal {
    background-clip: padding-box;
    background-color: #FFFFFF;
    border: 8px solid rgba(0, 0, 0, 0.5);
    border-radius: 6px 6px 6px 6px;
    box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
    left: 50%;
    margin: -295px 0 0 -390px;
    overflow: auto;
    position: fixed;
    top: 50%;
    width: 850px;
    z-index: 1050;
}

.modal-body {
    min-height: 450px;
    max-height: 450px;
    overflow-y: auto;
}
#showuser{
	border:1px solid #ccc;
	height: 300px;
	/*overflow-y:scroll;*/
	overflow-x: hidden;
}
 </style>