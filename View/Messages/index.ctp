<?php $currentUser = $this->Session->read('AuthUser');?>
<div class="row-fluid">
    <div class="span12">
        <div class="chat_box">
            <div class="row-fluid">
                <div class="span4 chat_sidebar">
                    <div class="chat_heading clearfix">
                        <div class="btn-group pull-right">
                            <a id="addContact" class="btn btn-mini ttip_t" href="#myModal" data-backdrop="static" data-toggle="modal" title="Add a Contact"><span id="countMyTask"><i class="splashy-contact_blue_add"></i></span></a>
                            <!--<a href="#" class="btn btn-mini ttip_t" title="Create a Group"><i class="splashy-group_blue_add"></i></a>-->
                            <a id="refreshList" href="#" class="btn btn-mini ttip_t" title="Refresh list"><i class="icon-refresh"></i></a>
<!--                            <a href="#" data-toggle="dropdown" class="dropdown-toggle btn btn-mini ttip_t" title="Options"><i class="icon-cog"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Ban selected users</a></li>
                                <li><a href="#">Another action</a></li>
                            </ul>	-->
                        </div>
                        <?php echo __('Contact list'); ?>
                    </div><?php //pr($_SESSION); ?>
                    <ul class="chat_user_list">
                        <li>
                            <a href="javascript:void(0)"><?php //pr($this->Session->read('AuthUser')); ?>
                                <?php 
                                    $my_img_key = $this->Session->read('AuthUser.AuthUserProfile.0.image_key'); 
                                    if(empty($my_img_key)){
                                        $my_img_key = 1;
                                    }
                                ?>
                                <img src="<?php echo $this->FileStorageHelper->urlByKey($my_img_key);?>" style="width:30px; height: 30px" />
                                <?php echo  $this->Session->read('AuthUser.AuthUserProfile.0.first_name_th') . ' ' . 
                                            $this->Session->read('AuthUser.AuthUserProfile.0.last_name_th'); 
                                ?>
                                        <span>(you)</span>
                            </a>
                        </li>                                            
                        <?php
                            if(!empty($messageGroup)){
                                foreach($messageGroup AS $i => $group){
                                    $g = $group['MessageGroup'];
                                    
                                    if(!empty($g['image_key'])){
                                        $img = '<img src="' . $this->FileStorageHelper->urlByKey($g['image_key']) . '" style="width:30px; height: 30px" />';
                                    }else{
                                        $img = '<img src="img/user_avatar.png" alt="" class="user_avatar">';
                                    }
                                    foreach($CountMessage['Group'] as $group_id=>$Message){
                                        
                                        if($g['id'] == $group_id){
                                            echo "
                                                <li id='UserContact{$g['id']}'>
                                                    <span class='pull-left'>
                                                        <a href='#' class='contactlist' cid='{$g['id']}'>
                                                            {$img}
                                                            {$g['group_name']}
                                                        </a>
                                                    </span>
                                                 ";
                                            echo "<span id='showcountmsg' class='pull-right label label-important' style='margin:15px 5px 0 0;'>
                                                    ".$Message."  
                                                 </span>";  
                                            echo "<span class='pull-right' style='margin:15px 5px 0 0; cursor:pointer;' onclick='deleteGroup({$g['id']})'><i class='splashy-group_blue_remove'></i></span>";    
                                            echo "</li>";
                                        }
                                    }
                                    
                                }
                            }
                        ?>
                    </ul>

                </div>


                <div class="span8 chat_content">
    
                    <div class="chat_heading clearfix">
                        <span id="contactname" class="pull-left"></span>
                        <span id="chat-control" class="pull-right" style="display:none;">
                            <a href="#" id="play_scroll"  class="btn btn-mini ttip_t" title="scrollbar enable"><i class="splashy-media_controls_play_small"></i></a>
                            <a href="#" id="pause_scroll"  class="btn btn-mini ttip_t" title="scrollbar disable"><i class="splashy-media_controls_pause_small"></i></a>
                            
                        </span>
                        <!--<div class="pull-right"><i class="icon-remove chat_close"></i></div>
                        <span class="act_users"></span>-->
                    </div>

                    <div class="msg_window">
                        <div class="chat_msg clearfix msg_clone" style="display:none">
                            <div class="chat_msg_heading"><span class="chat_msg_date"></span><span class="chat_user_name"></span></div>
                            <div class="chat_msg_body"></div>
                        </div>

                        <!--<div class="chat_msg clearfix">
                            <div class="chat_msg_heading"><span class="chat_msg_date">12:44</span><span class="chat_user_name">Summer Throssell</span></div>
                            <div class="chat_msg_body">Lorem ipsum dolor sit amet, .</div>
                        </div>-->

                    </div>

                    <div class="chat_editor_box">
                        <textarea name="chat_editor" id="chat_editor" cols="30" rows="3" class="span12"></textarea>
                        <div id="btn_chat" class="btn-group send_btns">
                            <a href="#" class="btn btn-mini send_msg">Send</a>
			    <a href="javascript:void(0)" class="btn btn-mini enter_msg active" data-toggle="button" style="display: none;"><i class="icon-adt_enter"></i></a>
<!--			    <span class="btn btn-mini"><i id="btn_upload_file" class="icon-adt_atach"></i></span>-->
                <a href="javascript:void(0)" id="btn_upload_file" class="btn btn-mini" style="cursor:default;" ><i id="btn_upload_file"  class="splashy-folder_modernist_stuffed_add"></i></a>
                        </div>

                        <input type="hidden" name="chat_user" id="chat_user" value="<?php echo $this->Session->read('AuthUser.AuthUserProfile.0.first_name_th') . " " . $this->Session->read('AuthUser.AuthUserProfile.0.last_name_th'); ?>" />
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

<!--<i id="btn_upload_file" class="icon-adt_atach"></i>-->
<span id="addGroup"></span>
<input id="user_id" class="chkuser_id" type="hidden" value="<?php echo $currentUser['AuthUser']['id'];?>" />
<input id="active_contact_id" type="hidden" value="" />
<input id="last_message_time" type="hidden" value="" />
<input id="is_fetch_data_completed" type="hidden" value="0" />

<textarea id="txtarea" style="display:none"></textarea>

<link rel="stylesheet" href="lib/CLEditor/jquery.cleditor.css" />
<script type="text/javascript" src="js/custom/upclick.js"></script>


<!-- CLEditor -->
<script src="lib/CLEditor/jquery.cleditor.js"></script>
<script src="lib/CLEditor/jquery.cleditor.icon.min.js"></script>
<!-- date library -->
<script src="lib/moment_js/moment.min.js"></script>
<!-- chat functions -->
<script src="js/custom/widget/chat.js"></script>
<script src="js/gebo_chat.js"></script>
<script>
function deleteGroup(group_id){
    if(confirm('<?php echo __('Confirm to delete'); ?> ?')==true){
        $.ajax({
            type: "POST",
            url: "Messages/deletedGroup",
            cache: false,
            data: {'group_id': group_id},
            success: function(data){
                if(data == 1){
                    $('#UserContact'+group_id).remove();
                }else{
                    alert(data);
                }
            }
        });  
    }
}    
</script>