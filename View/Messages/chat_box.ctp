<?php $currentUser = $this->Session->read('AuthUser');?>

<div>
	<div class="chat_box">
		<div class="row-fluid">
                    <div class="span12 chat_content">
    
						<div class="chat_heading clearfix">
							<span id="contactname" class="pull-left"><?php echo $display; ?></span>
							<span id="chat-control" class="pull-right">
								<a href="#" id="play_scroll"  class="btn btn-mini ttip_t" title="scrollbar enable"><i class="splashy-media_controls_play_small"></i></a>
								<a href="#" id="pause_scroll"  class="btn btn-mini ttip_t" title="scrollbar disable"><i class="splashy-media_controls_pause_small"></i></a>
								
							</span>
							
						</div>

						<div class="msg_window">
							<div class="chat_msg clearfix msg_clone" style="display:none">
								<div class="chat_msg_heading"><span class="chat_msg_date"></span><span class="chat_user_name"></span></div>
								<div class="chat_msg_body"></div>
							</div>

							

						</div>

						<div class="chat_editor_box">
							<textarea name="chat_editor" id="chat_editor" cols="30" rows="3" class="span12"></textarea>
							<div id="btn_chat" class="btn-group send_btns">
								<div style="float:left;">  
									<a href="#" class="btn btn-mini send_msg">Send</a>
									<a href="javascript:void(0)" class="btn btn-mini enter_msg active" data-toggle="button" style="display: none;"><i class="icon-adt_enter"></i></a>
								</div>    
									<span id="btn_upload_file" class="btn btn-mini"><i class="splashy-folder_modernist_stuffed_add"></i></span>
								<div style="clear:both;"></div>    
								
							</div>  
							<input type="hidden" name="chat_user" id="chat_user" value="<?php echo $this->Session->read('AuthUser.AuthUserProfile.0.first_name_th') . " " . $this->Session->read('AuthUser.AuthUserProfile.0.last_name_th'); ?>" />
							
						</div>        
                    </div>
                    
		</div>

	</div>
</div>

<span id="addGroup"></span>
<input id="user_id" class="chkuser_id" type="hidden" value="<?php echo $currentUser['AuthUser']['id'];?>" />
<input id="displayname" class="displayname" type="hidden" value="<?php echo $display;?>" />
<input id="active_contact_id" type="hidden" value="<?php echo $group_id; ?>" />
<input id="last_message_time" type="hidden" value="<?php echo $last_message_time; ?>" />
<input id="is_fetch_data_completed" type="hidden" value="0" />

<link rel="stylesheet" href="lib/CLEditor/jquery.cleditor.css" />
<script type="text/javascript" src="js/custom/jquery.ocupload-1.1.2.js"></script>

<!-- CLEditor -->
<script src="lib/CLEditor/jquery.cleditor.js"></script>
<script src="lib/CLEditor/jquery.cleditor.icon.min.js"></script>

<!-- date library -->
<script src="lib/moment_js/moment.min.js"></script>

<!-- chat functions -->
<script src="js/gebo_chat.js"></script>

<style>
/*.upchat {
    position: absolute;
    display: block;
    visibility: hidden;
    width: 0;
    height: 0;
} */   
span#btn_upload_file form{
    position: absolute;
    display: block;
    visibility: hidden;
    width: 0;
    height: 0;
}
.modal {
    background-clip: padding-box;
    background-color: #FFFFFF;
    border: 8px solid rgba(0, 0, 0, 0.5);
    border-radius: 6px 6px 6px 6px;
    box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
    left: 50%;
    margin: -290px 0 0 -450px;
    overflow: auto;
    position: fixed;
    top: 50%;
    width: 600px;
    z-index: 1050;
}

.modal-body {
    min-height: 460px;
    max-height: 460px;
    overflow-y: auto;
    padding:9px 5px;
    
}
</style>

<script>
$(document).ready(function(){
	gebo_chat.init();
	var flgupload = false;
    var control_scroll = 'on';

	// Set Play and Pause Scroll
    $('#play_scroll').hide();
    $('#pause_scroll').show();
    $('#pause_scroll').click(function(){
        $('#play_scroll').show();
        $('#pause_scroll').hide();
        control_scroll = 'off';
    });
    $('#play_scroll').click(function(){
        $('#play_scroll').hide();
        $('#pause_scroll').show();
        control_scroll = 'on';
    });



    var group_id = $('#active_contact_id').val();
    var display_name = $('#displayname').val();
    var last_message_time = $('#last_message_time').val();

	$('.msg_window').html('<div class="chat_msg clearfix msg_clone" style="display:none"><div class="chat_msg_heading"><span class="chat_msg_date"></span><span class="chat_user_name"></span></div><div class="chat_msg_body"></div></div>');
	// Query Data
	$.ajax({
		type: "POST",
		url: "/Messages/getMessage/?_="+Math.random(),
		cache: false,
		data: {'group_id': group_id},
		success: function(data){
			if(data.length == 0){ return false; }
			try{
                if(data.length == 0){ return false; }
                
                var obj = jQuery.parseJSON(data);
            }catch(e){
                return false;
            }
			$('.msg_window').html('<div class="chat_msg clearfix msg_clone" style="display:none"><div class="chat_msg_heading"><span class="chat_msg_date"></span><span class="chat_user_name"></span></div><div class="chat_msg_body"></div></div>');
            var d = new Date();
            var m = d.getMonth() + 1;
            var date = d.getDate();

            if(m < 10){ m = '0' + m.toString();}
            if(date < 10){ date = '0' + date.toString(); }

            var today = d.getFullYear() + "-" + m + "-" + date;
            var showtime = '';
            var message = '';
			 $.each(obj, function(key, val){
                message = val.message.replace(/\\/gi, "");
                
                if(val.created_date.substr(0, 10) == today){
                    showtime = val.created_date.substr(11, 5);
                }else{
                    showtime = val.created_date.substr(0, 16);
                }
                
                //$('.msg_window').append('<div class="chat_msg clearfix"><div class="chat_msg_heading"><span class="chat_msg_date">' + showtime + '</span><span class="chat_user_name">' + val.user_display_name + '</span></div><div class="chat_msg_body">' + message + '</div></div>');
                
				var msg_cloned = $('.msg_clone').clone();
				$('.msg_window').append(msg_cloned);
				msg_cloned.find('.chat_msg_date').html(showtime);
				msg_cloned.find('.chat_msg_body').html(message);
				msg_cloned.find('.chat_user_name').html(val.user_display_name);
				msg_cloned.removeClass('msg_clone').show();
				$('.msg_window').stop().animate({
				scrollTop: msg_cloned.offset().top
            }, 2000);
 
                $('#last_message_time').val(val.created_date);
            });
				$('#btn_chat').show();
				$('#chat-control').css('display','block');
				$('#is_fetch_data_completed').val(1); 
		
		}
		
	});
	/***************************************/
		var g_id = $('#active_contact_id').val();
		var user_id = $('.chkuser_id').val();
		$('#btn_upload_file').upload({
			name: 'Filedata',
			method: 'post',
			enctype: 'multipart/form-data',
			action: '/Messages/uploadFile/' + g_id + '/' + user_id, 
			onSubmit: function() {
				alert('<?php echo __('Uploading File....'); ?>');
			},
			onComplete: function(data) {
				if(data == 0){
					alert('<?php echo __('Upload Completed'); ?>');
				}else{
					alert(data);
				}
			}
			
		});
	/***************** Time Interval **********************/
	var chatTime = setInterval(function(){
			var group_id = $('#active_contact_id').val();
			var last_message_time = $('#last_message_time').val();


			if(group_id == '' || $('#is_fetch_data_completed').val() == "0"){ return false; }
			//alert(group_id + ' <===> ' + last_message_time);

			$.post('/Messages/getMessage/', {group_id : group_id, last_message_time : last_message_time }, function(data){
				
				if(group_id != $('#active_contact_id').val()){ return false; }
				try{
					//if(data.length == null){ alert(data); }else{ alert('slsls'); return false; }
					if(data.length == 0){ return false; }

					var obj = jQuery.parseJSON(data);
				}catch(e){
					//alert(e);
					return false;
				}


				var d = new Date();
				var m = d.getMonth() + 1;
				var date = d.getDate();

				if(m < 10){ m = '0' + m.toString();}
				if(date < 10){ date = '0' + date.toString(); }

				var today = d.getFullYear() + "-" + m + "-" + date;
				var showtime = '';
				var message = '';

				$.each(obj, function(key, val){
					message = val.message.replace(/\\/gi, "");

					if(val.created_date.substr(0, 10) == today){
						showtime = val.created_date.substr(11, 5);
					}else{
						showtime = val.created_date.substr(0, 16);
					}

					//$('.msg_window:last').append('<div class="chat_msg clearfix"><div class="chat_msg_heading"><span class="chat_msg_date">' + showtime + '</span><span class="chat_user_name">' + val.user_display_name + '</span></div><div class="chat_msg_body">' + message + '</div></div>');
				var msg_cloned = $('.msg_clone').clone();
				$('.msg_window').append(msg_cloned);
				msg_cloned.find('.chat_msg_date').html(showtime);
				msg_cloned.find('.chat_msg_body').html(message);
				msg_cloned.find('.chat_user_name').html(val.user_display_name);
				msg_cloned.removeClass('msg_clone').show();
				if(control_scroll =='on'){
				  $('.msg_window').stop().animate({
					  scrollTop: $('.msg_window')[0].scrollHeight
				  }, 2000);
				}  
					$('#last_message_time').val(val.created_date);
					
				});
			});


	//$('#txtarea').text('ssll');
	}, 10000);
    /******************************************************/
	$('#closechatbox,#customModalAction').click(function(event){
        clearInterval(chatTime);
        event.preventDefault();
        $('#uploadchat'+g_id).val('');
    });
});
 function modalAction(){
   
 }
</script>
<!--<link rel="stylesheet" href="css/custom/widget/chat.css" />-->
