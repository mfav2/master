<?php //pr($messageGroup); ?>
<li>
    <a href="javascript:void(0)"><?php 
                                    $my_img_key = $this->Session->read('AuthUser.AuthUserProfile.0.image_key'); 
                                    if(empty($my_img_key)){
                                        $my_img_key = 1;
                                    }
                                ?>
        <img src="<?php echo $this->FileStorageHelper->urlByKey($my_img_key);?>" style="width:30px; height: 30px" />
        <?php echo  $this->Session->read('AuthUser.AuthUserProfile.0.first_name_th') . ' ' . 
                    $this->Session->read('AuthUser.AuthUserProfile.0.last_name_th'); 
        ?>
                <span>(you)</span>
    </a>
</li>                                            
<?php
    if(!empty($messageGroup)){
            foreach($messageGroup AS $i => $group){
                $g = $group['MessageGroup'];

                if(!empty($g['image_key'])){
                    $img = '<img src="' . $this->FileStorageHelper->urlByKey($g['image_key']) . '" style="width:30px; height: 30px" />';
                }else{
                    $img = '<img src="img/user_avatar.png" alt="" class="user_avatar">';
                }
                foreach($CountMessage['Group'] as $group_id=>$Message){

                    if($g['id'] == $group_id){
                        echo "
                            <li id='UserContact{$g['id']}'>
                                <span class='pull-left'>
                                    <a href='#' class='contactlist' cid='{$g['id']}'>
                                        {$img}
                                        {$g['group_name']}
                                    </a>
                                </span>
                             ";
                        echo "<span id='showcountmsg' class='pull-right label label-important' style='margin:15px 5px 0 0;'>
                                ".$Message."  
                             </span>";  
                        echo "<span class='pull-right' style='margin:15px 5px 0 0; cursor:pointer;' onclick='deleteGroup({$g['id']})'><i class='splashy-group_blue_remove'></i></span>";    
                        echo "</li>";
                    }
                }

            }
    }
?>
<script src="js/gebo_chat.js"></script>
<script>
   // var flgupload = false;
    var control_scroll = 'on';
    
    $('#play_scroll').hide();
    
    
    $('#pause_scroll').click(function(){
        $('#play_scroll').show();
        $('#pause_scroll').hide();
        control_scroll = 'off';
    });
    $('#play_scroll').click(function(){
        $('#play_scroll').hide();
        $('#pause_scroll').show();
        control_scroll = 'on';
    });    
    /*$(".contactlist").click(function(){
        var group_id = $(this).attr('cid');
        
        if($('#active_contact_id').val() == group_id){ return false; }
        $('#is_fetch_data_completed').val(0); 
        $('#active_contact_id').val(group_id);
        $('#contactname').text($(this).text());
        $('.msg_window').html('<div class="chat_msg clearfix msg_clone" style="display:none"><div class="chat_msg_heading"><span class="chat_msg_date"></span><span class="chat_user_name"></span></div><div class="chat_msg_body"></div></div>');
        
        $.post('/Messages/getMessage/', {group_id : group_id}, function(data){
            //$('#txtarea').text(data);
            var obj = jQuery.parseJSON(data);
            
            $('.msg_window').html('<div class="chat_msg clearfix msg_clone" style="display:none"><div class="chat_msg_heading"><span class="chat_msg_date"></span><span class="chat_user_name"></span></div><div class="chat_msg_body"></div></div>');
            
            var d = new Date();
            var m = d.getMonth() + 1;
            var date = d.getDate();

            if(m < 10){ m = '0' + m.toString();}
            if(date < 10){ date = '0' + date.toString(); }

            var today = d.getFullYear() + "-" + m + "-" + date;
            var showtime = '';
            var message = '';

            $.each(obj, function(key, val){
                message = val.message.replace(/\\/gi, "");
                
                if(val.created_date.substr(0, 10) == today){
                    showtime = val.created_date.substr(11, 5);
                }else{
                    showtime = val.created_date.substr(0, 16);
                }
                
                $('.msg_window').append('<div class="chat_msg clearfix"><div class="chat_msg_heading"><span class="chat_msg_date">' + showtime + '</span><span class="chat_user_name">' + val.user_display_name + '</span></div><div class="chat_msg_body">' + message + '</div></div>');
                
                $('#last_message_time').val(val.created_date);
            });
            
            $('#is_fetch_data_completed').val(1);
        });
        
    });*/
    $(".contactlist").click(function(){
        var group_id = $(this).attr('cid');
        $('#uploadupclick').remove();
        $('#showcountmsg').val(0);
            
        if($('#active_contact_id').val() == group_id){ return false; }
        $('#is_fetch_data_completed').val(0); 
        $('#active_contact_id').val(group_id);
        $('#contactname').text($(this).text());
        
        
        $('.msg_window').html('<div class="chat_msg clearfix msg_clone" style="display:none"><div class="chat_msg_heading"><span class="chat_msg_date"></span><span class="chat_user_name"></span></div><div class="chat_msg_body"></div></div>');
           
        $.post('/Messages/getMessage/', {group_id : group_id}, function(data){
            //$('#txtarea').text(data);
            if(data.length == 0){ return false; }
            try{
                if(data.length == 0){ return false; }
                
                var obj = jQuery.parseJSON(data);
            }catch(e){
                return false;
            }
            
            $('.msg_window').html('<div class="chat_msg clearfix msg_clone" style="display:none"><div class="chat_msg_heading"><span class="chat_msg_date"></span><span class="chat_user_name"></span></div><div class="chat_msg_body"></div></div>');
            
            var d = new Date();
            var m = d.getMonth() + 1;
            var date = d.getDate();

            if(m < 10){ m = '0' + m.toString();}
            if(date < 10){ date = '0' + date.toString(); }

            var today = d.getFullYear() + "-" + m + "-" + date;
            var showtime = '';
            var message = '';

            $.each(obj, function(key, val){
                message = val.message.replace(/\\/gi, "");
                
                if(val.created_date.substr(0, 10) == today){
                    showtime = val.created_date.substr(11, 5);
                }else{
                    showtime = val.created_date.substr(0, 16);
                }
                
                //$('.msg_window').append('<div class="chat_msg clearfix"><div class="chat_msg_heading"><span class="chat_msg_date">' + showtime + '</span><span class="chat_user_name">' + val.user_display_name + '</span></div><div class="chat_msg_body">' + message + '</div></div>');
                
            var msg_cloned = $('.msg_clone').clone();
            $('.msg_window').append(msg_cloned);
            msg_cloned.find('.chat_msg_date').html(showtime);
            msg_cloned.find('.chat_msg_body').html(message);
            msg_cloned.find('.chat_user_name').html(val.user_display_name);
            msg_cloned.removeClass('msg_clone').show();
            $('.msg_window').stop().animate({
            scrollTop: msg_cloned.offset().top
            }, 2000);
 
                $('#last_message_time').val(val.created_date);
            });
            $('#btn_chat').show();
            $('#chat-control').css('display','block');
            $('#is_fetch_data_completed').val(1);
        });
            
            var uploader = document.getElementById('btn_upload_file');
            var g_id = $('#active_contact_id').val();
            
            var user_id = $('.chkuser_id').val();
            upclick({
                element: uploader,
                action: '/Messages/uploadFile/' + g_id + '/' + user_id, 
                onstart:function(filename){
                  alert('<?php echo __('Start upload'); ?> : ' + filename);
                   // alert(g_id+' '+user_id);
                    //flgupload = true;
                },
                oncomplete:function(response){
                    
                    alert('<?php echo __('Upload complete'); ?>');
                    $('#txtarea').text(response);
                    //flgupload = false;
                }
            });
         
        
    });   
function deleteGroup(group_id){
     if(confirm('<?php echo __('Confirm to delete'); ?> ?')==true){
        $.ajax({
            type: "POST",
            url: "Messages/deletedGroup",
            cache: false,
            data: {'group_id': group_id},
            success: function(data){
                if(data == 1){
                    $('#UserContact'+group_id).remove();
                }else{
                    alert(data);
                }
            }
        });  
    }
}       
</script>
