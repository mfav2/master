<?php if(empty($_POST)){ ?>
<div style="margin: 10px; margin-top:0px">
    <br />
    <table>
        <tr>
            <td><?php echo __('Add Type'); ?></td>
            <td>
                <select name="contact_type" id="contact_type">
                    <option value="1"><?php echo __('Person');?></option>
                    <option value="2"><?php echo __('Group');?></option>
		    <option value="3"><?php echo __('Add Person In Group');?></option>
                </select>
            </td>
        </tr>
        <tr id="tr_group_name" style="display: none">
            <td><?php echo __('Group Name'); ?></td>
            <td>
                <input id="add_group_name" value="" />
            </td>
        </tr>
	<tr id="tr_add_person_in_group" style="display: none">
		<td><?php echo __('Choose Group Name'); ?></td>
		<td><?php echo $this->Form->input('group_id', array('options' => $group, 'default' => '', 'label'=>false, 'div'=>false)); ?></td>
	</tr>
    </table>
</div>

<script type="text/javascript">
$(function(){
    $('#contact_type').change(function(){
        if($(this).val() == "1"){
            $('#tr_group_name').css('display', 'none');
			$('#tr_add_person_in_group').css('display', 'none');
        }else if($(this).val() == "3"){
			$('#tr_group_name').css('display', 'none');
			$('#tr_add_person_in_group').css('display', 'table-row');
	}else{
            $('#tr_group_name').css('display', 'table-row');
	    $('#tr_add_person_in_group').css('display', 'none');
        }
    })
})
</script>
<?php } ?>