<?php $currentUser = $this->Session->read('AuthUser');?>
<?php //pr($messageGroup); ?>
<!--<li>
    <a href="javascript:void(0)"><?php 
                                    $my_img_key = $this->Session->read('AuthUser.AuthUserProfile.0.image_key'); 
                                    if(empty($my_img_key)){
                                        $my_img_key = 1;
                                    }
                                ?>
        <img src="<?php echo $this->FileStorageHelper->urlByKey($my_img_key);?>" style="width:30px; height: 30px" />
        <?php echo  $this->Session->read('AuthUser.AuthUserProfile.0.first_name_th') . ' ' . 
                    $this->Session->read('AuthUser.AuthUserProfile.0.last_name_th'); 
        ?>
                <span>(you)</span>
    </a>
</li>                                            -->
<?php
    if(!empty($messageGroup)){
        foreach($messageGroup AS $i => $group){
            $g = $group['MessageGroup'];
			$groupuser = $group['MessageGroupUser'];
            if(!empty($g['image_key'])){
                $img = '<img src="' . $this->FileStorageHelper->urlByKey($g['image_key']) . '" style="width:30px; height: 30px" />';
            }else{
                $img = '<img src="img/user_avatar.png" alt="" class="user_avatar">';
            }
			if(!empty($CountMessage['Group'])){
				foreach($CountMessage['Group'] as $group_id=>$Message){
					
					 if($g['id'] == $group_id){
						
							echo " <li id='UserContact{$g['id']}' style='margin:0px; padding:0px;'>
									<span class='pull-left'>
									<a id='contactlist' group_id='{$g['id']}' class='contactlist' href='#myModal' data-backdrop='static' data-toggle='modal'>
									   {$img}
									   <span id='changegroup{$g['id']}'>
										{$g['group_name']}
									   </span>
									</a>
									</span>";
							echo "<span id='showcountmsg' class='pull-right label label-important' style='margin:15px 5px 0 0;'>
									".$Message."  
								  </span>";  
							//echo "<span class='pull-right' style='margin:15px 5px 0 0; cursor:pointer;' onclick='editGroup({$g['id']})'><i class='splashy-group_blue_edit'></i></span>";    
							
							echo "<span class='pull-right' style='margin:15px 5px 0 0; cursor:pointer;' onclick='deleteGroup({$g['id']})'><i class='splashy-group_blue_remove'></i></span>";
							if($g['contact_type'] == 2){
								foreach($groupuser as $msggroup){
									
									if($msggroup['deleted'] == 'N'){
										if($msggroup['is_admin'] == 'Y'){
											if($currentUser['AuthUser']['id'] == $msggroup['user_id']){	
											  echo "<span class='pull-right' style='margin:15px 5px 0 0; cursor:pointer;' onclick='editGroup({$g['id']})'><i class='splashy-group_blue_edit'></i></span>";
											}
										}else{
											if($currentUser['AuthUser']['id'] == $msggroup['user_id']){	
											 echo "<span class='pull-right' style='margin:15px 5px 0 0; cursor:pointer;' onclick='viewGroup({$g['id']})'><i class='splashy-group_blue'></i></span>";
											}
										}

									}
								}
								/*foreach($groupuser as $msggroup){
									if($currentUser['AuthUser']['id'] == $msggroup['user_id'] && $msggroup['is_admin'] == 'Y'){
									  echo "<span class='pull-right' style='margin:15px 5px 0 0; cursor:pointer;' onclick='editGroup({$g['id']})'><i class='splashy-group_blue_edit'></i></span>"; 
									}else{
										echo "<span class='pull-right' style='margin:15px 5px 0 0; cursor:pointer;' onclick='editGroup({$g['id']})'><i class='splashy-group_blue'></i></span>";
									}
								}*/
							} 

							echo "</li>";

							
							
					 }
				} 
			}
               

        }
    }
?>
<!--<script src="js/custom/widget/mini_chat.js"></script>-->
<script>
$(document).ready(function(){
    $(".contactlist").click(function(){
       $('#customModal').empty();
        var group_id = $(this).attr('group_id');
        
        $('#customModalHeader').html('Chat Room');
        $('#customModalAction').html('Exit');
        $('#customModal').load("/Messages/chatBox/"+group_id+"?_="+Math.random(),function(data) {
            //$('#customModal').html(data);
            /*********************************/
            
            /*********************************/
        });

    });
});

function deleteGroup(group_id){
    if(confirm('<?php echo __('Confirm to delete'); ?> ?')==true){
        $.ajax({
            type: "POST",
            url: "Messages/deletedGroup",
            cache: false,
            data: {'group_id': group_id},
            success: function(data){
                if(data == 1){
                    $('#UserContact'+group_id).remove();
                }else{
                    alert(data);
                }
            }
        });  
    }
}

function editGroup(group_id){
		$('#myModal').modal('show');
		$('#customModal').empty();
        $('#customModalHeader').html('Edit Group Name');
        $('#customModalAction').html('Save');
        $('#customModal').load("/Messages/editgroup/"+group_id+"/edit_group_name?_="+Math.random(),function(data) {
            //$('#customModal').html(data);
            /*********************************/

        });
}

function viewGroup(group_id){
		$('#myModal').modal('show');
		$('#customModal').empty();
        $('#customModalHeader').html('View User In Group');
        $('#customModalAction').html('Close');
        $('#customModal').load("/Messages/viewgroup/"+group_id+"/edit_group_name?_="+Math.random(),function(data) {
            //$('#customModal').html(data);
            /*********************************/
        });
}
</script>