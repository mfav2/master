<div id="composeDiv">
    <div class="modal-header">
        <button class="close" data-dismiss="modal">x</button>
        <h3>Send Message</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="span4">
                <b>Selected Users </b> Click at name to remove.<br><br>
                <div id="userlist">
                    <a href="#"><i class="splashy-contact_blue_remove"></i></a> User1<br>
                    <a href="#"><i class="splashy-contact_blue_remove"></i></a> User2<br>
                    <a href="#"><i class="splashy-contact_blue_remove"></i></a> User3<br>
                    <a href="#"><i class="splashy-contact_blue_remove"></i></a> Super User 1<br>
                    <a href="#"><i class="splashy-contact_blue_remove"></i></a> Super User 2<br>
                    <a href="#"><i class="splashy-contact_blue_remove"></i></a> Admin 1<br>
                    <a href="#"><i class="splashy-contact_blue_remove"></i></a> Admin 2<br>
                    <a href="#"><i class="splashy-contact_blue_remove"></i></a> Super Admin<br>
                </div>
                <div>
                    <br>Click<a href="#"> Add more <i class="splashy-contact_blue_add"></i> </a> to add more.
                </div>
            </div>
            <div class="span8">
                <label>
                    Message
                    <span class="f_req">*</span>
                </label>
                <textarea id="your_message" class="span12" rows="15" cols="200" name="message"></textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row-fluid">
            <div class="span6" style="text-align: left">
                <button class="btn" type="submit" id="sendMsg">Send Message</button>
            </div>
            <div class="span6" sytle="text-align: right">
                <a class="btn" data-dismiss="modal" href="#">Close</a>
            </div>
        </div>
    </div>
</div>