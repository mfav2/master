<?php 
    $currentUser = $this->Session->read('AuthUser');
    $permiss =  $currentUser['PermissionGroup'];
    $permisssubscribe = $currentUser['PermissionSubscribe'];

    $showdelete = 'N';
    foreach($permiss as $permission){
        if($permission['PermissionGroup']['function_id'] == 9){
            if($permission['PermissionGroup']['is_delete'] == 'Y'){
                $showdelete = 'Y';
            }      
        }
    }
?>
	<!-- ***************************************** -->
		<?php if(!empty($contents)){ ?>
		 
				<?php foreach ($contents as $content) : ?>
                    <?php 
                    //pr($content);
                    //organization_description
                        $org_desc = '';
                        $org_belong = '';
                        $showdiv = '';
                        if(!empty($content['ShowListContent']['organization_description'])){
                            $org_desc  = $content['ShowListContent']['organization_description']['description'];
                        }else{
                           if(!empty($content['ShowListContent']['old_organization_description'])){
                            $org_desc  = $content['ShowListContent']['old_organization_description']['description'];
                           } 
                        }
                        if(!empty($content['ShowListContent']['organization_belong'])){
                            $org_belong = '&nbsp;-&nbsp;'.$content['ShowListContent']['organization_belong'];
                        }

                        if(!empty($org_desc) && !empty($org_belong)){
                            $showdiv = '['.$org_desc.$org_belong.']';
                        }
                    ?>
            <div id="showmorecontent">
                    <div class="prettyprint" style="background-color: white; border-collapse: collapse; border-width: 1px; border-top-width: 0px;" >
                    <?php echo $this->Time->format('d M y', $content['ShowListContent']['event_date']).'&nbsp;&nbsp;&nbsp;'; ?>
                    <a class="pop_over" data-placement="bottom" href="<?= $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $content['ShowListContent']['id']); ?>">
                        <?php 
                            echo empty($content['ShowListContent']['title']) ? __('No title') : mb_substr(strip_tags(htmlspecialchars_decode($content['ShowListContent']['title'])), 0, 256, 'UTF-8');
                        ?>
                    </a>
                     <?php 
                        if(!empty($content['ShowListContent']['attachment1']) || !empty($content['ShowListContent']['attachment2']) || !empty($content['ShowListContent']['attachment3']) ){
                            echo ' - ';
                        }
                        if(!empty($content['ShowListContent']['attachment1'])){
                            echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['ShowListContent']['attachment1']) . '"><i class="icon-adt_atach"></i></a>';
                        }
                        if(!empty($content['ShowListContent']['attachment2'])){
                            echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['ShowListContent']['attachment2']) . '"><i class="icon-adt_atach"></i></a>';
                        }
                        if(!empty($content['ShowListContent']['attachment3'])){
                            echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['ShowListContent']['attachment3']) . '"><i class="icon-adt_atach"></i></a>';
                        }
                       
                    ?>

                      <?php if($showdelete == 'Y'){ ?>
                       <span class="pull-right">
                          
                               <a href="<?= $this->Portal->makeUrl($this->name, 'delete', 'content_id=' . $content['ShowListContent']['id'] ); ?>" onclick="return confirm('<?=__('Confirm to delete')?> ?')"><i class='icon-trash'></i></a>


                                   </span>
                      <?php } ?> 

                    
                   <span class="label label-info"><?= $content['ShowListContent']['count_view'] ?> <?php echo __('Views');?></span>

                </div>    
            </div>					


				<?php endforeach; ?> 
		<?php }?>
