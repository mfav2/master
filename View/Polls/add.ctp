<?php  echo $this->element('Components/breadcrumb'); ?>

<link rel="stylesheet" href="lib/tag_handler/css/jquery.taghandler.css" />

<?php
    $question_type = array(1=>__('One Choice'), 2=> __('Multiple Choices'));
?>
<?php
echo '<form id="AddPoll" enctype="multipart/form-data" method="post" action="/Polls/add" class="form-horizontal well">';

?>
    <p class="f_legend"><?php echo __("Add New Poll "); ?></p>
    <div>
        <label><?php echo __("Subject");?> <span style="color:#F00;">*</span></label>
        <?php echo $this->Form->input('title', array('label'=>false, 'class'=>'span10', 'placeholder'=>__('Subject'))); ?>
        <span id="err_title"></span>
        <br />
        
        <label><?php echo __("Detail");?> <span style="color:#F00;">*</span></label>
        <?php echo $this->Form->input('description', array('type'=>'textarea', 'label'=>false, 'class'=>'span10 wysiwg_mini', 'placeholder'=>__('Detail'))); ?>
        <span id="err_desc"></span>
        <br />
        
		<label><?php echo __('Expiry Date'); ?></label>
			<div class="input-append date" id="dp2" data-date-format="dd/mm/yyyy">
				<input id="ContentExpiryDate" name='data[expiry_date]' style="width:auto;" type="text" readonly="readonly" /><span class="add-on"><i class="splashy-calendar_day"></i></span>
			</div>	
		<div style="clear:both;"></div>
		<br />

        <label><?php echo __("Attachment");?></label>
        <?php echo $this->Form->input('attachment', array('type'=>'file', 'class'=>'span10', 'label'=>false)); ?><br />
        <span id="help-block"><?=__('Max File Size')?> <?=$size?></span><br />
        <span id="err_attach"></span>
        <hr />
        <div class="input-line">            
            <div class="post-que-title-input">
				<input name="data[Poll][0][Question]" class="remark-txt" placeholder="<?php echo __('Question'); ?>" type="text">
<!--				<a href="javascript:void(0);" class="remove-que" onclick="removeQuestion($(this))"><?php echo __('Delete');?></a>-->
			</div><br />
			<div class="input-line small-txt"><?php echo __("Question Type");?>
                <select name="data[Poll][0][Type]" onchange="changeOptionType($(this))">
                    <option value="1"><?php echo __("One Choice");?></option>
                    <option value="2"><?php echo __("Multiple Choices");?></option>
                </select>
                
			</div><br />
            
            <ul class="post-que-option-list" style="list-style: none;">
                <li class="one-choice">
<!--                    <input class="radio" name="item_group:1" type="radio">-->
                    <input class="text remark-txt" placeholder="<?php echo __("Choice"); ?>" name="data[Poll][0][Option][0]" type="text">
                    <span class="txt_error"></span>
                    <span class="icon-remove" title="<?php echo __("Delete"); ?>" onclick="removeOption($(this))"></span>
                </li>
                <li class="one-choice">
<!--                    <input class="radio" name="item_group:1" type="radio">-->
                    <input class="text remark-txt" placeholder="<?php echo __("Choice"); ?>" name="data[Poll][0][Option][1]" type="text">
                    <span class="txt_error"></span>
                    <span class="icon-remove" title="<?php echo __("Delete"); ?>" onclick="removeOption($(this))"></span>
                </li>
                
                <li class="add-option">
                    <a href="javascript:void(0);" class="add-choice" offset="0" next="2" onclick="addOption($(this))">+<?php echo __("Add More Choices"); ?></a>
                </li>
            </ul>
        </div><br /><br />
        
        
        
        <div style="margin:5px px;">
            <a href="javascript:void(0);" offset="1" next="1" class="post-add-question"><?php echo __("Add More Questions"); ?></a>
        </div>

        <br /><br /><span id="error_questionpoll"></span><br /><br />

        <div>
            <?php  
              echo $this->Form->checkbox('no_show_name', array('hiddenField' => false ,
                'style' => 'margin:0px 0px 0px 10px;'));
           ?>
           &nbsp;<span><?php echo __('Show Voter'); ?></span>
            <div style="clear:both;"></div>
            <br />
        </div> 

		<div style="margin:5px 0px;">
			<label><?php echo __('Readers'); ?></label>
			<div>
				<div class="form-inline">
					<input id="ContentNotificationTypeA" type="radio" checked="checked" value="A" style="margin: 10px 10px;" name="data[notification_type]">
					<label for="ContentNotificationTypeA">Everyone</label>
					<input id="ContentNotificationTypeD" type="radio" value="D" style="margin: 10px 10px;" name="data[notification_type]">
					<label for="ContentNotificationTypeD">Department</label>
					<input id="ContentNotificationTypeC" type="radio" value="C" style="margin: 10px 10px;" name="data[notification_type]">
					<label for="ContentNotificationTypeC">Custom</label>
				</div>
					<ul id="array_content_users" class="span8" style="background: #FFF; margin: 0px 0px 0px 0px">
					</ul>
				
			</div>
			<br /><br /><span class="error-choose-user"></span><br /><br />
		</div>
        
        


        <?php echo $this->Form->button(__('Submit'), array('type'=>'button','id'=>'submitFrom','class'=>'btn')); ?>
    </div>
	

<?php
echo "</form>";
?>

<?php
    if(!empty($this->data)){ pr($this->data); }
?>


<!-- autosize textareas -->
<script src="js/forms/jquery.autosize.min.js"></script>
<!-- TinyMce WYSIWG editor -->
<script src="lib/tiny_mce/jquery.tinymce.js"></script>
<!-- tag handler (recipients) -->
<script src="lib/tag_handler/jquery.taghandler.min.js"></script>
<script src="lib/jstree/jquery.jstree.js"></script>
<script src="js/custom/custom_content_forms.js"> </script>

<script type="text/javascript">
function changeOptionType(obj){
    //alert(obj.val());
    //obj.closest("ul.post-que-option-list").append('lslslsls');
}
    
function addOption(obj){
    var n = parseInt($(obj).attr("next"));
    var offset = parseInt($(obj).attr("offset"));
    var str = '';

    str += '<li class="one-choice">';
//    str += '	<input class="radio" name="item_group:1" type="radio">';
    str += '	<input class="text remark-txt" placeholder="<?php echo __("Choice"); ?>" name="data[Poll][' + offset + '][Option][' + n + ']" type="text">';
    str += '    <span class="txt_error"></span>';
    str += '    <span class="icon-remove" title="<?php echo __("Delete"); ?>" onclick="removeOption($(this))"></span>';
    str += '</li>';

    $(obj).parent().before(str);

    $(obj).attr("next", n + 1);
}


function removeOption(obj){
    $(obj).closest("li").remove();
}

function removeQuestion(obj){
    $(obj).closest("div.input-line").remove();
}





$(document).ready(function(){

        /************************* Check Comment ********************************/
        if($('#no_show_name').attr("checked")=="checked"){ 
            $("#no_show_name").val('1');
            $("#no_show_name").attr("checked","checked");
        }else{ 
            $("#no_show_name").val('0');
            $("#no_show_name").removeAttr("checked","checked");
        }
        
        $("#no_show_name").click(function(){
              if($(this).attr("checked")=="checked"){ 
                  $("#no_show_name").val('1');
                  $("#no_show_name").attr("checked","checked");
              }else{ 
                 $("#no_show_name").val('0');
                 $("#no_show_name").removeAttr("checked","checked");
              }
        }); 


	if($('#ContentNotificationTypeA').attr("checked")=="checked"){ 
           $('#array_content_users').hide(); 
        }
        
        $('#array_content_users').tagHandler();
        $("ul#array_content_users li.tagInput").hide();

			$('#ContentNotificationTypeA').click(function() {
				 $('#array_content_users').hide(); 
				 //$('#array_content_users').attr("disabled", true);     
            });

			$('#ContentNotificationTypeD').click(function() {
				 $('#array_content_users').show(); 
				 //$('#array_content_users').attr("disabled", true);     
            });

			$('#ContentNotificationTypeC').click(function() {
				 $('#array_content_users').show(); 
				 //$('#array_content_users').attr("disabled", true);     
            });
			

    var exdate = '<?php echo date('d/m/Y', strtotime('+10 days')); ?>';
	$('#ContentExpiryDate').val(exdate);  

    /*$("form").submit(function() {        
        $.each($('input[name*="data"]'), function(){
            if($(this).val() == ''){
                $(this).next("span.txt_error").html('*');
                $(this).focus();
                
                return false;
            }
        });
        
        
    });*/
    
	$('#dp2').datepicker({format: "dd/mm/yyyy"}).on('changeDate', function(ev){
       $('#dp2').datepicker('hide');
    });



    $("a.post-add-question").click(function(){
        var n = parseInt($(this).attr("next"));
        var str = '';
        
        str += '<div class="input-line">            ';
        str += '    <div class="post-que-title-input">';
		str += '		<input name="data[Poll][' + n + '][Question]" class="remark-txt" placeholder="<?php echo __('Question');?>" type="text">';
		str += '		<a href="javascript:void(0);" class="remove-que" onclick="removeQuestion($(this))"><?php echo __('Delete');?></a>';
		str += '	</div><br />';
		str += '	<div class="input-line small-txt"><?php echo __("Question Type");?>';
        str += '        <select name="data[Poll][' + n + '][Type]">';
        str += '            <option value="1"><?php echo __("One Choice");?></option>';
        str += '            <option value="2"><?php echo __("Multiple Choices");?></option>';
        str += '        </select>';
                
		str += '	</div><br />';
            
        str += '    <ul class="post-que-option-list" style="list-style: none;">';
        str += '        <li class="one-choice">';
//        str += '            <input class="radio" name="item_group:1" type="radio">';
        str += '            <input class="text remark-txt" placeholder="<?php echo __("Choice"); ?>" name="data[Poll][' + n + '][Option][0]" type="text">';
        str += '            <span class="txt_error"></span>';
        str += '            <span class="icon-remove" title="<?php echo __("Delete"); ?>" onclick="removeOption($(this))"></span>';
        str += '        </li>';
        str += '        <li class="one-choice">';
//        str += '            <input class="radio" name="item_group:1" type="radio">';
        str += '            <input class="text remark-txt" placeholder="<?php echo __("Choice"); ?>" name="data[Poll][' + n + '][Option][1]" type="text">';
        str += '            <span class="txt_error"></span>';
        str += '            <span class="icon-remove" title="<?php echo __("Delete"); ?>" onclick="removeOption($(this))"></span>';
        str += '        </li>';
                
        str += '        <li class="add-option">';
        str += '            <a href="javascript:void(0);" class="add-choice" offset="' + n + '" next="2" onclick="addOption($(this))">+<?php echo __("Add More Choices"); ?></a>';
        str += '        </li>';
        str += '    </ul>';
        str += '<br /><br /></div>';
        
        $(this).before(str);
        
        $(this).attr("next", n + 1);
    });
    
    $('#attachment').bind('change', function() {
        //$('#err_attach').css('display','none');
         err = checkfilesize(this.files[0].size);
         if(err == false){
             $('#err_attach').css({
                "color": "#f00",
                "margin": "0",
                "padding": "0"
            }).text('<?=__('File over limit size')?> (<?php echo $size; ?>)');
            $('#submitFrom').attr('disabled','disabled');
         }else{
            $('#err_attach').empty()
            $('#submitFrom').removeAttr('disabled','disabled');
         }
    });
    
    $('#submitFrom').click(function(){
        var data = $('#AddPoll').serialize();
        var title = $('#title').val();
        var desc = $('#description').val();
        var chk_err = false;
        var file = $('#attachment').val();
	    var noti_all = $('#ContentNotificationTypeA:checked').val();
        $('#error_attach').empty();
        $('#err_title').empty();
        $('#err_desc').empty();
        $('#error_questionpoll').empty();
	    $('.error-choose-user').empty();




        if(file != ''){
            var filetype = checktypefile(file);         
            if(filetype == 'N'){
                $('#error_attach').text('<?php echo __("Only file"); ?> <?php echo $extension; ?>');
                chk_err = true;
            }
        }
		if(noti_all != 'A'){
			
			var l = $('ul#array_content_users li.tagItem').length;
			if(l == 0 ){
				$('.error-choose-user').css({
					"color": "#f00",
					"margin": "0",
					"padding": "0"
				}).text(' <?php echo __('Please Insert Reader'); ?>');
				chk_err = true;
			}else{
				var i =0;
				$('ul#array_content_users li.tagItem').each(function() {
					var j = i++;
					var type = $(this).attr('itemtype');
					if(type=="orgs"){
                                            //$('ul#array_content_users').after('<input type="hidden" id="reader" name="data[Invite][Orgs][' + j + ']" value="' + $(this).attr('org_id') + '" />');    
                                            $('ul#array_content_users').after('<input type="hidden" id="reader" name="data[Invite][Orgs][' + j + '][id]" value="' + $(this).attr('org_id') + '" /><input type="hidden" id="reader" name="data[Invite][Orgs][' + j + '][organization_name]" value="' + $(this).attr('org_name') + '" />');  
					}else if(type=="user"){
                                            //$('ul#array_content_users').after('<input type="hidden" id="reader" name="data[Invite][Users][' + j + ']" value="' + $(this).attr('user_id') + '" />');
                                            $('ul#array_content_users').after('<input type="hidden" id="reader" name="data[Invite][Users][' + j + '][id]" value="' + $(this).attr('user_id') + '" /><input type="hidden" id="reader" name="data[Invite][Users][' + j + '][name]" value="' + $(this).attr('user_name') + '" />');
					}

				});
				
			}
		}
		

        if(title == ''){
            $('#err_title').css({
                "color": "#f00",
                "margin": "0",
                "padding": "0"
            }).text(' <?php echo __('Please Insert Title'); ?>.'); 
                chk_err = true;
        }
        if(desc == ''){
            $('#err_desc').css({
                "color": "#f00",
                "margin": "0",
                "padding": "0"
            }).text(' <?php echo __('Please Insert Detail'); ?>.');
            chk_err = true;
        }
        if(chk_err == false){
                $.ajax({
                        cache: false, 
                        type:'POST',
                        data: data,
                        url: 'Polls/checkandinsert',
                        success:function(data){
                            if(data == 0){
                               $('#AddPoll').submit();
                            }else{
                                $('#error_questionpoll').css({
                                    "color": "#f00",
                                    "margin": "0",
                                    "padding": "0",
                                    "font-size": "16px"
                                }).text(' <?=__('Please Insert Question and Answer')?>.');
                                
                            }
                        }
                });        
        }
//        
//        if(chk == true){
//            return false;
//        }else{
//            this.form.submit();
//        }
    });
    
 });
 function checkfilesize(filesize){
    var chk ='';
    var maxsize = '<?php echo Configure::read('Config.MaxFileSize'); ?>';
                    if(filesize > maxsize){
                        chk = false;
                    }else{
                        chk = true;
                    }
                    
    return  chk;                
}

 
 function checktypefile(file){
            var exts = ['doc', 'docx', 'txt', 'pdf', 'jpg', 'jpeg', 'png', 'gif', 
                        'xls', 'xlsx', 'ppt', 'pptx', 'bmp', 'zip', 'swf'];
            if (file) {
                 // split file name at dot
                 var get_ext = file.split('.');
                 // reverse name to check extension
                 get_ext = get_ext.reverse();
                 var chktype = $.inArray ( get_ext[0].toLowerCase(), exts ) > -1;
                 // check file type is valid as given in 'exts' array
                 if (chktype){
                   return 'Y';
                 } else {
                   return 'N';
                 }
             }   
             
}    
 
</script>
