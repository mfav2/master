<?php  
    echo $this->element('Components/breadcrumb'); 
    $currentUser = $this->Session->Read('AuthUser');
    $permiss =  $currentUser['PermissionGroup'];
    $permisssubscribe = $currentUser['PermissionSubscribe'];

    $showdelete = 'N';
    foreach($permiss as $permission){
        if($permission['PermissionGroup']['function_id'] == 9){
            if($permission['PermissionGroup']['is_delete'] == 'Y'){
                $showdelete = 'Y';
            }      
        }
    }
?>
<form id="resultpoll" method="post" action="/Polls/voteResult">
<div class="doc_view">
    <div class="doc_view_header">
        <dl class="dl-horizontal" style="padding:10px; ">
            <?php if($showdelete == 'Y') : ?>
                <span class="pull-right">
                    <a href="<?= $this->Portal->makeUrl($this->name, 'delete', 'content_id=' . $content['Content']['id']); ?>" onclick="return confirm('<?php echo __('Confirm to delete');?> ?')">
                        <img alt="" src="img/gCons/recycle-full.png">
                    </a>
                </span>
            <?php endif; ?>
            <!-- ************************ Title Name ***************************** -->
            <?php echo htmlspecialchars_decode($content['Content']['title']); ?>
        </dl>
    </div>
    <div class="doc_view_content">
        <?php echo htmlspecialchars_decode($content['Content']['description']); ?><br/>
        <h5><br />(<?php echo __('Expire on'); ?> : <?= $this->Time->format('d/m/Y', $content['Content']['expiry_date']); ?>)</h5>    
        <?php    
            if(!empty($content['Content']['attachment1'])){
                $path_parts = pathinfo($content['Content']['attachment1_name']);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
                $pic_ext = array("jpg", "gif", "png");
                if(in_array($path_parts['extension'], $pic_ext)){
                    echo "<div>";  
                    echo "<a target='_blank' href='".$this->FileStorageHelper->urlByKey($content['Content']['attachment1'])."'>";
                    echo "<img style='max-width:300px; border=0' src='/fileProviders/index/{$content['Content']['attachment1']}' />";
                    echo "</a>";
                    echo "</div><br />";                 
                }else{
                    if($path_parts['extension'] == 'doc' || $path_parts['extension'] == 'docx'){
                        $showicon = '<img src="img/custom/doc/word.png" width="26" height="26" />';
                    }else if($path_parts['extension'] == 'xls' || $path_parts['extension'] == 'xlsx'){
                        $showicon = '<img src="img/custom/doc/excel.png" width="26" height="26" />';
                    }else if($path_parts['extension'] == 'ppt' || $path_parts['extension'] == 'pptx'){
                        $showicon = '<img src="img/custom/doc/ppt.png" width="26" height="26" />';
                    }else if($path_parts['extension'] == 'pdf'){
                        $showicon = '<img src="img/custom/doc/pdf.png" width="26" height="26" />';
                    }else{
                        $showicon = '<i class="icon-adt_atach"></i>';
                    }
                    echo '&nbsp;&nbsp;<span ><a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment1']) . '">'.$showicon.' '.$content['Content']['attachment1_name'].'</a></span><br /><br />';
                }
                
            }
        ?>
        <br />
        <!-- Question -->
        <?php 
            if(!empty($content)){ echo "<input type='hidden' name='content_id' value='{$content['Content']['id']}' />"; }
            if(!empty($question)){ 
              
              $num = 0;
              echo '<div id="countallquestion">';              
                foreach($question AS $i => $q){

                    $question_type = $q['getQuestion']['question_type'];
            ?>

                    <div id="allquest" class="input-line">            
                        <div class="post-que-title-input">
                            <span><?php echo $q['getQuestion']['question_name']; ?></span>
                            <input id="question_type<?php echo $i;?>" type="hidden" name="data[Question][<?php echo $q['getQuestion']['id'];?>][question_type]" value="<?php echo $q['getQuestion']['question_type']; ?>" />
                        </div><br />

                        <?php 
                            if(!empty($question[$i]['QuestionAnswer'])){ 
                               if($question_type == 1){ 
                                    echo '<ul id="listRadio'.$question_type.'" class="post-que-option-list" style="list-style:none;">';
                               }else{
                                    echo '<ul datacheckbox="'.$q['getQuestion']['id'].'" class="listcheck'.$q['getQuestion']['id'].'" id="listCheckbox'.$question_type.'" class="post-que-option-list" style="list-style:none;">';
                               } 
                                foreach($question[$i]['QuestionAnswer'] AS $opt){
                                    if($question_type == 1){
                        ?>
                                        <li>
                                            <input class="radio" name="data[Option][<?php echo $q['getQuestion']['id'];?>]" type="radio" id="validateRadio" value="<?php echo $opt['id']; ?>">
                                            <?php echo $opt['answer_name']; ?>
                                        </li>
                        <?php
                                    }else if($question_type == 2){
                        ?>
                                        <li>
                                            <input class="checkbox" name="data[Option][<?php echo $q['getQuestion']['id'];?>][Answer][]" type="checkbox" id="validateCheckbox<?php ++$num;?>" value="<?php echo $opt['id']; ?>">
                                            <?php echo $opt['answer_name']; ?>
                                        </li>
                        <?php 
                                    }
                                }
                                echo '</ul>';
                            } 
                        ?>
                    </div>

                    <br />
            <?php 
                }
                echo '</div>';  
            } 
        ?>
       <span id="validcheckboxandraadio"></span>                
    </div>
    <div class="doc_view_footer">
<?php
    if($alreadyVote == 0)
        echo $this->Form->submit(__('Vote'), array('id'=>'votePoll','class'=>'btn', 'div'=>false));
    else
        echo "<a href='{$this->Portal->makeUrl($this->name, 'voteResult', 'content_id=' . $this->request->query['content_id'])}' class='btn'>".__('View Result')."</a>";
?>                
    </div>
</div>
</form>
<!-- *********************************************************************** --> 

    
<!--<script src="js/custom/event/validateans.js"></script>-->
<script>

		$("#votePoll").click(function(e){
                    e.preventDefault();
                    var check = false;
                    var allquest = $('div#allquest').length;
                    var listradio = $('ul#listRadio1').length;
                    var listchkbox = $('ul#listCheckbox2').length;
                    var data = $('#resultpoll').serialize();
                    $('#validcheckboxandraadio').empty();
                    $.ajax({
                            cache: false, 
                            type:'POST',
                            data: data,
                            url: 'Polls/validcheckbox',
                            success:function(data){
                               
                                if(data == 0){
                                    $('#resultpoll').submit(); 
                                }else{
                                    $('#validcheckboxandraadio ').css({
                                     "color": "#f00",
                                     "margin": "0",
                                     "padding": "0",
                                     "font-size": "16px"
                                    }).text('<?php echo __('Please Select Answer'); ?>.'); 
                                    return false;
                                }
   
                            }
                    });
         
		
		});
       
</script>