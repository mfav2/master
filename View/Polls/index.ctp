<?php 
    echo $this->element('Components/breadcrumb');
    $currentUser = $this->Session->read('AuthUser');  	
    $permiss =  $currentUser['PermissionGroup'];
    $permisssubscribe = $currentUser['PermissionSubscribe'];
    
    $showadd = 'N';
    $showdelete = 'N';
    foreach($permiss as $permission){
        if($permission['PermissionGroup']['function_id'] == 9){
            if($permission['PermissionGroup']['is_add'] == 'Y'){
                $showadd = 'Y'; 
            }
            if($permission['PermissionGroup']['is_delete'] == 'Y'){
                $showdelete = 'Y';
            }      
        }
    }
    
?>
<div class="row-fluid">
        <div class="span3">
		<?php
                    if($showadd == 'Y'){
			echo "<a href='{$this->Portal->makeUrl($this->name, 'add')}' class='btn'>".__('New poll')."</a>";
                    }    
                ?>
	</div>
	<div class="span9" style="text-align: right;">
            <form id="searchContent" action="<?php echo $this->Portal->makeUrl('Polls', 'search'); ?>" class="form-inline" method="post">
                <?php
                                    echo ' ';
                                    echo $this->Form->input('keyword', array('placeholder' => __('keyword'), 'label' => false, 'div'=>FALSE));
                                    echo ' ';
                                    echo $this->form->button(__('Search'), array('type'=>'button','id'=>'submitSearch','class' => 'btn', 'div'=>FALSE));
                ?>
            <span class="btn"><a style="color:#000; text-decoration: none;" href="<?php echo $this->Portal->makeUrl($this->name, 'viewall'); ?>"><?php echo __('View All'); ?></a></span>        
            </form>
        </div>
        
</div>
<hr style='border:0.5px solid #058dc7; margin:0 0 10px 0;'/>
<div id="searchcontent">
    <div class="prettyprint pop_over" style="background-color: white; border-collapse: collapse; border-width: 0px; border-bottom-width: 1px;" ></div>
    <!-- **************** ดึงข้อมูลมาแสดง ************************ --> 
        <?php if(!empty($contents)){ 
              foreach ($contents as $content) : 
        ?>
        <div id="showmorecontent">
            <div class="prettyprint" style="background-color: white; border-collapse: collapse; border-width: 1px; border-top-width: 0px;" >
                <?php echo $this->Time->format('d M y', $content['Content']['event_date']).'&nbsp;&nbsp;&nbsp;'; ?>
                <a class="pop_over" data-placement="bottom" href="<?php echo $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $content['Content']['id'] ); ?>">
                    <?php 
                       echo empty($content['Content']['title']) ? __('No title') : mb_substr(strip_tags(htmlspecialchars_decode($content['Content']['title'])), 0, 256, 'UTF-8');
                    ?>
                </a>
                <?php 
                    if(!empty($content['Content']['attachment1']) || !empty($content['Content']['attachment2']) || !empty($content['Content']['attachment3']) ){
                        echo ' - ';
                    }
                    if(!empty($content['Content']['attachment1'])){
                        echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment1']) . '"><i class="icon-adt_atach"></i></a>';
                    }
                    if(!empty($content['Content']['attachment2'])){
                        echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment2']) . '"><i class="icon-adt_atach"></i></a>';
                    }
                    if(!empty($content['Content']['attachment3'])){
                        echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment3']) . '"><i class="icon-adt_atach"></i></a>';
                    }

                ?>
                 <?php if($showdelete == 'Y' || $content['Content']['user_id'] == $currentUser['AuthUser']['id']){ ?>
                <span class="pull-right">
                    <a href="<?= $this->Portal->makeUrl($this->name, 'delete', 'content_id=' . $content['Content']['id'] ); ?>" onclick="return confirm('<?=__('Confirm to delete')?> ?')"><i class='icon-trash'></i></a>  
                </span>
                <?php } ?>      
                <span class="label label-info"><?php echo $content['Content']['count_vote']; ?> <?php echo __('Votes');?></span>
                
            </div>
        </div>
        <?php endforeach; ?>   
        <?php } ?>
        <div id="viewmore_result"></div><br />
        
        
        <input id="offset" type="hidden" value="<?php echo @$offset; ?>"/>
        <input id="limit" type="hidden" value="<?php echo @$limit; ?>"/>
        <input id="countcontent" type="hidden" value="<?php echo @$countContent; ?>" />
        <input id="category_id" type="hidden" value="16" />
        <input id="search" type="hidden" value="<?php echo @$this->request->query['keyword']; ?>" />
        <input id="controller" type="hidden" value="<?php echo $this->name; ?>" />
        <input id="has_expire" type="hidden" value="<?php echo $has_expire; ?>" />
        <input id="page" type="hidden" value="1"/>
        <div>
           <span class="pull-right"><?php //echo $this->Paginator->numbers(); ?></span>
           <div id="viewmore" class="btn" style="cursor: pointer; width: 97%"><?php echo __('View More...');?></div>
        </div>        
</div>

<script>
/*
    $('#reversion').click(function() {
        $('#box').load('/articles/reversion/');
    });

 */
$('#submitSearch').click(function(){
    var values = $('#searchContent').serialize();
    var controller = $('#controller').val();
    $('#searchcontent').empty().text('Loading...');
    //alert(values);
    $.ajax({
        cache: false,
        url: "/"+controller+"/search?_="+Math.random(),
        type: "POST",
        data : values,
        success:function(data){
             $('#searchcontent').empty().append(data);
            //$('#searchcontent').empty().append(data);
        }

    });
});

$('#keyword').keypress(function(e) {
    if (e.which == "13") { 
        $('#submitSearch').click();
        return false;
        //enter pressed 
    } 

});
$(function(){
    var offset = parseInt($('#offset').val());
    var totalContents = $('#countcontent').val();
    if(offset >= totalContents){ $('#viewmore').hide(); }
});
</script> 
