<?php 
    $currentUser = $this->Session->read('AuthUser');  
    $permiss =  $currentUser['PermissionGroup'];
    $permisssubscribe = $currentUser['PermissionSubscribe'];

    $showdelete = 'N';
    foreach($permiss as $permission){
        if($permission['PermissionGroup']['function_id'] == 9){
            if($permission['PermissionGroup']['is_delete'] == 'Y'){
                $showdelete = 'Y';
            }      
        }
    }	
?>
<div class="prettyprint pop_over" style="background-color: white; border-collapse: collapse; border-width: 0px; border-bottom-width: 1px;" ></div>
<?php if(!empty($contents)){ 
        foreach ($contents as $content) : 
            
    ?>
            <div id="showsearchmorecontent">
                <div class="prettyprint" style="background-color: white; border-collapse: collapse; border-width: 1px; border-top-width: 0px;" >
                    <?php echo $this->Time->format('d M y', $content['Content']['event_date']).'&nbsp;&nbsp;&nbsp;'; ?>
                    <a class="pop_over" data-placement="bottom" href="<?php echo $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $content['Content']['id'] ); ?>">
                        <?php 
                           echo empty($content['Content']['title']) ? __('No title') : mb_substr(strip_tags(htmlspecialchars_decode($content['Content']['title'])), 0, 256, 'UTF-8');
                        ?>
                    </a>
                    <?php 
                        if(!empty($content['Content']['attachment1']) || !empty($content['Content']['attachment2']) || !empty($content['Content']['attachment3']) ){
                            echo ' - ';
                        }
                        if(!empty($content['Content']['attachment1'])){
                            echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment1']) . '"><i class="icon-adt_atach"></i></a>';
                        }
                        if(!empty($content['Content']['attachment2'])){
                            echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment2']) . '"><i class="icon-adt_atach"></i></a>';
                        }
                        if(!empty($content['Content']['attachment3'])){
                            echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment3']) . '"><i class="icon-adt_atach"></i></a>';
                        }

                    ?>
                    <?php if($showdelete == 'Y'){ ?>
                    <span class="pull-right">
                        <a href="<?= $this->Portal->makeUrl($this->name, 'delete', 'content_id=' . $content['Content']['id'] ); ?>" onclick="return confirm('<?=__('Confirm to delete')?> ?')"><i class='icon-trash'></i></a>
                    </span>
                    <?php } ?> 
                    <span class="label label-info"><?php echo $content['Content']['count_vote']; ?> <?php echo __('Votes');?></span>

                </div>
            </div>            
        <?php endforeach; ?>
<?php } ?>
   <div id="viewsearchmore_result"></div><br />
   <input id="offset" type="hidden" value="<?php echo @$offset; ?>"/>
   <input id="limit" type="hidden" value="<?php echo @$limit; ?>"/>
   <input id="countcontent" type="hidden" value="<?php echo @$countContent; ?>" />
   <input id="category_id" type="hidden" value="<?php echo $category_id; ?>" />
   <input id="search" type="hidden" value="<?php echo $keyword; ?>" />
   <input id="controller" type="hidden" value="<?php echo $this->name; ?>" />
   <input id="has_expire" type="hidden" value="N" />
   <input id="contype" type="hidden" value="search"/>
   <input id="page" type="hidden" value="1"/>

<?php if($countContent != 0 || !empty($countContent)) { ?>
    <div>
        <span class="pull-right"><?php //echo $this->Paginator->numbers(); ?></span>
        <div id="viewsearchmore" class="btn" style="cursor: pointer; width: 97%">View More...</div>
    </div>

<?php } ?>
<script>

    
    $('#viewsearchmore').click(function(){
        var offset = parseInt($('#offset').val());
        var limit = parseInt($('#limit').val());
        var category_id = $('#category_id').val();
        var keyword = $('#search').val();
        var totalContents = $('#countcontent').val();
        var page = parseInt($('#page').val());
        var controller = $('#controller').val();

        $('#viewsearchmore').text('Loading...');
        //alert('offset = ' + offset + '; total = ' + totalContents);
        $.ajax({
            cache: false,
            url: '/'+controller+'/viewmoresearch?_='+Math.random(),
            type: 'POST',
            data: {'limit':limit,'countcontent':totalContents, 'offset' : offset, 'category_id' : category_id, 'keyword' : keyword, 'page': page },
            success:function(data){
                  //alert(data);
                  $('#viewsearchmore_result').append(data);
                  offset = offset + 10;
                  page = page +1;
                  $('#offset').val(offset);
                  $('#page').val(page);
                  $('#viewsearchmore').text('View More...');
                  if(offset >= totalContents){ $('#viewsearchmore').hide(); }     

            }
        });    
        
    });
    
    $(function(){
        var offset = parseInt($('#offset').val());
        var totalContents = $('#countcontent').val();
        if(offset >= totalContents){ $('#viewsearchmore').hide(); }
        
    });    
</script>      
   <!-- ***************************************** -->