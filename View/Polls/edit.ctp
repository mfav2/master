<?php  echo $this->element('Components/breadcrumb'); ?>

<link rel="stylesheet" href="lib/tag_handler/css/jquery.taghandler.css" />

<?php
    $question_type = array(1=>__('One Choice'), 2=> __('Multiple Choices'));
?>
<?php
echo '<form enctype="multipart/form-data" method="post" action="' . $this->Portal->makeUrl($this->name, 'edit') .'" class="form-horizontal well">';

?>
    <p class="f_legend"><?php echo __("Add New Poll "); ?></p>
    <div>
        <label><?php echo __("Subject");?></label>
        <?php echo $this->Form->input('id', array('type'=>'hidden', 'value'=>$content['Content']['id'])); ?>
        <?php echo $this->Form->input('title', array('label'=>false, 'class'=>'span10', 'placeholder'=>__('Subject'), 'value'=>@$content['Content']['title'])); ?><br />
        
        <label><?php echo __("Detail");?></label>
        <?php echo $this->Form->input('description', array('type'=>'textarea', 'label'=>false, 'class'=>'span10 wysiwg_mini', 'placeholder'=>__('Detail'), 'value'=>  htmlspecialchars_decode(@$content['Content']['description']))); ?><br />

        <label><?php echo __("Attachment");?></label>
        <?php //echo $this->Form->input('attachment', array('type'=>'file', 'class'=>'span10', 'label'=>false)); 
              if(!$attachment){
                echo $this->Form->input('attachment', array('type'=>'file', 'class'=>'span10', 'label'=>false));   
              }else{
                foreach($attachment as $attach){
                  echo   '<span style="margin:100px 0px 0px 10px;"><a href="/fileProviders/index/'.$attach['ResourceContent']['key'].'" target="_blank"><i class="icon-adt_atach"></i>&nbsp;'.$attach['ResourceContent']['original_name'].'</a></span><span style="margin:0 0 0 5px;"><i class="icon-trash" style="cursor:pointer;" onclick="deleteattach('.$attach['PortalAttachment']['resource_content_id'].')"></i></span>';
                }
              }  
                
        ?>
                
        <hr />
        <?php
            if(!empty($question)){
                $tmp_question_no = 0;
                foreach($question AS $i => $q){
        ?>
                <div class="input-line">            
                    <div class="post-que-title-input">
                        <input name="data[Poll][<?php echo $i; ?>][question_id]" type="hidden" value="<?php echo @$q['Question']['id']; ?>" />
                        <input name="data[Poll][<?php echo $i; ?>][Question]" class="remark-txt" placeholder="<?php echo __('Question'); ?>" type="text" value="<?php echo $q['Question']['question_name'];?>">
                        <a href="javascript:void(0);" class="remove-que" onclick="removeQuestion($(this))"><?php echo __('DeleteQuestion');?></a>
                    </div><br />
                    <div class="input-line small-txt"><?php echo __("Question Type");?>
                        <select name="data[Poll][<?php echo $i; ?>][Type]" onchange="changeOptionType($(this))">
                            <option value="1" <?php if($q['Question']['question_type'] == 1){ echo 'selected="selected"'; } ?>><?php echo __("One Choice");?></option>
                            <option value="2" <?php if($q['Question']['question_type'] == 2){ echo 'selected="selected"'; } ?>><?php echo __("Multiple Choices");?></option>
                        </select>

                    </div><br />

                    <ul class="post-que-option-list" style="list-style: none;">
                        <?php
                            $tmp_choice = 0;
                            foreach($q['QuestionAnswer'] AS $tmp => $answer){
                        ?>
                                <li class="one-choice">
                                    <input class="text remark-txt" placeholder="<?php echo __("Choice"); ?>" name="data[Poll][<?php echo $i; ?>][Option][id:<?php echo $answer['id'];?>]" type="text" value="<?php echo $answer['answer_name'];?>">
                                    <span class="txt_error"></span>
                                    <span class="icon-remove" title="<?php echo __("Delete"); ?>" onclick="removeOption($(this))"></span>
                                </li>
                        <?php
                                $tmp_choice++;
                            }
                        ?>

                        <li class="add-option">
                            <a href="javascript:void(0);" class="add-choice" offset="0" next="<?php echo $tmp_choice; ?>" onclick="addOption($(this))">+<?php echo __("Add More Choices"); ?></a>
                        </li>
                    </ul>
                </div><br /><br />
        <?php 
                    $tmp_question_no = $i + 1;
                }
            }
        ?>
        
        
        
        <div>
            <a href="javascript:void(0);" next="<?php echo $tmp_question_no; ?>" class="post-add-question"><?php echo __("Add More Questions"); ?></a>
        </div>
        
        
        <br /><br /><br />
        <?php echo $this->Form->submit(__('Submit'), array('class'=>'btn')); ?>
    </div>
<?php
echo "</form>";
?>

<?php
    //if(!empty($this->data)){ pr($this->data); }
    //pr($question);
    //pr($this->request->query);
?>

<script type="text/javascript">

function addOption(obj){
    var n = parseInt($(obj).attr("next"));
    var offset = parseInt($(obj).attr("offset"));
    var str = '';

    str += '<li class="one-choice">';
//    str += '	<input class="radio" name="item_group:1" type="radio">';
    str += '	<input class="text remark-txt" placeholder="<?php echo __("Choice"); ?>" name="data[Poll][' + offset + '][Option][' + n + ']" type="text">';
    str += '    <span class="txt_error"></span>';
    str += '    <span class="icon-remove" title="<?php echo __("Delete"); ?>" onclick="removeOption($(this))"></span>';
    str += '</li>';

    $(obj).parent().before(str);

    $(obj).attr("next", n + 1);
}


function removeOption(obj){
    $(obj).closest("li").remove();
}

function removeQuestion(obj){
    $(obj).closest("div.input-line").remove();
}

function deleteattach(id){
     //alert(id+' '+conid+ ' '+catid);
              if(confirm('<?php echo __("Do you want to delete this files?"); ?> ')){
                    //window.location.href="/appointments/insertappointment"
                  $.ajax({
                    cache: false,
                    url: "/polls/deleteAttachment",
                    type: "post",
                    data: {'id':id },
                    success:function(data){
                        if(data == 1){
                            location.reload();
                        }else{
                            alert(data);
                        }
                    }
                  });    
              }else{
                  return false;   
              }
 }

$(document).ready(function(){
    
    
    $("form").submit(function() {        
        $.each($('input[name*="data"]'), function(){
            if($(this).val() == ''){
                $(this).next("span.txt_error").html('*');
                $(this).focus();
                
                return false;
            }
        });
        
        
    });
    
    $("a.post-add-question").click(function(){
        var n = parseInt($(this).attr("next"));
        var str = '';
        
        str += '<div class="input-line">            ';
        str += '    <div class="post-que-title-input">';
		str += '		<input name="data[Poll][' + n + '][Question]" class="remark-txt" placeholder="<?=__('Question')?>" type="text">';
		str += '		<a href="javascript:void(0);" class="remove-que" onclick="removeQuestion($(this))"><?=__('DeleteQuestion')?></a>';
		str += '	</div><br />';
		str += '	<div class="input-line small-txt"><?php echo __("Question Type");?>';
        str += '        <select name="data[Poll][' + n + '][Type]">';
        str += '            <option value="1"><?php echo __("One Choice");?></option>';
        str += '            <option value="2"><?php echo __("Multiple Choices");?></option>';
        str += '        </select>';
                
		str += '	</div><br />';
            
        str += '    <ul class="post-que-option-list" style="list-style: none;">';
        str += '        <li class="one-choice">';
//        str += '            <input class="radio" name="item_group:1" type="radio">';
        str += '            <input class="text remark-txt" placeholder="<?php echo __("Choice"); ?>" name="data[Poll][' + n + '][Option][0]" type="text">';
        str += '            <span class="txt_error"></span>';
        str += '            <span class="icon-remove" title="<?php echo __("Delete"); ?>" onclick="removeOption($(this))"></span>';
        str += '        </li>';
        str += '        <li class="one-choice">';
//        str += '            <input class="radio" name="item_group:1" type="radio">';
        str += '            <input class="text remark-txt" placeholder="<?php echo __("Choice"); ?>" name="data[Poll][' + n + '][Option][1]" type="text">';
        str += '            <span class="txt_error"></span>';
        str += '            <span class="icon-remove" title="<?php echo __("Delete"); ?>" onclick="removeOption($(this))"></span>';
        str += '        </li>';
                
        str += '        <li class="add-option">';
        str += '            <a href="javascript:void(0);" class="add-choice" offset="' + n + '" next="2" onclick="addOption($(this))">+<?=__('Add More Choices')?></a>';
        str += '        </li>';
        str += '    </ul>';
        str += '<br /><br /></div>';
        
        $(this).before(str);
        
        $(this).attr("next", n + 1);
    });
    
 });
 
</script>
<script src="js/custom/custom_forms.js"> </script>