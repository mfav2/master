<?php
    $currentUser = $this->Session->read('AuthUser');
    $permiss =  $currentUser['PermissionGroup'];
    $permisssubscribe = $currentUser['PermissionSubscribe'];

    $showdelete = 'N';
    foreach($permiss as $permission){
        if($permission['PermissionGroup']['function_id'] == 9){
            if($permission['PermissionGroup']['is_delete'] == 'Y'){
                $showdelete = 'Y';
            }      
        }
    }
?>
    <?php if(!empty($contents)){ 
        foreach ($contents as $content) : 
    ?>
        <div id="showmorecontent">
            <div class="prettyprint" style="background-color: white; border-collapse: collapse; border-width: 1px; border-top-width: 0px;" >
                <?php echo $this->Time->format('d M y', $content['Content']['event_date']).'&nbsp;&nbsp;&nbsp;'; ?>
                <a class="pop_over" data-placement="bottom" href="<?php echo $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $content['Content']['id'] ); ?>">
                    <?php 
                       echo empty($content['Content']['title']) ? __('No title') : mb_substr(strip_tags(htmlspecialchars_decode($content['Content']['title'])), 0, 256, 'UTF-8');
                    ?>
                </a>
                <?php 
                    if(!empty($content['Content']['attachment1']) || !empty($content['Content']['attachment2']) || !empty($content['Content']['attachment3']) ){
                        echo ' - ';
                    }
                    if(!empty($content['Content']['attachment1'])){
                        echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment1']) . '"><i class="icon-adt_atach"></i></a>';
                    }
                    if(!empty($content['Content']['attachment2'])){
                        echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment2']) . '"><i class="icon-adt_atach"></i></a>';
                    }
                    if(!empty($content['Content']['attachment3'])){
                        echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment3']) . '"><i class="icon-adt_atach"></i></a>';
                    }

                ?>
                <?php if($showdelete == 'Y' || $content['Content']['user_id'] == $currentUser['AuthUser']['id']){ ?>
                <span class="pull-right">
                    <a href="<?= $this->Portal->makeUrl($this->name, 'delete', 'content_id=' . $content['Content']['id'] ); ?>" onclick="return confirm('<?=__('Confirm to delete')?> ?')"><i class='icon-trash'></i></a>
                </span>
                <?php } ?> 
                <span class="label label-info"><?php echo $content['Content']['count_vote']; ?> <?php echo __('Votes');?></span>
                
            </div>
        </div>       
     <?php endforeach; ?>
<?php } ?>

