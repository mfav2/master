<?php  
    echo $this->element('Components/breadcrumb'); 
    $currentUser = $this->Session->Read('AuthUser');    
    $permiss =  $currentUser['PermissionGroup'];
    $permisssubscribe = $currentUser['PermissionSubscribe'];

    $showdelete = 'N';
    foreach($permiss as $permission){
        if($permission['PermissionGroup']['function_id'] == 9){
            if($permission['PermissionGroup']['is_delete'] == 'Y'){
                $showdelete = 'Y';
            }      
        }
    }
?>
<div class="doc_view">
    <div class="doc_view_header">
        <dl class="dl-horizontal" style="padding:10px; ">
            <?php if ($showdelete == 'Y') : ?>
                <span class="pull-right">
                    <a href="<?= $this->Portal->makeUrl($this->name, 'delete', 'content_id=' . $content['Content']['id']); ?>" onclick="return confirm('<?php echo __('Confirm to delete');?> ?')">
                        <img alt="" src="img/gCons/recycle-full.png">
                    </a>
                </span>
            <?php endif; ?>
            <!-- ************************ Title Name ***************************** -->
            <?php echo htmlspecialchars_decode($content['Content']['title']); ?>
        </dl>
    </div>
    <div class="doc_view_content">
        <?php echo htmlspecialchars_decode($content['Content']['description']); ?><br/>
        <h5><br />(<?php echo __('Expire on'); ?> : <?= $this->Time->format('d/m/Y', $content['Content']['expiry_date']); ?>)</h5>    
        <?php    
            if(!empty($content['Content']['attachment1'])){
                $path_parts = pathinfo($content['Content']['attachment1_name']);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
                $pic_ext = array("jpg", "gif", "png");
                if(in_array($path_parts['extension'], $pic_ext)){
                    echo "<div>";  
                    echo "<a target='_blank' href='".$this->FileStorageHelper->urlByKey($content['Content']['attachment1'])."'>";
                    echo "<img style='max-width:300px; border=0' src='/fileProviders/index/{$content['Content']['attachment1']}' />";
                    echo "</a>";
                    echo "</div><br />";                 
                }else{
                    if($path_parts['extension'] == 'doc' || $path_parts['extension'] == 'docx'){
                        $showicon = '<img src="img/custom/doc/word.png" width="26" height="26" />';
                    }else if($path_parts['extension'] == 'xls' || $path_parts['extension'] == 'xlsx'){
                        $showicon = '<img src="img/custom/doc/excel.png" width="26" height="26" />';
                    }else if($path_parts['extension'] == 'ppt' || $path_parts['extension'] == 'pptx'){
                        $showicon = '<img src="img/custom/doc/ppt.png" width="26" height="26" />';
                    }else if($path_parts['extension'] == 'pdf'){
                        $showicon = '<img src="img/custom/doc/pdf.png" width="26" height="26" />';
                    }else{
                        $showicon = '<i class="icon-adt_atach"></i>';
                    }
                    echo '&nbsp;&nbsp;<span ><a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment1']) . '">'.$showicon.' '.$content['Content']['attachment1_name'].'</a></span><br /><br />';
                }
                
            }
        ?>
        <br />   
        <?php
            if(!empty($question)){ 
              foreach($question AS $i => $q){
                $tmp_i = $i + 1;   
        ?>
                <div class="input-line">            
                    <div class="post-que-title-input">
                        <span><?php echo $tmp_i . ". " . $q['Question']['question_name']; ?></span>
                            <?php 
                                if(!empty($question[$i]['QuestionAnswer'])){ 
                                    echo '<ul class="post-que-option-list" style="list-style:none;">';
                                    foreach($question[$i]['QuestionAnswer'] AS $opt){

                            ?>
                                        <li class="one-choice">
                                            <?php echo $opt['answer_name']; ?><br />
                                            <?php 
                                                $progress_bar_val = empty($results[$q['Question']['id']]['Option'][$opt['id']]['total']) ? 0 : $results[$q['Question']['id']]['Option'][$opt['id']]['total'];

                                                if(!empty($total_result[$i][0]['totuser'])){
                                                    $max = $total_result[$i][0]['totuser'];
                                                    $percent = round($progress_bar_val*100/$max, 2);
                                                }else{
                                                    $max = 0;
                                                    $percent = 0;
                                                }
                                            ?>
                                                <div class='progressbar' val="<?php echo $progress_bar_val; ?>" maxvalue="<?php echo $max; ?>" style="width: 80%; display: inline-block"></div><span> <?php echo $progress_bar_val . "/" . $max; ?>&nbsp;(<?=$percent?>&nbsp;%)</span>
                                        </li>
                            <?php 
                                    }
                                    echo '</ul>';
                                } 
                            ?>        
                    </div>
                    <br />
                </div>
                   <br /> 
        <?php
              }  
            }


            echo __("Already finshed poll ").$ResultOfPoll.' '.__("Persons");
        ?>
        <br />
        <?php 
        if($content['Content']['no_show_name'] == 'Y'){   
            if(!empty($question)){ 
               foreach($question AS $i => $q){
                  $tmp_i = $i + 1; 
        ?>
                <div class="input-line">            
                        <div class="post-que-title-input">
                            <span><?php echo $tmp_i . ". " . $q['Question']['question_name']; ?></span>
                            <input type="hidden" name="data[Question][<?php echo $q['Question']['id'];?>][question_type]" value="<?php echo $q['Question']['question_type']; ?>" />
                        </div>

                        <?php 
                            if(!empty($question[$i]['QuestionAnswer'])){ 
                                echo '<ul class="post-que-option-list" style="list-style:none;">';
                                foreach($question[$i]['QuestionAnswer'] AS $opt){
                        ?>
                                    <li class="one-choice"><b>
                                        <?php echo $opt['answer_name']; ?></b><br />
                                        <?php
                                        foreach($question[$i]['QuestionResult'] as $result){

                                            if(isset($result['answer_id'])){
                                                if($opt['id'] == $result['answer_id']){
                                                    $votetime = $this->Time->format('d M y H:i:s', $result['vote_date']);
                                                    echo '&nbsp;&nbsp;&nbsp;'.$result['first_name'].' '.$result['last_name'].' - [&nbsp;'.$result['organization_name'].'&nbsp;]&nbsp;('.$votetime.'&nbsp;)';
                                                   // echo '&nbsp;&nbsp;&nbsp;'.$result['content_updated_user']." - [&nbsp;".$result['organization_description']."&nbsp;]<br />";
                                                   
                                                }
                                            }else{
                                                echo " - ";
                                            }    
                                        }
                                        ?>
                                    </li>
                        <?php 

                                }
                                echo '</ul>';
                            } 
                        ?>
                </div>  
        <?php
               }  
            }
        }  
        ?>    
    </div>
    <div class="doc_view_footer">
        &nbsp;
    </div>
</div>

<script type="text/javascript">
$(function(){
//    $( "div.progressbar" ).progressbar({
//        max: 10,
//        value: 5
//    });
    var value = 0;
    var max = 0;
    $.each($(".progressbar"), function(){
        value = parseInt($(this).attr('val'));
        //value = 1;
        max = $(this).attr('maxvalue');
        
        $(this).progressbar({
            value: value,
            max: max
        });
        
    });
});
</script>