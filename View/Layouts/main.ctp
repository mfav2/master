<?php $currentUser = $this->session->read('AuthUser'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo Configure::read('Application.Name');?></title>
	<?php echo $this->element('Commons/meta'); ?>
	<?php echo $this->element('Commons/main/css'); ?>
	<?php echo $this->element('Commons/ico'); ?> 
	<?php echo $this->element('Commons/jsheader'); ?>
        
        
		
    </head>
    <body>
	<div id="loading_layer" style="display:none"><img src="img/ajax_loader.gif" alt="" /></div>
	<?php //echo $this->element('Widgets/style_switcher'); ?>	
        <?php echo $this->element('Widgets/chat_switcher'); ?>	
	<div id="maincontainer" class="clearfix">
		<?php echo $this->element('Commons/header'); ?>
		<!-- main content -->
		<div id="contentwrapper">
			<div class="main_content">
				<?php echo $this->fetch('content'); ?>                            
			</div>
		</div>
		<?php echo $this->element('Commons/sidebar'); ?> 
		<?php echo $this->element('Commons/main/jsfooter'); ?>	
              
	</div>
        <div id="footer">
            <div class="footer_content">
                <div style="color:#8e9397; text-align: right; vertical-align: middle;"><?php echo Configure::read('Application.Copyright');?></div>
		<div>
		<?php 
			if($currentUser['AuthUser']['username'] == 'pitak'){
				echo $this->element('sql_dump');
			}
		?>
		</div>
        </div>
    </body>
</html>
