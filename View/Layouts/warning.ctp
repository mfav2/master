<!DOCTYPE html>
<html lang="en" class="error_page">
    <head>

	<title>Error Page - 404</title>
	<!-- Bootstrap framework -->
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" href="bootstrap/css/bootstrap-responsive.min.css" />
	<!-- main styles -->
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Jockey+One" />
		
    </head>
    <body>
	<?php echo $this->fetch('content'); ?>
    </body>
</html>
