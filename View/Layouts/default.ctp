<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo Configure::read('Application.Name');?>
	<?php echo $this->element('Commons/meta'); ?>
	<?php echo $this->element('Commons/css'); ?>
	<?php echo $this->element('Commons/ico'); ?> 
	<?php echo $this->element('Commons/jsheader'); ?>
		
    </head>
    <body>
	<div id="loading_layer" style="display:none"><img src="img/ajax_loader.gif" alt="" /></div>
	<?php echo $this->element('Widgets/style_switcher'); ?>	
	<div id="maincontainer" class="clearfix">
		<?php echo $this->element('Commons/header'); ?>
		<!-- main content -->
		<div id="contentwrapper">
			<div class="main_content">
				<?php echo $this->fetch('content'); ?>
			</div>
		</div>
		<?php echo $this->element('Commons/sidebar'); ?> 
		<?php echo $this->element('Commons/jsfooter'); ?>		
	</div>
    </body>
</html>
