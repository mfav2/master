	
	<!-- smart resize event -->
	<script src="js/jquery.debouncedresize.min.js"></script>
	<!-- hidden elements width/height -->
	<script src="js/jquery.actual.min.js"></script>
	<!-- js cookie plugin -->
	<script src="js/jquery.cookie.min.js"></script>
	<!-- main bootstrap js -->
	<script src="bootstrap/js/bootstrap.min.js"></script>








	<!-- tooltips -->
	<script src="lib/qtip2/jquery.qtip.min.js"></script>
	<!-- jBreadcrumbs -->
	<script src="lib/jBreadcrumbs/js/jquery.jBreadCrumb.1.1.min.js"></script>
	<!-- sticky messages -->
	<script src="lib/sticky/sticky.min.js"></script>
	<!-- fix for ios orientation change -->
	<script src="js/ios-orientationchange-fix.js"></script>
	<!-- scrollbar -->
	<script src="lib/antiscroll/antiscroll.js"></script>
	<script src="lib/antiscroll/jquery-mousewheel.js"></script>
	<!-- lightbox -->
	<script src="lib/colorbox/jquery.colorbox.min.js"></script>
	<!-- common functions -->
	<script src="js/gebo_common.js"></script>
         <!-- datepicker -->
        <script src="lib/datepicker/bootstrap-datepicker.min.js"></script>
        <!-- timepicker -->
        <script src="lib/datepicker/bootstrap-timepicker.min.js"></script>






	<!-- jQuery UI -->
	<script src="lib/jquery-ui/jquery-ui-1.8.23.custom.min.js"></script>
	<!-- touch events for jQuery UI -->
	<script src="js/forms/jquery.ui.touch-punch.min.js"></script>
	<!-- calendar -->
	<script src="lib/fullcalendar/fullcalendar.js"></script>
	<script src="lib/fullcalendar/gcal.js"></script>

	<!-- calendar functions -->
<!--	<script src="js/gebo_calendar.js"></script>-->

	<script>
		$(document).ready(function() {
			//* show all elements & remove preloader
			setTimeout('$("html").removeClass("js")',1000);
		});
	</script>