	<script src="js/jquery.min.js"></script>
	<!-- smart resize event -->
	<script src="js/jquery.debouncedresize.min.js"></script>
	<!-- hidden elements width/height -->
	<script src="js/jquery.actual.min.js"></script>
	<!-- js cookie plugin -->
	<script src="js/jquery.cookie.min.js"></script>
	<!-- main bootstrap js -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- bootstrap plugins -->
	<script src="js/bootstrap.plugins.min.js"></script>
	<!-- code prettifier -->
	<script src="lib/google-code-prettify/prettify.min.js"></script>




	<!-- tooltips -->
	<script src="lib/qtip2/jquery.qtip.min.js"></script>
	<!-- jBreadcrumbs -->
	<script src="lib/jBreadcrumbs/js/jquery.jBreadCrumb.1.1.min.js"></script>
	<!-- sticky messages -->
	<script src="lib/sticky/sticky.min.js"></script>
	<!-- fix for ios orientation change -->
	<script src="js/ios-orientationchange-fix.js"></script>
	<!-- scrollbar -->
	<script src="lib/antiscroll/antiscroll.js"></script>
	<script src="lib/antiscroll/jquery-mousewheel.js"></script>


	<!-- common functions -->
	<script src="js/gebo_common.js"></script>

	<!-- colorbox -->
	<script src="lib/colorbox/jquery.colorbox.min.js"></script>
	<!-- datatable -->
	<script src="lib/datatables/jquery.dataTables.min.js"></script>
	<!-- additional sorting for datatables -->
	<script src="lib/datatables/jquery.dataTables.sorting.js"></script>
	<!-- tables functions -->
	<script src="js/gebo_tables.js"></script>

	<script>
		$(document).ready(function() {
			//* show all elements & remove preloader
			setTimeout('$("html").removeClass("js")',1000);
		});
	</script>