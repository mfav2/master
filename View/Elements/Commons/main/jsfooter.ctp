
                        <!-- smart resize event -->
                        <script src="js/jquery.debouncedresize.min.js"></script>
                        <!-- hidden elements width/height -->
                        <script src="js/jquery.actual.min.js"></script>

                       
                        <!-- js cookie plugin -->
                        <script src="js/jquery.cookie.min.js"></script>
                        <!-- main bootstrap js -->
                        <script src="bootstrap/js/bootstrap.js"></script>
                        <!-- bootstrap plugins -->
                        <script src="js/bootstrap.plugins.min.js"></script>
                        <!-- code prettifier -->
                        <script src="lib/google-code-prettify/prettify.min.js"></script>
                        <!-- lightbox -->
                        <script src="lib/colorbox/jquery.colorbox.min.js"></script>
                        <!-- tooltips -->
                        <script src="lib/qtip2/jquery.qtip.min.js"></script>
                        <!-- jBreadcrumbs -->
                        <script src="lib/jBreadcrumbs/js/jquery.jBreadCrumb.1.1.min.js"></script>
                        <!-- sticky messages -->
                        <script src="lib/sticky/sticky.min.js"></script>
                        <!-- fix for ios orientation change -->
                        <script src="js/ios-orientationchange-fix.js"></script>
                        <!-- scrollbar -->
                        <script src="lib/antiscroll/antiscroll.js"></script>
                        <script src="lib/antiscroll/jquery-mousewheel.js"></script>
                        <!-- lightbox -->
                        <script src="lib/colorbox/jquery.colorbox.min.js"></script>

                       
                        
                        <!-- to top -->
                        <script src="lib/UItoTop/jquery.ui.totop.min.js"></script>
                        <!-- jQuery UI -->
                        <script src="lib/jquery-ui/jquery-ui-1.8.23.custom.min.js"></script>
                        <!-- touch events for jQuery UI -->
                        <script src="js/forms/jquery.ui.touch-punch.min.js"></script>
                        <!-- multi-column layout -->
                        <script src="js/jquery.imagesloaded.min.js"></script>
                        <script src="js/jquery.wookmark.js"></script>
                        <!-- responsive table -->
                        <script src="js/jquery.mediaTable.min.js"></script>
                        <!-- small charts -->
                        <script src="js/jquery.peity.min.js"></script>
                        <!-- charts -->
                        <script src="lib/flot/jquery.flot.min.js"></script>
                        <script src="lib/flot/jquery.flot.resize.min.js"></script>
                        <script src="lib/flot/jquery.flot.pie.min.js"></script>
                        <!-- calendar -->
                        <script src="lib/fullcalendar/fullcalendar.js"></script>
                        <script src="lib/fullcalendar/gcal.js"></script>

                        <!-- datepicker -->
                        <script src="lib/datepicker/bootstrap-datepicker.min.js"></script>
                        <!-- timepicker -->
                        <script src="lib/datepicker/bootstrap-timepicker.min.js"></script>
                        <script src="lib/jtt-timepicker/jquery.timepicker.min.js"></script>
                        <!-- colorbox -->
                        <script src="lib/colorbox/jquery.colorbox.min.js"></script>
                        <!-- datatable (inbox,outbox) -->
                        <script src="lib/datatables/jquery.dataTables.min.js"></script>
                        <!-- additional sorting for datatables -->
                        <script src="lib/datatables/jquery.dataTables.sorting.js"></script>
                        <!-- plupload and all it's runtimes and the jQuery queue widget (attachments) -->
                        <!-- sortable/filterable list -->
                        <script src="lib/list_js/list.min.js"></script>
                        <script src="lib/list_js/plugins/paging/list.paging.js"></script>
                        <!-- input mask -->
                         <script src="js/forms/jquery.inputmask.min.js"></script>    
                        <!-- autosize textareas -->
                        <script src="js/forms/jquery.autosize.min.js"></script>
                        <!-- enhanced select -->
                        <script src="lib/chosen/chosen.jquery.min.js"></script>

                        <!-- TinyMce WYSIWG editor -->
                        <script src="lib/tiny_mce/jquery.tinymce.js"></script>
                        <!-- colorpicker -->
                        <script src="lib/colorpicker/bootstrap-colorpicker.js"></script>
                        <!-- password strength checker -->
                        <script src="lib/complexify/jquery.complexify.min.js"></script>
                        <!-- styled form elements -->
                        <script src="lib/uniform/jquery.uniform.min.js"></script> 
                        <!-- datatable -->
                        <script src="lib/datatables/jquery.dataTables.min.js"></script>
                        <!-- additional sorting for datatables -->
                        <script src="lib/datatables/jquery.dataTables.sorting.js"></script> 
                        <!-- validation -->
                        <script src="lib/validation/jquery.validate.min.js"></script>

                        <script type="text/javascript" src="lib/plupload/js/plupload.full.js"></script>
                        <script type="text/javascript" src="lib/plupload/js/jquery.plupload.queue/jquery.plupload.queue.full.js"></script>
                        <!-- autosize textareas (new message) -->
                        <script src="js/forms/jquery.autosize.min.js"></script>
                        <!-- tag handler (recipients) -->
                        <script src="lib/tag_handler/jquery.taghandler.min.js"></script>
                       
			<!--<script src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
			<!--<script src="js/gmap3.min.js"></script>-->
                        <!-- common functions -->
                        <script src="js/gebo_common.js"></script>
			<script src="js/event_common.js"></script>
			<script src="js/jquery.playSound.js"></script>
			<script src="lib/pnotify/jquery.pnotify.min.js"></script>
			
                        <!-- validation functions -->
                        <script src="js/gebo_validation.js"></script>
                        <!--tables functions--> 
                        <script src="js/gebo_tables.js"></script>
                        <script src="lib/jstree/jquery.jstree.js"></script>
                        <script src="js/jquery.form.js"></script>
			<script>
				$(document).ready(function() {
					//* show all elements & remove preloader
					setTimeout('$("html").removeClass("js")',1000);
				});
			</script>