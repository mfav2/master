<?php
$currentUser = $this->session->read('AuthUser');

$pr = '';
$cat_id = '';
$office_code = $currentUser['AuthUserProfile'][0]['officer_code'];
$subscribe = $currentUser['PermissionSubscribe'];

/* Check Data */
$showMP = 'N';
$showDG = 'N';
$showIDM = 'N';
$showSP = 'N';
$showBuild = 'N';
$showCabi = 'N'; 
$showPP = 'N';

// URL Show Menu
$allInternalarray = array();
$allgoodref = array();
$linkMP = array(); 
$linkDG = array();
$linkIDM = array();
$linkSP = array();
$linkBuild = array();
$linkCabi = array();
$linkPP = array();

if(!empty($subscribe)){
    foreach($subscribe as $sub){ 
       switch($sub['GroupSubscribe']['function_id']){
           case 3  :  
                      if(isset($sub['GroupSubscribe']['detail'])){
                            if($sub['GroupSubscribe']['detail'] == 'Morning Prayer'){
                                $showMP = 'Y';
                            }
                            if($sub['GroupSubscribe']['detail'] == 'DG Mtg'){
                                $showDG = 'Y';
                            }
                            if($sub['GroupSubscribe']['detail'] == 'Inter Dept Mtg'){
                                $showIDM = 'Y';
                            }
                            if($sub['GroupSubscribe']['detail'] == 'ประชุมหน่วยงาน สป.'){
                                $showSP = 'Y';
                            }
                            if($sub['GroupSubscribe']['detail'] == 'ประชุมบริหารอาคาร'){
                                $showBuild = 'Y';  
                            }
                            if($sub['GroupSubscribe']['detail'] == 'Cabinet Mtg.'){
                                $showCabi = 'Y';
                            }
                      }
                      
                      break;
                      
           case 4 :  if(isset($sub['GroupSubscribe']['detail'])){
                            if($sub['GroupSubscribe']['detail'] == 'Press Pointer'){
                                $showPP = 'Y';
                            }
                      }
                     break;   
           default : break;
                      
       } 
          
    }
    
    if($showMP =='Y'){
      $linkMP =  array('permission' => '', 'label' => __('Development Dept. Meeting'), 'controller' => 'Meetings', 'action' => 'viewall', 'query' => 'category_id=20', 'param' => '6');		        
    }
    if($showDG =='Y'){
      $linkDG = array('permission' => '', 'label' => __('Business Dept. Meeting'), 'controller' => 'Meetings', 'action' => 'viewall', 'query' => 'category_id=21', 'param' => '6');  
    }
    if($showIDM =='Y'){
      $linkIDM = array('permission' => '', 'label' => __('Product Development'), 'controller' => 'Meetings', 'action' => 'viewall', 'query' => 'category_id=23', 'param' => '6');  
    }
    if($showSP =='Y'){
      $linkSP = array('permission' => '', 'label' => __('Commercial'), 'controller' => 'Meetings', 'action' => 'viewall', 'query' => 'category_id=24', 'param' => '6'); 
    }
    if($showBuild =='Y'){
      $linkBuild = array('permission' => '', 'label' => __('Project Management'), 'controller' => 'Meetings', 'action' => 'viewall', 'query' => 'category_id=25', 'param' => '6'); 
    }
    if($showCabi =='Y'){
      $linkCabi = array('permission' => '', 'label' => __('Executive Meeting'), 'controller' => 'Meetings', 'action' => 'viewall', 'query' => 'category_id=123', 'param' => '6'); 
    }  
    
    /*if($showPP =='Y'){
      $linkPP = array('permission' => '', 'label' => __('Press Pointer'), 'controller' => 'References', 'action' => 'viewall', 'query' => 'category_id=48', 'param' => '11');
    }*/
    $allgoodref = array(
        /*array('permission' => '', 'label' => __('Speech'), 'controller' => 'References', 'action' => 'viewallsub', 'query' => 'main_cate=26&category_id=27', 'param' => '11'),
        array('permission' => '', 'label' => __('Note'), 'controller' => 'References', 'action' => 'viewallsub', 'query' => 'main_cate=26&category_id=28', 'param' => '11'),*/
        array('permission' => '', 'label' => __('SOP / Manual'), 'controller' => 'References', 'action' => 'viewall', 'query' => 'category_id=29', 'param' => '11'),
        array('permission' => '', 'label' => __('Useful Info'), 'controller' => 'References', 'action' => 'viewall', 'query' => 'category_id=30', 'param' => '11'),
        //array('permission' => '', 'label' => __('Amb. / CG. Handbook'), 'controller' => 'References', 'action' => 'viewallsub', 'query' => 'main_cate=26&category_id=31', 'param' => '11'),
        //$linkPP,
    );
    
    $allInternalarray = array(
        $linkMP,
        $linkDG,
        //array('permission' => '', 'label' => __('Amb. Meeting'), 'controller' => 'Meetings', 'action' => 'viewall', 'query' => 'category_id=22', 'param' => '6'),
        $linkIDM,
        $linkSP,
        $linkBuild,
        $linkCabi,
    );
    
    $internalmeeting = array(
        'label' => __('Internal Meetings'),
        'icon' => '',
        'class' => 'in',	
        'link' => '',
        'controller' => '',
        'action' => '',
        'query' => '',
        'param' => '',
        'menu' => $allInternalarray, 
    );
    
    
    $goodreffer = array(
        'label' => __('Good References'),
        'icon' => '',
        'class' => 'in',	
        'link' => '',
        'controller' => '',
        'action' => '',
        'query' => '',
        'param' => '',
        'menu' => $allgoodref,
    );
    
/**/  
}else{
  $internalmeeting = array(
        'label' => __('Internal Meetings'),
        'icon' => '',
        'class' => 'in',	
        'link' => '',
        'controller' => '',
        'action' => '',
        'query' => '',
        'param' => '',
        'menu' => array(
            array('permission' => '', 'label' => __('Amb. Meeting'), 'controller' => 'Meetings', 'action' => 'viewall', 'query' => 'category_id=22', 'param' => '6'),            
        ), 
    );
  $goodreffer = array();
}


/*if(){
   
}*/
/* End Check */


if(isset($this->request->query['pq'])){
    $cat_id = base64_decode($this->request->query['pq']);  
} 
if(isset($this->request->query['pr'])){
    $pr = base64_decode($this->request->query['pr']);  
} 


$menuAccordions = array(
	array(
		'label' => __('Circular Bulletin / <br>Article'),
		'icon' => '',
		'class' => 'in',
		'link' => '',
		'controller' => 'Contents',
		'action' => 'index',
		'query' => '',
		'param' => '',
		'menu' => '',
	),
	array(
		'label' => __('Schedule / <br>Appointment'),
		'icon' => '',
		'class' => 'in',
		'link' => '',
		'controller' => 'Appointments',
		'action' => 'calendar',
		'query' => '',
		'param' => '',
		'menu' => '',
	),
	array(
		'label' => __('To Do'),
		'icon' => '',
		'class' => 'in',
		'link' => '',
		'controller' => 'Contents',
		'action' => 'index',
		'query' => '',
		'param' => '',
		'menu' => '',
	),
	array(
		'label' => __('High Level Schedules'),
		'icon' => '',
		'class' => 'in',
		'link' => '',
		'controller' => 'Highlevels',
		'action' => 'highlevelindex',
		'query' => 'high_level_group=3',
		'param' => '',
		'menu' => '',
	),
	array(
		'label' => __('View Shared Schedules'),
		'icon' => '',
		'class' => 'in',
		'link' => '',
		'controller' => 'ViewShares',
		'action' => 'index',
		'query' => '',
		'param' => '',
		'menu' => '',
	),
	/*array(
		'label' => __('News Clipping'),
		'icon' => '',
		'class' => 'in',	
		'link' => 'http://prnews.mfa.go.th/prnews/newsindex.html',
		'controller' => '',
		'action' => '',
		'query' => '',
		'param' => '',
		'menu' => '',
	),
	array(
		'label' => __('Directories'),
		'icon' => '',
		'class' => 'in',	
		'link' => '',
		'controller' => '',
		'action' => '',
		'query' => '',
		'param' => '',
		'menu' => array(
			array('permission' => '', 'label' => __('Ministry & Thai Emb. /CG.'), 'controller' => '', 'action' => '', 'query' => '', 'param' => '', 'link' => 'http://www.mfa.go.th/mofa-login?cID='.$office_code),
			array('permission' => '', 'label' => __('Foreign Missions'), 'controller' => 'Contents', 'action' => '', 'query' => '', 'param' => '', 'link' => 'http://www.mfa.go.th/main/en/information'),
			
		),
	),*/
	$internalmeeting,
	array(
		'label' => __('Poll'),
		'icon' => '',
		'class' => 'in',	
		'link' => '',
		'controller' => 'Polls',
		'action' => 'index',
		'query' => '',
		'param' => '',
		'menu' => '',
	),
	array(
		'label' => __('Forum'),
		'icon' => '',
		'class' => 'in',	
		'link' => '',
		'controller' => 'Boards',
		'action' => 'index',
		'query' => 'category_id=18',
		'param' => '',
		'menu' => '',
	),
	array(
		'label' => __('Classified'),
		'icon' => '',
		'class' => 'in',	
		'link' => '',
		'controller' => 'Boards',
		'action' => 'index',
		'query' => 'category_id=17',
		'param' => '',
		'menu' => '',
	),
	array(
		'label' => __('Message Board'),
		'icon' => '',
		'class' => 'in',	
		'link' => '',
		'controller' => '',
		'action' => '',
		'query' => '',
		'param' => '',
		'menu' => array(
			//array('permission' => '', 'label' => __('Word Template'), 'controller' => 'FormTemplates', 'action' => 'viewallsub', 'query' => 'main_cate=32&category_id=33', 'param' => '10'),
			array('permission' => '', 'label' => __('Word Template'), 'controller' => 'FormTemplates', 'action' => 'viewall', 'query' => 'category_id=42', 'param' => '10'),
			array('permission' => '', 'label' => __('Presentation'), 'controller' => 'Contents', 'action' => 'viewall', 'query' => 'category_id=43', 'param' => '10'),
			array('permission' => '', 'label' => __('Template'), 'controller' => 'Contents', 'action' => 'viewall', 'query' => 'category_id=44', 'param' => '10'),
			//array('permission' => '', 'label' => __('เอกสารราชการทั่วไป'), 'controller' => 'Contents', 'action' => 'viewall', 'query' => 'category_id=45', 'param' => '10'),
			//array('permission' => '', 'label' => __('หนังสือราชการภาษาอังกฤษ'), 'controller' => 'Contents', 'action' => 'viewall', 'query' => 'category_id=46', 'param' => '10'),
			//array('permission' => '', 'label' => __('ศูนย์เทคโนโลยีสารสนเทศและการสื่อสาร'), 'controller' => 'Contents', 'action' => 'viewall', 'query' => 'category_id=119', 'param' => '10'),
			//array('permission' => '', 'label' => __('สำนักงานรัฐมนตรี'), 'controller' => 'Contents', 'action' => 'viewall', 'query' => 'category_id=120', 'param' => '10'),
			//array('permission' => '', 'label' => __('สำนักนโยบายและแผน'), 'controller' => 'Contents', 'action' => 'viewall', 'query' => 'category_id=121', 'param' => '10'),
			//array('permission' => '', 'label' => __('กองบรรณาสารและห้องสมุด'), 'controller' => 'Contents', 'action' => 'viewall', 'query' => 'category_id=122', 'param' => '10'),
		),
	),
	$goodreffer,
	/*array(
		'label' => __('Announcement'),
		'icon' => '',
		'class' => 'in',	
		'link' => '',
		'controller' => 'Contents',
		'action' => 'viewannouncement',
		'query' => 'category_id=5',
		'param' => '',
		'menu' => '',
	),
	array(
		'label' => __('MFA KPI'),
		'icon' => '',
		'class' => 'in',	
		'link' => '',
		'controller' => 'References',
		'action' => 'viewallsub',
		'query' => 'main_cate=26&category_id=47',
		'param' => '',
		'menu' => '',
	),*/
	array(
		'label' => __('Contact'),
		'icon' => '',
		'class' => 'in',	
		'link' => '',
		'controller' => 'Contacts',
		'action' => 'indexcontact',
		'query' => '',
		'param' => '',
		'menu' => '',
	),
	/*array(
		'label' => __('Menu'),
		'icon' => '',
		'class' => 'in',	
		'link' => '',
		'controller' => 'Menu',
		'action' => 'index',
		'query' => '',
		'param' => '',
		'menu' => '',		
	)*/

);

?>
<a href="javascript:void(0)" class="sidebar_switch on_switch ttip_r" title="Hide Sidebar">Sidebar switch</a>
<div class="sidebar">
	<div class="antiScroll">
		<div class="antiscroll-inner">
			<div class="antiscroll-content">
				<div class="sidebar_inner">
					<!-- Calendar -->
					<div>  
					<div id="calendarPane">
						<div id="calendarCurrentDate">
							<div class="calendarShowDate"><?=date("D j M Y");?></div>
						</div>
                                                <div id="calendarCurrentWeek">
                                                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                                                        <tr class="bg_weekwidgets" height="12">
                                                            <?php
                                                                $da = array("Su", "M", "T", "W", "Th", "F", "Sa");
                                                                for ($i=0; $i<7; $i++) {
                                                                   echo "<td align=\"center\">" . $da[$i] . "</td>";
                                                                }
                                                            ?>
                                                        </tr>
                                                        <tr class="bg_daywidgets" height="27">
                                                                <?php
                                                                        $dom = date("d");
                                                                        $dow = date("w");
                                                                        $startDate = new DateTime(date("Y-m-d"));
                                                                        $startDate->sub(new DateInterval('P' . $dow . 'D'));

                                                                        for ($i=0; $i<7; $i++) {
                                                                                        $calDate = $startDate;
                                                                                        if ($i > 0) {
                                                                                                        $calDate->modify('+1 day');
                                                                                        }
                                                                                        $isToday = false;
                                                                                        if ($calDate->format("d") == date('d')) {
                                                                                                        $isToday = true;
                                                                                        }

                                                                                        echo "<td align=\"center\" ><div " . ($isToday? " id='calendarCurrentDOM'": '') . "><a style='color:#8e9397; cursor:pointer;' onclick='addAppoint("  .$calDate->format("d"). ", "  .$calDate->format("m"). ", "  .$calDate->format("Y"). "  )'>"  . $calDate->format("d") . "</a></div></td>";
                                                                        }
                                                                ?>
                                                        </tr>
                                                    </table>
                                                </div>
					</div>
					<div style="clear:both;"></div>
					<!-- End -->
					<div id="side_accordion" class="accordion">
							<?php foreach ($menuAccordions as $key => $menuAccordion) { //pr($key); ?>
                                                           <div class="accordion-group"> 
                                                                <?php if(!empty($menuAccordion['menu'])) { ?>
                                                                        <div class="accordion-heading">
										<a href="#collapse-<?php echo $key ?>" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle collapsed">
											<?php echo $menuAccordion['icon'] . ' ' . $menuAccordion['label'] ?>
										</a>
									</div> 
                                                                        <?php 
                                                                            $add = '';
                                                                            if($key == $pr){
                                                                                    $add = 'in';
                                                                            }
                                                                        ?> 
                                                                        <div class="accordion-body collapse <?php echo $add; ?>" id="collapse-<?php echo $key ?>">
                                                                            <div class="accordion-inner">
                                                                                <ul class="nav nav-list">
                                                                                   <?php if (isset($menuAccordion['group'])) { ?>
                                                                                            <?php foreach ($menuAccordion['group']as $group) { 
                                                                                            	
                                                                                            	?>
                                                                                                <?php if ($group['label'] != '') { ?>
                                                                                                        <li class="nav-header"><?php echo $group['label']; ?></li>
                                                                                                <?php } ?>
                                                                                                <?php foreach($group['menus']as $menu) { ?>
                                                                                                        <?php if(!empty($menu)){ 
                                                                                                            $class = '';
                                                                                                            if(!empty($cat_id) && $menu['query'] == $cat_id){
                                                                                                                    $class = 'class="active"';
                                                                                                            }
                                                                                                        ?>
                                                                                                            <?php if(empty($menu['link'])){ ?>	
                                                                                                                    <li <?php echo $class ?>><a href="<?php echo $this->Portal->makeUrl($menu['controller'],$menu['action'],$menu['query'], $menu['param']); ?>"> - <?php echo $menu['label'] ?></a></li>
                                                                                                            <?php } else{ ?>
                                                                                                                    <li <?php echo $class ?>><a href="<?php echo $menu['link'] ?>" target="_blank"> - <?php echo $menu['label'] ?></a></li>
                                                                                                            <?php } ?>
                                                                                                        <?php } ?>
                                                                                                <?php } ?>
                                                                                            <?php } ?>    
                                                                                   <?php }else{ ?>
                                                                                            <?php foreach ($menuAccordion['menu']as $menu) { ?>
                                                                                                    <?php
                                                                                                    if(!empty($menu)){
                                                                                                             $class = '';
                                                                                                             if(!empty($cat_id) && $menu['query'] == $cat_id){
                                                                                                                            $class = 'class="active"';
                                                                                                             }
                                                                                                    ?>
                                                                                                    <?php if(empty($menu['link'])){ ?>	
                                                                                                                    <li <?php echo $class ?>><a href="<?php echo $this->Portal->makeUrl($menu['controller'],$menu['action'],$menu['query'], $menu['param']); ?>"> - <?php echo $menu['label'] ?></a></li>
                                                                                                            <?php } else{ ?>
                                                                                                                    <li <?php echo $class ?>><a href="<?php echo $menu['link'] ?>" target="_blank"> - <?php echo $menu['label'] ?></a></li>
                                                                                                            <?php } ?>

                                                                                            <?php 
                                                                                                    }
                                                                                                 } 

                                                                                            ?>                        
                                                                                   <?php } ?>
                                                                                </ul>    
                                                                            </div>
                                                                        </div>    
                                                                <?php }else{ ?>
                                                                       <?php if(!empty($menuAccordion)){ ?>
                                                                                <?php if(empty($menuAccordion['link'])){ ?>
                                                                                        <div class="accordion-heading">
                                                                                                <a href="#collapse-<?php echo $key ?>" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle collapsed" onclick="location.href='<?php echo $this->Portal->makeUrl($menuAccordion['controller'],$menuAccordion['action'],$menuAccordion['query'] ); ?>'">
                                                                                                        <?php echo $menuAccordion['label']; ?>
                                                                                                </a>
                                                                                        </div>
                                                                                <?php }else{ ?>
                                                                                        <div class="accordion-heading">
                                                                                                <a href="#collapse-<?php echo $key ?>" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle collapsed" 
                                                                                                	onclick="OpenInNewTab('<?php echo $menuAccordion['link']; ?>')">
                                                                                                        <?php echo $menuAccordion['label']; ?>
                                                                                                </a>
                                                                                        </div>
                                                                                <?php } ?>
                                                                       <?php } ?>
                                                                <?php }?>
                                                               </div>
							<?php } ?>
						<div class="push"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

 function addAppoint(day, month, year){
     var user = <?php echo $currentUser['AuthUser']['id']; ?>;
     var date = day+'/'+month+'/'+year;
     //alert(date+', '+month+'/'+year);
     $('#myModal').modal('show');
     $('#customModalHeader').html('New Appointment');
     $('#customModalAction').html('Close');
     $('#customModal').load("/appointments/addminicalendar/"+user,function(data) {
        $('#customModal').html(data);
        $('#fdate').val(date);
        $('#tdate').val(date);
     });
 }  

 function OpenInNewTab(url) {
  var win = window.open(url, '_blank');
  win.focus();
}
</script> 