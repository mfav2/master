	<!--[if lte IE 8]>
	<link rel="stylesheet" href="css/ie.css" />
	<script src="js/ie/html5.js"></script>
	<script src="js/ie/respond.min.js"></script>
	<script src="lib/flot/excanvas.min.js"></script>
	<![endif]-->

	<!--	<script>
	//* hide all elements & show preloader
	document.documentElement.className += 'js';
	</script>-->
	
       <script src="js/jquery.min.js"></script>
       <!-- smart resize event -->
        <script src="js/jquery.debouncedresize.min.js"></script>
        <!-- hidden elements width/height -->
        <script src="js/jquery.actual.min.js"></script>
        <!-- js cookie plugin -->
        <script src="js/jquery_cookie_min.js"></script>
        <!-- main bootstrap js -->
        <script src="bootstrap/js/bootstrap.js"></script>
        <!-- bootstrap plugins -->
        <script src="js/bootstrap.plugins.min.js"></script>
        <!-- tooltips -->
        <script src="lib/qtip2/jquery.qtip.min.js"></script>
        <!-- jBreadcrumbs -->
        <script src="lib/jBreadcrumbs/js/jquery.jBreadCrumb.1.1.min.js"></script>
        <!-- sticky messages -->
        <!--<script src="lib/sticky/sticky.min.js"></script>
        <script src="js/jquery.playSound.js"></script>
        <script src="lib/pnotify/jquery.pnotify.min.js"></script>
        <!-- fix for ios orientation change -->
        <!--<script src="js/ios-orientationchange-fix.js"></script>-->
        <!-- scrollbar -->
        <script src="lib/antiscroll/antiscroll.js"></script>
        <script src="lib/antiscroll/jquery-mousewheel.js"></script>
        <!-- common functions -->
        <script src="js/gebo_common.js"></script>
        <script src="js/event_common.js"></script>

        <!-- to top -->
        <!--<script src="lib/UItoTop/jquery.ui.totop.min.js"></script>-->
        <!-- jQuery UI -->
        <script src="lib/jquery-ui/jquery-ui-1.8.23.custom.min.js"></script>
        <!-- touch events for jQuery UI -->
        <script src="lib/jstree/jquery.jstree.js"></script>
        <!--<script src="js/forms/jquery.ui.touch-punch.min.js"></script>
        
        <script src="lib/colorbox/jquery.colorbox.min.js"></script>-->
        <!-- datepicker -->
        <script src="lib/datepicker/bootstrap-datepicker.min.js"></script>
        <!-- timepicker -->
        <script src="lib/datepicker/bootstrap-timepicker.min.js"></script>