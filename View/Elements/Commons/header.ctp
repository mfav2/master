	    <?php 
	    $currentUser = $this->Session->read('AuthUser');
	    $token_code = $currentUser['AuditToken']['token_code'];
	    $server_code = $currentUser['SystemServer']['server_code'];
	    $systemApplications = $currentUser['SystemApplication'];
	    
	    $application_name = Configure::read('Application.Name');
	    ?>
	    <!-- header -->
            <header>
                <div class="navbar navbar-fixed-top">
                    <div class="navbar-inner">
                        <div class="container-fluid">

                            <a class="brand" href="<?php echo $this->Portal->makeUrl('Dashboards','index');?>"><img alt="" src="img/logo/<?php echo $this->Portal->bannerLogo();?>"></a>
                            <ul class="nav user_menu pull-right">
                                <li class="hidden-phone hidden-tablet">
                                    <div id="notificationIcon" class="nb_boxes clearfix">
					
					<a id="myMail" class="label ttip_b" href="#myModal" data-backdrop="static" data-toggle="modal" title="New Mails"><span id="countMyMail"> 25 </span><i class="splashy-mail_light"></i></a>
       					<a id="myTask" class="label ttip_b" href="#myModal" data-backdrop="static" data-toggle="modal" title="New Tasks"><span id="countMyTask"> 10 </span><i class="splashy-calendar_week"></i></a>
					
                                    </div>
                                </li>

                                <li class="divider-vertical hidden-phone hidden-tablet"></li>
                                <li id="languageIcon" class="dropdown">
				    <div id="userProfile">
                                    <a href="#" class="dropdown-toggle nav_condensed" data-toggle="dropdown"><i class="flag-gb"></i> <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
					<li><a href="javascript:void(0)"><i class="flag-gb"></i> <?=__('English')?></a></li>
					<li><a href="javascript:void(0)"><i class="flag-th"></i> <?=__('Thai')?></a></li>
                                    </ul>
				    </div>
                                </li>
                                <li class="divider-vertical hidden-phone hidden-tablet"></li>
                                <li class="dropdown">
					
                                    <a href="#" class="dropdown-toggle userProfile" data-toggle="dropdown">
					    <span class="pull-right" style="margin:-5px 0 0px 0;" >
					    <img src="fileProviders/index/<?php echo $currentUser['AuthUserProfile'][0]['image_key'];?>" alt="" class="user_avatar"/>
					    
                                           <?php
                                                
                                                $position_name = '';
                                                $status = '';
                                                foreach($currentUser['AuthUserOrganizationPosition'] as $realpos){
                                                    if($realpos['is_real_position'] == 'Y'){
                                                        //pr($realpos['AuthOrganizationPosition']['AuthPosition']['position_name']);
                                                        if(!empty($realpos['AuthOrganizationPosition']['AuthPosition']['position_name'])){
                                                            $position_name  .= mb_substr(strip_tags($realpos['AuthOrganizationPosition']['AuthPosition']['position_name']),0, 23);
                                                            if (mb_strlen(strip_tags($realpos['AuthOrganizationPosition']['AuthPosition']['position_name'])) > 23) {
                                                                $position_name .='...';
                                                            }
                                                            
                                                        }
                                                    }
                                                    
                                                    //if($realpos == 'Y'){
                                                        
                                                        /*if(!empty($currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthPosition']['position_name'])){
                                                                $position_name  .= mb_substr(strip_tags($currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthPosition']['position_name']),0, 23);
                                                            if (mb_strlen(strip_tags($currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthPosition']['position_name'])) > 23) {
                                                                $position_name .='...';
                                                            }
                                                        }*/
                                                    //}
                                                }
                                                /*if($currentUser['AuthUserOrganizationPosition'][0]['is_real_position'] == 'Y'){
                                                    if(!empty($currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthPosition']['position_name'])){
                                                            $position_name  .= mb_substr(strip_tags($currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthPosition']['position_name']),0, 23);
                                                        if (mb_strlen(strip_tags($currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthPosition']['position_name'])) > 23) {
                                                            $position_name .='...';
                                                        }
                                                    }
                                                }*/
                                                    
                                                 $getstatus = $this->Profiles->checkstatus($currentUser['Appointment']);
												 //echo $getstatus;
												 //$status = $getstatus;
												 if(!empty($getstatus)){
													$status = mb_substr(strip_tags($getstatus),0, 23);
												 }else{
													if(!empty($currentUser['AuthUserProfile'][0]['responsibility'])){
                                                       $status  .= mb_substr(strip_tags($currentUser['AuthUserProfile'][0]['responsibility']),0, 23);
														if(mb_strlen(strip_tags($currentUser['AuthUserProfile'][0]['responsibility'])) > 23){
														   $status .= '...'; 
														   
														}
													}
												 }
                                                /**/
                                                
                                           ?>
					    </span>
					    <span class="pull-right" style="text-align:left;padding-right:10px">
					    <span class='span1'><b><?php echo __('Name');?></b></span><?php echo $currentUser['AuthUserProfile'][0]['first_name_th'] . ' ' . $currentUser['AuthUserProfile'][0]['last_name_th'];?><br>
					    <span class='span1'><b><?php echo __('Position');?></b></span><?php echo $position_name; ?><br>
					    <span class='span1'><b><?php echo __('Status');?></b></span><?php echo $status; ?>
					    <!--<span class='span1'><b><?php echo __('Status');?></b></span><?php echo !empty($currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthPosition']['position_name'])? $currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthPosition']['position_name']:'' ;?>-->
					    <!--<b class="caret"></b>-->
					    </span>			    
				    </a>

                                    <ul class="dropdown-menu">
					    <li><a id="myProfile" href="#myModal" data-backdrop="static" data-toggle="modal"><?=__('My Profile')?></a></li>
					    <li><a id="changePassword" href="#myModal" data-backdrop="static" data-toggle="modal"><?=__('Change Password')?></a></li>
					    <li><a id="notificationAlert" href="#myModal" data-backdrop="static" data-toggle="modal"><?=__('Set Notification')?></a></li>
					    <li class="divider"></li>
					    <li><a href="main/logout"><?=__('Log Out')?></a></li>
                                    </ul>
                                </li>
                            </ul>

                            <a data-target=".nav-collapse" data-toggle="collapse" class="btn_menu">
				<span class="icon-align-justify icon-white"></span>
                            </a>

                            <nav>

				<div class="icon">
					<div class="shortcutIcon">
						<div class="nav-collapse">
						    <ul class="nav">
							<!--
							<li class="dropdown">
							    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon-list-alt icon-white"></i> <b class="caret"></b></a>
							    <ul id="applicationIcon" class="dropdown-menu">
								<?php 
								foreach ($systemApplications as $systemApplication) {
								$applicationDomain = sprintf($systemApplication['SystemApplication']['application_domain'],$server_code);
								$applicationName = $systemApplication['SystemApplication']['application_name'];
								?>

								<li><a href="<?php echo $applicationDomain;?>/main/token/<?php echo $token_code;?>"><?php echo $applicationName;?></a></li>
				
								<?php }?>
			
							    </ul>
							</li>
							
							<li>
							    <a href="#"><i class="icon-book icon-white"></i></a>
							</li>
							
						
							<li>
							    <a href="widget/shortcut/word"><img src="img/icon/word.png"></a>
							</li>

							<li>
							    <a href="widget/shortcut/excel"><img src="img/icon/excel.png"></a>
							</li>

							<li>
							    <a href="widget/shortcut/powerpoint"><img src="img/icon/powerpoint.png"></a>
							</li>
							-->
						    </ul>
						</div>
					</div>
				</div>

                            </nav>
                        </div>
                    </div>
                </div>

               <div class="modal fade" id="myModal">
                    <div class="modal-header">
                        <button id="closechatbox" class="close" data-dismiss="modal">×</button>
                        <h3 id="customModalHeader"></h3>
                    </div>
		    <div class="modal-body">
			<div id="customModal">
			</div>
		    </div>
                    <div class="modal-footer">
                        <a id="customModalAction" data-dismiss="modal" href="javascript:void(0)" onclick='modalAction();' class="btn"><?=__('Next')?></a>
                    </div>
               </div>

            </header>