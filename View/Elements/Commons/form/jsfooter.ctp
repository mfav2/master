	
	<!-- smart resize event -->
	<script src="js/jquery.debouncedresize.min.js"></script>
	<!-- hidden elements width/height -->
	<script src="js/jquery.actual.min.js"></script>
	<!-- js cookie plugin -->
	<script src="js/jquery.cookie.min.js"></script>
	<!-- main bootstrap js -->
	<script src="bootstrap/js/bootstrap.min.js"></script>








	<!-- tooltips -->
	<script src="lib/qtip2/jquery.qtip.min.js"></script>
	<!-- jBreadcrumbs -->
	<script src="lib/jBreadcrumbs/js/jquery.jBreadCrumb.1.1.min.js"></script>
	<!-- sticky messages -->
	<script src="lib/sticky/sticky.min.js"></script>
	<!-- fix for ios orientation change -->
	<script src="js/ios-orientationchange-fix.js"></script>
	<!-- scrollbar -->
	<script src="lib/antiscroll/antiscroll.js"></script>
	<script src="lib/antiscroll/jquery-mousewheel.js"></script>
	<!-- lightbox -->
	<script src="lib/colorbox/jquery.colorbox.min.js"></script>
	<!-- common functions -->
	<script src="js/gebo_common.js"></script>
        <!-- datepicker -->
        <script src="lib/datepicker/bootstrap-datepicker.min.js"></script>
        <!-- timepicker -->
        <script src="lib/datepicker/bootstrap-timepicker.min.js"></script>
        <!-- TinyMce WYSIWG editor -->
        <script src="lib/tiny_mce/jquery.tinymce.js"></script>
        <!-- colorpicker -->
        <script src="lib/colorpicker/bootstrap-colorpicker.js"></script>
        <!-- password strength checker -->
        <script src="lib/complexify/jquery.complexify.min.js"></script>
        <!-- styled form elements -->
        <script src="lib/uniform/jquery.uniform.min.js"></script>
        <!-- datatable -->
        <script src="lib/datatables/jquery.dataTables.min.js"></script>
        <!-- additional sorting for datatables -->
        <script src="lib/datatables/jquery.dataTables.sorting.js"></script>      
        <!-- masked inputs -->
        <script src="js/forms/jquery.inputmask.min.js"></script>
	<!-- validation -->
	<script src="lib/validation/jquery.validate.min.js"></script>
	<!-- validation functions -->
	<script src="js/gebo_validation.js"></script>



        
        
	<script>
		$(document).ready(function() {
			//* show all elements & remove preloader
			setTimeout('$("html").removeClass("js")',1000);
		});
	</script>