<?php
	$array_contact = array('0'=>__('My Contacts'), '1'=>__('Shared Contacts'));
    //$array_contact = array('0'=>__('My Contacts'));
?>
<div class="row-fluid">
    <div class="span12">
        <div class="mbox">
            <div class="tabbable">
                <div class="heading">
                    <ul class="nav nav-tabs">
                        <?php
                        foreach ($TabView['panel'] as $panels) :
                            if (!empty($panels['active'])) {
                                $active = 'class="active"';
                            } else {
                                $active = "";
                            }
                            ?>
                            <li <?= $active ?>>
                                <a href="#<?= $panels['tabname'] ?>" data-toggle="tab">
                                    <i class="<?= $panels['class'] ?>"></i> <?= $panels['label'] ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
					</ul>
                </div>

                <div class="tab-content">
                    <?php foreach ($TabView['panel'] as $panels) { ?>
                        <?php
                        if (!empty($panels['active'])) {
                            $active = 'active';
                        } else {
                            $active = "";
                        }
                        ?>   
                        <div class="tab-pane <?= $active ?>" id="<?= $panels['tabname'] ?>">
                            <?php if ($panels['type'] == 'list') { ?> 
							<table width="100%">
							<tr><td align='right'>
							Search : <input type='text' id="searchContent"/>
							<?php
								 echo $this->form->input('contact_sharetype', array('type'=>'select', 'id'=>'contact_sharetype', 'label'=>false, 'div'=>false, 'options'=>$array_contact, 'default'=>'0', 'style'=>'width:auto;'));
							?>

							</td></tr></table>
                                <div id="searchContact">
                                    <table class="table table-bordered  table_vam" id="contacttable">
                                        <thead>
                                        <tr>
                                            <?php foreach ($panels['data'] as $data) { ?>
                                                <th<?php echo empty($data['width']) ? '' : ' style="width:' . $data['width'] . 'px"' ?>  class="sorting" 
                                                    ><?php echo $data['label']; ?></th>
                                            <?php } ?>  

                                            <th><?php echo _('Action');?></th>
                                        </tr>
                                        </thead>
                                        <tbody id="search_contact_result">
						<?php
						$phone = '';
						foreach ($contactall as $detail) { 
							if(!empty($detail['Contact']['phone1'])){
								$phone = $detail['Contact']['phone1'];
							}else{
								if(!empty($detail['Contact']['phone2'])){
									$phone = $detail['Contact']['phone2'];
								}else if(!empty($detail['Contact']['phone3'])){
									$phone = $detail['Contact']['phone3'];
								}else{
									$phone = $detail['Contact']['phone4'];
								}
							}
						?>
                                            <tr id="record<?php echo $detail['Contact']['id']; ?>">

                                                    <td><a href=<?php 
                                                    echo $this->Portal->makeUrl('Contacts', 'viewcontact', 'id=' . $detail['Contact']['id']); ?>''><?php echo $detail['Contact']['contact_name'] ?></a></td>
                                                    <td><?= $detail['Contact']['company'] ?></td>
                                                    <td><?= $detail['Contact']['job_title'] ?></td>
                                                    <td><?= $phone ?></td>
                                                    <td>
                                                        <?php 
                                                            if(empty($detail['Contact']['con_mfa'])){
                                                                $con_MFA = '';
                                                            }else{
                                                                $con_MFA = $detail['Contact']['con_mfa'];
                                                            } 
                                                            echo $con_MFA;    
                                                        ?>

                                                    </td>
                                                    <td><?= $detail['Contact']['share_type'] ?></td>

                                                    <td>
                                                        <a href=<?php echo $this->Portal->makeUrl('Contacts', 'editcontact', 'id=' . $detail['Contact']['id']); ?>''>
                                                            <i class="icon-pencil"></i></a>&nbsp&nbsp
														<a href="javascript:void(0);" onclick="checkdelete(<?php echo $detail['Contact']['id']; ?>);"><i class="icon-trash"></i></a>
	                                                        <!--<a class="contact_delete" href=<?php echo $this->Portal->makeUrl('Contacts', 'deletecontact', 'id=' . $detail['Contact']['id']); ?>''>
                                                            <i class="icon-trash"></i></a>-->
                                                    </td> 
                                            </tr>  
                                             <?php } ?>
                                        </body>    
                                    </table>
                                </div>    
                            
                                <?php
                            }
                            if ($panels['type'] == 'form') {
                                $templateMeta = $panels['templateMeta'];
                                $buttons = $templateMeta['form']['buttons'];
                                $panels1 = $panels['panels1'];
                                $panels2 = $panels['panels2'];
                                ?> 
                                <div class="btn-group sepH_b">
                                    <button data-toggle="dropdown" class="btn dropdown-toggle">Share <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                    <li>
       
                                    <!--<a class="share_all" id="share_all" style="cursor:pointer;" onclick="shareall()">
                                    <i class="splashy-add_small"></i><?=__('Share All')?></a>
                                    <a class="sharedep" id="sharedep" data-tableid="smpl_tbl" data-toggle="modal" data-backdrop="static" href="#myModal">
                                    <i class="splashy-add_small"></i><?=__('Share Department')?></a>
                                    <a class="notshare" id="notshare" style="cursor:pointer;" onclick="notshare()">-->
                                    <i class="splashy-add_small"></i><?=__('Not Share')?></a>
                                    </li>
                                    </ul>
                                    <input type="text" id="organi_name" style="margin:1px 0px 0px 5px; width:33.5%;" value="Not Share" />
                                </div>
                                
                                <?php
                                echo $this->Form->create($this->name, array('action' => $templateMeta['action'], 'class' => 'form-horizontal well','id'=>'addform','enctype'=>'multipart/form-data','novalidate'=>'novalidate'));
                                  ?>
                                <input type="hidden" id="organi_id" name="organization_id" value="" />
                                <input type="hidden" id="share_type" name="share_type" value="Not Share" />
                                <fieldset>
                                    <div class="span12">
                                        <div class="span7">
                                            <?php foreach ($panels1['data'] as $panel) { ?>
                                                <p class="f_legend">
                                                    <?php
                                                    echo $panel['label'];
                                                    if (!empty($panel['select'])) {
                                                        echo "&nbsp&nbsp" . $this->Form->select($panel['name'], $panel['option'], $panel['attr']);
                                                    }
                                                    ?>
                                                </p>
                                                <div class="row-fluid">     
                                                    <div class="control-group">

                                                        <?php foreach ($panel['detail'] as $detail) :?>
                                                            <div class="controls" style="margin:0px 0px 10px 0px" align="top">
                                                                <?php
                                                                if ($detail['label'] != false){
																	if($detail['name'] == 'title2'){
																		echo '<label class="control-label" id="title2">' . $detail["label"] . '&nbsp' . '</label>';
																	}else{
																		echo '<label class="control-label">' . $detail["label"] . '&nbsp' . '</label>';
																	}
																}
                                                                if ($detail['type'] == 'fileimage') {
                                                                    echo $this->Form->file($detail['name'], $detail['attr']);
                                                                    //echo '<div class="fileimage_require" align="center" style="color:#f00;"></div>';
                                                                }
                                                                elseif ($detail['type'] == 'text') {
                                                                    if (!empty($detail['select'])) {
                                                                        $tol = $detail['selectname'];
                                                                        $tol2 = $detail['name'];
                                                                        echo $this->Form->select($detail['name'] = $tol, $detail['option'], $detail['selectattr']);
                                                                        echo $this->Form->input($detail['name'] = $tol2, $detail['attr']);
                                                                        unset($tol, $tol2);

									if($detail['default_tel'] == "1"){
										echo "&nbsp;[".__('Default')."]";
									}

                                                                        if($detail['name'] == 'phone4'){
                                                                            echo "<div class='phone_require' style='color:#f00; text-align:center;'></div>";
                                                                        }
                                                                        
                                                                    } else {
                                                                        echo $this->Form->input($detail['name'], $detail['attr']);
                                                                        if($detail['name'] == 'contact_name'){
                                                                            echo "<div class='name_require' style='color:#f00; text-align:center;'></div>";
                                                                        }
                                                                    }
                                                                } elseif ($detail['type'] == 'select') {

                                                                    echo $this->Form->select($detail['name'], $detail['option'], $detail['attr']);
                                                                } elseif ($detail['type'] == 'textarea') {
                                                                    if (!empty($detail['select'])) {
                                                                        $tol = $detail['selectname'];
                                                                        $tol2 = $detail['name'];
                                                                        echo $this->Form->select($detail['name'] = $tol, $detail['option'], $detail['selectattr']);
                                                                        echo "<div style='margin:-33px 0px 0px 33%'>" . $this->Form->textarea($detail['name'] = $tol2, $detail['attr']) . "</div>";
                                                                    } else {
                                                                        echo $this->Form->textarea($detail['name'], $detail['attr']);
                                                                    }
                                                                } else {
                                                                    echo $this->Form->textarea($detail['name'], $detail['attr']);
                                                                }
                                                                ?>
                                                            </div>
                                                        <?php endforeach; ?>	
                                                    </div>
                                                </div>
                                            <?php } ?>
                                               
                                        </div>
                                      
                                        <div class="span5">
                                            <?php foreach ($panels2['data'] as $panel) { ?>
                                                <p class="f_legend">
                                                    <?php
                                                    echo $panel['label'] . "<br/>";
                                                    if (!empty($panel['select'])) {
                                                        echo "&nbsp&nbsp" . $this->Form->select($panel['name'], $panel['option'], $panel['attr']);
                                                    }
                                                    ?>
                                                </p>
                                                <div class="row-fluid">     
                                                    <div class="control-group">
                                                        <?php foreach ($panel['detail'] as $detail) : ?>
                                                            <div style="margin:0px 0px 10px 0px" align="top">
                                                                <?php
                                                                if ($detail['label'] != false)
                                                                    echo '<label class="control-label">' . $detail["label"] . '&nbsp' . '</label>';

                                                                if ($detail['type'] == 'fileimage') {
                                                                    //echo '<div align="center">'.$this->Form->file($detail['name'], $detail['attr']).'</div>';
                                                                    echo    
                                                                    '<div align="center">
                                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                            <div class="fileupload-preview thumbnail" style="width: 100px; height: 100px; line-height: 150px;"></div>
                                                                            <div>
                                                                               <input type="file" id="conpic" name="data[Contacts][conpic]">
                                                                            </div>
                                                                        </div>
                                                                    </div>';
                                                                    echo '<div align="center" style="font-size:12px;">'.__('Max File Size').' : 5 MB</div>';
                                                                    echo '<div class="fileimage_require" align="center" style="color:#f00;"></div>';
                                                                } elseif ($detail['type'] == 'text') {
                                                                    if (!empty($detail['select'])) {
                                                                        echo $this->Form->select($detail['name'], $detail['option'], $detail['selectattr']);
                                                                        echo $this->Form->input($detail['name'], $detail['attr']);
                                                                    } else {
                                                                        echo $this->Form->input($detail['name'], $detail['attr']);
                                                                        if($detail['name'] == 'email1'){
                                                                            echo "<div class='mail1_require' style='color:#f00; text-align:center;'></div>";
                                                                        }else if($detail['name'] == 'email2'){
                                                                            echo "<div class='mail2_require' style='color:#f00; text-align:center;'></div>";
                                                                        }
                                                                    }
                                                                } elseif ($detail['type'] == 'select') {

                                                                    echo $this->Form->select($detail['name'], $detail['option'], $detail['attr']);
                                                                } elseif ($detail['type'] == 'textarea') {
                                                                    if (!empty($detail['select'])) {
                                                                        echo $this->Form->select($detail['name'], $detail['option'], $detail['selectattr']);
                                                                        echo $this->Form->textarea($detail['name'], $detail['attr']);
                                                                    } else {
                                                                        echo $this->Form->textarea($detail['name'], $detail['attr']);
                                                                    }
                                                                } else {
                                                                    echo $this->Form->textarea($detail['name'], $detail['attr']);
                                                                }
                                                                ?>
                                                            </div>
                                                        <?php endforeach; ?>	
                                                    </div>
                                                </div>
                                            <?php } ?>
                                                   <div align="center" class="control-group">
                                         <span id="err" style="color:#F00;"></span>
                                     </div>
                                    <div align="center" class="control-group">
                                        <?php
                                        foreach ($buttons as $button) {
                                            echo $this->Form->button(__($button, true), array('class' => 'btn','id'=>'buten'));
                                            echo ' ';
                                        }
                                        ?>
                                    </div>
                                        </div>
                                    </div>
                                  
                                </fieldset>
                                <?php
                                echo $this->Form->end();
                               
                            }
                            ?>
                        </div>
                    <?php } ?>    
                </div>
            </div>
        </div>

    </div>
</div>

<!-- hide elements -->
<div class="hide">
    <!-- actions for inbox -->
    <div class="dt_inbox_actions">
        <div class="btn-group">
            <a href="javascript:void(0)" class="btn" title="Answer"><i class="icon-pencil"></i></a>
            <a href="javascript:void(0)" class="btn" title="Forward"><i class="icon-share-alt"></i></a>
            <a href="#" class="delete_msg btn" title="Delete" data-tableid="dt_inbox"><i class="icon-trash"></i></a>
        </div>
    </div>
    <!-- actions for outbox -->
    <div class="dt_outbox_actions">
        <div class="btn-group">
            <a href="javascript:void(0)" class="btn" title="Resend"><i class="icon-share-alt"></i></a>
            <a href="#" class="delete_msg btn" title="Delete" data-tableid="dt_outbox"><i class="icon-trash"></i></a>
        </div>
    </div>
    <!-- actions for trash -->
    <div class="dt_trash_actions">
        <div class="btn-group">
            <a href="#" class="delete_msg btn" title="Delete permamently" data-tableid="dt_trash"><i class="icon-trash"></i></a>
        </div>
    </div>
    <!-- confirmation box -->
    <div id="confirm_dialog" class="cbox_content">
        <div class="sepH_c tac"><strong>Are you sure you want to delete this message(s)?</strong></div>
        <div class="tac">
            <a href="#" class="btn btn-gebo confirm_yes">Yes</a>
            <a href="#" class="btn confirm_no">No</a>
        </div>
    </div>
</div>

<script src="js/custom/contact_index_edit.js"> </script>

<script type="text/javascript">
function checkdelete(id){
   
   if(confirm('<?php echo __("Do you want to delete this contact"); ?> ? ')){
        $.ajax({
          cache: false,
          url: "contacts/deletecontact",
          type: "post",
          data: {'id':id},
          success:function(data){
			  if(data == 1){
                  //location.reload();
				  $('#record'+id).remove();
              }else{
                  alert(data);
              }
          }
        });    
    }else{
        return false;   
    }
}
</script>

