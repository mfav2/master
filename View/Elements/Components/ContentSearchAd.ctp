<script>
    $(document).ready(function() {
        $('#category_id').change(function() {
<?php
// this function for hide or show <option> of sub_category
foreach ($categories as $category) :
    if ($category['SubCategory'] != null) :
        ?>
                    if ($(this).val() == <?= $category['Category']['id']; ?>) {
                        $(<?= '\'#span_category_' . $category['Category']['id'] . '\''; ?>).show();
                    } else {
                        $(<?= '\'#span_category_' . $category['Category']['id'] . '\''; ?>).hide();
                    }
        <?php
    endif;
endforeach;
?>
        });
    });
</script>
<div style="text-align: right">
    <form action="<?php echo $this->Portal->makeUrl('Contents', 'search'); ?>" class="form-inline" method="post">
        Category: 
        <select id="category_id" style="width: 200px" name="data[category_id]">
            <option value="0">All Category</option>
            <?php foreach ($categories as $category) : ?>
                <option value="<?= $category['Category']['id'] ?>" <?php if ($category['Category']['id'] == $this->request->data['category_id']) {
                echo 'selected';
            } ?>>
                <?= $category['Category']['category_name'] ?>
                </option>
        <?php endforeach; ?>
        </select>
        <?php
        // draw <option> if any sub_category
        foreach ($categories as $category) {
            if ($category['SubCategory'] != null) {
                echo '<span id="span_category_' . $category['Category']['id'] . '" style="display: none;"><br><br>';
                echo 'Subcategory: <select id="sub_category_id_' . $category['Category']['id'] . '" style="width: 200px;" name="data[sub_category_id_' . $category['Category']['id'] . ']" >';
                echo '<option value="0">All Sub Categories</option>';
                foreach ($category['SubCategory'] as $sub_category) {
                    echo '<option value="' . $sub_category['Category']['id'] . '">' . $sub_category['Category']['category_name'] . '</option>';
                }
                echo '</select>';
                echo '</span>';
            }
        }
        echo ' ';
        echo ' Period: ';
        echo $this->Form->input('period', array(
            'type' => 'select',
            'label' => false,
            'div' => false,
            'style' => 'width:111px',
            'options' => $periods,
        ));
        echo ' Sort: ';
        echo $this->Form->input('sort', array(
            'type' => 'select',
            'label' => false,
            'div' => false,
            'style' => 'width:111px',
            'options' => $sorts,
        ));
        echo ' ';
        echo $this->Form->input('keyword', array('placeholder' => 'keyword', 'label' => false, 'div'=>FALSE));
        echo ' ';
        echo $this->Form->button('Search', array('class' => 'btn', 'div'=>FALSE));
        ?>
    </form>
</div>