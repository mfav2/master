<?php
$templateMeta = $viewDefs[$this->name]['DetailView']['templateMeta'];
$buttons = $templateMeta['form']['buttons'];
$panel1 = $viewDefs[$this->name]['DetailView']['panel1'];
$panel2 = $viewDefs[$this->name]['DetailView']['panel2'];
?>    
<form class="form-horizontal well">
<div class="row-fluid">
<div class="span12">
<div class="span7">
		<?php foreach ($panel1 as $panel) { ?>
		<h3 class="heading"><?php echo $panel['label'] ?></h3>
			
				<?php foreach ($panel['data'] as $panelDatas) { ?>
				<!--<div class="formSep">-->
				<div class="control-group">
					<div class="row-fluid">
						<?php foreach ($panelDatas as $panelData) { ?>
						<div class="span12">
							<label class="control-label" ><span style="font-family: Tahoma; "><strong><?php echo $panelData['label'] ?></strong></span></label>
							<div class="controls text_line">
								<span style="font-family: Tahoma; margin:100px 0px 0px 50px; "><?php echo empty($viewDatas[$panelData['name']]) ? '' : $viewDatas[$panelData['name']] ?></span>
							</div>
						</div>
						<?php }?>
					</div>
                                                           </div>
				<!--</div> </div>-->
				<?php }?>
			
		<?php }?>
        </div>
        <div class="span5">
		<?php foreach ($panel2 as $panel) { ?>
		<h3 class="heading"><?php echo $panel['label'] ?></h3>
			
				<?php foreach ($panel['data'] as $panelDatas) { ?>
				<div class="formSep">
				<div class="control-group">
					<div class="row-fluid">
						<?php foreach ($panelDatas as $panelData) { ?>
						<div class="span12">
							<!--<label class="control-label" ><span style="font-family: Tahoma; "><?php echo $panelData['label'] ?></span></label>-->
							<!--<div class="controls text_line">-->
								<span style="font-family: Tahoma; margin:0px 0px 0px 0px; "><?php echo empty($viewDatas[$panelData['name']]) ? '' : $viewDatas[$panelData['name']] ?></span>
							<!--</div>-->
						</div>
						<?php }?>
					</div>
				</div></div>
				<?php }?>
			
		<?php }?>
<?php //pr($contact); ?>
        </div>
	</div>
    </div>
</form>
