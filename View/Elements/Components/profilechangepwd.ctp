<div id="changePwdDiv">
    <fieldset>
        <div class="modal-header">
            <button class="close" data-dismiss="modal">x</button>
            <h3>Change Password</h3>
        </div>
        <div class="modal-body">
            <label>Old Password:</label>
            <input type="text">
            <div class="row-fluid">
                <div class="span6">
                    <label>New Password:</label>
                    <input type="text">
                </div>
                <div class="span6">
                    <label>Re-new Password:</label>
                    <input type="text">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="row-fluid">
                <div class="span6" style="text-align: left">
                    <button class="btn" type="submit">Change Password</button>
                    <span class="help-inline"><a id="b2profile" href="#">Back to My Profile</a></span>
                </div>
                <div class="span6" sytle="text-align: right">
                    <a class="btn" data-dismiss="modal" href="#">Close</a>
                </div>
            </div>
        </div>
    </fieldset>
</div>
<script>
    $('#b2profile').click(function(){
        $('#box').load('/profiles/myprofile');
    })
</script>