<?php 
    $currentUser = $this->Session->read('AuthUser');   
?>

<div id="searchcontent">
 <?php 
      foreach($categories as $category){
            echo '<div style="margin:15px 0px; color:#006bd6;"><h4> - '.$category['SubCategory']['description'].'</h4></div>';  
		if(!empty($contents)){
			foreach($contents as $content){
				$org_desc = '';
				$org_belong = '';
				$showdiv = '';
					if(!empty($content['ShowListContent']['organization_description'])){
						$org_desc  = $content['ShowListContent']['organization_description']['description'];
					}
					if(!empty($content['ShowListContent']['organization_belong'])){
						$org_belong = '&nbsp;-&nbsp;'.$content['ShowListContent']['organization_belong'];
					}

					if(!empty($org_desc) && !empty($org_belong)){
						$showdiv = '['.$org_desc.$org_belong.']';
					}
				if($content['ContentCategory'][0]['sub_category_id'] == $category['getCategory']['id']){ ?>	
				
				<!-- Data Show -->
                        <div class="prettyprint " >
                                      
                                      <?php echo $this->Time->format('d M y', $content['ShowListContent']['event_date']).'&nbsp;&nbsp;&nbsp;'; ?>
                                        <a class="pop_over"  data-content="<?php echo $content['ShowListContent']['gist']; ?>" data-placement="bottom" href="<?= $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $content['ShowListContent']['id']); ?>">
                                            <?php 
                                                echo empty($content['ShowListContent']['title']) ? __('No title') : mb_substr(strip_tags(htmlspecialchars_decode($content['ShowListContent']['title'])), 0, 256, 'UTF-8');
                                            ?>
                                        </a>
                                            <?php 
                                             $first = 1;
                                             $count = count($content['PortalAttachment']);
                                             if($count != 0){
                                              echo ' - ';  
                                                 foreach($content['PortalAttachment'] as $attachment){

                                                     if($first == $count){

                                                         echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($attachment['ResourceContent']['key']) . '"><i class="icon-adt_atach"></i></a>';
                                                         break;
                                                     }else{
                                                         echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($attachment['ResourceContent']['key']) . '"><i class="icon-adt_atach"></i></a>';
                                                     }
                                                     $first++;
                                                 }
                                             }
                                             ?>
                                          <?php if($currentUser['AuthUser']['id'] == $content['ShowListContent']['user_id']){ ?>
                                           <span class="pull-right">
                                               <a href="<?= $this->Portal->makeUrl($this->name, 'edit', 'content_id=' . $content['ShowListContent']['id'] . '&category_id=' . $this->request->query['category_id']); ?>">
                                                   <i class='icon-pencil'></i></a>  | <a href="<?= $this->Portal->makeUrl($this->name, 'delete', 'content_id=' . $content['ShowListContent']['id']. '&category_id='.$this->request->query['category_id']); ?>" onclick="return confirm('<?=__('Confirm to delete')?> ?')"><i class='icon-trash'></i></a>
                                           </span>
                                       <?php } ?> 
                                    <br />
                                     <span title="<?= $this->Time->timeAgoInWords($content['ShowListContent']['created_date']) ?>">
                                        <a id="userProfile" data-toggle="modal" data-backdrop="static" href="#myModal" user_id="<?= $content['ShowListContent']['user_id'] ?>" style="color:black; font-size: smaller">
                                            <?= $content['ShowListContent']['content_updated_user']; ?>
                                        </a>
                                            &nbsp;<?php echo "<span style='font-size:smaller'>" . $showdiv; ?>
                                            - <?= $this->Time->format('Y-m-d H:i:s', $content['ShowListContent']['created_date'])  . "</span>" ?>
                                    </span>
                                     <?php if ($content['ShowListContent']['is_comment'] == 'Y' && $content['ShowListContent']['count_comment'] > 0) : ?>
                                     <span title="<?php
                                                    if (isset($content['CommentUserProfile']['UserProfile'])) {
                                                        foreach ($content['CommentUserProfile']['UserProfile'] as $i => $user) {
                                                            echo $user['UserProfile']['first_name_th'] . ' ' . $user['UserProfile']['last_name_th'];
                                                            if (isset($content['CommentUserProfile']['UserProfile'][$i + 1])) {
                                                                echo '&#13;';
                                                            }
                                                        }
                                                    }
                                                    ?>">
                                           <?php '<i class="splashy-comments"></i> ' . $content['ShowListContent']['count_comment']; ?>
                                         <span class="label label-info"><?= $content['ShowListContent']['count_comment'] ?> <?=__('Comments')?></span> 
                                     </span>
                                       <?php endif; ?>
                                    <span class="label label-info"><?= $content['ShowListContent']['count_view'] ?> <?=__('Views')?></span> 
                        </div>
                        <!-- End Data Show -->
<?php           
              }
					
			}
		}
            
      }  
?>   
</div>



<script>
    $('#reversion').click(function() {
        $('#box').load('/articles/reversion/');
    });
    
    $('#submitSearch').click(function() {
        var values = $('#searchContent').serialize();
        
         $.ajax({
                  cache: false,
                  url: "/Contents/search?_="+Math.random(),
                  type: "post",
				  data : values,
				  success:function(data){
					$('#searchcontent').empty().append(data);
				  }
				  	
		});	  
        /*$.get(
                '/Contents/search',
                values,
                function(data){
                    $('#searchcontent').empty().append(data);
                    //$('#values').val(values);
                },
                'html'
        );*/
    });
    
    $('input#keyword').keypress(function(event) {
        // detect ENTER key
        if (event.keyCode == 13) {
            // simulate submit button click
            //$("#btn-submit").click();
            // stop form from submitting via ENTER key press
            event.preventDefault ? event.preventDefault() : event.returnValue = false;
        }
    });
    
</script>


