<div class="row-fluid">
        <div class="span12">
                <h3 class="heading"><?=$topic['panel']['label']?>s</h3>
                <?php echo $this->Form->create('Tests', array('type'=>'file', 'url'=>'/'.$this->Portal->makeUrl('tests', 'testdel'))); ?>
                <table class="table table-striped table-bordered table-condensed">
                        <thead>
                                <tr>                                       
                                        <?php foreach ($listViewDefs as $listViewDef) { ?>
                                        <th<?php echo empty($listViewDef['width']) ? '' : ' style="width:'.$listViewDef['width'].'px"'?>><?php echo $listViewDef['label'];?></th>
                                        <?php }?>
                                </tr>
                        </thead>
                        <tbody>
                            <?php foreach($listViewDatas as $listViewData) { ?> 
                            <tr>
                                <?php foreach ($listViewDefs as $key=> $listViewDef) { ?>
                                    <?php if($key != "manage"){ ?>
                                               <td><?php echo $listViewData[$this->name][$key]?></td>
                                    <?php }else{ ?>  
                                                <td>

                                                        <a class="link_edit" href="<?=$this->Portal->makeUrl('Menu','adminAdd','parent_id='.$listViewData[$this->name]['manage'])?>"><i class="icon-plus"></i></a> | 
                                                        <a class="link_edit" href="<?=$this->Portal->makeUrl('Menu','adminEdit','id='.$listViewData[$this->name]['manage'])?>"><i class="icon-pencil"></i></a> |
                                                        <a class="link_edit" href="<?=$this->Portal->makeUrl('Menu','adminUp','id='.$listViewData[$this->name]['manage'])?>"><i class="icon-arrow-up"></i></a> | 
                                                        <a class="link_edit" href="<?=$this->Portal->makeUrl('Menu','adminDown','id='.$listViewData[$this->name]['manage'])?>"><i class="icon-arrow-down"></i></a> | 
                                                        <a class="link_edit" href="<?=$this->Portal->makeUrl('Menu','adminDelete','id='.$listViewData[$this->name]['manage'])?>"><i class="icon-trash"></i></a>
                                                </td>
                                    <?php } ?>
                               <?php } ?>
                            </tr>
                            <?php } ?>
                                <div class="hide">
                                        <!-- actions for datatables -->
                                        <div class="dt_gal_actions">
                                                <div class="btn-group">
                                                        <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                                <li><a href="javascript:void(0)"><i class="icon-pencil"></i> Add</a></li>
                                                                <li><a href="#" class="delete_rows_dt" data-tableid="dt_gal"><i class="icon-trash"></i> Delete</a></li>

                                                        </ul>
                                                </div>
                                        </div>
                                        <!-- confirmation box -->
                                        <div id="confirm_dialog" class="cbox_content">
                                                <div class="sepH_c tac"><strong>Are you sure you want to delete this row(s)?</strong></div>
                                                <div class="tac">
                                                        <a href="#" class="btn btn-gebo confirm_yes" >Yes</a>
                                                        <a href="#" class="btn confirm_no">No</a>
                                                </div>
                                        </div>
                                </div>    
                      
                        
                            
                        </tbody>
                </table>
  <?php echo $this->Form->end(); ?>
        </div>
</div>

<!-- hide elements (for later use) -->

   

   
