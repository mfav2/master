<?php
$templateMeta = $viewDefs[$this->name]['EditView']['templateMeta'];
$buttons = $templateMeta['form']['buttons'];
$panels = $viewDefs[$this->name]['EditView']['panels'];
//pr($this->query);
?>   
<div class="row-fluid">
    <div class="span12">
        <?php echo $this->Form->create($this->name, array('action' => $templateMeta['action'], 'class' => 'form-horizontal well')); ?>
        <fieldset>
            <?php foreach ($panels as $panel) { ?>
                <!--<p class="f_legend"><?php echo $panel['label'] ?></p>-->
                <?php foreach ($panel['data'] as $panelDatas) { ?>
                    <div class="control-group">
                        <div class="row-fluid">
                            <div class="span12">
                                <?php foreach ($panelDatas as $panelData) { ?>

                                    <div class="control-group formSep">
                                        <label class="control-label"><?php echo $panelData['label'] ?></label>
                                        <div class="controls">
                                            <?php
                                            if ($panelData['type'] =='button') {
                                                echo "<div " . $panelData['div'] . ">";
                                                echo $this->Form->input($panelData['label'], $panelData['type']);
                                                echo '<span class="add-on"><i class="' . $panelData['icon'] . '"></i></span>';
                                                echo "</div>";
                                            } else if ($panelData['type'] =='radio') {

                                                echo $this->Form->radio($panelData['name'], $panelData['options'], $panelData['attr']);
                                            } else if ($panelData['type'] =='select') {
                                                echo $this->Form->select($panelData['selectlabel'], $panelData['select'], $panelData['selectattr']);
                                            } else {
                                                echo $this->Form->input($panelData['label'], $panelData['type']);
                                            }
                                            ?>    
                                        </div>
                                    </div>
        <?php } ?>

                            </div>    
                        </div>
                    </div>   
    <?php } ?>
            <?php } ?>
            <div class="control-group">
                <div class="controls">
                    <!--<button class="btn" type="submit">Form button</button>-->
<?php
foreach ($buttons as $button) {
    echo $this->Form->button(__($button, true), array('class' => 'btn'));
    echo ' ';
}
?>
                </div>
            </div>
        </fieldset>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<script src="js/custom/p_forms.js"></script>  




<?php
//$templateMeta = $viewDefs[$this->name]['EditView']['templateMeta'];
//$buttons = $templateMeta['form']['buttons'];
//$panels = $viewDefs[$this->name]['EditView']['panels'];
//pr($this->query);
?>   
<!--    <div class="row-fluid">
    <div class="span12">
<?php echo $this->Form->create($this->name, array('action' => $templateMeta['action'], 'class' => 'form-horizontal well')); ?>
                        <fieldset>
<?php foreach ($panels as $panel) { ?>
                                <p class="f_legend"><?php echo $panel['label'] ?></p>
    <?php foreach ($panel['data'] as $panelDatas) { ?>
                                            <div class="control-group">
                                                <div class="row-fluid">
                                                     <div class="span12">
        <?php foreach ($panelDatas as $panelData) { ?>
                                                               
                                                                    <div class="control-group formSep">
                                                                        <label class="control-label"><?php echo $panelData['label'] ?></label>
                                                                            <div class="controls">
            <?php
            if (!empty($panelData['button'])) {
                echo "<div " . $panelData['div'] . ">";
                echo $this->Form->input($panelData['label'], $panelData['type']);
                echo '<span class="add-on"><i class="' . $panelData['icon'] . '"></i></span>';
                echo "</div>";
            } else if (!empty($panelData['radio'])) {

                echo $this->Form->radio($panelData['name'], $panelData['options'], $panelData['attr']);
            } else if (!empty($panelData['select'])) {
                echo $this->Form->select($panelData['selectlabel'], $panelData['select'], $panelData['selectattr']);
            } else {
                echo $this->Form->input($panelData['label'], $panelData['type']);
            }
            ?>    
                                                
                                                                                    
                                            </div>
                                                                    </div>
        <?php } ?>
                                                         
                                                       </div>    
                                                </div>
                                            </div>   
    <?php } ?>
<?php } ?>
                            <div class="control-group">
                                    <div class="controls">
                                            <button class="btn" type="submit">Form button</button>
<?php
foreach ($buttons as $button) {
    echo $this->Form->button(__($button, true), array('class' => 'btn'));
    echo ' ';
}
?>
                                    </div>
                            </div>
                        </fieldset>
<?php echo $this->Form->end(); ?>
    </div>
    </div>-->
<!--<script src="js/custom/p_forms.js"></script>-->  

