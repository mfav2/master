<link rel="stylesheet" href="lib/tag_handler/css/jquery.taghandler.css" />
<!--<script src="js/custom/custom_forms.js"></script>-->
<?php
//<script src="js/custom/event/Content.js"></script>
/*$olderTagList = '';
if (isset($ContentTag) && $ContentTag != null) {
    foreach ($ContentTag as $i => $tag) {
        $olderTagList .= '\'' . $tag['tag_name'] . '\'';
        if (isset($ContentTag[$i + 1])) {
            $olderTagList .= ',';
        }
    }
}
$availableTagList = '';
if (isset($availableTag) && $availableTag != null) {
    foreach ($availableTag as $i => $tag) {
        $availableTagList .= '\'' . $tag['ContentTag']['tag_name'] . '\'';
        if (isset($availableTag[$i + 1])) {
            $availableTagList .= ',';
        }
    }
}*/


$showlist  = '';
if (isset($OldUserReader) && $OldUserReader != null) {
    foreach ($OldUserReader as $i => $reader) {
           $showlist .= '<li itemtype="user" user_id="'.$reader['UserProfile']['user_id'].'" class="tagItem">'.$reader['UserProfile']['first_name_th'].' '.$reader['UserProfile']['last_name_th'].'</li>';
    }
}
if (isset($OldOrgReader) && $OldOrgReader != null) {
    foreach ($OldOrgReader as $i => $reader) {
        $showlist .= '<li itemtype="orgs" org_id="'.$reader['OrganizationDescription']['organization_id'].'" class="tagItem">'.$reader['OrganizationDescription']['description'].'</li>'; 
    }
}
?>
<script>
    
$(document).ready(function() {  
//        $("form").each(function() {
//            $(this).validate();
				//$('#array_content_users').hide(); 
//        });
        if($('#ContentNotificationTypeA').attr("checked")=="checked"){ 
           $('#array_content_users').hide(); 
        }
        /*$('#array_content_tags').tagHandler({
            assignedTags: [<?= $olderTagList; ?>],
            availableTags: [<?= $availableTagList ?>],
            autocomplete: true
        });*/
        
        $('#array_content_users').tagHandler();
        $("ul#array_content_users li.tagInput").hide();
        $("ul#array_content_users").prepend('<?=$showlist?>');
        
       
                if($('#ContentIsNotification').attr("checked")=="checked"){ 
                    $("#ContentIsNotification_").val('1');
                    $("#ContentIsNotification").val('1');
                    $("#ContentIsNotification").attr("checked","checked");
                }else{ 
                    $("#ContentIsNotification_").val('0');
                    $("#ContentIsNotification").val('0');
                    $("#ContentIsNotification").removeAttr("checked","checked");
                }
          
          $('#ContentIsNotification').click(function(){
                if($(this).attr("checked")=="checked"){ 
                    $("#ContentIsNotification_").val('1');
                    $("#ContentIsNotification").val('1');
                    $("#ContentIsNotification").attr("checked","checked");
                }else{ 
                    $("#ContentIsNotification_").val('0');
                    $("#ContentIsNotification").val('0');
                    $("#ContentIsNotification").removeAttr("checked","checked");
                }
          });
//        
//        $('#ContentNotificationTypeD').click(function(){
//            $("ul#array_content_users").empty();
//        });
//        $('#ContentNotificationTypeC').click(function(){
//            $("ul#array_content_users").empty();
//        });
        
            $('#ContentAttachment1').bind('change', function() {
                $('.attach1-error-type').empty();
                $('.attachment1-error-block').css('display','none');
                 err = checkfilesize(this.files[0].size);
                 if(err == false){
                     $('.attachment1-error-block').css('display','block');
                 }
            });
            $('#ContentAttachment2').bind('change', function() {
                 $('.attach2-error-type').empty();
                 $('.attachment2-error-block').css('display','none');
                 err = checkfilesize(this.files[0].size);  
                 if(err == false){
                     $('.attachment2-error-block').css('display','block');
                 }
            });
            $('#ContentAttachment3').bind('change', function() {
                $('.attach3-error-type').empty();
                $('.attachment3-error-block').css('display','none');
                err = checkfilesize(this.files[0].size);
                if(err == false){
                     $('.attachment3-error-block').css('display','block');
                 }
            });
			
            $('#ContentNotificationTypeA').click(function() {
                    $('#array_content_users').hide(); 
                    //$('#array_content_users').attr("disabled", true);     
            });

            $('#ContentNotificationTypeD').click(function() {
                    $('#array_content_users').show(); 
                    //$('#array_content_users').attr("disabled", true);     
            });

            $('#ContentNotificationTypeC').click(function() {
                    $('#array_content_users').show(); 
                    //$('#array_content_users').attr("disabled", true);     
            });




        $('button#submitForm').click(function() {
            var err = false;
            $('.subcategory-error').empty();
            $('.subject-error').empty();
            $('.detail-error').empty();
            $('.gist-error').empty();
            $('.attach1-error-type').empty();
            $('.attach2-error-type').empty();
            $('.attach3-error-type').empty();
            $('.error-choose-user').empty();
            $('input[id=reader]').remove();
            
            var txt = $("#ContentTitle").val();
            var detail = $("#ContentDescription").val();
            var gist = $("#ContentGist").val();
            var chkselected = $('select[id="ContentSubCategoryId"] option:selected').text();
            /******************* check type file *********************/
            var file1 = $('#ContentAttachment1').val();
            var file2 = $('#ContentAttachment2').val();
            var file3 = $('#ContentAttachment3').val();
            var noti_all = $('#ContentNotificationTypeA:checked').val();

                           // first check if file field has any value
            if(file1 != ''){
                var filetype1 = checktypefile(file1);         
                if(filetype1 == 'N'){
                    $('.attach1-error-type').text('<?php echo __("Only file"); ?> doc, docx, txt, pdf, jpg, jpeg, png, gif, xls, xlsx, ppt, pptx, bmp, zip, swf');
                    err = true;
                }
            }
            if(file2 != ''){
                var filetype2 = checktypefile(file2);         
                if(filetype2 == 'N'){
                    $('.attach2-error-type').text('<?php echo __("Only file"); ?> doc, docx, txt, pdf, jpg, jpeg, png, gif, xls, xlsx, ppt, pptx, bmp, zip, swf');
                    err = true;
                }
            }
            if(file3 != ''){
                var filetype3 = checktypefile(file3);         
                if(filetype3 == 'N'){
                    $('.attach3-error-type').text('<?php echo __("Only file"); ?> doc, docx, txt, pdf, jpg, jpeg, png, gif, xls, xlsx, ppt, pptx, bmp, zip, swf');
                    err = true;
                }
            }
                
            
            
//               var exts = ['doc','docx','rtf','odt'];
//               // first check if file field has any value
//               
            /*********************************************************/
           

            if(chkselected == 'N/A'){
                $('.subcategory-error').text('<?=__('Please Choose Sub Category')?>.');  
                err = true;
            }
            if(txt == ''){
                 $('.subject-error').text(' <?=__('Please Insert Title')?>.');  
                err = true;
            }
            if(detail == ''){
                $('.detail-error').text(' <?=__('Please Insert Detail')?>.');
                err = true;
            }
            if(gist == ''){
                $('.gist-error').text(' <?=__('Please Insert Description')?>.');
                err = true;
            }


            var i = 0;
            /*$('ul#array_content_tags li.tagItem').each(function() {
                $('ul#array_content_tags').after('<input type="hidden" id="tags" name="data[Tag][' + i++ + ']" value="' + $(this).text() + '" />');
            });*/

            //i = 0;
            
              
          
            if(noti_all != 'A'){
                
                var l = $('ul#array_content_users li.tagItem').length;
                if(l == 0 ){
                    $('.error-choose-user').text(' <?php echo __('Please Insert Reader'); ?>');
                    err = true;
                }else{
                    
                    $('ul#array_content_users li.tagItem').each(function() {
                        
                        var type = $(this).attr('itemtype');
                        if(type=="orgs"){
                        $('ul#array_content_users').after('<input type="hidden" id="reader" name="data[Invite][Orgs][' + i++ + ']" value="' + $(this).attr('org_id') + '" />');    
                        }else if(type=="user"){
                        $('ul#array_content_users').after('<input type="hidden" id="reader" name="data[Invite][Users][' + i++ + ']" value="' + $(this).attr('user_id') + '" />');
                        }

                    });
                    
                }
            }
          
            if(err == true){
                return false;
            }else{
                this.form.submit();
            }
           
            //this.form.submit();
            return false;
        });
});
 function checkfilesize(filesize){
    var chk ='';
    var maxsize = '<?php echo Configure::read('Config.MaxFileSize'); ?>';
                    if(filesize > maxsize){
                        chk = false;
                    }else{
                        chk = true;
                    }
                    
    return  chk;                
 }   
 
 function deleteattach(id,conid,catid){
     //alert(id+' '+conid+ ' '+catid);
              if(confirm('<?php echo __("Do you want to delete this files?"); ?> ')){
                    //window.location.href="/appointments/insertappointment"
              
                  $.ajax({
                    cache: false,
                    url: "/contents/deleteAttachment",
                    type: "post",
                    data: {'id':id, 'content_id':conid, 'category_id':catid },
                    success:function(data){
                        if(data == 1){
                            location.reload();
                        }else{
                            alert(data);
                        }
                    }
                  });    
              }else{
                  return false;   
              }
 }
 
 function checktypefile(file){
            var exts = ['doc', 'docx', 'txt', 'pdf', 'jpg', 'jpeg', 'png', 'gif', 
                        'xls', 'xlsx', 'ppt', 'pptx', 'bmp','zip', 'rar', '7z', 'swf'];
            if (file) {
                 // split file name at dot
                 var get_ext = file.split('.');
                 // reverse name to check extension
                 get_ext = get_ext.reverse();
                 var chktype = $.inArray ( get_ext[0].toLowerCase(), exts ) > -1;
                 // check file type is valid as given in 'exts' array
                 if (chktype){
                   return 'Y';
                 } else {
                   return 'N';
                 }
             }   
             
 }
</script>
<?php
echo $this->Form->create($modelClass, array('action' => $action, 'type'=>'file', 'class' => 'form-horizontal well'));
echo $this->Form->input('id', array('type' => 'hidden'));
echo $this->Form->input('category_id', array('type' => 'hidden'));
echo $this->Form->input('sub_category_id', array('type' => 'hidden'));
if($is_comment != 'S'){
  if($is_comment == 'Y'){
      $is_comment = 1;
  }else{
      $is_comment = 0;
  }  
  echo $this->Form->input('is_comment', array('type' => 'hidden', 'value' => $is_comment)); 
}
?>
<?php foreach ($viewDefs[$this->name]['panels'] as $panel) { ?>

<!--    <p class="f_legend"><?= $panel['label'] ?></p>-->
    <?php foreach ($panel['data'] as $panelDatas) { ?>
        <div class="control-group">
            <?php foreach ($panelDatas as $panelData) { ?>
                <div class="row-fluid">
                    <div class="span12">
                        <label class="control-label"  style="width:100px;">
                           <?php if ($panelData['type'] != 'checkbox') { 
                                    echo $panelData['label']; 
                                 } 
                           ?>    
                        </label>
                        <div class="controls" style="margin-left:140px;">
                            <?php
                            if ($panelData['type'] == 'radio') {
                                echo '<div class="form-inline">';
                                if (!empty($panelData['select'])) {
                                    echo $this->Form->radio($panelData['name'], $panelData['option'], $panelData['attr']);
                                    echo $this->Form->select($panelData['selectlabel'], $panelData['select'], $panelData['selectattr']);
                                } else {
                                    echo $this->Form->radio($panelData['name'], $panelData['option'], $panelData['attr']);
                                }
                                echo '</div>';
                            } elseif ($panelData['type'] == 'text') {
                                echo $this->Form->input($panelData['name'], $panelData['attr']);
                                if($panelData['label'] == 'Subject'){
                                  echo '<span class="subject-error" style="color:#F00;"></span>'; 
                                }
                                
                            } elseif ($panelData['type'] == 'checkbox') {
                                echo $this->Form->checkbox($panelData['name'], $panelData['attr']);
                                echo '<span>' . $panelData['label'] . '</span>';
                            } elseif ($panelData['type'] == 'multi-checkbox') {
                                echo $this->Form->input($panelData['name'], array(
                                    'type' => 'select',
                                    'label' => false,
                                    'multiple' => 'checkbox',
                                    $panelData['option']
                                        )
                                );
                            } elseif ($panelData['type'] == 'select') {
                                    echo $this->Form->select($panelData['name'], $panelData['option'], $panelData['attr']);
                                if($panelData['name'] == 'sub_category_id'){
                                    echo '<span class="subcategory-error" style="color:#F00;"></span>'; 
                                }
                                
                            } elseif ($panelData['type'] == 'calendar') {
                                echo "<div " . $panelData['div'] . ">";
                                echo $this->Form->input($panelData['name'], $panelData['attr']);
                                echo '<span class="add-on"><i class="' . $panelData['icon'] . '"></i></span>';
                                echo "</div>";
                            } elseif ($panelData['type'] == 'textarea') {
                                echo $this->Form->textarea($panelData['name'], $panelData['attr']);
                                if($panelData['label'] == 'Detail'){
                                  echo '<span class="detail-error" style="color:#F00;"></span>'; 
                                }
                                if($panelData['label'] == 'Gists'){
                                  echo '<span class="gist-error" style="color:#F00;"></span>'; 
                                }
                            } elseif ($panelData['type'] == 'button') {
                                echo "<div " . $panelData['div'] . ">";
                                echo $this->Form->input($panelData['name'], $panelData['attr']);
                                echo '<span class="add-on"><i class="' . $panelData['icon'] . '"></i></span>';
                                echo "</div>";
                            } elseif ($panelData['type'] == 'file') {
                                if(!empty($attachment)){
                                    foreach($attachment as $attach){
                                        if($panelData['name'] == 'attachment1'){  
                                            if(!empty($attachment[0])){  
                                               echo   '<span style="margin:100px 0px 0px 10px;"><a href="/fileProviders/index/'.$attachment[0]['ResourceContent']['key'].'" target="_blank"><i class="icon-adt_atach"></i>'.$attachment[0]['ResourceContent']['original_name'].'</a></span><span style="margin:0 0 0 5px;"><i class="icon-trash" style="cursor:pointer;" onclick="deleteattach('.$attachment[0]['PortalAttachment']['resource_content_id'].','.$this->request->query['content_id'].','.$this->request->query['category_id'].')"></i></span>';
                                            }else{
                                                  echo $this->Form->input($panelData['name'], $panelData['attr'] ); 
                                                  echo '<span class="attach1-error-type" style="color:#F00;"></span>';
                                                  echo '<span class="attachment1-error-block" style="color:#F00; display:none;">'.__('File over limit size').' (5MB)</span>';
                                                  echo '<span class="help-block">'.$panelData['help'].'</span>'; 
                                            } 
                                            break;  
                                        }else if($panelData['name'] == 'attachment2'){
                                            if(!empty($attachment[1])){  
                                              echo   '<span style="margin:100px 0px 0px 10px;"><a href="/fileProviders/index/'.$attachment[1]['ResourceContent']['key'].'" target="_blank"><i class="icon-adt_atach"></i>'.$attachment[1]['ResourceContent']['original_name'].'</a></span><span style="margin:0 0 0 5px;"><i class="icon-trash" style="cursor:pointer;" onclick="deleteattach('.$attachment[1]['PortalAttachment']['resource_content_id'].','.$this->request->query['content_id'].','.$this->request->query['category_id'].')"></i></span>';
                                            }else{
                                              echo $this->Form->input($panelData['name'], $panelData['attr'] ); 
                                              echo '<span class="attach2-error-type" style="color:#F00;"></span>';
                                              echo '<span class="attachment2-error-block" style="color:#F00; display:none;">'.__('File over limit size').' (5MB)</span>';
                                              echo '<span class="help-block">'.$panelData['help'].'</span>';
                                            }
                                            break;
                                        }else if($panelData['name'] == 'attachment3'){
                                            if(!empty($attachment[2])){  
                                              echo   '<span style="margin:100px 0px 0px 10px;"><a href="/fileProviders/index/'.$attachment[2]['ResourceContent']['key'].'" target="_blank"><i class="icon-adt_atach"></i>'.$attachment[2]['ResourceContent']['original_name'].'</a></span><span style="margin:0 0 0 5px;"><i class="icon-trash" style="cursor:pointer;" onclick="deleteattach('.$attachment[2]['PortalAttachment']['resource_content_id'].','.$this->request->query['content_id'].','.$this->request->query['category_id'].')"></i></span>';   
                                            }else{
                                              echo $this->Form->input($panelData['name'], $panelData['attr'] );
                                              echo '<span class="attach3-error-type" style="color:#F00;"></span>';
                                              echo '<span class="attachment3-error-block" style="color:#F00; display:none;">'.__('File over limit size').' (5MB)</span>';
                                              echo '<span class="help-block">'.$panelData['help'].'</span>';
                                            } 
                                            break;
                                        }    
                                       
                                    }
                                }else{
                                    if($panelData['name'] == 'attachment1'){  
                                        echo $this->Form->input($panelData['name'], $panelData['attr'] ); 
                                        echo '<span class="attach1-error-type" style="color:#F00;"></span>';
                                        echo '<span class="attachment1-error-block" style="color:#F00; display:none;">'.__('File over limit size').' ('.$size.')</span>';
                                        echo '<span class="help-block">'.$panelData['help'].'</span>';                                                                                
                                    }else if($panelData['name'] == 'attachment2'){
                                        echo $this->Form->input($panelData['name'], $panelData['attr'] ); 
                                        echo '<span class="attach2-error-type" style="color:#F00;"></span>';
                                        echo '<span class="attachment2-error-block" style="color:#F00; display:none;">'.__('File over limit size').'('.$size.')</span>';
                                        echo '<span class="help-block">'.$panelData['help'].'</span>';                                                                                
                                    }else if($panelData['name'] == 'attachment3'){
                                        echo $this->Form->input($panelData['name'], $panelData['attr'] );
                                        echo '<span class="attach3-error-type" style="color:#F00;"></span>';
                                        echo '<span class="attachment3-error-block" style="color:#F00; display:none;">'.__('File over limit size').' ('.$size.')</span>';
                                        echo '<span class="help-block">'.$panelData['help'].'</span>';                                        
                                    }
                                } 
                                
                            } elseif ($panelData['type'] == 'ul') {
                                echo '<ul id="' . $panelData['name'] . '"' . $panelData['attr'] . '></ul>';
                                if($panelData['name'] == 'array_content_users'){
                                    echo '<span class="error-choose-user" style="color:#F00;"></span>'; 
                                }
                                
                                
                            }
                            ?>
                               
                                
                        </div>
                    </div>
                </div>    
            <?php } ?>

        </div>
    <?php } ?>
<?php } ?>
<div class="control-group">
    <div class="controls">
        <!--<button class="btn" type="submit">Form button</button>-->
        <?php
        foreach ($buttons as $button) {
            echo $this->Form->button(__($button['name']), $button['attr']);
            echo ' ';
        }
        ?>
    </div>
</div>
<?php
echo $this->Form->end();
?>
