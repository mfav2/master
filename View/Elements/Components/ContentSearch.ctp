<?php
$periods = array('All', 'Last Month', 'Last Week', 'This Week', 'This Month', 'Next Week', 'Next Month');
$sorts = array('Create Date', /*'Publish Date',*/ 'Event Date', /* 'Title' */);
?>
<div class="row-fluid">
    <div class="span4">
		
		
        <?php if(isset($this->request->query['category_id'])) : ?>

        <?php //$accept_show_add_btn = array(17, 18, 20, 21, 22, 23, 24, 25, 27, 28, 29, 30, 31, 32, 33, 42, 43, 44, 45, 46, 47, 119, 120,121,122); ?>

			<?php if(in_array($this->request->query['category_id'], $accept_show_add_btn)){ ?>

				<a class="btn" href="<?= $this->Portal->makeURL('Contents', 'add', 'category_id=' . $this->request->query['category_id']) ?>"><?php echo __('Add New'); ?></a>

			<?php } ?>

			<?php if(in_array($this->request->query['category_id'], $can_add_subcategory)){ ?> 
				<a class="btn" href="javascript:void(0);" onclick="addsubgroup(<?php echo $main_category_id; ?>, <?php echo $this->request->query['category_id']; ?>);"><?php echo __('Add Sub Group'); ?></a>		
			<?php } ?>

        <?php endif; ?>

    </div>
    <div class="span8" style="text-align: right;">
        <form id="searchContent" action="<?php echo $this->Portal->makeUrl('Contents', 'search'); ?>" class="form-inline" method="post">
            <?php
            echo $this->Form->input('category_id', array('type' => 'hidden'));
            echo __('Period').' : ';
            echo $this->form->input('period', array(
                'type' => 'select',
                'label' => false,
                'div' => false,
                'style' => 'width:auto;',
                'options' => $periods,
            ));
            echo __('Sort').' : ';
            echo $this->form->input('sort', array(
                'type' => 'select',
                'label' => false,
                'div' => false,
                'style' => 'width:auto;',
                'options' => $sorts,
            ));
            echo ' ';
            echo $this->Form->input('keyword', array('placeholder' => __('keyword'), 'label' => false, 'div'=>FALSE));
            echo ' ';
            echo $this->form->button(__('Search'), array('type'=>'button','id'=>'submitSearch','class' => 'btn', 'div'=>FALSE));
            ?>
        </form>
    </div>
</div>