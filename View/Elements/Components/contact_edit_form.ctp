<?php
//pr($View);
$templateMeta = $View['panels']['templateMeta'];
$buttons = $templateMeta['form']['buttons'];
$panels1 = $View['panels']['panels1'];
$panels2 = $View['panels']['panels2'];
//$path = $View['path'];
//pr($share_type);
if (!isset($organi_id)) {
    $organi_id = '';
}
//pr($organi_id);
//if($old_key_pic==''){$url_old_pic = '../webroot/attachment/contact/no_image.jpg';}
//pr($url_old_pic); die();
?>

<div class="row-fluid">
    <div class="span12">
        <div class="btn-group sepH_b">
            <button data-toggle="dropdown" class="btn dropdown-toggle">Share <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li>
                    <!--<a class="share_all" id="share_all" style="cursor:pointer;" onclick="shareall()">
                        <i class="splashy-add_small"></i><?=__('Share All')?></a>
                    <a class="sharedep" id="sharedep" data-tableid="smpl_tbl" data-toggle="modal" data-backdrop="static" href="#myModal">
                        <i class="splashy-add_small"></i><?=__('Share Department')?></a>-->
                    <a class="notshare" id="notshare" style="cursor:pointer;" onclick="notshare()">
                        <i class="splashy-add_small"></i><?=__('Not Share')?></a>
                </li>
            </ul>
            <input type="text" id="organi_name" style="margin:1px 0px 0px 5px; width:33.5%;" value="<?php echo $share_type ?>" />
        </div>
        <?php
        echo $this->Form->create('Contact', array('action' => $templateMeta['action'], 'class' => 'form-horizontal well', 'id' => 'addform', 'enctype' => 'multipart/form-data'));
        ?>
        <input type="hidden" id="organi_id" name="organization_id" value="<?php echo $organi_id; ?>" />
        <input type="hidden" id="share_type" name="share_type" value="<?php echo $share_type; ?>" />
        <fieldset>
            <div class="span7">
                <?php foreach ($panels1['data'] as $panel) { ?>
                    <p class="f_legend">
                        <?php
                        echo $panel['label'];
                        if (!empty($panel['select'])) {
                            echo "&nbsp&nbsp" . $this->Form->select($panel['name'], $panel['option'], $panel['attr']);
                        }
                        ?>
                    </p>
                    <div class="row-fluid">     
                        <div class="control-group">
                            <?php foreach ($panel['detail'] as $detail) : ?>
                                <div class="controls" style="margin:0px 0px 10px 30px" align="top">
                                    <?php
                                    if ($detail['label'] != false)
                                        echo '<label class="control-label">' . $detail["label"] . '&nbsp' . '</label>';

                                    if ($detail['type'] == 'hidden') {
                                        echo $this->Form->hidden($detail['name'], $detail['attr']);
                                    } elseif ($detail['type'] == 'text') {
                                        if (!empty($detail['select'])) {
                                            $tol = $detail['selectname'];
                                            $tol2 = $detail['name'];
                                            echo $this->Form->select($detail['name'] = $tol, $detail['option'], $detail['selectattr']);
                                            echo $this->Form->input($detail['name'] = $tol2, $detail['attr']);
                                            unset($tol, $tol2);
                                            if($detail['name'] == 'phone4'){
                                                echo "<div class='phone_require' style='color:#f00; text-align:center;'></div>";
                                            }
					    if($detail['default_tel'] == "1"){
					    	echo "&nbsp;[".__('Default')."]";
					    }
                                        } else {
                                            echo $this->Form->input($detail['name'], $detail['attr']);
                                            if($detail['name'] == 'contact_name'){
                                                echo "<div class='name_require' style='color:#f00; text-align:center;'></div>";
                                            }
                                        }
                                    } elseif ($detail['type'] == 'select') {

                                        echo $this->Form->select($detail['name'], $detail['option'], $detail['attr']);
                                    } elseif ($detail['type'] == 'textarea') {
                                        if (!empty($detail['select'])) {
                                            $tol = $detail['selectname'];
                                            $tol2 = $detail['name'];
                                            echo $this->Form->select($detail['name'] = $tol, $detail['option'], $detail['selectattr']);
                                            echo "<div style='margin:-33px 0px 0px 33%'>" . $this->Form->textarea($detail['name'] = $tol2, $detail['attr']) . "</div>";
                                        } else {
                                            echo $this->Form->textarea($detail['name'], $detail['attr']);
                                        }
                                    } else {
                                        echo $this->Form->textarea($detail['name'], $detail['attr']);
                                    }
                                    ?>
                                </div>
                            <?php endforeach; ?>	
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="span5">
                <?php foreach ($panels2['data'] as $panel) { ?>
                    <p class="f_legend">
                        <?php
                        echo $panel['label'] . "<br/>";
                        if (!empty($panel['select'])) {
                            echo "&nbsp&nbsp" . $this->Form->select($panel['name'], $panel['option'], $panel['attr']);
                        }
                        ?>
                    </p>
                    <div class="row-fluid">     
                        <div class="control-group">
                            <?php foreach ($panel['detail'] as $detail) : ?>
                                <div class="controls" style="margin:0px 0px 10px 0px" align="top">
                                    <?php
                                    if ($detail['label'] != false)
                                        echo '<label class="control-label">' . $detail["label"] . '&nbsp' . '</label>';
                                    if ($detail['type'] == 'fileimage') {
                                        echo '<div align="center">' . $this->Form->file($detail['name'], $detail['attr']) . '</div>';
                                        echo '<div align="center" style="font-size:12px;">'.__('Max File Size').' : 5 MB</div>';
                                        echo '<div class="fileimage_require" align="center" style="color:#f00;"></div>';
                                    } elseif ($detail['type'] == 'image') {
                                        //echo $this->Html->image($detail['name'],$detail['src'], $detail['attr']); 
                                        echo '<div align="center">' . '<img style="border:1px; width:120px;" src=' . $url_old_pic. ' />' . '</div>';
                                    } elseif ($detail['type'] == 'text') {
                                        if (!empty($detail['select'])) {
                                            echo $this->Form->select($detail['name'], $detail['option'], $detail['selectattr']);
                                            echo $this->Form->input($detail['name'], $detail['attr']);
                                        } else {
                                            echo $this->Form->input($detail['name'], $detail['attr']);
                                            if($detail['name'] == 'email1'){
                                                echo "<div class='mail1_require' style='color:#f00; text-align:center;'></div>";
                                            }else if($detail['name'] == 'email2'){
                                                echo "<div class='mail2_require' style='color:#f00; text-align:center;'></div>";
                                            }
                                        }
                                    } elseif ($detail['type'] == 'select') {

                                        echo $this->Form->select($detail['name'], $detail['option'], $detail['attr']);
                                    } elseif ($detail['type'] == 'textarea') {
                                        if (!empty($detail['select'])) {
                                            echo $this->Form->select($detail['name'], $detail['option'], $detail['selectattr']);
                                            echo $this->Form->textarea($detail['name'], $detail['attr']);
                                        } else {
                                            echo $this->Form->textarea($detail['name'], $detail['attr']);
                                        }
                                    } else {
                                        echo $this->Form->textarea($detail['name'], $detail['attr']);
                                    }
                                    ?>
                                </div>
                            <?php endforeach; ?>	
                        </div>
                    </div>
                <?php } ?>
                <div align="center" class="control-group">

                    <!--<button class="btn" type="submit">Form button</button>-->
                    <?php
                    foreach ($buttons as $button) {
                        echo $this->Form->button(__($button, true), array('class' => 'btn', 'id' => 'buten'));
                        echo ' ';
                    }
                    ?>

                </div>
            </div>

        </fieldset>

        <?php
        echo $this->Form->end();
        ?>
    </div>
</div>

<!--<div align="center">
    <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-preview thumbnail" style="width: 100px; height: 100px; line-height: 150px;"></div>
        <div>
            <span class="btn btn-file">
                <span class="fileupload-new">Select image</span>
                <span class="fileupload-exists">Change</span>
                <input type="file" id="conpic" name="data[Contacts][conpic]">
            </span>
            <a class="btn fileupload-exists" href="#" data-dismiss="fileupload">Remove</a>
        </div>
    </div>
</div>-->