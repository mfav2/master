<div class="doc_view">
    <div class="doc_view_header">
        <?php
            $showcolor = '';
            if(!empty($category['Subscrible'])){
                $showcolor = 'background:'.$category['Subscrible'][0]['color'];
            }
        ?>
        <dl class="dl-horizontal" style="padding:10px; ">
            <?php if ($content['Content']['user_id'] == $this->Session->Read('AuthUser.AuthUser.id')) : ?>
                <span class="pull-right" style="width:13%;">
                    <a href="<?= $this->Portal->makeUrl($this->name, 'edit', 'content_id=' . $content['Content']['id'] . '&category_id=' . $content['ContentCategory'][0]['category_id']); ?>">
                        <?php echo __('Edit'); ?>  <i class="icon-pencil"></i></a> | <a href="<?= $this->Portal->makeUrl($this->name, 'delete', 'content_id=' . $content['Content']['id'] . '&category_id=' . $content['ContentCategory'][0]['category_id']); ?>" onclick="return confirm('<?=__('Confirm to delete')?> ?')">
                        <?php echo __('Delete'); ?> <i class="icon-trash"></i>
                    </a>
                </span>

            <?php endif; ?>
            <h3><?= htmlspecialchars_decode($content['Content']['title']) ?> </h3>
        </dl>
    </div>
    <div class="doc_view_content">
        <?= htmlspecialchars_decode($content['Content']['description']) ?> <br /><br />
        <?php 
            if(!empty($attachments)){ //pr($attachments);
                echo __('Attachment File').' : <br /><br />';
                foreach($attachments AS $i => $attach){
                    ++$i;
                    $pic_ext = array("image/pjpeg", "image/jpg", "image/jpeg", "image/gif", "image/png");
                    if(in_array($attach['ResourceContent']['content_type'], $pic_ext)){
                        echo "<div>";  
                        echo "<a target='_blank' href='".$this->FileStorageHelper->urlByKey($attach['ResourceContent']['key'])."'>";
                        echo "<img style='max-width:300px; border=0' src='/fileProviders/index/{$attach['ResourceContent']['key']}' />";
                        echo "</a>";
                        echo "</div><br />";  
                    }else{
                       switch($attach['ResourceContent']['content_type']){
                           case 'application/msword':   $showicon = '<img src="img/custom/doc/word.png" width="26" height="26" />';
                                                        break;
                           case 'application/vnd.ms-excel': $showicon = '<img src="img/custom/doc/excel.png" width="26" height="26" />';
                                                            break;
                           case 'application/pdf': $showicon = '<img src="img/custom/doc/pdf.png" width="26" height="26" />';
                                                    break;
                           default :    $showicon = '<i class="icon-adt_atach"></i>';
                                        break;
                       } 
                        echo '&nbsp;&nbsp;<span ><a target="_blank" href="' . $this->FileStorageHelper->urlByKey($attach['ResourceContent']['key']) . '">'.$showicon.' '.$attach['ResourceContent']['original_name'].'</a></span><br /><br />';
                    }
                    
                }
             
            }
        ?>
       <br />

        <h5><br />(<?php echo __('Expire on'); ?> : <?= $this->Time->format('d/m/Y', $content['Content']['expiry_date']); ?>)</h5>
    </div>
    <div class="doc_view_footer clearfix" style="padding:10px;">
        <div class=" pull-left">
            <span title="<?php echo __('Created on'); ?>: <?= $this->Time->format('l, F d, Y \a\t g:ia', $content['Content']['created_date']) ?> (<?= $this->Time->timeAgoInWords($content['Content']['created_date'], array('format' => 'F jS, Y')); ?>)">
             <?php 
                        
            //organization_description
                        $org_desc = '';
                        $org_belong = '';
                        $showdiv = '';
                        if(!empty($content['Content']['content_orgnization_desc'])){
                            $org_desc  = $content['Content']['content_orgnization_desc'];
                        }
                        if(!empty($content['Content']['organization_belong'])){
                            $org_belong = '&nbsp;-&nbsp;'.$content['Content']['organization_belong'];
                        }

                        if(!empty($org_desc) && !empty($org_belong)){
                            $showdiv = '['.$org_desc.$org_belong.']';
                        }
                         $showdiv = '['.$org_desc.$org_belong.']';
            ?>
                
                <?php echo __('Posted By');?>: <a id="userProfile" data-toggle="modal" data-backdrop="static" href="#myModal" user_id="<?= $content['Content']['user_id']; ?>"><?= $content['Content']['content_updated_user'] ?></a>&nbsp; <?= $showdiv ?> 
            </span>
            <span style="color: green;" title="<?= $this->Time->format('l, F d, Y \a\t g:ia', $content['Content']['event_date']); ?> - <?= $this->Time->format('l, F d, Y \a\t g:ia', $content['Content']['expiry_date']); ?>">
                <?= $this->Time->format('d/m/Y H:i:s', $content['Content']['created_date']); ?>
            </span>
            <!--*************************** -->
 <span title="<?php
    if (isset($content['ViewUserProfile']['UserProfile'])) {
        foreach ($content['ViewUserProfile']['UserProfile'] as $i => $user) {
            echo $user['UserProfile']['first_name_th'] . ' ' . $user['UserProfile']['last_name_th'];
            if (isset($content['ViewUserProfile']['UserProfile'][$i + 1])) {
                echo '&#13;';
            }
        }
    }
    ?>">
    <span class="label label-info"><?php echo __('Views'); ?></span> <?= $content['Content']['count_view'] ?></span> 
    <?php
        if(isset($content['Content']['content_liked_id'])){         
            $comment_id = 0;
    ?>      
            <span id="unlike" chkclick="enabled"><span id="last_like_id" like_id="<?=$content['Content']['content_liked_id']?>" class="label label-info" style="cursor: pointer;" onclick="getUnLike(<?=$content['Content']['id']?>,<?=$content['Content']['content_liked_id']?>, <?=$comment_id?>)" ><?php echo __('Unlike'); ?></span></span>
            <span id="like" chkclick="enabled" style="display:none;"><span class="label label-info" style="cursor: pointer;" onclick="getLike(<?=$content['Content']['id']?>, <?=$comment_id?>)"><?php echo __('Like');?></span></span>
            &nbsp;<span style="display:inline;" id="CountLike"><?php echo $content['Content']['count_like'] ?></span>
    <?php 
        }else{
            $content['Content']['content_liked_id'] = 0;
            $comment_id = 0;
    ?>
            <span id="like" chkclick="enabled"><span class="label label-info" style="cursor: pointer;" onclick="getLike(<?=$content['Content']['id']?>, <?=$comment_id?>)"><?php echo __('Like'); ?></span></span>
            <span id="unlike" chkclick="enabled" style="display:none;"><span id="last_like_id" class="label label-info" like_id="<?=$content['Content']['content_liked_id']?>" style="cursor: pointer;" onclick="getUnLike(<?=$content['Content']['id']?>,<?=$content['Content']['content_liked_id']?>, <?=$comment_id?>)" ><?php echo __('Unlike'); ?></span></span>
            <span style="display:inline;" id="CountLike"><?php echo $content['Content']['count_like'] ?></span>
    <?php    
        }     
    ?>

        </div>
    </div>
</div>

<br>
<?php if ($content['Content']['is_comment'] == 'Y') : ?>
    <?= $content['Content']['count_comment'] ?> <?php echo __('Comments'); ?> <img src="img/gCons/chat-.png" alt="" />
    <br />
  <!-- ******************** Show Comment ************************* -->  
    <div class="row-fluid">
        <div class="chat_box span12">
           <?php foreach ($comments as $i => $comment) : ?> 
            <div class="doc_view">
                <div class="doc_view_header">
                    <dl class="dl-horizontal" style="padding:10px;">
                          <?php echo __('Comment'); ?> <?= $i + 1 ?>
                    </dl>
                </div>
                <div class="doc_view_content">
                    <?= htmlspecialchars_decode($comment['ContentComment']['description']) ?> <br /><br />
                    <!-- Attachment -->
                    <?php	
                        if(!empty($comment['PortalAttachment']['resource_content_id'])){
                              $rc = $comment['ResourceContent'];
                             
                                $pic_ext = array("image/pjpeg", "image/jpg", "image/jpeg", "image/gif", "image/png");
                                if(in_array($rc['content_type'], $pic_ext)){
                                    echo "<div>";  
                                    echo "<a href='".$this->FileStorageHelper->urlByKey($rc['key'])."' target='_blank'><img style='max-width:300px;' src='/fileProviders/index/{$rc['key']}' /></a><br />";
                                    echo "</div>";  
                                }else{
                                     echo "Attachment File: <br/><r /><span style='font-size:12px;'>";
                                     echo '<a href="' . $this->FileStorageHelper->urlByKey($rc['key']) . '"><i class="icon-adt_atach"></i></a>';
                                     echo "</span>";
                                }
                              
                        }
                        
                   ?>
                   <div>
        <?php 
            
        ?>
           
       </div> 
                    <!-- End Attachment -->
                </div>
                <div class="doc_view_footer clearfix" style="padding:10px;">
                    <div class=" pull-left">
                    <?php
                        $is_like = false;
                        foreach ($comment['ContentLike'] as $like) {
                            if ($like['created_user_id'] == $this->Session->Read('AuthUser.AuthUser.id') && $like['deleted'] == 'N') {
                                $is_like = true;
                                $like_id = $like['id'];
                                $comment_id = $like['reference_id'];
                                break;
                            }
                        }
        
                        $org_description = '';
                        if(!empty($comment['ContentComment']['organization_description'])){
                            $org_description = $comment['ContentComment']['organization_description'];
                        }
                    ?>
                        
                        <?php echo __('Posted By'); ?>:
                        <a id="userProfile" data-toggle="modal" data-backdrop="static" href="#myModal" user_id="<?= $comment['ContentComment']['user_id'] ?>"><?= $comment['ContentComment']['updated_user'] ?></a> 
                        &nbsp;[ <?php echo $org_description.'&nbsp;-&nbsp;'.$comment['ContentComment']['organization_belong']; ?> ]&nbsp;
                        <span title="<?= $this->Time->format('l, F d, Y \a\t g:ia', $comment['ContentComment']['created_date'], array('format' => 'F jS, Y')) ?>">
                            <?= $this->Time->format('d/m/Y H:i:s', $comment['ContentComment']['created_date']) ?>
                        </span>
                        
                        <span style="cursor: pointer;">
                           <?php
                                if($is_like){
                           ?>
                                <span id="unlike<?=$comment_id?>" chkclick="enabled"><span id="last_like_id<?=$comment_id?>" like_id="<?=$like_id?>" class="label label-info" style="cursor: pointer;" onclick="getUnLike(<?=$content['Content']['id']?>,<?=$like_id?>, <?=$comment_id?>)" ><?php echo __('Unlike'); ?></span></span>
                                <span id="like<?=$comment_id?>" chkclick="enabled" style="display:none;"><span class="label label-info" style="cursor: pointer;" onclick="getLike(<?=$content['Content']['id']?>, <?=$comment_id?>)"><?php echo __('Like'); ?></span></span>
                                &nbsp;<span style="display:inline;" id="CountCommentLike<?=$comment['ContentComment']['id']?>"><?php echo $comment['ContentComment']['count_like'] ?></span>
            
                           <?php 
                                }else{
                                $content['Content']['content_liked_id'] = 0;    
                           ?>
                                <span id="like<?=$comment['ContentComment']['id']?>" chkclick="enabled"><span class="label label-info" style="cursor: pointer;" onclick="getLike(<?=$content['Content']['id']?>, <?=$comment['ContentComment']['id']?>)"><?php echo __('Like'); ?></span></span>
                                <span id="unlike<?=$comment['ContentComment']['id']?>" chkclick="enabled" style="display:none;"><span id="last_like_id<?=$comment['ContentComment']['id']?>" class="label label-info" like_id="<?=$content['Content']['content_liked_id']?>" style="cursor: pointer;" onclick="getUnLike(<?=$content['Content']['id']?>,<?=$content['Content']['content_liked_id']?>, <?=$comment['ContentComment']['id']?>)" ><?php echo __('Unlike'); ?></span></span>
                                &nbsp;<span style="display:inline;" id="CountCommentLike<?=$comment['ContentComment']['id']?>"><?php echo $comment['ContentComment']['count_like'] ?></span>
                           <?php  
                                }
                           ?>
                                
<!--                            <a href="javascript:void(0);" onclick="getLike($content['Content']['id'], $comment['ContentComment']['id'])"></a>
                            <a href="javascript:void(0);" onclick="getUnLike($content['Content']['id'],$like_id,$comment_id)"></a>                        -->
                            
                        </span>    
<!--                        <span style="cursor: pointer;">
                            <a  title="<?= ($is_like ? 'Click here to unlike this comment.' : 'Click here to like this comment.') ?>"
                            href="<?=
                            ($is_like ? '/Contents/unlike/' . $content['Content']['id'] . '/' . $like_id . '/' . $comment_id :
                                    '/Contents/like/' . $content['Content']['id'] . '/' . $comment['ContentComment']['id'])
                            ?>"
                            >
                                <span class="label label-info" style="cursor: pointer;"><?php echo $is_like ? 'Unlike' : 'Like';?> </span> 
                            </a>
                            &nbsp;<?= $comment['ContentComment']['count_like'] ?>
                        </span>-->
                    </div>
                </div>    
            </div>
          <?php endforeach; ?>  
        </div>    
    </div>
  <!-- ************************************************************************** --> 
    <br>
  <!-- **************************** Create Comment ****************************** -->  
    <div class="row-fluid">
        <div class="span12">
            <div class="doc_view" style="padding:10px 20px;">
                <div class="doc_view_header" >
                    <!-- Form Add Comment -->
                        <form method="post" enctype="multipart/form-data" action="<?php echo $this->Portal->makeUrl('Contents', 'comment', 'content_id=' . $content['Content']['id']); ?>" name="data[ContentComment]">
                            <?= $this->Form->input('content_id', array('value' => $content['Content']['id'], 'type' => 'hidden')); ?>
                            <textarea id ="description" name="description" rows="10" class="span12 required wysiwg_mini"></textarea>
                            <span class="error-comment" style="color:#F00;"></span>
                            <br>
                            <?= $this->Form->input('attachment', array('label'=>false,'type' => 'file', 'class'=> 'span12')); ?>
                            <span class="comment-error" style="color:#F00;"></span><br />
                            <span class="help-block"><?= __('Max File Size')?> : <?php echo $filesize; ?></span><br />
                            <input id="submitCommentform" class="btn" type="submit" value="<?=__('Compose')?>">
                        </form>
                    <!-- **************** -->
                </div>
                
            </div>
            
        </div>
    </div>
 <!-- *************************************************************** -->   
    
<?php endif; ?>
<div>
</div>
    <?php //pr($content); ?>

<script>
    
$('#attachment').bind('change', function() {
    $('.comment-error').empty();

     err = checkfileminsize(this.files[0].size);
     if(err == false){
         $('.comment-error').empty().text('<?=__('File over limit size')?> (5 MB)');
     }
});
$('#submitCommentform').click(function(){
    var err = false;
    $('.comment-error').empty();
    var comment =$('#description').val();
    if(comment == ''){
        $('.error-comment').empty().text('<?=__('This field is required')?>');
        err = true;
    }
/******************* check type file *********************/
        var file = $('#attachment').val();
                       // first check if file field has any value
        if(file != ''){
            var filetype = checktypefile(file);         
            if(filetype == 'N'){
                $('.comment-error').empty().text('<?php echo __("Only file"); ?> doc, docx, txt, pdf, jpg, jpeg, png, gif, xls, xlsx, ppt, pptx, bmp, zip, swf');
                err = true;
            }
        }
        /*********************************************************/        
    if(err == true){
        return false;
    }else{
        this.form.submit();
    }
});
    
 function checkfileminsize(filesize){
    var chk ='';
    var maxsize = '<?php echo Configure::read('Config.MinFileSize'); ?>';
                    if(filesize > maxsize){
                        chk = false;
                    }else{
                        chk = true;
                    }
                    
    return  chk;                
 }  
  function checktypefile(file){
            var exts = ['doc', 'docx', 'txt', 'pdf', 'jpg', 'jpeg', 'png', 'gif', 
                        'xls', 'xlsx', 'ppt', 'pptx', 'bmp', 'swf', 'zip'];
            if (file) {
                 // split file name at dot
                 var get_ext = file.split('.');
                 // reverse name to check extension
                 get_ext = get_ext.reverse();
                 var chktype = $.inArray ( get_ext[0].toLowerCase(), exts ) > -1;
                 // check file type is valid as given in 'exts' array
                 if (chktype){
                   return 'Y';
                 } else {
                   return 'N';
                 }
             }   
             
 }
 
 function getLike(content_id, comment_id){
        if(comment_id == 0 || comment_id ==''){
            var check = $('#like').attr('chkclick');
            $('#like').attr('chkclick', 'disabled');
            
        }else{
            var check = $('#like'+comment_id).attr('chkclick');
            $('#like'+comment_id).attr('chkclick', 'disabled');
        }   
            if(check == 'disabled'){
                return false;
            }
            $.ajax({
                cache: false,
                url: "/contents/like",
                type: "post",
                data: {'content_id':content_id, 'comment_id': comment_id},
                success:function(data){                 
                    data = $.parseJSON(data);
                    if(comment_id == 0 || comment_id ==''){
                        var countlike = data["Content"]["count_like"];
                        var like_id = data["Content"]["like_id"];
                        if(countlike != 0){
                            $('#CountLike').text(countlike);
                            $('#last_like_id').attr('like_id',like_id);
                            $('#like').css('display','none');
                            $('#unlike').css('display','inline');
                        }else{
                            alert(data);
                        }
                        $('#like').attr('chkclick', 'enabled');
                        $('#CountLike').show();
                    }else{
                        var countlike = data["ContentComment"]["count_like"];
                        var like_id = data["ContentComment"]["like_id"];
                        if(countlike != 0){
                            $('#CountCommentLike'+comment_id).text(countlike);
                            $('#last_like_id'+comment_id).attr('like_id',like_id);
                            $('#like'+comment_id).css('display','none');
                            $('#unlike'+comment_id).css('display','inline');
                            
                        }else{
                            alert(data);
                        }
                       $('#like'+comment_id).attr('chkclick', 'enabled');
                       $('#CountCommentLike'+comment_id).show(); 
                    }     
                }
            });
        
 }
 function getUnLike(content_id, like_id, comment_id){
        var num_default = 0;
        if(comment_id == 0 || comment_id ==''){
            var check = $('#unlike').attr('chkclick');
            $('#unlike').attr('chkclick', 'disabled');
            like_id =  $('#last_like_id').attr('like_id');
        }else{
            var check = $('#unlike'+comment_id).attr('chkclick')
            $('#unlike'+comment_id).attr('chkclick', 'disabled');
            like_id =  $('#last_like_id'+comment_id).attr('like_id');
        }   
            if(check == 'disabled'){
                return false;
            }
            $.ajax({
                cache: false,
                url: "Contents/unlike",
                type: "post",
                data: {'content_id':content_id, 'like_id': like_id, 'comment_id' : comment_id},
                success:function(data){
                    data = $.parseJSON(data); 
                    if(comment_id == 0 || comment_id ==''){
                        var countlike = data["Content"]["count_like"];
                        var like_id = data["Content"]["like_id"];
                       if(countlike != 0){
                         $('#CountLike').text(countlike);
                         $('#last_like_id').attr('like_id',num_default);
                       }else if(countlike == 0){
                         $('#CountLike').empty().text(countlike);
                         $('#last_like_id').attr('like_id',num_default);
                       }else{
                         alert(data);
                         $('#CountLike').text(num_default);
                         $('#last_like_id').attr('like_id',num_default);
                       }
                         $('#unlike').attr('chkclick', 'enabled'); 
                         $('#unlike').css('display','none');
                         $('#like').css('display','inline');   
                         $('#CountLike').show();
                    }else{
                        var countlike = data["ContentComment"]["count_like"];
                        var like_id = data["ContentComment"]["like_id"]; 
                       if(countlike != 0){
                         $('#CountCommentLike'+comment_id).text(countlike);
                         $('#last_like_id'+comment_id).attr('like_id',num_default);
                         
                       }else if(countlike == 0){
                         $('#CountCommentLike'+comment_id).text(countlike);
                         $('#last_like_id'+comment_id).attr('like_id',num_default);
                         
                       }else{
                         alert(data);
                         $('#CountCommentLike'+comment_id).text(num_default);
                         $('#last_like_id'+comment_id).attr('like_id',num_default);
                       }
                        $('#unlike'+comment_id).attr('chkclick', 'enabled');
                        $('#unlike'+comment_id).css('display','none');
                        $('#like'+comment_id).css('display','inline');
                        $('#CountCommentLike'+comment_id).show();
                    }    
                }
            });              
 }
</script>