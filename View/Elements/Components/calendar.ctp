<?php 
    $currentUser = $this->Session->read('AuthUser');
    $id = $currentUser['AuthUser']['id'];
    $selected = '';
    
?>
<div class="row-fluid">
    <div class="span12">
        <div style="float:right;">
            
            <select id="Ownercalendar">
                <option value="<?php echo $id; ?>" selected='selected'><?=__('My Appointment')?></option>
                <?php foreach($showowner as $showlists): ?>
                    <?php 
                      if($userid == $showlists['AppointmentHandle']['user_id']){ 
                          $selected = 'selected=selected';
                      }
                    ?>
                    <option <?php echo $selected; ?> value="<?php echo $showlists['AppointmentHandle']['user_id']; ?>"><?php echo $showlists['AppointmentHandle']['owner_name']; ?></option>   
                <?php endforeach; ?>
            </select>
            
            
            
        </div>    
        <div style="float:left;">
            <button id="compose" class="btn"><?=__('New Appointment')?></button> 
            <a id="myhandle" class="btn" href="<?php echo $this->Portal->makeUrl('appointments', 'indexhandle'); ?>"><?=__('My Appointment Handler')?></a>
           
        </div>
        <div style="clear:both;"></div>
<!--           <h4 class="heading">Calendar &nbsp;</h4>-->
           <h2 class="heading">&nbsp;</h2>
            
            <div id="calendar"></div>
    </div>
    <input type="hidden" id="userId" value="<?php echo $id; ?>" />
	<input type='hidden' id="start_date" value="" />
	<input type='hidden' id="end_date" value="" />
	
</div>





<script src="js/custom/custom_calendar.js"></script>
<script>

  $('#compose').click(function(){
        var user_id = $('#userId').val();
        //alert(user_id);
        window.location.href="/appointments/appointmentuseradd/"+user_id;
  });         
</script>