<?php
$templateMeta = $viewDefs[$this->name]['EditView']['templateMeta'];
$buttons = $templateMeta['form']['buttons'];
$panels = $viewDefs[$this->name]['EditView']['panels'];
?>   
    <div class="row-fluid">
	<div class="span12">

		<!--<form class="form-horizontal well">-->
		<?php echo $this->Form->create($this->name,array('action'=>$templateMeta['action'],'class'=>'form-horizontal well')); ?>
			<fieldset>
				<?php foreach ($panels as $panel) { ?>
				<p class="f_legend"><?php echo $panel['label'] ?></p>
					<?php foreach ($panel['data'] as $panelDatas) { ?>
					<div class="control-group">
						<div class="row-fluid">
							<?php foreach ($panelDatas as $panelData) { ?>
								<div class="span6">
								<label class="control-label"><?php echo $panelData['label'] ?></label>
								<div class="controls">
									<input type="text" class="span10" />
									<span class="help-block"><?php echo $panelData['help'] ?></span>
								</div>
								</div>
							<?php }?>
						</div>
					</div>
					<?php }?>

				<?php }?>
				<div class="control-group">
					<div class="controls">
						<!--<button class="btn" type="submit">Form button</button>-->
						<?php foreach ($buttons as $button) {
							echo $this->Form->button(__($button, true), array('class'=>'btn'));
							echo ' ';
						}?>
					</div>
				</div>	
			</fieldset>
		<!--</form>-->
		<?php echo $this->Form->end();?>
		
	</div>
    </div>