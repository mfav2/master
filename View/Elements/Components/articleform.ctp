<?php
//pr($viewDefs);
//pr($templateMeta);
//die();
$templateMeta = $viewDefs[$this->name][$template]['templateMeta'];
$buttons = $templateMeta['form']['buttons'];
$panels = $viewDefs[$this->name][$template]['panels'];

//pr($this->query);
?>   
    
    <div class="row-fluid">
	<div class="span12">
		<?php echo $this->Form->create($this->name,array('action'=>$templateMeta['action'],'class'=>'form-horizontal well')); ?>
                        <fieldset>
                            <?php foreach ($panels as $panel) { ?>
                            <p class="f_legend"><?php echo $panel['label'] ?></p>
                                <?php foreach ($panel['data'] as $panelDatas) { ?>
                                    <div class="control-group">
                                            
                                                    <?php foreach ($panelDatas as $panelData) { ?>
                                           <div class="row-fluid">     
                                               <div class="span12">
                                                    <label class="control-label"><?php echo $panelData['label'] ?></label>
                                                    <div class="controls">
                                                        <?php 
                                                        if($panelData['type'] == 'radio') { 
                                                           if(!empty($panelData['select'])){ 
                                                              echo $this->Form->radio($panelData['name'], $panelData['option'], $panelData['attr'] );
                                                              echo $this->Form->select($panelData['selectlabel'], $panelData['select'], $panelData['selectattr'] );
                                                              
                                                              
                                                           }else{
                                                               
                                                              echo $this->Form->radio($panelData['name'], $panelData['option'], $panelData['attr'] );   
                                                              
                                                           } 
                                                           
                                                        }else if($panelData['type'] == 'button'){
                                                            
                                                            echo "<div ".$panelData['div'].">";
                                                            echo $this->Form->input($panelData['name'], $panelData['attr']);
                                                            echo '<span class="add-on"><i class="'.$panelData['icon'].'"></i></span>';
                                                            echo "</div>";
                                                            
                                                        }else if($panelData['type'] == 'text'){
                                                            
                                                            echo $this->Form->input($panelData['name'], $panelData['attr'] );
                                                            
                                                        }else if($panelData['type'] == 'checkbox'){
                                                            
                                                             echo  $this->Form->checkbox($panelData['name'], $panelData['attr']);
                                                             echo  '<span>'.$panelData['label'].'</span>';
                                                             
                                                        }else if($panelData['type'] == 'file'){
                                                            
                                                             echo  $this->Form->input($panelData['name'], $panelData['attr']);
                                                             
                                                        }else if($panelData['type'] == 'hidden'){
                                                            echo  $this->Form->input($panelData['name'], $panelData['attr']);
                                                        }else if($panelData['type'] == 'select'){
                                                            echo $this->Form->select($panelData['name'], $panelData['option'], $panelData['attr'] );
                                                        }else{
                                                             echo $this->Form->textarea($panelData['name'], $panelData['attr']);
                                                        }
                                                        ?>
                                                            <!--<input type="text" class="span10" />
                                                            <span class="help-block"><?php echo $panelData['help'] ?></span> -->
                                                    </div>
                                                </div>
                                           </div>    
                                                    <?php }?>
                                            
                                    </div>
                                <?php }?>
                            <?php } ?>
                            <div class="control-group">
                                    <div class="controls">
                                            <!--<button class="btn" type="submit">Form button</button>-->
                                            <?php foreach ($buttons as $button) {
                                                    echo $this->Form->button(__($button, true), array('class'=>'btn'));
                                                    echo ' ';
                                            }?>
                                    </div>
                            </div>
                        </fieldset>
		<?php echo $this->Form->end();?>
	</div>
    </div>
<script src="js/custom/custom_forms.js"></script>  

