<div class="row-fluid">
        <div class="span12">
                <h3 class="heading">&nbsp;</h3>
                <table class="fc-view fc-view-month fc-grid" style="width:100%;">
                        <thead>
                                <tr class="fc-first fc-last">
                                        
                                        <?php foreach ($listViewDefs as $listViewDef) { ?>
                                        <th class="fc-agenda-axis fc-widget-header fc-first" <?php echo empty($listViewDef['width']) ? '' : ' style="width:'.$listViewDef['width'].'%"'?>><?php echo $listViewDef['label'];?></th>
                                        <?php }?>
                                </tr>
                        </thead>
                        <tbody>
                       
                           <?php foreach ($listViewDatas as $listViewData) { ?>
                            <?php
                                $start = $listViewData['Appointment']['start_date'];    
                                $end =   $listViewData['Appointment']['end_date'];    
                                $start_date = Utility::cdate($start, 'Y-m-d H:i:s', 'd/m/Y H:i:s');
                                $end_date = Utility::cdate($end, 'Y-m-d H:i:s', 'd/m/Y H:i:s');
                            ?>
                                <tr >
                                    <td style="padding:5px;" class="fc-agenda-axis fc-widget-header"><?php echo $start_date; ?></td>
                                    <td style="padding:5px;" class="fc-agenda-axis fc-widget-header"><?php echo $end_date; ?></td>
                                    <td style="padding:5px;" class="fc-agenda-axis fc-widget-header"><a href="<?php echo $this->Portal->makeUrl('appointments','view', 'id='.$listViewData['Appointment']['id']); ?>"><?php echo $listViewData['Appointment']['name']; ?></a></td>
                                </tr>

                           <?php }?>
                        </tbody>
                </table>
        </div>
</div>

<!-- hide elements (for later use) -->
