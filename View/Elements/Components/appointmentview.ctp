<?php
$templateMeta = $viewDefs[$this->name]['DetailView']['templateMeta'];
$buttons = $templateMeta['form']['buttons'];
$panels = $viewDefs[$this->name]['DetailView']['panel'];
//$panel2 = $viewDefs[$this->name]['DetailView']['panel2'];
$currentUser = $this->Session->read('AuthUser');
    //$id = $currentUser['AuthUser']['id'];
$countReader = count($userappt['AppointmentUserReader']);
?>    
<div align="right" style="margin:5px 0px;">
   <?php if($userappt['Appointment']['user_id'] == $currentUser['AuthUser']['id'] || $userappt['Appointment']['handle_user_id']== $currentUser['AuthUser']['id']){ ?> 
    <a class="btn" href="<?php echo $this->Portal->makeUrl('appointments','edit','id='.$this->request->query['id']) ?>">Edit</a>&nbsp;
    <a class="btn" onclick="deleteitem(<?php echo $this->request->query['id']; ?>)" href="javascript:void(0);">Delete</a>
   <?php } ?> 
</div>

<?php echo $this->Form->create('Appointment',array('action'=>$templateMeta['action'],'class'=>'form-horizontal well')); ?>
<div class="row-fluid">
    <div class="span12">
        <div class="span12">
		<?php foreach ($panels as $panel) { ?>
		<h3 class="heading"><?php echo $panel['label'] ?></h3>
                                <input id="id" class="span0" type="hidden" value="<?php echo $this->request->query['id']; ?>" name="data[Appointment][id]">
				<?php foreach ($panel['data'] as $panelDatas) { ?>
				<!--<div class="formSep">-->
				<div class="control-group">
					<div class="row-fluid">
						<?php foreach ($panelDatas as $panelData) { ?>
						<div class="span12">
							<label class="control-label" ><span style="font-family: Tahoma; "><strong><?php echo $panelData['label'] ?></strong></span></label>
							<div class="controls text_line">
                                                            <?php if($panelData['name'] == 'invite'){ ?> 
                                                                
                                                                    <?php foreach($viewDatas[$panelData['name']] as $invite){ ?>
                                                                        
                                                                        <?php foreach($chkname['AppointmentUserReader'] as $showname){ ?>
                                                                          <?php if($invite['AppointmentUserReader']['user_id'] == $showname['UserProfile']['user_id']) { ?>   
                                                                            <span style="font-family: Tahoma; margin:100px 0px 0px 50px; "><?php echo empty($invite['AppointmentUserReader']['user_id']) ? '' : $showname['UserProfile']['first_name_th'].' '.$showname['UserProfile']['last_name_th'] ?></span>
                                                                            <span>[ <?php echo empty($invite['AppointmentUserReader']['confirmed_status']) ? 'Waiting for confirm' : $invite['AppointmentUserReader']['confirmed_status']; ?>]</span>
                                                                            <br />                                                              
                                                                          <?php } ?>
                                                                          
                                                                          
                                                                        <?php } ?>  
                                                                                    
                                                                    <?php } ?>
                                                                    
                                                            <?php }else if($panelData['name'] == 'attach'){?>
                                                                    <?php foreach($attachment as $attach){    ?>
                                                                            <span style="margin:100px 0px 0px 50px;"><a href="/fileProviders/index/<?=$attach['ResourceContent']['key']?>" target="_blank"><i class="icon-adt_atach"></i><?=$attach['ResourceContent']['original_name']?></a></span>
                                                                    <?php } ?>        
                                                                            
                                                            <?php }else{ ?>
								<span style="font-family: Tahoma; margin:100px 0px 0px 50px; "><?php echo empty($viewDatas[$panelData['name']]) ? '' : $viewDatas[$panelData['name']] ?></span>
                                                            <?php } ?>    
							</div>
						</div>
						<?php }?>
					</div>
                                                           </div>
				<?php }?>
			
		<?php }?>
        </div>
       <?php if($countReader != 0 && $userappt['Appointment']['user_id'] != $currentUser['AuthUser']['id'] && $userappt['Appointment']['handle_user_id'] != $currentUser['AuthUser']['id']){ ?> 
        <div align="center" class="control-group">

                    <!--<button class="btn" type="submit">Form button</button>-->
                    <?php
                    foreach ($buttons as $button) {
                      
                        echo $this->Form->button(__($button, true), array('class' => 'btn','id' =>'confirmation_status', 'name' => 'data[AppointmentUserReader][confirmed_status]', 'value'=>$button));
                        
                    }
                    ?>

        </div>
      <?php } ?>  
    </div>
</div>
</form>
<div id="deletedata" class="modal hide fade">
    <div class="modal-header">
        <button id="closepopup" class="close" data-dismiss="modal">×</button>
        <h3>&nbsp;</h3>
    </div>
    <div id="deleteid" class="modal-body">
        <?php echo __("Are you sure to delete this appointment?"); ?>
    </div>
    <div class="modal-footer">
        <a id="deleteitem" class="btn" href="javascript:void(0);">Yes</a>
        <a id="closepopup" class="btn" data-dismiss="modal" href="javascript:void(0);">Cancel</a>
    </div>
</div>
<script>  
function deleteitem(id){
    $('#deletedata').modal('show');
    $('#deleteitem').click(function(){
         $.ajax({
            cache: false,
            url: "/appointments/delete",
            type: "post",
            data: {'id':id },
            success:function(data){
                if(data == 1){
                    $('#deletedata').modal('hide');
                    location.href='<?php echo $this->Portal->makeUrl('appointments','calendar'); ?>';
                }else{
                    alert(data);
                }
            }
         });   
    });
}
</script>