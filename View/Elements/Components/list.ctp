<div class="row-fluid">
        <div class="span12">
                <h3 class="heading"><?=$topic['panel']['label']?>s</h3>
                <?php echo $this->Form->create('Tests', array('type'=>'file', 'url'=>'/'.$this->Portal->makeUrl('tests', 'testdel'))); ?>
                <table class="table  table_vam" id="dt_gal">
                        <thead>
                                <tr>
                                        <th class="table_checkbox"><input type="checkbox" name="select_rows" class="select_rows" data-tableid="dt_gal" /></th>
                                        <?php foreach ($listViewDefs as $listViewDef) { ?>
                                        <th<?php echo empty($listViewDef['width']) ? '' : ' style="width:'.$listViewDef['width'].'px"'?>><?php echo $listViewDef['label'];?></th>
                                        <?php }?>
                                </tr>
                        </thead>
                        <tbody>
                       
                           <?php foreach ($listViewDatas as $listViewData) { ?>
                            
                                <tr>
                                    <td>
                                        <input  type="checkbox" name="row_sel" class="row_sel" value="<?=$listViewData['id']?>" />
                                        <input type="text" id="rowselect<?=$listViewData['id']?>" value="<?=$listViewData['id']?>" />
                                    </td>
                                    <?php foreach ($listViewDefs as $key=>$listViewDef) { ?>
                                        <td><?php echo $listViewData[$key]?></td>
                                    <?php }?>
                                </tr>
                                
                               <?php }?>
                                <div class="hide">
                                        <!-- actions for datatables -->
                                        <div class="dt_gal_actions">
                                                <div class="btn-group">
                                                        <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                                <li><a href="javascript:void(0)"><i class="icon-pencil"></i> Add</a></li>
                                                                <li><a href="#" class="delete_rows_dt" data-tableid="dt_gal"><i class="icon-trash"></i> Delete</a></li>

                                                        </ul>
                                                </div>
                                        </div>
                                        <!-- confirmation box -->
                                        <div id="confirm_dialog" class="cbox_content">
                                                <div class="sepH_c tac"><strong>Are you sure you want to delete this row(s)?</strong></div>
                                                <div class="tac">
                                                        <a href="#" class="btn btn-gebo confirm_yes" >Yes</a>
                                                        <a href="#" class="btn confirm_no">No</a>
                                                </div>
                                        </div>
                                </div>    
                      
                        
                            
                        </tbody>
                </table>
  <?php echo $this->Form->end(); ?>
        </div>
</div>

<!-- hide elements (for later use) -->


<script src="js/custom/custom_forms.js"></script>         

   
