					<div class="row-fluid">
						<div class="span12">
							<h3 class="heading">ร่างหนังสือเข้า</h3>
							<table class="table table-bordered table-striped table_vam" id="dt_gal">
								<thead>
									<tr>
										<th class="table_checkbox"><input type="checkbox" name="select_rows" class="select_rows" data-tableid="dt_gal" /></th>
										
										<?php foreach ($listViewDefs as $listViewDef) { ?>
										<th<?php echo empty($listViewDef['width']) ? '' : ' style="width:'.$listViewDef['width'].'px"'?>><?php echo $listViewDef['label'];?></th>
										<?php }?>
										<th>Date</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($listViewDatas as $listViewData) { ?>
									<tr>
										<td><input type="checkbox" name="row_sel" class="row_sel" /></td>
										<?php foreach ($listViewDefs as $key=>$listViewDef) { ?>
										<td><?php echo $listViewData[$key]?></td>
										<?php }?>
										<td></td>
										<td>
											<a href="#" class="sepV_a" title="Edit"><i class="icon-pencil"></i></a>
											<a href="#" class="sepV_a" title="View"><i class="icon-eye-open"></i></a>
											<a href="#" title="Delete"><i class="icon-trash"></i></a>
										</td>
									</tr>
									<?php }?>
			
								</tbody>
							</table>
							
						</div>
					</div>

					<!-- hide elements (for later use) -->
					<div class="hide">
						<!-- actions for datatables -->
						<div class="dt_gal_actions">
							<div class="btn-group">
								<button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
								<ul class="dropdown-menu">
									<li><a href="#" class="delete_rows_dt" data-tableid="dt_gal"><i class="icon-trash"></i> Delete</a></li>
									<li><a href="javascript:void(0)">Lorem ipsum</a></li>
									<li><a href="javascript:void(0)">Lorem ipsum</a></li>
								</ul>
							</div>
						</div>
						<!-- confirmation box -->
						<div id="confirm_dialog" class="cbox_content">
							<div class="sepH_c tac"><strong>Are you sure you want to delete this row(s)?</strong></div>
							<div class="tac">
								<a href="#" class="btn btn-gebo confirm_yes">Yes</a>
								<a href="#" class="btn confirm_no">No</a>
							</div>
						</div>
					</div>
					<?php //exit();?>
