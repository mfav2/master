<?php 
    $currentUser = $this->Session->read('AuthUser');
?>


<?php 
      foreach($categories as $category){
        if($category['Category']['id'] == $this->request->query['category_id']){  
          echo '<div style="padding:8px; color:#006bd6; border:1px solid #e1e1e8;"><h3>'.$category['Category']['category_name'].'</h3></div>';
        }  
          
      } 
          
?>
<div class="prettyprint pop_over" style="background-color: white; border-collapse: collapse; border-width: 0px; border-bottom-width: 1px;" ></div>
<?php foreach ($contents as $content) : ?>
            <?php 
            //organization_description
                $org_desc = '';
                $org_belong = '';
                $showdiv = '';
                if(!empty($content['Content']['organization_description'])){
                    $org_desc  = $content['Content']['organization_description']['description'];
                }
                if(!empty($content['Content']['organization_belong'])){
                    $org_belong = '&nbsp;-&nbsp;'.$content['Content']['organization_belong'];
                }
                
                if(!empty($org_desc) && !empty($org_belong)){
                    $showdiv = '['.$org_desc.$org_belong.']';
                }
               
                
            ?>                
            <div class="prettyprint" style="background-color: white; border-collapse: collapse; border-width: 1px; border-top-width: 0px;" >
                
            <?php echo $this->Time->format('d M y', $content['Content']['event_date']).'&nbsp;&nbsp;&nbsp;'; ?>
            <a class="pop_over"  data-content="<?php echo $content['ShowListContent']['gist']; ?>" data-placement="bottom" href="<?= $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $content['ShowListContent']['id']); ?>">
                <?php 
                    echo empty($content['Content']['title']) ? 'No title' : mb_substr(strip_tags($content['Content']['title']), 0, 256, 'UTF-8');
                ?>
            </a>
               
              <?php if($currentUser['AuthUser']['id'] == $content['Content']['user_id']){ ?>
               <span class="pull-right">
                   <a href="<?= $this->Portal->makeUrl($this->name, 'edit', 'content_id=' . $content['Content']['id'] . '&category_id=' . $content['ContentCategory']['category_id']); ?>">
                       <i class='icon-pencil'></i></a>  | <a href="<?= $this->Portal->makeUrl($this->name, 'delete', 'content_id=' . $content['Content']['id']); ?>" onclick="return confirm('<?=__('Confirm to delete')?> ?')"><i class='icon-trash'></i></a>
               </span>
              <?php } ?> <br />
                
            <span title="<?= $this->Time->timeAgoInWords($content['Content']['created_date']) ?>">
            <a id="userProfile" data-toggle="modal" data-backdrop="static" href="#myModal" user_id="<?= $content['Content']['user_id'] ?>" style="color:black; font-size: smaller">
                <?= $content['Content']['content_updated_user']; ?>
            </a>
                &nbsp;<?php echo "<span style='font-size:smaller'>" . $showdiv; ?>
                - <?= $this->Time->format('Y-m-d H:i:s', $content['Content']['created_date'])  . "</span>" ?>
            </span>
            <?php if ($content['Content']['is_comment'] == 'Y' && $content['Content']['count_comment'] > 0) : ?>
            <span title="<?php
                  if (isset($content['CommentUserProfile']['UserProfile'])) {
                      foreach ($content['CommentUserProfile']['UserProfile'] as $i => $user) {
                          echo $user['UserProfile']['first_name_th'] . ' ' . $user['UserProfile']['last_name_th'];
                          if (isset($content['CommentUserProfile']['UserProfile'][$i + 1])) {
                              echo '&#13;';
                          }
                      }
                  }
                  ?>">
                  <?php //'<i class="splashy-comments"></i> ' . $content['Content']['count_comment']; ?>
                <span class="label label-info"><?= $content['Content']['count_comment'] ?> Comments</span> 
            </span>
              <?php endif; ?>
           <span class="label label-info"><?= $content['Content']['count_view'] ?> Views</span> 
            
        </div>    

<?php endforeach; ?>
<div id="viewmore_result"></div><br />

<input id="offset" type="hidden" value="<?php echo @$offset; ?>"/>
<input id="total" type="hidden" value=""/>
<input id="countcontent" type="hidden" value="<?php echo @$countContent; ?>" />
<input id="category_id" type="hidden" value="<?php echo $this->request->query['category_id']; ?>" />
<input id="search" type="hidden" value="<?php echo @$this->request->query['keyword']; ?>" />

<div>
    <span class="pull-right"><?php //echo $this->Paginator->numbers(); ?></span>
    <div id="viewmore" class="btn" style="cursor: pointer; width: 97%">View More...</div>
</div>

<script>
    $('#reversion').click(function() {
        $('#box').load('/articles/reversion/');
    });
    
    $('#viewmore').click(function(){
        var offset = parseInt($('#offset').val());
        var category_id = $('#category_id').val();
        var keyword = $('#search').val();
        var totalContents = $('#countcontent').val();
        
        $('#viewmore').text('Loading...');
        //alert('offset = ' + offset + '; total = ' + totalContents);
        
        $.post('/Contents/ajaxsearch', {offset : offset, category_id : category_id, keyword : keyword}, function(data){
           $('#viewmore_result').append(data);
           
           offset = offset + 20;
           $('#offset').val(offset);
           
           $('#viewmore').text('View More...');
           if(offset >= totalContents){ $('#viewmore').hide(); }
        });
    });
    
    $(function(){
        var offset = parseInt($('#offset').val());
        var totalContents = $('#countcontent').val();
        if(offset >= totalContents){ $('#viewmore').hide(); }
        
    });
</script>