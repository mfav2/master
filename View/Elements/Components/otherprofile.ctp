<div id="otherProfileDiv">
    <div class="modal-header">
        <button class="close" data-dismiss="modal">x</button>
        <h3>Other Profile</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="span3"><img src="http://placekitten.com/820/140" /></div>
            <div class="span3">
                <b>
                    Name:<br>
                    Position:<br>
                    Div.Dept.<br>
                    Assignment/Desk:<br>
                    Internal Phone:<br>
                    Mobile:<br>
                    Direct Tel:<br>
                    Fax No:<br>
                    Email:
                </b>
            </div>
            <div class="span6">
                <?= $user['name']; ?><br>
                <?= $user['position']; ?><br>
                <?= $user['department']; ?><br>
                <?= $user['assignment']; ?><br>
                <?= $user['tel_phone']; ?><br>
                <?= $user['tel_mobile']; ?><br>
                <?= $user['tel_direct']; ?><br>
                <?= $user['tel_fax']; ?><br>
                <?= $user['email']; ?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <div class="row-fluid">
            <div class="span6" style="text-align: left">
                <button class="btn" type="submit">Send Message</button>
            </div>
            <div class="span6" sytle="text-align: right">
                <a class="btn" data-dismiss="modal" href="#">Close</a>
            </div>
        </div>
    </div>
</div>