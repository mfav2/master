<?php
$starthour_time = array('all'=>'All Day', 'morning'=>'All Morning', 'afternoon'=>'All Afternoon');
$endhour_time = array('all'=>'All Day', 'morning'=>'All Morning');
for($i=0;$i<=24;$i++){
    $hourtime = str_pad($i, 2, "0", STR_PAD_LEFT);
    $set[$i] = $hourtime;
}
$startmerge = array_merge($starthour_time, $set);
$endmerge = array_merge($endhour_time, $set);


    $templateMeta = $viewDefs[$this->name]['AddView']['templateMeta'];
    $buttons = $templateMeta['form']['buttons'];
    $labels = $viewDefs[$this->name]['AddView']['heads'];
    $panels = $viewDefs[$this->name]['AddView']['panels'];
    $allchecked = '';
    $alwaychecked = '';
    $currentUser = $this->Session->read('AuthUser');
    $selected = '';
    $beforespan = 0;
    $id = $currentUser['AuthUser']['id'];
    if($all == 'Y'){
       $allchecked = 'checked=checked';                                                         
    }
    if(empty($chkcolor)){
        $chkcolor = '1';
    }
    if($alway == 'Y'){
       $alwaychecked = 'checked=checked';        
    }
?> 
   <div class="row-fluid">
	<div class="span12">
		<?php echo $this->Form->create('Appointment',array('action'=>$templateMeta['action'], 'type'=>'file','class'=>'form_validation_ttip form-horizontal well')); ?>
                        <fieldset>
                            <?php foreach ($panels as $panel) { ?>
                              <!-- ************* Create Form *************** -->
                                <?php foreach ($panel['data'] as $panelDatas) { ?>
                                    <div class="control-group">
                                        <?php echo $this->Form->input('id', array('type'=>'hidden','id' => 'id', 'div'=>false, 'label'=>false)); ?>
                                        <?php echo $this->Form->input('user_id', array('type'=>'hidden','id' => 'user_id', 'div'=>false, 'label'=>false)); ?>
                                      <!-- **** Design box form **** -->  
                                        <div class="row-fluid">
                                            <?php foreach ($panelDatas as $panelData) { ?>   
                                                   <?php
                                                        $beforespan = $beforespan + $panelData['span'];    
                                                   ?>
                                            <div class="span<?php echo $panelData['span']; ?>" style="border:0px solid #000;" >
                                                    <?php 
                                                           if(!empty($panelData['label'])){
                                                               
                                                    ?>        

                                                             
                                                         <div class="controls span12">
                                                             
                                                            <?php 
                                                                
                                                                if($panelData['type'] == 'text') {
                                                                    if($panelData['name'] == 'name'){
                                                                        
                                                                        echo '<div class="span3" style="float:left; text-align:right; padding:5px 0 0 0; ">'.$panelData['label'].'</div>';
                                                                        echo '  <div style="float:left; margin:0 0 0 10px;">';
                                                                        echo        $this->Form->input($panelData['name'], $panelData['attr'] );
                                                                        echo '  </div>';
                                                                        echo '<div style="clear:both;"></div>';
                                                                        echo '<div style="text-align:right;"><span style="color:#f00;" class="help-block" id="error_title"></span></div>';
                                                                       
                                                                        
                                                                        
                                                                      
                                                                        echo '<div class="span3" style="float:left; text-align:right; margin:0px; padding:10px 0 0 0;">'.$panelData['labelstart'].'</div>'; 
                                                                        echo '  <div style="float:left; margin:0 0 0 10px;">';
                                                                        echo "          <div ".$panelData['divstart'].">";
                                                                        echo                $this->Form->input($panelData['namestart'], $panelData['attrstart']);
                                                                        echo '          <span class="add-on"><i class="'.$panelData['iconstart'].'"></i></span>';
                                                                        echo "          </div>";
                                                                        echo '  </div>';
                                                                        
                                                                        
                                                                        echo '  <div style="float:left; margin:5px 0 0 5px;">';
                                                                        echo        $this->Form->select($panelData['namestarthour'], $startmerge, $panelData['selectstarthourattr'] );
                                                                        echo '  </div>';
                                                                        
                                                                        echo '  <div style="float:left; margin:5px 0 0 5px;">';
                                                                        echo        $this->Form->select($panelData['namestartmin'], $panelData['selectstartmin'], $panelData['selectstartminattr'] );
                                                                        echo '  </div>';
                                                                        
                                                                        echo '<div style="clear:both; text-align:right;"><span style="color:#f00;" class="help-block" id="error_startdate"></span></div>';
                                                                        
                                                                        
                                                                        
                                                                        echo '<div class="span3" style="float:left; text-align:right; margin:0px; padding:10px 0 0 0;">'.$panelData['labelend'].'</div>'; 
                                                                        echo '  <div style="float:left; margin:0 0 0 10px;">';
                                                                        echo "          <div ".$panelData['divend'].">";
                                                                        echo                $this->Form->input($panelData['nameend'], $panelData['attrend']);
                                                                        echo '          <span class="add-on"><i class="'.$panelData['iconend'].'"></i></span>';
                                                                        echo "      </div>";
                                                                        echo '  </div>';
                                                                        
                                                                        echo '  <div style="float:left; margin:5px 0 0 5px;">';
                                                                        echo        $this->Form->select($panelData['nameendhour'], $endmerge, $panelData['selectendhourattr'] );
                                                                        echo '  </div>';
                                                                        
                                                                        echo '  <div style="float:left; margin:5px 0 0 5px;">';
                                                                        echo        $this->Form->select($panelData['nameendmin'], $panelData['selectendmin'], $panelData['selectendminattr'] );
                                                                        echo '  </div>';
                                                                        echo '<div style="clear:both; text-align:right;"><span style="color:#f00;" class="help-block" id="error_startdate"></span></div>';
                                                                        
                                                                        echo '<div style="text-align:right;"><span style="color:#f00;" class="help-block" id="error_time"></span></div>';
                                                                       

                                                                    }else{
                                                                        echo '<div class="span3" style="float:left; text-align:right; padding:5px 0 0 0; ">'.$panelData['label'].'</div>';
                                                                        echo '<div style="float:left; margin:0 0 0 10px;">';
                                                                        echo $this->Form->input($panelData['name'], $panelData['attr'] );
                                                                        echo '</div>';
                                                                        echo '<div style="clear:both;"></div>';
                                                                    } 
                                                                    
                                                                }else if($panelData['type'] == 'radio'){
                                                                        echo '<div style="float:left; text-align:left; margin:0 0 0 5px;">';
                                                                    if($panelData['label'] != 'Location'){
                                                                        echo '<div>'.$panelData['label'].'</div>';
                                                                    }
                                                                        
                                                                        echo '<ul class="span12" id="'.$panelData['namelist'].'" style="list-style:none; line-height:1.5;"><li>'; 
                                                                        echo $this->Form->radio($panelData['name'], $panelData['option'], $panelData['attr'] );  
                                                                        echo '</ul>';
                                                                        echo '</div>';
                                                                        echo '<div style="clear:both;"></div>';
                                                                        
                                                                    
                                                                        
                                                                }else if($panelData['type'] == 'checkbox'){
                                                                    echo  $this->Form->checkbox($panelData['name'], $panelData['attr']);
                                                                    echo  '<span>'.$panelData['label'].'</span>'; 
                                                                    
                                                                }else if($panelData['type'] == 'select'){
                                                                    echo $this->Form->select($panelData['name'], $panelData['option'], $panelData['attr'] );   
                                                                }else if($panelData['type'] == 'remind'){
                                                                    
                                                                    echo '<div style="float:left; text-align:left; padding:5px 0 0 0; ">'.$panelData['label'].'</div>';
                                                                    echo '<div style="float:left; margin:0 0 0 60px;">';
                                                                    echo  $this->Form->checkbox($panelData['name'], $panelData['attr']);
                                                                    echo  $this->Form->select($panelData['selectlabel'], $panelData['select'], $panelData['selectattr']);
                                                                    echo  $this->Form->select($panelData['selectlabel2'], $panelData['select2'], $panelData['selectattr2']);
                                                                    echo '</div>';
                                                                    echo '<div style="clear:both;"></div>';
//                                                                    echo  $this->Form->checkbox($panelData['name'], $panelData['attr']);
//                                                                //echo '<input id="is_alert" name="data[Appointments][is_alert]" type="checkbox" />&nbsp;Remind&nbsp;';
//                                                                    echo $this->Form->select($panelData['selectlabel'], $panelData['select'], $panelData['selectattr']);
//                                                                    echo $this->Form->select($panelData['selectlabel2'], $panelData['select2'], $panelData['selectattr2']);
//          
                                                                }else if($panelData['type'] == 'remind_repeat'){
                                                                    echo '<div id="remind_repeat"  style="float:left; text-align:left; padding:5px 0 0 0; ">'.$panelData['label'].'</div>';
                                                                    echo '<div style="float:left; margin:0 0 0 10px;">';
                                                                    echo $this->Form->checkbox($panelData['name'], $panelData['attr']);
                                                                    echo $this->Form->select($panelData['selectlabel2'], $panelData['select2'], $panelData['selectattr2']);
                                                                    echo '</div>';
                                                                    echo '<div style="clear:both;"></div>';
                                                                    //echo  $this->Form->checkbox($panelData['name'], $panelData['attr']);
                                                                    //echo $this->Form->select($panelData['selectlabel2'], $panelData['select2'], $panelData['selectattr2']);
                                                                    
                                                                }else if($panelData['type'] == 'file'){
                                                             
                                                                    echo '<div style="float:left; text-align:right; padding:5px 0 0 0; ">'.$panelData['label'].'</div>';
                                                                    echo '  <div style="float:left; text-align:left; margin:0 0 0 10px;">';
                                                                            if(!empty($attachment)){
                                                                                foreach($attachment as $attach){
                                                                                      //echo   '<span style="margin:100px 0px 0px 10px;"><a href="/fileProviders/index/'.$attach['ResourceContent']['key'].'" target="_blank"><i class="icon-adt_atach"></i>'.$attach['ResourceContent']['original_name'].'</a></span><span style="margin:0 0 0 5px;">[<a href="/appointments/deleteAttachment/'.$attach['PortalAttachment']['resource_content_id'].'">delete</a>]</span>';
                                                                                      echo   '<span style="margin:100px 0px 0px 10px;"><a href="/fileProviders/index/'.$attach['ResourceContent']['key'].'" target="_blank"><i class="icon-adt_atach"></i>'.$attach['ResourceContent']['original_name'].'</a></span><span style="margin:0 0 0 5px;"><i class="icon-trash" style="cursor:pointer;" onclick="deleteattach('.$attach['PortalAttachment']['resource_content_id'].')"></i></span>';
                                                                                }
                                                                            }else{
                                                                                echo $this->Form->input($panelData['name'], $panelData['attr'] ); 
                                                                            }
                                                                   // echo        $this->Form->input($panelData['name'], $panelData['attr'] );
                                                                    echo '  </div>';
                                                                    echo '  <div style="float:left; text-align:left; margin:0 0 0 12px;">';
                                                                    echo '      <span class="help-block">'.$panelData['help'].'</span>';
                                                                    echo '  </div>';
                                                                    echo '<div style="clear:both;"></div>';
                                                                    
                                                                }else if($panelData['type'] == 'textarea'){
                                                                    echo '<div class="span3" style="float:left; text-align:right; padding:5px 0 0 0; ">'.$panelData['label'].'</div>';
                                                                    echo '<div style="float:left; margin:0 0 0 10px;">';
                                                                    echo $this->Form->textarea($panelData['name'], $panelData['attr']);
                                                                    echo '</div>';
                                                                    echo '<div style="clear:both;"></div>';
                                                                }else if($panelData['type'] == 'color'){
                                                                    echo '<div style="float:left; margin:0 0 0 5px;">';
                                                                    echo '<div>'.$panelData['label'].'</div>';
                                                                    //echo '<ul id="'.$panelData['namelist'].'" style="list-style:none; line-height:1.5;"><li>'; 
                                                                            foreach($panelData['option'] as $showcolor){
                                                                                $checked = '';

                                                                                if($chkcolor == $showcolor['AppointmentColor']['id']){
                                                                                    $checked = "checked=checked";
                                                                                }
                                                                                
                                                                                echo '<input style="margin:0 5px 5px 10px;" id="color_id" type="radio" color="'.$showcolor['AppointmentColor']['color_code'].'" name="data[Appointment][color_id]"  value="'.$showcolor['AppointmentColor']['id'].'" '.$checked.'  />';
                                                                                echo '<span for="color_id" class="showchooseborder" style="background:'.$showcolor['AppointmentColor']['color_code'].' border:0px solid '.$showcolor['AppointmentColor']['color_code'].' width:40px; height:40px;" >&nbsp;&nbsp;&nbsp;&nbsp;</span>';
                                                                            }
                                                                    echo '</div>';
                                                                    echo '<div style="clear:both;"></div>'; 
                                                                    
                                                                }else if($panelData['type'] == 'invite'){
                                                                    echo '<div class="span2" style="float:left; text-align:left; padding:5px 0 0 0; ">'.$panelData['label'].'</div>';
                                                                    echo '  <div style="float:left; text-align:left; margin:0 0 0 0px;">';
                                                                    //echo '          ';
                                                                    echo '          <ul id="array_tag_handler" style="background:#fff; width:260px; margin:0px;"></ul>';
                                                                    echo '  </div>';
                                                                    echo '<div style="float:left;"></div>';
                                                                    echo '<a id="inviteUser"  href="#myModal" data-backdrop="static" data-toggle="modal" title="Invite User"><i class="splashy-contact_grey_add"></i></a>';
//                                                                      echo '      <div style="float:left; margin:0 0 0 10px;">';
//                                                                    echo '          <a id="inviteUser"  href="#myModal" data-backdrop="static" data-toggle="modal" title="Invite User"><i class="splashy-contact_grey_add"></i></a>';
//                                                                    echo '          <ul id="array_tag_handler" class="span12" style="background:#fff; margin:0px;"></ul>';
//                                                                    echo '      </div>';
                                                                    echo '<div style="clear:both;"></div>';
                                                                }else if($panelData['type'] == 'showinvite'){
                                                                    
                                                                    echo '<div class="span2" style="float:left; text-align:left; padding:5px 0 0 0; ">'.$panelData['label'].'</div>';
                                                                    echo '  <div style="float:left; text-align:left; margin:0 0 0 12px;">';
                                                                    echo '<ul style="list-style:none; ">';
                                                                    foreach($panelData['data'] as  $datapanel){
                                                                        foreach($chkname['AppointmentUserReader'] as $showname){
                                                                            if($datapanel['AppointmentUserReader']['user_id'] == $showname['UserProfile']['user_id']){
                                                                                 echo '<li style="display:inline; padding:5px; background:#cee5f5; color:#2b4f62; margin-right:5px;">'.$showname['UserProfile']['first_name_th'].'&nbsp;'.$showname['UserProfile']['last_name_th'].'</li>';                                                                       
                                                                            }

                                                                        }

                                                                    }

                                                                    echo '</ul>';
                                                                    echo '</div>';
                                                                    echo '<div style="clear:both;"></div>';
                                                                    //echo '<input type="text" id="AppointmentUserReader" name="data[AppointmentUserReader][][user_id]" value="" />';

                                                               }
                                                            ?> 
                                                             <div style="text-align:right;"><span style="color:#f00;" class="help-block" id="<?php echo $panelData['error'] ?>"></span></div>
                                                         </div>    
                                                    <?php 
                                                        }
                                                    ?> 
                                            </div>    
                                            <?php if($beforespan == 12){ ?>
                                            </div>        
                                            <div class="row-fluid">
                                            <?php 
                                                    $beforespan = 0;
                                                  } 
                                                } // end foreach    
                                            ?>  
                                        </div>    
                                      <!-- **** End Design box form **** -->  
                                    </div>
                                <?php } ?>
                              <!-- ************* End Create Form *************** -->  
                            <?php } ?>
                            <div class="control-group">
                                    <div class="controls">
                                            <!--<button class="btn" type="submit">Form button</button>-->
                                            <?php foreach ($buttons as $button) {
                                                echo $this->Form->button($button['name'], $button['attr']);    
                                            }
                                            ?>
                                    </div>
                            </div>
                        </fieldset>
		<?php echo $this->Form->end();?>
	</div>
    </div>
