<div class="row-fluid">
	<div class="span12">
		<h3 class="heading">Regular wizard</h3>
		<div class="row-fluid">
			<div class="span3"></div>
			<div class="span6">
				<form id="simple_wizard" class="stepy-wizzard form-horizontal">
					<fieldset title="Personal info">
						<legend class="hide">Lorem ipsum dolor&hellip;</legend>
						<div class="formSep control-group">
							<label for="s_username" class="control-label">Username:</label>
							<div class="controls">
								<input type="text" name="s_username" id="s_username" />
							</div>
						</div>
						<div class="formSep control-group">
							<label for="s_password" class="control-label">Password:</label>
							<div class="controls">
								<input type="password" name="s_password" id="s_password" />
							</div>
						</div>
						<div class="sepH_c control-group">
							<label for="s_email" class="control-label">E-mail:</label>
							<div class="controls">
								<input type="text" name="s_email" id="s_email" />
							</div>
						</div>
					</fieldset>
					<fieldset title="Contact info">
						<legend class="hide">Lorem ipsum dolor&hellip;</legend>
						<div class="formSep control-group">
							<label for="s_street" class="control-label">Street Address:</label>
							<div class="controls">
								<input type="text" name="s_street" id="s_street" />
							</div>
						</div>
						<div class="formSep control-group">
							<label for="s_city" class="control-label">City:</label>
							<div class="controls">
								<input type="text" name="s_city" id="s_city" />
							</div>
						</div>
						<div class="sepH_c control-group">
							<label for="s_country" class="control-label">Country:</label>
							<div class="controls">
								<input type="text" name="s_country" id="s_country" />
							</div>
						</div>
					</fieldset>
					<button type="button" class="finish btn btn-primary"><i class="icon-ok icon-white"></i> Send registration</button>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<h3 class="heading">Wizard with validation</h3>
		<div class="row-fluid">
			<div class="span2"></div>
			<div class="span8">
				<form id="validate_wizard" class="stepy-wizzard form-horizontal">
					<fieldset title="Personal info">
						<legend class="hide">Lorem ipsum dolor&hellip;</legend>
						<div class="formSep control-group">
							<label for="v_username" class="control-label">Username:</label>
							<div class="controls">
								<input type="text" name="v_username" id="v_username" />
							</div>
						</div>
						<div class="formSep control-group">
							<label for="v_password" class="control-label">Password:</label>
							<div class="controls">
								<input type="password" name="v_password" id="v_password" />
							</div>
						</div>
						<div class="control-group">
							<label for="v_email" class="control-label">E-mail:</label>
							<div class="controls">
								<input type="text" name="v_email" id="v_email" />
							</div>
						</div>
					</fieldset>
					<fieldset title="Contact info">
						<legend class="hide">Lorem ipsum dolor&hellip;</legend>
						<div class="formSep control-group">
							<label for="v_street" class="control-label">Street Address:</label>
							<div class="controls">
								<input type="text" name="v_street" id="v_street" />
							</div>
						</div>
						<div class="formSep control-group">
							<label for="v_city" class="control-label">City:</label>
							<div class="controls">
								<input type="text" name="v_city" id="v_city" />
							</div>
						</div>
						<div class="control-group">
							<label for="v_country" class="control-label">Country:</label>
							<div class="controls">
								<input type="text" name="v_country" id="v_country" />
							</div>
						</div>
					</fieldset>
					<fieldset title="Additional info">
						<legend class="hide">Lorem ipsum dolor&hellip;</legend>
						<div class="formSep control-group">
							<label for="v_message" class="control-label">Your Message:</label>
							<div class="controls">
								<textarea name="v_message" id="v_message" rows="3"></textarea>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Newsletter:</label>
							<div class="controls">
								<label class="radio inline" for="newsletter_yes">
									<input type="radio" value="yes" id="newsletter_yes" name="v_newsletter"> Yes
								</label>
								<label class="radio inline" for="newsletter_no">
									<input type="radio" value="no" id="newsletter_no" name="v_newsletter"> No
								</label>
							</div>
						</div>
					</fieldset>
					<button type="button" class="finish btn btn-primary"><i class="icon-ok icon-white"></i> Send registration</button>
				</form>
			</div>
		</div>
	</div>
</div>