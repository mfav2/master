<div id="myProfileDiv">
    <fieldset>
        <div class="modal-header">
            <button class="close" data-dismiss="modal" id="closeX">×</button>
            <h3><?=__('My Profile')?></h3>
        </div>
        <div class="modal-body">
            <label><?=__('Assignment/Desk')?> : </label>
            <input type="text">
            <div class="row-fluid">
                <div class="span6">
                    <label><?=__('Internal Tel')?> : </label>
                    <input type="text">
                </div>
                <div class="span6">
                    <label><?=__('Mobile')?> : </label>
                    <input type="text">
                    <label class="checkbox help-inline">
                        <input type="checkbox" name="optionsCheckboxList1" value="option1">
                        Hide
                    </label>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span6">
                    <label><?=__('Direct Tel')?> : </label>
                    <input type="text">
                </div>
                <div class="span6">
                    <label><?=__('Fax No')?> : </label>
                    <input type="text">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="row-fluid">
                <div class="span6" style="text-align: left">
                    <button class="btn" type="submit"><?=__('Update')?></button>
                    <span class="help-inline">
                        <a id="changePwd" href="#"><?=__('Change Password')?></a>
                    </span>
                </div>
                <div class="span6" sytle="text-align: right">
                    <a class="btn" data-dismiss="modal" href="#" id="closeB"><?=__('Close')?></a>
                </div>
            </div>
        </div>
    </fieldset>
</div>

<script>
    $('#changePwd').click(function(){
        $('#box').load('/profiles/changepwd');
    });
</script>