 
<div class="row-fluid">
        <div class="span12">
                <div class="mbox">
                        <div class="tabbable">
                                <div class="heading">
                                        <ul class="nav nav-tabs">
                                                <?php foreach($listViewDefs['panel'] as $panels) { ?>
                                                     <?php 
                                                       if(!empty($panels['active'])){
                                                          $active = 'class="active"';   
                                                       }else{
                                                          $active = "";  
                                                       }
                                                       
                                                     ?>   
                                                <li <?=$active?>><a href="#<?=$panels['tabname']?>" data-toggle="tab"><i class="<?=$panels['class']?>"></i> <?=$panels['label']?></a></li>
                                                <?php }?>
                                            
                                        </ul>
                                </div>
                                <div class="tab-content">
                                    <?php foreach($listViewDefs['panel'] as $panels) { ?>
                                        <?php 
                                            if(!empty($panels['active'])){
                                               $active = 'active';   
                                            }else{
                                               $active = "";  
                                            }
                                        ?>   
                                        <div class="tab-pane <?=$active?>" id="<?=$panels['tabname']?>">
                                             <?php if($panels['type'] == 'list') { ?> 
                                            <table class="table table-bordered  table_vam" id="dt_gal">
                                                <thead>
                                                    <tr>
                                                        <?php foreach($panels['data'] as $data){ ?>
                                                                
                                                        <th<?php echo empty($data['width']) ? '' : ' style="width:'.$data['width'].'px"'?>><?php echo $data['label']; ?></th>
                                                        <?php } ?>    
                                                        <th><i class="icon-trash"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($panels['detaildata'] as $detail){ ?>
                                                        <tr>
                                                             <?php foreach($panels['data'] as $key=>$data){ ?>
                                                             <td><?php echo $detail[$key]; ?></td> 
                                                             <?php } ?>  
                                                             <th><i class="icon-trash"></i></th>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>       
                                             <?php }else{ 

										$templateMeta = $viewDefs[$this->name]['EditView']['templateMeta'];
										$buttons = $templateMeta['form']['buttons'];
										$panelss = $viewDefs[$this->name]['EditView']['panels'];
										//pr($this->query);
 
   										echo $this->Form->create($this->name,array('action'=>$templateMeta['action'],'class'=>'form-horizontal well')); ?>
													
                       					 <fieldset>
                                         	 <?php foreach ($panelss['data'] as $key=>$panel) { ?>
                                             		<p class="f_legend"><?php echo $panel['label'] ?></p>
                                                    <div class="control-group">
                                     					<div class="row-fluid">
                                                          <div class="span12">
                                                        	<?php foreach($panel['detail'] as $detail){ ?>
                                                            
                                                            	 <?php if(!empty($detail['label_name'])){ ?>
                                                           			<div class="controls"  style="text-align:left; margin: 0 0 0 10px;">
                                                                      <label class="control-label"><?php echo $detail['label_name'] ?></label>
                                                                      <div class="controls text_line">
                                                                        <?php echo $this->Form->input($detail['name'], $detail['txtattr']) ?>
                                                                      </div> 	
                                                            		</div>
                                                                 <?php }else{ ?>
                                                                 
                                                                 		 <?php if($detail['type'] == "select"){ ?>
                                                                         	<div class="controls">
                                                                              
                                                                               <label class="controls"><?php echo $this->Form->select($detail['selectlabel'], $detail['selectoption'], $detail['selectattr']); ?>
																			   <?php echo $this->Form->input($detail['name'], $detail['txtattr']) ?></label>
                                                                               
                                                                              	
                                                                            </div>
                                                                            
                                                                           
                                                                   		 <?php }else{ ?>
                                                                         
                                                                         		textarea
                                                                         <?php } ?>	
                                                                 <?php } ?>
                                                            <?php } ?>
                                                           </div> 
                                                        </div>
                                                    </div>
                                                        
                                             <?php } ?>
                                         </fieldset>
                        			<?php							
											echo $this->Form->end();
									  } ?>
                                        </div>
                                   <?php }?>    
                                </div>
                        </div>
                </div>

        </div>
</div>

<!-- hide elements -->
<div class="hide">
        <!-- actions for inbox -->
        <div class="dt_inbox_actions">
                <div class="btn-group">
                        <a href="javascript:void(0)" class="btn" title="Answer"><i class="icon-pencil"></i></a>
                        <a href="javascript:void(0)" class="btn" title="Forward"><i class="icon-share-alt"></i></a>
                        <a href="#" class="delete_msg btn" title="Delete" data-tableid="dt_inbox"><i class="icon-trash"></i></a>
                </div>
        </div>
        <!-- actions for outbox -->
        <div class="dt_outbox_actions">
                <div class="btn-group">
                        <a href="javascript:void(0)" class="btn" title="Resend"><i class="icon-share-alt"></i></a>
                        <a href="#" class="delete_msg btn" title="Delete" data-tableid="dt_outbox"><i class="icon-trash"></i></a>
                </div>
        </div>
        <!-- actions for trash -->
        <div class="dt_trash_actions">
                <div class="btn-group">
                        <a href="#" class="delete_msg btn" title="Delete permamently" data-tableid="dt_trash"><i class="icon-trash"></i></a>
                </div>
        </div>
        <!-- confirmation box -->
        <div id="confirm_dialog" class="cbox_content">
                <div class="sepH_c tac"><strong>Are you sure you want to delete this message(s)?</strong></div>
                <div class="tac">
                        <a href="#" class="btn btn-gebo confirm_yes">Yes</a>
                        <a href="#" class="btn confirm_no">No</a>
                </div>
        </div>
</div>

