                  <div class="row-fluid">
						<div class="span12">
							<h3 class="heading">Google map search</h3>
							<div class="row-fluid">
								<div class="span4">
									<div class="row-fluid" id="g-map-top">
										<div class="span12">
											<div class="well">
												<form class="input-append" id="gmap_search">
													<input autocomplete="off" class="span8" type="text" placeholder="Find location on map..." /><button type="submit" class="btn"><i class="splashy-marker_rounded_add"></i></button>
												</form>
											</div>
											<div class="location_add_form well" style="display: none">
												<p class="formSep"><strong>Add/Edit location:</strong></p>
												<div class="formSep">
													<label>Name</label>
													<input type="text" class="span10" id="comp_name" />
													<label>Contact</label>
													<input type="text" class="span10" id="comp_contact" />
													<label>Phone</label>
													<input type="text" class="span10" id="comp_phone" />
													<label>Address</label>
													<input type="text" class="span10" id="comp_address" readonly="readonly" />
													<label>Lat, Lng</label>
													<input type="text" class="span10" id="comp_lat_lng" readonly="readonly" />
													<input type="hidden" id="comp_id" />
												</div>
												<button class="btn btn-invert">Save</button>
											</div>
										</div>
									</div>
								</div>	
								<div class="span8">
									<div class="well">
										<div id="g_map" style="width:100%;height:400px"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span12">
							<table class="table table-striped location_table">
								<thead>
									<tr>
										<th>Id</th>
										<th>Name</th>
										<th>Contact</th>
										<th>Adress</th>
										<th>Lat, Lng</th>
										<th>Phone</th>
										<th style="width:90px">Actions</th>
									</tr>
								</thead>
								<tbody>
									<tr>
                                        <td>1</td>
                                        <td>Daily News</td>
                                        <td>John Doe</td>
                                        <td class="address">4 New York Plaza, New York, NY 10004, United States</td>
                                        <td>40.702677, -74.011277</td>
                                        <td>(212) 210-2100</td>
                                        <td>
                                            <a href="javascript:void(0)" class="show_on_map btn btn-gebo btn-mini">Show</a>
                                            <a href="javascript:void(0)" class="comp_edit btn btn-mini">Edit</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Global Radio</td>
                                        <td>Kate Beck</td>
                                        <td class="address">31 Leicester Square, London, Greater London WC2H 7LH, UK</td>
                                        <td>51.510117, -0.129468</td>
                                        <td>020 7484 8958</td>
                                        <td>
                                            <a href="javascript:void(0)" class="show_on_map btn btn-gebo btn-mini">Show</a>
                                            <a href="javascript:void(0)" class="comp_edit btn btn-mini">Edit</a>
                                        </td>
                                    </tr>
									<tr>
										<td>3</td>
										<td>Exchange Square</td>
										<td>John Woo</td>
										<td class="address">1 Hoi Wan St, Quarry Bay, Hong Kong</td>
										<td>22.288074, 114.211743</td>
										<td>2825-7878</td>
										<td>
											<a href="javascript:void(0)" class="show_on_map btn btn-gebo btn-mini">Show</a>
											<a href="javascript:void(0)" class="comp_edit btn btn-mini">Edit</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>