<?php 
    $currentUser = $this->Session->read('AuthUser');
    
?>

    <?php
        if(!empty($top5)){
            //pr($this->request->query);
            echo '<div class="prettyprint"  style="background-color: white;">
                    <h4 style="padding-bottom:15px">Recommended Topics </h4>';
            foreach($top5 AS $i => $t){
                //if($t['ContentCategory']['category_id'] != $this->request->query['category_id']){ continue; }
                //echo '<div class="prettyprint ttip_t" title="' . strip_tags($t['Content']['description']) . '">';
                echo "<hr style='padding-top:2px; margin: 2px'>";
                echo '<a href="' . $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $t['Content']['id']) . '">';
                echo empty($t['Content']['title']) ? __('No title') : strip_tags(htmlspecialchars_decode($t['Content']['title']));
                echo "</a>";
                
            }
            echo '</div><br /><br />';
        }
    ?>
<div id="searchcontent">
   <!-- ***************************************** -->
        <div class="prettyprint pop_over" style="background-color: white; border-collapse: collapse; border-width: 0px; border-bottom-width: 1px;" ></div>
           <?php if(!empty($contents)){ ?> 
            <?php foreach ($contents as $content) : ?>
                    <?php 
                    //pr($content);
                    //organization_description
                        $org_desc = '';
                        $org_belong = '';
                        $showdiv = '';
                        if(!empty($content['ShowListContent']['organization_description'])){
                            $org_desc  = $content['ShowListContent']['organization_description']['description'];
                        }else{
                           if(!empty($content['ShowListContent']['old_organization_description'])){
                            $org_desc  = $content['ShowListContent']['old_organization_description']['description'];
                           } 
                        }
                        if(!empty($content['ShowListContent']['organization_belong'])){
                            $org_belong = '&nbsp;-&nbsp;'.$content['ShowListContent']['organization_belong'];
                        }

                        if(!empty($org_desc) && !empty($org_belong)){
                            $showdiv = '['.$org_desc.$org_belong.']';
                        }


                    ?>
            <div id="showmorecontent">
                    <div class="prettyprint" style="background-color: white; border-collapse: collapse; border-width: 1px; border-top-width: 0px;" >
                    <?php echo $this->Time->format('d M y', $content['ShowListContent']['event_date']).'&nbsp;&nbsp;&nbsp;'; ?>
                    <a class="pop_over"  data-content="<?php echo $content['ShowListContent']['gist'] ?>" data-placement="bottom" href="<?= $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $content['ShowListContent']['id']. '&category_id='.$this->request->query['category_id']  ); ?>">
                        <?php 
                            echo empty($content['ShowListContent']['title']) ? __('No title') : 
						strip_tags(htmlspecialchars_decode($content['ShowListContent']['title']));/*mb_substr(strip_tags(htmlspecialchars_decode($content['ShowListContent']['title'])), 0, 256, 'UTF-8')*/
                        ?>
                    </a>
                                <?php 
                                $first = 1;
                                $count = count($content['PortalAttachment']);
                                if($count != 0){
                                 echo ' - ';  
                                    foreach($content['PortalAttachment'] as $attachment){
                                        
                                        if($first == $count){
                                            
                                            echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($attachment['ResourceContent']['key']) . '"><i class="icon-adt_atach"></i></a>';
                                            break;
                                        }else{
                                            echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($attachment['ResourceContent']['key']) . '"><i class="icon-adt_atach"></i></a>';
                                        }
                                        $first++;
                                    }
                                }
                                ?>

                      <?php if($currentUser['AuthUser']['id'] == $content['ShowListContent']['user_id']){ ?>
                       <span class="pull-right">
                           <a href="<?= $this->Portal->makeUrl($this->name, 'edit', 'content_id=' . $content['ShowListContent']['id'] . '&category_id=' . $content['ContentCategory'][0]['category_id']); ?>">
                               <i class='icon-pencil'></i></a>  | <a href="<?= $this->Portal->makeUrl($this->name, 'delete', 'content_id=' . $content['ShowListContent']['id']. '&category_id='.$this->request->query['category_id'] ); ?>" onclick="return confirm('<?=__('Confirm to delete')?> ?')"><i class='icon-trash'></i></a>


                                   </span>
                      <?php } ?> <br />

                    <span title="<?= $this->Time->timeAgoInWords($content['ShowListContent']['created_date']) ?>">
                    <a id="userProfile" data-toggle="modal" data-backdrop="static" href="#myModal" user_id="<?= $content['ShowListContent']['user_id'] ?>" style="color:black; font-size: smaller">
                        <?= $content['ShowListContent']['content_updated_user']; ?>
                    </a>
                        &nbsp;<?php echo "<span style='font-size:smaller;'>". $showdiv; ?>
                        - <?= $this->Time->format('Y-m-d H:i:s', $content['ShowListContent']['created_date'])  . "</span>" ?>
                    </span>
                    <?php if ($content['ShowListContent']['is_comment'] == 'Y' && $content['ShowListContent']['count_comment'] > 0) : ?>
                    <span title="<?php
                          if (isset($content['CommentUserProfile']['UserProfile'])) {
                              foreach ($content['CommentUserProfile']['UserProfile'] as $i => $user) {
                                  echo $user['UserProfile']['first_name_th'] . ' ' . $user['UserProfile']['last_name_th'];
                                  if (isset($content['CommentUserProfile']['UserProfile'][$i + 1])) {
                                      echo '&#13;';
                                  }
                              }
                          }
                          ?>">
                          <?php //'<i class="splashy-comments"></i> ' . $content['Content']['count_comment']; ?>
                        <span class="label label-info"><?= $content['ShowListContent']['count_comment'] ?> <?php echo __('Comments');?></span> 
                    </span>
                      <?php endif; ?>
                   <span class="label label-info"><?= $content['ShowListContent']['count_view'] ?> <?php echo __('Views');?></span>

                </div>    
            </div>
        <?php endforeach; ?>
        <?php } ?>

        <div id="viewmore_result"></div><br />



        <input id="offset" type="hidden" value="<?php echo @$offset; ?>"/>
        <input id="countcontent" type="hidden" value="<?php echo @$countContent; ?>" />
        <input id="category_id" type="hidden" value="<?php echo $this->request->query['category_id']; ?>" />
        <input id="search" type="hidden" value="<?php echo @$this->request->query['keyword']; ?>" />
        <input id="order" type="hidden" value=""/>
        <input id="period" type="hidden" value=""/>
        <input id="contype" type="hidden" value="viewall"/>
        <input id="page" type="hidden" value="1"/>
        <div>
            <span class="pull-right"><?php //echo $this->Paginator->numbers(); ?></span>
            <div id="viewmore" class="btn" style="cursor: pointer; width: 97%"><?php echo __('View More...');?></div>
        </div>
</div>


<script>
    
    $('#reversion').click(function() {
        $('#box').load('/articles/reversion/');
    });
    
    $('#submitSearch').click(function() {
        var values = $('#searchContent').serialize();
                $.ajax({
                    cache: false,
                    url: "/Contents/search?_="+Math.random(),
                    type: "post",
                    data : values,
                    success:function(data){
                       $('#searchcontent').empty().append(data);
                    }

                });	  
/*$.get(
                '/Contents/search',
                values,
                function(data){
                    $('#searchcontent').empty().append(data);
                    //$('#values').val(values);
                },
                'html'
        );*/
    });
    
    $('#viewmore').click(function(){
        var offset = parseInt($('#offset').val());
        var category_id = $('#category_id').val();
        var keyword = $('#search').val();
        var totalContents = $('#countcontent').val();
        var contype = $('#contype').val();
        var order = $('#order').val();
        var period = $('#period').val();
        var page = parseInt($('#page').val());
        
        $('#viewmore').text('Loading...');
        //alert('offset = ' + offset + '; total = ' + totalContents);
        
        $.post('/Contents/ajaxsearch', {offset : offset, category_id : category_id, keyword : keyword, contype : contype, order:order, period: period, page: page}, function(data){
           $('#viewmore_result').append(data);
           
           offset = offset + 20;
           page = page +1;
           $('#offset').val(offset);
           $('#page').val(page);
           
           $('#viewmore').text('View More...');
           if(offset >= totalContents){ $('#viewmore').hide(); }
        });
    });
    
    $('input#keyword').keypress(function(event) {
        // detect ENTER key
        if (event.keyCode == 13) {
            // simulate submit button click
            //$("#btn-submit").click();
            // stop form from submitting via ENTER key press
            event.preventDefault ? event.preventDefault() : event.returnValue = false;
        }
    });
    
    $(function(){
        var offset = parseInt($('#offset').val());
        var totalContents = $('#countcontent').val();
        if(offset >= totalContents){ $('#viewmore').hide(); }
        
        
    });
    
</script>   
   <!-- ***************************************** -->


