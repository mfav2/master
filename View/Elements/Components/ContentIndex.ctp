<div class="row-fluid">
    <?php
    $index = 0;
    $background = '';
    $image = '';
    
    foreach ($subscribles as $category) :
          if(!empty($category['Subscrible']['color'])){
              //$background = 'style="background:'.$category['Subscrible']['color'].'"';
              $font_color =  'style="font-size:20px; font-weight:bold; color:'.$category['Subscrible']['color'].'"';
          }
          if(!empty($category['Subscrible']['image'])){
              $image = '<img src="img/custom/'.$category['Subscrible']['image'].'" alt="" width="26px" height="26px" />'; 
          } 

        ?>
        <div class="span6">
            <div class="span12" style="clear:both; border-bottom:2px solid #ccc; padding:0 0 5px 0; margin:0 0 5px 0;">
                <span class="pull-left" <?php echo $font_color; ?>><?= $image."&nbsp;".$category['getCategory']['description'] ?></span>
                <span class="pull-right"><a href="<?= $this->Portal->makeUrl($this->name, 'add', 'category_id=' . $category['Category']['id']); ?>"
                       class="btn btn-small" title="<?=__('Add New')?>">
                        <i class="icon-pencil"></i> <?=__('Add New')?>
                    </a>                                    
                    <a href="<?= $this->Portal->makeUrl($this->name, 'viewall', 'category_id=' . $category['Category']['id']); ?>"
                       class="btn btn-small" title="<?=__('View All')?>">
                        <i class="icon-eye-open"></i> <?=__('View All')?>
                    </a>
                </span>
                
            </div>
            <table class="table table-striped table-bordered" data-provides="rowlink">
                <tbody>
                    <?php foreach ($category['Contents'] as $content) : ?>
		    <?php
			$title = htmlspecialchars_decode($content['Content']['title'], ENT_QUOTES);
			$title = htmlspecialchars_decode($title);
			$title = strip_tags($title); 
			
			$gist = htmlspecialchars_decode($content['Content']['gist'], ENT_QUOTES);
			$gist = htmlspecialchars_decode($gist);
			$gist = strip_tags($gist); 
		    ?>
                    
                    <tr class="rowlink pop_over" data-content="<?php echo $gist; ?>"
                    data-placement="bottom"   
                     onclick="location = '<?= $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $content['Content']['id']); ?>'"
                    >
                            <td>
                                <?php 
//                                    if ($content['Content']['PortalAttachment'] != null) {
//                                        echo '<i class="icon-adt_atach"></i> ';
//                                    }
                                echo $this->Time->format('d M y', $content['Content']['created_date']).'&nbsp;&nbsp;&nbsp;';

				echo mb_substr($title, 0, 48, 'UTF-8');

                                if (mb_strlen($content['Content']['title']) > 48) {
                                    echo'...';
                                }
                                ?>
                                
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>                
            </table>

        </div>   
        <?php
        if ($index++ % 2 == 1) {
            echo '</div><div class="row-fluid">';
        }
    endforeach;
    ?>
</div>