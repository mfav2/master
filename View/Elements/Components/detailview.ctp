<?php
$templateMeta = $viewDefs[$this->name]['DetailView']['templateMeta'];
$buttons = $templateMeta['form']['buttons'];
$panels = $viewDefs[$this->name]['DetailView']['panels'];
?>    
    <div class="row-fluid">
	<div class="span12">

		<?php foreach ($panels as $panel) { ?>
		<h3 class="heading"><?php echo $panel['label'] ?></h3>
			<form class="form-horizontal well">
				<?php foreach ($panel['data'] as $panelDatas) { ?>
				<!--<div class="formSep">-->
				<div class="control-group">
					<div class="row-fluid">
						<?php foreach ($panelDatas as $panelData) { ?>
						<div class="span6">
							<label class="control-label"><?php echo $panelData['label'] ?></label>
							<div class="controls text_line">
								<strong><?php echo empty($viewDatas[$panelData['name']]) ? '' : $viewDatas[$panelData['name']] ?></strong>
							</div>
						</div>
						<?php }?>
					</div>
				</div>
				<?php }?>
			</form>
		<?php }?>

	</div>
    </div>