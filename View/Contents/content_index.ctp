<div class="row-fluid">
            <div class="span6">
            <div class="span12" style="clear:both; border-bottom:2px solid #ccc; padding:0 0 5px 0; margin:0 0 5px 0;">
                <span class="pull-left" style="font-size:20px; font-weight:bold; color:#3d7609;"><img src="img/custom/notice.png" alt="" width="26px" height="26px" />&nbsp;ข่าว Notices</span>
                <span class="pull-right"><a href="/main?pc=Contents&pa=add&pq=Y2F0ZWdvcnlfaWQ9Mw=="
                       class="btn btn-small" title="เพิ่ม">
                        <i class="icon-pencil"></i> เพิ่ม                    </a>                                    
                    <a href="/main?pc=Contents&pa=viewall&pq=Y2F0ZWdvcnlfaWQ9Mw=="
                       class="btn btn-small" title="ดูทั้งหมด">
                        <i class="icon-eye-open"></i> ดูทั้งหมด                    </a>
                </span>
                
            </div>
            <table class="table table-striped table-bordered" data-provides="rowlink">
                <tbody>
					
					<?php 
						foreach($query1 as $qr) { 
						$title = htmlspecialchars_decode($qr[0]['title'], ENT_QUOTES);
						$title = strip_tags($title); 
						$gist = htmlspecialchars_decode($qr[0]['gist'], ENT_QUOTES);
						$gist = strip_tags($gist); 

					?>

                    <tr class="rowlink pop_over" data-content="<?php echo $gist; ?>" data-placement="bottom"   
                     onclick="location = '<?= $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $qr[0]['id']); ?>'">
                         <td>
							<?php
							echo $this->Time->format('d M y', $qr[0]['created_date']).'&nbsp;&nbsp;&nbsp;';
							echo $title; 
							?>
						 </td>
                    </tr>
                   <?php } ?>
                </tbody>                
            </table>

        </div>   
		<!------------------------------------------------------------------------------------------------------->
                <div class="span6">
            <div class="span12" style="clear:both; border-bottom:2px solid #ccc; padding:0 0 5px 0; margin:0 0 5px 0;">
                <span class="pull-left" style="font-size:20px; font-weight:bold; color:#ff4040;"><img src="img/custom/news.png" alt="" width="26px" height="26px" />&nbsp;News Alert</span>
                <span class="pull-right"><a href="/main?pc=Contents&pa=add&pq=Y2F0ZWdvcnlfaWQ9MTM="
                       class="btn btn-small" title="เพิ่ม">
                        <i class="icon-pencil"></i> เพิ่ม                    </a>                                    
                    <a href="/main?pc=Contents&pa=viewall&pq=Y2F0ZWdvcnlfaWQ9MTM="
                       class="btn btn-small" title="ดูทั้งหมด">
                        <i class="icon-eye-open"></i> ดูทั้งหมด                    </a>
                </span>
                
            </div>
             <table class="table table-striped table-bordered" data-provides="rowlink">
                <tbody>
					
					<?php 
						foreach($query2 as $qr2) { 
						$title = htmlspecialchars_decode($qr2[0]['title'], ENT_QUOTES);
						$title = strip_tags($title); 
						$gist = htmlspecialchars_decode($qr2[0]['gist'], ENT_QUOTES);
						$gist = strip_tags($gist); 

					?>

                    <tr class="rowlink pop_over" data-content="<?php echo $gist; ?>" data-placement="bottom"   
                     onclick="location = '<?= $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $qr2[0]['id']); ?>'">
                         <td>
							<?php
							echo $this->Time->format('d M y', $qr2[0]['created_date']).'&nbsp;&nbsp;&nbsp;';
							echo $title; 
							?>
						 </td>
                    </tr>
                   <?php } ?>
                </tbody>                
            </table>

        </div>   
        </div>
		<!------------------------------------------------------------------------------------------------------->
		<div class="row-fluid">        <div class="span6">
            <div class="span12" style="clear:both; border-bottom:2px solid #ccc; padding:0 0 5px 0; margin:0 0 5px 0;">
                <span class="pull-left" style="font-size:20px; font-weight:bold; color:#225f7f;"><img src="img/custom/misc.png" alt="" width="26px" height="26px" />&nbsp;สัพเพเหระ</span>
                <span class="pull-right"><a href="/main?pc=Contents&pa=add&pq=Y2F0ZWdvcnlfaWQ9MTQ="
                       class="btn btn-small" title="เพิ่ม">
                        <i class="icon-pencil"></i> เพิ่ม                    </a>                                    
                    <a href="/main?pc=Contents&pa=viewall&pq=Y2F0ZWdvcnlfaWQ9MTQ="
                       class="btn btn-small" title="ดูทั้งหมด">
                        <i class="icon-eye-open"></i> ดูทั้งหมด                    </a>
                </span>
                
            </div>
           <table class="table table-striped table-bordered" data-provides="rowlink">
                <tbody>
					
					<?php 
						foreach($query3 as $qr3) { 
						$title = htmlspecialchars_decode($qr3[0]['title'], ENT_QUOTES);
						$title = strip_tags($title); 
						$gist = htmlspecialchars_decode($qr3[0]['gist'], ENT_QUOTES);
						$gist = strip_tags($gist); 

					?>

                    <tr class="rowlink pop_over" data-content="<?php echo $gist; ?>" data-placement="bottom"   
                     onclick="location = '<?= $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $qr3[0]['id']); ?>'">
                         <td>
							<?php
							echo $this->Time->format('d M y', $qr3[0]['created_date']).'&nbsp;&nbsp;&nbsp;';
							echo $title; 
							?>
						 </td>
                    </tr>
                   <?php } ?>
                </tbody>                 
            </table>

        </div>   
		<!------------------------------------------------------------------------------------------------------->
                <div class="span6">
            <div class="span12" style="clear:both; border-bottom:2px solid #ccc; padding:0 0 5px 0; margin:0 0 5px 0;">
                <span class="pull-left" style="font-size:20px; font-weight:bold; color:#f0aa17;"><img src="img/custom/hot.png" alt="" width="26px" height="26px" />&nbsp;ประเด็นร้อน</span>
                <span class="pull-right"><a href="/main?pc=Contents&pa=add&pq=Y2F0ZWdvcnlfaWQ9MTU="
                       class="btn btn-small" title="เพิ่ม">
                        <i class="icon-pencil"></i> เพิ่ม                    </a>                                    
                    <a href="/main?pc=Contents&pa=viewall&pq=Y2F0ZWdvcnlfaWQ9MTU="
                       class="btn btn-small" title="ดูทั้งหมด">
                        <i class="icon-eye-open"></i> ดูทั้งหมด                    </a>
                </span>
                
            </div>
            <table class="table table-striped table-bordered" data-provides="rowlink">
                <tbody>
					
					<?php 
						foreach($query4 as $qr4) { 
						$title = htmlspecialchars_decode($qr4[0]['title'], ENT_QUOTES);
						$title = strip_tags($title); 
						$gist = htmlspecialchars_decode($qr4[0]['gist'], ENT_QUOTES);
						$gist = strip_tags($gist); 

					?>

                    <tr class="rowlink pop_over" data-content="<?php echo $gist; ?>" data-placement="bottom"   
                     onclick="location = '<?= $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $qr4[0]['id']); ?>'">
                         <td>
							<?php
							echo $this->Time->format('d M y', $qr4[0]['created_date']).'&nbsp;&nbsp;&nbsp;';
							echo $title; 
							?>
						 </td>
                    </tr>
                   <?php } ?>
                </tbody>                 
            </table>

        </div>   
        </div><div class="row-fluid"></div>
</div>