<?php
    $currentUser = $this->Session->read('AuthUser');
?>
   <?php if(!empty($contents)){ ?> 
    <?php foreach ($contents as $content) : ?>
            <?php 
            
            //organization_description
                $org_desc = '';
                $org_belong = '';
                $showdiv = '';
                if(!empty($content['ShowListContent']['organization_description'])){
                    $org_desc  = $content['ShowListContent']['organization_description']['description'];
                }
                if(!empty($content['ShowListContent']['organization_belong'])){
                    $org_belong = '&nbsp;-&nbsp;'.$content['ShowListContent']['organization_belong'];
                }
                
                if(!empty($org_desc) && !empty($org_belong)){
                    $showdiv = '['.$org_desc.$org_belong.']';
                }
               
                
            ?>
<div id="showmorecontent">
            <div class="prettyprint pop_over" style="background-color: white; border-collapse: collapse; border-width: 1px; border-top-width: 0px;" >
                
            <?php echo $this->Time->format('d M y', $content['ShowListContent']['event_date']).'&nbsp;&nbsp;&nbsp;'; ?>
            <a class="pop_over" data-content="<?php echo $content['ShowListContent']['gist']; ?>" data-placement="bottom"   href="<?= $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $content['ShowListContent']['id']); ?>">
                <?php 
                    echo empty($content['ShowListContent']['title']) ? __('No title') : mb_substr(strip_tags(htmlspecialchars_decode($content['ShowListContent']['title'])), 0, 256, 'UTF-8');
                ?>
            </a>
			<?php 
                            $first = 1;
                            $count = count($content['PortalAttachment']);
                            if($count != 0){
                             echo ' - ';  
                                foreach($content['PortalAttachment'] as $attachment){

                                    if($first == $count){

                                        echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($attachment['ResourceContent']['key']) . '"><i class="icon-adt_atach"></i></a>';
                                        break;
                                    }else{
                                        echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($attachment['ResourceContent']['key']) . '"><i class="icon-adt_atach"></i></a>';
                                    }
                                    $first++;
                                }
                            }
                        ?>
       
              <?php if($currentUser['AuthUser']['id'] == $content['ShowListContent']['user_id']){ ?>
               <span class="pull-right">
                   <a href="<?= $this->Portal->makeUrl($this->name, 'edit', 'content_id=' . $content['ShowListContent']['id'] . '&category_id=' . $content['ContentCategory'][0]['category_id']); ?>">
                       <i class='icon-pencil'></i></a>  | <a href="<?= $this->Portal->makeUrl($this->name, 'delete', 'content_id=' . $content['ShowListContent']['id'] . '&category_id=' . $content['ContentCategory'][0]['category_id']); ?>" onclick="return confirm('<?=__('Confirm to delete')?> ?')"><i class='icon-trash'></i></a>


			   </span>
              <?php } ?> <br />
                
            <span title="<?= $this->Time->timeAgoInWords($content['ShowListContent']['created_date']) ?>">
            <a id="userProfile" data-toggle="modal" data-backdrop="static" href="#myModal" user_id="<?= $content['ShowListContent']['user_id'] ?>" style="color:black; font-size: smaller">
                <?= $content['ShowListContent']['content_updated_user']; ?>
            </a>
                &nbsp;<?php echo "<span style='font-size:smaller;'>". $showdiv; ?>
                - <?= $this->Time->format('Y-m-d H:i:s', $content['ShowListContent']['created_date'])  . "</span>" ?>
            </span>
            <?php if ($content['ShowListContent']['is_comment'] == 'Y' && $content['ShowListContent']['count_comment'] > 0) : ?>
            <span title="<?php
                  if (isset($content['CommentUserProfile']['UserProfile'])) {
                      foreach ($content['CommentUserProfile']['UserProfile'] as $i => $user) {
                          echo $user['UserProfile']['first_name_th'] . ' ' . $user['UserProfile']['last_name_th'];
                          if (isset($content['CommentUserProfile']['UserProfile'][$i + 1])) {
                              echo '&#13;';
                          }
                      }
                  }
                  ?>">
                  <?php //'<i class="splashy-comments"></i> ' . $content['Content']['count_comment']; ?>
                <span class="label label-info"><?= $content['ShowListContent']['count_comment'] ?> <?php echo __('Comments'); ?></span> 
            </span>
              <?php endif; ?>
           <span class="label label-info"><?= $content['ShowListContent']['count_view'] ?> <?php echo __('Views'); ?></span>

        </div>     
</div>    

<?php endforeach; ?>
<?php } ?>
<script src="bootstrap/js/bootstrap.js"></script>
<script>
$(".pop_over").popover({
    trigger: 'hover'
});
</script>
<?php //echo $this->element('sql_dump'); ?>