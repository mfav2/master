<?php // UPDATE BY BAIPHAI 2013-02-09 16:00
$currentUser = $this->Session->read('AuthUser');
    $permiss =  $currentUser['PermissionGroup'];
    $permisssubscribe = $currentUser['PermissionSubscribe'];
    
    $showadd = 'N';
    $showedit = 'N';
    $showdelete = 'N';
    $showaddsubgroup = 'N';
    foreach($permiss as $permission){
        if($permission['PermissionGroup']['function_id'] == 3 || $permission['PermissionGroup']['function_id'] == 8 || $permission['PermissionGroup']['function_id'] == 9){
            if($permission['PermissionGroup']['is_add'] == 'Y'){
                $showadd = 'Y'; 
            }
            if($permission['PermissionGroup']['is_edit'] == 'Y'){
                $showedit = 'Y';
            }
            if($permission['PermissionGroup']['is_delete'] == 'Y'){
                $showdelete = 'Y';
            }    
            if($permission['PermissionGroup']['is_add_sub_group'] == 'Y'){
                $showaddsubgroup = 'Y';
            }  
        }
    }

   if(!empty($categories)) { 
        foreach($categories as $category) {
           
            echo '<div style="margin:15px 0px; color:#006bd6;"><h4> - '.$category['getCategory']['detail']['description'].'</h4></div>'; 
            if(!empty($contents)){
                foreach($contents as $content){
                   $showdiv = '['.$content['Content']['organization_name'].' '.$content['Content']['belong_organization_name'].']';
                    if($content['getContentCategory']['sub_category_id'] == $category['getCategory']['id']) { ?>

                      <!-- Show Content -->
                      <div class="prettyprint " >
                          <?php echo $this->Time->format('d M y', $content['Content']['event_date']).'&nbsp;&nbsp;&nbsp;'; ?>
                            <a class="pop_over"  data-content="<?php echo $content['Content']['gist']; ?>" data-placement="bottom" href="<?= $this->Portal->makeUrl($this->name, 'view', 'main_cate='.$main_cate.'&content_id=' . $content['Content']['id']); ?>">
                                <?php 
                                    echo empty($content['Content']['title']) ? __('No title') : mb_substr(strip_tags(htmlspecialchars_decode($content['Content']['title'])), 0, 256, 'UTF-8');
                                ?>
                            </a>
                            <?php 
                                if(!empty($content['Content']['attachment1']) || !empty($content['Content']['attachment2']) || !empty($content['Content']['attachment3']) ){
                                    echo ' - ';
                                }
                                if(!empty($content['Content']['attachment1'])){
                                    echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment1']) . '"><i class="icon-adt_atach"></i></a>';
                                }
                                if(!empty($content['Content']['attachment2'])){
                                    echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment2']) . '"><i class="icon-adt_atach"></i></a>';
                                }
                                if(!empty($content['Content']['attachment3'])){
                                    echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment3']) . '"><i class="icon-adt_atach"></i></a>';
                                }

                            ?>
                            <?php if($currentUser['AuthUser']['id'] == $content['Content']['user_id'] || $currentUser['AuthUser']['username'] == 'pitak'){ ?>
                               <span class="pull-right">
                                   <?php if($showedit == 'Y'){ ?>
                                   <a href="<?php echo $this->Portal->makeUrl($this->name, 'edit', 'main_cate='.$main_cate.'&content_id=' . $content['Content']['id'] . '&category_id=' . $content['getContentCategory']['category_id']); ?>">
                                       <i class='icon-pencil'></i></a>  | 
                                   <?php } ?>    
                                   <?php if($showdelete == 'Y'){ ?>    
                                   <a href="<?php echo $this->Portal->makeUrl($this->name, 'delete', 'main_cate='.$main_cate.'&content_id=' . $content['Content']['id']. '&category_id='.$content['getContentCategory']['category_id'] ); ?>" onclick="return confirm('<?php echo __('Confirm to delete')?> ?')"><i class='icon-trash'></i></a>
                                   <?php } ?>
                               </span>
                            <?php } ?> <br />
                            <span title="<?php echo $this->Time->timeAgoInWords($content['Content']['created_date']); ?>">
                                <a id="userProfile" data-toggle="modal" data-backdrop="static" href="#myModal" user_id="<?php echo $content['Content']['user_id']; ?>" style="color:black; font-size: smaller">
                                    <?php echo $content['Content']['first_name'].' '.$content['Content']['last_name'] ?>
                                </a>
                                    &nbsp;<?php echo "<span style='font-size:smaller;'>".$showdiv; ?>
                                - <?= $this->Time->format('Y-m-d H:i:s', $content['Content']['created_date'])  . "</span>" ?>
                            </span>
                            <span class="label label-info"><?php echo $content['Content']['count_view']; ?> <?php echo __('Views');?></span>
                      </div>
                      <!-- End Content -->
  <?php
                    }
                }
            }
  ?>          
  <?php } ?>
<?php } ?>                      
</div>
