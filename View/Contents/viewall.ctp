<?php // UPDATE BY BAIPHAI 2013-02-09 16:00
$currentUser = $this->Session->read('AuthUser');
$permiss =  $currentUser['PermissionGroup'];
$permisssubscribe = $currentUser['PermissionSubscribe'];

$cate = array('3', '13', '14', '15');

    $showadd = 'N';
    $showedit = 'N';
    $showdelete = 'N';
    foreach($permiss as $permission){
        if($permission['PermissionGroup']['function_id'] == 3 || $permission['PermissionGroup']['function_id'] == 8 || $permission['PermissionGroup']['function_id'] == 9){
            if($permission['PermissionGroup']['is_add'] == 'Y'){
                $showadd = 'Y'; 
            }
            if($permission['PermissionGroup']['is_edit'] == 'Y'){
                $showedit = 'Y';
            }
            if($permission['PermissionGroup']['is_delete'] == 'Y'){
                $showdelete = 'Y';
            }      
        }
    }  
 
$periods = array('All', 'Last Month', 'Last Week', 'This Week', 'This Month', 'Next Week', 'Next Month');
$sorts = array('Create Date', /*'Publish Date',*/ 'Event Date', /* 'Title' */);
echo $this->element('Components/breadcrumb'); 
?>
<div class="row-fluid">
        <div class="span3">
        <?php 
            if(!in_array($this->request->query['category_id'], $cate)){
                if($showadd == 'Y'){
        ?>                    
            <a class="btn" href="<?php echo $this->Portal->makeURL($this->name, 'add', 'category_id=' . $this->request->query['category_id']); ?>"><?php echo __('Add New'); ?></a>
        <?php
                }
            }
        ?>
  
	</div>
	<div class="span9" style="text-align: right;">
        <form id="searchContent" action="<?php echo $this->Portal->makeUrl($this->name, 'search'); ?>" class="form-inline" method="post">
            <?php
            echo $this->Form->input('category_id', array('type' => 'hidden'));
            echo $this->Form->input('sub_id', array('type' => 'hidden'));
            if(isset($this->request->query['sub_id'])){
              echo $this->form->input('period', array(
                    'type' => 'select',
                    'label' => false,
                    'div' => false,
                    'style' => 'width:auto;',
                    'options' => $sub_cate,
                ));
              echo ' ';
            }
            echo __('Period').' : ';
            echo $this->form->input('period', array(
                'type' => 'select',
                'label' => false,
                'div' => false,
                'style' => 'width:auto;',
                'options' => $periods,
            ));
            echo __('Sort').' : ';
            echo $this->form->input('sort', array(
                'type' => 'select',
                'label' => false,
                'div' => false,
                'style' => 'width:auto;',
                'options' => $sorts,
            ));
            echo ' ';
            echo $this->Form->input('keyword', array('placeholder' => __('keyword'), 'label' => false, 'div'=>FALSE));
            echo ' ';
            echo $this->form->button(__('Search'), array('type'=>'button','id'=>'submitSearch','class' => 'btn', 'div'=>FALSE));
            ?>
            <?php if($this->request->query['category_id'] == '17' || $this->request->query['category_id'] == '18'){ ?>
                 <span class="btn"><a style="color:#000; text-decoration: none;" href="<?php echo $this->Portal->makeUrl($this->name, 'viewall','category_id='.$this->request->query['category_id']); ?>"><?php echo __('View All'); ?></a></span>
           <?php } ?>
        </form>
    </div>
</div>
<hr style='border:0.5px solid #058dc7; margin:0 0 10px 0;'/>
<div id="searchcontent">
	<div class="prettyprint pop_over" style="background-color: white; border-collapse: collapse; border-width: 0px; border-bottom-width: 1px;" ></div>
	<!-- **************** ดึงข้อมูลมาแสดง ************************ --> 
        <?php if(!empty($contents)){ 
            foreach ($contents as $content) : 
            $showdiv = '['.$content['Content']['organization_name'].' '.$content['Content']['belong_organization_name'].']';
        ?>
          
                
   
        <div id="showmorecontent">
		<div class="prettyprint" style="background-color: white; border-collapse: collapse; border-width: 1px; border-top-width: 0px;" >
                    <?php echo $this->Time->format('d M y', $content['Content']['event_date']).'&nbsp;&nbsp;&nbsp;'; ?>
                    <a class="pop_over"  data-content="<?php echo strip_tags(htmlspecialchars_decode($content['Content']['gist'])); ?>" data-placement="bottom" href="<?= $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $content['Content']['id']. '&category_id='.$this->request->query['category_id']  ); ?>">
                        <?php 
                            echo empty($content['Content']['title']) ? __('No title') : 
                            strip_tags(htmlspecialchars_decode($content['Content']['title']));/*mb_substr(strip_tags(htmlspecialchars_decode($content['ShowListContent']['title'])), 0, 256, 'UTF-8')*/
                        ?>
                    </a>
                    <?php 
                        
                        if(!empty($content['Content']['attachment1']) || !empty($content['Content']['attachment2']) || !empty($content['Content']['attachment3']) ){
                            echo ' - ';
                        }
                        if(!empty($content['Content']['attachment1'])){
                            echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment1']) . '"><i class="icon-adt_atach"></i></a>';
                        }
                        if(!empty($content['Content']['attachment2'])){
                            echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment2']) . '"><i class="icon-adt_atach"></i></a>';
                        }
                        if(!empty($content['Content']['attachment3'])){
                            echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment3']) . '"><i class="icon-adt_atach"></i></a>';
                        }
                       
                    ?>
                    
                       <span class="pull-right">
                        <?php if($showedit == 'Y'){ ?>
                           <a href="<?php echo $this->Portal->makeUrl($this->name, 'edit', 'content_id=' . $content['Content']['id'] . '&category_id=' . $content['getContentCategory']['category_id']); ?>">
                               <i class='icon-pencil'></i></a>  | 
                        <?php } ?>
                               
                        <?php if($showdelete == 'Y'){ ?>       
                           <a href="<?php echo $this->Portal->makeUrl($this->name, 'delete', 'content_id=' . $content['Content']['id']. '&category_id='.$content['getContentCategory']['category_id'] ); ?>" onclick="return confirm('<?php echo __('Confirm to delete')?> ?')"><i class='icon-trash'></i></a>
                        <?php } ?>       
                       </span>
                    <br />
                    <span title="<?php echo $this->Time->timeAgoInWords($content['Content']['created_date']); ?>">
                    <a id="userProfile" data-toggle="modal" data-backdrop="static" href="#myModal" user_id="<?php echo $content['Content']['user_id']; ?>" style="color:black; font-size: smaller">
                        <?php echo $content['Content']['first_name'].' '.$content['Content']['last_name'] ?>
                    </a>
                        &nbsp;<?php echo "<span style='font-size:smaller;'>".$showdiv; ?>
                        - <?= $this->Time->format('Y-m-d H:i:s', $content['Content']['created_date'])  . "</span>" ?>
                    </span>
                     <?php if ($content['Content']['is_comment'] == 'Y' && $content['Content']['count_comment'] > 0) : ?>
                         <span class="label label-info"><?php echo $content['Content']['count_comment']; ?> <?php echo __('Comments');?></span> 
                     <?php endif; ?>
                   <span class="label label-info"><?php echo $content['Content']['count_view']; ?> <?php echo __('Views');?></span>
                </div>
	</div>
         <?php endforeach; ?>   
        <?php } ?>
        <!-- **************************************** -->
        <!--            Show view more                -->             
        <!-- **************************************** -->
        <div id="viewmore_result"></div><br />
        <!-- Keep Detail For view more -->
        <input id="offset" type="hidden" value="<?php echo @$offset; ?>"/>
        <input id="limit" type="hidden" value="<?php echo @$limit; ?>"/>
        <input id="countcontent" type="hidden" value="<?php echo @$countContent; ?>" />
        <input id="category_id" type="hidden" value="<?php echo $this->request->query['category_id']; ?>" />
        <input id="search" type="hidden" value="<?php echo @$this->request->query['keyword']; ?>" />
        <input id="order" type="hidden" value=""/>
        <input id="period" type="hidden" value=""/>
        <input id="contype" type="hidden" value="viewall"/>
        <input id="controller" type="hidden" value="<?php echo $this->name; ?>" />
        <input id="has_expire" type="hidden" value="<?php echo $has_expire; ?>" />
        <input id="page" type="hidden" value="1"/>
        <div>
           <span class="pull-right"><?php //echo $this->Paginator->numbers(); ?></span>
           <div id="viewmore" class="btn" style="cursor: pointer; width: 97%"><?php echo __('View More...');?></div>
        </div>

</div>

<script>
$('#viewmore').click(function(){
    var offset = parseInt($('#offset').val());
    var limit = $('#limit').val();
    var category_id = $('#category_id').val();
    var keyword = $('#search').val();
    var totalContents = $('#countcontent').val();
    var contype = $('#contype').val();
    var order = $('#order').val();
    var period = $('#period').val();
    var page = parseInt($('#page').val());
    var controller = $('#controller').val();
    var expire = $('#has_expire').val();
   
    $('#viewmore').text('Loading...');
     $.ajax({
          cache: false,
          url: '/'+controller+'/viewmore?_='+Math.random(),
          type: 'POST',
          data: {'expire':expire,'limit':limit,'countcontent':totalContents, 'offset' : offset, 'category_id' : category_id, 'keyword' : keyword, 'contype' : contype, 'order':order, 'period': period, 'page': page },
          success:function(data){

                $('#viewmore_result').append(data);
                offset = offset + 10;
                page = page +1;
                $('#offset').val(offset);
                $('#page').val(page);
                $('#viewmore').text('View More...');
                if(offset >= totalContents){ $('#viewmore').hide(); }       

          }
     });
});
$('#submitSearch').click(function(){
    var values = $('#searchContent').serialize();
    var controller = $('#controller').val();
    $('#searchcontent').empty().text('Loading...');
    $.ajax({
        cache: false,
        url: "/"+controller+"/search?_="+Math.random(),
        type: "POST",
        data : values,
        success:function(data){
             $('#searchcontent').empty().append(data);
            //$('#searchcontent').empty().append(data);
        }

    });
});

$('#keyword').keypress(function(e) {
    if (e.which == "13") { 
        $('#submitSearch').click();
        return false;
        //enter pressed 
    } 

});
$(function(){
    var offset = parseInt($('#offset').val());
    var totalContents = $('#countcontent').val();
    if(offset >= totalContents){ $('#viewmore').hide(); }
});
</script>