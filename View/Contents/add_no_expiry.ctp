<?php
echo $this->element('Components/breadcrumb'); 
echo $this->Form->create($modelClass, array('action' => '$action', 'url'=> array('controller' => $this->name, 'action' => 'add'), 'type'=>'file', 'class' => 'form-horizontal well'));
echo $this->Form->input('id', array('type' => 'hidden'));
echo $this->Form->input('main_cate', array('type' => 'hidden'));
echo $this->Form->input('category_id', array('type' => 'hidden'));
echo $this->Form->input('sub_category_id', array('type' => 'hidden'));

echo $this->Form->input('expiry_date', array(
'div' => false, 'class' => 'span10 required', 'label' => false, 'readonly' => 'readonly', 'type' => 'hidden','style' => 'margin:0px 0px 0px 10px;')
);


if($chk_comment != 'S'){
  if($chk_comment == 'Y'){
      $is_comment = 1;
  }else{
      $is_comment = 0;
  }  
  echo $this->Form->input('is_comment', array('type' => 'hidden', 'value' => $is_comment)); 
}
?>
<div class="control-group">
    <div class="row-fluid">
        <div class="span12">
            <table cellpadding="5" cellspaing="2" border="0" width="100%">
                <?php if(!empty($subcategory)) { ?>
                <tr>
                    <td width="10%">
                        <label class="control-label">
                           <?php echo __('Sub Category'); ?>
                        </label>
                    </td>
                    <td>
                        <?php
                        echo $this->Form->select('sub_category_id', $subcategory, array(
                            'legend' => false, 
                            'default' => 4, 
                            'div' => false, 
                            'label' => false, 
                            'style' => 'margin:0px 0px 0px 10px;', 
                            'empty' => false
                        ));
                        echo '<span class="subcategory-error" style="color:#F00;"></span>';     
                        ?>
                        
                    </td>
                </tr>
                <?php } ?>
                <tr>
                   <td width="10%" val ign="top">
                        <label class="control-label">
                           <?php echo __('Event Date'); ?>
                        </label>
                    </td>
                    <td>
                        <div class="input-append date" id="dp_start">
                             <!--<input class="span6" type="text" readonly="readonly" /><span class="add-on"><i class="splashy-calendar_day_up"></i></span>-->
                            <?php
                                echo $this->Form->input('event_date', array(
                                         'div' => false, 'class' => 'span10 required', 'label' => false, 'readonly' => 'readonly', 'type' => 'text','style' => 'margin:0px 0px 0px 10px;')
                                     );
                            ?>
                            <span class="add-on"><i class="splashy-calendar_day_up"></i></span>
                        </div>
                    </td>
                </tr>
                
                <tr>
                    <td valign="top">
                        <label class="control-label">
                           <?php echo __('Subject'); ?>
                        </label>
                    </td>
                    <td>
                        <?php
                            echo $this->Form->input('title', array(
                                'div' => false,
                                'class' => 'span8 required',
                                'label' => false,
                                'maxlength'=>1024,
                                'style' => 'margin:0px 0px 0px 10px;'
                                )
                            );
                        ?>
                        <span class="subject-error" style="color:#F00;"></span> 
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label class="control-label">
                           <?php echo __('Description'); ?>
                        </label>
                    </td>
                    <td>
                        <?php
                            echo $this->Form->textarea('gist', array('div' => false, 'class' => 'span8 required', 'label' => false, 'style' => 'height:70px; margin:0px 0px 0px 10px;'));
                            
                        ?> 
                        <span class="gist-error" style="color:#F00;"></span>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label class="control-label">
                           <?php echo __('Detail'); ?>
                        </label>
                    </td>
                    <td>
                        <?php
                            echo $this->Form->textarea('description', array('div' => false, 'class' => 'span8 required wysiwg_mini', 'label' => false, 'style' => 'height:100px; margin:0px 0px 0px 10px;'));
                            
                        ?>
                        <span class="detail-error" style="color:#F00;"></span>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label class="control-label">
                           <?php echo __('Attachment 1'); ?>
                        </label>
                    </td>
                    <td>
                       <?php
                            echo $this->Form->input('attachment1', array('type' => 'file', 'label' => false ,
                                'style' => 'margin:0px 0px 0px 10px;'
                                )
                            );
                        ?>
                        <span style="margin-left:10px;"  class="help-block"><?php echo __('Max File Size : ')." ".$size; ?></span>
                        <span class="attach1-error-type" style="color:#F00;"></span>
                        <span class="attachment1-error-block" style="color:#F00; display:none;"><?php echo __('File over limit size').' '.$size; ?></span>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <label class="control-label">
                           <?php echo __('Attachment 2'); ?>
                        </label>
                    </td>
                    <td>
                       <?php
                            echo $this->Form->input('attachment2', array('type' => 'file', 'label' => false ,
                                'style' => 'margin:0px 0px 0px 10px;'
                                )
                            );
                        ?> 
                        <span style="margin-left:10px;"  class="help-block"><?php echo __('Max File Size : ')." ".$size; ?></span>
                        <span class="attach2-error-type" style="color:#F00;"></span>
                        <span class="attachment2-error-block" style="color:#F00; display:none;"><?php echo __('File over limit size').' '.$size; ?></span>
                    </td>
                </tr>
                 <tr>
                    <td valign="top">
                        <label class="control-label">
                           <?php echo __('Attachment 3'); ?>
                        </label>
                    </td>
                    <td>
                       <?php
                            echo $this->Form->input('attachment3', array('type' => 'file', 'label' => false ,
                                'style' => 'margin:0px 0px 0px 10px;'
                                )
                            );
                        ?> 
                        <span style="margin-left:10px;"  class="help-block"><?php echo __('Max File Size : ')." ".$size; ?></span>
                        <span class="attach3-error-type" style="color:#F00;"></span>
                        <span class="attachment3-error-block" style="color:#F00; display:none;"><?php echo __('File over limit size').' '.$size; ?></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                           <?php  
                              echo $this->Form->checkbox('is_notification', array('hiddenField' => false ,
                                'style' => 'margin:0px 0px 0px 10px;'));
                           ?>
                           &nbsp;<span><?php echo __('Check to alert readers'); ?></span>
                    </td>
                </tr>
                <?php if($chk_comment == 'S') { ?>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                           <?php  
                              echo $this->Form->checkbox('is_comment', array('hiddenField' => false ,
                                'style' => 'margin:0px 0px 0px 10px;'));
                           ?>
                           &nbsp;<span><?php echo __('Allow Comment'); ?></span>
                    </td>
                </tr>
                <?php } ?>
               
                <tr>
                    <td valign="top">
                       <label class="control-label">
                          <?php echo __('Readers'); ?>
                        </label>
                    </td>
                    <td>
                       <div class="form-inline">
                          <?php echo $this->Form->radio('notification_type', 
                                  array('A' => __('Everyone'), 'D' => __('My Department'),'C' => __('Custom')), 
                                  array('legend' => false, 'default' => 'A', 'style' => 'margin: 10px 10px;')
                                ); 
                          ?>
                       </div>
                        <div class="taghandler">
                            <ul id="array_content_users" class="span8" style="background: #FFF; margin: 0px 0px 0px 0px; padding:5px;" ></ul>
                            <span class="error-choose-user" style="color:#F00;"></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><?php echo $this->Form->button(__('Add'), array('id' => 'submitForm', 'class' => 'btn', 'type'=> 'button', 'style'=>'margin:0 0 0 10px;')); ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<?php 
    echo $this->Form->end();
?>

<!-- Script -->

<!-- tree -->
<script src="lib/jstree/jquery.jstree.js"></script>
<!-- tag handler (recipients) -->
<script src="lib/tag_handler/jquery.taghandler.min.js"></script>
<link rel="stylesheet" href="lib/tag_handler/css/jquery.taghandler.css" />
<!-- autosize textareas -->
<script src="js/forms/jquery.autosize.min.js"></script>
<!-- TinyMce WYSIWG editor -->
<script src="lib/tiny_mce/jquery.tinymce.js"></script>
<script src="js/custom/custom_content_forms.js"> </script>
<script>    
$(document).ready(function() {
  /************************* Check Notification ***************************/
        if($('#ContentIsNotification').attr("checked")=="checked"){ 
            $("#ContentIsNotification").val('1');
            $("#ContentIsNotification").attr("checked","checked");
        }else{ 
            $("#ContentIsNotification").val('0');
            $("#ContentIsNotification").removeAttr("checked","checked");
        }
        
        $('#ContentIsNotification').click(function(){
              if($(this).attr("checked")=="checked"){ 
                 
                  $("#ContentIsNotification").val('1');
                  $("#ContentIsNotification").attr("checked","checked");
              }else{ 
                  
                  $("#ContentIsNotification").val('0');
                  $("#ContentIsNotification").removeAttr("checked","checked");
              }
        });
  /************************* Check Comment ********************************/
         if($('#ContentIsComment').attr("checked")=="checked"){ 
            $("#ContentIsComment").val('1');
            $("#ContentIsComment").attr("checked","checked");
        }else{ 
            $("#ContentIsComment").val('0');
            $("#ContentIsComment").removeAttr("checked","checked");
        }
        
        $("#ContentIsComment").click(function(){
              if($(this).attr("checked")=="checked"){ 
                  $("#ContentIsComment").val('1');
                  $("#ContentIsComment").attr("checked","checked");
              }else{ 
                 $("#ContentIsComment").val('0');
                 $("#ContentIsComment").removeAttr("checked","checked");
              }
        }); 
  
  /************************* Tag Handler **********************************/  
        if($('#ContentNotificationTypeA').attr("checked")=="checked"){ 
           $('#array_content_users').hide(); 
        }
    
        $('#array_content_users').tagHandler();
        $("ul#array_content_users li.tagInput").hide();

  /* *************************** Time ********************************* */
    
        //var pdate = '<?php //echo date('d/m/Y'); ?>';
        var eventdate = '<?php echo date('d/m/Y'); ?>';
        var exdate = '<?php echo date('d/m/Y'); ?>';
        $('#ContentEventDate').val(eventdate);
        $('#ContentExpiryDate').val(exdate);  
        
        $('#dp1').datepicker({format: "dd/mm/yyyy"}).on('changeDate', function(ev){
            $('#dp1').datepicker('hide');
        });
        $('#dp_start').datepicker({format: "dd/mm/yyyy"}).on('changeDate', function(ev){
                var dateText = $(this).data('date');

                var endDateTextBox = $('#dp_end input');
                if (endDateTextBox.val() != '') {
                        var testStartDate = new Date(dateText);
                        var testEndDate = new Date(endDateTextBox.val());
                        if (testStartDate > testEndDate) {
                                endDateTextBox.val(dateText);
                        }
                }
                else {
                        endDateTextBox.val(dateText);
                };
                $('#dp_end').datepicker('setStartDate', dateText);
                $('#dp_start').datepicker('hide');
        });
       /* $('#dp_end').datepicker({format: "dd/mm/yyyy"}).on('changeDate', function(ev){
                var dateText = $(this).data('date');
                var startDateTextBox = $('#dp_start input');
                var startdate = $('#dp_start input').val();
                
                if (startdate != '') {
                    
                        var start = convertdate(startdate);
                        var end = convertdate(dateText);
                        
                        var testStartDate = new Date(start);
                        var testEndDate = new Date(end);
                        if (testEndDate > testStartDate) {
                              $('#dp_end').datepicker('setStartDate', startdate);
                        }
                }
                else {
                        //startDateTextBox.val(dateText);
                }
                $('#dp_start').datepicker('setEndDate', dateText);
                $('#dp_end').datepicker('hide');
        });*/ 

        
});


function convertdate(date_str){
    var datearray = date_str.split("/");
    var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
    return newdate;
}	
/* *********************************************************************** */
        
        //    Tag handler
        
        $('#ContentNotificationTypeA').click(function() {
                $('#array_content_users').hide(); 
                //$('#array_content_users').attr("disabled", true);     
        });

        $('#ContentNotificationTypeD').click(function() {
                $('#array_content_users').show(); 
                //$('#array_content_users').attr("disabled", true);     
        });

        $('#ContentNotificationTypeC').click(function() {
                $('#array_content_users').show(); 
                //$('#array_content_users').attr("disabled", true);     
        });
        
/* *********************************************************************** */

/* *************************** Select Attach File ************************ */
        $('#ContentAttachment1').bind('change', function() {
            $('.attach1-error-type').empty();
            $('.attachment1-error-block').css('display','none');
             err = checkfilesize(this.files[0].size);
             if(err == false){
                 $('.attachment1-error-block').css('display','block');
             }
        });
        $('#ContentAttachment2').bind('change', function() {
             $('.attach2-error-type').empty();
             $('.attachment2-error-block').css('display','none');
             err = checkfilesize(this.files[0].size);  
             if(err == false){
                 $('.attachment2-error-block').css('display','block');
             }
        });
        $('#ContentAttachment3').bind('change', function() {
            $('.attach3-error-type').empty();
            $('.attachment3-error-block').css('display','none');
            err = checkfilesize(this.files[0].size);
            if(err == false){
                 $('.attachment3-error-block').css('display','block');
             }
        });
            
 function checkfilesize(filesize){
    var chk ='';
    var maxsize = '<?php echo Configure::read('Config.MaxFileSize'); ?>';
                    if(filesize > maxsize){
                        chk = false;
                    }else{
                        chk = true;
                    }
                    
    return  chk;                
 }   
 
 function deleteattach(id,conid,catid){
     //alert(id+' '+conid+ ' '+catid);
              if(confirm('<?php echo __("Do you want to delete this files?"); ?> ')){
                    //window.location.href="/appointments/insertappointment"
              
                  $.ajax({
                    cache: false,
                    url: "/contents/deleteAttachment",
                    type: "post",
                    data: {'id':id, 'content_id':conid, 'category_id':catid },
                    success:function(data){
                        if(data == 1){
                            location.reload();
                        }else{
                            alert(data);
                        }
                    }
                  });    
              }else{
                  return false;   
              }
 }
 
 function checktypefile(file){
            var exts = ['doc', 'docx', 'txt', 'pdf', 'jpg', 'jpeg', 'png', 'gif', 
                        'xls', 'xlsx', 'ppt', 'pptx', 'bmp','zip', 'rar', '7z', 'swf'];
            if (file) {
                 // split file name at dot
                 var get_ext = file.split('.');
                 // reverse name to check extension
                 get_ext = get_ext.reverse();
                 var chktype = $.inArray ( get_ext[0].toLowerCase(), exts ) > -1;
                 // check file type is valid as given in 'exts' array
                 if (chktype){
                   return 'Y';
                 } else {
                   return 'N';
                 }
             }   
             
 }            
/*****************************************************************************/
$('button#submitForm').click(function() {
    var err = false;
    // clear show red message
    $('.subcategory-error').empty();
    $('.subject-error').empty();
    $('.detail-error').empty();
    $('.gist-error').empty();
    $('.attach1-error-type').empty();
    $('.attach2-error-type').empty();
    $('.attach3-error-type').empty();
    $('.error-choose-user').empty();
    $('input[id=reader]').remove();
   
   //  get value
    var txt = $("#ContentTitle").val();
    var detail = $("#ContentDescription").val();
    var gist = $("#ContentGist").val();
    var chkselected = $('select[id="ContentSubCategoryId"] option:selected').text();
    /******************* check type file *********************/
    var file1 = $('#ContentAttachment1').val();
    var file2 = $('#ContentAttachment2').val();
    var file3 = $('#ContentAttachment3').val();
    var noti_all = $('#ContentNotificationTypeA:checked').val();
    
    /* ********************* Check Type File **********************************/
    if(file1 != ''){
        var filetype1 = checktypefile(file1);         
        if(filetype1 == 'N'){
            $('.attach1-error-type').text('<?php echo __("Only file"); ?> <?php echo $extension; ?>');
            err = true;
        }
    }
    if(file2 != ''){
        var filetype2 = checktypefile(file2);         
        if(filetype2 == 'N'){
            $('.attach2-error-type').text('<?php echo __("Only file"); ?> <?php echo $extension; ?>');
            err = true;
        }
    }
    if(file3 != ''){
        var filetype3 = checktypefile(file3);         
        if(filetype3 == 'N'){
            $('.attach3-error-type').text('<?php echo __("Only file"); ?> <?php echo $extension; ?>');
            err = true;
        }
    }
    
    /* ***********************************************************************/
    if(chkselected == 'N/A'){
        $('.subcategory-error').text('<?=__('Please Choose Sub Category')?>.');  
        err = true;
    }
    if(txt == ''){
         $('.subject-error').text(' <?=__('Please Insert Title')?>.');  
        err = true;
    }
    if(detail == ''){
        $('.detail-error').text(' <?=__('Please Insert Detail')?>.');
        err = true;
    }
    if(gist == ''){
        $('.gist-error').text(' <?=__('Please Insert Description')?>.');
        err = true;
    }
    
    /******************* Check data in TagHandler ****************************/
    var i = 0;
    if(noti_all != 'A'){

        var l = $('ul#array_content_users li.tagItem').length;
        if(l == 0 ){
            $('.error-choose-user').text(' <?php echo __('Please Insert Reader'); ?>');
            err = true;
        }else{

            $('ul#array_content_users li.tagItem').each(function() {

                var type = $(this).attr('itemtype');
                if(type=="orgs"){
                    $('ul#array_content_users').after('<input type="hidden" id="reader" name="data[Invite][Orgs][' + i + '][id]" value="' + $(this).attr('org_id') + '" /><input type="hidden" id="reader" name="data[Invite][Orgs][' + i + '][organization_name]" value="' + $(this).attr('org_name') + '" />');    
                }else if(type=="user"){
                    $('ul#array_content_users').after('<input type="hidden" id="reader" name="data[Invite][Users][' + i + '][id]" value="' + $(this).attr('user_id') + '" /><input type="hidden" id="reader" name="data[Invite][Users][' + i + '][name]" value="' + $(this).attr('user_name') + '" />');
                }
                i++;
              
            });
        }
    } 
    /************************************************************************/
    if(err == true){
        return false;
    }else{
        this.form.submit();
    }
   
});
</script>