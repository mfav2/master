<?php
    $currentUser = $this->Session->read('AuthUser');
    $permiss =  $currentUser['PermissionGroup'];
    $permisssubscribe = $currentUser['PermissionSubscribe'];
    
    $showadd = 'N';
    $showedit = 'N';
    $showdelete = 'N';
    foreach($permiss as $permission){
        if($permission['PermissionGroup']['function_id'] == 3 || $permission['PermissionGroup']['function_id'] == 8 || $permission['PermissionGroup']['function_id'] == 9){
            if($permission['PermissionGroup']['is_add'] == 'Y'){
                $showadd = 'Y'; 
            }
            if($permission['PermissionGroup']['is_edit'] == 'Y'){
                $showedit = 'Y';
            }
            if($permission['PermissionGroup']['is_delete'] == 'Y'){
                $showdelete = 'Y';
            }    
        }
    }      
?>
    <?php if(!empty($contents)){ 
        foreach ($contents as $content) : 
        $showdiv = '['.$content['Content']['organization_name'].' '.$content['Content']['belong_organization_name'].']';
    ?>
<div id="showmorecontent">
    <div class="prettyprint pop_over" style="background-color: white; border-collapse: collapse; border-width: 1px; border-top-width: 0px;" >
        <?php echo $this->Time->format('d M y', $content['Content']['event_date']).'&nbsp;&nbsp;&nbsp;'; ?>
            <a class="pop_over"  data-content="<?php echo strip_tags(htmlspecialchars_decode($content['Content']['gist'])); ?>" data-placement="bottom" href="<?php echo $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $content['Content']['id']. '&category_id='.$category_id ); ?>">
                <?php 
                    echo empty($content['Content']['title']) ? __('No title') : 
                    strip_tags(htmlspecialchars_decode($content['Content']['title']));/*mb_substr(strip_tags(htmlspecialchars_decode($content['ShowListContent']['title'])), 0, 256, 'UTF-8')*/
                ?>
            </a>
             <?php 
                        if(!empty($content['Content']['attachment1']) || !empty($content['Content']['attachment2']) || !empty($content['Content']['attachment3']) ){
                            echo ' - ';
                        }
                        if(!empty($content['Content']['attachment1'])){
                            echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment1']) . '"><i class="icon-adt_atach"></i></a>';
                        }
                        if(!empty($content['Content']['attachment2'])){
                            echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment2']) . '"><i class="icon-adt_atach"></i></a>';
                        }
                        if(!empty($content['Content']['attachment3'])){
                            echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment3']) . '"><i class="icon-adt_atach"></i></a>';
                        }
                       
            ?>
            
                <span class="pull-right">
                    <?php if($showedit == 'Y'){ ?>
                    <a href="<?php echo $this->Portal->makeUrl($this->name, 'edit', 'content_id=' . $content['Content']['id'] . '&category_id=' . $content['getContentCategory']['category_id']); ?>">
                        <i class='icon-pencil'></i></a>  | 
                    <?php } ?>
                        
                    <?php if($showdelete == 'Y'){ ?>    
                        <a href="<?= $this->Portal->makeUrl($this->name, 'delete', 'content_id=' . $content['Content']['id']. '&category_id='.$category_id ); ?>" onclick="return confirm('<?php echo __('Confirm to delete')?> ?')"><i class='icon-trash'></i></a>
                    <?php } ?>    
                </span>
              <br />
             <span title="<?php echo $this->Time->timeAgoInWords($content['Content']['created_date']); ?>">
                    <a id="userProfile" data-toggle="modal" data-backdrop="static" href="#myModal" user_id="<?php echo $content['Content']['user_id']; ?>" style="color:black; font-size: smaller">
                        <?php echo $content['Content']['first_name'].' '.$content['Content']['last_name'] ?>
                    </a>
                        &nbsp;<?php echo "<span style='font-size:smaller;'>".$showdiv; ?>
                        - <?= $this->Time->format('Y-m-d H:i:s', $content['Content']['created_date'])  . "</span>" ?>
                    </span>
              <?php if ($content['Content']['is_comment'] == 'Y' && $content['Content']['count_comment'] > 0) : ?>
                  <span class="label label-info"><?= $content['Content']['count_comment'] ?> <?php echo __('Comments');?></span> 
              <?php endif; ?>
            <span class="label label-info"><?= $content['Content']['count_view'] ?> <?php echo __('Views');?></span>
    </div>
</div>        
     <?php endforeach; ?>
<?php } ?>

<script>
$(".pop_over").popover({
    trigger: 'hover'
});
</script>
<?php //echo $this->element('sql_dump'); ?>