<?php // UPDATE BY BAIPHAI 2013-02-09 16:00
$currentUser = $this->Session->read('AuthUser');
//pr($currentUser);
//pr($this->name);
$cate = array('3', '13', '14', '15'); 
$permiss =  $currentUser['PermissionGroup'];
$permisssubscribe = $currentUser['PermissionSubscribe'];

$showadd = 'N';
$showedit = 'N';
$showdelete = 'N';
foreach($permiss as $permission){
    if($permission['PermissionGroup']['function_id'] == 4 || $permission['PermissionGroup']['function_id'] == 5){
        if($permission['PermissionGroup']['is_add'] == 'Y'){
            $showadd = 'Y'; 
        }
        if($permission['PermissionGroup']['is_edit'] == 'Y'){
            $showedit = 'Y';
        }
        if($permission['PermissionGroup']['is_delete'] == 'Y'){
            $showdelete = 'Y';
        }     
    }
}        
$periods = array('All', 'Last Month', 'Last Week', 'This Week', 'This Month', 'Next Week', 'Next Month');
$sorts = array('Create Date', /*'Publish Date',*/ 'Event Date', /* 'Title' */);
echo $this->element('Components/breadcrumb'); 
?>
<div class="row-fluid">
    <div class="span3">
            &nbsp;
  
	</div>
	<div class="span9" style="text-align: right;">
        <form id="searchContent" action="<?php echo $this->Portal->makeUrl('Contents', 'search'); ?>" class="form-inline" method="post">
            <?php
            echo $this->Form->input('category_id', array('type' => 'hidden'));
            echo $this->Form->input('sub_id', array('type' => 'hidden'));
            if(isset($this->request->query['sub_id'])){
              echo $this->form->input('period', array(
                    'type' => 'select',
                    'label' => false,
                    'div' => false,
                    'style' => 'width:auto;',
                    'options' => $sub_cate,
                ));
              echo ' ';
            }
            echo __('Period').' : ';
            echo $this->form->input('period', array(
                'type' => 'select',
                'label' => false,
                'div' => false,
                'style' => 'width:auto;',
                'options' => $periods,
            ));
            echo __('Sort').' : ';
            echo $this->form->input('sort', array(
                'type' => 'select',
                'label' => false,
                'div' => false,
                'style' => 'width:auto;',
                'options' => $sorts,
            ));
            echo ' ';
            echo $this->Form->input('keyword', array('placeholder' => __('keyword'), 'label' => false, 'div'=>FALSE));
            echo ' ';
            echo $this->form->button(__('Search'), array('type'=>'button','id'=>'submitSearch','class' => 'btn', 'div'=>FALSE));
            ?>
            <?php if($this->request->query['category_id'] == '17' || $this->request->query['category_id'] == '18'){ ?>
                 <span class="btn"><a style="color:#000; text-decoration: none;" href="<?php echo $this->Portal->makeUrl('Boards', 'viewall','category_id='.$this->request->query['category_id']); ?>"><?php echo __('View All'); ?></a></span>
           <?php } ?>
        </form>
    </div>
</div>
<hr style='border:0.5px solid #058dc7; margin:0 0 10px 0;'/>
<div id="searchcontent">
    
     <?php foreach($getOrg as $organize){ 
                if(!empty($contents)){ 
                foreach($contents as $content){
                         if($content['Content']['div_created_id'] == $organize['id']){
                             echo '<div style="margin:15px 0px; color:#006bd6;"><h4> - '.$content['Content']['div_created_name'].'</h4></div>';?>
                             <div class="prettyprint pop_over" style="background-color: white; border-collapse: collapse; border-width: 0px; border-bottom-width: 1px;" ></div>   
                         <?php    
                             foreach($contents as $content){ 
                                 $showdiv = '['.$content['Content']['organization_name'].' '.$content['Content']['belong_organization_name'].']';
                                  if($content['Content']['div_created_id'] == $organize['id']){ ?>
    
                                <div id="showmorecontent">
                                    
                                        <div class="prettyprint" style="background-color: white; border-collapse: collapse; border-width: 1px; border-top-width: 0px;" >
                                            <?php echo $this->Time->format('d M y', $content['Content']['event_date']).'&nbsp;&nbsp;&nbsp;'; ?>
                                            <a class="pop_over"  data-content="<?php echo strip_tags(htmlspecialchars_decode($content['Content']['gist'])); ?>" data-placement="bottom" href="<?= $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $content['Content']['id']. '&category_id='.$this->request->query['category_id']  ); ?>">
                                                <?php 
                                                    echo empty($content['Content']['title']) ? __('No title') : 
                                                    strip_tags(htmlspecialchars_decode($content['Content']['title']));/*mb_substr(strip_tags(htmlspecialchars_decode($content['ShowListContent']['title'])), 0, 256, 'UTF-8')*/
                                                ?>
                                            </a>
                                            <?php 
                                                if(!empty($content['Content']['attachment1']) || !empty($content['Content']['attachment2']) || !empty($content['Content']['attachment3']) ){
                                                    echo ' - ';
                                                }
                                                if(!empty($content['Content']['attachment1'])){
                                                    echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment1']) . '"><i class="icon-adt_atach"></i></a>';
                                                }
                                                if(!empty($content['Content']['attachment2'])){
                                                    echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment2']) . '"><i class="icon-adt_atach"></i></a>';
                                                }
                                                if(!empty($content['Content']['attachment3'])){
                                                    echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment3']) . '"><i class="icon-adt_atach"></i></a>';
                                                }

                                            ?>
                                            <?php if($currentUser['AuthUser']['id'] == $content['Content']['user_id'] || $currentUser['AuthUser']['username'] == 'pitak'){ ?>
                                               <span class="pull-right">
                                              <!--  Check permission  -->     
                                                   <?php if($showedit == 'Y'){ ?>
                                                   <a href="<?php echo $this->Portal->makeUrl($this->name, 'edit', 'content_id=' . $content['Content']['id'] . '&category_id=' . $content['getContentCategory']['category_id']); ?>">
                                                       <i class='icon-pencil'></i></a>  | 
                                                   <?php } ?>    
                                                   <?php if($showdelete == 'Y'){ ?>    
                                                   <a href="<?php echo $this->Portal->makeUrl($this->name, 'delete', 'content_id=' . $content['Content']['id']. '&category_id='.$content['getContentCategory']['category_id'] ); ?>" onclick="return confirm('<?php echo __('Confirm to delete')?> ?')"><i class='icon-trash'></i></a>
                                                   <?php } ?>
                                              <!-- End -->      
                                               </span>
                                            <?php } ?> <br />
                                            <span style="font-size: normal;" title="<?php echo $this->Time->timeAgoInWords($content['Content']['created_date']); ?>">
                                            <a id="userProfile" data-toggle="modal" data-backdrop="static" href="#myModal" user_id="<?php echo $content['Content']['user_id']; ?>" style="color:black; font-size: smaller;">
                                                <?php echo $content['Content']['first_name'].' '.$content['Content']['last_name'] ?>
                                            </a>
                                                &nbsp;<?php echo "<span style='font-size:smaller;'>". $showdiv;  ?>
                                                - <?= $this->Time->format('Y-m-d H:i:s', $content['Content']['created_date'])  . "</span>" ?>
                                            </span>
                                             <?php if ($content['Content']['is_comment'] == 'Y' && $content['Content']['count_comment'] > 0) : ?>
                                                 <span class="label label-info"><?php echo $content['Content']['count_comment']; ?> <?php echo __('Comments');?></span> 
                                             <?php endif; ?>
                                           <span class="label label-info"><?php echo $content['Content']['count_view']; ?> <?php echo __('Views');?></span>
                                        </div>
                                </div>  
                             <?php       
                                  }
                             }
                             break;
                         }       
                    }
                }
           }
     ?>       
       
       
</div>
 <input id="controller" type="hidden" value="<?php echo $this->name; ?>" />
<script>

$('#submitSearch').click(function(){
    var values = $('#searchContent').serialize();
    var controller = $('#controller').val();
    $('#searchcontent').empty().text('Loading...');
    $.ajax({
        cache: false,
        url: "/"+controller+"/searchannouncement?_="+Math.random(),
        type: "POST",
        data : values,
        success:function(data){
             $('#searchcontent').empty().append(data);
            //$('#searchcontent').empty().append(data);
        }

    });
});

$('#keyword').keypress(function(e) {
    if (e.which == "13") { 
        $('#submitSearch').click();
        return false;
        //enter pressed 
    } 

});

</script>