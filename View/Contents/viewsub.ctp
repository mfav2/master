<?php
    echo $this->element('Components/breadcrumb');
    $currentUser = $this->Session->read('AuthUser');
    $permiss =  $currentUser['PermissionGroup'];
    $permisssubscribe = $currentUser['PermissionSubscribe'];

    $showadd = 'N';
    $showedit = 'N';
    $showdelete = 'N';
    foreach($permiss as $permission){
        if($permission['PermissionGroup']['function_id'] == 4 || $permission['PermissionGroup']['function_id'] == 5){
            if($permission['PermissionGroup']['is_add'] == 'Y'){
                $showadd = 'Y'; 
            }
            if($permission['PermissionGroup']['is_edit'] == 'Y'){
                $showedit = 'Y';
            }
            if($permission['PermissionGroup']['is_delete'] == 'Y'){
                $showdelete = 'Y';
            }      
        }
    }
?>
<div class="doc_view">
    <div class="doc_view_header">
        <dl class="dl-horizontal" style="padding:10px; ">
           
                <span class="pull-right" style="width:13%;">
                    <?php if($showedit == 'Y'){ ?>
                    <a href="<?= $this->Portal->makeUrl($this->name, 'edit', 'content_id=' . $content['ShowListContent']['id'] . '&category_id=' . $content['ContentCategory'][0]['category_id']); ?>">
                        <?php echo __('Edit'); ?>  <i class="icon-pencil"></i></a> |
                    <?php } ?>    
                    <?php if($showdelete == 'Y'){ ?>    
                    <a href="<?= $this->Portal->makeUrl($this->name, 'delete', 'content_id=' . $content['ShowListContent']['id'] . '&category_id=' . $content['ContentCategory'][0]['category_id']); ?>" onclick="return confirm('<?=__('Confirm to delete')?> ?')">
                        <?php echo __('Delete'); ?> <i class="icon-trash"></i>
                    </a>
                    <?php } ?>    
                </span>

           
            <!-- ************************ Title Name ***************************** -->
            <?php echo htmlspecialchars_decode($content['ShowListContent']['title']) ?>
        </dl>
    </div>
    <div class="doc_view_content">
        <?php echo htmlspecialchars_decode($content['ShowListContent']['description']); ?> <br /><br />
        <?php 
            if(!empty($content['ShowListContent']['attachment1']) || !empty($content['ShowListContent']['attachment2']) || !empty($content['ShowListContent']['attachment3'])){
                 echo __('Attachment File').' : <br /><br />';     
            }
            if(!empty($content['ShowListContent']['attachment1'])){
                $path_parts = pathinfo($content['ShowListContent']['attachment1_name']);
                $pic_ext = array("jpg", "gif", "png");
                if(in_array($path_parts['extension'], $pic_ext)){
                    echo "<div>";  
                    echo "<a target='_blank' href='".$this->FileStorageHelper->urlByKey($content['ShowListContent']['attachment1'])."'>";
                    echo "<img style='max-width:300px; border=0' src='/fileProviders/index/{$content['ShowListContent']['attachment1']}' />";
                    echo "</a>";
                    echo "</div><br />";                 
                }else{
                    if($path_parts['extension'] == 'doc' || $path_parts['extension'] == 'docx'){
                        $showicon = '<img src="img/custom/doc/word.png" width="26" height="26" />';
                    }else if($path_parts['extension'] == 'xls' || $path_parts['extension'] == 'xlsx'){
                        $showicon = '<img src="img/custom/doc/excel.png" width="26" height="26" />';
                    }else if($path_parts['extension'] == 'ppt' || $path_parts['extension'] == 'pptx'){
                        $showicon = '<img src="img/custom/doc/ppt.png" width="26" height="26" />';
                    }else if($path_parts['extension'] == 'pdf'){
                        $showicon = '<img src="img/custom/doc/pdf.png" width="26" height="26" />';
                    }else{
                        $showicon = '<i class="icon-adt_atach"></i>';
                    }
                    echo '&nbsp;&nbsp;<span ><a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['ShowListContent']['attachment1']) . '">'.$showicon.' '.$content['ShowListContent']['attachment1_name'].'</a></span><br /><br />';
                }
                
            }
            if(!empty($content['ShowListContent']['attachment2'])){
                 $path_parts = pathinfo($content['ShowListContent']['attachment2_name']);
                $pic_ext = array("jpg", "gif", "png");
                if(in_array($path_parts['extension'], $pic_ext)){
                    echo "<div>";  
                    echo "<a target='_blank' href='".$this->FileStorageHelper->urlByKey($content['ShowListContent']['attachment2'])."'>";
                    echo "<img style='max-width:300px; border=0' src='/fileProviders/index/{$content['ShowListContent']['attachment2']}' />";
                    echo "</a>";
                    echo "</div><br />";                 
                }else{
                    if($path_parts['extension'] == 'doc' || $path_parts['extension'] == 'docx'){
                        $showicon = '<img src="img/custom/doc/word.png" width="26" height="26" />';
                    }else if($path_parts['extension'] == 'xls' || $path_parts['extension'] == 'xlsx'){
                        $showicon = '<img src="img/custom/doc/excel.png" width="26" height="26" />';
                    }else if($path_parts['extension'] == 'ppt' || $path_parts['extension'] == 'pptx'){
                        $showicon = '<img src="img/custom/doc/ppt.png" width="26" height="26" />';
                    }else if($path_parts['extension'] == 'pdf'){
                        $showicon = '<img src="img/custom/doc/pdf.png" width="26" height="26" />';
                    }else{
                        $showicon = '<i class="icon-adt_atach"></i>';
                    }
                    echo '&nbsp;&nbsp;<span ><a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['ShowListContent']['attachment2']) . '">'.$showicon.' '.$content['ShowListContent']['attachment2_name'].'</a></span><br /><br />';
                }
                
            }
            if(!empty($content['ShowListContent']['attachment3'])){
                $path_parts = pathinfo($content['ShowListContent']['attachment3_name']);
                $pic_ext = array("jpg", "gif", "png");
                if(in_array($path_parts['extension'], $pic_ext)){
                    echo "<div>";  
                    echo "<a target='_blank' href='".$this->FileStorageHelper->urlByKey($content['ShowListContent']['attachment3'])."'>";
                    echo "<img style='max-width:300px; border=0' src='/fileProviders/index/{$content['ShowListContent']['attachment3']}' />";
                    echo "</a>";
                    echo "</div><br />";                 
                }else{
                    if($path_parts['extension'] == 'doc' || $path_parts['extension'] == 'docx'){
                        $showicon = '<img src="img/custom/doc/word.png" width="26" height="26" />';
                    }else if($path_parts['extension'] == 'xls' || $path_parts['extension'] == 'xlsx'){
                        $showicon = '<img src="img/custom/doc/excel.png" width="26" height="26" />';
                    }else if($path_parts['extension'] == 'ppt' || $path_parts['extension'] == 'pptx'){
                        $showicon = '<img src="img/custom/doc/ppt.png" width="26" height="26" />';
                    }else if($path_parts['extension'] == 'pdf'){
                        $showicon = '<img src="img/custom/doc/pdf.png" width="26" height="26" />';
                    }else{
                        $showicon = '<i class="icon-adt_atach"></i>';
                    }
                    echo '&nbsp;&nbsp;<span ><a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['ShowListContent']['attachment3']) . '">'.$showicon.' '.$content['ShowListContent']['attachment3s_name'].'</a></span><br /><br />';
                }
                                
            }
            /*if(!empty($attachments)){ //pr($attachments);
                echo __('Attachment File').' : <br /><br />';
                foreach($attachments AS $i => $attach){
                    ++$i;
                    $pic_ext = array("image/pjpeg", "image/jpg", "image/jpeg", "image/gif", "image/png");
                    if(in_array($attach['ResourceContent']['content_type'], $pic_ext)){
                        echo "<div>";  
                        echo "<a target='_blank' href='".$this->FileStorageHelper->urlByKey($attach['ResourceContent']['key'])."'>";
                        echo "<img style='max-width:300px; border=0' src='/fileProviders/index/{$attach['ResourceContent']['key']}' />";
                        echo "</a>";
                        echo "</div><br />";  
                    }else{
                       switch($attach['ResourceContent']['content_type']){
                           case 'application/msword':   $showicon = '<img src="img/custom/doc/word.png" width="26" height="26" />';
                                                        break;
                           case 'application/vnd.ms-excel': $showicon = '<img src="img/custom/doc/excel.png" width="26" height="26" />';
                                                            break;
                           case 'application/pdf': $showicon = '<img src="img/custom/doc/pdf.png" width="26" height="26" />';
                                                    break;
                           default :    $showicon = '<i class="icon-adt_atach"></i>';
                                        break;
                       } 
                        echo '&nbsp;&nbsp;<span ><a target="_blank" href="' . $this->FileStorageHelper->urlByKey($attach['ResourceContent']['key']) . '">'.$showicon.' '.$attach['ResourceContent']['original_name'].'</a></span><br /><br />';
                    }
                    
                }
             
            }*/
        ?>
       <br />
       <div>
        
           
       </div>
       <h5><br />(<?php echo __('Expire on'); ?> : <?= $this->Time->format('d/m/Y', $content['ShowListContent']['expiry_date']); ?>)</h5>
    </div>
    <div class="doc_view_footer clearfix" style="padding:10px;">
        <div class=" pull-left">
            <span title="<?php echo __('Created on'); ?>: <?php echo $this->Time->format('l, F d, Y \a\t g:ia', $content['ShowListContent']['created_date']); ?> (<?php echo $this->Time->timeAgoInWords($content['ShowListContent']['created_date'], array('format' => 'F jS, Y')); ?>)">
                <?php echo __('Posted By');?> <a id="userProfile" data-toggle="modal" data-backdrop="static" href="#myModal" user_id="<?php echo $content['ShowListContent']['user_id']; ?>"><?php echo $content['ShowListContent']['first_name'].' '.$content['ShowListContent']['last_name']; ?></a>&nbsp; <?php echo '[ '.$content['ShowListContent']['organization_name'].' '.$content['ShowListContent']['belong_organization_name'].' ]'; ?> 
            </span>
            <span style="color: green;" title="<?php echo $this->Time->format('l, F d, Y \a\t g:ia', $content['ShowListContent']['event_date']); ?> - <?php echo $this->Time->format('l, F d, Y \a\t g:ia', $content['ShowListContent']['expiry_date']); ?>">
                <?php echo $this->Time->format('d/m/Y H:i:s', $content['ShowListContent']['created_date']); ?>
            </span>
            <span class="label label-info"><?php echo __('Views'); ?></span>&nbsp;<span style="display:inline;" id="CountView"><?php echo $content['ShowListContent']['count_view']; ?></span>    
            <?php
                if(empty($content['ShowListContent']['content_liked_id'])){ 
                      $content['ShowListContent']['content_liked_id'] = 0;
                      $comment_id =0;
            ?>
                    <span id="like" chkclick="enabled"><span class="label label-info" style="cursor: pointer;" onclick="getLike(<?=$content['ShowListContent']['id']?>, <?=$comment_id?>)"><?php echo __('Like'); ?></span></span>
                    <span id="unlike" chkclick="enabled" style="display:none;"><span id="last_like_id" class="label label-info" like_id="<?=$content['ShowListContent']['content_liked_id']?>" style="cursor: pointer;" onclick="getUnLike(<?=$content['ShowListContent']['id']?>,<?=$content['ShowListContent']['content_liked_id']?>, <?=$comment_id?>)" ><?php echo __('Unlike'); ?></span></span>
                    <span style="display:inline;" id="CountLike"><?php echo $content['ShowListContent']['count_like'] ?></span>
            <?php
                  }else{
                      $comment_id = 0;
            ?>
                     <span id="unlike" chkclick="enabled"><span id="last_like_id" like_id="<?=$content['ShowListContent']['content_liked_id']?>" class="label label-info" style="cursor: pointer;" onclick="getUnLike(<?=$content['ShowListContent']['id']?>,<?=$content['ShowListContent']['content_liked_id']?>, <?=$comment_id?>)" ><?php echo __('Unlike'); ?></span></span>
                    <span id="like" chkclick="enabled" style="display:none;"><span class="label label-info" style="cursor: pointer;" onclick="getLike(<?=$content['ShowListContent']['id']?>, <?=$comment_id?>)"><?php echo __('Like');?></span></span>
                    &nbsp;<span style="display:inline;" id="CountLike"><?php echo $content['ShowListContent']['count_like'] ?></span>    
                    
            <?php
                    
                  } 
            ?>
                

        </div>
    </div>
</div>

<br />

<!-- Show Comment Form  if it has -->
<?php if ($content['ShowListContent']['is_comment'] == 'Y') : ?>
<span style='font-weight:bold;'><?php echo __('Comments Box'); ?></span> <br />
  <!-- **************************** Create Comment ****************************** -->  
    <div class="row-fluid">
        <div class="span12">
            <div class="doc_view" style="padding:10px 20px;">
                <div class="doc_view_header" >
                    <!-- Form Add Comment -->
                        <form method="post" enctype="multipart/form-data" action="<?php echo $this->Portal->makeUrl('Contents', 'comment', 'content_id=' . $content['ShowListContent']['id']); ?>" name="data[ContentComment]">
                            <?= $this->Form->input('content_id', array('value' => $content['ShowListContent']['id'], 'type' => 'hidden')); ?>
                            <textarea id ="description" name="description" rows="10" class="span12 required wysiwg_mini"></textarea>
                            <span class="error-comment" style="color:#F00;"></span>
                            <br>
                            <?= $this->Form->input('attachment', array('label'=>false,'type' => 'file', 'class'=> 'span12')); ?>
                            <span class="comment-error" style="color:#F00;"></span><br />
                            <span class="help-block"><?= __('Max File Size')?> : <?php echo $filesize; ?></span><br />
                            <input id="submitCommentform" class="btn" type="submit" value="<?=__('Compose')?>">
                        </form>
                    <!-- **************** -->
                </div>
                
            </div>
            
        </div>
    </div>
 <!-- *************************************************************** -->     
    <br />
     <?php echo $content['ShowListContent']['count_comment'] ?> <?php echo __('Comments'); ?> <img src="img/gCons/chat-.png" alt="" />
    <br />
  <!-- ******************** Show Comment ************************* -->  
  <div class="row-fluid">
      <div class="chat_box span12">
          <?php foreach ($comments as $i => $comment) : ?> 
          <div class="doc_view">
                <div class="doc_view_header">
                  <dl class="dl-horizontal" style="padding:10px;">
                       <?php echo __('Comment'); ?> <?= $i + 1 ?>
                  </dl>                 
                </div>
                <div class="doc_view_content">
                    <?php echo htmlspecialchars_decode($comment['ContentComment']['description']); ?> <br /><br />
                </div>
                <div class="doc_view_footer clearfix" style="padding:10px;">
                    <div class=" pull-left">
                        <?php
                            $is_like = false;
                            foreach ($comment['ContentLike'] as $like) {
                                if ($like['created_user_id'] == $this->Session->Read('AuthUser.AuthUser.id') && $like['deleted'] == 'N') {
                                    $is_like = true;
                                    $like_id = $like['id'];
                                    $comment_id = $like['reference_id'];
                                    break;
                                }
                            }
                        ?>
                        <?php echo __('Posted By'); ?>:
                        <a id="userProfile" data-toggle="modal" data-backdrop="static" href="#myModal" user_id="<?= $comment['ContentComment']['user_id'] ?>"><?php echo $comment['ContentComment']['first_name'].' '.$comment['ContentComment']['last_name']; ?></a> 
                        &nbsp;[ <?php echo $comment['ContentComment']['organization_name'].'&nbsp;-&nbsp;'.$comment['ContentComment']['belong_organization_name']; ?> ]&nbsp;
                        <span title="<?php echo $this->Time->format('l, F d, Y \a\t g:ia', $comment['ContentComment']['created_date'], array('format' => 'F jS, Y')) ?>">
                            <?php echo $this->Time->format('d/m/Y H:i:s', $comment['ContentComment']['created_date']) ?>
                        </span>
                        <span style="cursor: pointer;">
                         <!-- ============ Like Comment ==================== -->
                         <?php
                                if($is_like){
                           ?>
                                <span id="unlike<?=$comment_id?>" chkclick="enabled"><span id="last_like_id<?=$comment_id?>" like_id="<?=$like_id?>" class="label label-info" style="cursor: pointer;" onclick="getUnLike(<?=$content['ShowListContent']['id']?>,<?=$like_id?>, <?=$comment_id?>)" ><?php echo __('Unlike'); ?></span></span>
                                <span id="like<?=$comment_id?>" chkclick="enabled" style="display:none;"><span class="label label-info" style="cursor: pointer;" onclick="getLike(<?=$content['ShowListContent']['id']?>, <?=$comment_id?>)"><?php echo __('Like'); ?></span></span>
                                &nbsp;<span style="display:inline;" id="CountCommentLike<?=$comment['ContentComment']['id']?>"><?php echo $comment['ContentComment']['count_like'] ?></span>
            
                           <?php 
                                }else{
                                $content['Content']['content_liked_id'] = 0;    
                           ?>
                                <span id="like<?=$comment['ContentComment']['id']?>" chkclick="enabled"><span class="label label-info" style="cursor: pointer;" onclick="getLike(<?=$content['ShowListContent']['id']?>, <?=$comment['ContentComment']['id']?>)"><?php echo __('Like'); ?></span></span>
                                <span id="unlike<?=$comment['ContentComment']['id']?>" chkclick="enabled" style="display:none;"><span id="last_like_id<?=$comment['ContentComment']['id']?>" class="label label-info" like_id="<?=$content['ShowListContent']['content_liked_id']?>" style="cursor: pointer;" onclick="getUnLike(<?=$content['ShowListContent']['id']?>,<?=$content['ShowListContent']['content_liked_id']?>, <?=$comment['ContentComment']['id']?>)" ><?php echo __('Unlike'); ?></span></span>
                                &nbsp;<span style="display:inline;" id="CountCommentLike<?=$comment['ContentComment']['id']?>"><?php echo $comment['ContentComment']['count_like'] ?></span>
                           <?php  
                                }
                           ?>
                         <!-- ============ End Like Comment ==================== -->       
                        </span>
                    </div>
                </div>
          </div>
          <?php endforeach; ?> 
      </div>  
  </div>
  <!-- *********************************************************** -->      
 
<?php endif; ?>


<!-- TinyMce WYSIWG editor -->
<script src="lib/tiny_mce/jquery.tinymce.js"></script>
<script src="js/custom/custom_content_forms.js"> </script>
<script>
    
$('#attachment').bind('change', function() {
    $('.comment-error').empty();

     err = checkfileminsize(this.files[0].size);
     if(err == false){
         $('.comment-error').empty().text('<?=__('File over limit size')?> (5 MB)');
     }
});
$('#submitCommentform').click(function(){
    var err = false;
    $('.comment-error').empty();
    var comment =$('#description').val();
    if(comment == ''){
        $('.error-comment').empty().text('<?=__('This field is required')?>');
        err = true;
    }
/******************* check type file *********************/
        var file = $('#attachment').val();
                       // first check if file field has any value
        if(file != ''){
            var filetype = checktypefile(file);         
            if(filetype == 'N'){
                $('.comment-error').empty().text('<?php echo __("Only file"); ?> <?php echo Configure::read('Eportal.Extension.Allow'); ?>');
                err = true;
            }
        }
        /*********************************************************/        
    if(err == true){
        return false;
    }else{
        this.form.submit();
    }
});
    
 function checkfileminsize(filesize){
    var chk ='';
    var maxsize = '<?php echo Configure::read('Config.MinFileSize'); ?>';
                    if(filesize > maxsize){
                        chk = false;
                    }else{
                        chk = true;
                    }
                    
    return  chk;                
 }  
  function checktypefile(file){
            var exts = ['doc', 'docx', 'txt', 'pdf', 'jpg', 'jpeg', 'png', 'gif', 
                        'xls', 'xlsx', 'ppt', 'pptx', 'bmp', 'swf', 'zip'];
            if (file) {
                 // split file name at dot
                 var get_ext = file.split('.');
                 // reverse name to check extension
                 get_ext = get_ext.reverse();
                 var chktype = $.inArray ( get_ext[0].toLowerCase(), exts ) > -1;
                 // check file type is valid as given in 'exts' array
                 if (chktype){
                   return 'Y';
                 } else {
                   return 'N';
                 }
             }   
             
 }    
function getLike(content_id, comment_id){
        if(comment_id == 0 || comment_id ==''){
            var check = $('#like').attr('chkclick');
            $('#like').attr('chkclick', 'disabled');
            
        }else{
            var check = $('#like'+comment_id).attr('chkclick');
            $('#like'+comment_id).attr('chkclick', 'disabled');
        }   
            if(check == 'disabled'){
                return false;
            }
            $.ajax({
                cache: false,
                url: "/contents/like",
                type: "post",
                data: {'content_id':content_id, 'comment_id': comment_id},
                success:function(data){                 
                    data = $.parseJSON(data);
                    if(comment_id == 0 || comment_id ==''){
                        var countlike = data["Content"]["count_like"];
                        var like_id = data["Content"]["like_id"];
                        if(countlike != 0){
                            $('#CountLike').text(countlike);
                            $('#last_like_id').attr('like_id',like_id);
                            $('#like').css('display','none');
                            $('#unlike').css('display','inline');
                        }else{
                            alert(data);
                        }
                        $('#like').attr('chkclick', 'enabled');
                        $('#CountLike').show();
                    }else{
                        var countlike = data["ContentComment"]["count_like"];
                        var like_id = data["ContentComment"]["like_id"];
                        if(countlike != 0){
                            $('#CountCommentLike'+comment_id).text(countlike);
                            $('#last_like_id'+comment_id).attr('like_id',like_id);
                            $('#like'+comment_id).css('display','none');
                            $('#unlike'+comment_id).css('display','inline');
                            
                        }else{
                            alert(data);
                        }
                       $('#like'+comment_id).attr('chkclick', 'enabled');
                       $('#CountCommentLike'+comment_id).show(); 
                    }     
                }
            });
        
 }
 function getUnLike(content_id, like_id, comment_id){
        var num_default = 0;
        if(comment_id == 0 || comment_id ==''){
            var check = $('#unlike').attr('chkclick');
            $('#unlike').attr('chkclick', 'disabled');
            like_id =  $('#last_like_id').attr('like_id');
        }else{
            var check = $('#unlike'+comment_id).attr('chkclick')
            $('#unlike'+comment_id).attr('chkclick', 'disabled');
            like_id =  $('#last_like_id'+comment_id).attr('like_id');
        }   
            if(check == 'disabled'){
                return false;
            }
            $.ajax({
                cache: false,
                url: "Contents/unlike",
                type: "post",
                data: {'content_id':content_id, 'like_id': like_id, 'comment_id' : comment_id},
                success:function(data){
                    data = $.parseJSON(data); 
                    if(comment_id == 0 || comment_id ==''){
                        var countlike = data["Content"]["count_like"];
                        var like_id = data["Content"]["like_id"];
                       if(countlike != 0){
                         $('#CountLike').text(countlike);
                         $('#last_like_id').attr('like_id',num_default);
                       }else if(countlike == 0){
                         $('#CountLike').empty().text(countlike);
                         $('#last_like_id').attr('like_id',num_default);
                       }else{
                         alert(data);
                         $('#CountLike').text(num_default);
                         $('#last_like_id').attr('like_id',num_default);
                       }
                         $('#unlike').attr('chkclick', 'enabled'); 
                         $('#unlike').css('display','none');
                         $('#like').css('display','inline');   
                         $('#CountLike').show();
                    }else{
                        var countlike = data["ContentComment"]["count_like"];
                        var like_id = data["ContentComment"]["like_id"]; 
                       if(countlike != 0){
                         $('#CountCommentLike'+comment_id).text(countlike);
                         $('#last_like_id'+comment_id).attr('like_id',num_default);
                         
                       }else if(countlike == 0){
                         $('#CountCommentLike'+comment_id).text(countlike);
                         $('#last_like_id'+comment_id).attr('like_id',num_default);
                         
                       }else{
                         alert(data);
                         $('#CountCommentLike'+comment_id).text(num_default);
                         $('#last_like_id'+comment_id).attr('like_id',num_default);
                       }
                        $('#unlike'+comment_id).attr('chkclick', 'enabled');
                        $('#unlike'+comment_id).css('display','none');
                        $('#like'+comment_id).css('display','inline');
                        $('#CountCommentLike'+comment_id).show();
                    }    
                }
            });              
 }
</script>
