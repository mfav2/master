<?php // UPDATE BY BAIPHAI 2013-02-09 16:00
    $currentUser = $this->Session->read('AuthUser');
    $permiss =  $currentUser['PermissionGroup'];
    $permisssubscribe = $currentUser['PermissionSubscribe'];

    $showadd = 'N';
    $showedit = 'N';
    $showdelete = 'N';
    $showsubgroup = 'N';
    foreach($permiss as $permission){
        if($permission['PermissionGroup']['function_id'] == 4 || $permission['PermissionGroup']['function_id'] == 5){
            if($permission['PermissionGroup']['is_add'] == 'Y'){
                $showadd = 'Y'; 
            }
            if($permission['PermissionGroup']['is_edit'] == 'Y'){
                $showedit = 'Y';
            }
            if($permission['PermissionGroup']['is_delete'] == 'Y'){
                $showdelete = 'Y';
            }    
            if($permission['PermissionGroup']['is_add_sub_group'] == 'Y'){
                $showsubgroup = 'Y';
            }
        }
    }
//pr($currentUser);
    
$periods = array('All', 'Last Month', 'Last Week', 'This Week', 'This Month', 'Next Week', 'Next Month');
$sorts = array('Create Date', /*'Publish Date',*/ 'Event Date', /* 'Title' */);
echo $this->element('Components/breadcrumb'); 
?>
<div class="row-fluid">
    <div class="span3">
            <?php if($showadd == 'Y'){ ?>
                <a class="btn" href="<?php echo $this->Portal->makeURL($this->name, 'add', 'main_cate='.$this->request->query['main_cate'].'&category_id=' . $this->request->query['category_id']); ?>"><?php echo __('Add New'); ?></a>
            <?php } ?> 
                
            <?php if($showsubgroup == 'Y'){ ?>    
                <a class="btn" href="javascript:void(0);" onclick="addsubgroup(<?php echo $this->request->query['main_cate']; ?>, <?php echo $this->request->query['category_id']; ?>);"><?php echo __('Add Sub Group'); ?></a>    
            <?php } ?>    
    </div>
	<div class="span9" style="text-align: right;">
        <form id="searchContent" action="<?php echo $this->Portal->makeUrl($this->name, 'search'); ?>" class="form-inline" method="post">
            <?php
            echo $this->Form->input('main_cate', array('type' => 'hidden'));
            echo $this->Form->input('category_id', array('type' => 'hidden'));
            echo $this->Form->input('sub_id', array('type' => 'hidden'));
            if(isset($this->request->query['sub_id'])){
              echo $this->form->input('period', array(
                    'type' => 'select',
                    'label' => false,
                    'div' => false,
                    'style' => 'width:auto;',
                    'options' => $sub_cate,
                ));
              echo ' ';
            }
            echo __('Period').' : ';
            echo $this->form->input('period', array(
                'type' => 'select',
                'label' => false,
                'div' => false,
                'style' => 'width:auto;',
                'options' => $periods,
            ));
            echo __('Sort').' : ';
            echo $this->form->input('sort', array(
                'type' => 'select',
                'label' => false,
                'div' => false,
                'style' => 'width:auto;',
                'options' => $sorts,
            ));
            echo ' ';
            echo $this->Form->input('keyword', array('placeholder' => __('keyword'), 'label' => false, 'div'=>FALSE));
            echo ' ';
            echo $this->form->button(__('Search'), array('type'=>'button','id'=>'submitSearch','class' => 'btn', 'div'=>FALSE));
            ?>

        </form>
    </div>
</div>
<hr style='border:0.5px solid #058dc7; margin:0 0 10px 0;'/>

<div id="searchcontent">
  <?php 
        foreach($categories as $category) {
           
            echo '<div style="margin:15px 0px; color:#006bd6;"><h4> - '.$category['getCategory']['detail']['description'].'</h4></div>'; 
            if(!empty($contents)){
                foreach($contents as $content){
                   $showdiv = '['.$content['Content']['organization_name'].' '.$content['Content']['belong_organization_name'].']';
                    if($content['getContentCategory']['sub_category_id'] == $category['getCategory']['id']) { ?>

                      <!-- Show Content -->
                      <div class="prettyprint " >
                          <?php echo $this->Time->format('d M y', $content['Content']['event_date']).'&nbsp;&nbsp;&nbsp;'; ?>
                            <a class="pop_over"  data-content="<?php echo $content['Content']['gist']; ?>" data-placement="bottom" href="<?= $this->Portal->makeUrl($this->name, 'view', 'main_cate='.$this->request->query['main_cate'].'&content_id=' . $content['Content']['id']); ?>">
                                <?php 
                                    echo empty($content['Content']['title']) ? __('No title') : mb_substr(strip_tags(htmlspecialchars_decode($content['Content']['title'])), 0, 256, 'UTF-8');
                                ?>
                            </a>
                            <?php 
                                if(!empty($content['Content']['attachment1']) || !empty($content['Content']['attachment2']) || !empty($content['Content']['attachment3']) ){
                                    echo ' - ';
                                }
                                if(!empty($content['Content']['attachment1'])){
                                    echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment1']) . '"><i class="icon-adt_atach"></i></a>';
                                }
                                if(!empty($content['Content']['attachment2'])){
                                    echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment2']) . '"><i class="icon-adt_atach"></i></a>';
                                }
                                if(!empty($content['Content']['attachment3'])){
                                    echo '<a target="_blank" href="' . $this->FileStorageHelper->urlByKey($content['Content']['attachment3']) . '"><i class="icon-adt_atach"></i></a>';
                                }

                            ?>
                           
                               <span class="pull-right">
                                   <?php if($showedit == 'Y'){ ?>
                                       <a href="<?php echo $this->Portal->makeUrl($this->name, 'edit', 'main_cate='.$this->request->quetry['main_cate'].'&content_id=' . $content['Content']['id'] . '&category_id=' . $content['getContentCategory']['category_id']); ?>">              
                                       <i class='icon-pencil'></i></a>  | 
                                   <?php } ?>

                                   <?php if($showdelete == 'Y'){ ?>    
                                       <a href="<?php echo $this->Portal->makeUrl($this->name, 'delete', 'main_cate='.$this->request->query['main_cate'].'&content_id=' . $content['Content']['id']. '&category_id='.$content['getContentCategory']['category_id'] ); ?>" onclick="return confirm('<?php echo __('Confirm to delete')?> ?')"><i class='icon-trash'></i></a>
                                   <?php } ?>    
                               </span>
                               <br />
                            <span title="<?php echo $this->Time->timeAgoInWords($content['Content']['created_date']); ?>">
                                <a id="userProfile" data-toggle="modal" data-backdrop="static" href="#myModal" user_id="<?php echo $content['Content']['user_id']; ?>" style="color:black; font-size: smaller">
                                    <?php echo $content['Content']['first_name'].' '.$content['Content']['last_name'] ?>
                                </a>
                                    &nbsp;<?php echo "<span style='font-size:smaller;'>".$showdiv; ?>
                                - <?= $this->Time->format('Y-m-d H:i:s', $content['Content']['created_date'])  . "</span>" ?>
                            </span>
                            <span class="label label-info"><?php echo $content['Content']['count_view']; ?> <?php echo __('Views');?></span>
                      </div>
                      <!-- End Content -->
  <?php
                    }
                }
            }
  ?>          
  <?php } ?>
</div>
<input id="controller" type="hidden" value="<?php echo $this->name; ?>" />

<script>
function addsubgroup(MainCatId, SubCate){
	//alert(MainCatId+'/'+SubCate);
	$('#myModal').modal('show');
	$('#customModal').empty();
    $('#customModalHeader').html('<?php echo __('Add Sub Category'); ?>');
    $('#customModalAction').html('<?php echo __('Next'); ?>');
    $('#customModal').load("/Categories/addsubgroup/"+MainCatId+"/"+SubCate+"?_="+Math.random(),function(data) {
        //$('#customModal').html(data);
    });
	/*$('#customModal').load("/Categories/showsubcategory/"+MainCatId+"/"+SubCate+"?_="+Math.random(),function(data) {
        //$('#customModal').html(data);
    });*/
}

function modalAction(){
    location.reload();
}
$('#submitSearch').click(function(){
    var values = $('#searchContent').serialize();
    var controller = $('#controller').val();
    $('#searchcontent').empty().text('Loading...');
    $.ajax({
        cache: false,
        url: "/"+controller+"/searchallsub?_="+Math.random(),
        type: "POST",
        data : values,
        success:function(data){
             $('#searchcontent').empty().append(data);
            //$('#searchcontent').empty().append(data);
        }

    });
});

$('#keyword').keypress(function(e) {
    if (e.which == "13") { 
        $('#submitSearch').click();
        return false;
        //enter pressed 
    } 

});
$(function(){
    var offset = parseInt($('#offset').val());
    var totalContents = $('#countcontent').val();
    if(offset >= totalContents){ $('#viewmore').hide(); }
});
</script>