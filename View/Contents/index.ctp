<?php 
  $currentUser = $this->session->read('AuthUser'); 
  $permiss =  $currentUser['PermissionGroup'];
  $permisssubscribe = $currentUser['PermissionSubscribe'];
?>

<div class="row-fluid">
	<div class="span6">
		<div class="span12" style="clear:both; border-bottom:2px solid #ccc; padding:0 0 5px 0; margin:0 0 5px 0;">
			<span class="pull-left" style="font-size:20px; font-weight:bold; color:#3d7609;"><img src="img/custom/notice.png" alt="" width="26px" height="26px" />&nbsp;<?php echo __('Circular Bulletin'); ?></span>
			<span class="pull-right">
			
                            <?php 
                                foreach($permiss as $permission){ 
                                    if($permission['PermissionGroup']['function_id'] == 1){
                                        if($permission['PermissionGroup']['is_add'] == 'Y'){
                            ?>
				<a href="<?php echo $this->Portal->makeUrl($this->name, 'add', 'category_id=3'); ?>"
				   class="btn btn-small" title="<?=__('Add New')?>">
					<i class="icon-pencil"></i> <?=__('Add New')?>
				</a>
                            <?php 
                                        }
                                    }
                                } 
                            ?> 
				<a href="<?php echo $this->Portal->makeUrl($this->name, 'viewall', 'category_id=3'); ?>"
				class="btn btn-small" title="<?=__('View All')?>">
				<i class="icon-eye-open"></i> <?=__('View All')?>
				</a>
			</span>
		</div>
		<table class="table table-striped table-bordered" data-provides="rowlink">
			<tbody>
				
				<?php 
					foreach($query1 as $qr) { 
					
					$title = htmlspecialchars_decode($qr['Content']['title'], ENT_QUOTES);
					$title = strip_tags($title); 
					$gist = htmlspecialchars_decode($qr['Content']['gist'], ENT_QUOTES);
					$gist = strip_tags($gist); 

				?>

				<tr class="rowlink pop_over" data-content="<?php echo $gist; ?>" data-placement="bottom"   
				 onclick="location = '<?= $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $qr['Content']['id']); ?>'">
					 <td>
						<?php
						echo $this->Time->format('d M y', $qr['Content']['created_date']).'&nbsp;&nbsp;&nbsp;';
						echo $title; 
						?>
					 </td>
				</tr>
			   <?php } ?>
			</tbody>                
		</table>
	</div>
	<!-- ------------------------------------------------------------------------------------------ -->
	<div class="span6">
		<div class="span12" style="clear:both; border-bottom:2px solid #ccc; padding:0 0 5px 0; margin:0 0 5px 0;">
			<span class="pull-left" style="font-size:20px; font-weight:bold; color:#ff4040;"><img src="img/custom/notice.png" alt="" width="26px" height="26px" />&nbsp;<?php echo __('Announcement'); ?></span>
			<span class="pull-right">
                            <?php 
                                foreach($permiss as $permission){ 
                                    if($permission['PermissionGroup']['function_id'] == 1){
                                        if($permission['PermissionGroup']['is_add'] == 'Y'){
                            ?>                            
				<a href="<?php echo $this->Portal->makeUrl($this->name, 'add', 'category_id=13'); ?>"
				   class="btn btn-small" title="<?=__('Add New')?>">
					<i class="icon-pencil"></i> <?=__('Add New')?>
				</a>
                            <?php 
                                        }
                                    }
                                } 
                            ?> 
				<a href="<?php echo $this->Portal->makeUrl($this->name, 'viewall', 'category_id=13'); ?>"
				class="btn btn-small" title="<?=__('View All')?>">
				<i class="icon-eye-open"></i> <?=__('View All')?>
				</a>
			</span>
		</div>
		<table class="table table-striped table-bordered" data-provides="rowlink">
			<tbody>
				
				<?php 
					foreach($query2 as $qr2) { 
					
					$title = htmlspecialchars_decode($qr2['Content']['title'], ENT_QUOTES);
					$title = strip_tags($title); 
					$gist = htmlspecialchars_decode($qr2['Content']['gist'], ENT_QUOTES);
					$gist = strip_tags($gist); 

				?>

				<tr class="rowlink pop_over" data-content="<?php echo $gist; ?>" data-placement="bottom"   
				 onclick="location = '<?= $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $qr2['Content']['id']); ?>'">
					 <td>
						<?php
						echo $this->Time->format('d M y', $qr2['Content']['created_date']).'&nbsp;&nbsp;&nbsp;';
						echo $title; 
						?>
					 </td>
				</tr>
			   <?php } ?>
			</tbody>                
		</table>
	</div>
</div>
<div class="row-fluid">
	<div class="span6">
		<div class="span12" style="clear:both; border-bottom:2px solid #ccc; padding:0 0 5px 0; margin:0 0 5px 0;">
			<span class="pull-left" style="font-size:20px; font-weight:bold; color:#f0aa17;"><img src="img/custom/notice.png" alt="" width="26px" height="26px" />&nbsp;<?php echo __('Tips'); ?></span>
			
			<span class="pull-right">
                            <?php 
                                foreach($permiss as $permission){ 
                                    if($permission['PermissionGroup']['function_id'] == 1){
                                        if($permission['PermissionGroup']['is_add'] == 'Y'){
                            ?>                            
				<a href="<?php echo $this->Portal->makeUrl($this->name, 'add', 'category_id=14'); ?>"
				   class="btn btn-small" title="<?=__('Add New')?>">
					<i class="icon-pencil"></i> <?=__('Add New')?>
				</a>
                            <?php 
                                        }
                                    }
                                } 
                            ?> 
				<a href="<?php echo $this->Portal->makeUrl($this->name, 'viewall', 'category_id=14'); ?>"
				class="btn btn-small" title="<?=__('View All')?>">
				<i class="icon-eye-open"></i> <?=__('View All')?>
				</a>
			</span>
		</div>
		<table class="table table-striped table-bordered" data-provides="rowlink">
			<tbody>
				<?php 
					foreach($query3 as $qr3) { 
					
					$title = htmlspecialchars_decode($qr3['Content']['title'], ENT_QUOTES);
					$title = strip_tags($title); 
					$gist = htmlspecialchars_decode($qr3['Content']['gist'], ENT_QUOTES);
					$gist = strip_tags($gist); 

				?>

				<tr class="rowlink pop_over" data-content="<?php echo $gist; ?>" data-placement="bottom"   
				 onclick="location = '<?php echo $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $qr3['Content']['id']); ?>'">
					 <td>
						<?php
						echo $this->Time->format('d M y', $qr3['Content']['created_date']).'&nbsp;&nbsp;&nbsp;';
						echo $title; 
						?>
					 </td>
				</tr>
			   <?php } ?>
			</tbody>                
		</table>
	</div>
        <?php
            if(!empty($permisssubscribe)){
                foreach($permisssubscribe as $subscribe){
                    if($subscribe['GroupSubscribe']['function_id'] == 1){
        ?>
                    <div class="span6">
                            <div class="span12" style="clear:both; border-bottom:2px solid #ccc; padding:0 0 5px 0; margin:0 0 5px 0;">
                                    <span class="pull-left" style="font-size:20px; font-weight:bold; color:#225f7f;"><img src="img/custom/notice.png" alt="" width="26px" height="26px" />&nbsp;<?php echo __('Misc.'); ?></span>
                                    <span class="pull-right">
                                        <?php 
                                            foreach($permiss as $permission){ 
                                                if($permission['PermissionGroup']['function_id'] == 1){
                                                    if($permission['PermissionGroup']['is_add'] == 'Y'){
                                        ?>                            
                                            <a href="<?php echo $this->Portal->makeUrl($this->name, 'add', 'category_id=15'); ?>"
                                               class="btn btn-small" title="<?=__('Add New')?>">
                                                    <i class="icon-pencil"></i> <?=__('Add New')?>
                                            </a>
                                        <?php 
                                                    }
                                                }
                                            } 
                                        ?> 
                                            <a href="<?php echo $this->Portal->makeUrl($this->name, 'viewall', 'category_id=15'); ?>"
                                            class="btn btn-small" title="<?=__('View All')?>">
                                            <i class="icon-eye-open"></i> <?=__('View All')?>
                                            </a>
                                    </span>
                            </div>		
                            <table class="table table-striped table-bordered" data-provides="rowlink">
                                    <tbody>

                                            <?php 
                                                    foreach($query4 as $qr4) { 

                                                    $title = htmlspecialchars_decode($qr4['Content']['title'], ENT_QUOTES);
                                                    $title = strip_tags($title); 
                                                    $gist = htmlspecialchars_decode($qr4['Content']['gist'], ENT_QUOTES);
                                                    $gist = strip_tags($gist); 

                                            ?>

                                            <tr class="rowlink pop_over" data-content="<?php echo $gist; ?>" data-placement="bottom"   
                                             onclick="location = '<?php echo $this->Portal->makeUrl($this->name, 'view', 'content_id=' . $qr4['Content']['id']); ?>'">
                                                     <td>
                                                            <?php
                                                            echo $this->Time->format('d M y', $qr4['Content']['created_date']).'&nbsp;&nbsp;&nbsp;';
                                                            echo $title; 
                                                            ?>
                                                     </td>
                                            </tr>
                                       <?php } ?>
                                    </tbody>                
                            </table>

                    </div>        
        <?php
                    }
                }
            }
        ?> 

</div>