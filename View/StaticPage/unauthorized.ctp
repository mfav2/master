		<div class="error_box">
			<h1>Unauthorized Access</h1>
			<p>The page/file you've requested has been moved or taken off the site.</p>
			<a href="javascript:history.back()" class="back_link btn btn-small">Go back</a>
		</div>