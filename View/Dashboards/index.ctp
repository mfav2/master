<?php
	$this->layout = 'dashboard'; 
	//echo $this->element('Components/dashboard'); 
	
?>
<style>
body {
	margin:-14px 0 0 0;
}
div{
	margin:0;
	padding:0;
}
#wrap_pti {
	width:1250px;
	height:auto;
	margin:auto;
	border:1px solid #E2E1E4;
	border-width:0 1px 0 1px;
	bottom:0;
	top:0;
	font:normal 14px Century Gothic;
}
#footer_pti {
	width:100%;
	margin:140px 0 0 0;
	font:normal 12px Century Gothic;
	text-align:right;
}
.wrap_header {
	width:1235px;
	height:100%;
	margin:auto;
}
.wrap_header_column {
	width:205px;
	min-height:900px;
	max-height:1900px;
	border:1px solid #E2E1E4;
	border-width:0 1px 0 1px;
	margin:0 10px 0 10px;
	padding:10px;
	float:left;
}
.text_header {
	width:180px;
	font:bold 16px Century Gothic;
	border:1px solid #000/*#E2E1E4*/;
	border-width:0 0 1px 0;
}
.text_sup_header {
	font:normal 16px Century Gothic;
}
.wrap_menu {
	width:205px;
	height:auto;
	margin:20px 0 0 0;
}
.wrap_box_menu {
	width:204px;
	height:auto;
	border:1px solid #464C53;
	margin:auto;
	position:relative;
}
.text_header_menu {
	width:auto;
	font:normal 20px Century Gothic;
	border:1px solid #000/*#E2E1E4*/;
	border-width:0 0 1px 0;
	padding:10px 0 0 0;
	margin:auto;
	top:0;
	left:15px;	
	position:absolute;
	line-height:1.3;
}
.text_header_menu a {
	text-decoration:none;
	color:#000;
}
.text_sub_header_menu {
	width:174px;
	height:auto;
	background:#FFF;
	margin:37px 0 0 0;
	top:0;
	left:15px;
	position:absolute;
	opacity:0.9;
}
.text_sub_header_menu p {
	padding:0 0 0 10px;
}
.text_sub_header_menu a {
	text-decoration:none;
	color:#000;
}
.text_sub_header_menu a:hover {
	text-decoration:underline;
}
</style>

<div id="wrap_pti">
	<div class="wrap_header">
    	<div class="wrap_header_column">
        	<div class="text_header">Portal
            </div>
            <div class="text_sup_header">Time International
            </div>
            <p>Publish and share news, images, voice clips and video clips according to content category on web portal.
            </p>
            
            <div class="wrap_menu">
            	<div class="wrap_box_menu"><img src="img/dashboard/01_article.png" alt="">
               		<div class="text_header_menu"><a href="<?php echo $this->Portal->makeurl('Contents','index'); ?>">Circular Bulletin / <br>Article</a>
                	</div>
            	</div>
        	</div>
            
            <div class="wrap_menu">
            	<div class="wrap_box_menu"><img src="img/dashboard/06_internal_meeting.png" alt="">
               		<div class="text_header_menu">Internal Meeting
                	</div>
                    <div class="text_sub_header_menu">
                    	<p>
                    		<a href="<?php echo $this->Portal->makeurl('Meetings','viewall','category_id=20'); ?>">Development Dept. <br>Meeting</a>
                        </p>
                        <p>
                    		<a href="<?php echo $this->Portal->makeurl('Meetings','viewall','category_id=21'); ?>">Business Dept. Meeting</a>
                        </p>
                        <p>
                    		<a href="<?php echo $this->Portal->makeurl('Meetings','viewall','category_id=23'); ?>">Product Development</a>
                        </p>
                        <p>
                    		<a href="<?php echo $this->Portal->makeurl('Meetings','viewall','category_id=24'); ?>">Commercial</a>
                        </p>
                        <p>
                    		<a href="<?php echo $this->Portal->makeurl('Meetings','viewall','category_id=25'); ?>">Project Management</a>
                        </p>
                        <p>
                    		<a href="<?php echo $this->Portal->makeurl('Meetings','viewall','category_id=123'); ?>">Executive Meeting</a>
                        </p>
                	</div>
            	</div>
        	</div>
        </div>
        
        <div class="wrap_header_column">
        	<div>&nbsp;
            </div>
            <div class="text_sup_header">&nbsp;
            </div>
            <p>
                <div class="wrap_menu">
                    <div class="wrap_box_menu"><img src="img/dashboard/02_schedule.png" alt="">
                        <div class="text_header_menu"><a href="<?php echo $this->Portal->makeurl('Appointments','calendar'); ?>">Schedule / <br>Appointment</a>
                        </div>
                    </div>
                </div>
                
                <div class="wrap_menu">
                    <div class="wrap_box_menu"><img src="img/dashboard/07_poll_forum.png" alt="">
                        <div class="text_header_menu"><a href="#">Poll & Forum</a>
                        </div>
                        <div class="text_sub_header_menu">
                            <p>
                                <a href="<?php echo $this->Portal->makeurl('Polls','index'); ?>">Poll</a>
                            </p>
                            <p>
                                <a href="<?php echo $this->Portal->makeurl('Boards','index','category_id=18'); ?>">Forum</a>
                            </p>
                        </div>
                    </div>
                </div>
                
                <div class="wrap_menu">
                    <div class="wrap_box_menu"><img src="img/dashboard/11_good_references.png" alt="">
                        <div class="text_header_menu">Good References
                        </div>
                        <div class="text_sub_header_menu">
                            <p>
                                <a href="<?php echo $this->Portal->makeurl('References','viewall','category_id=29'); ?>">SOP / Manual</a>
                            </p>
                            <p>
                                <a href="<?php echo $this->Portal->makeurl('References','viewall','category_id=30'); ?>">Useful Info</a>
                            </p>
                        </div>
                    </div>
                </div>
            </p>
        </div>
        
        <div class="wrap_header_column">
        	<div>&nbsp;
            </div>
            <div class="text_sup_header">&nbsp;
            </div>
            <p>
                <div class="wrap_menu">
                    <div class="wrap_box_menu"><img src="img/dashboard/03_to_do.png" alt="">
                        <div class="text_header_menu">To Do
                        </div>
                        <div class="text_sub_header_menu">
                            <p>
                                (Not included in trial version)
                            </p>
                            
                        </div>
                    </div>
                </div>
                
                <div class="wrap_menu">
                    <div class="wrap_box_menu"><img src="img/dashboard/08_classified.png" alt="">
                        <div class="text_header_menu"><a href="<?php echo $this->Portal->makeurl('Boards','index','category_id=17'); ?>">Classified</a>
                        </div>
                    </div>
                </div>
           	</p>
        </div>
        
        <div class="wrap_header_column">
        	<div>&nbsp;
            </div>
            <div class="text_sup_header">&nbsp;
            </div>
            <p>
                <div class="wrap_menu">
                    <div class="wrap_box_menu"><img src="img/dashboard/04_high_level.png" alt="">
                        <div class="text_header_menu"><a href="<?php echo $this->Portal->makeurl('Highlevels','highlevelindex','high_level_group=3'); ?>">High Level <br>Schedule</a>
                        </div>
                    </div>
                </div>
                
                <div class="wrap_menu">
                    <div class="wrap_box_menu"><img src="img/dashboard/09_contact.png" alt="">
                        <div class="text_header_menu"><a href="<?php echo $this->Portal->makeurl('Contacts','indexcontact'); ?>">Contact</a>
                        </div>
                    </div>
                </div>
           	</p>
        </div>
        
        <div class="wrap_header_column">
        	<div>&nbsp;
            </div>
            <div class="text_sup_header">&nbsp;
            </div>
            <p>
                <div class="wrap_menu">
                    <div class="wrap_box_menu"><img src="img/dashboard/05_view shared .png" alt="">
                        <div class="text_header_menu"><a href="<?php echo $this->Portal->makeurl('ViewShares','index'); ?>">View Shared <br>Schedule</a>
                        </div>
                    </div>
                </div>
                
                <div class="wrap_menu">
                    <div class="wrap_box_menu"><img src="img/dashboard/10_message_board.png" alt="">
                        <div class="text_header_menu">Message Board
                        </div>
                        <div class="text_sub_header_menu">
                            <p>
                                <a href="<?php echo $this->Portal->makeurl('FormTemplates','viewall','category_id=42'); ?>">Word Template</a>
                            </p>
                            <p>
                                <a href="<?php echo $this->Portal->makeurl('FormTemplates','viewall','category_id=43'); ?>">Presentation</a>
                            </p>
                            <p>
                                <a href="<?php echo $this->Portal->makeurl('FormTemplates','viewall','category_id=44'); ?>">Template</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!--footer-->
              	<div class="wrap_menu"
                    <div id="footer_pti">Copyright © 2014 PAKGON Co.,Ltd. <br>All Right Reserved.
                    </div>
                </div>
           	</p>
        </div>
        
        <div style="clear:both;">
        </div>

	</div><!--wrap_header-->  
</div><!--wrap_pti-->

