
<div style="text-align: right">
    <form class="form-inline">
        <input name="keyword" type="text" placeholder="keyword"><br>
        Period: <select name="period">
            <?php foreach ($periods as $i => $period) : ?>
                <option value="<?= $i ?>"><?= $period ?></option>
            <? endforeach; ?>
        </select>
        Sort by: 
        <select name="sort">
            <option value="0">Posted Date</option>
            <option value="1">Event Date</option>
        </select>
        <button class="btn" type="submit">Search</button>
    </form>
</div>
<?php foreach ($contents as $content) : ?>
    <?php
//    pr($content);
    ?>
    <div class="w-box-header">
        <span style="color: green">
            <?= $this->Time->format('d M y', $content['Article']['event_date']) ?></span>
        <?= $content['Article']['title'] ?> 
        [<?= $content['Category']['category_name'] ?>]
        <a class="btn btn-small toolbar-icon" href="javascript:void(0)"><i class="icon-pencil"></i></a>
    </div>
    <div class="w-box-content cnt_a">
        <dd><?= $content['Article']['description'] ?>
    </div>
    <div class="w-box-footer">
        <?php
//        if ($content['att'] != null) :
//            echo '<b>Attachment</b>:';
//            foreach ($content['att'] as $att) :
//                echo '<a href="#"><' . $att['name'] . '</a>';
//            endforeach;
//            echo '(กดปุ่มขวาของ mouse ที่ชื่อ file, หากต้องการพิมพ์หรือ save file)<br>';
//        endif;
        ?>
        <b>Posted by</b>: <?= $content['Article']['created_user_id']; ?> <span style="color: green">[<?php //= $content['dept']      ?>] - <?= $this->Time->format('d M y', $content['Article']['published_date']) ?></span>
    </div>
    <br>
<?php endforeach; ?>
<script>
    $('#reversion').click(function(){
        $('#box').load('/articles/reversion/'); 
    });
</script>