<?php
/* /app/View/Helper/LinkHelper.php */
App::uses('AppHelper', 'View/Helper');

class PortalHelper extends AppHelper {

    function makeUrl($controller, $action, $query = "", $param = "") {
        // Logic to create specially formatted link goes here...
        if (!empty($containerId)) {
            $url = "pc=" . $controller . "&pa=" . $action . ($query == "" ? "" : "&pq=" . base64_encode($query)) . ($param == "" ? "" : "&pr=" . base64_encode($param));
        } else {
            $url = "pc=" . $controller . "&pa=" . $action . ($query == "" ? "" : "&pq=" . base64_encode($query)) . ($param == "" ? "" : "&pr=" . base64_encode($param));
        }
        return "/main?" . $url;
    }

    function bannerLogo(){

        $banner = 'default.png';
        $current_date = date('Y m d');

        $morning_time = str_replace(' ', '', $current_date .' 06 00 00' );		//$current_date 6:00
        $afternoon_time = str_replace(' ', '', $current_date .' 12 00 00' );		//$current_date 12:00
        $evening_time = str_replace(' ', '', $current_date .' 18 00 00' );		//$current_date 18:00

        $current_time = str_replace(' ', '', date("Y m d H i s"));
        
        if($morning_time <= $current_time && $current_time < $afternoon_time){
			$banner = 'morning.png';
        }else if($afternoon_time <= $current_time && $current_time <= $evening_time){
			$banner = 'afternoon.png';
        }else if($current_time > $evening_time){
			$banner = 'evening.png';
        }
	
        return $banner;
    }
	

}

?>
