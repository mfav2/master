<?php
	$mode = array('1'=>'Today', '2'=>'Tomorrow', '3'=>'This Week', '4'=>'This Month', '5'=>'Next week', '6'=>'Next 7 days', '7'=>'Next 14 days', '8'=>'Next 30 days');
	$current_date = date("Y-m-d");
	
	
	$current_year = date("Y");
	$current_month = date("m");

	/*$start_date_begin = 1;
	$timestamp = mktime(0, 0, 0, $selected_month, 1, $selected_year);
	$days_in_month = date("t", $timestamp);

	if(!empty($mode)){
		$start_date_begin  = $setdatestart;
		$days_in_month = $fin_date_month;
	}*/
 
    $tmp_month_list = range(1, 12);
    $month_list = array();
    foreach($tmp_month_list AS $i => $m){
        $month_list[$m] = date("M", mktime(0, 0, 0, $m, 1, $selected_year));
    }
    
    $tmp_years_list = range(date("Y") - 1, date("Y") + 1);
    foreach($tmp_years_list AS $i => $y){
        $years_list[$y] = $y;
    }
	
    //$time = mktime(0, 0, 0, $selected_month, $i, $selected_year);
	
?>
<div id="filterevent">
		<div class="row-fluid">
				<div class="span12">
					<?php
						echo "<div align='right'>";
						echo '<form style="margin-bottom:0px; margin-top:0px" method="post" action="' . $this->Portal->makeUrl($this->name, 'agenda') .'">';

						echo __('Mode').'&nbsp;'.$this->form->input('mode', array('type'=>'select', 'label'=>false, 'div'=>false, 'options'=>$mode, 'default'=>4, 'style'=>'width:auto; margin:8px 5px 5px 0;'));

						echo '&nbsp;'.__('Year').'&nbsp;'.$this->form->input('selected_year', array('type'=>'select', 'label'=>false, 'div'=>false, 'options'=>$years_list, 'default'=>$current_year, 'style'=>'width:auto; margin:8px 5px 5px 0;'));

						echo '&nbsp;'.__('Month').'&nbsp;'.$this->form->input('selected_month', array('type'=>'select', 'label'=>false, 'div'=>false, 'options'=>$month_list, 'default'=>$current_month, 'style'=>'width:auto; margin:8px 0 5px 0;'));

						echo $this->form->input('user_id', array('type'=>'hidden', 'value'=>$user_id));
						echo "</form>";
						echo "</div>";  
					?>
				</div>
		</div>

		<div class="row-fluid">
				<div id="agendaAllMonth" class="span12">
					<?php
						
						echo '<table class="table table-bordered table-striped table_vam">';
						echo "<tr><th style='padding:5px;'>".$user[$user_id]['detail']."</th>";
						echo "</tr>";
						foreach($alldate as $dateid=> $showdate){
							$this_day_event_title = '';
							$day = $this->Time->format('d', $showdate);
							$month = $this->Time->format('m', $showdate);
							$year = $this->Time->format('Y', $showdate);
							$full_date = $this->Time->format('D d M y', $showdate);
							//=====================
							//$time = mktime(0, 0, 0, $selected_month, $day, $selected_year);

							$time = mktime(0, 0, 0, $month, $day, $year);

							$today_start_time = date("Y-m-d H:i:s", $time);
							$today_end_time = date("Y-m-d 23:59:59", $time);
				
							$this_date = $year."-".$month."-".$day;
							$day_of_week = date("w", strtotime($this_date));
							
							if($day_of_week == 0){
								$colorevent = "#ffbad2;";
							}else if($day_of_week == 6){
								$colorevent = "#8f5e99;";
							}else{
								$colorevent = "#F3F3F3;";
							}
							
							if($this_date == $current_date){
								$colorevent = "#F2F5A9;";
							}
							//=====================
							if(!empty($appointments['event_date'])){
								//pr($appointments['event_date']);
								foreach($appointments['event_date'] AS $app_id => $event_date){
									$event_datetime_start = $event_date['start_date'];
									$event_datetime_end = $event_date['end_date'];
									//pr($event_datetime_start);
									//pr($event_datetime_end);
									if(empty($event_date['appointment_name'])){
										$event_date['appointment_name'] = $event_date['detail'];
									}
									
									
									//$today_start_time = date("Y-m-d H:i:s", $time);
									//$today_end_time = date("Y-m-d 23:59:59", $time);
									//$today_start_time = $chkstart;
									//$today_end_time = $chkend;
									
									if($today_start_time <= $event_datetime_start){
										$start_time = substr($event_datetime_start, 11, 5);
									}else{
										$start_time = substr($today_start_time, 11, 5);
									}

									if($today_end_time >= $event_datetime_end){
										$end_time = substr($event_datetime_end, 11, 5);
									}else{
										$end_time = substr($today_end_time, 11, 5);
									}
									
	
									if($today_start_time > $event_datetime_start && $today_end_time < $event_datetime_end){
										$event_title = "<div class='app-detail'>All day {$event_date['appointment_name']}</div>";
										
									}else if($today_start_time <= $event_datetime_start && $today_end_time >= $event_datetime_start){

										$event_title = "<div class='app-detail'>{$start_time} - {$end_time} {$event_date['appointment_name']}</div>";

										

									}else if($today_start_time >= $event_datetime_start && $today_start_time <= $event_datetime_end){
										$event_title = "<div class='app-detail'>{$start_time} - {$end_time} {$event_date['appointment_name']}</div>";
										
									}else{
										$event_title = "";
										
									}
									
									$this_day_event_title .= $event_title;	
								}
							}
							
							echo "<tr>";
							echo "	<td style='background-color:" . $colorevent . " padding:5px;'>";
                                                        echo "<div style='cursor:pointer;' class='app-date' onclick='addappointment(".$day.",".$month.",".$year.")'>" . $full_date . "</div>";
							echo $this_day_event_title;
							echo "	</td>";
							echo "</tr>";

						}


						echo "</table>";
					?>
				</div>
		</div>
</div>

<style>
.table th, .table td {
    border-top: 1px solid #DDDDDD;
    line-height: 15px;
    padding: 3px;
    text-align: left;
    vertical-align: top;
}


.app-date{
    text-align: right;
    width:100px;
}

.app-detail{
    margin-left: 50px
}
</style>
<script>
function addappointment(day, month, year){
     var user = $('#userId').val();
     var date = day+'/'+month+'/'+year;
     //alert(date+', '+day+'/'+month+'/'+year);
     $('#myModal').modal('show');
     $('#customModalHeader').html('New Appointment');
     $('#customModalAction').html('Close');
     $('#customModal').load("/appointments/addminicalendar/"+user,function(data) {
        $('#customModal').html(data);
        $('#fdate').val(date);
        $('#tdate').val(date);
     });
 }   


$('#selected_year').change(function() {
	 var mode = '';
	 var year = $('#selected_year option:selected').val();
	 var month =  $('#selected_month option:selected').val();
	 var userId = $('#userId').val();
	 //alert(month+'/'+year);
	 $('#filterevent').empty();
		$.ajax({
			type: "POST",
			cache: false,
			url: '/Appointments/agenda?_='+Math.random(),
			data: {'selected_year':year, 'selected_month':month, 'user_id':userId, 'mode':mode},
			success: function(data){
				$('#filterevent').append(data);
			}
			
		});
	 //$('#filterevent').append(data);		

});
$('#selected_month').change(function() {
	 var mode = '';
	 var year = $('#selected_year option:selected').val();
	 var month =  $('#selected_month option:selected').val();
	 var userId = $('#userId').val();
	 //alert(month+'/'+year);
	 $('#filterevent').empty();
		$.ajax({
			type: "POST",
			cache: false,
			url: '/Appointments/agenda?_='+Math.random(),
			data: {'selected_year':year, 'selected_month':month, 'user_id':userId, 'mode':mode},
			success: function(data){
				$('#filterevent').append(data);
			}
			
		});

});

$('#mode').change(function() {
	 var mode = $('#mode option:selected').val();
	 var year = $('#selected_year option:selected').val();
	 var month =  $('#selected_month option:selected').val();
	 var userId = $('#userId').val();
	 //alert(month+'/'+year);
	 $('#filterevent').empty();
		$.ajax({
			type: "POST",
			cache: false,
			url: '/Appointments/agenda?_='+Math.random(),
			data: {'selected_year':year, 'selected_month':month, 'user_id':userId, 'mode':mode},
			success: function(data){
				$('#filterevent').append(data);
			}
			
		});

});


</script>