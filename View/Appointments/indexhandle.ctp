<?php echo $this->element('Components/breadcrumb'); ?>
<?php //echo $this->element('Components/handleform'); ?>
<div class="row-fluid">
    <div class="span12">
        <div class="btn-group sepH_b">
        <button data-toggle="dropdown" class="btn dropdown-toggle">Action <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <li>
                <a class="addhandle" id="addhandle2" data-tableid="smpl_tbl" data-toggle="modal" data-backdrop="static" href="#myModal">
                    <i class="splashy-add_small"></i><?=__('Add Appointment Handler')?></a>
            </li>
        </ul>
        </div>
        <table class="table table-bordered  table_vam" id="tablehandle">
            <thead>
                <tr>
                    <th width="5%">#</th>
                    <th><?=__('Handler Name')?></th>
                    <th width="5%"><i class="icon-trash"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($findhandle as $handle) :
                    echo '<tr>';
                    echo '<td>'.$i.'</td>';
                    echo '<td>'.$handle['AppointmentHandle']['handle_name'].'</td>';
                    $i++;
                ?>
                    <td><i id="trash" style="cursor:pointer;" onclick="deletehandle(<?php echo $handle['AppointmentHandle']['id']; ?>)" class="icon-trash"></i></td>
                <?php
                    echo '</tr>';              
                    /*foreach($chkname['AppointmentHandle'] as $showname){
                        if($handle['AppointmentHandle']['handle_user_id'] == $showname['UserProfile']['user_id']){
                            echo '<tr>';
                            echo '<td>'.$i.'</td>';
                            $i++;
                                echo '<td>'.$showname['UserProfile']['first_name_th'].' '.$showname['UserProfile']['last_name_th'].'</td>';
                            ?>
                            <td><i id="trash" style="cursor:pointer;" onclick="deletehandle(<?php echo $handle['AppointmentHandle']['id']; ?>)" class="icon-trash"></i></td>
                            <?php
                            echo '</tr>';                            
                        }
                    }*/
            endforeach;
            ?>
            </tbody>
        </table>
<?php //pr($findhandle);  ?>
    </div>
</div>

<script>
    function deletehandle(id){
        if (confirm("<?=__('Confirm to delete')?>")) {
            location.href="/appointments/deletedhandle/"+id;
        }
        
    }

</script>

<!--<div class="modal hide fade" id="addhandle">
    <div class="modal-header">
        <button class="close" data-dismiss="modal">×</button>
        <h3>New Handle</h3>
    </div>
    <div class="modal-body">
        <div align="center">

        </div>
    </div>
    <div class="modal-footer">
        <div align="right">
            
        </div>
    </div> 
</div> -->
    