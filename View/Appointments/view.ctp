<?php echo $this->element('Components/breadcrumb'); ?>
<?php 
    //echo $this->element('Components/appointmentview'); 
    $currentUser = $this->Session->read('AuthUser');
    $countReader = count($chkuser);
    $chkhandler = 'N';


    switch($userappt['Appointment']['location_type_id']){
        case 1 :  $location = "<img src='img/icon/B_trans1.gif' /> Bangkok";
                  break; 
        case 2 :  $location = "<img src='img/icon/bus_trans1.gif' /> Domestic";
                  break; 
        case 3 :  $location = "<img src='img/icon/airplane_trans1.gif' /> Overseas";
                  break; 
        default :  $location = "<img src='img/icon/B_trans1.gif' /> Bangkok";
                  break; 
    }
    
    switch($userappt['Appointment']['priority_type_id']){
        case 1 :  $priority = "None";
                  break; 
        case 2 :  $priority = "<img src='img/icon/green_flag.png' />";
                  break; 
        case 3 :  $priority = "<img src='img/icon/yellow_flag.png' />";
                  break; 
        case 4 :  $priority = "<img src='img/icon/red_flag.png' />";
                  break;       
        default : $priority = "None";
                  break; 
    }
    
?>
<div align="right" style="margin:5px 0px;">
   <?php if($userappt['Appointment']['user_id'] == $currentUser['AuthUser']['id'] || $userappt['Appointment']['handle_user_id']== $currentUser['AuthUser']['id']){ ?> 
    <a class="btn" href="<?php echo $this->Portal->makeUrl('appointments','edit','id='.$this->request->query['id']) ?>">Edit</a>&nbsp;
    <a class="btn" onclick="deleteitem(<?php echo $this->request->query['id']; ?>)" href="javascript:void(0);">Delete</a>
   <?php } ?> 
</div>

<?php echo $this->Form->create('Appointment', array('action' => 'view', 'type'=>'file', 'class' => 'form-horizontal well')); ?>
<?php echo $this->Form->input('id', array('type'=>'hidden','id' => 'id', 'div'=>false, 'label'=>false, 'value'=>$this->request->query['id'])); ?>

<div class="row-fluid">
    <div class="span12">
            <h3 class="heading"><?php echo __('My Appointment'); ?></h3>
    </div>
    <div class="span12">
        <table width="100%" cellspacing="0" cellpadding="8" border="0">
            <tr valign="top">
                <td width="200"><?php echo __('Title'); ?></td>
                <td><?php echo $userappt['Appointment']['name']; ?></td>
            </tr>
            <tr valign="top">
                <td><?php echo __('Start Date'); ?></td>
                <td><?php echo $userappt['Appointment']['start_date']; ?></td>
            </tr>
            <tr valign="top">
                <td><?php echo __('End Date'); ?></td>
                <td><?php echo $userappt['Appointment']['end_date']; ?></td>
            </tr>
            <tr valign="top">
                <td><?php echo __('Invite'); ?></td>
                <td>
                  <?php 
                        foreach($chkuser as $reader){

                            if($reader['AppointmentUserReader']['user_id'] == $currentUser['AuthUser']['id']){
                                $chkhandler = 'Y';
                            }    


                            $reader['AppointmentUserReader']['confirmed_status'];    

                            $getdate = '';
                            $gettime = '';
                            if(!empty($reader['AppointmentUserReader']['confirmed_date'])){
                                $readdatetime = explode(' ', $reader['AppointmentUserReader']['confirmed_date']);
                                $getconfirmdate =  $readdatetime[0];
                                $getconfirmtime =  $readdatetime[1];
                                
                            }

                            if($reader['AppointmentUserReader']['confirmed_status'] == 'Reject' || $reader['AppointmentUserReader']['confirmed_status'] == 'reject'){
                                $reason = 'Reason: '.$reader['AppointmentUserReader']['reason'];
                            }else{
                                $reason ='';
                            }


                            if(!empty($reader['AppointmentUserReader']['confirmed_date'])){
                                $status = '['.$reader['AppointmentUserReader']['confirmed_status'].' On '.$getconfirmdate.' at '.$getconfirmtime.']';
                            }else{
                                $status = '['.$reader['AppointmentUserReader']['confirmed_status'].']';
                            }

                            $name = $reader['AppointmentUserReader']['first_name'].' '.$reader['AppointmentUserReader']['last_name']; 
                            //$organdbelong = '['.$reader['AppointmentUserReader']['organization_name'].' - '.$reader['AppointmentUserReader']['belong_organization_name'].']';

                            //$organdbelong = '['.$reader['AppointmentUserReader']['organization_name'].']';

                            //$organdbelong = '['.$reader['AppointmentUserReader']['belong_organization_name'].']';

                           /*echo $name.' '.$organdbelong. '<br />
                           '.$status.'<br /><br /><br />; */ 
                           //echo $name.' '.$organdbelong. '<br />'.$status.'<br />'.$reason.'<br/><br/>';
                           echo $name.' <br />'.$status.'<br />'.$reason.'<br/><br/>';
                        } 
                  ?>
                </td>
            </tr>
            <tr valign="top">
                <td><?php echo __('Created By'); ?></td>
                <td>
                    <?php echo $create_by; ?>
                </td>
            </tr>
            <tr valign="top">
                <td><?php echo __('Created Date'); ?></td>
                <td>
                    <?php echo $userappt['Appointment']['created_date']; ?>
                </td>
            </tr>
            <tr valign="top">
                <td><?php echo __('Location'); ?></td>
                <td>
                    <?php echo $location ?>
                </td>
            </tr>
            <tr valign="top">
                <td><?php echo __('Place'); ?></td>
                <td><?php echo $userappt['Appointment']['location']; ?></td>
            </tr>
            <tr valign="top">
                <td><?php echo __('Description'); ?></td>
                <td><?php echo $userappt['Appointment']['detail']; ?></td>
            </tr>
            <tr valign="top">
                <td><?php echo __('Attachment'); ?></td>
                <td>
                    <?php if($userappt['Appointment']['attachment'] != ''){ 
                       
                         echo   '<span style="margin:100px 0px 0px 10px;"><a href="/fileProviders/index/'.$userappt['Appointment']['attachment'].'" target="_blank"><i class="icon-adt_atach"></i>'.$userappt['Appointment']['attachment_name'].'</a></span><span style="margin:0 0 0 5px;"><i class="icon-trash" style="cursor:pointer;" ></i></span>';
                                  
                    } ?>
                </td>
            </tr>
            <tr valign="top">
                <td><?php echo __('Private Note'); ?></td>
                <td><?php echo $userappt['Appointment']['private_note']; ?></td>
            </tr>
            <tr valign="top">
                <td><?php echo __('Priority'); ?></td>
                <td>
                    <?php echo $priority; ?>
                </td>
            </tr>
               
                <?php if($countReader != 0  && $chkhandler =='Y' ){ ?>
                    
                    <tr valign="top">
                        <td> <?php echo __('Reason'); ?> </td>
                        <td>
                           <?php echo $this->Form->textarea('Reason', array('div'=>false, 'name'=>'data[AppointmentUserReader][reason]', 'class' => 'span5', 'label'=>false, 'style'=>'margin:0px; height:100px;')); ?> 
                        </td>    
                    </tr>
                    <tr>
                        <td colspan='2'>**<?php echo __('Reason for Reject Meeting'); ?></td>    
                    </tr>
                    <tr>
                        
                        <td colspan="2">
                            <?php echo $this->Form->button(__('Accepted', true), array('class' => 'btn','id' =>'confirmation_status', 'name' => 'data[AppointmentUserReader][confirmed_status]', 'value'=>'Accepted')); ?>
                            <?php echo $this->Form->button(__('Tentative', true), array('class' => 'btn','id' =>'confirmation_status', 'name' => 'data[AppointmentUserReader][confirmed_status]', 'value'=>'Tentative')); ?>
                            <?php echo $this->Form->button(__('Reject', true), array('class' => 'btn','id' =>'confirmation_status', 'name' => 'data[AppointmentUserReader][confirmed_status]', 'value'=>'Reject')); ?>
                        </td>
                        
                    </tr>
                    
                <?php } ?>
                         
            

            
        </table>
    </div>
</div>

<?php echo $this->Form->end(); ?>

<div id="deletedata" class="modal hide fade">
    <div class="modal-header">
        <button id="closepopup" class="close" data-dismiss="modal">×</button>
        <h3>&nbsp;</h3>
    </div>
    <div id="deleteid" class="modal-body">
        <?php echo __("Are you sure to delete this appointment?"); ?>
    </div>
    <div class="modal-footer">
        <a id="deleteitem" class="btn" href="javascript:void(0);">Yes</a>
        <a id="closepopup" class="btn" data-dismiss="modal" href="javascript:void(0);">Cancel</a>
    </div>
</div>
<script>  
function deleteitem(id){
    $('#deletedata').modal('show');
    $('#deleteitem').click(function(){
         $.ajax({
            cache: false,
            url: "/appointments/delete",
            type: "post",
            data: {'id':id },
            success:function(data){
                if(data == 1){
                    $('#deletedata').modal('hide');
                    location.href='<?php echo $this->Portal->makeUrl('appointments','calendar'); ?>';
                }else{
                    alert(data);
                }
            }
         });   
    });
}
</script>