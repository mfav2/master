<?php echo $this->element('Components/breadcrumb'); ?>
<?php 
    $minutes_select = array('00'=>'00','05'=>'05','10'=>'10','15'=>'15','20'=>'20','25'=>'25','30'=>'30','35'=>'35','40'=>'40','45'=>'45','50'=>'50','55'=>'55');
    $starthour_time = array('all'=>'All Day', 'morning'=>'All Morning', 'afternoon'=>'All Afternoon');
    $endhour_time = array('all'=>'All Day', 'morning'=>'All Morning');
    for($i=0;$i<=24;$i++){
        $hourtime = str_pad($i, 2, "0", STR_PAD_LEFT);
        $set[$i] = $hourtime;
    }
    $startmerge = array_merge($starthour_time, $set);
    $endmerge = array_merge($endhour_time, $set);
    $option = array('1'=>'<img src="img/icon/B_trans1.gif">&nbsp;'.__("Bangkok")."<br />",'2'=>'<img src="img/icon/bus_trans1.gif">&nbsp;'.__("Domestic")."<br />",'3'=>'<img src="img/icon/airplane_trans1.gif">&nbsp;'.__("Overseas"));
    $color_code = array('1'=>'<span class="showchooseborder" style="background:#83ba1f; border:0px solid #83ba1f; width:40px; height:40px;" >&nbsp;&nbsp;&nbsp;&nbsp;</span>', 
        '2'=>'<span class="showchooseborder" style="background:#2673ec; border:0px solid #2673ec; width:40px; height:40px;" >&nbsp;&nbsp;&nbsp;&nbsp;</span>', 
        '3'=>'<span class="showchooseborder" style="background:#e56c19; border:0px solid #e56c19; width:40px; height:40px;" >&nbsp;&nbsp;&nbsp;&nbsp;</span>');
    $priority = array('4'=>'<img src="img/icon/red_flag.png" />', '3'=>'<img src="img/icon/yellow_flag.png" />', '2'=>'<img src="img/icon/green_flag.png" />', '1'=>'None');
    $privacy = array('1'=>'Personal', '2'=>'Shared');
    $alert_list = array('1'=>'None', /*'2'=>'At Time of event',*/ '3'=>'5 minutes before', '4'=>'15 minutes before', '5'=>'30 minutes before',
         '6'=>'1 hour before', '7'=>'2 hours before', '8'=>'1 day before', '9'=>'2 days before');
    
    $showread = '';
    foreach($userread as $read){
        $name = $read['AppointmentUserReader']['first_name'].' '.$read['AppointmentUserReader']['last_name'];
        $showread .= '<li id="'.$read['AppointmentUserReader']['user_id'].'" class="tagItem" user_name="'.$name.'">'.$name.'</li>';
    }
    
?>
    
<?php
    echo $this->Form->create('Appointment', array('action' => 'edit', 'type'=>'file', 'class' => 'form-horizontal well'));
    echo $this->Form->input('id', array('type'=>'hidden','id' => 'id', 'div'=>false, 'label'=>false));
    echo $this->Form->input('user_id', array('type'=>'hidden','id' => 'user_id', 'div'=>false, 'label'=>false));
    echo $this->Form->input('handle_user_id', array('type'=>'hidden','id' => 'handle_user_id', 'div'=>false, 'label'=>false));
    echo $this->Form->input('title', array('type'=>'hidden','label'=>false, 'div'=>false));
    //pr($this->request->data);
?>
   <div class="row-fluid">
       <div class="span12">
           <table width="100%" border="0" cellspacing="5" cellpadding="3">
               <tr valign="top">
                   <td width="55%"> 
                       <!-- #################### LEFT ################### -->
                       <table width="100%" cellpadding='3' cellspacing='5'>
                           <tr valign="top">
                               <td width="120"><?php echo __('Title'); ?></td>
                               <td>
                                   <?php echo $this->Form->input('name', array('label'=>false, 'div'=>false, 'class'=>'span12')); ?><br />
                                   <span style="color:#f00;" class="help-block" id="error_title"></span>
                               </td>
                           </tr>
                           <tr valign="top">
                               <td><?php echo __('From'); ?></td>
                               <td>
                                   <div id="start_date" class="input-append date" style="float:left; margin: 5px 0 0 0;">
                                     <?php echo $this->Form->input('fdate', array('id'=>'fdate', 'label'=>false, 'div'=>false, 'style'=>'width:80px;', 'readonly'=>'readonly', 'name'=>'data[Appointment][start_date]', 'value'=>$this->request->data['Appointment']['start_date'], 'div'=>false)); ?>
                                       <span class="add-on"><i class="splashy-calendar_day"></i></span>
                                   </div>
                                   <div style="float:left; margin:5px 0 0 5px;">
                                     <?php echo $this->Form->select('fhour', $startmerge, array('default'=>'', 'empty'=>false, 'div'=>false, 'label', false, 'style'=>'width:100px;')); ?>  
                                   </div>
                                   <div style="float:left; margin:5px 0 0 5px;">
                                     <?php echo $this->Form->select('fmin', $minutes_select, array('default'=>'','class'=>'span12',  'empty'=>false, 'div'=>false, 'label', false)); ?>
                                   </div>
                                   <span style="color:#f00;" class="help-block" id="error_startdate"></span>
                               </td>
                           </tr>
                           <tr valign="top">
                               <td><?php echo __('To'); ?></td>
                               <td>
                                   <div id="end_date" class="input-append date" style="float:left; margin: 5px 0 0 0;">
                                     <?php echo $this->Form->input('tdate', array('id'=>'tdate', 'label'=>false, 'div'=>false, 'style'=>'width:80px;', 'readonly'=>'readonly', 'name'=>'data[Appointment][end_date]', 'value'=>$this->request->data['Appointment']['end_date'], 'div'=>false)); ?>
                                       <span class="add-on"><i class="splashy-calendar_day"></i></span>
                                   </div>
                                   <div style="float:left; margin:5px 0 0 5px;">
                                     <?php echo $this->Form->select('thour', $endmerge, array('default'=>'', 'empty'=>false, 'div'=>false, 'label', false, 'style'=>'width:100px;')); ?>  
                                   </div>
                                   <div style="float:left; margin:5px 0 0 5px;">
                                     <?php echo $this->Form->select('tmin', $minutes_select, array('default'=>'','class'=>'span12',  'empty'=>false, 'div'=>false, 'label', false)); ?>
                                   </div>
                                   <span style="color:#f00;" class="help-block" id="error_enddate"></span><br />
                                   <span style="color:#f00;" class="help-block" id="error_time"></span>
                               </td>
                           </tr>
                           <tr valign="top">
                               <td><?php echo __('Place'); ?></td>
                               <td>
                                   <?php echo $this->Form->textarea('location', array('div'=>false, 'class' => 'span12', 'label'=>false, 'style'=>'height:70px;')); ?>
                                   <span style="color:#f00;" class="help-block" id="error_location"></span>
                               </td>
                           </tr>
                           <tr valign="top">
                               <td><?php echo __('Message shown in shared schedule'); ?></td>
                               <td>
                                   <?php echo $this->Form->textarea('detail', array('div'=>false, 'class' => 'span12', 'label'=>false, 'style'=>'margin:0px; height:100px;')); ?>        
                                   <span style="color:#f00;" class="help-block" id="error_detail"></span>
                               </td>
                           </tr>
                           
                           <tr valign="top">
                               <td><?php echo __('Private Note'); ?></td>
                               <td>
                                  <?php echo $this->Form->textarea('private_note', array('div'=>false, 'class' => 'span12', 'label'=>false, 'style'=>'margin:0px; height:65px;')); ?>         
                               </td>
                           </tr>
                           
                       </table>
                       <!-- ############################################ -->
                   </td>
                   <td width="45%"> 
                       <!-- #################### RIGHT ################### -->
                       <table width="100%" cellpadding='5'>
                           <tr valign="top">
                               <td width="50%">
                                   <?php echo __('Location'); ?><br />
                                   <?php echo $this->Form->radio('location_type_id', $option, array( 'legend' => false, 'default'=>'1', 'div'=>false, 'label'=>false, 'style'=>'margin:10px 5px 5px 20px;'));  ?>
                               </td>
                           </tr>
                           <tr valign="top">
                               <td width="50%">
                                   <?php echo __('Label'); ?><br />
                                   <?php echo $this->Form->radio('color_id', $color_code, array( 'legend' => false, 'default'=>'1', 'div'=>false, 'label'=>false, 'style'=>'margin:10px 5px 5px 20px;'));  ?>
                               </td>
                               
                           </tr> 
                           <tr valign="top">
                               <td width="50%"><?php echo __('Priority'); ?><br />
                               <?php echo $this->Form->radio('priority_type_id', $priority, array( 'legend' => false, 'default'=>'1', 'div'=>false, 'label'=>false, 'style'=>'margin:10px 5px 5px 20px;'));  ?>
                               </td>
                           </tr> 
                           <tr valign="top">
                               <td width="50%"><?php echo __('Privacy'); ?><br />
                                <?php echo $this->Form->radio('privacy_type_id', $privacy, array( 'legend' => false, 'default'=>'1', 'div'=>false, 'label'=>false, 'style'=>'margin:10px 5px 5px 20px;'));  ?>
                               </td>
                           </tr> 
                       </table>
                       <!-- ############################################ --> 
                   </td>
               </tr>
               <tr>
                   <td width="100%" colspan="2">
                        <div style="float:left; text-align:left; margin:0 10px 0 0px;">
                            <?php echo __('Attachment File'); ?>
                        </div>
                       
                       <!-- ############ Attachment #################### -->
                       
                       <?php if(empty($this->request->data['Appointment']['attachment'])) { ?>
                            <div style="float:left; text-align:left; margin:0 10px 0 0px;">
                                 <?php echo $this->Form->input('attach1',array('type'=>'file', 'div'=>false, 'class' => 'span12', 'label'=>false) ); ?>
                            </div>

                            <div style="float:left; text-align:left; margin:0 10px 0 0px;">    
                                 <span class="help-block"><?php echo __('Max File Size').' : '.$size; ?></span>
                            </div>
                       <?php }else{ ?>
                            <div style="float:left; text-align:left; margin:0 10px 0 0px;">
                                <?php
                                   echo   '<span style="margin:100px 0px 0px 10px;"><a href="/fileProviders/index/'.$this->request->data['Appointment']['attachment'].'" target="_blank"><i class="icon-adt_atach"></i>'.$this->request->data['Appointment']['attachment_name'].'</a></span><span style="margin:0 0 0 5px;"><i class="icon-trash" style="cursor:pointer;" onclick="deleteattach('.$this->request->data['Appointment']['id'].')"></i></span>';
                                ?>
                                <br />
                                <?php echo $this->Form->input('attachment', array('type'=>'hidden', 'div'=>false, 'label'=>false)); ?>
                                <?php echo $this->Form->input('attachment_name', array('type'=>'hidden', 'div'=>false, 'label'=>false)); ?>
                            </div>
                               
                       <?php } ?>
                       <!-- ############################################ -->
                       
                       <span style="color:#f00;" class="help-block" id="error_attach"></span>
                   </td>
               </tr>
               <tr>
                   <td width='120' colspan='2'>
                       <div style="float:left; text-align:left; margin:0 10px 0 0px;">
                           <?php echo __('Invite Meeting'); ?>
                       </div>
                       &nbsp;
                       <div style="float:left; text-align:left; margin:0 10px 0 0px;">
                           <ul id="array_tag_handler" style="background:#fff; width:500px; margin:0px;"></ul>
                       </div>
                       <div style="float:left;">
                           <a id="inviteUser"  href="#myModal" data-backdrop="static" data-toggle="modal" title="Invite User"><span class="add-on ad-on-icon"><i class="splashy-contact_grey_add"></i></span></a>
                       </div>
                       
                   </td>
               </tr>
               <tr>
                   <td width="100%" colspan="2">
                       <?php echo __('Remind'); ?>
                       <?php echo  $this->Form->checkbox('is_alert', array('style'=>'margin:0 10px;')); ?>
                       <?php echo  $this->Form->select('alert_type_id', $alert_list, array('empty'=>false, 'div'=>false, 'label', false, 'style'=>'margin:0 0 0 5px;')); ?>
                   </td>
               </tr>

               <tr id="remind_repeat">
                   <td width="100%" colspan="2">
                       <?php echo __('Remind Repeat'); ?>
                       <?php echo  $this->Form->checkbox('is_alert_repeat', array('style'=>'margin:0 10px;')); ?>
                       <?php echo  $this->Form->select('alert_repeat_type_id', $alert_list, array('empty'=>false, 'div'=>false, 'label', false, 'style'=>'margin:0 0 0 5px;')); ?>
                   </td>
               </tr>

               <tr>
                   <td align="center">
                      <?php echo $this->Form->button(__('Save'), array('class'=>'btn','id'=>'submitForm', 'type'=>'button', 'div'=>false, 'label'=>false)); ?>
                      <?php echo $this->Form->button(__('Cancel'), array('class'=>'btn','id'=>'back', 'type'=>'button', 'div'=>false, 'label'=>false, 'onclick'=>'history.go(-1)', 'style'=>'margin: 0 0 0 5px;')); ?>
                   <a class="btn" onclick="deleteitem(<?php echo $this->request->query['id']; ?>)" href="javascript:void(0);"><?php echo __('Delete'); ?></a>
                   </td>
                   <td>&nbsp;</td>
               </tr>
           </table>
       </div>
   </div>     
<?php echo $this->Form->end(); ?>
<div id="deletedata" class="modal hide fade">
    <div class="modal-header">
        <button id="closepopup" class="close" data-dismiss="modal">×</button>
        <h3>&nbsp;</h3>
    </div>
    <div id="deleteid" class="modal-body">
        <?php echo __("Are you sure to delete this appointment?"); ?>
    </div>
    <div class="modal-footer">
        <a id="deleteitem" class="btn" href="javascript:void(0);">Yes</a>
        <a id="closepopup" class="btn" data-dismiss="modal" href="javascript:void(0);">Cancel</a>
    </div>
</div>

<link rel="stylesheet" href="lib/tag_handler/css/jquery.taghandler.css" />
<!-- datepicker -->
<script src="lib/datepicker/bootstrap-datepicker.min.js"></script>
<!-- autosize textareas -->
<script src="js/forms/jquery.autosize.min.js"></script>
<!-- tag handler (recipients) -->
<script src="lib/tag_handler/jquery.taghandler.min.js"></script>

<script>
$(document).ready(function() {
    
    
    
    if($("#AppointmentIsAlert").attr("checked")=="checked"){
            $("#remind_repeat").show();
            $("#AppointmentRemind").show();
            $("#AppointmentAlertTypeId").show();
            $("#AppointmentIsAlertRepeat").show();
    }else{
            $("#remind_repeat").hide();
            $("#AppointmentRemind").hide();
            $("#AppointmentAlertTypeId").hide();
            $("#AppointmentIsAlertRepeat").hide();
    }
    
    if($("#AppointmentIsAlertRepeat").attr("checked")=="checked"){
        $("#AppointmentAlertRepeatTypeId").show();
    }else{
        $("#AppointmentAlertRepeatTypeId").hide();
    }
    
    $('#AppointmentFhour').change(function() {
         var chkhour = $('#AppointmentFhour option:selected').val();
         var start = $('#fdate').val();
         var end = $('#tdate').val();
           
         if(chkhour != 'all' && chkhour != 'morning' && chkhour != 'afternoon'){
                $('#AppointmentFmin').show(); 
                $('#AppointmentThour').show();
            
         }else{
           if(start == end){
                $('#AppointmentThour').hide();
                $('#AppointmentTmin').hide(); 
           }   
            $('#AppointmentFmin').hide(); 
         }
    });
    
     $('#AppointmentThour').change(function() {
         var chktohour = $('#AppointmentThour option:selected').val();
         if(chktohour != 'all' && chktohour != 'morning'){
             $('#AppointmentTmin').show();
         }else{
             $('#AppointmentTmin').hide();
         }    
     });
    $("#AppointmentIsAlert").click(function(){ 
        if($(this).attr("checked")=="checked"){
            $("#remind_repeat").show();
            $("#AppointmentRemind").show();
            $("#AppointmentAlertTypeId").show();
            $("#AppointmentIsAlertRepeat").show();
        }else{ 
            $("#remind_repeat").hide();
            $("#AppointmentRemind").hide();
            $("#AppointmentAlertTypeId").hide();
            $("#AppointmentAlertRepeatTypeId").hide();
            $("#AppointmentIsAlertRepeat").hide();
            $("#AppointmentIsAlertRepeat").removeAttr("checked","checked");
            $("#AppointmentIsAlert").removeAttr("checked","checked");
            
            
        }
    });
    $("#AppointmentIsAlertRepeat").click(function(){ 
        if($(this).attr("checked")=="checked"){ 
            $("#AppointmentAlertRepeatTypeId").show();
        }else{ 
            $("#AppointmentAlertRepeatTypeId").hide();
        }
    });
    
    $('#AppointmentAttach1').bind('change', function() {
        $('#error_attach').empty();
         err = checkfilesize(this.files[0].size);
         if(err == false){
             $('#error_attach').text('<?php echo __('File over limit size');?> (5MB)');
         }
    });
    
    $('#submitForm').click(function(){
           var title = $('#AppointmentName').val(); 
           var start = $('#fdate').val();
           var end = $('#tdate').val();
           var start_hour =  $('#AppointmentFhour option:selected').val();
           var start_min =  $('#AppointmentFmin option:selected').val();
	         var end_hour =  $('#AppointmentThour option:selected').val();
           var end_min =  $('#AppointmentTmin option:selected').val();
           var current = '<?php echo date('d/m/Y');?>';
           
	         var location = $('#AppointmentLocation').val();
           var detail = $('#AppointmentDetail').val();
           var err = false;
           var starttime;
           var endtime;
            $('#error_title').empty(); 
            $('#error_startdate').empty();  
            $('#error_enddate').empty();  
	          $('#error_location').empty();
            $('#error_attach').empty();
            $('#error_detail').empty();
            $('#error_time').empty();
           /***************** check Time ******************/
           
            var date1 = start;
            var date2 = end;
            var date3 = current;
            date1 = date1.split("/");
            date2 = date2.split("/");
            date3 = date3.split("/");
            sDate = new Date(date1[2],date1[1]-1,date1[0]); 
            eDate = new Date(date2[2],date2[1]-1,date2[0]); 
            cDate = new Date(date3[2],date3[1]-1,date3[0]);


            if(sDate < cDate){
              $('#error_time').text('<?php echo __('start date is before current date');?>.');
              err = true;
            } 

            if(eDate < sDate){
              $('#error_time').text('<?php echo __('end date is before start date');?>.');
              err = true;
            }


            if(sDate == eDate){
                if(start_hour == 'all'){
                   starttime = '08:30:00';
                   endtime = '16:30:00';
                  
                }else if(start_hour == 'morning'){
                  starttime = '08:30:00';
                  endtime = '12:00:00';  
                }else if(start_hour == 'afternoon'){
                  starttime = '13:00:00';
                  endtime = '16:30:00';
                }else{
                  starttime = start_hour+':'+start_min+':00';
                   if(end_hour == 'all'){
                      endtime = '16:30:00'; 

                   }else if(end_hour == 'morning'){
                      endtime = '12:00:00';

                   }else{
                      endtime = end_hour+':'+end_min+':00';  
                   }
                }
                var start_time = parseInt(starttime);
                var end_time = parseInt(endtime);
                if(end_time < start_time){
                    $('#error_time').text('<?php echo __('start time is more than end time');?>.');    
                    err = true;    
                }
                
            }
            
           /**********************************************/
           /********************/ 
            var all= $("ul#array_tag_handler li.tagItem").length;
            var i = 1;
            var j = 0;
            var txt ='';
            $("ul#array_tag_handler li.tagItem").each(function() {
              var id = this.id;
              var name = this.user_name;

                $('ul#array_tag_handler').after('<input type="hidden" id="AppointmentUserReader" name="data[AppointmentUserReader]['+j+'][user_id]" value="'+id+'" /><input type="hidden" id="AppointmentUserReader" name="data[AppointmentUserReader]['+j+'][name]" value="'+$(this).attr('user_name')+'" />');
                j++;
                //$('#AppointmentUserReader').val(id);
            });
           
            
            var file = $('#AppointmentAttach1').val();
            if(file != ''){
                var filetype = checktypefile(file);         
                if(filetype == 'N'){
                    $('#error_attach').text('<?php echo __("Only file"); ?> doc, docx, txt, pdf, jpg, jpeg, png, gif, xls, xlsx, ppt, pptx, bmp, zip, rar, 7z, swf');  
                    err = true;
                }
            }
            if(title == ''){
              $('#error_title').text('<?php echo __('Please Insert Title')?>.');  
              err = true; 
            }
            if(start == ''){
              $('#error_startdate').text('<?php echo __('Please Insert Start Date')?>.');    
              err = true;
            }

            if(end == ''){
              $('#error_enddate').text('<?php echo __('Please Insert End Date')?>.');    
              err = true;  
            }

            if(detail == ''){
              $('#error_detail').text('<?php echo __('Please Insert Detail')?>.');    
              err = true;    
            }

            if(err == true){
                return false;
            }else{
                this.form.submit();
            }
           
            
            
    });
    
    $('#deleteForm').click(function(){
        var id = $('#id').val();
        
        if(confirm('<?=__('Do you want to delete this appointment')?>? ')){
            $.ajax({
                cache: false,
                url: "/appointments/delete",
                type: "post",
                data: {'id':id },
                success:function(data){
                    if(data == 1){
                        location.href='<?php echo $this->Portal->makeUrl('appointments','calendar'); ?>';
                    }else{
                        alert(data);
                    }
                }
            }); 
        }else{
            return false;
        }
    });
    
    $('#start_date').datepicker({format: "dd/mm/yyyy"}).on('changeDate', function(ev){
            /*var dateText = $(this).data('date');

            var endDateTextBox = $('#end_date input');
            if (endDateTextBox.val() != '') {
                    var testStartDate = new Date(dateText);
                    var testEndDate = new Date(endDateTextBox.val());
                    if (testStartDate > testEndDate) {
                            endDateTextBox.val(dateText);
                    }
            }
            else {
                    endDateTextBox.val(dateText);
            }
            $('#end_date').datepicker('setStartDate', dateText);*/
            $('#start_date').datepicker('hide');
    });
      
    $('#end_date').datepicker({format: "dd/mm/yyyy"}).on('changeDate', function(ev){
            /*var dateText = $(this).data('date');
            var startDateTextBox = $('#start_date input');
            if (startDateTextBox.val() != '') {
                    var testStartDate = new Date(startDateTextBox.val());
                    var testEndDate = new Date(dateText);
                    if (testStartDate > testEndDate) {
                            startDateTextBox.val(dateText);
                    }
            }
            else {
                    //startDateTextBox.val(dateText);
            }
            var chkstart = startDateTextBox.val();
            if(chkstart != dateText){
                $('#AppointmentThour').show();
            }else{
                $('#AppointmentThour').hide();
                $('#AppointmentTmin').hide();
            }
            $('#start_date').datepicker('setEndDate', dateText);*/
            $('#end_date').datepicker('hide');
    });
    
    $("#array_tag_handler").tagHandler();
    $("ul#array_tag_handler li.tagInput").hide();
    $("ul#array_tag_handler").prepend('<?php echo $showread; ?>');
});

function deleteitem(id){
    $('#deletedata').modal('show');
    $('#deleteitem').click(function(){
         $.ajax({
            cache: false,
            url: "/appointments/delete",
            type: "post",
            data: {'id':id },
            success:function(data){
                if(data == 1){
                    $('#deletedata').modal('hide');
                    location.href='<?php echo $this->Portal->makeUrl('appointments','calendar'); ?>';
                }else{
                    alert(data);
                }
            }
         });   
    });
}

function deleteattach(id){
    if(confirm('Do you want to delete this files? ')){

        $.ajax({
          cache: false,
          url: "/appointments/deleteAttachment",
          type: "post",
          data: {'id':id},
          success:function(data){
              if(data == 1){
                  location.reload();
              }else{
                  alert(data);
              }
          }
        });    
    }else{
        return false;   
    }
}

function checkfilesize(filesize){
    var chk ='';
    var maxsize = '<?php echo Configure::read('Config.MaxFileSize'); ?>';
                    if(filesize > maxsize){
                        chk = false;
                    }else{
                        chk = true;
                    }
                    
    return  chk;                
}

 
function checktypefile(file){
            var exts = ['doc', 'docx', 'txt', 'pdf', 'jpg', 'jpeg', 'png', 'gif', 
                        'xls', 'xlsx', 'ppt', 'pptx', 'bmp', 'zip', 'rar', '7z', 'swf'];
            if (file) {
                 // split file name at dot
                 var get_ext = file.split('.');
                 // reverse name to check extension
                 get_ext = get_ext.reverse();
                 var chktype = $.inArray ( get_ext[0].toLowerCase(), exts ) > -1;
                 // check file type is valid as given in 'exts' array
                 if (chktype){
                   return 'Y';
                 } else {
                   return 'N';
                 }
             }   
             
}    
  
</script>

