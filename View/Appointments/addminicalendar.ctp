<?php $currentUser = $this->Session->read('AuthUser'); ?>
<?php echo $this->element('Components/appointmentform'); ?>
<input id="ApptId" type="hidden" value="" />
<!--<link rel="stylesheet" href="css/custom/widget/user_directory.css" />-->
<!-- datepicker -->
<script src="lib/datepicker/bootstrap-datepicker.min.js"></script>

<style>
.modal {
    background-clip: padding-box;
    background-color: #FFFFFF;
    border: 8px solid rgba(0, 0, 0, 0.5);
    border-radius: 6px 6px 6px 6px;
    box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
    left: 50%;
    margin: -290px 0 0 -390px;
    overflow: auto;
    position: fixed;
    top: 50%;
    width: 850px;
    z-index: 1050;
}

.modal-body {
    min-height: 440px;
    max-height: 440px;
    overflow-y: auto;
}
 </style>
<script>
$(document).ready(function() {
 
        $('#AppointmentFmin').hide(); 
        $('#AppointmentThour').hide();
        $('#AppointmentTmin').hide();

        $("#remind_repeat").hide();
        $("#AppointmentRemind").hide();
        $("#AppointmentAlertTypeId").hide();
        $("#AppointmentIsAlertRepeat").hide();
        $("#AppointmentAlertRepeatTypeId").hide();

        $('#start_date').datepicker({format: "dd/mm/yyyy"}).on('changeDate', function(ev){
                /*var dateText = $(this).data('date');

                var endDateTextBox = $('#end_date input');
                if (endDateTextBox.val() != '') {
                        var testStartDate = new Date(dateText);
                        var testEndDate = new Date(endDateTextBox.val());
                        if (testStartDate > testEndDate) {
                                endDateTextBox.val(dateText);
                        }
                }
                else {
                        endDateTextBox.val(dateText);
                }
                $('#end_date').datepicker('setStartDate', dateText);*/
                $('#start_date').datepicker('hide');
        });

        $('#end_date').datepicker({format: "dd/mm/yyyy"}).on('changeDate', function(ev){
                /*var dateText = $(this).data('date');
                var startDateTextBox = $('#start_date input');
                if (startDateTextBox.val() != '') {
                        var testStartDate = new Date(startDateTextBox.val());
                        var testEndDate = new Date(dateText);
                        if (testStartDate > testEndDate) {
                                startDateTextBox.val(dateText);
                        }
                }
                else {
                        startDateTextBox.val(dateText);
                }
                var chkstart = startDateTextBox.val();
                if(chkstart != dateText){
                    $('#AppointmentThour').show();
                }else{
                    $('#AppointmentThour').hide();
                    $('#AppointmentTmin').hide();
                }
                $('#start_date').datepicker('setEndDate', dateText);*/
                $('#end_date').datepicker('hide');
        });        
});
    
    $('#AppointmentFhour').change(function() {
         var chkhour = $('#AppointmentFhour option:selected').val();
         var start = $('#fdate').val();
         var end = $('#tdate').val();
         
         if(chkhour != 'all' && chkhour != 'morning' && chkhour != 'afternoon'){
            $('#AppointmentFmin').show(); 
            $('#AppointmentThour').show();
            
         }else{
            if(start == end){
                $('#AppointmentThour').hide();
                $('#AppointmentTmin').hide(); 
            }   
            $('#AppointmentFmin').hide(); 
         }
    });
    
     $('#AppointmentThour').change(function() {
         var chktohour = $('#AppointmentThour option:selected').val();
         if(chktohour != 'all' && chktohour != 'morning'){
             $('#AppointmentTmin').show();
         }else{
             $('#AppointmentTmin').hide();
         }    
     });

    $("input[id=isall]").on("click",function(){
        if($('input[id=isall]').attr('checked')){
             $('#ftime').hide();
             $('#ttime').hide();
        }else{
             $('#ftime').show();
             $('#ttime').show();
        }
    });
    
    
    
    $("#AppointmentIsAlert").click(function(){ 
        if($(this).attr("checked")=="checked"){ 
            $("#remind_repeat").show();
            $("#AppointmentRemind").show();
            $("#AppointmentAlertTypeId").show();
            $("#AppointmentIsAlertRepeat").show();
            $("#AppointmentIsAlert_").val('1');
            $("#AppointmentIsAlert").val('1');
            $("#AppointmentIsAlert").attr("checked","checked");
        }else{ 
            $("#remind_repeat").hide();
            $("#AppointmentRemind").hide();
            $("#AppointmentAlertTypeId").hide();
            $("#AppointmentAlertRepeatTypeId").hide();
            $("#AppointmentIsAlertRepeat").hide();
            $("#AppointmentIsAlertRepeat").removeAttr('checked');
            $("#AppointmentIsAlert_").val('0');
            $("#AppointmentIsAlert").val('0');
            $("#AppointmentIsAlert").removeAttr("checked","checked");
            
            
        }
    });
    
    
    

    
    
    $("#AppointmentIsAlertRepeat").click(function(){ 
        if($(this).attr("checked")=="checked"){ 
            $("#AppointmentAlertRepeatTypeId").show();
        }else{ 
            $("#AppointmentAlertRepeatTypeId").hide();
        }
    });
    
    $('#AppointmentAttach1').bind('change', function() {
        $('#error_attach').empty();
         err = checkfilesize(this.files[0].size);
         if(err == false){
             $('#error_attach').text('<?php echo __('File over limit size');?> (5MB)');
         }
    });
    
    
    //var reload = '<?php echo $this->Portal->makeUrl('appointments','calendar'); ?>';
//    $("#submitminiForm").click(function(){
//        var user_id = $('#user_id').val();
//        var handler = '<?php echo $currentUser['AuthUser']['id'] ?>';
//        var title = $('#AppointmentName').val();
//        var detail = $('#AppointmentDetail').val();
//        var privatenote = $('#AppointmentPrivateNote').val();
//        var sdate = $('#fdate').val();
//        var stime = $('#ftime').val();
//        var tdate = $('#tdate').val();
//        var ttime = $('#ttime').val(); 
//        var location = $('#AppointmentLocation').val();
//        
//        var isall = 'N';
//        var repeat = 'N';
//        //var isall = $('input[id=isall]:checked').val();
//        //var repeat = $('input[id=isalway]:checked').val();
//        var locate_id = $("input[type='radio'][name='data[Appointment][location_type_id]']:checked").val();
//        var priority = $("input[type='radio'][name='data[Appointment][priority_type_id]']:checked").val();
//        var status_id = $("input[type='radio'][name='data[Appointment][status_type_id]']:checked").val();
//        var privacy_id = $("input[type='radio'][name='data[Appointment][privacy_type_id]']:checked").val();
//        var color = $('input[id=color_id]:checked').val();
//        if($('input[id=isall]').attr( 'checked' )){
//                isall = 'Y';
//        }
//        if($('input[id=isalway]').attr( 'checked' )){
//                repeat ='Y';
//        }
////        var formatdatestart = $('#formatstartdate').val();
////        var formatdateend = $('#formatenddate').val();
////        var formatstart = formatdatestart+' '+stime;
////        var formatend = formatdateend+' '+ttime;
////        
////        
////        $('#formatstartdate').val(formatstart);
////        $('#formatenddate').val(formatend);
//       var err = false;
//            $('#error_title').empty(); 
//            $('#error_startdate').empty();  
//            $('#error_enddate').empty();  
//            $('#error_location').empty();  
//            $('#error_detail').empty();
//            if(title == ''){
//              $('#error_title').text('Please Insert Title.');  
//              err = true; 
//            }
//            if(sdate == ''){
//              $('#error_startdate').text('Please Insert Start Date.');    
//              err = true;
//            }
//            
//            if(tdate == ''){
//              $('#error_enddate').text('Please Insert End Date.');    
//              err = true;  
//            }
//            
//            if(detail == ''){
//              $('#error_detail').text('Please Insert Detail.');    
//              err = true;    
//            }
//           
//
//            if(err == true){
//                return false;
//            }else{
//                $.ajax({
//                  cache: false,
//                  url: "/appointments/insertappointment",
//                  type: "post",
//                  data: {'title':title, 'detail':detail, 'private_note':privatenote, 'start_date':sdate, 'start_time':stime, 'end_date':tdate, 'end_time': ttime,
//                      'privacy_type_id': privacy_id, 'priority_type_id': priority, 'location_type_id': locate_id,
//                      'color_id': color, 'location': location, 'is_all_day':isall, 'is_always': repeat,
//                      'status_type_id':status_id, 'user_id': user_id, 'handle': handler },
//                  success:function(data){
//                      
//                      if(data != ''){
//                          $('#ApptId').val(data);
//                      }else{
//                          alert(data);
//                      }
//                      //if(data == 0){
//                         //$("#personal").modal('hide');
//                        // setTimeout('window.location.href="'+reload+'"', 1000);
//                      //}
//                  }
//               });
//            }
//       
//        
//    });

    $('#submitForm').click(function(){
           var title = $('#AppointmentName').val(); 
           var start = $('#fdate').val();
           var end = $('#tdate').val();
           var start_hour =  $('#AppointmentFhour option:selected').val();
           var start_min =  $('#AppointmentFmin option:selected').val();
	       var end_hour =  $('#AppointmentThour option:selected').val();
           var end_min =  $('#AppointmentTmin option:selected').val();
           var current = '<?php echo date('d/m/Y');?>';
           //alert(start_hour+' : '+start_min+' To '+end_hour+' : '+end_min);
           
            
           var location = $('#AppointmentLocation').val();
           var detail = $('#AppointmentDetail').val();
           var err = false;
           var starttime;
           var endtime;
            $('#error_title').empty(); 
            $('#error_startdate').empty();  
            $('#error_enddate').empty();  
	        $('#error_location').empty();
            $('#error_attach').empty(); 
            $('#error_detail').empty();
            $('#error_time').empty();
           /***************** check Time ******************/

            var date1 = start;
            var date2 = end;
            var date3 = current;
            date1 = date1.split("/");
            date2 = date2.split("/");
            date3 = date3.split("/");
            sDate = new Date(date1[2],date1[1]-1,date1[0]); 
            eDate = new Date(date2[2],date2[1]-1,date2[0]); 
            cDate = new Date(date3[2],date3[1]-1,date3[0]);


            if(sDate < cDate){
              $('#error_time').text('<?php echo __('start date is before current date');?>.');
              err = true;
            } 

            if(eDate < sDate){
              $('#error_time').text('<?php echo __('end date is before start date');?>.');
              err = true;
            }


            if(sDate == eDate){
                if(start_hour == 'all'){
                   starttime = '08:30:00';
                   endtime = '16:30:00';
                  
                }else if(start_hour == 'morning'){
                  starttime = '08:30:00';
                  endtime = '12:00:00';  
                }else if(start_hour == 'afternoon'){
                  starttime = '13:00:00';
                  endtime = '16:30:00';
                }else{
                  starttime = start_hour+':'+start_min+':00';
                   if(end_hour == 'all'){
                      endtime = '16:30:00'; 

                   }else if(end_hour == 'morning'){
                      endtime = '12:00:00';

                   }else{
                      endtime = end_hour+':'+end_min+':00';  
                   }
                }
                var start_time = parseInt(starttime);
                var end_time = parseInt(endtime);
                if(end_time < start_time){
                    $('#error_time').text('<?php echo __('start time is more than end time');?>.');   
                    err = true;    
                }
                
            }
            
           /**********************************************/
           /********************/ 
           var all= $("ul#array_tag_handler li.tagItem").length;
           var i = 1;
           var j = 0;
           var txt ='';
            $("ul#array_tag_handler li.tagItem").each(function() {
              var id = this.id;
              // do something with it
                if(i == all){
                    txt += id;
                }else{
                    txt += id+', ';
                }
                i++;
                
                $('ul#array_tag_handler').after('<input type="hidden" id="AppointmentUserReader" name="data[AppointmentUserReader]['+j+'][user_id]" value="'+id+'" />');
                j++;
                //$('#AppointmentUserReader').val(id);
            });
            //alert(txt);
            
            var file = $('#AppointmentAttach1').val();
            if(file != ''){
                var filetype = checktypefile(file);         
                if(filetype == 'N'){
                    $('#error_attach').text('<?php echo __("Only file"); ?> doc, docx, txt, pdf, jpg, jpeg, png, gif, xls, xlsx, ppt, pptx, bmp, zip, swf');  
                    err = true;
                }
            }
            if(title == ''){
              $('#error_title').text('<?php echo __('Please Insert Title')?>.');  
              err = true; 
            }
            if(start == ''){
              $('#error_startdate').text('<?php echo __('Please Insert Start Date')?>.');    
              err = true;
            }

            if(end == ''){
              $('#error_enddate').text('<?php echo __('Please Insert End Date')?>.');    
              err = true;  
            }

            if(detail == ''){
              $('#error_detail').text('<?php echo __('Please Insert Detail')?>.');    
              err = true;    
            }
            if(err == true){
                return false;
            }else{
                this.form.submit();
            }
           
            
            
    }); 
   
function checkfilesize(filesize){
    var chk ='';
    var maxsize = '<?php echo Configure::read('Config.MaxFileSize'); ?>';
                    if(filesize > maxsize){
                        chk = false;
                    }else{
                        chk = true;
                    }
                    
    return  chk;                
}

 
 function checktypefile(file){
            var exts = ['doc', 'docx', 'txt', 'pdf', 'jpg', 'jpeg', 'png', 'gif', 
                        'xls', 'xlsx', 'ppt', 'pptx', 'bmp', 'zip', 'swf'];
            if (file) {
                 // split file name at dot
                 var get_ext = file.split('.');
                 // reverse name to check extension
                 get_ext = get_ext.reverse();
                 var chktype = $.inArray ( get_ext[0].toLowerCase(), exts ) > -1;
                 // check file type is valid as given in 'exts' array
                 if (chktype){
                   return 'Y';
                 } else {
                   return 'N';
                 }
             }   
             
}    
  
</script>

