<?php echo $this->element('Components/appointmentform'); ?>

<script>
    $("#submitminiForm").click(function(){
        var type = 'mini';
        var title = $('#AppointmentsName').val();
        var sdate = $('#fdate').val();
        var stime = $('#ftime').val();
        var tdate = $('#tdate').val();
        var ttime = $('#ttime').val(); 
        var location = $('#AppointmentsLocation').val();
        var isall = 'N';
        var repeat = 'N';
        //var isall = $('input[id=isall]:checked').val();
        //var repeat = $('input[id=isalway]:checked').val();
        var locate_id = $("input[type='radio'][name='data[Appointments][location_type_id]']:checked").val();
        var priority = $("input[type='radio'][name='data[Appointments][priority_type_id]']:checked").val();
        var status_id = $("input[type='radio'][name='data[Appointments][status_type_id]']:checked").val();
        var privacy_id = $("input[type='radio'][name='data[Appointments][privacy_type_id]']:checked").val();
        var color = $('input[id=color_id]:checked').val();
        if($('input[id=isall]').attr( 'checked' )){
                isall = 'Y';
        }
        if($('input[id=isalway]').attr( 'checked' )){
                repeat ='Y';
        }
       
        $.ajax({
            cache: false,
            url: "/appointments/insertappointment",
            type: "post",
            data: {'title':title, 'start_date':sdate, 'start_time':stime, 'end_date':tdate, 'end_time': ttime,
                'privacy_type_id': privacy_id, 'priority_type_id': priority, 'location_type_id': locate_id,
                'color_id': color, 'location': location, 'is_all_day':isall, 'is_always': repeat,
                'status_type_id':status_id, 'form_type': type },
            success:function(data){
                if(data == 0){
//                    $("#personal").removeClass(" in "); 
//                    $("#personal").attr("aria-hidden",true);
//                    $("#personal").css("display","none");
                      $("#personal").modal('hide');
                }else{
                    $('#error_msg').empty().css("color","#f00").append(data);
                }
            }
         });
    });
    
//    $("#submitForm").click(function(){
//         var title = $('#AppointmentsName').val();
//         var sdate = $('#fdate').val();
//         var stime = $('#ftime').val();
//         var tdate = $('#tdate').val();
//         var ttime = $('#ttime').val();
//         var locate_id = $('input[id=location_id]:checked').val();
//         var priority = $('input[id=priority_id]:checked').val();
//         var color = $('input[id=color_id]:checked').val();
//         
//         var desc = $('#AppointmentsDescription').val();
//         var note_desc = $('#AppointmentsPrivateDescription').val();
//
//       
//         $.ajax({
//            cache: false,
//            url: "/appointments/insertappointment",
//            type: "post",
//            data: {'title':title, 'start_date':sdate, 'start_time':stime, 'end_date':tdate, 'end_time': ttime, 'description':desc,
//                'private_description': note_desc, 'is_share': shared, 'priority_id': priority, 'location_id': locate_id,
//                'color_id': color},
//            success:function(data){
//                if(data == 0){
//                   
//                    $("#personal").removeClass(" in "); 
//                    
//                }else{
//                    $('#error_msg').empty().css("color","#f00").append(data);
//                }
//            }
//         });
//    });
    
    $("#fdate").datepicker({dateFormat: 'dd/mm/yy',minDate: 0});
    $("#tdate").datepicker({dateFormat: 'dd/mm/yy',minDate: 0});
    $("#ftime").timepicker({
            defaultTime: '00:00',
            minuteStep: 5,
            disableFocus: true,
            template: 'modal',
            showMeridian: false
    });
    $("#ttime").timepicker({
            defaultTime: '23:59',
            minuteStep: 5,
            disableFocus: true,
            template: 'modal',
            showMeridian: false
    });
</script>

