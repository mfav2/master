<?php //echo $this->element('Components/breadcrumb');            ?>
<h3 class="heading"><?= $calName ?></h3>
<div class="row-fluid">
    <div class="span6">
        <a class="btn" href=<?= $toggleview ?>>Month view</a>
        <a class="btn" href="/main?pc=Appointments&pa=listview">This Month</a>
        <a class="btn" href=<?= $prevMonth ?>><i class="icon-arrow-left"></i></a>
        <?= date('F ', mktime(0, 0, 0, $month, 1, $year)) . $year ?>
        <a class="btn" href=<?= $nextMonth ?>><i class="icon-arrow-right"></i></a>
    </div>
    <div class="span3" style="text-align: right">
        <form method="post"action="<?= $this->Portal->makeUrl('Appointments', 'listview'); ?>">
    <!--<select name="pc" style="display: none"><option selected>Appointments</option></select>-->
    <!--<select name="pa" style="display: none"/><option selected>listview</option></select>-->
            <select name="mode" id="mode" style="width: 120px">
                <?php foreach ($modes as $i => $item) : ?>
                    <option value="<?= $i; ?>" <?= ($i == $mode) ? 'selected' : '' ?>><?= $item; ?></option>
                <?php endforeach; ?>
            </select>
            <select name="month" style="display: none"><option value="<?= date('n'); ?>" selected=""></option></select>
            <select name="year" style="display: none"><option value="<?= date('Y'); ?>" selected=""></option></select>
        </form>
    </div>
    <div class="span3" style="text-align: right">
        <form method="post"action="<?= $this->Portal->makeUrl('Appointments', 'listview'); ?>">
            <select name="mode" style="display: none"><option value="3" selected=""></option></select>
            <select name="month" id="month" style="width: 120px">
                <?php foreach ($montharray as $i => $item) : ?>
                    <option value="<?= $i ?>" <?= ($i == $month ? 'selected' : '' ); ?>><?= $item ?></option>
                <?php endforeach; ?>
            </select>
            <select name="year" id="year" style="width: 65px">
                <?php for ($i = date('Y') - 3; $i < date('Y') + 3; ++$i) : ?>
                    <option value="<?= $i ?>" <?= (($i == $year) ? 'selected' : '' ); ?>><?= $i ?></option>
                <?php endfor; ?>
            </select>
        </form>
    </div>
</div>
<table class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th colspan="2">
    <div class="row-fluid">
        <div class="span6">
            <h3><?= $apmname ?></h3>
        </div>
        <div class="span6" style="text-align: right">
            <select>
                <?php foreach($appointmentnames as $appointmentname) : ?>
                    <option><?=$appointmentname?></option>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</th>
</tr>
</thead>
<tbody>
    <?php
    // mode 0: Today        4: Next week    
    //      1: Tomorrow     5: Next 7 days
    //      2: This week    6: Next 14 days
    //      3: This month   7: Next 30 days
    switch ($mode) {
        case 0: // Today
            $_day = $i = date('j');
            break;
        case 1: // Tomorrow
            $i = (date('j') + 1);
            if ($i > cal_days_in_month(CAL_GREGORIAN, $month, $year)) {
                $i = 1;
                if ($month == 12) {
                    $month = 1;
                    ++$year;
                } else {
                    ++$month;
                }
            }
            $_day = $i;
            break;
        case 2: // This week
            $i = date('j', strtotime('Last Sunday', time()));
            $_day = date('j', strtotime('Next Saturday', time()));
            if ($_day < $i) {
                $_day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            }
            $nextDay = 7;
            break;
        case 3: // This month
            $i = 1;
            $_day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            break;
        case 4: // Next week
            if (date('n', strtotime('Next Sunday', time())) > $month) {
                ++$month;
                if ($month == 13) {
                    $month = 1;
                    ++$year;
                }
            }
            $i = date('j', strtotime('Next Sunday', time()));
            $_day = date('j', strtotime('Next Saturday', (time() + (7 * 60 * 60 * 24))));
            if ($_day < $i) {
                $_day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            }
            $daytogo = 7;
            break;
        case 5: // Next 7 days
            $_day = ($i = date('j')) + 8;
            if ($_day > cal_days_in_month(CAL_GREGORIAN, $month, $year)) {
                $_day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            }
            $daytogo = 8;
            break;
        case 6: // Next 14 days
            $_day = ($i = date('j')) + 15;
            if ($_day > cal_days_in_month(CAL_GREGORIAN, $month, $year)) {
                $_day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            }
            $daytogo = 15;
            break;
        case 7: // Next 30 days
            $_day = ($i = date('j')) + 31;
            if ($_day > cal_days_in_month(CAL_GREGORIAN, $month, $year)) {
                $_day = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            }
            $daytogo = 31;
            break;
    }
    $count = 0; // use for case 2
    ?>
    <?php for (; $i <= $_day; ++$i) : ?>
        <tr
        <?php
        if ($i == date('j')) {
            echo 'BGCOLOR=lightyellow';
        } else {
            switch (date('D', mktime(0, 0, 0, $month, $i, $year))) {
                case 'Sat': echo 'BGCOLOR=8F5E99';
                    break;
                case 'Sun': echo 'BGCOLOR=FFBAD2';
                    break;
            }
        }
        //call date() and mktime() to many times
//            echo ($i == date('j')) ?
//                    'BGCOLOR=lightyellow' :
//                    ((date('D', mktime(0, 0, 0, $month, $i, $year)) == 'Sat') ?
//                            'BGCOLOR=lightpink' :
//                            ((date('D', mktime(0, 0, 0, $month, $i, $year)) == 'Sun') ?
//                                    'BGCOLOR=plum' : ''));
        ?>>
            <td width="1%"><?= date('D', mktime(0, 0, 0, $month, $i, $year)); ?></td>
            <td><?= date('d, F Y', mktime(0, 0, 0, $month, $i, $year)); ?></td>
        </tr>
        <?php
        if (isset($calendar[$i])) {
            foreach ($calendar[$i] as $item) :
                ?>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr><td><?= $item['time']; ?></td><td><?= $item['text']; ?></td></tr>
                        </table>
                    </td>
                </tr>
                <?php
            endforeach;
        }

        if ($mode == 2 || $mode > 3) {
            ++$count;
            if ($i == cal_days_in_month(CAL_GREGORIAN, $month, $year) && $count < $daytogo) {
                $i = 0;
                ++$month;
                if ($month == 13) {
                    $month = 1;
                    ++$year;
                }
                $_day = $daytogo - $count;
            }
        }
    endfor;
    ?>
</tbody>
</table>
<script>
    $('#mode').change(function() { this.form.submit(); });
    $('#year').change(function() { this.form.submit(); });
    $('#month').change(function() { this.form.submit(); });
</script>