<script src="js/custom/contact_index_edit.js"> </script>
<?php echo $this->element('Components/breadcrumb'); ?>
<?php  echo $this->element('Components/contact_index_tabform'); ?>

<!-- datatable (inbox,outbox) -->
<script src="lib/datatables/jquery.dataTables.min.js"></script>
<!-- additional sorting for datatables -->
<script src="lib/datatables/jquery.dataTables.sorting.js"></script>
<!-- plupload and all it's runtimes and the jQuery queue widget (attachments) -->
<!-- sortable/filterable list -->
<script src="lib/list_js/list.min.js"></script>
<script src="lib/list_js/plugins/paging/list.paging.js"></script>
<!-- autosize textareas -->
<script src="js/forms/jquery.autosize.min.js"></script>
<script src="lib/validation/jquery.validate.min.js"></script>
<!-- validation functions -->
<script type="text/javascript" src="lib/plupload/js/plupload.full.js"></script>
<script type="text/javascript" src="lib/plupload/js/jquery.plupload.queue/jquery.plupload.queue.full.js"></script>
<!--tables functions--> 
<script src="js/gebo_validation.js"></script>
<script src="lib/jstree/jquery.jstree.js"></script>
<script src="js/gebo_tables.js"></script>