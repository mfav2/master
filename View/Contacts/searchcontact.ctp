<table class="table table-bordered  table_vam" id="contacttable">
    <thead>
        <tr>
            <?php
                foreach($listViewDefs['data'] as $data){ ?>
                <th<?php echo empty($data['width']) ? '' : ' style="width:' . $data['width'] . 'px"' ?>  class="sorting">
                        <?php echo $data['label']; ?>
                </th>
            <?php } ?>
                
        </tr>
    </thead>
    <tbody id="search_contact_result">
		<?php foreach ($listViewDatas as $listViewData) { ?>
			<tr id="record<?php echo $listViewData['id']; ?>">
				<?php foreach ($listViewDefs['data'] as $key=>$listViewDef) { ?>
					<td><?php echo $listViewData[$key]?></td>
				<?php } ?>
			</tr>
		<?php } ?>
        

    </tbody>
</table>   