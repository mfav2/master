<table class="table table-bordered table-striped table_vam" id="ObjectTable">
    <tr>
        <th></th>
        <th><?php echo __('Folder Code'); ?></th>
        <th><?php echo __('Folder Name'); ?></th>
        <th><?php echo __('Created Date'); ?></th>
    </tr>
<?
foreach($result as $d) : ?>
    <tr>
        <td><input type="checkbox"  name="row_sel" class="row_sel" id="" value="<?php echo $d['Folder']['id'] ?>"/></td>
        <td><?= $d['Folder']['folder_code']; ?></td>
        <td><?= $d['Folder']['folder_name']; ?></td>
        <td><?= $d['Folder']['created_date']; ?></td>
    </tr>
<?    
endforeach;
?>
</table>
    
    
<?php if(empty($event)) $event = 'folder_handle';?>
<script src="js/custom/event/<?=$event;?>.js"></script>
