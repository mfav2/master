<?php 

$currentUser = $this->Session->read('AuthUser');
$user_id = $currentUser['AuthUser']['id'];

$notification_mode_value = empty($currentUser['UserNotification']['notification_mode'])? 'summary':$currentUser['UserNotification']['notification_mode'];
$notification_type_value = empty($currentUser['UserNotification']['notification_type'])? '1':$currentUser['UserNotification']['notification_type'];
if($currentUser['UserNotification']['notification_type']==0) $notification_type_value = $currentUser['UserNotification']['notification_type'];
$notification_sound_value = empty($currentUser['UserNotification']['notification_sound'])? 'Y':$currentUser['UserNotification']['notification_sound'];
$notification_song_value = empty($currentUser['UserNotification']['notification_song'])? 'default.mp3':$currentUser['UserNotification']['notification_song'];

$notification_mode = array('summary' => __('Notification Summary'), 'detail' => __('Notification Detail'));

$notification_type = array('0' => __('No Alert'), '1' => __('Alert'), '2' => __('Alert 1 hour'));

$sound = array('new-message1.mp3' => 'new-message1.mp3', 'new-message2.mp3' => 'new-message2.mp3', 'new-message3.mp3' => 'new-message3.mp3', 'new-message4.mp3' => 'new-message4.mp3');
?>

<div class="row-fluid">
	<div class="span12">
		<div class="vcard">
			<ul>

				<li>	
						<span class="item-key span4"><?php echo __('Notification Mode');?>&nbsp;:&nbsp;</span>
						<?php echo $this->Form->input('notification_mode', array('options' => $notification_mode, 'default' => $notification_mode_value,'label' => false)); ?>
				</li>

				<li>	
						<span class="item-key span4"><?php echo __('Notification Type');?>&nbsp;:&nbsp;</span>
						<?php echo $this->Form->input('notification_type', array('options' => $notification_type, 'default' => $notification_type_value,'label' => false)); ?>
				</li>
	
				<li>	
						<span class="item-key span4"><?php echo __('Sound');?>&nbsp;:&nbsp;<i id="playSoundIcon" class="splashy-volume_off" style="cursor:pointer;"></i></span>
						<div class="vcard-item"><?php echo $this->Form->input('notification_song',array('options' => $sound, 'default' => $notification_song_value,'label' => false)); ?>
						<?php echo $this->Form->input('notification_sound', array('type' => 'hidden','label' => false,'value' => $notification_sound_value)); ?>
				</li>


			</ul>
		</div>
	</div>
</div>
<script src="js/custom/widget/notification_setting.js"></script>
<script src="js/custom/event/notification_setting.js"></script>
<script>
    gebo_tips.init();
</script>