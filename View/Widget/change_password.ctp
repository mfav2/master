<div class="row-fluid">

	<div class="span12">

	    <label><?php echo __('Current password');?>&nbsp;:</label>
	    <input id="old_pass" type="password">
	    <div class="row-fluid">
		<div class="span6">
		    <label><?php echo __('New password');?>&nbsp;:</label>
		    <input id="new_pass" type="password">
		</div>
		<div class="span6">
		    <label><?php echo __('Confirm new password');?>&nbsp;:</label>
		    <input id="re_new_pass" type="password">
		</div>
	    </div>

	</div>

</div>
<?php
if(empty($event)) $event = 'click_select';
?>
<script src="js/custom/event/<?php echo $event;?>.js"></script>
<script>
$(document).ready(function(){
    $('#customModalAction').removeAttr('data-dismiss');
});
$('#closechatbox').click(function(){
    $('#customModalAction').attr('data-dismiss','modal');
});
</script>