<?php foreach ($userProfiles as $userProfile) { ?>
<div class="row-fluid">
	<div class="span2">
		<img class="user_avatar" alt="" src="fileProviders/index/<?php echo $userProfile['UserProfile']['image_key'];?>">
	</div>
	<div class="span10">

		    <div class="row-fluid">
			 <div class="span5">			
			    <label><?php echo __('First Name');?>&nbsp;:&nbsp;</label>
			    <?php echo $userProfile['UserProfile']['first_name_th'];?>
			</div>
			<div class="span5">
			    <label><?php echo __('Last Name');?>&nbsp;:&nbsp;</label>
			    <?php echo $userProfile['UserProfile']['last_name_th'];?>
			</div>
			<div class="span2">
			</div>
		    </div>
		    
		    <div class="row-fluid">
			 <div class="span12">
			    <label><?php echo __('Assignment/Desk');?>&nbsp;:&nbsp;</label>
			    <?php echo $userProfile['UserProfile']['responsibility'];?>
			 </div>
		    </div>

		    <div class="row-fluid">
                       <?php if($userProfile['UserProfile']['is_hide_phone_mobile'] =='Y'){ ?>
                                <div class="span10">			
                                    <label><?php echo __('Internal Tel');?>&nbsp;:&nbsp;</label>
                                    <?php echo $userProfile['UserProfile']['phone_internal'];?>
                                </div>
                                <div class="span2">
                                </div>  
                       <?php }else{ ?>
                                <div class="span5">			
                                    <label><?php echo __('Internal Tel');?>&nbsp;:&nbsp;</label>
                                    <?php echo $userProfile['UserProfile']['phone_internal'];?>
                                </div>
                                <div class="span5">
                                    <label><?php echo __('Mobile');?>&nbsp;:&nbsp;</label>
                                    <?php echo $userProfile['UserProfile']['phone_mobile'];?>
                                </div>
                                <div class="span2">
                                </div>                        
                       <?php }?> 
                        
			
                        
		    </div>

		    <div class="row-fluid">
			<div class="span5">
			    <label><?php echo __('Direct Tel');?>&nbsp;:&nbsp;</label>
			    <?php echo $userProfile['UserProfile']['phone_direct'];?>
			</div>
			<div class="span5">
			    <label><?php echo __('Fax No');?>&nbsp;:&nbsp;</label>
			    <?php echo $userProfile['UserProfile']['fax_number'];?>
			</div>
			<div class="span2">
			</div>
		    </div>
			
			<div class="row-fluid">
				<div class="span12">
			    <label><?php echo __('E-mail');?>&nbsp;:&nbsp;</label>
					<?php echo $userProfile['UserProfile']['email'];?>
				</div>
			</div>
		</div>

	</div>
</div>
<?php }?>
<script>
function modalAction(){
    
}    
</script>