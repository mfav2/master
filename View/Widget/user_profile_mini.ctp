<?php foreach ($userProfiles as $userProfile) { ?>
<div class="row-fluid userProfileMini" >
	<!--<button user_id='<?php echo $userProfile['UserProfile']['id'];?>' class="close userClose">×</button>-->
	<div class="span3 userImageMini">
		<img class="user_avatar" alt="" src="fileProviders/index/<?php echo $userProfile['UserProfile']['image_key'];?>">
	</div>
	<div class="span9">

		    <div class="row-fluid">
			<br>
		    </div>
	
		    <div class="row-fluid">
			 <div class="span12">
			    <b><?php echo __('Assignment/Desk');?> : </b>
			    <?php echo $userProfile['UserProfile']['responsibility'];?>
			 </div>
		    </div>

		    <div class="row-fluid">
			<br>
		    </div>

		    <div class="row-fluid">
			 <div class="span6">			
			    <b><?php echo __('Internal Tel');?> : </b>
			    <?php echo $userProfile['UserProfile']['phone_internal'];?>
			</div>

			<div class="span6">
				<?php if($userProfile['UserProfile']['is_hide_phone_mobile'] !='Y'){ ?>
					<b><?php echo __('Mobile');?></b> : 
					<?php echo $userProfile['UserProfile']['phone_mobile'];?>
				<?php }?> 
			</div>

		    </div>

		    <div class="row-fluid">
			<div class="span6">
			    <b><?php echo __('Direct Tel');?> : </b>
			    <?php echo $userProfile['UserProfile']['phone_direct'];?>
			</div>
			<div class="span6">
			    <b><?php echo __('Fax No');?> : </b>
			    <?php echo $userProfile['UserProfile']['fax_number'];?>
			</div>
		    </div>
			<div class="row-fluid">
			<div class="span12">
			    <b><?php echo __('E-mail');?> : </b>
			    <?php echo $userProfile['UserProfile']['email'];?>
			</div>
		    </div>
		</div>

	</div>
</div>
<?php }?>