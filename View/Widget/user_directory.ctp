
					<div class="row-fluid">

						<div class="span7">
							
							<div id="divGroupControl" class="tabbable">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tabCreateGroup" data-toggle="tab"><i class="splashy-group_grey_add"></i> <?php echo __('New Group'); ?></a></li>
									<li><a href="#tabSelectGroup" data-toggle="tab"><i class="splashy-group_grey"></i> <?php echo __('Private Group'); ?></a></li>
								</ul>

								<div class="tab-content">
									<div class="tab-pane active" id="tabCreateGroup">
										<input id="is_public" type="checkbox" name="is_public">					
										<input id="group_name" type="text" name="group_name"  style="width:375px"> 
										<button id="btnCreateGroup" name="clear" class="btn" type="button"><?php echo __('Save'); ?></button>
										<button id="btnCancel" name="clear" class="btn btnCancel" type="button"><?php echo __('Cancel'); ?></button>
									</div>

									<div class="tab-pane" id="tabSelectGroup">
										<div class="antiScroll antiScroll2 antiscroll-inner2">
											<ul id="select_group_list" class="list select_group_list">
											<?php foreach ($privateGroups as $privateGroup) { ?>
												<li id="select_li_group_<?php echo $privateGroup['ContactGroup']['id'];?>">
													<input id='<?php echo $privateGroup['ContactGroup']['id'];?>'  class='select_groups' type='checkbox' name='row_group'> 
													<!--<span class="label pull-right"><i class="splashy-pencil_small editGroup" group_id="<?php echo $privateGroup['ContactGroup']['id'];?>"></i></span>-->
													<!--<span class="label pull-right"><i class="icon-trash deleteGroup" group_id="<?php echo $privateGroup['ContactGroup']['id'];?>"></i></span>-->
													<span id="select_sp_count_<?php echo $privateGroup['ContactGroup']['id'];?>" class="label label-info pull-right sl_status"><?php echo $privateGroup['ContactGroup']['count_user'];?></span>
													<a id="select_label_<?php echo $privateGroup['ContactGroup']['id'];?>" class="privateGroup sl_name" href="#" group="selectGroup" group_id="<?php echo $privateGroup['ContactGroup']['id'];?>"><?php echo $privateGroup['ContactGroup']['contact_group_name'];?></a>
												</li>
											<?php }?>
											
											</ul>
											<!--<input id="group_name_" type="text" name="group_name_"  style="width:375px">-->
											<button id="btnSelectGroup" name="clear" class="btn" type="button"><?php echo __('Save'); ?></button>
											<button id="btnCancel" name="clear" class="btn btnCancel" type="button"><?php echo __('Cancel'); ?></button>

											<div class="pagination"><ul class="paging bottomPagingPrivateGroup"></ul></div>
										</div>
									</div>

								</div>

							</div>

							<div class="tabbable">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tabStructTree" data-toggle="tab" class="tabControl"><i class="splashy-contact_blue"></i> <?php echo __('Structure Tree'); ?></a></li>
									<li><a href="#tabPublicGroup" data-toggle="tab" class="tabControl"><i class="splashy-group_blue"></i> <?php echo __('Public Group'); ?></a></li>
									<li><a href="#tabPrivateGroup" data-toggle="tab" class="tabControl"><i class="splashy-group_grey"></i> <?php echo __('Private Group'); ?></a></li>
									<!--<li><a href="#tabSearch" data-toggle="tab" class="tabControl"><i class="splashy-zoom"></i> Search</a></li>-->
								</ul>
								<div class="tab-content">

									<div class="tab-pane active" id="tabStructTree">
										<div class="antiScroll antiScroll2 antiscroll-inner2">
											<div id="organizations"><?php echo __('Load tree'); ?></div>
										</div>
									</div>

									<div class="tab-pane" id="tabPublicGroup">
											
										<div id="public_group_list" class="span10 antiscroll-inner2">
											<ul id="public_group_list" class="list public_group_list">
											<?php foreach ($publicGroups as $publicGroup) { ?>
												<li id="public_li_group_<?php echo $publicGroup['ContactGroup']['id'];?>">
													<span id="public_sp_count_<?php echo $publicGroup['ContactGroup']['id'];?>" class="label label-info pull-right sl_status"><?php echo $publicGroup['ContactGroup']['count_user'];?></span>
													<a id="public_label_<?php echo $publicGroup['ContactGroup']['id'];?>" onclick="groupclick('publicGroup', '<?php echo $publicGroup['ContactGroup']['id'];?>')" class="publicGroup sl_name" href="#" group="publicGroup" group_id="<?php echo $publicGroup['ContactGroup']['id'];?>"><?php echo $publicGroup['ContactGroup']['contact_group_name'];?></a>
													<!--<small class="s_color sl_email">johnd@example1.com</small>-->
												</li>
											<?php }?>

											</ul>

											<div class="pagination"><ul class="paging bottomPagingPublicGroup"></ul></div>

										</div>

									</div>

									<div class="tab-pane" id="tabPrivateGroup">
										<div id="private_group_list" class="span10 antiscroll-inner2">
											<ul id="private_group_list" class="list private_group_list">
											<?php foreach ($privateGroups as $privateGroup) { ?>
												<li id="private_li_group_<?php echo $privateGroup['ContactGroup']['id'];?>">
													
													<div id="divInfoGroup_<?php echo $privateGroup['ContactGroup']['id'];?>">
														<!--<span class="label pull-right"><i class="splashy-contact_blue_add addContactGroupUser" group_id="<?php echo $privateGroup['ContactGroup']['id'];?>"></i></span>-->
														<span class="label pull-right" onclick="editgroupname(<?php echo $privateGroup['ContactGroup']['id'];?>)"><i style="cursor:pointer;" class="splashy-pencil_small editGroup"  group_id="<?php echo $privateGroup['ContactGroup']['id'];?>"></i></span>
														<span class="label pull-right" onclick="deletegroupname(<?php echo $privateGroup['ContactGroup']['id'];?>)"><i style="cursor:pointer;" class="icon-trash deleteGroup" group_id="<?php echo $privateGroup['ContactGroup']['id'];?>"></i></span>
														<span id="private_sp_count_<?php echo $privateGroup['ContactGroup']['id'];?>" class="label label-info pull-right sl_status"><?php echo $privateGroup['ContactGroup']['count_user'];?></span>
														<a id="private_label_<?php echo $privateGroup['ContactGroup']['id'];?>" onclick="groupclick('privateGroup', '<?php echo $privateGroup['ContactGroup']['id'];?>')" class="privateGroup sl_name" href="#" group="privateGroup" group_id="<?php echo $privateGroup['ContactGroup']['id'];?>"><?php echo $privateGroup['ContactGroup']['contact_group_name'];?></a>
														<!--<small class="s_color sl_email">johnd@example1.com</small>-->
													</div>

													<div id="divEditGroup_<?php echo $privateGroup['ContactGroup']['id'];?>" class="input-append" style="display: none;">_<?php echo $privateGroup['ContactGroup']['id'];?>" group_id="<?php echo $privateGroup['ContactGroup']['id'];?>" type="text" name="edit_group_name" value="<?php echo $privateGroup['ContactGroup']['contact_group_name'];?>" readonly="readonly" style="width:350px">                                                                                                               <br                                                                                                             <input id="edit_is_public_<?php echo $privateGroup['ContactGroup']['id'];?>" type="checkbox" name="edit_is_public" <?php if($privateGroup['ContactGroup']['is_public']=='Y') echo "checked";?>>                                                                                                              <input id="edit_new_group_name_<?php echo $privateGroup['ContactGroup']['id'];?>" group_id="<?php echo $privateGroup['ContactGroup']['id'];?>" type="text" n 
													</div>

												</li>
						
											<?php }?>
												
											</ul>
											
											<div class="pagination"><ul class="paging bottomPagingPrivateGroup"></ul></div>
																					
										</div>

									</div>
									<!--
									<div class="tab-pane" id="tabSearch">
										<div id="search"><input id="searchUser" class="span10" type="text" placeholder="search"></div>
									</div>
									-->
								</div>
							</div>
						</div>
						
						<div class="span5">
							<div class="input-prepend">
								<span class="add-on ad-on-icon"><i class="icon-user"></i></span><input id="searchUser" class="span11" type="text" placeholder="<?php echo __('Search user'); ?>" />
							</div>

							<div id="user-list">
								<div class="row-fluid">
									
									<!--
									<div class="input-prepend">
										<span class="add-on ad-on-icon"><i class="icon-user"></i></span><input type="text" class="user-list-search search" placeholder="Search user" />
									</div>
									-->
									
									<ul class="nav nav-pills line_sep">
										<li><input id='all' onclick="select_memo_all()" class='select_rows' type='checkbox' name='row_sel'></li>

										<!--
										<li class="dropdown">
											<a class="dropdown-toggle" data-toggle="dropdown" href="#">Sort by <b class="caret"></b></a>
											<ul class="dropdown-menu sort-by">
												<li><a href="javascript:void(0)" class="sort" data-sort="sl_name">by name</a></li>
												<li><a href="javascript:void(0)" class="sort" data-sort="sl_status">by status</a></li>
											</ul>
										</li>
										<li class="dropdown">
											<a class="dropdown-toggle" data-toggle="dropdown" href="#">Show <b class="caret"></b></a>
											<ul class="dropdown-menu filter">
												<li class="active"><a href="javascript:void(0)" id="filter-none">All</a></li>
												<li><a href="javascript:void(0)" id="filter-online">Online</a></li>
												<li><a href="javascript:void(0)" id="filter-offline">Offline</a></li>
											</ul>
										</li>
										-->
										<li>
											<li><a id="groupControl" href="javascript:void(0)"> <i class="splashy-group_grey_add"></i> <?php echo __('Add Group'); ?></a></li>
										</li>
									</ul>

								</div>
								<div style="border: 1px solid #ccc; padding:3px; background:#f5f5f5; color:#000;">
									<?php echo __('User List'); ?>
								</div>
								<div class="antiscroll-inner2" id="user_list" style="height:200px; border:1px solid #ccc; padding:0 0 0 5px;" >
										<ul id="search_list" class="list user_list">
											<!--
											<li>
												<span class="label label-success pull-right sl_status">online</span><span><i class="icon-trash deleteUser"></i></span>
												<input class="select_rows" type="checkbox" name="row_sel"> <a href="#" class="sl_name">John Doe</a><br />
												
											</li>
											-->
											<?php if(!empty($contactUsers)) { ?>
											<?php foreach ($contactUsers as $contactUser) { ?>
											<?php if(!empty($contactUser['User']['UserProfile'])) { ?>
												<li>
													<span class="label label-success pull-right sl_status"><?php echo __('online'); ?></span>
													<a href="javascript:void(0)" onclick='selectUser(<?php echo $contactUser['User']['UserProfile']['id'];?>);' class="sl_name"><?php echo $contactUser['User']['UserProfile']['first_name_th'];?> <?php echo $contactUser['User']['UserProfile']['last_name_th'];?></a><br />
													<small class="s_color sl_email">kmiller@example1.com</small>
												</li>
											<?php }?>
											<?php }?>
											<?php }?>
											<!--
											<li>
												<span class="label label-important pull-right sl_status"><?=__('offline');?></span>
												<a href="#" class="sl_name">James Vandenberg</a><br />
												<small class="s_color sl_email">jamesv@example2.com</small>
											</li>
											-->
										</ul>
									</div>
									<div style="border: 1px solid #ccc; margin:8px 0 0 0; padding:3px; background:#f5f5f5; color:#000;">
										<?php echo __('Selected List'); ?>
									</div>
									<div class="antiscroll-inner2" id="memory_users" style=" border:1px solid #ccc; height: 150px; padding:0 0 0 5px;">
										<ul class="list memory_user_list" style="list-style:none;">
										</ul>
									</div>
									
								

								<div class="pagination"><ul class="paging bottomPaging"></ul></div>
								
							</div>
							

					</div>

<?php
if(empty($event)) $event = 'click_select';
?>

<link rel="stylesheet" href="css/custom/widget/user_directory.css" />
<script src="js/custom/widget/user_directory.js"></script>
<script src="js/custom/event/<?php echo $event;?>.js"></script>