<ul id="search_list" class="list user_list">
	<?php 
	
	if(!empty($users)){
		$online = '';
		$label = '';

		$user_id = '';
		$first_name = '' ;
		$last_name = '';
		$organization_id = '';
		$organization_name = '';
		$current_organization_id = '';
		$position_name = '';

		$contact_user_id = '';

		foreach($users as $user){
			$online = '';
			if(!empty($user['AuthUserAudit']['is_online'])){
				if($user['AuthUserAudit']['is_online'] == 'Y'){
					$label = 'label-success';
					$online = 'online';
				}else{
					$label = 'label-important';
					$online = 'offline';
				}
			}else{
					$label = 'label-important';
					$online = 'offline';		

			}
			
			if(!isset($user['Organization'])){
				if(!empty($user['UserOrganizationPosition'][0]['OrganizationPosition']['Organization'])){
					$user['Organization']['id'] = $user['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['id'];

					$user['OrganizationDescription'][0]['OrganizationDescription']['description'] = $user['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['OrganizationDescription'][0]['description'];	
				}
			}

			if(!isset($user['Position'])){
				$position = '';
				if(!empty($user['UserOrganizationPosition'][0]['OrganizationPosition']['Position'])){
					$position = $user['UserOrganizationPosition'][0]['OrganizationPosition']['Position']['position_name'];
				}

				$user['Position']['position_name'] = $position;	
			}

			$user_id =  $user['UserProfile']['user_id'];
			$first_name = $user['UserProfile']['first_name_th'];
			$last_name = $user['UserProfile']['last_name_th'];
			
			if(!empty($user['Organization']['id'])){
				$organization_id = $user['Organization']['id'];
				$organization_name = $user['OrganizationDescription'][0]['OrganizationDescription']['description'];	
			}
			
			$position_name = '';
			if(!empty($user['Position']['position_name'])) $position_name =  $user['Position']['position_name'];

			if($organization_id != $current_organization_id){
				echo $organization_name."<br />";

				$current_organization_id = $organization_id;
			}
			
			if($organization_id != $current_organization_id){
				if($current_organization_id=='')
					echo "<li organization_id='".$organization_id."'>".$organization_name."</li>";
				else
					echo "<li organization_id='".$organization_id."' class='group_organization'>&nbsp;".$organization_name."</li>";

				$current_organization_id = $organization_id;
			}

			$contact_user_id = '';
			if($action != 'getOrganizationUser' && $action != 'searchUserProfile') $contact_user_id = "id='li_user_".$user['ContactGroupUser']['id']."'";
			echo "<li ".$contact_user_id.">";

			if($event=='privateGroup') echo "<span class='label pull-right'><i class='icon-trash deleteUser' group_id='". $user['ContactGroup']['id'] ."' contact_user_id='". $user['ContactGroupUser']['id'] ."'></i></span>";
			echo "<span class='label pull-right'><i class='splashy-information userProfile' user_id='". $user_id ."' status='close'></i></span>";
			echo "<span class='label ". $label ." pull-right sl_status'>". $online ."</span>";
			echo "<input id='" . $user_id . "' class='select_row' type='checkbox' name='row_sel' onclick='select_memo_user(".$user_id.")'> ";
			echo "<a id='user_id_" . $user_id . "' href='javascript:void(0)' onclick='selectUser(" . $user_id . ");' class='sl_name'>" . $first_name . " " . $last_name ."</a>";
			echo "<small id='user_position_".$user_id."' class='s_color sl_email'>(" . $position_name . ")</small>";
			echo "<div id='profile_user_id_" . $user_id . "'></div>";
			echo "</li>";
			/*if(!empty($user['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['id'])){
				$organization_id = $user['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['id'];
				$organization_name = $user['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['OrganizationDescription'][0]['description'];	
			}*/

			/*if(!empty($user['Organization']['id'])){
				$organization_id = $user['Organization']['id'];
				$organization_name = $user['OrganizationDescription'][0]['description'];	
			}
			
			$position_name = '';
			if(!empty($user['UserOrganizationPosition'][0]['OrganizationPosition']['Position']['position_name'])) $position_name =  $user['UserOrganizationPosition'][0]['OrganizationPosition']['Position']['position_name'];

			if($organization_id != $current_organization_id){
				echo $organization_name."<br />";

				$current_organization_id = $organization_id;
			}*/

			
			
			/*if(!empty($user['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['id'])){
				$organization_id = $user['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['id'];
				$organization_name = $user['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['OrganizationDescription'][0]['description'];	
			}*/
			/*if(!empty($user['Organization']['id'])){
				$organization_id = $user['Organization']['id'];
				$organization_name = $user['OrganizationDescription'][0]['description'];		
			}
			
			$position_name = '';
			if(!empty($user['UserOrganizationPosition'][0]['OrganizationPosition']['Position']['position_name'])) $position_name =  $user['UserOrganizationPosition'][0]['OrganizationPosition']['Position']['position_name'];

			if($organization_id != $current_organization_id){
				if($current_organization_id=='')
					echo "<li organization_id='".$organization_id."'>".$organization_name."</li>";
				else
					echo "<li organization_id='".$organization_id."' class='group_organization'>&nbsp;".$organization_name."</li>";

				$current_organization_id = $organization_id;
			}
			$contact_user_id = '';
			if($action != 'getOrganizationUser' && $action != 'searchUserProfile') $contact_user_id = "id='li_user_".$user['ContactGroupUser']['id']."'";
			echo "<li ".$contact_user_id.">";

			if($event=='privateGroup') echo "<span class='label pull-right'><i class='icon-trash deleteUser' group_id='". $user['ContactGroup']['id'] ."' contact_user_id='". $user['ContactGroupUser']['id'] ."'></i></span>";
			echo "<span class='label pull-right'><i class='splashy-information userProfile' user_id='". $user_id ."' status='close'></i></span>";
			echo "<span class='label ". $label ." pull-right sl_status'>". $online ."</span>";
			echo "<input id='" . $user_id . "' class='select_row' type='checkbox' name='row_sel' onclick='select_memo_user(".$user_id.")'> ";
			echo "<a id='user_id_" . $user_id . "' href='javascript:void(0)' onclick='selectUser(" . $user_id . ");' class='sl_name'>" . $first_name . " " . $last_name ."</a>";
			echo "<small id='user_position_".$user_id."' class='s_color sl_email'>(" . $position_name . ")</small>";
			echo "<div id='profile_user_id_" . $user_id . "'></div>";
			echo "</li>";*/

		}
	}
	?>
</ul>


<script>
function select_memo_all(){
	
    setTimeout('check_memo_all()',1000);
	//alert(all);
}

function check_memo_all(){
	var txt='';
	var i = 1;
	var all= $('input[name=row_sel]:checked').length;
	$('input[name=row_sel]:checked').each(function(){
		var id = $(this).attr('id');
		var user = $('#user_id_' + id).text();
	    var position = $('#user_position_'+id).text();
		//alert(p);
		//alert($('#user_id_'+p).text());
		if(i++==all){
			txt+=id;
		}else{
			txt+=id+', ';
		}                            
		
		if(id != 'all'){
			var Ischecked = $('input[id='+id+']').is(':checked');
			var detail = user +' '+ position;
			var memo = $('#memo_list_'+id).attr('memo');
			if(Ischecked == true){
				if(id != memo){
					var list = "";
					list += "<li class='memory_user' memo="+id+"  style='margin:5px 0 0 0; padding:0;' id='memo_list_"+id+"'>";
					list +=		detail +' <i class="splashy-contact_blue_remove" style="cursor:pointer;" onclick="remove_memo_user('+id+')"></i>';
					list += "<span id='memo_user_"+id+"' style='display:none;'>"+user+"</span>";
					list += "</li>";
				}
			}		
			$('.memory_user_list').append(list);
		}
	});
	
	
}

function select_memo_user(id){
	var user = $('#user_id_' + id).text();
	var position = $('#user_position_'+id).text();
	//alert(id +' / '+user_name +' '+ position );
	var Ischecked = $('input[id='+id+']').is(':checked');
	var detail = user +' '+ position;
	var memo = $('#memo_list_'+id).attr('memo');
	if(Ischecked == true){
	  if(id != memo){
			var list = "";
			list += "<li class='memory_user' memo="+id+"  style='margin:5px 0 0 0; padding:0;' id='memo_list_"+id+"'>";
			list +=		detail +' <i class="splashy-contact_blue_remove" style="cursor:pointer;" onclick="remove_memo_user('+id+')"></i>';
			list += "<span id='memo_user_"+id+"' style='display:none;'>"+user+"</span>";
			list += "</li>";
	  }
	}
	
	$('.memory_user_list').append(list);
}

function remove_memo_user(id){
   $('#memo_list_'+id).remove();
}

</script>