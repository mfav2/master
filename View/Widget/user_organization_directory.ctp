					<div class="row-fluid">

						<div class="span8">

							<div class="tabbable">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tabStructTree" data-toggle="tab"><i class="splashy-contact_blue"></i> <?php echo __('Structure Tree'); ?></a></li>
									<li><a href="#tabPublicGroup" data-toggle="tab"><i class="splashy-group_blue"></i> <?php echo __('Public Group'); ?></a></li>
									<li><a href="#tabPrivateGroup" data-toggle="tab"><i class="splashy-group_grey"></i> <?php echo __('Private Group'); ?></a></li>
								</ul>
								<div class="tab-content">

									<div class="tab-pane active" id="tabStructTree">
										<div id="demo3"><?php echo __('Load tree'); ?></div>
									</div>

									<div class="tab-pane" id="tabPublicGroup">
											
										<div id="public-group-list" class="span8">
											<ul class="list public_group_list">
											<?php foreach ($publicGroups as $publicGroup) { ?>
												<li>
													<span class="label label-info pull-right sl_status">1</span>
													<a class="sl_name" href="#"><?php echo $publicGroup['ContactGroup']['contact_group_name'];?></a><br>
													<small class="s_color sl_email">johnd@example1.com</small>
												</li>
											<?php }?>
											</ul>
											<div class="pagination"><ul class="paging bottomPagingPublicGroup"></ul></div>
										</div>

									</div>

									<div class="tab-pane" id="tabPrivateGroup">

										<div id="private-group-list" class="span8">
											<ul class="list private_group_list">
											<?php foreach ($privateGroups as $privateGroup) { ?>
												<li>
													<span class="label label-info pull-right sl_status">1</span>
													<a class="sl_name" href="#"><?php echo $privateGroup['ContactGroup']['contact_group_name'];?></a><br>
													<small class="s_color sl_email">johnd@example1.com</small>
												</li>
											<?php }?>
											</ul>
											<div class="pagination"><ul class="paging bottomPagingPrivateGroup"></ul></div>
										</div>

									</div>
								</div>
							</div>
						</div>

						<div class="span4" id="user-list">
							<div class="row-fluid">
								<div class="input-prepend">
									<span class="add-on ad-on-icon"><i class="icon-user"></i></span><input type="text" class="user-list-search search" placeholder="<?php echo __('Search user'); ?>" />
								</div>
								<ul class="nav nav-pills line_sep">
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo __('Sort by'); ?> <b class="caret"></b></a>
										<ul class="dropdown-menu sort-by">
											<li><a href="javascript:void(0)" class="sort" data-sort="sl_name"><?php echo __('by name'); ?></a></li>
											<li><a href="javascript:void(0)" class="sort" data-sort="sl_status"><?php echo __('by status'); ?></a></li>
										</ul>
									</li>
									<li class="dropdown">
										<a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo __('Show'); ?> <b class="caret"></b></a>
										<ul class="dropdown-menu filter">
											<li class="active"><a href="javascript:void(0)" id="filter-none"><?php echo __('All'); ?></a></li>
											<li><a href="javascript:void(0)" id="filter-online"><?php echo __('Online'); ?></a></li>
											<li><a href="javascript:void(0)" id="filter-offline"><?php echo __('Offline'); ?></a></li>
										</ul>
									</li>
								</ul>
							</div>
							<ul class="list user_list">

								<li>
									<span class="label label-success pull-right sl_status"><?php echo __('online'); ?></span><span><i class="icon-trash"></i></span>
									<input class="select_row" type="checkbox" name="row_sel"> <a href="#" class="sl_name">John Doe</a><br />
									
								</li>
								
								<?php foreach ($contactUsers as $contactUser) { ?>
								<?php if(!empty($contactUser['User']['UserProfile'])) { ?>
									<li>
										<span class="label label-success pull-right sl_status"><?php echo __('online'); ?></span>
										<a href="#" class="sl_name"><?php echo $contactUser['User']['UserProfile']['first_name_th'];?> <?php echo $contactUser['User']['UserProfile']['last_name_th'];?></a><br />
										<small class="s_color sl_email">kmiller@example1.com</small>
									</li>
								<?php }?>
								<?php }?>
								<li>
									<span class="label label-important pull-right sl_status"><?php echo __('offline'); ?></span>
									<a href="#" class="sl_name">James Vandenberg</a><br />
									<small class="s_color sl_email">jamesv@example2.com</small>
								</li>
							</ul>
							<div class="pagination"><ul class="paging bottomPaging"></ul></div>
						</div>

					</div>
<?php
if(empty($event)) $event = 'click_select';
?>
<link rel="stylesheet" href="css/custom/widget/user_directory.css" />
<script src="js/custom/widget/user_directory.js"></script>
<script src="js/custom/event/<?php echo $event;?>.js"></script>