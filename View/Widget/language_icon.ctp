<?php
$currentUser = $this->Session->read('AuthUser');
$systemLanguages = $currentUser['SystemLanguage'];
$language_code = '';
?>

<ul class="dropdown-menu">
<?php 
foreach ($systemLanguages as $systemLanguage) {
if($systemLanguage['SystemLanguage']['id']==$currentUser['AuthUserAudit']['select_language']){ $language_code = $systemLanguage['SystemLanguage']['language_code'];}
?>
<li><a language_id="<?php echo $systemLanguage['SystemLanguage']['id'];?>" language_flag="<?php echo $systemLanguage['SystemLanguage']['language_code'];?>" language_directory="<?php echo $systemLanguage['SystemLanguage']['directory'];?>" chref="javascript:void(0)" class="language"><i class="flag-<?php echo $systemLanguage['SystemLanguage']['language_code'];?>"></i> <?php echo $systemLanguage['SystemLanguage']['language_name'];?></a></li>
<?php }?>
</ul>
<a href="#" class="dropdown-toggle nav_condensed" data-toggle="dropdown"><div id="flag"><i class="flag-<?php echo $language_code;?>"></i> <b class="caret"></b></div></a>
<script src="js/custom/widget/language_icon.js"></script>