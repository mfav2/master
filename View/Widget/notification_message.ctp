			<?php 
			    $currentUser = $this->Session->read('AuthUser');
			    $server_code = $currentUser['SystemServer']['server_code'];
			?>
			<button type="button" class="btn btn-mini" onclick="chk_all();"><?php echo __('Check All'); ?></button>
			<button type="button" class="btn btn-mini" onclick="unchk_all();"><?php echo __('Uncheck All'); ?></button>
			<button type="button" class="btn btn-mini" onclick="alldel();"><?php echo __('Delete'); ?></button>
			<br /><br />
			<table class="table table-striped">
                            <thead>
				<tr>
					<th>&nbsp;</th>
					<th><?php echo __('Sender');?></th>
					<th><?php echo __('Subject');?></th>
					<th><?php echo __('Date');?></th>
					<th> </th>
                                </tr>
                            </thead>
                            <tbody>
				<?php
				if(!empty($notifications)){
					$user_sender = array();
					foreach ($notifications as $notification) { 
					if($notification['NotificationAlertMessage']['from_user_id']!=''){
						$user_sender = $users[$notification['NotificationAlertMessage']['from_user_id']];
						$user_sender_display =  $user_sender['UserProfile']['first_name_th'] . ' ' . $user_sender['UserProfile']['last_name_th'];
					}else{
						$user_sender_display = $notification['NotificationAlertMessage']['from_user_id'];
					}
					?>
					<tr id="tr_<?php echo $notification['NotificationAlertMessage']['id'];?>">
						<td><input class="select_row" type="checkbox" name="row_sel" value="<?php echo $notification['NotificationAlertMessage']['id'];?>"  /></td>
					    <td><?php echo $user_sender_display;?></td>
					    <td><a href="<?php echo sprintf($notification['NotificationAlertMessage']['url'],$server_code);?>"><?php echo $notification['NotificationAlertMessage']['subject'];?></a></td>
					    <td><?php echo $notification['NotificationAlertMessage']['send_date'];?></td>
					    <td><a class="deleteMessage" title="Delete" href="#" value="<?php echo $notification['NotificationAlertMessage']['id'];?>"><i class="icon-trash"></i></a></td>
					</tr>
					<?}?>
				<?}?>
                            </tbody>
                        </table>
						
			<button type="button" class="btn btn-mini" onclick="chk_all();"><?php echo __('Check All'); ?></button>
			<button type="button" class="btn btn-mini" onclick="unchk_all();"><?php echo __('Uncheck All'); ?></button>
			<button type="button" class="btn btn-mini" onclick="alldel();"><?php echo __('Delete'); ?></button>

			<link rel="stylesheet" href="css/custom/widget/notification_message.css" />
			<script src="js/custom/widget/notification_message.js"></script>

<script language="javascript">
	function chk_all(){
			$("input[type=checkbox]").attr('checked', 'checked');
	}
	function unchk_all(){	
			$("input[type=checkbox]").removeAttr('checked', 'checked');
	}
	function modalAction(){
	
	}
	function alldel(){
		var all= $('input[name=row_sel]:checked').length;
		var i = 1;
		var l = 1;
		var txt='';
		if(all != 0){
			$('input[name=row_sel]:checked').each(function(){
				var p = $(this).val();
				if(i == all){
					txt+=p;
				}else{
					txt+= p+', ';
				}
				i++;
			});
			if(txt != ''){
				//alert(txt);
				if(confirm('Do you want delete notification message?')==true){
					$.ajax({
						cache: false, 
						url: "/Widget/deleteAllNotification",
						type: "post", 
						data: {'noti_id':txt,},
						success:function(data){
							$('input[name=row_sel]:checked').each(function(){
								var p = $(this).val();
								$('#tr_'+p).remove();
								
								//alert($(this).val());
							});
							//alert(data);
							//$('#viewshare_data').html(data);
						}
					});
				}
			}
		}else{
			alert('Please Select Notification');
			return false;
		}
		
	}
</script>