<?php 

$currentUser = $this->Session->read('AuthUser');
$user_id = $currentUser['AuthUser']['id'];

$notification_id = empty($currentUser['UserNotification']['id'])? '0':$currentUser['UserNotification']['id'];
$notification_mode_value = empty($currentUser['UserNotification']['notification_mode'])? 'summary':$currentUser['UserNotification']['notification_mode'];
$notification_type_value = empty($currentUser['UserNotification']['notification_type'])? '1':$currentUser['UserNotification']['notification_type'];
$notification_sound_value = empty($currentUser['UserNotification']['notification_sound'])? 'Y':$currentUser['UserNotification']['notification_sound'];
$notification_song_value = empty($currentUser['UserNotification']['notification_song'])? 'default.mp3':$currentUser['UserNotification']['notification_song'];

?>
<a class="label ttip_b" href="widget/shortcut/word" title="Open Word"><img src="img/icon/word.png"></a>
<a class="label ttip_b" href="widget/shortcut/excel" title="Open Excel"><img src="img/icon/excel.png"></a>
<a class="label ttip_b" href="widget/shortcut/powerpoint" title="Open Powerpoint"><img src="img/icon/powerpoint.png"></a>
<a id="myMail" class="label ttip_b" href="#myModal" data-backdrop="static" data-toggle="modal" title="New Mails"><span id="countMyMail"> 0 </span><i class="splashy-mail_light"></i></a>
<a id="myTask" class="label ttip_b" href="#myModal" data-backdrop="static" data-toggle="modal" title="New Tasks"><span id="countMyTask"> 0 </span><i class="splashy-calendar_week"></i></a>
<!--<a id="myMessage" class="label ttip_b" href="#myModal" data-backdrop="static" data-toggle="modal" title="New Message"><span id="countMyMessage"> 0 </span><i class="splashy-comments_reply"></i></a>-->
<!--
<a class="label ttip_b" href="<?php echo $this->Portal->makeUrl('Messages','index'); ?>"><span id="countMyMessage-"> 0 </span><i class="splashy-comments_reply"></i></a>
<a id="folderHandle" class="label ttip_b" href="#myModal" data-backdrop="static" data-toggle="modal" title="New Folder"><span id="countMyTask"> 0 </span><i class="splashy-folder_classic_opened_stuffed"></i></a>
<a id="userDirectory" class="label ttip_b" href="#myModal" data-backdrop="static" data-toggle="modal" title="New Folder"><span id="countMyTask"> 0 </span><i class="splashy-contact_blue_add"></i></a>
<a id="chatDirectory" class="label ttip_b" href="#myModal" data-backdrop="static" data-toggle="modal" title="New Folder"><span id="countMyTask"> 0 </span><i class="splashy-comments"></i></a>
-->

<span id="user_id" value="<?php echo $user_id;?>"></span>
<span id="notification_id" value="<?php echo $notification_id;?>"></span>
<span id="notification_mode_value" value="<?php echo $notification_mode_value;?>"></span>
<span id="notification_type_value" value="<?php echo $notification_type_value;?>"></span>
<span id="notification_sound_value" value="<?php echo $notification_sound_value;?>"></span>
<span id="notification_song_value" value="<?php echo $notification_song_value;?>"></span>
<?php
$currentUser = $this->Session->read('AuthUser');
$user_id = $currentUser['AuthUser']['id'];
?>
<script src="js/custom/widget/notification_icon.js"></script>