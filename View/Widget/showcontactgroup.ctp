<?php foreach ($privateGroups as $privateGroup) { ?>
        <li id="private_li_group_<?php echo $privateGroup['ContactGroup']['id'];?>">

                <div id="divInfoGroup_<?php echo $privateGroup['ContactGroup']['id'];?>">
                        <!--<span class="label pull-right"><i class="splashy-contact_blue_add addContactGroupUser" group_id="<?php echo $privateGroup['ContactGroup']['id'];?>"></i></span>-->
                        <span class="label pull-right" onclick="editgroupname(<?php echo $privateGroup['ContactGroup']['id'];?>)"><i style="cursor:pointer;" class="splashy-pencil_small editGroup"  group_id="<?php echo $privateGroup['ContactGroup']['id'];?>"></i></span>
                        <span class="label pull-right" onclick="deletegroupname(<?php echo $privateGroup['ContactGroup']['id'];?>)"><i style="cursor:pointer;" class="icon-trash deleteGroup" group_id="<?php echo $privateGroup['ContactGroup']['id'];?>"></i></span>
                        <span id="private_sp_count_<?php echo $privateGroup['ContactGroup']['id'];?>" class="label label-info pull-right sl_status"><?php echo $privateGroup['ContactGroup']['count_user'];?></span>
                        <a id="private_label_<?php echo $privateGroup['ContactGroup']['id'];?>" onclick="groupclick('privateGroup', '<?php echo $privateGroup['ContactGroup']['id'];?>')" class="privateGroup sl_name" href="#" group="privateGroup" group_id="<?php echo $privateGroup['ContactGroup']['id'];?>"><?php echo $privateGroup['ContactGroup']['contact_group_name'];?></a>
                        <!--<small class="s_color sl_email">johnd@example1.com</small>-->
                </div>

                <div id="divEditGroup_<?php echo $privateGroup['ContactGroup']['id'];?>" class="input-append" style="display: none;">
                       
                        <input id="edit_group_name_<?php echo $privateGroup['ContactGroup']['id'];?>" group_id="<?php echo $privateGroup['ContactGroup']['id'];?>" type="text" name="edit_group_name" value="<?php echo $privateGroup['ContactGroup']['contact_group_name'];?>" readonly="readonly" style="width:350px">
                        <br />
                        <input id="edit_is_public_<?php echo $privateGroup['ContactGroup']['id'];?>" type="checkbox" name="edit_is_public" <?php if($privateGroup['ContactGroup']['is_public']=='Y') echo "checked";?>>
                        <input id="edit_new_group_name_<?php echo $privateGroup['ContactGroup']['id'];?>" group_id="<?php echo $privateGroup['ContactGroup']['id'];?>" type="text" name="edit_group_name" value="" style="width:350px">
                        
                        <button group_id="<?php echo $privateGroup['ContactGroup']['id'];?>" onclick="checkeditbutton(<?php echo $privateGroup['ContactGroup']['id'];?>);" name="clear" class="btn " type="button">Done</button>

                </div>

        </li>

<?php }?>


<link rel="stylesheet" href="css/custom/widget/user_directory.css" />
<script src="js/custom/widget/user_directory.js"></script>
<!--<script>
function checkeditbutton(group_id){
    var group_name = $('#edit_group_name_'+group_id).val();
    var is_public = $('#edit_is_public_'+group_id).is(':checked');
    var new_group_name = $('#edit_new_group_name_'+group_id).val();
    
    if(is_public==true){
            group_type = 'public';
            is_public = 'Y';
    }else{
            group_type = 'private';
            is_public = 'N';
    }
    
    if(new_group_name != ""){
        group_name = new_group_name;
    }
    
    $("#divEditGroup_"+group_id).hide();
    $("#divInfoGroup_"+group_id).show();

    $.getJSON("/Widget/editContactGroup/"+group_id+"/"+group_name+"/"+is_public,
    function(data){

            $("#select_label_"+group_id).text(group_name);
            $("#public_label_"+group_id).text(group_name);
            $("#private_label_"+group_id).text(group_name);
    });
    
}
</script>-->