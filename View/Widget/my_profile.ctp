<?php foreach ($userProfiles as $userProfile) { ?>
<div class="row-fluid">
	<div class="span12">
		<div class="vcard">
			<img alt="" src="fileProviders/index/<?php echo $userProfile['UserProfile']['image_key'];?>" class="thumbnail">
			<ul>
				<li>
					<span class="item-key"><?php echo __('Assignment/Desk');?>&nbsp;:&nbsp;</span>
					<div class="vcard-item">
					<?php echo $this->Form->input('responsibility', array('id'=>'responsibility','class'=>'span12','type' => 'textarea','label' => false,'value' => $userProfile['UserProfile']['responsibility'])); ?>
					</div>
				</li>
				<li>	
						<span class="item-key"><?php echo __('Internal Tel');?>&nbsp;:&nbsp;</span>
						<div class="vcard-item"><?php echo $this->Form->input('phone_internal', array('class'=>'span12','type' => 'text','label' => false,'value' => $userProfile['UserProfile']['phone_internal'])); ?></div>
				</li>
				<li>	
						<span class="item-key"><?php echo __('Mobile');?>&nbsp;:&nbsp;</span>
						<div class="vcard-item">
						<?php echo $this->Form->input('phone_mobile', array('class'=>'span12','type' => 'text','label' => false,'value' => $userProfile['UserProfile']['phone_mobile'])); ?>
						<input type="checkbox" name="is_hide_phone_mobile" value="option1"<?php if($userProfile['UserProfile']['is_hide_phone_mobile']=='Y'){echo 'checked';} ?>>
						<?php echo __('Hide Mobile'); ?>
						</div>
				</li>
				<li>	
						<span class="item-key"><?php echo __('Direct Tel');?>&nbsp;:&nbsp;</span>
						<div class="vcard-item">
						<?php echo $this->Form->input('phone_direct', array('class'=>'span12','type' => 'text','label' => false,'value' => $userProfile['UserProfile']['phone_direct'])); ?>
						</div>
				</li>
				<li>	
						<span class="item-key"><?php echo __('Fax No');?>&nbsp;:&nbsp;</span>
						<div class="vcard-item">
						<?php echo $this->Form->input('fax_number', array('class'=>'span12','type' => 'text','label' => false,'value' => $userProfile['UserProfile']['fax_number'])); ?>
						</div>
				</li>
				<li>	
						<span class="item-key"><?php echo __('Email');?>&nbsp;:&nbsp;</span>
						<div class="vcard-item">
						<?php echo $this->Form->input('email', array('class'=>'span12','type' => 'text','label' => false,'value' => $userProfile['UserProfile']['email'])); ?>
						</div>
				</li>
				<li>	
						<span class="item-key"><?php echo __('VOIP');?>&nbsp;:&nbsp;</span>
						<div class="vcard-item">
						<?php echo $this->Form->input('voip', array('class'=>'span12','type' => 'text','label' => false,'value' => $userProfile['UserProfile']['voip'])); ?>
						</div>
				</li>
			</ul>
		</div>
	</div>
</div>
<?php echo $this->Form->input('id',array('type'=>'hidden','value'=>$userProfile['UserProfile']['user_id'],'id'=>'id_hidden')); ?>
<?php }?>
<?php
if(empty($event)) $event = 'click_select';
?>
<link rel="stylesheet" href="css/custom/widget/user_profile.css" />
<script src="js/custom/event/<?php echo $event;?>.js"></script>