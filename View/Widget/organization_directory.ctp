					<div class="row-fluid">

						<div class="span12">

							<div class="tabbable">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tabStructTree" data-toggle="tab" class="tabControl"><i class="splashy-contact_blue"></i> <?php echo __('Structure Tree'); ?></a></li>
								</ul>
								<div class="tab-content">

									<div class="tab-pane active" id="tabStructTree">
										<div class="antiScroll antiScroll2 antiscroll-inner2">
											<div id="organizations"><?php echo __('Load tree'); ?></div>
										</div>
									</div>

								</div>
							</div>
						</div>


					</div>
<?php
if(empty($event)) $event = 'click_select';
?>
<link rel="stylesheet" href="css/custom/widget/user_directory.css" />
<script src="js/custom/widget/user_directory.js"></script>
<script src="js/custom/event/<?php echo $event;?>.js"></script>