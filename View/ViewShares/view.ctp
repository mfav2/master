<?php  //echo $this->element('Components/breadcrumb'); ?>
<?php
//    if(!empty($this->request->data)){
//        $selected_year = $this->request->data['ViewShares']['selected_year'];
//        $tmp_selected_month = $this->request->data['ViewShares']['selected_month'];
//        $selected_month = $tmp_selected_month < 10 ? "0{$tmp_selected_month}" : $tmp_selected_month;
//    }else{
//        $selected_year = date("Y");
//        $selected_month = date("m");
//    }


    
    $current_date = date("Y-m-d");

    $timestamp = mktime(0, 0, 0, $selected_month, 1, $selected_year);
    $days_in_month = date("t", $timestamp);

    $tmp_month_list = range(1, 12);
    $month_list = array();
    foreach($tmp_month_list AS $i => $m){
        $month_list[$m] = date("M", mktime(0, 0, 0, $m, 1, $selected_year));
    }
    
    $tmp_years_list = range(date("Y") - 1, date("Y") + 1);
    foreach($tmp_years_list AS $i => $y){
        $years_list[$y] = $y;
    }
    
    echo "<div align='right'>";
    echo '<form style="margin-bottom:0px; margin-top:0px" method="post" action="' . $this->Portal->makeUrl($this->name, 'view') .'">';
    echo $this->form->input('selected_year', array('type'=>'select', 'label'=>false, 'div'=>false, 'options'=>$years_list, 'selected'=>$selected_year, 'style'=>'width:100px; margin:8px 5px 5px 0;', 'onchange'=>'this.form.submit()'));
    echo $this->form->input('selected_month', array('type'=>'select', 'label'=>false, 'div'=>false, 'options'=>$month_list, 'selected'=>$selected_month, 'style'=>'width:100px; margin:8px 0 5px 0;', 'onchange'=>'this.form.submit()'));
    echo $this->form->input('user_id', array('type'=>'hidden', 'value'=>$user_id));
    echo "</form>";
    echo "</div>";
    
    
    if(!empty($appointments['event_date'])){
        echo '<table class="table table-bordered table-striped table_vam">';
        echo "<tr><th>{$appointments['user_full_name']}</th>";
        echo "</tr>";
        
        for($i = 1; $i <= $days_in_month; $i++){
            $i = $i < 10 ? "0$i" : $i;
            $this_date = "$selected_year-$selected_month-$i";
            $day_of_week = date("w", strtotime($this_date));

            if($day_of_week == 0){
                $colorevent = "#ffbad2";
            }else if($day_of_week == 6){
                $colorevent = "#8f5e99";
            }else{
                $colorevent = "#F3F3F3;";
            }
            
            if($this_date == $current_date){
                $colorevent = "#F2F5A9";
            }
            
            $this_day_event_title = '';
            foreach($appointments['event_date'] AS $app_id => $event_date){
                $time = mktime(0, 0, 0, $selected_month, $i, $selected_year);
//                $datetime_today = date("Y-m-d H:i:s", $time);
//                $datetime_tomorrow = date("Y-m-d 23:59:00", $time);
                
                $event_datetime_start = $event_date['start_date'];
                $event_datetime_end = $event_date['end_date'];
                
                if(empty($event_date['appointment_name'])){
                    $event_date['appointment_name'] = $event_date['detail'];
                }
                
                
                $today_start_time = date("Y-m-d H:i:s", $time);
                $today_end_time = date("Y-m-d 23:59:59", $time);
                
                if($today_start_time <= $event_datetime_start){
                    $start_time = substr($event_datetime_start, 11, 5);
                }else{
                    $start_time = substr($today_start_time, 11, 5);
                }

                if($today_end_time >= $event_datetime_end){
                    $end_time = substr($event_datetime_end, 11, 5);
                }else{
                    $end_time = substr($today_end_time, 11, 5);
                }
                
                
                if($today_start_time > $event_datetime_start && $today_end_time < $event_datetime_end){
                    $event_title = "<div class='app-detail'>All day {$event_date['appointment_name']}</div>";
                }else if($today_start_time <= $event_datetime_start && $today_end_time >= $event_datetime_start){
                    $event_title = "<div class='app-detail'>{$start_time} - {$end_time} {$event_date['appointment_name']}</div>";
                }else if($today_start_time >= $event_datetime_start && $today_start_time <= $event_datetime_end){
                    $event_title = "<div class='app-detail'>{$start_time} - {$end_time} {$event_date['appointment_name']}</div>";
                }else{
                    $event_title = '';
                }
                
                $this_day_event_title .= $event_title;
            }
            
            echo "<tr><td style='background-color:" . $colorevent . "'>";
            echo "<div class='app-date' >" . date("D d M y", $time) . "</div>";
            echo $this_day_event_title;
            echo "</td></tr>";
        }
        echo "</table>";
    }else{
        echo '<table class="table table-bordered table-striped table_vam">';
        echo "<tr><th>{$user[$user_id]['detail']}</th>";
        echo "</tr>";
        
        for($i = 1; $i <= $days_in_month; $i++){
            $this_day_event_title = '';
            $time = mktime(0, 0, 0, $selected_month, $i, $selected_year);
            
            echo "<tr><td>";
            echo "<div class='app-date'>" . date("D d M y", $time) . "</div>";
            echo $this_day_event_title;
            echo "</td></tr>";
        }
        echo "</table>";
    }
    
    
?>

<?php //if(!empty($appointments)){ pr($appointments); } ?>

<style>
.table th, .table td {
    border-top: 1px solid #DDDDDD;
    line-height: 15px;
    padding: 3px;
    text-align: left;
    vertical-align: top;
}


.app-date{
    text-align: right;
    width:100px;
}

.app-detail{
    margin-left: 50px
}
</style>