<?php
    if(empty($selected_year)){ $selected_year = date("Y"); }
    if(empty($selected_month)){ $selected_month = date("m"); }

    $timestamp = mktime(0, 0, 0, $selected_month, 1, $selected_year);
    $days_in_month = date("t", $timestamp);

    $tmp_month_list = range(1, 12);
    $month_list = array();
    foreach($tmp_month_list AS $i => $m){
    //    $month_list[$m]['num_month'] = $m;
    //    $month_list[$m]['month_short_name'] = date("M", mktime(0, 0, 0, $m, 1, $selected_year));
        $month_list[$m] = date("M", mktime(0, 0, 0, $m, 1, $selected_year));
    }
    $p = 1800;
    $midnight_am = $timestamp + 60*60*7;
    $eleven_pm = $timestamp + 60*60*24;
   
?>
<table class="table table-bordered table_vam" >
    <tr valign="middle" style="background-color:#fff;">
        <th align="center" style="width:100%; text-align:center"><?= $selected_date."&nbsp;".__($month_list[(int)$selected_month]) . "&nbsp;" . $selected_year ?></th>
        <?php 
        for($i=$midnight_am; $i<$eleven_pm; $i+=$p){
              $t = date('H:i',$i);
              $t2 = date('H:i',$i+$p);
              echo "<th style='font-weight:normal; font-size:8px;'>";
              echo $t ."<br/>" .$t2;
              echo "</th>";
        }
        ?>
    </tr>
    <?php 
        if(!empty($user_list)){ 
                foreach ($user_list as $key => $list) {
                     $orgId[$key]  = $list['org_id'];
                     $pos_sort[$key] = $list['Position_sort'];
                }
                array_multisort($orgId, SORT_ASC, $pos_sort, SORT_ASC, $user_list);
                foreach($user_list as $i=> $list){
                    echo "<tr style='height:6px'>"; 
                    echo "<td style='font-size:12px;'>{$list['detail']}</td>";
                    
                    for($i=$midnight_am; $i<$eleven_pm; $i+=$p){
                            $t = date('H:i',$i);
                            $t2 = date('H:i',$i+$p);
                            $colorevent = "#FFF;";
                            //echo "<td>";
                            foreach($appointments as $userId=> $appointment){
                                foreach($appointment['event_date'] as $event_date){
                                    $time = mktime(0, 0, 0, $selected_month, $selected_date, $selected_year);
                                    $datetime_today = date("Y-m-d H:i:s", $time);
                                    $datetime_tomorrow = date("Y-m-d 23:59:00", $time);

                                    $event_datetime_start = $event_date['start_date'];
                                    $event_datetime_end = $event_date['end_date'];
                                    
                                    $start_time = substr($event_datetime_start, 11, 5);
                                    $end_time = substr($event_datetime_end, 11, 5);
                                    
                                   
                                    if($event_datetime_start <= $datetime_today && $event_datetime_end >= $datetime_tomorrow){
                                      if($list['user_id'] == $event_date['user_id']){  

                                        if($t >= $start_time){
                                            if($t< $end_time){
                                                // pr($event_date['user_id']);
                                                //echo "<div style='background:#92D2FF;'>&nbsp;&nbsp;</div>";
                                                $colorevent = "#058dc7;";
                                            }
                                        }
                                      }  
                                    }else if($event_datetime_start <= $datetime_today && $event_datetime_end < $datetime_tomorrow && $event_datetime_end >= $datetime_today){
                                      if($list['user_id'] == $event_date['user_id']){  

                                        if($t >= $start_time){
                                            if($t< $end_time){
                                                 //pr($event_date['user_id']);
                                                //echo "<div style='background:#92D2FF;'>&nbsp;&nbsp;</div>";
                                                $colorevent = "#058dc7;";
                                            }
                                        }
                                      }  
                                    }else if($event_datetime_start >= $datetime_today && $event_datetime_end <= $datetime_tomorrow){
                                      if($list['user_id'] == $event_date['user_id']){  

                                        if($t >= $start_time){
                                            if($t< $end_time){
                                                 //pr($event_date['user_id']);
                                                //echo "<div style='background:#92D2FF; margin:0px; padding:0px;'>&nbsp;&nbsp;</div>";
                                                $colorevent = "#058dc7;";
                                            }
                                        }
                                      }  
                                    }

                                }
                            }
                            echo "<td style='background:".$colorevent."'>&nbsp;&nbsp;</td>";
                            //echo "</td>";

                    }
                    
                    echo "</tr>";
                }
        }  
    ?>
</table>    
<style>
.table th, .table td {
    border-top: 1px solid #DDDDDD;
    line-height: 15px;
    padding: 3px;
    text-align: left;
    vertical-align: middle;
    
}


.app-date{
    text-align: right;
    width:100px;
}

.app-detail{
    margin-left: 50px
}
.modal {
    background-clip: padding-box;
    background-color: #FFFFFF;
    border: 8px solid rgba(0, 0, 0, 0.5);
    border-radius: 6px 6px 6px 6px;
    box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
    left: 50%;
    margin: -300px 0 0 -600px;
    overflow: auto;
    position: fixed;
    top: 50%;
    width: 1200px;
    z-index: 1050;
}

.modal-body {
    min-height: 450px;
    max-height: 450px;
    overflow-y: auto;
}
</style>
