<?php  echo $this->element('Components/breadcrumb'); ?>

<?php
//$templateMeta = $viewDefs[$this->name]['AddView']['templateMeta'];
//$buttons = $templateMeta['form']['buttons'];
//$panels = $viewDefs[$this->name]['AddView']['panels'];

//$datemonth = cal_days_in_month(CAL_GREGORIAN, $NumMonth, $SelectedYear); //use for call variable
if(empty($selected_year)){ $selected_year = date("Y"); }
if(empty($selected_month)){ $selected_month = date("m"); }

$current_date = date("Y-m-d");

$timestamp = mktime(0, 0, 0, $selected_month, 1, $selected_year);
$days_in_month = date("t", $timestamp);

$tmp_month_list = range(1, 12);
$month_list = array();
foreach($tmp_month_list AS $i => $m){
//    $month_list[$m]['num_month'] = $m;
//    $month_list[$m]['month_short_name'] = date("M", mktime(0, 0, 0, $m, 1, $selected_year));
    $month_list[$m] = date("M", mktime(0, 0, 0, $m, 1, $selected_year));
}
//pr($month_list);
//$SelectedMonth = date("M", $timestamp);

$tmp_years_list = range(date("Y") - 1, date("Y") + 1);
foreach($tmp_years_list AS $i => $y){
    $years_list[$y] = $y;
}
?>  
<div class="row-fluid">
    <div class="span12" align="right">
        <div id="viewshare_data" class="boxbar_content">
            <!---------------- Table ----------------->
            <?php
                echo '<form style="margin-bottom:0px; margin-top:0px" method="post" action="' . $this->Portal->makeUrl('ViewShares', 'index') .'">';

				echo "<a id='chooseuser' href='#myModal' class='btn' data-backdrop='static' data-toggle='modal'><i class='splashy-group_blue'></i></a>&nbsp;";
                
				//echo $this->form->input('filter_user', array('type'=>'text', 'label'=>false, 'div'=>false));

				echo $this->form->create('Highlevels', array('action'=>'highlevelindex'));
					
                echo $this->form->input('selected_year', array('type'=>'select', 'label'=>false, 'div'=>false, 'options'=>$years_list, 'selected'=>$selected_year, 'style'=>'width:100px; margin:8px 5px 5px 0;', 'onchange'=>'this.form.submit()'));
                echo $this->form->input('selected_month', array('type'=>'select', 'label'=>false, 'div'=>false, 'options'=>$month_list, 'selected'=>$selected_month, 'style'=>'width:100px; margin:8px 0 5px 0;', 'onchange'=>'this.form.submit()'));

                //echo $this->form->submit('Search', array('class'=>'btn', 'label'=>false, 'div'=>false));
				
                echo "</form>";
            ?>
				
            <table class="table table-bordered table-striped table_vam" >
                <tr style="background-color:#0695ff;">
                    <th align="center" class="textdatehigh" style="width:280px; text-align:center"><?= __($month_list[(int)$selected_month]) . "&nbsp;" . __($selected_year) ?></th>
                    <?php
                        for ($i = 1; $i <= $days_in_month; $i++) {
                            $Selecteddate = $selected_year . "-" . $selected_month . "-" . $i;
                            $DayOfWeek = date("w", strtotime($Selecteddate));

                            if ($DayOfWeek == 0){ //sunday
                                if($i < 10){
                                    echo "<th style='background:#ffbad2; color:#fff; cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>0{$i}</th>";
                                }else{
                                    echo "<th style='background:#ffbad2; color:#fff; cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>{$i}</th>";
                                }
                            }else if ($DayOfWeek == 6){ //saturday
                                if($i < 10){
                                    echo "<th style='background:#8f5e99; color:#fff; cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>0{$i}</th>";
                                }else{
                                    echo "<th style='background:#8f5e99; color:#fff; cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>{$i}</th>";
                                }
                            }else {
                                if($Selecteddate == $current_date){
                                    if($i < 10){
                                        echo "<th style='background:#F2F5A9; color:#fff; cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>0{$i}</th>";
                                    }else{
                                        echo "<th style='background:#F2F5A9; color:#fff; cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>{$i}</th>";
                                    }
                                }else{
                                    if($i < 10){
                                        echo "<th style='cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>0{$i}</th>";
                                    }else{
                                        echo "<th style='cursor:pointer;' onclick='showtimeline(".$i.",".$selected_month.",".$selected_year.")'>{$i}</th>";
                                    }
                                }
                            }
                        }
                    ?>
                </tr>
               <?php
              // pr($alluser_id);
               if(!empty($user_list)){  
                   foreach ($user_list as $key => $list) {
                        $orgId[$key]  = $list['org_id'];
                        $pos_sort[$key] = $list['Position_sort'];
                   }
                   array_multisort($orgId, SORT_ASC, $pos_sort, SORT_ASC, $user_list);
                   //pr($user_list);
                   foreach($user_list as $i=> $list){
                       echo "<tr style='height:6px'>";  
                       echo "<td><a href='{$this->Portal->makeUrl('Highlevels', 'view', 'user_id=' . $list['user_id'] . '&selected_year=' . $selected_year . '&selected_month=' . $selected_month)}'>{$list['detail']}</a></td>";  
                            for ($i = 1; $i <= $days_in_month; $i++) {

                                 $i = $i < 10 ? "0$i" : $i;

                                 $this_date = "$selected_year-$selected_month-$i";
                                 $day_of_week = date("w", strtotime($this_date));

                                 if($day_of_week == 0){
                                     $colorevent = "#ffbad2";
                                 }else if($day_of_week == 6){
                                     $colorevent = "#8f5e99";
                                 }else{
                                     $colorevent = "#F3F3F3;";
                                 }

                                 if($this_date == $current_date){
                                     $colorevent = "#F2F5A9";
                                 } 
                                 echo "<td align='center' valign='middle' style='background-color:" . $colorevent . "'>";           
                                 foreach($appointments AS $user_id => $ap){
                                   $location = ''; 
                                   foreach($ap['event_date'] AS $app_id => $event_date){
                                        $event_start_date = $event_date['start_date'];
                                        $event_end_date = $event_date['end_date'];
                                        $this_day_start_time = $this_date . " 00:00:00";
                                        $this_day_end_time =  $this_date . " 23:59:59";
                                        if(empty($event_date['appointment_name'])){
                                            $event_date['appointment_name'] = $event_date['detail'];
                                        }

                                        $arr_location = array(
                                            //1=>'<img src="../img/icon/B_trans1.gif" class="personPopupTrigger" rel="' . $event_date['appointment_name'] . '" />',
                                            //2=>'<img src="../img/icon/bus_trans1.gif" class="personPopupTrigger" rel="' . $event_date['appointment_name'] . '" />',
                                            //3=>'<img src="../img/icon/airplane_trans1.gif" class="personPopupTrigger" rel="' . $event_date['appointment_name'] . '" />'
                                            1=>'<div class="personPopupTrigger" rel="'.$ap['user_id'].','.$i.','.$selected_month.','.$selected_year.'"><img src="../img/icon/B_trans1.gif" style="cursor:pointer;" onclick="showevent('.$app_id.','.$ap['user_id'].','.$i.','.$selected_month.','.$selected_year.')" /></div>',
                                            2=>'<div class="personPopupTrigger" rel="'.$ap['user_id'].','.$i.','.$selected_month.','.$selected_year.'" ><img src="../img/icon/bus_trans1.gif" style="cursor:pointer;" onclick="showevent('.$app_id.','.$ap['user_id'].','.$i.','.$selected_month.','.$selected_year.')" /></div>',
                                            3=>'<div class="personPopupTrigger" rel="'.$ap['user_id'].','.$i.','.$selected_month.','.$selected_year.'" ><img src="../img/icon/airplane_trans1.gif" style="cursor:pointer;" onclick="showevent('.$app_id.','.$ap['user_id'].','.$i.','.$selected_month.','.$selected_year.')"  /></div>',
                                        );

                                        if($this_day_start_time >= $event_start_date && $this_day_start_time <= $event_end_date){
                                            $location = $arr_location[$event_date['location_type_id']];
                                        }else if($event_start_date >= $this_day_start_time && $event_start_date <= $this_day_end_time){
                                            $location = $arr_location[$event_date['location_type_id']];
                                        }else if($event_start_date >= $this_day_start_time && $event_end_date <= $this_day_end_time){
                                            $location = $arr_location[$event_date['location_type_id']];
                                        }                                       
                                   }
                                   if (!empty($location)){
                                            //$colorevent = "#92D2FF;";
                                   }
                                   if($user_id == $list['user_id']){
                                     echo $location;
                                   }
                                 }  
                                 
                                 echo "</td>";
                            }     
                       echo "</tr>";
                   }
               }
                
               ?>
            </table>
            <!---------------- End ------------------->
        </div>
    </div>
</div>
<input id="alluser" type="hidden" value="<?php echo $alluser_id; ?>" />
<?php //if(!empty($appointments)){ pr($appointments); } ?>

<?php //if(!empty($this->request->data)){ pr($this->request->data); } ?>

<script src="/lib/qtip2/jquery.qtip.min.js"></script>
<script>

$(document).ready(function(){
      $('div.personPopupTrigger').each(function() {
            $currentLink = $(this);
            var settings = $currentLink.attr('rel').split(',');
            var eventid = 0;
            var userId = settings[0];
            var selectday = settings[1];
            var selectmonth = settings[2];
            var selectyear = settings[3];
            $currentLink.qtip({
                content: {
                    text: 'Loading...',
                    ajax: {
                        url: '/Highlevels/highlevelapi/'+eventid+'/'+userId+'/'+selectday+'/'+selectmonth+'/'+selectyear,
                        loading: false
                    }
                },

                position: {
                        my: 'top left',
                        target: 'mouse',
                        viewport: $(window), // Keep it on-screen at all times if possible
                        adjust: {
                        x: 10,  y: 10
                        }
                },
                hide: {
                        fixed: true // Helps to prevent the tooltip from hiding ocassionally when tracking!
                },
                style: 'qtip-shadow',
                show: 'mouseover',
                hide: 'mouseout'
            });
      });    

      $("#chooseuser").click(function(event){
		  $('#customModalHeader').html('User Directory');
		  $('#customModalAction').html('Next');
		  $('#customModal').load("/Widget/userDirectory/view_share?_="+Math.random(),function(data) {
		  //$('#customModal').html(data);
		  });
	  }); 
});
function showevent(event_id, userid, selectday, selectmonth, selectyear){
    $('#myModal').modal('show');
    $('#customModal').empty();
    $('#customModalHeader').html('<?=__('Appointment')?>');
    $('#customModalAction').html('<?=__('Next')?>');
    $('#customModal').load("/Highlevels/highlevelapi/"+event_id+'/'+userid+'/'+selectday+'/'+selectmonth+'/'+selectyear,function(data) {
        //$('#customModal').html(data);
    });
}

function showtimeline(selectday, selectmonth, selectyear){
    var alluser  = $('#alluser').val();
        $('#myModal').modal('show');
        $('#customModal').empty();
        $('#customModalHeader').html('<?=__('Timeline')?>');
        $('#customModalAction').html('<?=__('Close')?>');
     $.ajax({
        cache: false, 
        url: "/ViewShares/sharedtimeline",
        type: "post", 
        data: {'alluser': alluser, 'selecteddate':selectday, 'selectedmonth':selectmonth, 'selectedyear':selectyear },
        success:function(data){
            $('#customModal').html(data);
        }
     });
    
}
function modalAction(){
    $('#myModal').modal('hide');
}

</script>
<style>
.table th, .table td {
    border-top: 1px solid #DDDDDD;
    line-height: 15px;
    padding: 3px;
    text-align: left;
    vertical-align: top;
}

</style>