function modalAction(){
	event_action.init();
}

event_action = {
    init: function(){
        var groupname = $('#groupname').val();
		var group_id  = $('#groupid').val();

		var txt='';
		var i = 1;
		var all= $('input[name=rowname]:checked').length;
		$('input[name=rowname]:checked').each(function(){
			var p = $(this).attr('id');
			//alert(p);
			if(i==all){
				txt+=p;
			}else{
				txt+=p+', ';
			}                            
			i++;
		});

	$.ajax({
            type: "POST",
            url: "Messages/changegroupname?_="+Math.random(),
            cache: false,
            data: {'group_id': group_id, 'groupname' : groupname, 'users_list': txt},
            success: function(data){
                
				if(data == 1){
					$('#myModal').modal('hide');
				}
            }
        });  
		
    }
};