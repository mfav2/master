function modalAction(){
	event_action.init();
}

event_action = {
    init: function(){
        var old = $('#old_pass').val();
        var newpass = $('#new_pass').val();
        var renew = $('#re_new_pass').val();
        var url = 'Widget/changePassword/';
        if(old==''){
            alert('กรุณาใส่รหัสผ่านเดิม !');
            return false;
        }
        if(newpass.length<8 || renew.length<8){
            alert('กรุณาใส่รหัสผ่านอย่างน้อย 8 ตัวอักษร !');
            return false;
        }else{
            if(newpass == renew){
                $.post(url, {
                    password:old,
                    new_password:newpass,
                    re_new_password:renew
                }, function(data){
                    if(data=='incurrect'){
                        alert('Old Password incurrect');
                    }else if(data=='saved'){
                        alert('เปลี่ยนรหัสผ่านเรียบร้อย');
                        $('#customModalAction').attr('data-dismiss','modal');
                        $('#myModal').modal('hide');
                    }else if(data=='incurrectpass'){
                        alert('กรุณาใส่รหัสผ่านใหม่ !');
                    }
                });
            }else{
                alert('รหัสผ่านใหม่ไม่ตรงกัน !');
            }
        }
    }
};

