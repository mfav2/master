	
		function modalAction(){
			event_action.init();
		}

		function selectUser(user_id){
			event_selectUser.init(user_id);
		}

		selectOrganizationUser = {
			init: function(organization_id){
				
				//alert('ORG - '+organization_id);
				//var contact_group_id = $(this).attr("data-content");

				get_user.init('getOrganizationUser',organization_id);
				//user_list.init();

			}
		};

		event_selectUser = {
			init: function(user_id){
				
				//alert('USER - '+user_id);
				var user_name = $('#user_id_'+user_id).text();

			}
		};

		event_selectOrganization = {
			init: function(organization_id){
				
				//alert('ORGss - '+organization_id);

			}
		};
		
		// v1
		event_action_ = {
			init: function(){

				//alert('event_action');

				var txt='';
				var i = 1;
				var all= $('input[name=row_sel]:checked').length;
				$('input[name=row_sel]:checked').each(function(){
					var p = $(this).attr('id');
					//alert(p);
					if(i==all){
						txt+=p;
					}else{
						txt+=p+', ';
					}                            
					i++;
				});
                
                if(txt != ''){
                    $('#addGroup').load("/Messages/addGroupForm/");
                    $('#addGroup').dialog({
                        title: 'Add by Person or Group?',
                        modal: true,
                        buttons:{
                            "OK":function(){ 
                                if($('#contact_type').val() == 1){
                                    //// DEBUGGING!!! IT's not working
                                    //alert('contact_type = 1');
                                    //alert(txt);
									var contact_type = $('#contact_type').val();
                                    var group_name = $('#add_group_name').val();
                                    
                                    $.post("/Messages/addGroup/", {users_list : txt, group_name : group_name, contact_type : contact_type}, function(){
                                        $('#addGroup').dialog( "close" );
                                    });
                                        
                                }else if($('#contact_type').val() == 2){
                                    ////OK
									var contact_type = $('#contact_type').val();
                                    var group_name = $('#add_group_name').val();
                                    
                                    if(group_name == ''){
                                        $('#group_name').after('<p style="color:red; display:inline">***Require!!!</p>');
                                    }else{
                                        //alert(group_name);
                                        //alert(txt);
                                        
                                        $.post("/Messages/addGroup/", {contact_type : contact_type, users_list : txt, group_name : group_name}, function(){
                                            $('#addGroup').dialog( "close" );
                                        });
                                        
                                    }
                                }
                                
                                $.post('/Messages/contactList/', function(data){
                                    $('.chat_user_list').html(data);
                                });
                            },
                            Cancel: function() {
                                $( this ).dialog( "close" );
                            }
                        }
                    });
                    
                }


			}
		};
		
		// v2
		event_action_ = {
			init: function(){

				//alert('event_action');

				var txt = '';
				var i = 1;
				var all = $('.memory_user').length;
				$('li.memory_user').each(function() {
					
					var id = $(this).attr('memo');
					var user_name =  $('#memo_user_' + id).text();
					
					if (i == all) {
						txt += id;
					}
					else {
						txt += id + ', ';
					}
					i++;
				});
                $('#myModal').modal('hide');
                if(txt != ''){
                    $('#addGroup').load("/Messages/addGroupForm/");
                    $('#addGroup').dialog({
                        title: 'Add by Person or Group?',
                        modal: true,
                        buttons:{
                            "OK":function(){ 
                                if($('#contact_type').val() == 1){
                                    //// DEBUGGING!!! IT's not working
                                    //alert('contact_type = 1');
                                    //alert(txt);
									var contact_type = $('#contact_type').val();
                                    var group_name = $('#add_group_name').val();
                                    
                                    $.post("/Messages/addGroup/", {users_list : txt, group_name : group_name, contact_type : contact_type}, function(){
                                        $('#addGroup').dialog( "close" );
                                    });
                                        
                                }else if($('#contact_type').val() == 2){
                                    ////OK
									var contact_type = $('#contact_type').val();
                                    var group_name = $('#add_group_name').val();
                                    
                                    if(group_name == ''){
                                        $('#group_name').after('<p style="color:red; display:inline">***Require!!!</p>');
                                    }else{
                                        //alert(group_name);
                                        //alert(txt);
                                        
                                        $.post("/Messages/addGroup/", {contact_type : contact_type, users_list : txt, group_name : group_name}, function(){
                                            $('#addGroup').dialog( "close" );
                                        });
                                        
                                    }
                                }
                                
                                $.post('/Messages/contactList/', function(data){
                                    $('.chat_user_list').html(data);
                                });
                            },
                            Cancel: function() {
                                $( this ).dialog( "close" );
                            }
                        }
                    });
                    
                }


			}
		};