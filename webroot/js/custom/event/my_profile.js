function modalAction(){
    event_action.init();
}

event_action = {
    init: function(){
        var id = $('#id_hidden').val();
        //var responsibility = $('input[name="data[responsibility]"]').val();
        var responsibility = $('#responsibility').val();
        var phone_internal = $('input[name="data[phone_internal]"]').val();
        var phone_mobile = $('input[name="data[phone_mobile]"]').val();
        var phone_hide = 'N';
        if ($('input[name="is_hide_phone_mobile"]').attr('checked')){
            phone_hide = 'Y';
        }
        var email = $('input[name="data[email]"]').val();
        var voip = $('input[name="data[voip]"]').val();
        var phone_direct = $('input[name="data[phone_direct]"]').val();
        var fax_number = $('input[name="data[fax_number]"]').val();
        var url = 'Widget/setUserProfile/';
        $.post(url,{
            user_id:id,
            responsibility:responsibility,
            phone_internal:phone_internal,
            phone_mobile:phone_mobile,
            phone_direct:phone_direct,
            fax_number:fax_number,
            email:email,
            voip:voip,
            is_hide_phone_mobile:phone_hide
        },function(data){
            
        });
        
        
    }
};

