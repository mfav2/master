/* [ ---- Gebo Admin Panel - extended form elements ---- ] */
	$(document).ready(function() {

		gebo_datepicker.init();
                //* WYSIWG editor
                gebo_wysiwg.init();
		
		$('.open_modal_form').click(function(e) {
			$.colorbox({
				href: '#modal_form',
				inline: true,
				opacity: '0.2',
				fixed: true,
				scrolling: false
			});
			e.preventDefault();
		});
		
		//* password strength checker
		//gebo_pass_check.init();
		
	});
	
        //appointment_add
	
	//* bootstrap datepicker
	gebo_datepicker = {
		init: function() {
			$('#dp1').datepicker().on('changeDate', function(ev){
                            $('#dp1').datepicker('hide');
                        });
			//$('#dp2').datepicker();
			$('#dp_expire').datepicker({format: "mm/dd/yyyy"})
                        
			$('#dp_start').datepicker({format: "mm/dd/yyyy"}).on('changeDate', function(ev){
				var dateText = $(this).data('date');
				
				var endDateTextBox = $('#dp_end input');
				if (endDateTextBox.val() != '') {
					var testStartDate = new Date(dateText);
					var testEndDate = new Date(endDateTextBox.val());
					if (testStartDate > testEndDate) {
						endDateTextBox.val(dateText);
					}
				}
				else {
					endDateTextBox.val(dateText);
				};
				$('#dp_end').datepicker('setStartDate', dateText);
				$('#dp_start').datepicker('hide');
			});
			$('#dp_end').datepicker({format: "mm/dd/yyyy"}).on('changeDate', function(ev){
				var dateText = $(this).data('date');
				var startDateTextBox = $('#dp_start input');
				if (startDateTextBox.val() != '') {
					var testStartDate = new Date(startDateTextBox.val());
					var testEndDate = new Date(dateText);
					if (testStartDate > testEndDate) {
						startDateTextBox.val(dateText);
					}
				}
				else {
					startDateTextBox.val(dateText);
				};
				$('#dp_start').datepicker('setEndDate', dateText);
				$('#dp_end').datepicker('hide');
			});
			$('#dp_modal').datepicker();
		}
	};
    
    //* TinyMce
	gebo_wysiwg = {
		init: function(){
			// File Browser
            function openKCFinder(field_name, url, type, win) {
                alert("Field_Name: " + field_name + "nURL: " + url + "nType: " + type + "nWin: " + win); // debug/testing
                tinyMCE.activeEditor.windowManager.open({
                    file: '/file-manager/browse.php?opener=tinymce&type=' + type,
                    title: 'KCFinder',
                    width: 700,
                    height: 500,
                    resizable: "yes",
                    inline: true,
                    close_previous: "no",
                    popup_css: false
                }, {
                    window: win,
                    input: field_name
                });
                return false;
            };
            var FontSize = '12pt',
                ac;
            $('textarea#wysiwg_full').tinymce({
                // Location of TinyMCE script
                script_url 							: 'lib/tiny_mce/tiny_mce.js',
                // General options
                theme 								: "advanced",
                plugins 							: "autoresize,style,table,advhr,advimage,advlink,emotions,inlinepopups,preview,media,contextmenu,paste,fullscreen,noneditable,xhtmlxtras,template,advlist",
                // Theme options
                theme_advanced_buttons1 			: "undo,redo,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,fontselect,fontsizeselect",
                theme_advanced_buttons2 			: "forecolor,backcolor,|,cut,copy,paste,pastetext,|,bullist,numlist,link,image,media,|,code,preview,fullscreen",
                theme_advanced_buttons3 			: "",
                theme_advanced_toolbar_location 	: "top",
                theme_advanced_toolbar_align 		: "left",
                theme_advanced_statusbar_location 	: "none",
                theme_advanced_font_sizes : "8=8pt,10=10pt,12=12pt,14=14pt,16=16pt,18=18pt,24=24pt,28=28pt,32=32pt",
                theme_advanced_resizing 			: false,
                //font_size_style_values 				: "10pt,12pt,14pt,16pt,18pt,24pt,36pt",
                init_instance_callback				: function(){
                        
                        ac = tinyMCE.activeEditor;
                        ac.getBody().style.fontSize = '12pt';

                        ac.execCommand("fontName", false, "Times New Roman");
                        ac.execCommand("fontSize", false, "12pt");
                    function resizeWidth() {
                        document.getElementById(tinyMCE.activeEditor.id+'_tbl').style.width='100%';
                    }
                    resizeWidth();
                    $(window).resize(function() {
                        resizeWidth();
                    })
                },
                // file browser
                file_browser_callback: function openKCFinder(field_name, url, type, win) {
                    tinyMCE.activeEditor.windowManager.open({
                        file: 'file-manager/browse.php?opener=tinymce&type=' + type + '&dir=image/themeforest_assets',
                        title: 'KCFinder',
                        width: 700,
                        height: 500,
                        resizable: "yes",
                        inline: true,
                        close_previous: "no",
                        popup_css: false
                    }, {
                        window: win,
                        input: field_name
                    });
                    return false;
                }
            });
            /********************************************************************/
            $('textarea.wysiwg_mini').tinymce({
                // Location of TinyMCE script
                script_url 							: 'lib/tiny_mce/tiny_mce.js',
                // General options
                theme 								: "advanced",
                plugins 							: "autoresize,style,table,advhr,advimage,advlink,emotions,inlinepopups,preview,media,contextmenu,paste,fullscreen,noneditable,xhtmlxtras,template,advlist",
                // Theme options
                theme_advanced_buttons1 			: "fontselect,fontsizeselect,|,bold,italic,underline,forecolor,backcolor,|,bullist,numlist,justifyleft,justifycenter,justifyright,justifyfull,|,link,image,media",
                theme_advanced_buttons2 			: "",
                theme_advanced_buttons3 			: "",
                theme_advanced_toolbar_location 	: "top",
                theme_advanced_toolbar_align 		: "left",
                theme_advanced_statusbar_location 	: "none",
                theme_advanced_font_sizes : "8=8pt,10=10pt,12=12pt,14=14pt,16=16pt,18=18pt,24=24pt,28=28pt,32=32pt",
                theme_advanced_resizing 			: false,
                //font_size_style_values 				: "10pt,12pt,14pt,16pt,18pt,24pt,36pt",
                init_instance_callback	 : function(){
                    
                        ac = tinyMCE.activeEditor;
                        ac.getBody().style.fontSize = '12pt';
                        
                    ac.execCommand("fontName", false, "Times New Roman");
                    ac.execCommand("fontSize", false, "12pt");
                    function resizeWidth() {
                        document.getElementById(tinyMCE.activeEditor.id+'_tbl').style.width='100%';
                    }
                    resizeWidth();
                    $(window).resize(function() {
                        resizeWidth();
                    })
                },
                // file browser
                file_browser_callback: function openKCFinder(field_name, url, type, win) {
                    tinyMCE.activeEditor.windowManager.open({
                        file: 'file-manager/browse.php?opener=tinymce&type=' + type + '&dir=image/themeforest_assets',
                        title: 'KCFinder',
                        width: 700,
                        height: 500,
                        resizable: "yes",
                        inline: true,
                        close_previous: "no",
                        popup_css: false
                    }, {
                        window: win,
                        input: field_name
                    });
                    return false;
                }
            });
            /********************************************************************/
            
         
		}
	};
   
	
	