/* [ ---- Gebo Admin Panel - calendar ---- ] */

	$(document).ready(function() {
                var user_id = '';
				var startdate = $('#start_date').val();
				var enddate = $('#end_date').val();
		gebo_calendar.regular(user_id, startdate, enddate);
		//* resize elements on window resize
		var lastWindowHeight = $(window).height();
		var lastWindowWidth = $(window).width();
                $("a#closepopup, #closepopup").click(function(){
                    $("#personal").modal("hide"); 
                });
                
		$(window).on("debouncedresize",function() {
			if($(window).height()!=lastWindowHeight || $(window).width()!=lastWindowWidth){
				lastWindowHeight = $(window).height();
				lastWindowWidth = $(window).width();
				//* rebuild calendar
				$('#calendar').fullCalendar('render');
				//$('#calendar_google').fullCalendar('render');
			}
		});
                $('#Ownercalendar').change(function() {
                     var user_id = $('#Ownercalendar option:selected').val();
					 var startdate = $('#start_date').val();
					 var enddate = $('#end_date').val();
                     //$('#ownercal').val(chk);
                     /********************************/
                     $('#userId').val(user_id);
                     $('#calendar').empty();
                     gebo_calendar.regular(user_id, startdate, enddate);
                     
                });  
				
	});
	
	
	
	//* calendar
	gebo_calendar = {
		regular: function(user_id, startdate, enddate) {
			var date = new Date();
			var d = date.getDate();
            var m = date.getMonth();
			var y = date.getFullYear();
                       
			var calendar = $('#calendar').fullCalendar({                             
                                header: {
					left: 'prev next',
                                        //center: 'title,today',
                                        center: 'title,today',
                                        //right: 'month,fourday,agendaWeek,agendaDay'
                                        //right: 'agendaDay,agendaWeek,month,fourday'
                                        right: 'month'
					
					
				},
                                showAgendaButton: true,
                                selectMY: {
                                        years: 2
                                },
				buttonText: {
					prev: '<i class="icon-chevron-left cal_prev" />',
					next: '<i class="icon-chevron-right cal_next" />'
                                        
				},
                                allDayText: 'All day',
				aspectRatio: 2,
				selectable: true,
				selectHelper: true,
				select: function(start, end, allDay) {
                                    $('#myModal').modal('show');
                                    $('#customModalHeader').html('New Appointment');
                                    $('#customModalAction').html('Close');
                                    $('#customModal').load("/appointments/addminicalendar/"+user_id+"/?_="+Math.random(),function(data) {
                                        $('#customModal').html(data);  
                                        //var endtime = $.fullCalendar.formatDate(end, "HH:mm");
//                                        if(endtime == '00:00'){
//                                           endtime = '23:59'; 
//                                        }
                                        var starttime = '08:30';
                                        var endtime = '16:30';
                                        $('#fdate').val($.fullCalendar.formatDate(start, "dd/MM/yyyy"));
                                        $('#tdate').val($.fullCalendar.formatDate(end, "dd/MM/yyyy"));
                                        $('#ftime').val(starttime);
                                        $('#ttime').val(endtime);
                                        
//                                        $('#formatstartdate').val($.fullCalendar.formatDate(start, "MM/dd/yyyy"));
//                                        $('#formatenddate').val($.fullCalendar.formatDate(start, "MM/dd/yyyy"));
                                        $("#submitminiForm").click(function(){
                                            var title = $('#AppointmentName').val();
                                            var place = $('#AppointmentLocation').val();
                                            var detail = $('#AppointmentPrivateNote').val();
                                            var color = $('input[id=color_id]:checked').attr('color');
                                            var sdate = $('#fdate').val();
                                            var stime = $('#ftime').val();
                                            //var starts = sdate+' '+stime+':00';
					    					var tdate = $('#tdate').val();
                                            var ttime = $('#ttime').val();
                                            
                                            //var startdate = new Date(starts);
                                            var startdate = sdate.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/,"$2$1$3");  
                                            var enddate = tdate.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/,"$2$1$3");
                                            
                                            
                                            
   

                                            if (title != '' && detail!='') {
                                                  setTimeout(function() {
                                                     var appoint_id = $('#ApptId').val();   
                                                     //alert(appoint_id);
                                                     var starts = startdate+' '+stime+':00';
                                                     var ends = enddate+' '+ttime+':00';
                                                     
                                                    
                                                     calendar.fullCalendar('renderEvent',
							{
								title: '',
								start: starts,
								end: ends,
                                                                description: detail,
                                                                url: 'appointments/view/'+appoint_id,
								backgroundColor: color
                                                                
                                                                
							},
							true // make the event "stick"
                                                        
                                                    );
                                                   $("#myModal").modal('hide');     
                                                  }, 2000);
						
                                                
                                            }
                                            calendar.fullCalendar('unselect');
                                            
                                        });
                                    });
					
				},
                                more: 2,
				editable: false,
				theme: false,
                                /*eventSources: [

                                    // your event source
                                    {
                                        url: 'AppointmentsAPI/showdata/'+startdate+'/'+enddate+'/'+user_id+'/', // use the `url` property
                                        textColor: 'black',  // an option!
                                        allDay:false
                                    }

                                    // any other sources...
                                    
                                ],*/
								events : function(start, end, callback){
									
									var startdate = $('#start_date').val();
									var enddate = $('#end_date').val();
									
									//alert(startdate+' ' + enddate + ' '+user_id);
									$.ajax({
										url: 'AppointmentsAPI/showdata/'+startdate+'/'+enddate+'/'+user_id+'/?_='+Math.random(),
										dataType: 'json',
										cache: false,
										success: function(doc){
							
											var my_events = [];
											if(doc != ''){
												$.each(doc.appointment, function (index, elem){
												//alert(elem.title);
													my_events.push({
														title: elem.title,
														start: elem.start,
														end: elem.end,
														description:elem.description,
														img: elem.img,
														url: elem.url,
														color : elem.color,
														allDay: elem.allDay,
														remind : elem.remind
													});
												});
												callback(my_events);
											}
											

										}	
									});
								},
                                eventRender: function(event, element) {
									
                                    if(event.img){
                                       element.find(".fc-event-title").after($("<span class=\"fc-event-icons\"></span>").html('<img src="'+event.img+'" />')); 
                                    }
                                    if(event.remind == 'Y'){
                                       element.find(".fc-event-title").after($("<span class=\"fc-event-icons\"></span>").html('<img src="img/icon/time.png" />')); 
                                    }
									element.qtip({

										content: event.description,
										position: {
												my: 'top left',
												target: 'mouse',
												viewport: $(window), // Keep it on-screen at all times if possible
												adjust: {
												x: 10,  y: 10
												}
										},
										hide: {
												fixed: true // Helps to prevent the tooltip from hiding ocassionally when tracking!
										},
										style: 'qtip-shadow'
									});
                                      
                                }         
			});
                    
                        
		}
	};
        //* calendar
	