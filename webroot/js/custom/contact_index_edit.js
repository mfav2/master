$(document).ready(function() {
    
    $('#titlename2').hide();
    $('label#title2').hide();


    
    
$('#titlename').change(function(){
    var title = $('#titlename').val();
    if(title == 'Etc')
    {
        $('label#title2').show();
        $('#titlename2').show();
    } else {
        $('label#title2').hide();
        $('#titlename2').hide();
    }
});

$("#searchContent").keypress(function(e){
	if(e.which == 13) {
	var text=$(this).val();
	var share=$('#contact_sharetype').val();
	
	//alert(id);
	
	
		$.ajax
		({
			type: "POST",
			url: "Contacts/searchcontact?_="+Math.random(),
			data: {'text':text,'share':share},
			cache: false,
			success: function(html){
			//alert(html);
			$('#searchContact').html(html);
			}
		});
	}
	
});
    $('.contact_delete').click(function(){
        var check = window.confirm("Do you want to delete this contact?");
        if(check == true){
            return true;
        }else{
            return false;
        }
    });

$("#contact_sharetype").change(function(){
				$('#searchContact').empty();
	var share = $(this).val();
	var text = $("#searchContent").val();
	
	//alert(share+"  "+text);
	
	
		$.ajax
		({
			type: "POST",
			url: "Contacts/searchcontact?_="+Math.random(),
			data: {'text':text,'share':share},
			cache: false,
			success: function(html){
			//alert(html);
			//$("#search_contact_result").html(html);
							$('#searchContact').html(html);
			}
		});
	
	
	
});


$('#buten').click(function(){
    
    var ph1 = $("#phone1").val();
    var ph2 = $("#phone2").val();
    var ph3 = $("#phone3").val();
    var ph4 = $("#phone4").val();
    var attach = $("#conpic").val();
    var name = $("#contact_name").val();
    var email1 = $("#mail1").val();
    var email2 = $("#mail2").val();
    $('.phone_require').empty();
    $('.name_require').empty();
    $('.fileimage_require').empty();
    $('.mail1_require').empty();
    $('.mail2_require').empty();
    var err = false;   
   
    
//    $('#addform').validate({
//        rules: { 
//            contact_name: {
//                required: true, 
//                minlength: 2
//            }
//        },
//        messages: { //messages to appear on error
//           contact_name: {
//                required:"<br/><div align='center' style='color:red;'>Please Input Name.</div>",
//                minlength:"<br/><div align='center' style='color:red;'>Example: Passakorn Jaidee.</div>"
//            }
//        }
//    });
    //alert(attach);
    if(attach != ''){
        var filetype = checktypefile(attach);    
        
        if(filetype == 'N'){
            $('.fileimage_require').append(' Only file jpg, jpeg, png, gif');  
            err = true;
        }
    }
    if(email1 !=''){
        if (!validEmail(email1)) {
            $(".mail1_require").append('Wrong email format.');
            err = true; 
        }    
    }
      
    if(email2 !=''){
        if (!validEmail(email2)) {
            $(".mail2_require").append('Wrong email format.');
            err = true; 
        }    
    }
    
    if(name == ''){
        $('.name_require').append('Please Input Name.');
        err = true;
    }
    if(ph1=='' && ph2=='' && ph3=='' && ph4==''){
        $('.phone_require').append('Please Insert Phone Number');
        err = true;
    }
    if(err == true){
        return false;
    }
    
});

//$('#buten').click(function(){
//    if(!$('#contact_name').val()) 
//    {
//        $('#err').empty().append('Check Full Name');
//        return false;
//    }
//})

    
//gebo_galery_table.init();
    //* actions for tables, datatables
    //gebo_validation.reg();
//* regular validation
});

gebo_galery_table = {
    init: function() {  
        $('#contacttable').dataTable({
            "sDom": "<'row'<'span6'<'dt_actions'>l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
            "sPaginationType": "bootstrap",
            "aaSorting": [[ 2, "asc" ]],
            "aoColumns": [
            {
                "bSortable": true
            },

            {
                "bSortable": true
            },

            {
                "bSortable": true
            },

            {
                "sType": 'string'
            },

            {
                "bSortable": true
            },

            {
                "bSortable": true
            },

            {
                "bSortable": false
            }]
        });
        $('.dt_actions').html($('.dt_gal_actions').html());
    }
};
function shareall(){
        //alert($('#organi_name').val());
        $('#organi_name').val('ศูนย์การทหารปืนใหญ่');
        $('#organi_id').val('');
        $('#share_type').val('Share All');
    }
function notshare(){
        $('#organi_name').val('Not Share');
        $('#organi_id').val('');
        $('#share_type').val('Not Share');
}

 function checktypefile(file){
            var exts = ['jpg', 'jpeg', 'png', 'gif', 
                        'JPG', 'JPEG', 'PNG', 'GIF'];
            if (file) {
                 // split file name at dot
                 var get_ext = file.split('.');
                 // reverse name to check extension
                 get_ext = get_ext.reverse();
                 var chktype = $.inArray ( get_ext[0].toLowerCase(), exts ) > -1;
                 // check file type is valid as given in 'exts' array
                 if (chktype){
                   return 'Y';
                 } else {
                   return 'N';
                 }
             }   
             
 }
function validEmail(v) {
    var r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
    return (v.match(r) == null) ? false : true;
}
