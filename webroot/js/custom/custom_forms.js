/* [ ---- Gebo Admin Panel - extended form elements ---- ] */
//<!--<script src="js/custom/custom_forms.js"> </script>-->
	$(document).ready(function() {
       
                //p_code.init();
		gebo_datepicker.init();
		gebo_timepicker.init();
                //gebo_uniform.init();
                gebo_galery_table.init();
                //* actions for tables, datatables
                gebo_select_row.init();
                gebo_delete_rows.simple();
                gebo_delete_rows.dt();
		//* words, characters limit for textarea
		//gebo_limiter.init();
		//* autosize for textareas
		//gebo_auto_expand.init();
       
                //* WYSIWG editor
                gebo_wysiwg.init();

                //* colorpicker
		//gebo_colorpicker.init();
		
		$('.open_modal_form').click(function(e) {
			$.colorbox({
				href: '#modal_form',
				inline: true,
				opacity: '0.2',
				fixed: true,
				scrolling: false
			});
			e.preventDefault();
		});
		
		//* password strength checker
		//gebo_pass_check.init();
		
	});
	/*p_code ={
            init: function() {
                $('input[name="articletype"]').click(function(){
                    var typ = $('input[name="articletype"]:checked').val();
                    if(typ == 'notice'){
                        $('#shownotice').show();
                    }else{
                        $('#shownotice').hide();
                    }
                }); 
            }
        };*/
        
        
	//* masked input
	/*gebo_mask_input = {
		init: function() {
			$("#mask_date").inputmask("99/99/9999",{placeholder:"dd/mm/yyyy"});
			$("#mask_phone").inputmask("(999) 999-9999");
			$("#mask_ssn").inputmask("999-99-9999");
			$("#mask_product").inputmask("AA-999-A999");
		}
	};*/
	
	//* bootstrap datepicker
	gebo_datepicker = {
		init: function() {
			$('#dp1').datepicker().on('changeDate', function(ev){
                            $('#dp1').datepicker('hide');
                        });
			//$('#dp2').datepicker();
			$('#dp_expire').datepicker({format: "mm/dd/yyyy"})
                        
			$('#dp_start').datepicker({format: "mm/dd/yyyy"}).on('changeDate', function(ev){
				var dateText = $(this).data('date');
				
				var endDateTextBox = $('#dp_end input');
				if (endDateTextBox.val() != '') {
					var testStartDate = new Date(dateText);
					var testEndDate = new Date(endDateTextBox.val());
					if (testStartDate > testEndDate) {
						endDateTextBox.val(dateText);
					}
				}
				else {
					endDateTextBox.val(dateText);
				};
				$('#dp_end').datepicker('setStartDate', dateText);
				$('#dp_start').datepicker('hide');
			});
			$('#dp_end').datepicker({format: "mm/dd/yyyy"}).on('changeDate', function(ev){
				var dateText = $(this).data('date');
				var startDateTextBox = $('#dp_start input');
				if (startDateTextBox.val() != '') {
					var testStartDate = new Date(startDateTextBox.val());
					var testEndDate = new Date(dateText);
					if (testStartDate > testEndDate) {
						startDateTextBox.val(dateText);
					}
				}
				else {
					startDateTextBox.val(dateText);
				};
				$('#dp_start').datepicker('setEndDate', dateText);
				$('#dp_end').datepicker('hide');
			});
			$('#dp_modal').datepicker();
		}
	};
	
	//* bootstrap timepicker
	gebo_timepicker = {
		init: function() {
                        $('#publishtime').val('00:00');
                        $('#eventtime').val('00:00');
                        $('#extime').val('23:59');
                        $('#publishtime').timepicker({'timeFormat': 'H:i'});
                        $('#eventtime').timepicker({'timeFormat': 'H:i'});
                        $('#extime').timepicker({'timeFormat': 'H:i'});
		}
	};
	
	//* textarea limiter
	/*gebo_limiter = {
		init: function(){
			$("#txtarea_limit_chars").counter({
				goal: 120
			});
			$("#txtarea_limit_words").counter({
				goal: 20,
				type: 'word'
			});
		}
	};*/
	
	//* textarea autosize
	/*gebo_auto_expand = {
		init: function() {
			$('#auto_expand').autosize();
		}
	};*/
    
    //* tag handler
	/*gebo_tag_handler = {
		init: function() {
			$("#array_tag_handler").tagHandler({
				assignedTags: [ 'C', 'Perl', 'PHP' ],
				availableTags: [ 'C', 'C++', 'C#', 'Java', 'Perl', 'PHP', 'Python' ],
				autocomplete: true
			});
			$("#max_tags_tag_handler").tagHandler({
				assignedTags: [ 'Perl' ],
				availableTags: [ 'C', 'C++', 'C#', 'Java', 'Perl', 'PHP', 'Python' ],
				autocomplete: true,
				maxTags:5
			});
		}
	};*/

	//* spinners
	/*gebo_spinners = {
		init: function() {
			$("#sp_basic").spinner();
			$("#sp_dec").spinner({
				decimals: 2,
				stepping: 0.25
			});
			$("#sp_currency").spinner({
				currency: '$',
				max: 20,
				min: 2
			});
			$("#sp_list").spinner();
			$("#sp_users").spinner({
				format: ' <a href="%(url)">%(title)</a>',
				items: [
                    {url: "mailto:laurat@example3.com", title: "Laura Taggart"},
                    {url: "mailto:charlesb@example3.com", title: "Charles Bledsoe"},
                    {url: "mailto:johnd@example1.com", title: "John Doe"},
                    {url: "mailto:kmiller@example1.com", title: "Kate Miller"},
                    {url: "mailto:jamesv@example2.com", title: "James Vandenberg"},
                    {url: "mailto:wnedyp@example1.com", title: "Wendy Proto"},
                    {url: "mailto:ericc@example4.com", title: "Eric Cantrell"},
                    {url: "mailto:yveso@example2.com", title: "Yves Ouellet"}
                ]
			});
		}
	};*/
	//* select all rows
	gebo_select_row = {
		init: function() {
			$('.select_rows').click(function () {
				var tableid = $(this).data('tableid');                                
                $('#'+tableid).find('input[name=row_sel]').attr('checked', this.checked);
			});
		}
	};
	
	//* delete rows
	gebo_delete_rows = {
		simple: function() {
			$(".delete_rows_simple").on('click',function (e) {
				e.preventDefault();
				var tableid = $(this).data('tableid');
                if($('input[name=row_sel]:checked', '#'+tableid).length) {
                    $.colorbox({
                        initialHeight: '0',
                        initialWidth: '0',
                        href: "#confirm_dialog",
                        inline: true,
                        opacity: '0.3',
                        onComplete: function(){
                            $('.confirm_yes').click(function(e){
                                e.preventDefault();
                                $('input[name=row_sel]:checked', '#'+tableid).closest('tr').fadeTo(300, 0, function () { 
                                    $(this).remove();
                                    $('.select_rows','#'+tableid).attr('checked',false);
                                });
                                $.colorbox.close();
                            });
                            $('.confirm_no').click(function(e){
                                e.preventDefault();
                                $.colorbox.close(); 
                            });
                        }
                    });
                }
			});
		},
        dt: function() {
			$(".delete_rows_dt").on('click',function (e) {
				e.preventDefault();
				var tableid = $(this).data('tableid'),
                    oTable = $('#'+tableid).dataTable();
                   
                    var all = $('input[name=row_sel]:checked', '#'+tableid).length; 
                    var mycheckbox=$('input[name=mycheckbox]:checkbox:checked');
                if($('input[name=row_sel]:checked', '#'+tableid).length) {
                    $.colorbox({
                        initialHeight: '0',
                        initialWidth: '0',
                        href: "#confirm_dialog",
                        inline: true,
                        opacity: '0.3',
                        onComplete: function(){
                            $('.confirm_yes').click(function(e){
                                e.preventDefault();

                                    var txt = '';
                                    var i = 1;
                                    var all = $('input[name="row_sel"]:checked').length;
                                    $('input[name="row_sel"]:checked').each(function(){
                                        var p = $(this).val();
                                         if(i == all){
                                             txt += p;

                                         }else{
                                             txt += p+', ';
                                         }    

                                        //var txt = $('#chkcode'+p).val();

                                         i++;

                                    });
                                    alert(txt);
                               
                               /* $('input[name=row_sel]:checked', oTable.fnGetNodes()).closest('tr').fadeTo(300, 0, function () {
                                    $(this).remove();
									oTable.fnDeleteRow( this );
                                    $('.select_rows','#'+tableid).attr('checked',false);
                                });*/
                                $.colorbox.close();
                            });
                            $('.confirm_no').click(function(e){
                                e.preventDefault();
                                $.colorbox.close(); 
                            });
                        }
                    });
                }    
			});
		}
	};
        
    //* gallery table view
    gebo_galery_table = {
        init: function() {
           $('#dt_gal').dataTable({
				"sDom": "<'row'<'span6'<'dt_actions'>l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
               "sPaginationType": "bootstrap",
                "aaSorting": [[ 2, "asc" ]],
				"aoColumns": [
					{ "bSortable": false },
					{ "bSortable": false },
					{ "bSortable": false },
					{ "bSortable": true },
					{ "bSortable": true },
					{ "bSortable": true }
				]
			});
	    $('#contacttable').dataTable({
				"sDom": "<'row'<'span6'<'dt_actions'>l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
               "sPaginationType": "bootstrap",
                "aaSorting": [[ 2, "asc" ]],
				"aoColumns": [
					{ "bSortable": true },
					{ "bSortable": true },
					{ "bSortable": true },
					{ "sType": 'string' },
					{ "bSortable": true },
					{ "bSortable": true },
                    { "bSortable": true }
				]
			});
           $('.dt_actions').html($('.dt_gal_actions').html());
        }
    };
    
    
    //* uniform
    gebo_uniform = {
		init: function() {
            $(".uni_style").uniform();
        }
    };
	
	//* progressbars
	/*gebo_progressbars = {
		init: function(){
			var iEnd1 = new Date().setTime(new Date().getTime() + 25 * 1000); // now plus 25 secs
			$('#progress1').anim_progressbar({
				finish: iEnd1,
				callback: function() {
					$.sticky("Progressbar no 1 callback", {autoclose : false, position: "top-right", type: "st-info" });
				}
			});
			var iNow = new Date().setTime(new Date().getTime() + 2 * 1000); // now plus 2 secs
			var iEnd2 = new Date().setTime(new Date().getTime() + 10 * 1000); // now plus 10 secs
			$('#progress2').anim_progressbar({
				start: iNow,
				finish: iEnd2,
				interval: 100,
				callback: function() {
					$.sticky("Progressbar no 2 callback", {autoclose : false, position: "top-right", type: "st-success" });
				}
			});
			var iEnd3 = new Date().setTime(new Date().getTime() + 15 * 1000); // now plus 15 secs
			$('#progress3').anim_progressbar({
				interval: 1000,
				finish: iEnd3,
				callback: function() {
					$.sticky("Progressbar no 3 callback", {autoclose : false, position: "top-right", type: "st-error" });
				}
			});
		}
	};*/

	//* sliders
	/*gebo_sliders = {
		init: function(){
			//* default slider
			$( ".ui_slider1" ).slider({
				value:100,
				min: 0,
				max: 500,
				step: 50,
				slide: function( event, ui ) {
					$( ".ui_slider1_val" ).text( "$" + ui.value );
					$( "#ui_slider_default_val" ).val( "$" + ui.value );
				}
			});
			$( ".ui_slider1_val" ).text( "$" + $( ".ui_slider1" ).slider( "value" ) );
			$( "#ui_slider_default_val" ).val( "$" + $( ".ui_slider1" ).slider( "value" ) );

			//* range slider
			$( ".ui_slider2" ).slider({
				range: true,
				min: 0,
				max: 500,
				values: [ 75, 300 ],
				slide: function( event, ui ) {
					$( ".ui_slider2_val" ).text( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
					$( "#ui_slider_min_val" ).val( "$" + ui.values[ 0 ] );
					$( "#ui_slider_max_val" ).val( "$" + ui.values[ 1 ] );
				}
			});
			$( ".ui_slider2_val" ).text( "$" + $( ".ui_slider2" ).slider( "values", 0 ) + " - $" + $( ".ui_slider2" ).slider( "values", 1 ) );
			$( "#ui_slider_min_val" ).val( "$" + $( ".ui_slider2" ).slider( "values", 0 ) );
			$( "#ui_slider_max_val" ).val( "$" + $( ".ui_slider2" ).slider( "values", 1 ) );
			
			//* slider with select
			var select = $( "#ui_slider3_sel" );
			var slider = $( "<div id='ui_slider3'></div>" ).insertAfter( select ).slider({
				min: 1,
				max: 6,
				range: "min",
				value: select[ 0 ].selectedIndex + 1,
				slide: function( event, ui ) {
					select[ 0 ].selectedIndex = ui.value - 1;
				}
			});
			$( "#ui_slider3_sel" ).change(function() {
				slider.slider( "value", this.selectedIndex + 1 );
			});
		}
	};*/
	
	//* multiselect
	/*gebo_multiselect = {
		init: function(){
			
			$('.multiselect').multiSelect({
				selectableHeader	: '<h4>Selectable Items</h4>',
				selectedHeader		: '<h4>Selected Items</h4>'
			});
			$('#ms-optgroup .ms-selectable, #ms-outsideCountries .ms-selectable').find('li.ms-elem-selectable').hide();
			$('.ms-optgroup-label').click(function(){
				if ($(this).hasClass('ms-collapse')){
					$(this).nextAll('li').hide();
					$(this).removeClass('ms-collapse'); 
				} else {
					$(this).nextAll('li:not(.ms-selected)').show();
					$(this).addClass('ms-collapse');
				}
			});
		  
			$('#searchable-form').multiSelect({
				selectableHeader : '<input type="text" id="multi_search" autocomplete="off" placeholder="search" />',
				selectedHeader	 : '<a href="javascript:void(0)" id="sForm_deselect" class="btn">Deselect All</a>'
			});
		
			$('input#multi_search').quicksearch('#ms-searchable-form .ms-selectable li');
			$('#searchable-form').multiSelect();
			
			$('#select_all').on('click', function(){
				$('.multiselect').multiSelect('select_all');
				return false;
			});
			
			$('#deselect_all').on('click', function(){
				$('.multiselect').multiSelect('deselect_all');
				return false;
			});
			
			$('#sForm_deselect').on('click', function(){
				$('#searchable-form').multiSelect('deselect_all');
				return false;
			});

		}
	};
	
	//* enhanced select elements
	gebo_chosen = {
		init: function(){
			$(".chzn_a").chosen({
				allow_single_deselect: true
			});
			$(".chzn_b").chosen();
		}
	};*/
    
    //* TinyMce
	gebo_wysiwg = {
		init: function(){
			// File Browser
            function openKCFinder(field_name, url, type, win) {
                alert("Field_Name: " + field_name + "nURL: " + url + "nType: " + type + "nWin: " + win); // debug/testing
                tinyMCE.activeEditor.windowManager.open({
                    file: '/file-manager/browse.php?opener=tinymce&type=' + type,
                    title: 'KCFinder',
                    width: 700,
                    height: 500,
                    resizable: "yes",
                    inline: true,
                    close_previous: "no",
                    popup_css: false
                }, {
                    window: win,
                    input: field_name
                });
                return false;
            };
            var FontSize = '12pt',
                ac;
            $('textarea#wysiwg_full').tinymce({
                // Location of TinyMCE script
                script_url 							: 'lib/tiny_mce/tiny_mce.js',
                // General options
                theme 								: "advanced",
                plugins 							: "autoresize,style,table,advhr,advimage,advlink,emotions,inlinepopups,preview,media,contextmenu,paste,fullscreen,noneditable,xhtmlxtras,template,advlist",
                // Theme options
                theme_advanced_buttons1 			: "undo,redo,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,fontselect,fontsizeselect",
                theme_advanced_buttons2 			: "forecolor,backcolor,|,cut,copy,paste,pastetext,|,bullist,numlist,link,image,media,|,code,preview,fullscreen",
                theme_advanced_buttons3 			: "",
                theme_advanced_toolbar_location 	: "top",
                theme_advanced_toolbar_align 		: "left",
                theme_advanced_statusbar_location 	: "bottom",
                theme_advanced_font_sizes : "8=8pt,10=10pt,12=12pt,14=14pt,16=16pt,18=18pt,24=24pt,28=28pt,32=32pt",
                theme_advanced_resizing 			: false,
                //font_size_style_values 				: "10pt,12pt,14pt,16pt,18pt,24pt,36pt",
                init_instance_callback				: function(){
                        
                        ac = tinyMCE.activeEditor;
                        ac.getBody().style.fontSize = '12pt';

                        ac.execCommand("fontName", false, "Times New Roman");
                        ac.execCommand("fontSize", false, "12pt");
                    function resizeWidth() {
                        document.getElementById(tinyMCE.activeEditor.id+'_tbl').style.width='100%';
                    }
                    resizeWidth();
                    $(window).resize(function() {
                        resizeWidth();
                    })
                },
                // file browser
                file_browser_callback: function openKCFinder(field_name, url, type, win) {
                    tinyMCE.activeEditor.windowManager.open({
                        file: 'file-manager/browse.php?opener=tinymce&type=' + type + '&dir=image/themeforest_assets',
                        title: 'KCFinder',
                        width: 700,
                        height: 500,
                        resizable: "yes",
                        inline: true,
                        close_previous: "no",
                        popup_css: false
                    }, {
                        window: win,
                        input: field_name
                    });
                    return false;
                }
            });
            /********************************************************************/
            $('textarea.wysiwg_mini').tinymce({
                // Location of TinyMCE script
                script_url 							: 'lib/tiny_mce/tiny_mce.js',
                // General options
                theme 								: "advanced",
                plugins 							: "autoresize,style,table,advhr,advimage,advlink,emotions,inlinepopups,preview,media,contextmenu,paste,fullscreen,noneditable,xhtmlxtras,template,advlist",
                // Theme options
                theme_advanced_buttons1 			: "fontselect,fontsizeselect,|,bold,italic,underline,forecolor,backcolor,|,bullist,numlist,justifyleft,justifycenter,justifyright,justifyfull,|,link,image,media",
                theme_advanced_buttons2 			: "",
                theme_advanced_buttons3 			: "",
                theme_advanced_toolbar_location 	: "top",
                theme_advanced_toolbar_align 		: "left",
                theme_advanced_statusbar_location 	: "bottom",
                theme_advanced_font_sizes : "8=8pt,10=10pt,12=12pt,14=14pt,16=16pt,18=18pt,24=24pt,28=28pt,32=32pt",
                theme_advanced_resizing 			: false,
                //font_size_style_values 				: "10pt,12pt,14pt,16pt,18pt,24pt,36pt",
                init_instance_callback	 : function(){
                    
                        ac = tinyMCE.activeEditor;
                        ac.getBody().style.fontSize = '12pt';
                        
                    ac.execCommand("fontName", false, "Times New Roman");
                    ac.execCommand("fontSize", false, "12pt");
                    function resizeWidth() {
                        document.getElementById(tinyMCE.activeEditor.id+'_tbl').style.width='100%';
                    }
                    resizeWidth();
                    $(window).resize(function() {
                        resizeWidth();
                    })
                },
                // file browser
                file_browser_callback: function openKCFinder(field_name, url, type, win) {
                    tinyMCE.activeEditor.windowManager.open({
                        file: 'file-manager/browse.php?opener=tinymce&type=' + type + '&dir=image/themeforest_assets',
                        title: 'KCFinder',
                        width: 700,
                        height: 500,
                        resizable: "yes",
                        inline: true,
                        close_previous: "no",
                        popup_css: false
                    }, {
                        window: win,
                        input: field_name
                    });
                    return false;
                }
            });
            /********************************************************************/
            
         
		}
	};
    
	//* drag&drop multi-upload
    /*gebo_multiupload = {
        init: function() {
            $("#multi_upload").pluploadQueue({
                // General settings
                runtimes : 'html5,flash,silverlight',
                url : 'lib/plupload/examples/upload.php',
                max_file_size : '10mb',
                chunk_size : '1mb',
                unique_names : true,
                browse_button : 'pickfiles',
        
                // Specify what files to browse for
                filters : [
                    {title : "Image files", extensions : "jpg,gif,png"},
                    {title : "Zip files", extensions : "zip"}
                ],
        
                // Flash settings
                flash_swf_url : 'lib/plupload/js/plupload.flash.swf',
        
                // Silverlight settings
                silverlight_xap_url : 'lib/plupload/js/plupload.silverlight.xap'
            });
        }
    };*/
	
	//* colorpicker
	/*gebo_colorpicker = {
		init: function(){
			$('#cp1').colorpicker({
				format: 'hex'
			});
			$('#cp2').colorpicker();
			$('#cp3').colorpicker();
			
			$('#cp_modal').colorpicker();
		}
	};*/
	
	//* password strength checker
	gebo_pass_check = {
		init: function() {
			$("#pass_check").complexify({
					minimumChars: '6',
					strengthScaleFactor: '0.8'
				}, function (valid, complexity) {
				if (!valid) {
					$('#pass_progress .bar').css({'width':complexity + '%'}).parent().removeClass('progress-success').addClass('progress-danger');
				} else {
					$('#pass_progress .bar').css({'width':complexity + '%'}).parent().removeClass('progress-danger').addClass('progress-success');
				}
			});
		}
	};
	
	