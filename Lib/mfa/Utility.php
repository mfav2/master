<?php
class Utility {

	
	public static function cdate(&$inDate, $inFormat='d/m/Y', $outFormat='Y-m-d') {
		if (!empty($inDate)) {
			$dDate = new DateTime();
			$dDate = DateTime::createFromFormat($inFormat, $inDate);
			if ($dDate) {
				return $dDate->format($outFormat);
			} else {
				CakeLog::write('error', "[pakgon.Utility] Cannot convert date $inDate from $inFormat to $outFormat");
			}
		}
		return "";
		
	}

	public static function YearThai($year=0) {
			return $year+543;
	}
        
        public static function YearEng($year=0) {
			return $year-543;
	}

	public static function DateThai($strDate) {
		if (!empty($strDate))
		{
			$strYear = date("Y",strtotime($strDate))+543;
			$strMonth= date("n",strtotime($strDate));
			$strDay= date("j",strtotime($strDate));
			$strHour= date("H",strtotime($strDate));
			$strMinute= date("i",strtotime($strDate));
			$strSeconds= date("s",strtotime($strDate));
			//$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
			$strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
			$strMonthThai=$strMonthCut[$strMonth];
			//return "$strDay $strMonthThai $strYear, $strHour:$strMinute";
			return "$strDay $strMonthThai $strYear";
		}else{
			return "";
		}
		
	}

	public static function uuid() {
        if (function_exists('com_create_guid')) {
            return trim(com_create_guid(), '{}');
        } else {
            mt_srand((double)microtime()*10000);
            $charid = md5(uniqid(rand(), true));
            $hyphen = chr(45);// "-"
            $uuid = substr($charid, 0, 8).$hyphen
                  . substr($charid, 8, 4).$hyphen
                  . substr($charid,12, 4).$hyphen
                  . substr($charid,16, 4).$hyphen
                  . substr($charid,20,12);

            return $uuid;
        }
    }

	public static function checkfilesize($size){
		if ($size < 1024) {
			return $size .' B';
		} elseif ($size < 1048576) {
			return round($size / 1024, 2) .' KB';
		} elseif ($size < 1073741824) {
			return round($size / 1048576, 2) . ' MB';
		} elseif ($size < 1099511627776) {
			return round($size / 1073741824, 2) . ' GB';
		} elseif ($size < 1125899906842624) {
			return round($size / 1099511627776, 2) .' TB';
		} elseif ($size < 1152921504606846976) {
			return round($size / 1125899906842624, 2) .' PB';
		} elseif ($size < 1180591620717411303424) {
			return round($size / 1152921504606846976, 2) .' EB';
		} elseif ($size < 1208925819614629174706176) {
			return round($size / 1180591620717411303424, 2) .' ZB';
		} else {
			return round($size / 1208925819614629174706176, 2) .' YB';
		}
	}
	
	public static function getcontenttype($type){
			$pathArray = explode(".", $type);
			$pathtype = array_pop($pathArray);
			return $pathtype;
	}

	public static function addTime($hours=0, $minutes=0, $seconds=0,$current_date=''){
		
		if(empty($current_date)) $current_date = date("Y-m-d H:i:s");
		$current_time = strtotime($current_date);

		//Add one hour equavelent seconds 60*60
		$increase_time = $current_time + $hours*60*60 + $minutes*60 + $seconds;

		return date("Y-m-d H:i:s",$increase_time);

	}
        
	public static function getParameter($param) {
		$exploded = explode('&', $param);
		
		$conditions = array();
		
		foreach($exploded as $exp) {
			$wheres = explode('=', $exp); //debug($wheres);
			@$conditions[$wheres[0]] = $wheres[1];
		}
					
		//debug($conditions);
		
		// in form data[folder_code_1]=>'', change to folder_code_1=>''

		foreach($conditions as $k=>$v) {
			if(substr($k,0,5)=='data[') {
				preg_match_all('/\[([^\]]+)\]/',$k,$matches);
				unset($conditions[$k]);
				@$conditions[$matches[1][0]] = $v;
			}
		}
		//debug($conditions);
		return $conditions;
		
	}

	public static function valueConvert($action='',$value=''){

		if($action == 'IE'){
			$value = iconv('TIS-620','UTF-8',$value);
		}
		
		return $value;
	}

	public static function DifferDayFromNow($datetimeCompare){
		$timestampCompare = strtotime($datetimeCompare);
		$timestampNow = mktime(0,0,0,date('m'),date('d'),date('Y'));
		$timestampDiffer = abs($timestampCompare - $timestampNow);
		$numberOfDay = $timestampDiffer/(60*60*24);
		return floor($numberOfDay);
	}
 
}
?>
