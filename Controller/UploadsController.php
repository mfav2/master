<?php
class UploadsController extends AppController {
    
    public $components = array('FileStorageComponent', 'Session');
    public $helpers = array('FileStorageHelper');
    
    public function index() {
    }
    
    public function upload() {
        /* Example of upload file */
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $allowedExts = array('doc', 'docx', 'txt', 'pdf', 'jpg', 'jpeg', 'png', 'gif', 
                'xls', 'xlsx', 'ppt', 'pptx', 'bmp', 'zip', 'swf', 'rar', '7z');
            $explodedArray = explode('.', $_FILES['file']['name']);
            $extension = strtolower(end($explodedArray));
            if (is_uploaded_file($_FILES['file']['tmp_name']) 
                    && ($_FILES['file']['size'] < 60000000) 
                    && in_array($extension, $allowedExts)) {
                if ($_FILES['file']['error'] > 0) {
                    echo 'Error : Code ' . $_FILES['file']['error'] . '<br />';
                } else {
                    $path = 'files/test/';
                    if(!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }

                    // Save file
                    $uploadSuccess = move_uploaded_file($_FILES['file']['tmp_name'], $path . $_FILES['file']['name']);
                    
                    if ($uploadSuccess) {
                        // Save file to FileStrorage
                        $key = $this->FileStorageComponent->save($path, $_FILES['file']['name'], $_FILES['file']['type']);
                        
                        if ($key) {
                            /* Save file information into database */
                            /* Use $result to be FK in database */
                            // ... code here ...
                            
                            $this->redirect(array('action' => 'index'));
                        }
                    } else {
                        // return -1 on failure upload
                        echo '-1';
                    }
                }
            } else {
                // return -2 on Invalid file
                echo '-2';
            }
        }
    }
    
    public function download($key = null) {
        /* Example of download file */
        $this->autoRender = false;
        // Use $id to Query database for $key
        // ... code here ...
        if ($key != null) {
            $this->FileStorageComponent->download($key);
        }
    }
    
    public function show($id = null) {
        /* Example of show link to file */
        
        /* If generate url in controller use Component intead of helper
         * $this->FileStorageComponent->urlByKey($key);
         */
    }
    
}
?>