<?php

// can't edit sub cate WTF!!!
if (isset($this->request->query['category_id'])) {
    $category = $this->getCategory->findById($this->request->query['category_id']);
} elseif (isset($this->request->data['Content']['category_id'])) {
    $category = $this->getCategory->findById($this->request->data['Content']['category_id']);
}
       //pr($currentUser['AuthUserAudit']['UserProfile']'[language']);
 $description = $this->ProfileHelper->CheckLanguage($category['CategoryDescription'], $currentUser['AuthUserAudit']['UserProfile']['language']);   

$breadCrumbs = array(
//    array(
//        'label' => $this->name,
//        'link' => array('controller' => $this->name, 'action' => '', 'query' => '', 'param' => '')
//    ),
    array(
        'label' => $description['description'],
        'link' => array('controller' => $this->name, 'action' => 'viewall', 'query' => 'category_id='.$category['getCategory']['id'], 'param' => $this->Session->read('pr'))
    ),
    array(
        'label' => ucfirst($this->view),
        'link' => '',
    ),
//    array(
//        'label' => ($content['Content']['title'] == null) ? $this->name : $content['Content']['title'],
//    ),
);
$this->set('breadCrumbs', $breadCrumbs);
$this->set('modelClass', $this->modelClass);

// FOR MULTI-SELECT TYPE 
//$subscribles = $this->Subscrible->find('all', array(
//            'Conditions' => array(
//                'Content.deleted != \'Y\''
//            ),
//            'order' => 'Subscrible.sort_order ASC',
//            'recursive' => 2
//                )
//        );
//foreach ($subscribles as $i => $category) {
//    $content_type[$category['Category']['id']] = $category['Category']['CategoryDescription']['0']['description'];
//}
//

$viewDefs[$this->name] = array(
    'templateMeta' => array(
        'action' => $this->view,
        'widths' => array(
            array('label' => '10', 'field' => '30'),
            array('label' => '10', 'field' => '30'),
        ),
    ),
    'panels' => array(
        //array('label' => ucfirst($this->view . ' ' . $this->name),
        array('label' => ucfirst($this->view),    
            'data' => array(
                array(
//                    array(
//                        'name' => 'published_date',
//                        'type' => 'calendar',
//                        'div' => 'id="dp1" class="input-append date"',
//                        'label' => __('Publish Date'),
//                        'attr' => array('div' => false, 'class' => 'span10 required', 'label' => false, 'readonly' => 'readonly', 'type' => 'text'),
//                        'icon' => 'splashy-calendar_day_down',
//                        'help' => '',
//                    ),

//                    array(
//                        'name' => 'published_time',
//                        'type' => 'text',
//                        'span' => '12',
//                        'label' => __('Publish Time'),
//                        'attr' => array('div'=>false, 'id'=>"publishtime", 'class' => 'span3', 'label'=>false), 
//                        'help' => '',
//                    ),
                    array(
                        'name' => 'event_date',
                        'type' => 'calendar',
                        'div' => 'id="dp_start" class="input-append date"',
                        'attr' => array('div' => false, 'class' => 'span10 required', 'label' => false, 'readonly' => 'readonly', 'type' => 'text'),
                        'label' => __('Event Date'),
                        'icon' => 'splashy-calendar_day',
                        'help' => '',
                    ),
//                    array(
//                        'name' => 'event_time',
//                        'type' => 'text',
//                        'span' => '12',
//                        'label' => __('Event Time'),
//                        'attr' => array('div'=>false, 'id'=>"eventtime", 'class' => 'span3', 'label'=>false), 
//                        'help' => '',
//                    ),
                    array(
                        'name' => 'expiry_date',
                        'type' => 'calendar',
                        'div' => 'id="dp_end" class="input-append date"',
                        'label' => __('Expiry Date'),
                        'attr' => array('div' => false, 'class' => 'span10 required', 'label' => false, 'readonly' => 'readonly', 'type' => 'text'),
                        'icon' => 'splashy-calendar_day_up',
                        'help' => '',
                    ),
//                    array(
//                        'name' => 'expiry_time',
//                        'type' => 'text',
//                        'span' => '12',
//                        'label' => __('Expiration Time'),
//                        'attr' => array('div'=>false, 'id'=>"extime", 'class' => 'span3', 'label'=>false), 
//                        'help' => '',
//                    ),
                    array(
                        'name' => 'title',
                        'type' => 'text',
                        'label' => __('Subject'),
                        'attr' => array('div' => false, 'class' => 'span8 required', 'label' => false, 'maxlength'=>1024),
                        'help' => '',
                    ),
                    array(
                        'name' => 'gist',
                        'type' => 'textarea',
                        'label' => __('Description'),
                        'attr' => array('div' => false, 'class' => 'span8 required', 'label' => false, 'style' => 'height:70px;','maxlength' => Configure::read('Config.MaxLength')),
                        'help' => '',
                    ),
                    array(
                        'name' => 'description',
                        'type' => 'textarea',
                        'label' => __('Detail'),
                        'attr' => array('div' => false, 'class' => 'span8 required wysiwg_mini', 'label' => false, 'style' => 'height:100px;'),
                        'help' => '',
                    ),
                    array(
                        'name' => 'attachment1',
                        'type' => 'file',
                        'label' => __('Attachment 1'),
                        'attr' => array('type' => 'file', 'label' => false),
                        'help' => __('Max File Size : ').$size,
                    ),
                    array(
                        'name' => 'attachment2',
                        'type' => 'file',
                        'label' => __('Attachment 2'),
                        'attr' => array('type' => 'file', 'label' => false),
                        'help' => __('Max File Size : ').$size,
                    ),
                    array(
                        'name' => 'attachment3',
                        'type' => 'file',
                        'label' => __('Attachment 3'),
                        'attr' => array('type' => 'file', 'label' => false),
                        'help' => __('Max File Size : ').$size,
                    ),
//                    array(
//                        'name' => 'array_content_tags',
//                        'type' => 'ul',
//                        'label' => 'Tags',
//                        'attr' => 'class="span8" style="background: #FFF; margin: 0px 0px 0px 0px"',
//                        'help' => '',
//                    ),
                    array(
                        'name' => 'is_notification',
                        'type' => 'checkbox',
                        'label' => __('Check to alert readers'),
                        'attr' => array('style' => 'margin:0 10px;', 'label'=>false),
                        'help' => '',
                    ),
                ),
            ),
        )
    ),
);
if ($category['getCategory']['is_comment'] == 'S') {
    $viewDefs[$this->name]['panels'][0]['data'][0] = array_merge($viewDefs[$this->name]['panels'][0]['data'][0], array(
        array(
            'name' => 'is_comment',
            'type' => 'checkbox',
            'select' => TRUE,
            'label' => __('Allow Comment'),
            'attr' => array('style' => 'margin:0 10px;'),
            'help' => '',
        ),)
    );
}

$viewDefs[$this->name]['panels'][0]['data'][0] = array_merge($viewDefs[$this->name]['panels'][0]['data'][0], array(
    array(
        'name' => 'notification_type',
        'type' => 'radio',
        'select' => false,
        'label' => __('Readers'),
        'option' => array('A' => __('Everyone'), 'D' => __('Department'),'C' => __('Custom')),
        'attr' => array(
            'legend' => false,
            'default' => 'A',
            'style' => 'margin: 10px 10px;',
        ),
        'help' => '',
    ),
    array(
        'name' => 'array_content_users',
        'type' => 'ul',
        'label' => '',
        'attr' => 'class="span8" style="background: #FFF; margin: 0px 0px 0px 0px"',
        'help' => '',
    )
        )
);

$key = null;
$options = null;
$sub_category = null;
//$childs = $this->getCategory->children($category['getCategory']['id']);
$childs = $this->getCategory->find('all', array(
    'conditions' => array(
        'getCategory.parent_id' => $category['getCategory']['id'],
        'getCategory.deleted' => 'N'
    ) 
));

foreach ($childs as $i => $child) {
    $children = $this->ProfileHelper->CheckLanguage($child['CategoryDescription'], $currentUser['AuthUserAudit']['UserProfile']['language']);   
    $options[$child['getCategory']['id']] = $children['description'];
    if (!$key) {
        $key = $child['getCategory']['id'];
    }
}

if ($options != null) {
    // if it has sub_category or child will build this sub_category option
    // search "array_unshift("); for look next command if options != null
    $sub_category = array(
        'name' => 'sub_category_id',
        'type' => 'select',
        'select' => false,
        'label' => __('Sub Category'),
        'option' => $options,
        'attr' => array('legend' => false, 'default' => $key, 'div' => false, 'label' => false, 'style' => 'margin:0px 0px 0px 0px;', 'empty' => false),
        'help' => '',
    );
}
if ($sub_category != null) {
    array_unshift($viewDefs[$this->name]['panels'][0]['data'][0], $sub_category);
}

if ($this->request->query['pa'] == 'add') {
    $this->set('buttons', array(
        array('name' => __('Add'),
            'attr' => array('id' => 'submitForm', 'class' => 'btn', 'type'=> 'button'),
        ),
       
    ));
    $this->set('action', '/add/');
} elseif ($this->request->query['pa'] == 'edit') {
    $this->set('buttons', array(
        array('name' => __('Update'),
            'attr' => array('id' => 'submitForm', 'class' => 'btn'),
        ),
       
    ));
    $this->set('action', '/edit/');
} else {
    $this->redirect($this->PortalHelper->makeUrl($this->name, ''));
}
$this->set('is_comment', $category['getCategory']['is_comment']);
$this->set('viewDefs', $viewDefs);
//$this->set("availableTag", $this->ContentTag->find('all', array('conditions' => array('ContentTag.tag_name != ' . "''"),'fields' => array('DISTINCT tag_name'),)));