<?php
$breadCrumbs = array(
    array(
            'label' => 'Dashboard', 
            'link'  => array('controller'=>'tests','action'=>'lists','query'=>'id=3','param'=>'')
    )

);
//'type' => 'address','link','text'
    $viewDefs[$this->name]['AddView'] = array(
                'templateMeta' => array(
                                            'action' => '',
                'form' => array('buttons'=>array('Add', 'Reset')),
                'widths' => array(
                                array('label' => '10', 'field' => '30'),
                                array('label' => '10', 'field' => '30'),
                                ),
               ),
                'panels' => array(                       
                        array('label' => __('Add New Article'),
                                  'data'  => array(
                                        array(
                                                array (
                                                    'name' => 'article_type',
                                                    'type' => 'radio',
                                                    'label' => __('Article Type'),
                                                    'option' => array('News Alert'),
                                                    'attr' => array('legend' => false, 'label'=>false, 'checked'=>'checked', 'style'=>'margin:0 10px;', 'name'=>'articletype'),
                                                    'help' => '',
                                                ),			
                                                array (
                                                    'name' => 'date_event',
                                                    'type' => 'button',
                                                    'div' =>  'id="dp_start" class="input-append date"',
                                                    'attr' => array('div'=>false, 'class' => 'span10', 'label'=>false, 'readonly'=>'readonly'), 
                                                    'label' => __('Event Date'),
                                                    'icon' => 'splashy-calendar_day_up',
                                                    'help' => 'xxx',
                                                ),  
                                                array (
                                                     'name' => 'date_expire',
                                                     'type' => 'button',
                                                     'div' => 'id="dp_end" class="input-append date"',
                                                     'label' => __('Expiration Date'),
                                                     'attr' => array('div'=>false, 'class' => 'span10', 'label'=>false, 'readonly'=>'readonly'),
                                                     'icon' => 'splashy-calendar_day_down',
                                                     'help' => 'xxxxx',
                                                ),
                                                array (
                                                     'name' => 'subject',
                                                     'type' => 'text',
                                                     'label' => __('Subject'),
                                                     'attr' => array('div'=>false, 'class' => 'span8', 'label'=>false),
                                                     'help' => 'xxx',
                                                ),
                                                array(
                                                     'name' => 'detail',
                                                     'type' => 'textarea',
                                                     'label' => __('Detail'),
                                                     'attr' => array('div'=>false, 'class' => 'span8', 'label'=>false, 'style'=>'height:100px;'),
                                                     'help' => 'xxx',
                                                ),
                                                array(
                                                     'name' => 'attach1',
                                                     'type' => 'file',
                                                     'label' => __('Attachment 1'),
                                                     'attr' => array('type'=>'file', 'div'=>false, 'class' => 'span8', 'label'=>false),
                                                     'help' => 'xxx',
                                                ),
                                                array(
                                                     'name' => 'attach2',
                                                     'type' => 'file',
                                                     'label' => __('Attachment 2'),
                                                     'attr' => array('type'=>'file', 'div'=>false, 'class' => 'span8', 'label'=>false),
                                                     'help' => 'xxx',
                                                ),
                                                array(
                                                     'name' => 'attach3',
                                                     'type' => 'file',
                                                     'label' => __('Attachment 3'),
                                                     'attr' => array('type'=>'file','div'=>false, 'class' => 'span8', 'label'=>false),
                                                     'help' => 'xxx',
                                                ),
                                                array(
                                                     'name' => 'readers',
                                                     'type' => 'radio',
                                                     'select'=> false, 
                                                     'label' => __('Readers'),
                                                     'option' => array('' => 'All', 'I' => 'In Thai', 'D' => 'My Department', 'C'=>'Custom'),
                                                     'attr' => array('legend' => false, 'default'=>'I', 'div'=>false, 'label'=>false, 'style'=>'margin:0 10px;'),
                                                     'help' => 'xxx',
                                                ),
                                                array(
                                                     'name' => 'alerts',
                                                     'type' => 'checkbox',
                                                     'label' => __('Check to alert readers'),
                                                     'attr' => array('value' => '1' , 'style'=>'margin:0 10px;'),
                                                     'help' => 'xxx',
                                                ),
                                        ),      
                                  ),
                        )
                ),

    );
?>