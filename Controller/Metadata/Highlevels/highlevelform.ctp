<?php

$viewDefs[$this->name]["AddView"] = array(
            'templateMeta' => array(
                'action' => 'highlevelindex',
                'attr' => array('div'=>false),
                'form' => array('buttons' => array(__('Search'))),
            ),
            'panels' => array(
                array('label' => __('High Level Schedule'),
                    'data' => array(
                        array(
                            array(
                                'type' => 'year'
                            ),
                            array(
                                'type' => 'month'
                            ),
                            array(
                                'type' => 'select',
                                'name' => 'heightlevelgroup',
                                'option' => $listhighlevel_group,
                                'attr' => array('empty'=>false,'id' => 'heightlevelselectgroup', 'default' => 1, 'empty' => false,),
                            ),
                            array(
                                'type' => 'button',
                                'name' => __('This Month'),
                                'attr' => array('id' => 'thismonth', 'value' => 'This Month', 'class' => 'btn')
                            ),
                            array(
                                'type' => 'button',
                                'name' => __('High Level Schedule'),
                                'attr' => array('type'=>'button', 'id' => 'highlevelgroup', 'value' => 'High Level', 'class' => 'btn')
                            ),
                        //   echo '<input type="submit" value="Refresh" class="btn">'.'&nbsp';
                        )
                    )
                )
            )
        );
?>
