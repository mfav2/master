<?php 
    $listViewDefs = array(

            'start_date' => array(
                    'width' => '', 
                    'label' => __('Start Date'), 
                    'link' => true,
                    'default' => true), 
            'end_date' => array(
                    'width' => '', 
                    'label' => __('End Date'),
                    'default' => true 
                    ),
            'event' => array(
                    'width' => '60', 
                    'label' => __('Event'),
                    'default' => true),
    );

?>