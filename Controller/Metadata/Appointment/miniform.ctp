<?php
$minutes_select = array('00'=>'00','05'=>'05','10'=>'10','15'=>'15','20'=>'20','25'=>'25','30'=>'30','35'=>'35','40'=>'40','45'=>'45','50'=>'50','55'=>'55');


//'type' => 'address','link','text'
    $viewDefs[$this->name]['AddView'] = array(
                        'templateMeta' => array(
                                                    'action' => '/addappointment',
                        'form' => array(
                                'buttons'=>array(
                                    array(
                                      'name' => __('Save'),
                                      'attr' => array('class'=>'btn','id'=>'submitForm', 'type'=>'submit', 'div'=>false, 'label'=>false)  
                                    )
                                )
                        ),
                        'widths' => array(
                                        array('label' => '10', 'field' => '30'),
                                        array('label' => '10', 'field' => '30'),
                                        ),
                       ),
                        'heads' => 'Appointment',
                        'panels' => array(                       
                        array(
                                  'data'  => array(
                                        array(
                                               array (
                                                    'name' => 'name',
                                                    'type' => 'text',
                                                    'span' => '7',
                                                    //'div' =>  'id="dp_start" class="input-append date"',
                                                    'attr' => array( 'div'=>false, /*'class' => 'span12',*/ 'label'=>false, 'style'=>'width:160%;', 'maxlength'=>Configure::read('Config.MaxLength')), 
                                                    'label' => __('Title'),
                                                    'error' => '',
                                                   
                                                    /***************** start Date Time ************/
                                                    'namestart' => 'start_date',
                                                    'spanstart'=> '6',
                                                    'typestart' => 'calendar',
                                                    'divstart' => 'id="start_date" class="input-append date" style="float:left; margin: 5px 0 0 0;"',
                                                    'attrstart' => array('id'=>'fdate' ,'div' => false,'style'=>'width:75px;', 'label' => false, 'readonly' => 'readonly', 'type' => 'text'),
                                                    'labelstart' => __('From'),
                                                    'iconstart' => 'splashy-calendar_day',
                                                    'errorstart' => 'error_startdate',
                                                    'help' => '',
                                                    
                                                    'namestarthour' => 'fhour',
                                                    'selectstarthourattr' => array('default'=>'', 'empty'=>false, 'div'=>false, 'label', false, 'style'=>'width:100px;'),
                                                     
                                                   
                                                   
                                                    'namestartmin' => 'fmin',
                                                    'selectstartmin'=> $minutes_select,
                                                    'selectstartminattr' => array('default'=>'','class'=>'span12',  'empty'=>false, 'div'=>false, 'label', false),
//                                                    'namestarttime' => 'ftime',
//                                                    'typestarttime' => 'text',
//                                                    'spanstarttime' => '3',
//                                                    'attrstarttime' => array('div'=>false, 'id'=>"ftime", 'label'=>false, 'style'=>'margin:5px 0 0 6px; width:80px;'), 
//                                                    'labelstarttime' => __('Time'),
//                                                    'errorstarttime' => '',
                                                   
//                                                   'namestarttime' => 'ftime',
//                                                   'typestarttime' => 'select',
//                                                   'spanstarttime' => '3',
//                                                   'attrstarttime' => array('div'=>false, 'id'=>"ftime", 'label'=>false, 'style'=>'margin:5px 0 0 6px; width:80px;'), 
//                                                   'labelstarttime' => __('Time'),
//                                                   'errorstarttime' => '',
//                                                   'selectstartmin'=> $minutes_select,
//                                                   'selectstartattr' => array('default'=>'','class'=>'span12',  'empty'=>false, 'div'=>false, 'label', false),

                                                   
                                                   
                                                    /**************** end Date Time ***************/
                                                    'nameend' => 'end_date',
                                                    'spanend'=> '6',   
                                                    'typeend' => 'calendar',
                                                    'divend' => 'id="end_date" class="input-append date"  style="float:left; margin: 5px 0 0 0;"',
                                                    'labelend' => __('To'),
                                                    'attrend' => array('id'=>'tdate','div' => false, 'label' => false,'style'=>'width:75px;', 'readonly' => 'readonly', 'type' => 'text'),
                                                    'iconend' => 'splashy-calendar_day_up',
                                                    'errorend' => 'error_enddate',
                                                    
                                                   
                                                    'nameendhour' => 'thour',
                                                    'selectendhourattr' => array('default'=>'', 'empty'=>false, 'div'=>false, 'label', false, 'style'=>'width:100px;'),
                                                     
                                                   
                                                   
                                                    'nameendmin' => 'tmin',
                                                    'selectendmin'=> $minutes_select,
                                                    'selectendminattr' => array('default'=>'','class'=>'span12',  'empty'=>false, 'div'=>false, 'label', false), 
                                                     
//                                                    'nameendtime' => 'ttime',
//                                                    'typeendtime' => 'select',
//                                                    'spanendtime' => '3',
//                                                    'attrendtime' => array('div'=>false, 'id'=>"ttime", 'label'=>false, 'style'=>'margin:5px 0 0 6px; width:80px;'), 
//                                                    'labelendtime' => __('Time'),
//                                                    'errorendtime' => '',
                                                    
                                                    
                                                ),
                                                array(
                                                     'name' => 'location_type_id',
                                                     'namelist' => 'LocationType',
                                                     'span' => '5',
                                                     'type' => 'radio',
                                                     'label' => __('Location'),
                                                     //'option' => $location,
                                                     'option' =>array('1'=>'<img src="img/icon/B_trans1.gif">&nbsp;'.__("Bangkok"),'2'=>'<img src="img/icon/bus_trans1.gif">&nbsp;'.__("Domestic"),'3'=>'<img src="img/icon/airplane_trans1.gif">&nbsp;'.__("Overseas")), 
                                                     'attr' => array( 'legend' => false, 'default'=>'1', 'div'=>false, 'label'=>false, 'separator'=>'</li><li>', 'style'=>'margin:0 5px 5px 5px;'),
                                                     'help' => '',
                                                     'error' => '',
                                                ),
                                                array (
                                                     'name' => 'location',
                                                     'span' => '7',
                                                     'type' => 'textarea',
                                                     'label' => __('Place'),
                                                     'attr' => array('div'=>false, 'class' => 'span12', 'label'=>false, 'style'=>'height:70px; width:150%;'),
                                                     'help' => '',
                                                     'error' => 'error_location',
                                                ),
                                                array(
                                                     'name' => 'color_id',
                                                     'namelist' => 'ColorLabel',
                                                     'span' => '5',
                                                     'type' => 'color',
                                                     'label' => __('Label'),
                                                     'option' => $color,
                                                     'attr' => array( 'legend' => false, 'default'=>'1', 'div'=>false, 'label'=>false, 'separator'=>'</li><li>', 'style'=>'margin:0 5px 5px 5px;'),
                                                     'help' => '',
                                                     'error' => '',
                                                ),
                                                array (
                                                     'name' => 'detail',
                                                     'span' => '7',
                                                     'type' => 'textarea',
                                                     'label' => __('Message shown in shared schedule'),
                                                     'attr' => array('div'=>false, 'class' => 'span12', 'label'=>false, 'style'=>'margin:0px; height:100px; width:150%;' ),
                                                     'help' => '',
                                                     'error' => 'error_detail',
                                                ),
                                                array(
                                                     'name' => 'priority_type_id',
                                                     'namelist' => 'PriorityType',
                                                     'span' => '5',
                                                     'type' => 'radio',
                                                     'label' => __('Priority'),
                                                      //'option' => $list,
                                                     'option' =>array('4'=>'<img src="img/icon/red_flag.png" />','3'=>'<img src="img/icon/yellow_flag.png" />','2'=>'<img src="img/icon/green_flag.png" />','1'=>__('None')), 
                                                     'attr' => array('id'=>'priority_id', 'legend' => false, 'default'=>'1', 'div'=>false, 'label'=>false, 'style'=>'margin:0 5px 5px 5px;'),
                                                     //'attr' => array('id'=>'priority_id', 'legend' => false, 'default'=>'1', 'div'=>false, 'label'=>false, 'separator'=>'</li><li>', 'style'=>'margin:0 5px 5px 0;'),
                                                     'help' => '',
                                                     'error' => '',
                                                ),
                                                array(
                                                     'name' => 'private_note',
                                                     'span' => '7',
                                                     'type' => 'textarea',
                                                     'label' => __('Private Note'),
                                                     'attr' => array('div'=>false, 'class' => 'span12', 'label'=>false, 'style'=>'height:50px; width:150%;'),
                                                     'help' => '',
                                                    'error' => '',
                                                ),
                                                array(
                                                     'name' => 'privacy_type_id',
                                                     'namelist' => 'PrivacyType',
                                                     'span' => '5',
                                                     'type' => 'radio',
                                                     'label' => __('Privacy'),
                                                     'option' => $privacy_list,
                                                     'attr' => array('id'=>'privacy_id', 'legend' => false, 'default'=>'1', 'div'=>false, 'label'=>false, 'style'=>'margin:0 5px 5px 5px;'),
                                                     'help' => '',
                                                     'error' => '',
                                                ),
                                                array(
                                                     'name' => 'attach1',
                                                     'span' => '12',
                                                     'type' => 'file',
                                                     'label' => __('Attachment File'),
                                                     'attr' => array('type'=>'file', 'div'=>false, 'class' => 'span12', 'label'=>false), 
                                                     'help' => __('Max File Size : ').$size,
                                                    'error' => 'error_attach',
                                                ),
//                                                array(
//                                                     'name' => 'invite',
//                                                     'span' => '6',
//                                                     'type' => 'invite',
//                                                     'label' => __('Invite Meeting '),
//                                                     'attr' => array('type'=>'hidden', 'div'=>false, 'class' => 'span12', 'label'=>false), 
//                                                     'help' => '',
//                                                     'error' => '',
//                                                ),
                                                 array(
//                                                   
                                                    'name' => 'is_alert',
                                                    'type' => 'remind',
                                                    'span' => '12',
                                                    'label' => __('Remind'),
                                                    'attr' => array('style'=>'margin:0 10px;'),
                                                    'select'=> array(''=>'Portal'),
                                                    'selectlabel' => 'remind',
                                                    'selectattr' => array('default'=>'', 'empty'=>false, 'div'=>false, 'label', false),
                                                    'select2'=> $alert_list,
                                                    'selectlabel2' => 'alert_type_id',
                                                    'selectattr2' => array('empty'=>false, 'div'=>false, 'label', false, 'style'=>'margin:0 0 0 5px;'),
                                                    'help' => '',
                                                    'error' => '',
                                                ),
                                                array(
//                                                   
                                                    'name' => 'is_alert_repeat',
                                                    'type' => 'remind_repeat',
                                                    'span' => '12',
                                                    'label' => __('Remind Repeat'),
                                                    'attr' => array('style'=>'margin:0 10px 0 15px;'),
                                                    'select2'=> $alert_list,
                                                    'selectlabel2' => 'alert_repeat_type_id',
                                                    'selectattr2' => array('empty'=>false, 'div'=>false, 'label', false, 'style'=>'margin:0 0 0 5px;'),
                                                    'help' => '',
                                                    'error' => '',
                                                )
                                                
                                               
                                        ),      
                                  ),
                        )
                )

   );
?>