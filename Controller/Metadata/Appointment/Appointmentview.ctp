<?php

$viewDefs[$this->name]['DetailView'] = array(
    'templateMeta' => array(
         'action' => false,
        'form' => array(
            'buttons' => array('Accepted', 'Tentative', 'Reject')
         ),
        'widths' => array(
            array('label' => '10', 'field' => '30'),
            array('label' => '10', 'field' => '30'),
        ),
    /* 'includes'=> array(
      array('file'=>'modules/Accounts/Account.js'),
      ), */
    ),
    'panel' => array(
        array('label' => __('My Appointment'),
            'data' => array(
                array(
                    array(
                        'name' => 'name',
                        'label' => __('Title'),
                         ),
                     ),
                array(
                    array(
                        'name' => 'fdate',
                        'label' => __('Start Date'),
                         ),
                     ),
                array(
                    array(
                        'name' => 'tdate',
                        'label' => __('End Date'),
                         ),
                     ),
                array(
                    array(
                        'name' => 'invite',
                        'label' => __('Invite'),
                         ),
                     ),
                array(
                    array(
                        'name' => 'location_type_id',
                        'label' => __('Location'),
                         ),
                     ),
                array(
                    array(
                        'name' => 'location',
                        'label' => __('Place'),
                         ),
                     ),
                array(
                    array(
                        'name' => 'detail',
                        'label' => __('Description'), 
                        ),
                    ),
                array(
                    array(
                        'name' => 'attach',
                        'label' => __('Attachment'), 
                        ),
                    ),
                array(
                    array(
                        'name' => 'private_note',
                        'label' => __('Private Note'), 
                    ),
                ),
                array(
                    array(
                        'name' => 'priority',
                        'label' => __('Priority'), 
                    ),
                ),
              ),  
                
            ),
       
        
    ),
    
);
$viewDatas = array(
    'name' => $viewappoint['Appointment']['name'],
    'fdate' => $viewappoint['Appointment']['start_date'],
    'tdate' => $viewappoint['Appointment']['end_date'],
    'invite' => $chkuser,
    'location_type_id' =>$viewappoint['AppointmentLocation']['location_name'],
    'location' =>$viewappoint['Appointment']['location'],
    'detail' =>$viewappoint['Appointment']['detail'],
    'private_note' =>$viewappoint['Appointment']['private_note'],
    'attach' => '',
    'priority'=>$viewappoint['AppointmentPriority']['priority_name'],
);


?>
