<?php
$TabView = array(
            'panel' => array(
                array(
                    'label' => __('Contact'), 'link' => '', 'class' => 'splashy-document_letter_edit', 'active' => true, 'tabname' => 'showcontact',
                    //////////////////
                    'type' => 'list',
                    //////////////////
                    'data' => array(
                        'name' => array('label' => __('Full Name'), 'width' => '',),
                        'company' => array('label' => __('Company'), 'width' => '',),
                        'position' => array('label' => __('Position'), 'width' => '',),
                        'phone' => array('label' => __('Phone'), 'width' => '',),
                        'connection' => array('label' => __('Relation with MFA'), 'width' => '',),
                        'share' => array('label' => __('Shared'), 'width' => '',),
                    ),
                ),
                array(
                    'label' => __('New Contact'), 'link' => '', 'class' => 'splashy-mail_light_down', 'active' => false, 'tabname' => 'addcontact',
                    //////////////////
                    'type' => 'form',
                    /////////////////
                    'templateMeta' => array(
                        'action' => 'addcontact',
                        'form' => array('buttons' => array(__('Save'))),
                    ),
                    'panels1' => array(
                        'data' => array(
                            array(
                                'label' => __('Detail'),
                                /*'select' => true,*/
                                //'option' => array( 'All' => __('Share All'), 'Division' => __('Share Division'),'Department' => __('Share Department'), 'Not Share' => __('Not Share')),
                                'option' => array('Not Share' => __('Not Share')),
                                'attr' => array( 'name' => 'share_type','id' => 'share', 'empty' => false, 'default' => 'Not Share', 'class' => 'span4',),
                                'name' => 'share_type',
                                //////////////////
                                'detail' => array(
                                    array(
                                        'name' => 'title', 
                                        'type' => 'select', 
                                        'option' => array( 'Mr' => __('Mr'), 'Ms' => __('Ms'),'Mrs' => __('Mrs'), 'Etc' => __('Etc')),
                                        'label' => __('Title (Mr, Ms, Mrs, etc)'),
                                        'attr' => array('id' => 'titlename', 'label' => false,'name'=>'title', 'class' => 'span4','empty'=>false),
                                    ),
                                    array(
                                        'name' => 'title2', 'type' => 'text', 'label' => __('Etc Title'),
                                        'attr' => array('id' => 'titlename2','name'=>'title2', 'label' => false, 'class' => 'span4'),
                                    ),
                                    array(
                                        'name' => 'contact_name', 'type' => 'text', 'label' => __('Full Name'),
                                        'attr' => array( 'name' => 'contact_name','id' => 'contact_name', 'label' => false, 'class' => 'span6'),
                                    ),
                                    
                                    array(
                                        'name' => 'job_title', 'type' => 'text', 'label' => __('Position'),
                                        'attr' => array(  'name' => 'job_title','id' => 'job_title', 'label' => false, 'class' => 'span6'),
                                    ),
                                    
                                    array(
                                        'name' => 'department', 'type' => 'text', 'label' => __('Department'),
                                        'attr' => array('name' => 'department','id' => 'department', 'label' => false, 'class' => 'span6'),
                                    ),
                                    array(
                                        'name' => 'division', 'type' => 'text', 'label' => __('Division'),
                                        'attr' => array( 'name' => 'division','id' => 'division', 'label' => false, 'class' => 'span6'),
                                    ),
                                    array(
                                        'name' => 'company', 'type' => 'text', 'label' => __('Company'),
                                        'attr' => array('name' => 'company','id' => 'company', 'label' => false, 'class' => 'span6'),
                                    ),
                                ),
                            ),
                            array(
                                'label' => __('Phone Number'),
                                'detail' => array(
                                    array(
                                        'name' => 'phone1', 'type' => 'text', 'label' => false,
                                        'attr' => array( 'name' => 'phone1','id' => 'phone1', 'label' => false, 'class' => 'uni_style', 'div' => false, 'class' => 'span7'),
                                        'select' => true, 'selectname' => 'phone1_type',
                                        'option' => array('office' => __('Office'), 'officefax' => __('Office Fax'), 'home' => __('Home'), 'mobile' => __('Mobile'), 'pager' => __('Pager'), 'other' => __('Other')),
                                        'selectattr' => array( 'name' => 'phone1_type','id' => 'phone1_type', 'default' => 'office', 'empty' => false, 'class' => 'uni_style', 'div' => false, 'class' => 'span4', 'style' => 'margin:2px'),'default_tel'=>"1",
                                    ),
                                    array(
                                        'name' => 'phone2', 'type' => 'text', 'label' => false,
                                        'attr' => array( 'name' => 'phone2','id' => 'phone2', 'label' => false, 'class' => 'uni_style', 'label' => false, 'div' => false, 'class' => 'span7'),
                                        'select' => true, 'selectname' => 'phone2_type',
                                        'option' => array('office' => __('Office'), 'officefax' => __('Office Fax'), 'home' => __('Home'), 'mobile' => __('Mobile'), 'pager' => __('Pager'), 'other' => __('Other')),
                                        'selectattr' => array( 'name' => 'phone2_type','id' => 'phone2_type', 'default' => 'officefax', 'empty' => false, 'class' => 'uni_style', 'label' => false, 'div' => false, 'class' => 'span4', 'style' => 'margin:2px;'),'default_tel'=>"0",
                                    ),
                                    array(
                                        'name' => 'phone3', 'type' => 'text', 'label' => false,
                                        'attr' => array( 'name' => 'phone3','id' => 'phone3', 'label' => false, 'class' => 'uni_style', 'label' => false, 'div' => false, 'class' => 'span7'),
                                        'select' => true, 'selectname' => 'phone3_type',
                                        'option' => array('office' => __('Office'), 'officefax' => __('Office Fax'), 'home' => __('Home'), 'mobile' => __('Mobile'), 'pager' => __('Pager'), 'other' => __('Other')),
                                        'selectattr' => array( 'name' => 'phone3_type','id' => 'phone3_type', 'default' => 'home', 'empty' => false, 'class' => 'uni_style', 'label' => false, 'div' => false, 'class' => 'span4', 'style' => 'margin:2px;'),'default_tel'=>"0",
                                    ),
                                    array(
                                        'name' => 'phone4', 'type' => 'text', 'label' => false,
                                        'attr' => array('name' => 'phone4','id' => 'phone4', 'label' => false, 'class' => 'uni_style', 'label' => false, 'div' => false, 'class' => 'span7'),
                                        'select' => true, 'selectname' => 'phone4_type',
                                        'option' => array('office' => __('Office'), 'officefax' => __('Office Fax'), 'home' => __('Home'), 'mobile' => __('Mobile'), 'pager' => __('Pager'), 'other' => __('Other')),
                                        'selectattr' => array('name' => 'phone4_type','id' => 'phone4_type', 'default' => 'mobile', 'empty' => false, 'class' => 'uni_style', 'label' => false, 'div' => false, 'class' => 'span4', 'style' => 'margin:2px;'),
										'default_tel'=>"0",
                                    ),
                                ),
                            ),
                            array('label' => __('Address'),
                                'detail' => array(
                                    array(
                                        'name' => 'address1',
                                        'type' => 'textarea',
                                        'label' => false,
                                        'attr' => array('id' => 'address1','name'=>'address1', 'label' => false, 'div' => false, 'class' => 'span12', 'style' => 'margin:2.5px 0px 0px 0px; height:80px;'),
                                        'select' => true,
                                        'selectname' => 'address1_type',
                                        'option' => array('business' => __('Business'), 'home' => __('Home'), 'other' => __('Other')),
                                        'selectattr' => array( 'name' => 'address1_type','id' => 'address1_type', 'default' => 'business', 'empty' => false, 'class' => 'uni_style', 'label' => false, 'div' => false, 'class' => 'span4', 'align' => 'top'),
                                    ),
//                                    array(
//                                        'name' => 'country1',
//                                        'type' => 'select',
//                                        'label' => 'Country',
//                                        'option' => "",
//                                        'attr' => array('id' => 'country1', 'label' => false, 'class' => 'uni_style', 'div' => false, 'class' => 'span6', 'style' => 'margin: 2px;'),
//                                    ),
//                                    array(
//                                        'name' => 'province_state1',
//                                        'type' => 'select',
//                                        'label' => 'Province / State',
//                                        'option' => "",
//                                        'attr' => array('id' => 'province_state1', 'label' => false, 'class' => 'uni_style', 'div' => false, 'class' => 'span6', 'style' => 'margin: 2px;'),
//                                    ),
                                    array(
                                        'name' => 'address2',
                                        'type' => 'textarea',
                                        'label' => false,
                                        'attr' => array('name' => 'address2','id' => 'address2', 'label' => false, 'label' => false, 'div' => false, 'class' => 'span12', 'style' => 'margin:2.5px 0px 0px 0px; height:80px;'),
                                        'select' => true,
                                        'selectname' => 'address2_type',
                                        'option' => array('business' => __('Business'), 'home' => __('Home'), 'other' => __('Other')),
                                        'selectattr' => array('name' => 'address2_type','id' => 'address2_type', 'default' => 'home', 'empty' => false, 'class' => 'uni_style', 'label' => false, 'div' => false, 'class' => 'span4',),
                                    ),
//                                    array(
//                                        'name' => 'country2',
//                                        'type' => 'select',
//                                        'label' => 'Country',
//                                        'option' => '',
//                                        'attr' => array('id' => 'country2', 'label' => false, 'class' => 'uni_style', 'div' => false, 'class' => 'span6', 'style' => 'margin: 2px;'),
//                                    ),
//                                    array(
//                                        'name' => 'province_state2',
//                                        'type' => 'select',
//                                        'label' => 'Province /State',
//                                        'option' => '',
//                                        'attr' => array('id' => 'province_state2', 'label' => false, 'class' => 'uni_style', 'div' => false, 'class' => 'span6', 'style' => 'margin: 2px;'),
//                                    ),
                                ),
                            ),
                        )
                    ),
                    'panels2' => array(
                        'data' => array(
                            array('label' => __('Picture'),
                                'detail' => array(
                                    array(
                                        'name' => 'conpic',
                                        'type' => 'fileimage',
                                        'label' => false,
                                        'attr' => array('id' => 'conpic', 'label' => false, 'class' => 'btn', 'div' => false, 'class' => 'span7'),
                                    ),
                                ),
                            ),
                            array('label' => __('E-mail'),
                                'detail' => array(
                                    array(
                                        'name' => 'email1',
                                        'type' => 'text',
                                        'label' => __('E-Mail 1'),
                                        'attr' => array('name' => 'email1','id' => 'mail1', 'label' => false, 'class' => 'uni_style', 'div' => false, 'class' => 'span5'),
                                    ),
                                    array(
                                        'name' => 'email2',
                                        'type' => 'text',
                                        'label' => __('E-Mail 2'),
                                        'attr' => array('name' => 'email2','id' => 'mail2', 'label' => false, 'class' => 'uni_style', 'div' => false, 'class' => 'span5'),
                                    ),
                                    array(
                                        'name' => 'web_address',
                                        'type' => 'text',
                                        'label' => __('Web Page Address'),
                                        'attr' => array('name' => 'web_address','id' => 'web_address', 'label' => false, 'class' => 'uni_style', 'div' => false, 'class' => 'span5'),
                                    ),
                                ),
                            ),
                            array('label' => __('Relation with MFA'),
                                'detail' => array(
                                    array(
                                        'type' => 'textarea',
                                        'name' => 'con_mfa',
                                        'label' => false,
                                        'attr' => array('name' => 'con_mfa','id' => 'con_mfa', 'label' => false, 'div' => false, 'class' => 'span12', 'style' => 'height:100px;')
                                    ),
                                ),
                            ),
                            array('label' => __('Note'),
                                'detail' => array(
                                    array(
                                        'type' => 'textarea',
                                        'name' => 'note',
                                        'label' => false,
                                        'attr' => array('name' => 'note','id' => 'note', 'label' => false, 'div' => false, 'class' => 'span12', 'style' => 'height:100px;')
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
        );
?>