<?php
//if($old_key_pic==''){$url_old_pic = '../webroot/attachment/contact/no_image.jpg';}
$viewDefs[$this->name]['DetailView'] = array(
    'templateMeta' => array(
        'form' => array('buttons' => array(__('Edit'), __('Cancel'))),
        'widths' => array(
            array('label' => '10', 'field' => '30'),
            array('label' => '10', 'field' => '30'),
        ),
    /* 'includes'=> array(
      array('file'=>'modules/Accounts/Account.js'),
      ), */
    ),
    'panel1' => array(
        array('label' => __('Detail'),
            'data' => array(
                array(
                    array(
                        'name' => 'contact_name',
                        'label' => __('FULL NAME'),
                         ),
                     ),
                array(
                    array(
                        'name' => 'position',
                        'label' => __('Position'),
                         ),
                     ),
                array(
                    array(
                        'name' => 'department',
                        'label' => __('Department'),
                         ),
                     ),
                array(
                    array(
                        'name' => 'division',
                        'label' => __('Division'),
                         ),
                     ),
                array(
                    array(
                        'name' => 'company',
                        'label' => __('Company'),
                         ),
                     ),
                ),
            ),
       
        array('label' => __('Phone Number'),
            'data' => array(
                array(
                    array(
                        'name' => 'Phone 1',
                        'label' => __('Phone 1'),
                    ),
                ),
                array(
                    array(
                        'name' => 'Phone 2',
                        'label' => __('Phone 2'),
                    ),
                ),
                array(
                    array(
                        'name' => 'Phone 3',
                        'label' => __('Phone 3'),
                    ),
                ),
                array(
                    array(
                        'name' => 'Phone 4',
                        'label' => __('Phone 4'),
                    ),
                ),
            ),
        ),
        array('label' => 'Address',
            'data' => array(
                array(
                    array(
                        'name' => 'Address 1',
                        'label' => __('Address 1'),
                    ),
                ),
                array(
                    array(
                        'name' => 'Address 2',
                        'label' => __('Address 2'),
                    ),
                ),
            ),
        ),
    ),
    'panel2' => array(
         array('label' => __('Picture'),
            'data' => array(
                array(
                    array(
                        'name' => 'con_pic',
                        'label' => false,
                    ),
                ),
            ),
        ),
        array('label' => __('E-Mail'),
            'data' => array(
                array(
                    array(
                        'name' => 'email1',
                        'label' => false,
                    ),
                ),
                array(
                    array(
                        'name' => 'email2',
                        'label' => false,
                    ),
                ),
                array(
                    array(
                        'name' => 'web_address',
                        'label' => false,
                    ),
                ),
            ),
        ),
        array('label' => __('Relation with MFA'),
            'data' => array(
                array(
                    array(
                        'name' => 'con_MFA',
                        'label' => false,
                    ),
                ),
            ),
        ),
        array('label' => __('NOTE'),
            'data' => array(
                array(
                    array(
                        'name' => 'note',
                        'label' => false,
                    ),
                ),
            ),
        ),
    ),
);
if(empty($contact['Contact']['con_mfa'])){
    $con_MFA = '';
}else{
    $con_MFA = $contact['Contact']['con_mfa'];
}

$chkpic = '';
        
if(empty($contact['Contact']['attachment'])){
    $chkpic = '../webroot/attachment/contact/no_image.jpg';
    $url = $chkpic;
}else{
    $chkpic = $contact['Contact']['attachment'];
    $url = '/fileProviders/index/'.$chkpic;
}



$viewDatas = array(
    'contact_name' => $contact['Contact']['title'] . '.&nbsp' . $contact['Contact']['contact_name'],
    'position' => $contact['Contact']['job_title'],
    'department' => $contact['Contact']['department'],
    'division' => $contact['Contact']['division'],
    'company' => $contact['Contact']['company'],
    'Phone 1' => $contact['Contact']['phone1_type'] . '&nbsp&nbsp' . $contact['Contact']['phone1'],
    'Phone 2' => $contact['Contact']['phone2_type'] . '&nbsp&nbsp' . $contact['Contact']['phone2'],
    'Phone 3' => $contact['Contact']['phone3_type'] . '&nbsp&nbsp' . $contact['Contact']['phone3'],
    'Phone 4' => $contact['Contact']['phone4_type'] . '&nbsp&nbsp' . $contact['Contact']['phone4'],
    'Address 1' => $contact['Contact']['address1'],
    'Address 2' => $contact['Contact']['address2'],
    'con_pic' =>'<center><img  style="width:120px;"  src ="'.$url.'"></center>',
    'email1' => $contact['Contact']['email1'],
    'email2' => $contact['Contact']['email2'],
    'web_address' => '<a href='.'http://'.$contact['Contact']['web_address'].'>'.$contact['Contact']['web_address'].'</a>'.'&nbsp( WEB ADDRESS )',
    'con_MFA' => $con_MFA,
    'note' => $contact['Contact']['note'],
);
?>
 