<?php
        $listViewDefs = array(
                    'label' => __('Contact'), 'link' => '', 'class' => 'splashy-document_letter_edit', 'active' => true, 'tabname' => 'showcontact',
                    //////////////////
                    'type' => 'list',
                    //////////////////
                    'data' => array(
                        'name' => array('label' => __('Full Name'), 'width' => '',),
                        'company' => array('label' => __('Company'), 'width' => '',),
                        'position' => array('label' => __('Position'), 'width' => '',),
                        'phone' => array('label' => __('Phone'), 'width' => '',),
                        'connection' => array('label' => __('Relation with MFA'), 'width' => '',),
                        'share' => array('label' => __('Shared'), 'width' => '',),
						'manage' =>array('label' => __('Action'), 'width' => '',),
                    ),
                );
$listdata = array();
$i =0;
arsort($mycontact);
$phone = '';
foreach($mycontact as $showcontact){
	if(!empty($showcontact['Contact']['phone1'])){
		$phone = $showcontact['Contact']['phone1'];
	}else{
		if(!empty($showcontact['Contact']['phone2'])){
			$phone = $showcontact['Contact']['phone2'];
		}else if(!empty($showcontact['Contact']['phone3'])){
			$phone = $showcontact['Contact']['phone3'];
		}else{
			$phone = $showcontact['Contact']['phone4'];
		}
	}


	$listdata[$i]['id'] =  $showcontact['Contact']['id'];
	$listdata[$i]['name'] = '<a href="'.$this->PortalHelper->makeUrl('Contacts', 'viewcontact', 'id=' . $showcontact['Contact']['id']).'">'.$showcontact['Contact']['contact_name'].'</a>';

	$listdata[$i]['company'] = $showcontact['Contact']['company'];
	$listdata[$i]['position'] = $showcontact['Contact']['job_title'];
	$listdata[$i]['phone'] = $phone;
	$listdata[$i]['connection'] = $showcontact['Contact']['con_mfa'];
	$listdata[$i]['share'] = $showcontact['Contact']['share_type'];
	$listdata[$i]['manage'] = '<a href="'.$this->PortalHelper->makeUrl('Contacts', 'editcontact', 'id=' . $showcontact['Contact']['id']).'"><i class="icon-pencil"></i></a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="checkdelete('.$showcontact['Contact']['id'].');"><i class="icon-trash"></i></a>';
	$i++;
}
?>