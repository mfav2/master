<?php

App::uses('AppController', 'Controller');
App::uses('Utility', 'mfa');
App::uses('HttpSocket', 'Network/Http');
class AppointmentsAPIController extends AppController {

    public $name = 'AppointmentsAPI';
    public $uses = array('Appointment', 'AppointmentAttachment', 'AppointmentColor',
        'AppointmentHandle', 'AppointmentLocation', 'AppointmentPriority',
        'AppointmentStatus', 'AppointmentUserReader');
    public $components = array("PortalHelper");
    public $helpers = array('Portal');

 /*   public function showdata($start_date= null, $end_date = null, $id = null) {
        
		 $currentUser = $this->Session->read('AuthUser');
         if(empty($id)){
             $id = $currentUser['AuthUser']['id'];
         }
            
            $setshow = array();

            $invite_array = array();
            $person_array = array();    
            $userinvite = $this->AppointmentUserReader->find('all', array(
                    'conditions' => array(
                        'AppointmentUserReader.user_id'=>$id
                    ),
               )
            );
            foreach ($userinvite as $using) :
                $invite_array[] = $using['AppointmentUserReader']['appointment_id'];
            endforeach;
            
            $userappoint = $this->Appointment->find('all', array(
                    'conditions' =>array(
                        'Appointment.user_id'=>$id
                    ),
                )
            );
			

            foreach ($userappoint as $using) :
                $person_array[] = $using['Appointment']['id'];
            endforeach;            
            $merging = array_merge($person_array, $invite_array);
            
            $appt_id = '';
            $count_array = count($merging);
            $chk = 1;
            foreach ($merging as $allid) {
                if ($chk == $count_array) {
                    $appt_id .= $allid;
                } else {
                    $appt_id .= $allid . ", ";
                }
                $chk++;
            }
          $showdata = $this->Appointment->find('all', array(
                'fields' => 'Appointment.id, Appointment.name, Appointment.start_date, Appointment.end_date, Appointment.detail,Appointment.private_note,
                             Appointment.user_id, Appointment.is_all_day, Appointment.is_alert, AppointmentColor.color_code, Appointment.priority_type_id',      
                    'conditions' => array(
                        'Appointment.id in (' . $appt_id . ')',
                        'Appointment.deleted = ' => 'N',
                    ),
                    'order' => 'Appointment.start_date ASC',
                )
           );  
            $showdetail = array();
            $showdetail_root = array();
            $priorities = '';
            $private = '';
            $public = '';
            $detail = '-';
            foreach ($showdata as $show) {
                
                $priority = $show['Appointment']['priority_type_id'];
                switch($priority){
                    case 1:    $priorities = '';
                                break;
                    case 2:    $priorities = 'img/icon/green_flag.png';
                               break;
                    case 3:    $priorities = 'img/icon/yellow_flag.png';
                                break;
                    case 4:    $priorities = 'img/icon/red_flag.png';
                               break;
                    default:   break;             
                }
                  
                if(!empty($show['Appointment']['private_note'])){
                    $detail = $show['Appointment']['private_note'];
                }else{
                    $detail = $show['Appointment']['detail'];
                }

              
                if($show['Appointment']['is_all_day'] == 'Y'){
                    $showall = true;
                    $title = 'All day';
                }else{
                    $showall = false;
                    $title = '';
                }
                
				
	    $check = $this->Appointment->find('all', array(
                    'conditions' =>array(
                        'Appointment.user_id'=>$id,
					    'Appointment.id'=> $show['Appointment']['id']
                    ),
                )
            );

             
				if(!empty($check))
				$url= $this->PortalHelper->makeUrl('Appointments', 'edit', 'id=' . $show['Appointment']['id']);
				else
				$url= $this->PortalHelper->makeUrl('Appointments', 'view', 'id=' . $show['Appointment']['id']);

                $showdetail['title'] = $title;
                $showdetail['start'] = $show['Appointment']['start_date'];
                $showdetail['end'] = $show['Appointment']['end_date'];
                $showdetail['description'] = $detail; 
                $showdetail['img'] = $priorities; 
                $showdetail['url'] = $url; 
                $showdetail['color'] = $show['AppointmentColor']['color_code'];
                $showdetail['allDay'] = $showall;
                $showdetail['remind'] = $show['Appointment']['is_alert'];
                $showdetail_root[] = $showdetail;
            }
            $setshow = $showdetail_root;   
            $this->set('showdata', $setshow);
            $this->layout = "blank";

    }*/

    public function showdata($start_date= null, $end_date = null, $id = null){
        $currentUser = $this->Session->read('AuthUser');
        $setshow = array();
        $invite_array = array();
        $showdetail = array();
        
        if(empty($id)){
            $id = $currentUser['AuthUser']['id'];
        }
        if(empty($start_date)){
            $start_date = date('Y-m')."-1";
            
        }
        if(empty($end_date)){
            $end_date = date('Y-m-d', strtotime('first day of next month'));
        }
        $start_date = date('Y-m-d', strtotime('-1 month', strtotime($start_date)));
        $end_date = date('Y-m-d', strtotime('+2 month', strtotime($end_date)));
        $start = $start_date.' 00:00:00';
        $end = $end_date.' 00:00:00';
        
        $user_invite = $this->AppointmentUserReader->find('list', array(
            'conditions' =>array(
               'AppointmentUserReader.user_id'=>$id,
               'AppointmentUserReader.deleted' => 'N', 
               "AppointmentUserReader.confirmed_status" => 'Accepted',
               'AppointmentUserReader.start_date >= ' => $start,
               'AppointmentUserReader.end_date < ' => $end,
            ),
            'fields' => array('AppointmentUserReader.appointment_id'),
        ));
        
        //pr($user_invite);
        
        //die();
        $userappoint = $this->Appointment->find('list', array(
            'conditions' => array(
                'Appointment.user_id'=>$id,
                'Appointment.deleted'=> 'N',
                'Appointment.start_date >= \'' . $start_date . ' 00:00:00\'',
                'Appointment.end_date < \'' . $end_date . ' 00:00:00\'',
            ),
            'fields' => array('Appointment.id'),
        ));
        
        $merging_list = array_merge($user_invite, $userappoint);
        
        $appoint_list = implode(',', $merging_list);
        $showdata = '';
        $showdetail_root = array();
        if(!empty($appoint_list)){
            
            $showdata = $this->Appointment->find('all', array(
                'conditions' => array(
                    'Appointment.deleted' => 'N',
                    'Appointment.id in (' . $appoint_list . ')',
                ),
                'fields' => array('Appointment.id', 'Appointment.name', 'Appointment.start_date', 'Appointment.end_date', 'Appointment.detail', 
                 'Appointment.private_note', 'Appointment.user_id', 'Appointment.is_all_day', 'Appointment.is_alert', 'Appointment.color_id', 'Appointment.priority_type_id'),
            ));
						
            /************* Query for show data *****************/
            foreach ($showdata as $show) {
                $priority = $show['Appointment']['priority_type_id'];
                $color = $show['Appointment']['color_id'];
                switch($priority){
                    case 1:    $priorities = '';
                               break;

                    case 2:    $priorities = 'img/icon/green_flag.png';
                               break;

                    case 3:    $priorities = 'img/icon/yellow_flag.png';
                               break;

                    case 4:    $priorities = 'img/icon/red_flag.png';
                               break;

                    default:   break;             
                }
                
                switch($color){
                    case 1 : $color_code = '#83ba1f;';
                             break;
                    
                    case 2 : $color_code = '#2673ec;';
                             break;
                         
                    case 3 : $color_code = '#e56c19;';
                             break;
                         
                    default: $color_code = '#83ba1f;';
                             break; 
                }
                
                if(!empty($show['Appointment']['private_note'])){
                    $detail = $show['Appointment']['private_note'];
                }else{
                    $detail = $show['Appointment']['detail'];
                }
                
                if($show['Appointment']['is_all_day'] == 'Y'){
                    $showall = true;
                    $title = 'All day';
                }else{
                    $showall = false;
                    $title = '';
                }
                
                $check = $this->Appointment->find('all', array(
                    'conditions' =>array(
                            'Appointment.user_id'=>$id,
                            'Appointment.id'=> $show['Appointment']['id'],
                            'Appointment.deleted = ' => 'N',
                            'Appointment.start_date >= \'' . $start_date . ' 00:00:00\'',
                            'Appointment.end_date < \'' . $end_date . ' 00:00:00\'',
                    ),
                ));
                
                if(!empty($check))
                   $url= $this->PortalHelper->makeUrl('Appointments', 'edit', 'id=' . $show['Appointment']['id']);
                else
                   $url= $this->PortalHelper->makeUrl('Appointments', 'view', 'id=' . $show['Appointment']['id']);
                
                $showdetail['title'] = $title;
                $showdetail['start'] = $show['Appointment']['start_date'];
                $showdetail['end'] = $show['Appointment']['end_date'];
                $showdetail['description'] = $detail; 
                $showdetail['img'] = $priorities; 
                $showdetail['url'] = $url; 
                $showdetail['color'] = $color_code;
                $showdetail['allDay'] = $showall;
		$showdetail['remind'] = $show['Appointment']['is_alert'];
                $showdetail_root['appointment'][] = $showdetail;
            }
            /***************************************************/
        }
        
        $setshow = $showdetail_root;   
        $this->set('showdata', $setshow);
        $this->layout = "blank";
       
        
        /*$userappoint = $this->Appointment->find('all', array(
            'conditions' =>array("
                (
                Appointment.user_id = $id
                AND
                Appointment.deleted = 'N'
                AND
                Appointment.start_date >= '$start_date 00:00:00'
                AND
                Appointment.end_date <  '$end_date 00:00:00'
                )
                
                OR 
                
                (
                Appointment.id IN($invite_list)
                )
                "
                /*
                    'Appointment.user_id'=>$id,
                    'Appointment.deleted'=> 'N',
                    'Appointment.start_date >= \'' . $start_date . ' 00:00:00\'',
                    'Appointment.end_date < \'' . $end_date . ' 00:00:00\'',
                    array('OR' => array(
                          'Appointment.id in ('.$invite_list.')',
                        ),
                    ),
                
            ),
        ));*/
        
    }
        
        
    public function show(){
        $result = array();
        $data_array = array();
        $Folders = array();
        $bError = false;
        $sErrorCode = '';
        $sErrorMessage = '';
        //$this->request->data['user_id'] = 188;
            if (!$this->request->isPost()) {
                    $bError = true;
                    $sErrorCode = 'AUTHBASE0001';
                    $sErrorMessage = 'POST only API';
            }
            if (!$bError) {
                    if (empty($this->request->data['user_id'])) {
                            $bError = true;
                            $sErrorCode = 'AUTHBASE0002';
                            $sErrorMessage = 'Invalid input parameter';
                    }
            }

            $result = array();
            if ($bError) {
                    $result['Result']['Error']['ErrorCode'] = $sErrorCode;
                    $result['Result']['Error']['ErrorMessage'] = $sErrorMessage;
            } else {
                    $result['Result']['Showdata'] = $this->request->data['user_id'];
            }
            //pr($result);    
            $this->set('result', $result);
            $this->layout = "blank";
    }

}
