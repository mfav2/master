<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('Utility', 'mfa');
class PollsController extends AppController {

    public $name = 'Polls';
    public $uses = array('ShowListContent','Content', 'ContentCategory', 'getQuestion', 'Question', 'QuestionAnswer',
            'QuestionResult', 'Category', 'CategoryDescription', 'ContentType', 'ResourceContent', 'PortalAttachment', 'getCategory', 'getContentCategory', 'ContentUserReader', 'ContentOrganization');
    public $components = array("PortalHelper", "FileStorageComponent", "ProfileHelper");
    public $helpers = array('Form', 'Html', 'Portal', 'Session');
    
    
    private function allowContentByUserId() {
            $currentUser = $this->Session->read('AuthUser');
            $user_id = $currentUser['AuthUser']['id'];
            $ContentUserReader = $this->ContentUserReader->find('list', array(
                    'conditions' => array(
                        'ContentUserReader.user_id = ' . $user_id,
                        'ContentUserReader.content_type_id' => 5,
                        'ContentUserReader.deleted ' =>'N' ,
                    ),
                    'fields' => array('ContentUserReader.content_id')
            ));
                    $contentIdList = -1;
            if(!empty($ContentUserReader)){
                    $contentIdList = implode(',' , $ContentUserReader);
            }

            return $contentIdList;

    }
    private function allowContentByOrgId(){
            // Get All Position!!!
            $authUserOrgPosition = $this->Session->read('AuthUser.AuthUserOrganizationPosition'); // FUCK PHP5.3
            $orgIdList = '';
                    foreach ($this->Session->read('AuthUser.AuthUserOrganizationPosition') as $i => $position) {
                            $orgIdList[] = $position['AuthOrganizationPosition']['organization_id'];
                    }
                    if (!empty($orgIdList) || $orgIdList != null) {
                $orgIdList = implode(',' , $orgIdList);	
            }
            $data_org = array();
            $data_org['organization_id'] = $orgIdList;
            $socket = new HttpSocket();
            $getOrganization = $socket->post(Configure::read('Config.CenterBaseAPI.EndPoint') . 'getOrganizationMain/.json', $data_org);
            $getOrganize = json_decode($getOrganization, true);	
            $getOrg = $getOrganize['Result']['OrganizationMain'];

            if(!empty($getOrg)){
                    $getdata = implode(',' , $getOrg);	
            }
            $ContentOrganization = $this->ContentOrganization->find('list', array(
                    'conditions' => array(
                            'ContentOrganization.organization_id in (' . $getdata . ')',
                            'ContentOrganization.deleted '=>'N' ,
                    ),
                    'fields' => array('ContentOrganization.content_id')
            ));

            $contentIdList = -1;
            if(!empty($ContentOrganization)){
                    $contentIdList = implode(',' , $ContentOrganization);	
            }
            return $contentIdList;
    }


    private function getAllowContentId() {
        $x = $this->allowContentByUserId();
	//$x = -1;  // empty and Is Null ; 
        $y = $this->allowContentByOrgId();
	//$y = -1;  // empty and Is Null ;
        if ($x != -1 && $y != -1) {
			
            return $x . ',' . $y;
        } elseif ($x != -1 && $y == -1) {
            return $x;
        } elseif ($x == -1 && $y != -1) {
            return $y;
        } else {
            return -1;
        }
    }

    private function getUserNameById($id) {
        $belong_org_id = '';
        $org_description = '';
        $currentUser = $this->Session->read('AuthUser');
        
        $cache_type = 'getUserProfile';
        $cache_key = $cache_type.'-'.$id;
        Cache::set(array('duration' => '+1 day'));
        $userProfile = Cache::read($cache_key);
        
        if(empty($userProfile)){
            $socket = new HttpSocket();
            $userProfile = json_decode($socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', array('list_user_id' => $id)), true);
            Cache::set(array('duration' => '+5 minutes'));  
            Cache::write($cache_key,$userProfile);
        }
        
          if (isset($userProfile['Result']['UserProfile'])) {
              if ($userProfile['Result']['UserProfile'] != null) {
                
                $cache_type = 'getUserNameById';
                $cache_key = $cache_type.'-'.$id;
                Cache::set(array('duration' => '+1 day'));
                $userdetail = Cache::read($cache_key);
                if(empty($userdetail)){
                    $userdetail['Profile'] = $userProfile['Result']['UserProfile'][0]['UserProfile']['first_name_th'] . ' ' . $userProfile['Result']['UserProfile'][0]['UserProfile']['last_name_th'];
                    //pr($userProfile['Result']['UserProfile'][0]['UserOrganizationPosition']);

                    foreach($userProfile['Result']['UserProfile'][0]['UserOrganizationPosition'] as $OrgPos){

                        if($OrgPos['is_real_position'] == 'Y'){

                            $userdetail['Position'] = $OrgPos['OrganizationPosition']['Position']['position_name'];
                            if(!empty($OrgPos['OrganizationPosition']['Organization']['parent_id'])){
                                $belong_org_id = $OrgPos['OrganizationPosition']['Organization']['parent_id'];
                            }
                            $userdetail['belong_organize_id'] = $belong_org_id; 
                        }
                        /*if(!empty($OrgPos['OrganizationPosition']['Organization'])){
                            foreach($OrgPos['OrganizationPosition']['Organization']['OrganizationDescription'] as $orgDesc){
                                $userdetail['OrganizationDescription'] = $orgDesc;
                            }
                        }else{
                                $userdetail['OrganizationDescription'] = '';
                        }    */
                        if(!empty($OrgPos['OrganizationPosition']['Organization'])){
                         $userdetail['OrganizationDescription'] = $OrgPos['OrganizationPosition']['Organization']['OrganizationDescription'];
                         
                            foreach($OrgPos['OrganizationPosition']['Organization']['OrganizationDescription'] as $orgDesc){
                                $userdetail['old_OrganizationDescription'] = $orgDesc;
                            }
                            
                        }else{
                             $userdetail['OrganizationDescription'] = '';
                             $userdetail['old_OrganizationDescription'] = '';
                        }
                        Cache::set(array('duration' => '+1 days'));  
                        Cache::write($cache_key,$userdetail);
                    }
                    
                }
                
                return $userdetail;
              }else{
                return '<span title="'.__('There is no user in database.').'">'.__('N/A').'</span>';
              }
          }  
        /*if (isset($userProfile['Result']['UserProfile'])) {
            if ($userProfile['Result']['UserProfile'] != null) {
                return $userProfile['Result']['UserProfile'][0]['UserProfile']['first_name_th'] . ' ' . $userProfile['Result']['UserProfile'][0]['UserProfile']['last_name_th'];
            } else {
                return '<span title="There is no user in database.">N/A</span>';
            }
        }*/
    }

    
    private function getListUserProfile($item) {
        /* [+++++++++++ START GET LIST OF VIEWED USER +++++++++++] */
        $view_user_id_list = '';
        foreach ($item as $i => $e) {
            $view_user_id_list .= $e['created_user_id'];
            if (isset($item[$i + 1])) {
                $view_user_id_list .= ',';
            }
        }
        $socket = new HttpSocket();
        $result = json_decode($socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', array('list_user_id' => $view_user_id_list)), true);
        return $result['Result'];
        /* [--------- END GET LIST OF VIEWED USER ----------] */
    }

    private function contentsRelatedUser($contents) {
        $currentUser = $this->Session->read('AuthUser');
       if(!empty($contents)){ 
            foreach ($contents as $i => $content) {
				 //pr($content);
                  //$contents[$i]['Content']['content_updated_user'] = $this->getUserNameById($content['Content']['user_id']);
                  $get_username = $this->getUserNameById($content['ShowListContent']['user_id']);
                  
                  /************** Get Under Organization ***********************/
                 if(!empty($get_username['belong_organize_id'])){
                        $cache_type = 'getOrganizationName';
                        $cache_key = $cache_type.'-'.$get_username['belong_organize_id'];
                        Cache::set(array('duration' => '+1 day'));
                        $orgBelong = Cache::read($cache_key);

                        if(empty($orgBelong)){
                            $data = array();
                            $data['list_org_id'] = $get_username['belong_organize_id'];
                            $socket = new HttpSocket();
                            $getOrgBelong = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganizationName/.json', $data);
                            $orgBelong = json_decode($getOrgBelong, true);
                            Cache::set(array('duration' => '+1 days'));  
                            Cache::write($cache_key,$orgBelong);
                        }
                            foreach($orgBelong['Result']['OrganizationName'] as $j=>$BelongOrganization){
                               $contents[$i]['ShowListContent']['organization_belong'] = $orgBelong['Result']['OrganizationName'];
                               //pr($BelongOrganization['OrganizationDescription']['description']);
                               $contents[$i]['ShowListContent']['old_organization_belong'] = $BelongOrganization['OrganizationDescription']['description'];
                            }
							
                           if(!empty($contents[$i]['ShowListContent']['organization_belong'])){
                                  $belongdesc = $this->ProfileHelper->CheckOrgBelongLang($contents[$i]['ShowListContent']['organization_belong'], $currentUser['AuthUserAudit']['UserProfile']['language']); 
                                  
                              if(!empty($belongdesc['belong_description'])){
                                  $belong_cate = $this->ProfileHelper->CheckLanguage($belongdesc['belong_description'], $currentUser['AuthUserAudit']['UserProfile']['language']);
                                  
								  $contents[$i]['ShowListContent']['organization_belong_id'] = $belong_cate['organization_id'];
								  
								  $contents[$i]['ShowListContent']['organization_belong'] = $belong_cate['description'];
								  
                              }
                           } 
                            /*$belongdesc = $this->ProfileHelper->CheckOrgBelongLang($contents[$i]['ShowListContent']['organization_belong'], $currentUser['AuthUserAudit']['UserProfile']['language']);
                            //pr($desc);
                            $belong_cate = $this->ProfileHelper->CheckLanguage($belongdesc['belong_description'], $currentUser['AuthUserAudit']['UserProfile']['language']);
                            
                            $contents[$i]['ShowListContent']['organization_belong'] = $belong_cate['description'];*/
                  }else{
                        $contents[$i]['ShowListContent']['organization_belong'] = '';
                  }       
                  
                  if(!empty($get_username['OrganizationDescription'])){
                    $Organize_desc =  $this->ProfileHelper->CheckLanguage($get_username['OrganizationDescription'], $currentUser['AuthUserAudit']['UserProfile']['language']);
                  }else{
                    $Organize_desc = '';  
                  }
                  
                  /*************************************************************/
                  $contents[$i]['ShowListContent']['content_updated_user'] = $get_username['Profile'];
                  $contents[$i]['ShowListContent']['position'] = $get_username['Position'];
                  $contents[$i]['ShowListContent']['old_organization_description'] = $get_username['old_OrganizationDescription'];
                  $contents[$i]['ShowListContent']['organization_description'] = $Organize_desc;
                  //$contents[$i]['ViewUserProfile'] = @$this->getListUserProfile($content['Content']['ContentView']);
                  //$contents[$i]['LikeUserProfile'] = @$this->getListUserProfile($content['Content']['ContentLike']);
                  //$contents[$i]['CommentUserProfile'] = @$this->getListUserProfile($content['Content']['ContentComment']);

            }
       } 
        //pr($contents);
        return $contents;
    }
    
	private function questionsRelatedUser($question) {
        $currentUser = $this->Session->read('AuthUser');
         if(!empty($question)){
             foreach($question AS $i => $q){
                $tmp_i = $i + 1;
                foreach($question[$i]['QuestionResult'] as $j=> $result){
                      $get_username = $this->getUserNameById($result['user_id']);
                      
                  /************** Get Under Organization ***********************/
                  if(!empty($get_username['belong_organize_id'])){
                        $cache_type = 'getOrganizationName';
                        $cache_key = $cache_type.'-'.$get_username['belong_organize_id'];
                        Cache::set(array('duration' => '+1 day'));
                        $orgBelong = Cache::read($cache_key);

                        if(empty($orgBelong)){
                            $data = array();
                            $data['list_org_id'] = $get_username['belong_organize_id'];
                            $socket = new HttpSocket();
                            $getOrgBelong = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganizationName/.json', $data);
                            $orgBelong = json_decode($getOrgBelong, true);
                            Cache::set(array('duration' => '+1 days'));  
                            Cache::write($cache_key,$orgBelong);
                        }
                            foreach($orgBelong['Result']['OrganizationName'] as $BelongOrganization){
                               $question[$i]['QuestionResult'][$j]['organization_belong'] = $orgBelong['Result']['OrganizationName'];
                               $question[$i]['QuestionResult'][$j]['old_organization_belong'] = $BelongOrganization['OrganizationDescription']['description'];
                            }
                            $belongdesc = $this->ProfileHelper->CheckOrgBelongLang($question[$i]['QuestionResult'][$j]['organization_belong'], $currentUser['AuthUserAudit']['UserProfile']['language']);
                            //pr($desc);
                            if(!empty($belongdesc['belong_description'])){
                                $belong_cate = $this->ProfileHelper->CheckLanguage($belongdesc['belong_description'], $currentUser['AuthUserAudit']['UserProfile']['language']);
                            }else{
                                $question[$i]['QuestionResult'][$j]['organization_belong'] = $belong_cate['description'];
                            }

                  }else{
                        $question[$i]['QuestionResult'][$j]['organization_belong'] = '';
                  }
                  
                  if(!empty($get_username['OrganizationDescription'])){
                    $Organize_desc =  $this->ProfileHelper->CheckLanguage($get_username['OrganizationDescription'], $currentUser['AuthUserAudit']['UserProfile']['language']);
                    
                    $question[$i]['QuestionResult'][$j]['organization_description'] = $Organize_desc['description'];
                  }else{
                    $question[$i]['QuestionResult'][$j]['organization_description'] = '';
                  }
                  /*************************************************************/
                  $question[$i]['QuestionResult'][$j]['content_updated_user'] = $get_username['Profile'];
                  $question[$i]['QuestionResult'][$j]['position'] = $get_username['Position'];
                  
                }
             }   
         }
         //pr($question);
        return $question;
    }


    public function index(){
        $currentUser = $this->Session->read('AuthUser');
        $user_id = $currentUser['AuthUser']['id'];
        $offset = 10;
        $limit = 10;
        $has_expire = 'Y';
        $expire = 'Content.expiry_date >= \'' . date('Y-m-d') . ' \'';
        $lang = 1;
        if(!empty($currentUser['AuthUserProfile'][0]['language'])){
                $lang = $currentUser['AuthUserProfile'][0]['language'];
        }
        $description = $this->getCategoryName(16, $lang);
        $allow_content = $this->getAllowContentId();
        $checkallow = '';
        if(!empty($allow_content)){
                $checkallow = 'getContentCategory.content_id in (' . $allow_content . ')';
        }

        $breadCrumbs = array(
            array(
                'label' => $description,
                'link' => false
            ),
        );
        $countallcontent = $this->getContentCategory->find('count', array(
                'conditions' => array(
                   'getContentCategory.category_id'=>16,
                   'Content.deleted' => 'N',
                   'Content.is_poll' => 'Y', 
                    $expire,
                   'OR' => array(
                        'Content.notification_type = \'A\'',
                        'Content.user_id = ' . $this->Session->read('AuthUser.AuthUser.id'),
                        $checkallow,

                   ),
                ),
                //'fields' => array('ContentCategory.content_id'),
                'order' => array('Content.event_date'=>'DESC'),
        ));
        
        $getContentCategory = $this->getContentCategory->find('all', array(
                'conditions' => array(
                        'getContentCategory.category_id'=>16,
                        'Content.deleted' => 'N',
                        'Content.is_poll' => 'Y', 
                        $expire,
                        'OR' => array(
                                'Content.notification_type = \'A\'',
                                'Content.user_id = ' . $user_id,
                                $checkallow,
                                
                        )
                ),
                'fields' => array('getContentCategory.category_id', 'Content.id', 'Content.event_date', 
                    'Content.title', 'Content.gist', 'Content.organization_name', 'Content.belong_organization_name',
                    'Content.created_date', 'Content.user_id', 'Content.count_view', 'Content.is_comment',
                    'Content.count_comment', 'Content.first_name', 'Content.last_name', 'Content.count_vote', 
                    'Content.attachment1', 'Content.attachment2', 'Content.attachment3', 
                    'Content.attachment1_name', 'Content.attachment2_name', 'Content.attachment3_name'
                ),
                'order' => array('Content.event_date'=>'DESC'),
                'limit' => $limit
        ));
        $this->request->data['category_id'] = 16;
        $this->set('countContent', empty($countallcontent) ? 0 : $countallcontent);
        $this->set('contents', $getContentCategory);
        $this->set('offset', $offset);
        $this->set('limit', $limit);
        $this->set('has_expire', $has_expire);
        $this->set('breadCrumbs', $breadCrumbs);
        
        
    }
    
    public function viewall(){
        $currentUser = $this->Session->read('AuthUser');
        $user_id = $currentUser['AuthUser']['id'];
        $offset = 10;
        $limit = 10;
        $has_expire = 'N';
        $lang = 1;
        if(!empty($currentUser['AuthUserProfile'][0]['language'])){
                $lang = $currentUser['AuthUserProfile'][0]['language'];
        }
        $description = $this->getCategoryName(16, $lang);
        $allow_content = $this->getAllowContentId();
        $checkallow = '';
        if(!empty($allow_content)){
                $checkallow = 'getContentCategory.content_id in (' . $allow_content . ')';
        }

        $breadCrumbs = array(
            array(
                'label' => $description,
                'link' => false
            ),
        );
        $countallcontent = $this->getContentCategory->find('count', array(
                'conditions' => array(
                   'getContentCategory.category_id'=>16,
                   'Content.deleted' => 'N',
                   'Content.is_poll' => 'Y', 
                   'OR' => array(
                        'Content.notification_type = \'A\'',
                        'Content.user_id = ' . $this->Session->read('AuthUser.AuthUser.id'),
                        $checkallow,

                   ),
                ),
                //'fields' => array('ContentCategory.content_id'),
                'order' => array('Content.event_date'=>'DESC'),
        ));
        
        $getContentCategory = $this->getContentCategory->find('all', array(
                'conditions' => array(
                        'getContentCategory.category_id'=>16,
                        'Content.deleted' => 'N',
                        'Content.is_poll' => 'Y', 
                        'OR' => array(
                                'Content.notification_type = \'A\'',
                                'Content.user_id = ' . $user_id,
                                $checkallow,
                        )
                ),
                'fields' => array('getContentCategory.category_id', 'Content.id', 'Content.event_date', 
                    'Content.title', 'Content.gist', 'Content.organization_name', 'Content.belong_organization_name',
                    'Content.created_date', 'Content.user_id', 'Content.count_view', 'Content.is_comment',
                    'Content.count_comment', 'Content.first_name', 'Content.last_name', 'Content.count_vote', 
                    'Content.attachment1', 'Content.attachment2', 'Content.attachment3', 
                    'Content.attachment1_name', 'Content.attachment2_name', 'Content.attachment3_name'
                ),
                'order' => array('Content.event_date'=>'DESC'),
                'limit' => $limit
        ));
        $this->request->data['category_id'] = 16;
        $this->set('countContent', empty($countallcontent) ? 0 : $countallcontent);
        $this->set('contents', $getContentCategory);
        $this->set('offset', $offset);
        $this->set('limit', $limit);
        $this->set('has_expire', $has_expire);
        $this->set('breadCrumbs', $breadCrumbs);
        
        
    }
    /******************* Search **********************************/
        function search(){
            $this->layout = 'blank';
            $offset = 10;
            $limit = 10;
            $currentUser = $this->Session->read('AuthUser');
            $user_id = $currentUser['AuthUser']['id'];
            $allow_content = $this->getAllowContentId();
            $checkallow = '';
            if(!empty($allow_content)){
              $checkallow = 'getContentCategory.content_id in (' . $allow_content . ')';
            }
            $data = $this->request->data;
            $countallcontent = $this->getContentCategory->find('count', array(
                    'conditions' => array(
                       'getContentCategory.category_id'=>16,
                       "Content.title ILIKE '%".trim($data['keyword'])."%'",
                       'Content.deleted' => 'N',
                       'Content.is_poll' => 'Y', 
                       'OR' => array(
                            'Content.notification_type = \'A\'',
                            'Content.user_id = ' . $user_id,
                            $checkallow
                       ),
                    ),
                    //'fields' => array('ContentCategory.content_id'),
                    'order' => array('Content.event_date'=>'DESC'),
                    'recursive' => 3
            ));
            $getContentCategory = $this->getContentCategory->find('all', array(
                    'conditions' => array(
                       'getContentCategory.category_id'=>16,
                       "Content.title ILIKE '%".trim($data['keyword'])."%'",
                       'Content.deleted' => 'N',
                       'Content.is_poll' => 'Y',  
                       'OR' => array(
                            'Content.notification_type = \'A\'',
                            'Content.user_id = ' . $user_id,
                            $checkallow
                       ),
                    ),
                    'fields' => array('getContentCategory.category_id', 'Content.id', 'Content.event_date', 
                        'Content.title', 'Content.gist', 'Content.organization_name', 'Content.belong_organization_name',
                        'Content.created_date', 'Content.user_id', 'Content.count_view', 'Content.is_comment',
                        'Content.count_comment', 'Content.first_name', 'Content.last_name','Content.count_vote', 
                        'Content.attachment1', 'Content.attachment2', 'Content.attachment3', 
                        'Content.attachment1_name', 'Content.attachment2_name', 'Content.attachment3_name'
                    ),
                    'order' => array('Content.event_date'=>'DESC'),
                    'recursive' => 3,
                    'limit' => $limit
            ));
            // pr($getContentCategory);
            $this->request->data['category_id'] = 16;
            $this->set('category_id', 16);
            $this->set('countContent', empty($countallcontent) ? 0 : $countallcontent);
            $this->set('contents', $getContentCategory);
            $this->set('keyword', $data['keyword']);
            $this->set('offset', $offset);
            $this->set('limit', $limit);

        }

	/*************************************************************/
        public function viewmore(){
            $this->layout = 'blank';
            $currentUser = $this->Session->read('AuthUser');
            $user_id = $currentUser['AuthUser']['id'];
            $offset = $_POST['offset'];
            $limit = $_POST['limit'];
            $category_id = $_POST['category_id'];
            $chk_expire = $_POST['expire'];
            
            $allow_content = $this->getAllowContentId();
            $checkallow = '';
            $expire = '';
            if($chk_expire == 'Y'){
                $expire = 'Content.expiry_date >= \'' . date('Y-m-d') . ' \'';
            }
            if(!empty($allow_content)){
              $checkallow = 'getContentCategory.content_id in (' . $allow_content . ')';
            }
            $getContentCategory = $this->getContentCategory->find('all', array(
                    'conditions' => array(
                            'getContentCategory.category_id'=>$category_id,
                            'Content.deleted' => 'N',
                            'Content.is_poll' => 'Y',
                            $expire,
                            'OR' => array(
                                    'Content.notification_type = \'A\'',
                                    'Content.user_id = ' . $user_id,
                                    $checkallow,
                                    
                            )
                    ),
                    'fields' => array('getContentCategory.category_id', 'Content.id', 'Content.event_date', 
                        'Content.title', 'Content.gist', 'Content.organization_name', 'Content.belong_organization_name',
                        'Content.created_date', 'Content.user_id', 'Content.count_view', 'Content.is_comment',
                        'Content.count_comment', 'Content.first_name', 'Content.last_name', 'Content.count_vote', 
                        'Content.attachment1', 'Content.attachment2', 'Content.attachment3', 
                        'Content.attachment1_name', 'Content.attachment2_name', 'Content.attachment3_name'
                    ),
                    'order' => array('Content.event_date'=>'DESC'),
                    'recursive' => 3,
                    'offset' => $offset,
                    'limit' => $limit
            ));
            $this->set('category_id', $category_id);
            $this->set('contents', $getContentCategory);
            $this->set('offset', $offset);
            $this->set('limit', $limit);
            //pr($getContentCategory);
        }
	/*************************************************************/
        function viewmoresearch(){
            $this->layout = 'blank';
            
            $currentUser = $this->Session->read('AuthUser');
            $user_id = $currentUser['AuthUser']['id'];
            $offset = $_POST['offset'];
            $limit = $_POST['limit'];
            $category_id = $_POST['category_id'];
            
            $allow_content = $this->getAllowContentId();
            $checkallow = '';
            if(!empty($allow_content)){
              $checkallow = 'getContentCategory.content_id in (' . $allow_content . ')';
            }
            $data = $this->request->data;

            
            $getContentCategory = $this->getContentCategory->find('all', array(
                    'conditions' => array(
                       'getContentCategory.category_id'=>$category_id,
                       "Content.title ILIKE '%".trim($data['keyword'])."%'",
                       'Content.deleted' => 'N',
                       'Content.is_poll' => 'Y',
                       'OR' => array(
                            'Content.notification_type = \'A\'',
                            'Content.user_id = ' . $user_id,
                            $checkallow
                       ),
                    ),
                    'fields' => array('getContentCategory.category_id', 'Content.id', 'Content.event_date', 
                        'Content.title', 'Content.gist', 'Content.organization_name', 'Content.belong_organization_name',
                        'Content.created_date', 'Content.user_id', 'Content.count_view', 'Content.is_comment',
                        'Content.count_comment', 'Content.first_name', 'Content.last_name', 'Content.count_vote', 
                        'Content.attachment1', 'Content.attachment2', 'Content.attachment3', 
                        'Content.attachment1_name', 'Content.attachment2_name', 'Content.attachment3_name'
                    ),
                    'order' => array('Content.event_date'=>'DESC'),
                    'recursive' => 3,
                    'offset' => $offset,
                    'limit' => $limit
            ));
            $this->request->data['category_id'] = $category_id;
            $this->set('category_id', $category_id);
            $this->set('contents', $getContentCategory);
            $this->set('offset', $offset);
            $this->set('limit', $limit);
            
        }
        /*************************************************************/
    public function add(){
        $currentUser = $this->Session->read('AuthUser');
        $lang = 1;
        if(!empty($currentUser['AuthUserProfile'][0]['language'])){
                $lang = $currentUser['AuthUserProfile'][0]['language'];
        }
        $description = $this->getCategoryName(16, $lang);
        $breadCrumbs = array(
            array(
                'label' => $description,
                'link' => array('controller' => $this->name, 'action' => 'viewall', 'query' => false)
            ),
            array(
                'label' => __('Add New Poll'),
                'link' => false
            ),
        );
        if(!empty($this->request->data)){

            
            $this->Content->create();
            $content = array();
            $is_globe = FALSE;
            $data_noti = array();
            $contentOrgReader = array();
            $contentUserReader = array();
           // pr($this->request->data); die();
            /******************** Readers *************************/
            if (isset($this->request->data['Invite']['Orgs'])) {
                foreach ($this->data['Invite']['Orgs'] as $invite) {
                    if ($invite == 1) { // 1 means globe
                        $this->request->data['notification_type'] = 'A';
                        $is_globe = TRUE;
                        break;
                    }
                }
            }
            // $this->request->data['notification_type'] != list make it 'A'
            if (!($this->request->data['notification_type'] == 'A' ||
            $this->request->data['notification_type'] == 'D' ||
            $this->request->data['notification_type'] == 'C')) {
              $this->request->data['notification_type'] = 'A';
            }
            if($this->request->data['notification_type'] == 'A'){
               $is_globe = TRUE;
            }

            if($this->request->data['no_show_name'] == 1){
                $content['Content']['no_show_name'] = 'Y';
            }else{
                $content['Content']['is_notification'] = 'N';
            }

            $expire = $this->request->data['expiry_date'];
            $expiry_date = Utility::cdate($expire, 'd/m/Y', 'Y-m-d');
            /******************************************************/
            
            
            $content['Content']['title'] = htmlspecialchars($this->request->data['title'], ENT_QUOTES);
            $content['Content']['description'] = htmlspecialchars($this->request->data['description'], ENT_QUOTES);
            $content['Content']['is_poll'] = 'Y';
            $content['Content']['notification_type'] = $this->request->data['notification_type'];
            $content['Content']['published_date'] = date("Y-m-d H:i:s");
            $content['Content']['event_date'] = date("Y-m-d H:i:s");
            $content['Content']['user_id'] = $currentUser['AuthUser']['id'];
            $content['Content']['expiry_date'] = $expiry_date;
            $content['Content']['is_comment'] = 'N';
            $content['Content']['is_notification'] = 'N';
            $content['Content']['first_name'] = $currentUser['AuthUserProfile'][0]['first_name_th'];
            $content['Content']['last_name'] = $currentUser['AuthUserProfile'][0]['last_name_th'];
            $content['Content']['organization_name'] = $currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthOrganization']['AuthOrganizationDescription'][0]['description'];
            $content['Content']['position_name'] = htmlspecialchars($currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthPosition']['position_name'], ENT_QUOTES);
            
            /******************************* get Department for Announcement ****************************************/
                $data_org = array();
		$data_org['organization_id'] = $currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthOrganization']['id'];
		$socket = new HttpSocket();
		$getOrganization = $socket->post(Configure::read('Config.CenterBaseAPI.EndPoint') . 'getDepartDiv/.json', $data_org);
		$getOrganize = json_decode($getOrganization, true);	
		$getOrg = $getOrganize['Result']['getDepartDiv'];
            $content['Content']['div_created_id'] = $getOrg['depart_id'];
            $content['Content']['div_created_name'] = $getOrg['depart_name'];   
             /***********************************************************************************/
            
            /******************************* get Belong ****************************************/
            $data_detail = array();
            $data_detail['list_org_id'] = $currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthOrganization']['parent_id'];
            
            $socket = new HttpSocket();
            $getBelongOrganization = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganizationName/.json', $data_detail);
            $getbelongOrganize = json_decode($getBelongOrganization, true);
            foreach($getbelongOrganize['Result']['OrganizationName'] as $j=>$BelongOrganization){
                if($lang == $BelongOrganization['OrganizationDescription']['language_id']){
                    $desc_belong = $BelongOrganization['OrganizationDescription']['description'];
                    $content['Content']['belong_organization_name'] = $desc_belong;
                    break;
                }
            }
            /***********************************************************************************/ 
            
            if (!empty($this->request->data['attachment']['tmp_name'])) {
                $post = $this->request->data['attachment'];
                $tmp_name = $post['tmp_name'];
                $file_name = $post['name'];
                $file_error = $post['error'];
                $file_size = $post['size'];
                $file_type = $post['type'];
                        
                $attach_key = $this->addAttachment($post, $tmp_name, $file_name, $file_error, $file_size, $file_type, $currentUser['AuthUser']['id']);
                //pr($attach_key);
                $content['Content']['attachment1_name'] = $attach_key['original_name'];
                $content['Content']['attachment1'] = $attach_key['key'];
            }else{
                $content['Content']['attachment1'] = '';
                $content['Content']['attachment1_name'] = '';
            }
            
            if ($this->Content->save($content)) {
                $concate_arr = array();
                $concate_arr['ContentCategory']['content_id'] = $this->Content->id;
                $concate_arr['ContentCategory']['category_id'] = 16;
                $this->ContentCategory->save($concate_arr);
                
                // Add Org reader
                if(isset($this->request->data['Invite']['Orgs']) && !$is_globe) {
                    sort($this->request->data['Invite']['Orgs']);
                    foreach ($this->request->data['Invite']['Orgs'] as $invite) { 
                        $orgname = split(' ', $invite['organization_name']);
                        
                        $org_name = $orgname[1];
                        $this->ContentOrganization->create();
                        $contentReader['ContentOrganization']['content_id'] = $this->Content->id;
                        $contentReader['ContentOrganization']['organization_id'] = $invite['id'];
                        $contentReader['ContentOrganization']['organization_name'] = $org_name;
                        $this->ContentOrganization->save($contentReader);
                    }
                }
                 // Add User reader
                if (isset($this->request->data['Invite']['Users']) && !$is_globe) {
                    sort($this->request->data['Invite']['Users']);
                    foreach ($this->request->data['Invite']['Users'] as $invite) {
                        $this->ContentUserReader->create();
                        $contentReader['ContentUserReader']['content_id'] = $this->Content->id;
                        $contentReader['ContentUserReader']['content_type_id'] = 5;
                        $contentReader['ContentUserReader']['message'] = $this->data['title'];
                        $contentReader['ContentUserReader']['user_id'] = $invite['id'];
                        $contentReader['ContentUserReader']['name'] = $invite['name'];
                        $this->ContentUserReader->save($contentReader);
                        
                    }
                }
                
              
                
            }
            if(!empty($this->data['Poll'])){ 
               foreach($this->data['Poll'] AS $i => $poll){
                    $question['question_name'] = $poll['Question'];
                    $question['content_id'] = $this->Content->id;
                    //$question['resource_content_id'] = '';
                    $question['description'] = '';
                    $question['question_type'] = $poll['Type']; //1 = one answer, 2 = multi answers
                    
                    $this->Question->create();
                    $question_id = $this->Question->save($question);
                    
                    
                    /****************** Choice ********************/
                    
                    if(empty($poll['Option'])){
                        continue; 
                    }
                    
                    foreach($poll['Option'] AS $opt => $val){
                        $option['question_id'] = $this->Question->id;
                        $option['answer_name'] = $val;
                        
                        $this->QuestionAnswer->create();
                        $this->QuestionAnswer->save($option);
                    }
                }

                //$this->redirect($this->PortalHelper->makeUrl($this->name, 'view', 'content_id=' . $this->Content->id));
                $this->redirect($this->PortalHelper->makeUrl($this->name, 'index')); 
            }
            
            
        }else{
            
            $size = Utility::checkfilesize(Configure::read('Config.MaxFileSize'));
            $exten = Configure::read('Eportal.Extension.Allow');
            $this->set("size", $size);
            $this->set("breadCrumbs", $breadCrumbs); 
            $this->set('extension', $exten);
        }
    }
    public function checkandinsert(){
        $this->layout = "blank"; 
        $this->autoRender = false;
        $questans = false;
        
        if(!empty($this->request->data)){
            $title = $this->request->data['title'];
            $desc = $this->request->data['description'];
            
            foreach($this->request->data['Poll'] as $poll){ 
                if(empty($poll['Question'])){                    
                    $questans = true;
                    break;
                }else{
                    foreach($poll['Option'] as $answer){
                        if(empty($answer)){
                            $questans = true;
                            break;
                        }    
                    }
                    if($questans == true) break;
                }
            }
            
            if($questans == false){
                echo 0;
            }else{
                echo 1;
            }
        }
    }
    
    function validcheckbox(){
        $this->layout = "blank"; 
        $this->autoRender = false;
        $questans = false;
        //pr($this->request->data);
        if(!empty($this->request->data)){
            foreach($this->request->data['Question'] as $i=> $q){
                if($q['question_type'] == 1){
                   if(!isset($this->request->data['Option'][$i])){
                      $questans = true;
                      break;
                   }
                }else{
                   if(!isset($this->request->data['Option'][$i])){
                      $questans = true;
                      break;
                   }
                }
               
            }
            if($questans == false){
                echo 0;
            }else{
                echo 1;
            }
        }
        
    }

    
    function view(){
        $currentUser = $this->Session->read('AuthUser'); 
        $lang = 1;
        if(!empty($currentUser['AuthUserProfile'][0]['language'])){
                $lang = $currentUser['AuthUserProfile'][0]['language'];
        }
        $description = $this->getCategoryName(16, $lang);
        
        $breadCrumbs = array(
            array(
                'label' => $description,
                'link' => array('controller' => $this->name, 'action' => 'viewall', 'query' => false)
            ),
            array(
                'label' => __('Vote'),
                'link' => false
            ),
        );
        
        if(empty($this->request->query['content_id'])){
            $this->redirect("/main?pc=Polls&pa=index");
        }
        
        $content_id = $this->request->query['content_id'];
        
        $content = $this->Content->find('first', array(
            'conditions'=>array(
                'Content.id'=>$content_id,
                'Content.deleted'=>'N',
                'Content.is_poll'=>'Y',
            ),
            'fields' => array('Content.id', 'Content.user_id', 'Content.title', 'Content.description', 'Content.expiry_date', 
            'Content.attachment1', 'Content.attachment1_name')
        ));
        
        if(empty($content)){
           $this->redirect("/main?pc=Polls&pa=index"); 
        }
        
        $question = $this->getQuestion->find('all', array(
            'conditions'=>array(
                'getQuestion.content_id'=>$content_id,
                'getQuestion.deleted'=>'N',
            ),
            
        ));
        
        $alreadyAnsQuestion = array();
        foreach($question as $ques){
           $alreadyAnsQuestion[] = $ques['getQuestion']['id'];
        }
        $list_vote = implode(",", $alreadyAnsQuestion);
        
        $alreadyVote = $this->QuestionResult->find('count', array(
                'conditions'=>array(
                    'QuestionResult.question_id in ('.$list_vote.')',
                    'QuestionResult.deleted '=> 'N',
                    'QuestionResult.user_id' => $currentUser['AuthUser']['id']
                ),
                'fields' => array('QuestionResult.id')
        ));
        
        
        if(!empty($alreadyVote)){
            $this->set('alreadyVote',1);
        }else{
            $this->set('alreadyVote',0);
        }
        
        $this->set("breadCrumbs", $breadCrumbs);
        $this->set('content', $content);
        $this->set('question', $question);
    }  
    
    public function voteResult(){
        $currentUser = $this->Session->read('AuthUser');
        $lang = 1;
        if(!empty($currentUser['AuthUserProfile'][0]['language'])){
                $lang = $currentUser['AuthUserProfile'][0]['language'];
        }
        $description = $this->getCategoryName(16, $lang);
        
        $breadCrumbs = array(
            array(
                'label' => $description,
                'link' => array('controller' => $this->name, 'action' => 'viewall', 'query' => false)
            ),
            array(
                'label' => __('Result Vote'),
                'link' => false
            ),
        );
                
        if(!empty($this->request->data)){
            $content_id = $this->request->data['content_id'];
            $question = $this->request->data['Question'];
            $option = $this->request->data['Option'];
            
            $vote = $this->Content->find('first', array(
                'conditions' => array(
                    'Content.id' => $content_id,
                    'Content.deleted' => 'N'
                ),
                'fields' => array('Content.count_vote', 'Content.created_user_id', 'Content.updated_user_id')
            ));
            foreach($question AS $question_id => $q){
                $question_id_list[] = $question_id;
                $question_type = $q['question_type']; 
                
                $is_voted = $this->QuestionResult->find('first', array(
                        'conditions'=>array(
                            'QuestionResult.question_id' => $question_id,
                            'QuestionResult.deleted' => 'N',
                            'QuestionResult.created_user_id' => $currentUser['AuthUser']['id']
                    )
                ));

                if(!empty($is_voted)){ continue; }

                $question_result = array();
                if($question_type == 1){ // radio button
                    $question_result['question_id'] = $question_id;

                        $question_result['answer_id'] = $option[$question_id];
                        $question_result['user_id'] = $currentUser['AuthUser']['id'];
                        $question_result['vote_date'] = date('Y-m-d H:i:s');
                        $question_result['first_name'] = $currentUser['AuthUserProfile'][0]['first_name_th'];
                        $question_result['last_name'] = $currentUser['AuthUserProfile'][0]['last_name_th'];
                        $question_result['organization_name'] = $currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthOrganization']['AuthOrganizationDescription'][0]['description'];
                        $question_result['position_name'] = htmlspecialchars($currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthPosition']['position_name'], ENT_QUOTES);    

                        $this->QuestionResult->create();
                        $this->QuestionResult->save($question_result);

                }else if($question_type == 2){ // checkbox
                    foreach($option[$question_id]['Answer'] AS $i => $answer_id){

                        $question_result['question_id'] = $question_id;
                        $question_result['answer_id'] = $answer_id;
                        $question_result['user_id'] = $currentUser['AuthUser']['id'];
                        $question_result['vote_date'] = date('Y-m-d H:i:s');
                        $question_result['first_name'] = $currentUser['AuthUserProfile'][0]['first_name_th'];
                        $question_result['last_name'] = $currentUser['AuthUserProfile'][0]['last_name_th'];
                        $question_result['organization_name'] = $currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthOrganization']['AuthOrganizationDescription'][0]['description'];
                        $question_result['position_name'] = htmlspecialchars($currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthPosition']['position_name'], ENT_QUOTES);    

                        $this->QuestionResult->create();
                        $this->QuestionResult->save($question_result);
                    }
                }
            
            }
            $count = $vote['Content']['count_vote'];
            $count = $count+1;
            
            $vote_array = array();
            $this->Content->id = $content_id;
            $vote_array['Content']['count_vote'] = $count;
            $vote_array['Content']['created_user_id'] = $vote['Content']['created_user_id'];
            $vote_array['Content']['updated_user_id'] = $vote['Content']['updated_user_id'];
            $this->Content->save($vote_array);   
            $url = $this->PortalHelper->makeUrl($this->name, 'voteResult', 'content_id='.$content_id);
            $this->redirect($url);
            // End If request->data
        }else{
            $content_id = $this->request->query['content_id'];
           
            // ----------    show number of people in this Poll ------------
            $questions = $this->Question->find('all', array(
                'conditions'=>array(
                    'Question.content_id'=>$content_id,
                    'Question.deleted'=>'N'
                )
            ));

            if(empty($questions)){ return false; }

            $alreadyAnsQuestion = array();
            $ResultOfPoll = 0;

            foreach($questions as $question){
               $alreadyAnsQuestion[] = $question['Question']['id'];
               $question_id_list[] = $question['Question']['id'];
            }

            $list_vote = implode(',', $alreadyAnsQuestion);
            $ResultOfPoll = $this->QuestionResult->find('count', array(
                  'conditions'=>array(
                      'QuestionResult.question_id in ('.$list_vote.')',
                      'QuestionResult.deleted '=> 'N'
                  ),
                  'group' => array('QuestionResult.user_id')
            ));
            
            $this->set('ResultOfPoll',$ResultOfPoll);
                //-----------------------------------------------------------------
            
            $content = $this->Content->find('first', array(
                'conditions'=>array(
                    'Content.id'=>$content_id,
                    'Content.deleted'=>'N'
                ),
                'fields' => array('Content.id', 'Content.no_show_name', 'Content.user_id', 'Content.title', 'Content.description', 
            'Content.attachment1', 'Content.attachment1_name', 'Content.expiry_date')
            ));
            
            if(is_array($question_id_list)){
                $tmp_question_id_list = implode(",", $question_id_list);
            }else{
                $tmp_question_id_list = $question_id_list;
            }
            
            $sql_count = "Select count(*) as totuser
                    FROM pg_mb_question_results 
                    WHERE question_id in ($tmp_question_id_list) AND
                    answer_id IS NOT NULL AND
                    deleted = 'N' 
                    GROUP BY question_id 
                    ORDER BY question_id";
            $count_res = $this->QuestionResult->query($sql_count);
        
            $sql = "SELECT question_id, answer_id, count(answer_id) AS total
                    FROM pg_mb_question_results
                    WHERE question_id in ($tmp_question_id_list) AND
                    deleted = 'N' 
                    GROUP BY question_id, answer_id
                    ORDER BY question_id ASC, total";
            $tmp_result = $this->QuestionResult->query($sql);
            
            $max = 0;
            $result = array();
            
            if(!empty($tmp_result)){
                
                 foreach($tmp_result AS $i => $r){
                     $r = $r[0];
                     if($r['total'] > $max){ $max = $r['total']; }

                     $result[$r['question_id']]['Option'][$r['answer_id']] = array(
                         'answer_id'=>$r['answer_id'],
                         'total'=>$r['total']
                     );
                 }
            }
            
            $this->set('results', $result);
            $this->set('total_result', $count_res);
            $this->set('content', $content);
            $this->set('question', $questions);
            
            
        }
            $this->set("breadCrumbs", $breadCrumbs);
        
    }
    
    
    function delete() {
        $url = $this->PortalHelper->makeUrl('Polls', 'index');
        if(isset($this->request->query['content_id'])) {
                $data_arr = array();
                $id = $this->request->query['content_id'];

                $this->ShowListContent->id = $id;
                $data_arr['ShowListContent']['deleted'] = 'Y';
                if($this->ShowListContent->save($data_arr)){
                    $this->redirect($url);
                }

        }else{
               $this->redirect($url);
        }
        
    }
    
    function deleteAttachment(){
        $this->autoRender = false;
        $this->layout ='blank';
        $dataresource = array();
        $datacontent = array();
        
        $key = $this->request->data['id'];
        $content_id = $this->request->data['content_id'];
        $category_id = $this->request->data['category_id'];
        $type = $this->request->data['type'];
        $attach_name = $type.'_name';
        
         
        $this->Content->id = $content_id;
        $datacontent['Content'][$type] = '';
        $datacontent['Content'][$attach_name] = '';        
        if($this->Content->save($datacontent)){
            echo '1';
        }else{
            echo '0';
        }
 
    }
    
    function getCategoryName($category_id, $lang_id){
        $category = $this->CategoryDescription->find('first', array(
            'conditions'=>array('CategoryDescription.category_id'=>$category_id, 'CategoryDescription.language_id'=> $lang_id), 
            'fields'=>array('description')
        ));
        
        return @$category['CategoryDescription']['description'];
    }
    /***************************** Add Attachment *********************/
        function addAttachment($attachment, $tmp_name, $file_name, $file_error, $file_size, $file_type, $user_id){
        
            $result['original_name'] = $file_name;
            $result['pic_type'] = $file_type;
            $result['key'] = '';
            
            $code = Utility::uuid();
            $allowedExts = array('doc', 'docx', 'txt', 'pdf', 'jpg', 'jpeg', 'png', 'gif', 'xls', 'xlsx', 'ppt', 'pptx', 'bmp', 'zip', 'rar', '7z', 'swf');
            $receieve_error = explode(".", $file_name);
            $extension = strtolower(end($receieve_error));
            if(is_uploaded_file($tmp_name) && ($file_size < Configure::read('Config.MaxFileSize')) && in_array($extension, $allowedExts)) { 
                    if ($file_error > 0) {
                        echo __("Return Error Code: ") . $file_error . "<br>";
                    }else { 
                        $path = 'attachment/content/';
                        if(!file_exists($path)) {
                            mkdir($path, 0777, true);
                        }
                        $uploadSuccess = move_uploaded_file($tmp_name, $path . $code . '.' . $extension);
                        $filename = $code . '.' . $extension;
                        if ($uploadSuccess) {
                             $results = $this->FileStorageComponent->save($path, $code . '.' . $extension, $file_type);
                             if ($results) {
                                    /*$data =array();
                                    $data['ResourceContent']['resource_category_id'] = 0;
                                    $data['ResourceContent']['path'] = $path;
                                    $data['ResourceContent']['content_type'] = $file_type;
                                    $data['ResourceContent']['original_name'] = $file_name;
                                    $data['ResourceContent']['file_size'] = $file_size;
                                    $data['ResourceContent']['user_id'] = $user_id;
                                    $data['ResourceContent']['key'] = $results;
                                    $this->ResourceContent->create();
                                    $this->ResourceContent->save($data);*/
                                        
                                    $result['key'] = $results;
                             }
                        }else{
                            echo  '-1';
                           $result['key'] = '';
                        }
                    }                         
            } 
            
            return $result;
        }   
}
?>