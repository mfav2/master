<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');

class MessagesController extends AppController {

    public $name = 'Messages';
    public $uses = array('MessageRegister', 'MessageUser', 'Message', 'MessageGroup', 'MessageGroupUser', 'ResourceContent', 'PortalAttachment');
    public $components = array('FileStorageComponent', "ProfileHelper"); 
	public $helpers = array('Form', 'Html', 'Portal', 'Session');
    
    
    public function getUserNameFromList($user_list){ // array $user_list
        $socket = new HttpSocket();
        $ulist['list_user_id'] = implode(",", $user_list);
        $getUserProfileResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', $ulist);
        $getUserProfile = json_decode($getUserProfileResult, true);

        $user = array();
        if(empty($getUserProfile)){ 
            return false; 
        }else{
            if(!empty($getUserProfile['Result']['Error'])){
                return false;
            }
            
            foreach($getUserProfile['Result']['UserProfile'] AS $i => $u){
                $uid = $u['UserProfile']['user_id'];
                $uname = $u['UserProfile']['first_name_th'] . " " . $u['UserProfile']['last_name_th'];

                $user[$uid] = $uname;
            }
            
            return $user;
        }
    }
    
    
    //ok
    public function index($event = '') {
        $this->Session->write('pc', $this->request->query['pc']);
        $this->Session->write('pa', $this->request->query['pa']);
        if(!empty($this->request->query['pq'])){
            $this->Session->write('pq', base64_decode($this->request->query['pq']));
        }else{
            $this->Session->delete('pq');
        }
        if(!empty($this->request->query['pr'])){
           $this->Session->write('pr', base64_decode($this->request->query['pr'])); 
        }else{
            $this->Session->delete('pr');
        }        
        $this->layout = 'dashboard';
        
		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];

		$data["user_id"] = $user_id;

		$socket = new HttpSocket();
		$getGroupResult = $socket->post(Configure::read('Config.MessageBaseAPI.EndPoint') . 'getMessageGroup/.json', $data);
		$contactGroup = json_decode($getGroupResult, true);
		//pr($contactGroup);
		//die();
                $getCountMessage = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'summarySomeMessage/.json', $data);
                $contactMessage = json_decode($getCountMessage, true);
                $countMsg = $contactMessage['Result']['CountMessage'];
                
                $this->set('CountMessage',$countMsg);
		$messageGroup = $contactGroup['Result']['MessageGroup'];
		$this->set('messageGroup', $messageGroup);
        
        //pr($this->Session->read('AuthUser'));
	}
    
    public function miniContactList(){
        $this->layout = 'blank';
        $currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];

		$data["user_id"] = $user_id;

		$socket = new HttpSocket();
		$getGroupResult = $socket->post(Configure::read('Config.MessageBaseAPI.EndPoint') . 'getMessageGroup/.json', $data);
		$contactGroup = json_decode($getGroupResult, true);
		//pr($contactGroup);
		//die();
		if(empty($contactGroup['Result']['Error'])){				
		$messageGroup = $contactGroup['Result']['MessageGroup'];
                
                $getCountMessage = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'summarySomeMessage/.json', $data);
                $contactMessage = json_decode($getCountMessage, true);
                $countMsg = $contactMessage['Result']['CountMessage'];
                
                $this->set('CountMessage',$countMsg);
			$this->set('messageGroup', $messageGroup);
				
		}
				
    }
    
    public function miniListContact(){
        $this->layout = 'blank';
        
		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];

		$data["user_id"] = $user_id;

		$socket = new HttpSocket();
		$getGroupResult = $socket->post(Configure::read('Config.MessageBaseAPI.EndPoint') . 'getMessageGroup/.json', $data);
		$contactGroup = json_decode($getGroupResult, true);
       
		
		if(empty($contactGroup['Result']['Error'])){				
				$messageGroup = $contactGroup['Result']['MessageGroup'];
                
                $getCountMessage = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'summarySomeMessage/.json', $data);
                $contactMessage = json_decode($getCountMessage, true);
                $countMsg = $contactMessage['Result']['CountMessage'];
                
                $this->set('CountMessage',$countMsg);
				$this->set('messageGroup', $messageGroup);
				
		}
		
    }
    
    public function contactList(){
        $this->layout = 'blank';
        
		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];

		$data["user_id"] = $user_id;

		$socket = new HttpSocket();
		$getGroupResult = $socket->post(Configure::read('Config.MessageBaseAPI.EndPoint') . 'getMessageGroup/.json', $data);
		$contactGroup = json_decode($getGroupResult, true);
                
                //$getSummaryGroup = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'summaryMyMessage/.json', $data);
                //$summaryGroup = json_decode($getSummaryGroup, true);
		//pr($contactGroup);
		//die();
        
		$messageGroup = $contactGroup['Result']['MessageGroup'];
                
                $getCountMessage = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'summarySomeMessage/.json', $data);
                $contactMessage = json_decode($getCountMessage, true);
                $countMsg = $contactMessage['Result']['CountMessage'];
                
                $this->set('CountMessage',$countMsg);
		$this->set('messageGroup', $messageGroup);
    }

	public function getMessageGroupUser($group_id = 0, $last_message_time = null){
        $this->layout = 'blank';

		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];

		$data["group_id"] = $group_id;
        if(!empty($last_message_time)){ 
            $data['last_message_time'] = $last_message_time; 
        }

		$socket = new HttpSocket();
		$getMessageGroupUserResult = $socket->post(Configure::read('Config.MessageBaseAPI.EndPoint') . 'getMessageGroupUser/.json', $data);
		$messageGroupUser = json_decode($getMessageGroupUserResult, true);
		//pr($messageGroupUser);

		$messageGroup = $messageGroupUser['Result']['Message'];

		$this->set('result', $messageGroup);
		$this->render('get_result');
	}

    public function getMessage($group_id=39, $limit=10){
        $this->layout = FALSE;

		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];
        
        if(!empty($_POST['group_id'])){
            $group_id = $_POST['group_id'];
        }
        if(!empty($_POST['last_message_time'])){
            $last_message_time = $_POST['last_message_time'];
        }
                
		$data["group_id"] = $group_id;
		$data["user_id"] = $user_id;
		if(!empty($last_message_time)){
			$data['last_message_time'] = $last_message_time;
		}
		$data["limit"] = $limit;

		$socket = new HttpSocket();
		$getMessageResult = $socket->post(Configure::read('Config.MessageBaseAPI.EndPoint') . 'getMessage/.json', $data);
		$getMessage = json_decode($getMessageResult, true);
                
		$messages = $getMessage['Result']['Message'];
        
        if(empty($messages)){ return false; }
        
        foreach($messages AS $i => $m){
            $message[] = $m['Message'];
            $user_lists[] = $m['Message']['user_id'];
        }
        
        $user = $this->getUserNameFromList($user_lists);
        
        foreach($message AS $i => $m){
           //$message[$i]['user_display_name'] = $user[$m['user_id']];
            
            if(!empty($m['document_key'])){
             $ext = array("JPEG","JPG", "GIF", "PNG", "jpg", "jpeg", "gif", "png");
               if(in_array($m['file_type'], $ext)){
                $message[$i]['message'] = '<a target="_blank" href="' . $this->FileStorageComponent->urlByKey($m['document_key']) . '"><img style="max-width:150px; max-height:150px;" src="/fileProviders/index/'.$m['document_key'].'" /></a>';   
               }else{
                $message[$i]['message'] = '<i class="icon-adt_atach"></i>&nbsp;<a target="_blank" href="' . $this->FileStorageComponent->urlByKey($m['document_key']) . '">' . $m['message'] . '</a>';
               } 
                
            }
        }
        
        $message = json_encode($message);
        echo $message;
    }
    
    
    public function setMessage(){

        $this->layout = 'blank';

		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];

        $data['message'] = @$_POST['message'];
        $data['user_id'] = @$_POST['user_id'];
        $data['group_id'] = @$_POST['group_id'];
        
//        $data['message'] = 'messagesss';
//        $data['user_id'] = '2885';
//        $data['group_id'] = '7';
//        echo "<br /><br /><br /><br />";
//pr($data);
		$socket = new HttpSocket();
		$setMessageResult = $socket->post(Configure::read('Config.MessageBaseAPI.EndPoint') . 'setMessage/.json', $data);
		$setMessage = json_decode($setMessageResult, true);
        
        //pr($setMessage);
        //$this->render('/Layouts/blank');
		//$this->set('result', $message);
		//$this->render('get_result');	
    }

    public function deleteMessage($message_id=0){

        $this->layout = 'blank';

		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];

                $data['message_id'] = $message_id;

		$socket = new HttpSocket();
		$deleteMessageResult = $socket->post(Configure::read('Config.MessageBaseAPI.EndPoint') . 'deleteMessage/.json', $data);
		$deleteMessage = json_decode($deleteMessageResult, true);
		//pr($message);
		$message = $deleteMessage['Result']['Message'];

		$this->set('result', $message);
		$this->render('get_result');
    }
    
    
    function addGroupForm(){
        $this->layout = 'blank';
        $group = $this->MessageGroup->find('list',array(
			'conditions' => array(
				'MessageGroup.deleted' => 'N',
				'MessageGroup.user_id' => $this->Session->read('AuthUser.AuthUser.id'),
				'MessageGroup.contact_type' => 2
				
			),
			'fields' => array('MessageGroup.group_name')
		));
		
		$this->set('group', $group);

        $this->render('add_group');
    }
    
    function addGroup(){
		$this->layout = 'blank';
		$currentUser = $this->Session->read('AuthUser');
		if(!empty($_POST)){ 
			
			$users_list = $_POST['users_list'] . ", " . $currentUser['AuthUser']['id'];
			$add_users = $_POST['users_list'];
            $group_name = $_POST['group_name'];
            $contact_type = $_POST['contact_type'];
			$group_id = $_POST['group_id'];
			$users_list = split(",", $users_list);
			$add_users = split(",", $add_users);
			
			if($contact_type == 1){
				$this->addContact($users_list, $contact_type);
			}else if($contact_type == 3){
				$this->addpersoningroup($add_users, $contact_type, $group_id);
			}else{
				if(!empty($group_name)){
					$group['group_name'] = $group_name;
					$group['created_date'] = date('Y-m-d H:i:s');
					$group['updated_date'] = date('Y-m-d H:i:s');
					$group['created_user_id'] = $currentUser['AuthUser']['id'];
					$group['updated_user_id'] = $currentUser['AuthUser']['id'];
					$group['deleted'] = 'N';			
					$group['contact_type'] = $contact_type;
					$group['user_id'] = $currentUser['AuthUser']['id'];
					$r = $this->MessageGroup->save($group);

					$group_id = $this->MessageGroup->getLastInsertID();
					$is_already_add_own = FALSE;

					foreach($users_list AS $u){
						if(!is_numeric($u)){ continue; }
						
						$this->MessageGroupUser->create();
						
						//// CHECK is group creator was added to group ?
						if($u == $currentUser['AuthUser']['id']){
							$checkIsAddOwn = $this->MessageGroupUser->find('first', array(
								'conditions'=>array(
									'MessageGroupUser.group_id'=>$group_id,
									'MessageGroupUser.user_id'=>$u,
									'MessageGroupUser.deleted'=>'N'
								)
							));
							
							if(!empty($checkIsAddOwn)){
								$is_already_add_own = TRUE;
							}
						}
						
						if($u == $currentUser['AuthUser']['id'] && $is_already_add_own){ continue; }
						
						$gUser['created_date'] = date('Y-m-d H:i:s');
						$gUser['created_user_id'] = $currentUser['AuthUser']['id'];
						$gUser['updated_date'] = date('Y-m-d H:i:s');
						$gUser['updated_user_id'] = $currentUser['AuthUser']['id'];
						$gUser['deleted'] = 'N';
						$gUser['user_id'] = $u;
						$gUser['group_id'] = $group_id;
						$gUser['is_admin'] = $u == $currentUser['AuthUser']['id'] ? 'Y' : 'N';
						$gUser['message_id'] = '0';
						$gUser['main_message_id'] = '0';
						$gUser['message_type'] = 'N/A';
						$gUser['contact_type'] = $contact_type;
						$gUser['owner_user_id'] = $currentUser['AuthUser']['id'];
						$this->MessageGroupUser->save($gUser);
					}
					$FirstMsg = __('Welcome');
					$this->Message->create();
					$FMsg['message'] = $FirstMsg;
					$FMsg['user_id'] = $currentUser['AuthUser']['id'];
					$FMsg['group_id'] = $group_id;
					$this->Message->save($FMsg);
				}
			}
		}
	}


	function addContact($users_list, $contact_type){
		if(!is_array($users_list)){
            $users_list = array($users_list); 
        }
		
		$currentUser = $this->Session->read('AuthUser');
		$my_user_id = $currentUser['AuthUser']['id'];
		foreach($users_list AS $u){
			if($u == $my_user_id || !is_numeric($u)){ continue; }
			$checkgroups = $this->MessageGroupUser->find('first', array(
					'conditions'=>array(
						'MessageGroupUser.owner_user_id '=>$u,
						'MessageGroupUser.user_id '=>$my_user_id,
						'MessageGroupUser.contact_type ' => 1,
					)
				));
			if(empty($checkgroups)){
				$checkgroups = $this->MessageGroupUser->find('first', array(
					'conditions'=>array(
						'MessageGroupUser.owner_user_id '=>$my_user_id,
						'MessageGroupUser.user_id '=>$u,
						'MessageGroupUser.contact_type ' => 1,
					)
				));
			}	

			if(!empty($checkgroups)){
				$group_id = $checkgroups['MessageGroupUser']['group_id'];
				$main_user = $this->MessageGroup->find('first' , array(
					'conditions' => array(
						'MessageGroup.deleted' => 'N',
						'MessageGroup.id' => $group_id
					)
				));	
				
				$chkgroupuser = $this->MessageGroupUser->find('all', array(
					'conditions' => array(
						'MessageGroupUser.group_id '=> $group_id
					)
				));
				

				$data_group_user = array();
				
				foreach($chkgroupuser as $chkgroup){
					
					if($chkgroup['MessageGroupUser']['deleted'] == 'Y'){
						$this->MessageGroupUser->id = $chkgroup['MessageGroupUser']['id'];
						$data_group_user['MessageGroupUser']['deleted'] = 'N';
						$data_group_user['MessageGroupUser']['created_user_id'] = $main_user['MessageGroup']['user_id'];
						$data_group_user['MessageGroupUser']['updated_date'] = date('Y-m-d H:i:s');
						$data_group_user['MessageGroupUser']['updated_user_id'] = $main_user['MessageGroup']['user_id'];
						$r = $this->MessageGroupUser->save($data_group_user);
					}

				}
			}else{
				$this->MessageGroup->create();
				$group['group_name'] = 'Unknow';
				$group['created_date'] = date('Y-m-d H:i:s');
				$group['updated_date'] = date('Y-m-d H:i:s');
				$group['created_user_id'] = $my_user_id;
				$group['updated_user_id'] = $my_user_id;
				$group['deleted'] = 'N';
				$group['user_id'] = $this->Session->read('AuthUser.AuthUser.id');
				$group['contact_type'] = $contact_type;
				$r = $this->MessageGroup->save($group);
				$group_id = $this->MessageGroup->getLastInsertID();		

				$u1 = array($u, $my_user_id);
				

				foreach($u1 as $u2){
					$this->MessageGroupUser->create();
					$gUser['created_date'] = date('Y-m-d H:i:s');
					$gUser['created_user_id'] = $my_user_id;
					$gUser['updated_date'] = date('Y-m-d H:i:s');
					$gUser['updated_user_id'] = $my_user_id;
					$gUser['deleted'] = 'N';
					$gUser['user_id'] = $u2;
					$gUser['group_id'] = $group_id;
					$gUser['is_admin'] = 'N';
					$gUser['message_id'] = '0';
					$gUser['main_message_id'] = '0';
					$gUser['message_type'] = 'N/A';
					$gUser['contact_type'] = $contact_type;
					$gUser['owner_user_id'] = $my_user_id;
					
					$this->MessageGroupUser->save($gUser);
				}
					$FirstMsg = __('Welcome');
					$this->Message->create();
					$FMsg['message'] = $FirstMsg;
					$FMsg['user_id'] = $my_user_id;
					$FMsg['group_id'] = $group_id;
					$this->Message->save($FMsg);
			}
		}
	}

 
    
	public function addpersoningroup($users_list, $contact_type, $group_id){
		$this->layout = 'blank';
		$group_user = array();
		if(!is_array($users_list)){
            $users_list = array($users_list); 
        }
		
		$findowner = $this->MessageGroup->find('first', array(
			'conditions'=>array(
						'MessageGroup.id '=>$group_id,
						'MessageGroup.deleted '=>'N',
						'MessageGroup.contact_type ' => 2,
			),
			'fields' => array('MessageGroup.user_id')
		));

		

		$checkalluseringroup = $this->MessageGroupUser->find('count', array(
			'conditions'=>array(
						'MessageGroupUser.group_id '=>$group_id,
						'MessageGroupUser.deleted '=>'N',
						'MessageGroupUser.contact_type ' => 2,
			)
		));
		
		$all = 31;
		$checkalluseringroup = intval($checkalluseringroup);
		$getAll = count($users_list);
		$canadd = 31 - $checkalluseringroup;
		
		if($getAll <= $canadd){
			foreach($users_list AS $u){
				
				$checkuseringroups = $this->MessageGroupUser->find('count', array(
					'conditions'=>array(
						'MessageGroupUser.group_id '=>$group_id,
						'MessageGroupUser.contact_type ' => 2,
						'MessageGroupUser.user_id '=>$u,
					)
				));
				
				if($checkuseringroups != 1){
					$this->MessageGroupUser->create();
					$group_user['created_date'] = date('Y-m-d H:i:s');
					$group_user['created_user_id'] = $this->Session->read('AuthUser.AuthUser.id');
					$group_user['updated_date'] = date('Y-m-d H:i:s');
					$group_user['updated_user_id'] = $this->Session->read('AuthUser.AuthUser.id');
					$group_user['deleted'] = 'N';
					$group_user['user_id'] = $u;
					$group_user['group_id'] = $group_id;
					$group_user['is_admin'] = 'N';
					$group_user['message_id'] = '0';
					$group_user['main_message_id'] = '0';
					$group_user['message_type'] = 'N/A';
					$group_user['contact_type'] = 2;
					$group_user['owner_user_id'] = $findowner['MessageGroup']['user_id'];
					$this->MessageGroupUser->save($group_user);
				}else{
					
					$checkusersg = $this->MessageGroupUser->find('first', array(
						'conditions'=>array(
							'MessageGroupUser.group_id '=>$group_id,
							'MessageGroupUser.contact_type ' => 2,
							'MessageGroupUser.user_id '=>$u,
							'MessageGroupUser.deleted' => 'Y'
						)
					));
					if(!empty($checkusersg)){
						$group_user['MessageGroupUser']['deleted'] = 'N';
						$group_user['MessageGroupUser']['created_user_id'] = $this->Session->read('AuthUser.AuthUser.id');
						$group_user['MessageGroupUser']['updated_date'] = date('Y-m-d H:i:s');
						$group_user['MessageGroupUser']['updated_user_id'] = $this->Session->read('AuthUser.AuthUser.id');
						$this->MessageGroupUser->id = $checkuseringroups['MessageGroupUser']['id'];
						$this->MessageGroupUser->save($group_user);
					}
					
				}
				/*if($checkuseringroups != 1){
					$this->MessageGroupUser->create();
					$group_user['created_date'] = date('Y-m-d H:i:s');
					$group_user['created_user_id'] = $this->Session->read('AuthUser.AuthUser.id');
					$group_user['updated_date'] = date('Y-m-d H:i:s');
					$group_user['updated_user_id'] = $this->Session->read('AuthUser.AuthUser.id');
					$group_user['deleted'] = 'N';
					$group_user['user_id'] = $u;
					$group_user['group_id'] = $group_id;
					$group_user['is_admin'] = 'N';
					$group_user['message_id'] = '0';
					$group_user['main_message_id'] = '0';
					$group_user['message_type'] = 'N/A';
					$group_user['contact_type'] = 2;
					$group_user['owner_user_id'] = $findowner['MessageGroup']['user_id'];
					$this->MessageGroupUser->save($group_user);
				}else{
					$checkuseringroups = $this->MessageGroupUser->find('first', array(
						'conditions'=>array(
							'MessageGroupUser.group_id '=>$group_id,
							'MessageGroupUser.contact_type ' => 2,
							'MessageGroupUser.user_id '=>$u,
							'MessageGroupUser.deleted' => 'Y'
						)
					));
					
					//pr($checkuseringroups);
					$group_user['deleted'] = 'N';
					$group_user['created_user_id'] = $this->Session->read('AuthUser.AuthUser.id');
					$group_user['updated_date'] = date('Y-m-d H:i:s');
					$group_user['updated_user_id'] = $this->Session->read('AuthUser.AuthUser.id');

					$this->MessageGroupUser->id = $checkuseringroups['MessageGroupUser']['id'];
					$this->MessageGroupUser->save($group_user);

				}*/
			}
			echo 'ok'; 
		}else{
			echo $canadd;
		}
		


	}


    function uploadFile($group_id = 0, $user_id = 0){
        $this->layout = 'blank';
        $filedata = $_FILES['Filedata'];
       
        if(!empty($filedata)){
            $data['message'] = $filedata['name'];
            $data['user_id'] = $user_id;
            $data['group_id'] = $group_id;
            $d_update_doc_key = $data;
            $socket = new HttpSocket();
            $original_name = $filedata['name'];
            $pic_type = $filedata['type'];
            $original_size = $filedata['size'];
            $code = Utility::uuid();
            $allowedExts = array('doc', 'docx', 'txt', 'pdf', 'jpg', 'jpeg', 'png', 'gif', 'xls', 'xlsx', 'ppt', 'pptx', 'bmp', 'zip', 'swf', 'rar');
            $receieve_error = explode(".", $filedata["name"]);
            $extension = strtolower(end($receieve_error));
            if(is_uploaded_file($filedata['tmp_name']) && ($filedata['size'] < 3145728) && in_array($extension, $allowedExts)) {
                if ($filedata["error"] > 0) {
                    echo __("Return Error Code: ") . $filedata["error"] . "<br />";
                }else { 
                    $path = 'attachment/message/';
                    if(!file_exists($path)) {
                        mkdir($path, 0777, true);
                    } 
                    $new_file_name = "$code.$extension";
                    $destination_path = "{$path}{$new_file_name}";
                    $uploadSuccess = move_uploaded_file($_FILES['Filedata']['tmp_name'], $destination_path);
                    if ($uploadSuccess) {
                            $result = $this->FileStorageComponent->save($path, $new_file_name, $filedata['type']);
                            if ($result) {
                                $data['document_key'] = $result;
                                $data['file_type'] = $extension;
                                $setMessageResult = $socket->post(Configure::read('Config.MessageBaseAPI.EndPoint') . 'setMessage/.json', $data);
                                $setMessage = json_decode($setMessageResult, true);
                                
                                echo 0;
                            }    
                    }else{
                        echo -1;
                    }
                }    
            }else{
                if($filedata['size'] > 3145728){
                   echo __("File over limit size")." (3MB)"; 
                }else if(!in_array($extension, $allowedExts)){
                   echo __("Only file ")."doc, docx, txt, pdf, jpg, jpeg, png, gif, xls, xlsx, ppt, pptx, bmp, zip, swf, rar"; 
                }else{
                   echo __("Can not upload file"); 
                }
                die();
            }
        }else{
            echo __("File Not Found");
            die();
        }
    }
    
    
    
    function test($user_list){
        $resource = $this->ResourceContent->find('all');
        //$this->set('resource', $resource);
        
        $attach = $this->PortalAttachment->find('all'); 
        //$this->set('attach', $attach); 
        
        $message = $this->Message->find('all', array(
            'recursive'=>2
        ));
        //$this->set('message', $message);
        if(!is_array($user_list)){
            $user_list = array($user_list);
        }
        
        $socket = new HttpSocket();
        $ulist['list_user_id'] = implode(",", $user_list);
        $getUserProfileResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', $ulist);
        $getUserProfile = json_decode($getUserProfileResult, true);
	pr($getUserProfile);
        $user = array();
        if(empty($getUserProfile)){ 
            return false; 
        }else{
            if(!empty($getUserProfile['Result']['Error'])){
                return false;
            }
            
            foreach($getUserProfile['Result']['UserProfile'] AS $i => $u){
                $uid = $u['UserProfile']['user_id'];
                $uname = $u['UserProfile']['first_name_th'] . " " . $u['UserProfile']['last_name_th'];

                $user[$uid] = $uname;
            }
            
            pr($user);
        }
    }


    function chatBox($group_id){
	$this->layout = 'blank';
        $currentUser = $this->Session->read('AuthUser');
        $user_id = $currentUser['AuthUser']['id'];
		$display = '';
		$last_message_time ='';
        $data["user_id"] = $user_id;
		$data["group_id"] = $group_id;

        $socket = new HttpSocket();
        $getGroupResult = $socket->post(Configure::read('Config.MessageBaseAPI.EndPoint') . 'getChatroom/.json', $data);
        $contactGroup = json_decode($getGroupResult, true);
        //pr($contactGroup);
        //die();
		
        $messageGroup = $contactGroup['Result']['MessageGroup'];

		foreach($messageGroup as $group_message){
			if($group_id == $group_message['getMessageGroup']['id']){
				 $display = $group_message['getMessageGroup']['group_name'];
				/*if(!empty($group_message['Message']['last_message_time'])){
					$last_message_time = $group_message['Message']['last_message_time'];
				}else{
					$last_message_time = date('Y-m-d H:i:s');
				}*/
				foreach($group_message['getMessageGroupUser'] as $last_message){
					
					if(!empty($group_message['getMessageGroupUser']['last_message_date'])){
						$last_message_time = $group_message['getMessageGroupUser']['last_message_date'];
					}else{
						$last_message_time = date('Y-m-d H:i:s');
					}

				}
				break;
			}
		}
      
        
        $this->set('messageGroup', $messageGroup);        
        $this->set('group_id', $group_id);
        $this->set('display', $display);
        $this->set('last_message_time', $last_message_time);
    }
    
    function deletedGroup(){
        $this->layout ='blank';
        $this->autoRender = false;
        $currentUser = $this->Session->read('AuthUser');
        $groupId = $this->request->data['group_id'];
        $user_id = $currentUser['AuthUser']['id'];
        if(!empty($groupId)){
                $data['group_id'] = $groupId;
                $data['user_id'] = $user_id;
		$socket = new HttpSocket();
		$deleteGroupResult = $socket->post(Configure::read('Config.MessageBaseAPI.EndPoint') . 'deleteGroup/.json', $data);
		$deleteGroup = json_decode($deleteGroupResult, true);
		//pr($message);
		$group = $deleteGroup['Result']['checkdelete'];
                
		echo $group;
        }
    }

	function viewgroup($groupId = '' ,$event){
		$this->layout ='blank';

		if(!empty($groupId)){
			$data = array();
			$data['group_id'] = $groupId;
			$socket = new HttpSocket();
			$showGroupResult = $socket->post(Configure::read('Config.MessageBaseAPI.EndPoint') . 'showGroup/.json', $data);
			$showGroup = json_decode($showGroupResult, true);
			$showuser = $showGroup['Result']['MessageGroupUser'];
			$this->set('showuser', $this->showRelatedUser($showuser));
			$this->set('groupId', $groupId);
			$this->set('event', $event);
		}
	}
	
	function editgroup($groupId = '' ,$event){
		$this->layout ='blank';

		if(!empty($groupId)){
			$data = array();
			$data['group_id'] = $groupId;
			$socket = new HttpSocket();
			$showGroupResult = $socket->post(Configure::read('Config.MessageBaseAPI.EndPoint') . 'showGroup/.json', $data);
			$showGroup = json_decode($showGroupResult, true);
			$show = $showGroup['Result']['MessageGroup'];
			$showuser = $showGroup['Result']['MessageGroupUser'];
			$this->set('showgroup', $show);
			$this->set('showuser', $this->showRelatedUser($showuser));
			$this->set('groupId', $groupId);
			$this->set('event', $event);
		}

	}

	function changegroupname(){
		//pr($this->request->data);
		$this->layout ='blank';
		$this->autoRender = false;
		if(!empty($this->request->data)){
			$data_group = array();
			$data_group['group_id'] = $this->request->data['group_id'];
			$data_group['groupname'] = $this->request->data['groupname'];
			$data_group['users_list'] = $this->request->data['users_list'];
			
			$socket = new HttpSocket();
			$editGroupResult = $socket->post(Configure::read('Config.MessageBaseAPI.EndPoint') . 'editGroup/.json', $data_group);
			$editGroup = json_decode($editGroupResult, true);

			$group = $editGroup['Result']['checkedit'];
			echo $group;
		}
	}
	private function showRelatedUser($showuser){
		$currentUser = $this->Session->read('AuthUser');
		if(!empty($showuser)){
			foreach ($showuser as $i => $show) {
			    $get_username = $this->getUserNameById($show['MessageGroupUser']['user_id']);
				
				if(!empty($get_username['DeptOrgCode'])){
					$data_dept = array();
					$data_dept['organization_id'] = $get_username['DeptOrgCode'];
					$socket = new HttpSocket();
					$getOrgDept = $socket->post(Configure::read('Config.CenterBaseAPI.EndPoint') . 'getOrganizationMain/.json', $data_dept);
					$orgDept = json_decode($getOrgDept, true);

					if(isset($orgDept['Result']['OrganizationMain'][2])){
						foreach($orgDept['Result']['OrganizationMain'] as $j=>$DeptOrganization){
						   $showuser[$i]['MessageGroupUser']['organization_department'] = $orgDept['Result']['OrganizationMain'][2];

						   $department_cate = $this->ProfileHelper->CheckLanguage($orgDept['Result']['OrganizationDepartDesc'][2]['OrganizationDescription'], $currentUser['AuthUserAudit']['UserProfile']['language']);
						   $showuser[$i]['MessageGroupUser']['organization_department_detail'] = $department_cate['description'];
							
						}
					}else{
						$showuser[$i]['MessageGroupUser']['organization_department'] = '';
						$showuser[$i]['MessageGroupUser']['organization_department_detail'] = '';
					}

				}
				if(!empty($get_username['belong_organize_id'])){
					$data = array();
					$data['list_org_id'] = $get_username['belong_organize_id'];
					$socket = new HttpSocket();
					$getOrgBelong = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganizationName/.json', $data);
					$orgBelong = json_decode($getOrgBelong, true);

				}
				foreach($orgBelong['Result']['OrganizationName'] as $j=>$BelongOrganization){
				   $showuser[$i]['MessageGroupUser']['organization_belong'] = $orgBelong['Result']['OrganizationName'];
				   //pr($BelongOrganization['OrganizationDescription']['description']);
				   
				}
				
				if(!empty($showuser[$i]['MessageGroupUser']['organization_belong'])){
					  $belongdesc = $this->ProfileHelper->CheckOrgBelongLang($showuser[$i]['MessageGroupUser']['organization_belong'], $currentUser['AuthUserAudit']['UserProfile']['language']); 
					  
				  if(!empty($belongdesc['belong_description'])){
					  $belong_cate = $this->ProfileHelper->CheckLanguage($belongdesc['belong_description'], $currentUser['AuthUserAudit']['UserProfile']['language']);
					 
					$showuser[$i]['MessageGroupUser']['organization_belong_id'] = $belong_cate['organization_id'];

					$showuser[$i]['MessageGroupUser']['organization_belong'] = $belong_cate['description'];
					  
				  }
			   }
			   
			   else{
				   $showuser[$i]['MessageGroupUser']['organization_belong'] = '';
			   }

				if(!empty($get_username['OrganizationDescription'])){
					$Organize_desc =  $this->ProfileHelper->CheckLanguage($get_username['OrganizationDescription'], $currentUser['AuthUserAudit']['UserProfile']['language']);
				}else{
					$Organize_desc = '';  
				}
				/*************************************************************/
                  $showuser[$i]['MessageGroupUser']['show_user'] = $get_username['Profile'];
                  $showuser[$i]['MessageGroupUser']['position'] = $get_username['Position'];
                  $showuser[$i]['MessageGroupUser']['old_organization_description'] = $get_username['old_OrganizationDescription'];
                  $showuser[$i]['MessageGroupUser']['organization_description'] = $Organize_desc;
                  
			}
		}
		return $showuser;
	}

	private function getUserNameById($id){
		$belong_org_id = '';
		$org_id = '';
        $org_description = '';
        $currentUser = $this->Session->read('AuthUser');
		$socket = new HttpSocket();
        $userProfile = json_decode($socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', array('list_user_id' => $id)), true);
		if (isset($userProfile['Result']['UserProfile'])) {
			if ($userProfile['Result']['UserProfile'] != null) {
				$userdetail['Profile'] = $userProfile['Result']['UserProfile'][0]['UserProfile']['first_name_th'] . ' ' . $userProfile['Result']['UserProfile'][0]['UserProfile']['last_name_th'];
				foreach($userProfile['Result']['UserProfile'][0]['UserOrganizationPosition'] as $OrgPos){
					if($OrgPos['is_real_position'] == 'Y'){
						$userdetail['Position'] = $OrgPos['OrganizationPosition']['Position']['position_name'];
						if(!empty($OrgPos['OrganizationPosition']['Organization']['parent_id'])){
							$belong_org_id = $OrgPos['OrganizationPosition']['Organization']['parent_id'];
						}

						if(isset($OrgPos['OrganizationPosition']['Organization']['id'])){	
							$org_id = $OrgPos['OrganizationPosition']['Organization']['id'];
										
						}else{
							$org_id = '';
						}
						$userdetail['belong_organize_id'] = $belong_org_id; 
						$userdetail['DeptOrgCode'] = $org_id;
					}
					if(!empty($OrgPos['OrganizationPosition']['Organization'])){
						 $userdetail['OrganizationDescription'] = $OrgPos['OrganizationPosition']['Organization']['OrganizationDescription'];
						 
						foreach($OrgPos['OrganizationPosition']['Organization']['OrganizationDescription'] as $orgDesc){
							$userdetail['old_OrganizationDescription'] = $orgDesc;
								
						}
						
					}else{
						 $userdetail['OrganizationDescription'] = '';
						 $userdetail['old_OrganizationDescription'] = '';
					}
				}
			}

			return $userdetail;
		}else{
			return '<span title="'.__('There is no user in database.').'">'.__('N/A').'</span>';
		}
		
	}
}
