<?php

App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('Utility', 'mfa');
class BoardsController extends AppController {

    public $name = 'Boards';
    public $uses = array('Content', 'ContentCategory', 'CategoryDescription', 'Category',
        'ContentComment', 'ContentLike', 'ContentType', 'ContentView', 'ContentTag', 'ContentUserReader', 'ContentOrganization', 'ResourceContent', 'getContentCategory', 
        'PortalAttachment', 'getCategory', 'ShowListContent' );
    public $components = array("PortalHelper", "FileStorageComponent", "ProfileHelper");
	var $helpers = array('Session');
    public $helper = array('Portal');
    public $paginate = array(
        'limit' => 20,
//        'order' => array('File.id' => 'desc'),
        'recursive' => 2
    );

	 
	private function allowContentByUserId() {
		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];
		$ContentUserReader = $this->ContentUserReader->find('list', array(
			'conditions' => array(
                    'ContentUserReader.user_id = ' . $user_id,
					'ContentUserReader.content_type_id' => 1,
                    'ContentUserReader.deleted ' =>'N' ,
			),
			'fields' => array('ContentUserReader.content_id')
		));
			$contentIdList = -1;
		if(!empty($ContentUserReader)){
			$contentIdList = implode(',' , $ContentUserReader);
		}

		return $contentIdList;

	}
	private function allowContentByOrgId(){
		// Get All Position!!!
        $authUserOrgPosition = $this->Session->read('AuthUser.AuthUserOrganizationPosition'); // FUCK PHP5.3
        $orgIdList = '';
		foreach ($this->Session->read('AuthUser.AuthUserOrganizationPosition') as $i => $position) {
			$orgIdList[] = $position['AuthOrganizationPosition']['organization_id'];
		}
		if (!empty($orgIdList) || $orgIdList != null) {
            $orgIdList = implode(',' , $orgIdList);	
        }
		$data_org = array();
		$data_org['organization_id'] = $orgIdList;
		$socket = new HttpSocket();
		$getOrganization = $socket->post(Configure::read('Config.CenterBaseAPI.EndPoint') . 'getOrganizationMain/.json', $data_org);
		$getOrganize = json_decode($getOrganization, true);	
		$getOrg = $getOrganize['Result']['OrganizationMain'];
		
		if(!empty($getOrg)){
			$getdata = implode(',' , $getOrg);	
		}
		$ContentOrganization = $this->ContentOrganization->find('list', array(
			'conditions' => array(
				'ContentOrganization.organization_id in (' . $getdata . ')',
				'ContentOrganization.deleted '=>'N' ,
			),
			'fields' => array('ContentOrganization.content_id')
		));

		$contentIdList = -1;
		if(!empty($ContentOrganization)){
			$contentIdList = implode(',' , $ContentOrganization);	
		}
		return $contentIdList;
	}


    private function getAllowContentId() {
        $x = $this->allowContentByUserId();
	//$x = -1;  // empty and Is Null ; 
        $y = $this->allowContentByOrgId();
	//$y = -1;  // empty and Is Null ;
        if ($x != -1 && $y != -1) {
			
            return $x . ',' . $y;
        } elseif ($x != -1 && $y == -1) {
            return $x;
        } elseif ($x == -1 && $y != -1) {
            return $y;
        } else {
            return -1;
        }
    }

    private function getUserNameById($id) {
        $belong_org_id = '';
	$org_id = '';
        $org_description = '';
        $currentUser = $this->Session->read('AuthUser');
        
        $cache_type = 'getUserProfile';
        $cache_key = $cache_type.'-'.$id;
        Cache::set(array('duration' => '+1 day'));
        $userProfile = Cache::read($cache_key);
        
        if(empty($userProfile)){
            $socket = new HttpSocket();
            $userProfile = json_decode($socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', array('list_user_id' => $id)), true);
            Cache::set(array('duration' => '+5 minutes'));  
            Cache::write($cache_key,$userProfile);
        }
        
          if (isset($userProfile['Result']['UserProfile'])) {
              if ($userProfile['Result']['UserProfile'] != null) {
                
                $cache_type = 'getUserNameById';
                $cache_key = $cache_type.'-'.$id;
                Cache::set(array('duration' => '+1 day'));
                $userdetail = Cache::read($cache_key);
                if(empty($userdetail)){
                    $userdetail['Profile'] = $userProfile['Result']['UserProfile'][0]['UserProfile']['first_name_th'] . ' ' . $userProfile['Result']['UserProfile'][0]['UserProfile']['last_name_th'];
                    //pr($userProfile['Result']['UserProfile'][0]['UserOrganizationPosition']);

                    foreach($userProfile['Result']['UserProfile'][0]['UserOrganizationPosition'] as $OrgPos){

                        if($OrgPos['is_real_position'] == 'Y'){

                            $userdetail['Position'] = $OrgPos['OrganizationPosition']['Position']['position_name'];
                            if(!empty($OrgPos['OrganizationPosition']['Organization']['parent_id'])){
                                $belong_org_id = $OrgPos['OrganizationPosition']['Organization']['parent_id'];
                            }

				if(isset($OrgPos['OrganizationPosition']['Organization']['id'])){	
					$org_id = $OrgPos['OrganizationPosition']['Organization']['id'];
								
				}else{
					$org_id = '';
				}
                            $userdetail['belong_organize_id'] = $belong_org_id; 
			    $userdetail['DeptOrgCode'] = $org_id;
                        }
                        /*if(!empty($OrgPos['OrganizationPosition']['Organization'])){
                            foreach($OrgPos['OrganizationPosition']['Organization']['OrganizationDescription'] as $orgDesc){
                                $userdetail['OrganizationDescription'] = $orgDesc;
                            }
                        }else{
                                $userdetail['OrganizationDescription'] = '';
                        }    */
                        if(!empty($OrgPos['OrganizationPosition']['Organization'])){
                         $userdetail['OrganizationDescription'] = $OrgPos['OrganizationPosition']['Organization']['OrganizationDescription'];
                         
                            foreach($OrgPos['OrganizationPosition']['Organization']['OrganizationDescription'] as $orgDesc){
                                $userdetail['old_OrganizationDescription'] = $orgDesc;
									
                            }
                            
                        }else{
                             $userdetail['OrganizationDescription'] = '';
                             $userdetail['old_OrganizationDescription'] = '';
                        }
                        Cache::set(array('duration' => '+1 days'));  
                        Cache::write($cache_key,$userdetail);
                    }
                    
                }
                
                return $userdetail;
              }else{
                return '<span title="'.__('There is no user in database.').'">'.__('N/A').'</span>';
              }
          }  
        /*if (isset($userProfile['Result']['UserProfile'])) {
            if ($userProfile['Result']['UserProfile'] != null) {
                return $userProfile['Result']['UserProfile'][0]['UserProfile']['first_name_th'] . ' ' . $userProfile['Result']['UserProfile'][0]['UserProfile']['last_name_th'];
            } else {
                return '<span title="There is no user in database.">N/A</span>';
            }
        }*/
    }

    
    private function getListUserProfile($item) {
        /* [+++++++++++ START GET LIST OF VIEWED USER +++++++++++] */
        $view_user_id_list = '';
        foreach ($item as $i => $e) {
            $view_user_id_list .= $e['created_user_id'];
            if (isset($item[$i + 1])) {
                $view_user_id_list .= ',';
            }
        }
        $socket = new HttpSocket();
        $result = json_decode($socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', array('list_user_id' => $view_user_id_list)), true);
        return $result['Result'];
        /* [--------- END GET LIST OF VIEWED USER ----------] */
    }

    private function contentsRelatedUser($contents) {
        $currentUser = $this->Session->read('AuthUser');
       if(!empty($contents)){ 
            foreach ($contents as $i => $content) {
                  //$contents[$i]['Content']['content_updated_user'] = $this->getUserNameById($content['Content']['user_id']);
                  $get_username = $this->getUserNameById($content['ShowListContent']['user_id']);
                  
				  //pr($get_username); 
                  /************** Get Under Organization ***********************/
				  if(!empty($get_username['DeptOrgCode'])){
						$cache_type = 'getOrganizationDepartment';
						$cache_key = $cache_type.'-'.$get_username['DeptOrgCode'];
						Cache::set(array('duration' => '+1 day'));
						$orgDept = Cache::read($cache_key);
						if(empty($orgDept)){
							$data_dept = array();
							$data_dept['organization_id'] = $get_username['DeptOrgCode'];
							$socket = new HttpSocket();
                            $getOrgDept = $socket->post(Configure::read('Config.CenterBaseAPI.EndPoint') . 'getOrganizationMain/.json', $data_dept);
                            $orgDept = json_decode($getOrgDept, true);
                            Cache::set(array('duration' => '+1 days'));  
                            Cache::write($cache_key,$orgDept);	
						}

						if(isset($orgDept['Result']['OrganizationMain'][2])){
							foreach($orgDept['Result']['OrganizationMain'] as $j=>$DeptOrganization){
							   $contents[$i]['ShowListContent']['organization_department'] = $orgDept['Result']['OrganizationMain'][2];

							   $department_cate = $this->ProfileHelper->CheckLanguage($orgDept['Result']['OrganizationDepartDesc'][2]['OrganizationDescription'], $currentUser['AuthUserAudit']['UserProfile']['language']);
							   $contents[$i]['ShowListContent']['organization_department_detail'] = $department_cate['description'];
								
							}
						}else{
							$contents[$i]['ShowListContent']['organization_department'] = '';
							$contents[$i]['ShowListContent']['organization_department_detail'] = '';
						}
				  }
				  /*if(!empty($get_username['DeptOrgCode'])){
						$cache_type = 'getOrganizationDepartment';
						$cache_key = $cache_type.'-'.$get_username['DeptOrgCode'];
						Cache::set(array('duration' => '+1 day'));
						$orgDept = Cache::read($cache_key);
						if(empty($orgDept)){
							$data_dept = array();
							$data_dept['org_code'] = $get_username['DeptOrgCode'];
							$socket = new HttpSocket();
                            $getOrgDept = $socket->post(Configure::read('Config.CenterBaseAPI.EndPoint') . 'getOrganizationFromLevel/.json', $data_dept);
                            $orgDept = json_decode($getOrgDept, true);
                            Cache::set(array('duration' => '+1 days'));  
                            Cache::write($cache_key,$orgDept);	
						}
						foreach($orgDept['Result']['OrganizationLevel'] as $j=>$DeptOrganization){
						   $contents[$i]['ShowListContent']['organization_department'] = $orgDept['Result']['OrganizationLevel'];
						   
						}
				  }*/

				
                 if(!empty($get_username['belong_organize_id'])){
                        $cache_type = 'getOrganizationName';
                        $cache_key = $cache_type.'-'.$get_username['belong_organize_id'];
                        Cache::set(array('duration' => '+1 day'));
                        $orgBelong = Cache::read($cache_key);

                        if(empty($orgBelong)){
                            $data = array();
                            $data['list_org_id'] = $get_username['belong_organize_id'];
                            $socket = new HttpSocket();
                            $getOrgBelong = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganizationName/.json', $data);
                            $orgBelong = json_decode($getOrgBelong, true);
                            Cache::set(array('duration' => '+1 days'));  
                            Cache::write($cache_key,$orgBelong);
                        }
                            foreach($orgBelong['Result']['OrganizationName'] as $j=>$BelongOrganization){
                               $contents[$i]['ShowListContent']['organization_belong'] = $orgBelong['Result']['OrganizationName'];
                               //pr($BelongOrganization['OrganizationDescription']['description']);
                               $contents[$i]['ShowListContent']['old_organization_belong'] = $BelongOrganization['OrganizationDescription']['description'];
                            }
							
                           if(!empty($contents[$i]['ShowListContent']['organization_belong'])){
                                  $belongdesc = $this->ProfileHelper->CheckOrgBelongLang($contents[$i]['ShowListContent']['organization_belong'], $currentUser['AuthUserAudit']['UserProfile']['language']); 
                                  
                              if(!empty($belongdesc['belong_description'])){
                                  $belong_cate = $this->ProfileHelper->CheckLanguage($belongdesc['belong_description'], $currentUser['AuthUserAudit']['UserProfile']['language']);
                                  
				  $contents[$i]['ShowListContent']['organization_belong_id'] = $belong_cate['organization_id'];
								  
				  $contents[$i]['ShowListContent']['organization_belong'] = $belong_cate['description'];
								  
                              }
                           } 
                            /*$belongdesc = $this->ProfileHelper->CheckOrgBelongLang($contents[$i]['ShowListContent']['organization_belong'], $currentUser['AuthUserAudit']['UserProfile']['language']);
                            //pr($desc);
                            $belong_cate = $this->ProfileHelper->CheckLanguage($belongdesc['belong_description'], $currentUser['AuthUserAudit']['UserProfile']['language']);
                            
                            $contents[$i]['ShowListContent']['organization_belong'] = $belong_cate['description'];*/
                  }else{
                        $contents[$i]['ShowListContent']['organization_belong'] = '';
                  }       
                  
                  if(!empty($get_username['OrganizationDescription'])){
                    $Organize_desc =  $this->ProfileHelper->CheckLanguage($get_username['OrganizationDescription'], $currentUser['AuthUserAudit']['UserProfile']['language']);
					

                  }else{
                    $Organize_desc = '';  
                  }
                  
                  /*************************************************************/
                  $contents[$i]['ShowListContent']['content_updated_user'] = $get_username['Profile'];
                  $contents[$i]['ShowListContent']['position'] = $get_username['Position'];
                  $contents[$i]['ShowListContent']['old_organization_description'] = $get_username['old_OrganizationDescription'];
                  $contents[$i]['ShowListContent']['organization_description'] = $Organize_desc;
                  //$contents[$i]['ViewUserProfile'] = @$this->getListUserProfile($content['Content']['ContentView']);
                  //$contents[$i]['LikeUserProfile'] = @$this->getListUserProfile($content['Content']['ContentLike']);
                  //$contents[$i]['CommentUserProfile'] = @$this->getListUserProfile($content['Content']['ContentComment']);

            }
       } 
        //pr($contents);
        return $contents;
    }

    public function index(){
      $currentUser = $this->Session->read('AuthUser');
      $user_id = $currentUser['AuthUser']['id'];
      $offset = 10;
      $limit = 10;
      $has_expire = 'Y';
      $expire = 'Content.expiry_date >= \'' . date('Y-m-d') . ' \'';
      $lang = 1;
      if(!empty($currentUser['AuthUserProfile'][0]['language'])){
        $lang = $currentUser['AuthUserProfile'][0]['language'];
      }
      $description = $this->getCategoryName($this->request->query['category_id'], $lang);
      $allow_content = $this->getAllowContentId();
      $checkallow = '';

      if(!empty($allow_content)){
          $checkallow = 'getContentCategory.content_id in (' . $allow_content . ')';
      }

        $breadCrumbs = array(
            array(
                'label' => $description,
                'link' => false
            ),
        );

        $countallcontent = $this->getContentCategory->find('count', array(
          'conditions' => array(
             'getContentCategory.category_id'=>$this->request->query['category_id'],
             'Content.deleted' => 'N',
              $expire,
             'OR' => array(
              'Content.notification_type = \'A\'',
              'Content.user_id = ' . $this->Session->read('AuthUser.AuthUser.id'),
              $checkallow
             ),
          ),
          //'fields' => array('ContentCategory.content_id'),
          'order' => array('Content.event_date'=>'DESC'),
          'recursive' => 3
        ));


        $getContentCategory = $this->getContentCategory->find('all', array(
          'conditions' => array(
            'getContentCategory.category_id'=>$this->request->query['category_id'],
            'Content.deleted' => 'N',
            $expire,
            'OR' => array(
              'Content.notification_type = \'A\'',
              'Content.user_id = ' . $user_id,
              $checkallow
            )
          ),
          'fields' => array('getContentCategory.category_id', 'Content.id', 'Content.event_date', 
              'Content.title', 'Content.gist', 'Content.organization_name', 'Content.belong_organization_name',
              'Content.created_date', 'Content.user_id', 'Content.count_view', 'Content.is_comment',
              'Content.count_comment', 'Content.first_name', 'Content.last_name', 'Content.attachment1', 'Content.attachment2', 'Content.attachment3', 
          'Content.attachment1_name', 'Content.attachment2_name', 'Content.attachment3_name'
          ),
          'order' => array('Content.event_date'=>'DESC'),
          'recursive' => 3,
          'limit' => $limit
        ));

        $this->request->data['category_id'] = $this->request->query['category_id'];
        $this->set('countContent', empty($countallcontent) ? 0 : $countallcontent);
        $this->set('contents', $getContentCategory);
        $this->set('offset', $offset);
        $this->set('limit', $limit);
        $this->set('has_expire', $has_expire);
        $this->set('breadCrumbs', $breadCrumbs);
        $this->render('/Contents/viewall');

    }


	/*function index(){
		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];
		$offset = 10;
		$limit = 10;
                $has_expire = 'Y';
		$expire = 'Content.expiry_date >= \'' . date('Y-m-d') . ' \'';
		$lang = 1;
		if(!empty($currentUser['AuthUserProfile'][0]['language'])){
			$lang = $currentUser['AuthUserProfile'][0]['language'];
		}
		$description = $this->getCategoryName($this->request->query['category_id'], $lang);
		$allow_content = $this->getAllowContentId();
		$checkallow = '';
		if(!empty($allow_content)){
			$checkallow = 'getContentCategory.content_id in (' . $allow_content . ')';
		}

                $breadCrumbs = array(
                    array(
                        'label' => $description,
                        'link' => false
                    ),
                );
		
		$countallcontent = $this->getContentCategory->find('count', array(
			'conditions' => array(
			   'getContentCategory.category_id'=>$this->request->query['category_id'],
			   'Content.deleted' => 'N',
                            $expire,
			   'OR' => array(
					'Content.notification_type = \'A\'',
					'Content.user_id = ' . $this->Session->read('AuthUser.AuthUser.id'),
					$checkallow,
                                        
			   ),
			),
			//'fields' => array('ContentCategory.content_id'),
			'order' => array('Content.event_date'=>'DESC'),
		));



		$getContentCategory = $this->getContentCategory->find('all', array(
			'conditions' => array(
				'getContentCategory.category_id'=>$this->request->query['category_id'],
				'Content.deleted' => 'N',
                                $expire,
				'OR' => array(
					'Content.notification_type = \'A\'',
					'Content.user_id = ' . $user_id,
					$checkallow,
                                        $expire,
				)
			),
                        'fields' => array('getContentCategory.category_id', 'Content.id', 'Content.event_date', 
                            'Content.title', 'Content.gist', 'Content.organization_name', 'Content.belong_organization_name',
                            'Content.created_date', 'Content.user_id', 'Content.count_view', 'Content.is_comment',
                            'Content.count_comment', 'Content.first_name', 'Content.last_name', 'Content.attachment1', 'Content.attachment2', 'Content.attachment3', 
                        'Content.attachment1_name', 'Content.attachment2_name', 'Content.attachment3_name'
                        ),
			'order' => array('Content.event_date'=>'DESC'),
			'limit' => $limit
		));
		
		$this->request->data['category_id'] = $this->request->query['category_id'];
		$this->set('countContent', empty($countallcontent) ? 0 : $countallcontent);
		$this->set('contents', $getContentCategory);
		$this->set('offset', $offset);
		$this->set('limit', $limit);
                $this->set('has_expire', $has_expire);
		$this->set('breadCrumbs', $breadCrumbs);
                $this->render('/Contents/viewall');
	}*/
	
        
    /******************************* Add ***********************************/
    public function add(){
        $currentUser = $this->Session->read('AuthUser');
        //pr($this->request->data); 
        $lang = 1;
        if(!empty($currentUser['AuthUserProfile'][0]['language'])){
            $lang = $currentUser['AuthUserProfile'][0]['language'];
        }
        
       if(!empty($this->request->data)){
           $this->Content->create();
           $is_globe = FALSE;
           $data_noti = array();
           $contentReader = array();
           
            $sendnotidetail = strip_tags($this->request->data['Content']['description']);
            switch ($this->request->data['Content']['category_id']) {
                case 17 : $iclass = '10601';
                    break;

                case 18 : $iclass = '10602';
                    break;
                
                default: $iclass = '00000';
                    break;
            }
           
           
           
           if($this->request->data['Content']['notification_type'] == 'A'){
              $is_globe = TRUE;
           }
           
           if(isset($this->request->data['Content']['is_notification'])){
               if($this->request->data['Content']['is_notification'] == 1){
                  $this->request->data['Content']['is_notification'] = 'Y';
               }else{
                  $this->request->data['Content']['is_notification'] = 'N'; 
               }
           }else{
              $this->request->data['Content']['is_notification'] = 'N';
           }
           
           if(isset($this->request->data['Content']['is_comment'])){
               if($this->request->data['Content']['is_comment'] == 1){
                  $this->request->data['Content']['is_comment'] = 'Y'; 
               }else{
                  $this->request->data['Content']['is_comment'] = 'N'; 
               }
           }else{
               $this->request->data['Content']['is_comment'] = 'N';
           }
           
            $event_date = Utility::cdate($this->request->data['Content']['event_date'], 'd/m/Y', 'Y-m-d');
            $expiry_date = Utility::cdate($this->request->data['Content']['expiry_date'], 'd/m/Y', 'Y-m-d');
            
            $this->request->data['Content']['title'] = htmlspecialchars($this->request->data['Content']['title'], ENT_QUOTES);
            $this->request->data['Content']['description'] = htmlspecialchars($this->request->data['Content']['description'], ENT_QUOTES);
            $this->request->data['Content']['gist'] = htmlspecialchars($this->request->data['Content']['gist'], ENT_QUOTES);
            $this->request->data['Content']['published_date'] = date('Y-m-d');
            $this->request->data['Content']['event_date'] = $event_date;       
            $this->request->data['Content']['expiry_date'] = $expiry_date;        
            $this->request->data['Content']['user_id'] = $currentUser['AuthUser']['id'];
            $this->request->data['Content']['is_poll'] = 'N';
            $this->request->data['Content']['is_comment'] = 'Y';
            
            
            $this->request->data['Content']['first_name'] = $currentUser['AuthUserProfile'][0]['first_name_th'];
            $this->request->data['Content']['last_name'] = $currentUser['AuthUserProfile'][0]['last_name_th'];
            $this->request->data['Content']['organization_name'] = $currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthOrganization']['AuthOrganizationDescription'][0]['description'];
            $this->request->data['Content']['position_name'] = htmlspecialchars($currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthPosition']['position_name'], ENT_QUOTES);
            
            /******************************* get Department for Announcement ****************************************/
                $data_org = array();
		$data_org['organization_id'] = $currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthOrganization']['id'];
		$socket = new HttpSocket();
		$getOrganization = $socket->post(Configure::read('Config.CenterBaseAPI.EndPoint') . 'getDepartDiv/.json', $data_org);
		$getOrganize = json_decode($getOrganization, true);	
		$getOrg = $getOrganize['Result']['getDepartDiv'];
            $this->request->data['Content']['div_created_id'] = $getOrg['depart_id'];
            $this->request->data['Content']['div_created_name'] = $getOrg['depart_name'];   
             /***********************************************************************************/
            
            /******************************* get Belong ****************************************/
            $data_detail = array();
            $data_detail['list_org_id'] = $currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthOrganization']['parent_id'];
            
            $socket = new HttpSocket();
            $getBelongOrganization = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganizationName/.json', $data_detail);
            $getbelongOrganize = json_decode($getBelongOrganization, true);
            foreach($getbelongOrganize['Result']['OrganizationName'] as $j=>$BelongOrganization){
                if($lang == $BelongOrganization['OrganizationDescription']['language_id']){
                    $desc_belong = $BelongOrganization['OrganizationDescription']['description'];
                    $this->request->data['Content']['belong_organization_name'] = $desc_belong;
                    break;
                }
            }
            /***********************************************************************************/    
            if (!empty($this->request->data['Content']['attachment1']['tmp_name'])) {
                $post = $this->request->data['Content']['attachment1'];
                $tmp_name = $post['tmp_name'];
                $file_name = $post['name'];
                $file_error = $post['error'];
                $file_size = $post['size'];
                $file_type = $post['type'];
                        
                $attach_key = $this->addAttachment($post, $tmp_name, $file_name, $file_error, $file_size, $file_type, $currentUser['AuthUser']['id']);
                //pr($attach_key);
                $this->request->data['Content']['attachment1_name'] = $attach_key['original_name'];
                $this->request->data['Content']['attachment1'] = $attach_key['key'];
            }else{
                $this->request->data['Content']['attachment1'] = '';
                $this->request->data['Content']['attachment1_name'] = '';
            }

            if (!empty($this->request->data['Content']['attachment2']['tmp_name'])) {
                
                $post = $this->request->data['Content']['attachment2'];
                $tmp_name = $post['tmp_name'];
                $file_name = $post['name'];
                $file_error = $post['error'];
                $file_size = $post['size'];
                $file_type = $post['type'];
                        
                $attach_key = $this->addAttachment($post, $tmp_name, $file_name, $file_error, $file_size, $file_type, $currentUser['AuthUser']['id']);
                
                //$attach_key = $this->addAttachment($this->request->data['Content']['attachment2'], $currentUser['AuthUser']['id']);
                //pr($attach_key);
                $this->request->data['Content']['attachment2_name'] = $attach_key['original_name'];
                $this->request->data['Content']['attachment2'] = $attach_key['key'];
            }else{
                $this->request->data['Content']['attachment2'] = '';
                $this->request->data['Content']['attachment2_name'] = '';
            }

            if (!empty($this->request->data['Content']['attachment3']['tmp_name'])){
                   
                $post = $this->request->data['Content']['attachment3'];
                $tmp_name = $post['tmp_name'];
                $file_name = $post['name'];
                $file_error = $post['error'];
                $file_size = $post['size'];
                $file_type = $post['type'];
                        
                $attach_key = $this->addAttachment($post, $tmp_name, $file_name, $file_error, $file_size, $file_type, $currentUser['AuthUser']['id']);

                //$attach_key = $this->addAttachment($this->request->data['Content']['attachment3'], $currentUser['AuthUser']['id']);
                //pr($attach_key);
                $this->request->data['Content']['attachment3_name'] = $attach_key['original_name'];
                $this->request->data['Content']['attachment3'] = $attach_key['key'];
            }else{
                $this->request->data['Content']['attachment3'] = '';
                $this->request->data['Content']['attachment3_name'] = '';
            }
            
            if ($this->Content->save($this->request->data)) {
                /********************* Check is globe *************************/
                if($is_globe && $this->request->data['Content']['is_notification'] == 'Y'){
                    $notidate = Utility::addTime(0, 1,0);
                    $notidate = Utility::cdate($notidate, 'Y-m-d H:i:s', 'Y-m-d H:i');
                    $sendnotidetail = str_replace("&nbsp; ","",$sendnotidetail);     
                    $data_noti = array(
                        'time' => $notidate,
                        'to_id13' => 0,
                        'message' => $sendnotidetail,
                        'from_id13' => $currentUser['AuthUserProfile'][0]['officer_code'],
                        'iclass' => '00000',
                        'action' => substr(Configure::read('Portal.Application'), 0, -1) . $this->PortalHelper->makeUrl($this->name, 'view', 'content_id=' . $this->Content->id)
                    );
                    $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addAlert', $data_noti);
                }
                /********************* Add Org Reader *************************/
                if(isset($this->request->data['Invite']['Orgs']) && !$is_globe) {
                    sort($this->request->data['Invite']['Orgs']);
                    foreach ($this->request->data['Invite']['Orgs'] as $invite) { 
                        $orgname = split(' ', $invite['organization_name']);
                        
                        $org_name = $orgname[1];
                        $this->ContentOrganization->create();
                        $contentReader['ContentOrganization']['content_id'] = $this->Content->id;
                        $contentReader['ContentOrganization']['organization_id'] = $invite['id'];
                        $contentReader['ContentOrganization']['organization_name'] = $org_name;
                        $this->ContentOrganization->save($contentReader);

                    }
                     /************* Noti Organize ****************/
                    if ($this->request->data['Content']['is_notification'] == 'Y') {
                        $showdatanoti = $this->ContentOrganization->find('list', array(
                            'conditions' => array(
                                'ContentOrganization.content_id' => $this->Content->id,
                                'ContentOrganization.deleted = ' => 'N',
                            ),
                            'fields' => array('ContentOrganization.organization_id')
                        ));
                        $notiorg = '';
                        
                        $notiorg = implode(',', $showdatanoti);    
                        
                        // send Org to 
                        $notidate = Utility::addTime(0, 1,0);
                        $notidate = Utility::cdate($notidate, 'Y-m-d H:i:s', 'Y-m-d H:i');
                        $sendnotidetail = str_replace("&nbsp; ","",$sendnotidetail);
                        $data_noti = array(
                            'time' => $notidate,
                            'to_id' => $notiorg,
                            'message' => $sendnotidetail,
                            'from_id' => $currentUser['AuthUser']['id'],
                            'iclass' => $iclass,
                            'use_suborg' => 'Y',
                            'action' => substr(Configure::read('Portal.Application'), 0, -1) . $this->PortalHelper->makeUrl($this->name, 'view', 'content_id=' . $this->Content->id)
                        );
                        $socket = new HttpSocket();
                        $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addOrgAlert', $data_noti);

                    }
                    
                }
                /************************************************************ */

                /******************* Add User Reader ************************ */
                if (isset($this->request->data['Invite']['Users']) && !$is_globe) {
                    sort($this->request->data['Invite']['Users']);
                    foreach ($this->request->data['Invite']['Users'] as $invite) {
                        $this->ContentUserReader->create();
                        $contentReader['ContentUserReader']['content_id'] = $this->Content->id;
                        $contentReader['ContentUserReader']['content_type_id'] = '1';
                        $contentReader['ContentUserReader']['message'] = $this->request->data['Content']['title'];
                        $contentReader['ContentUserReader']['user_id'] = $invite['id'];
                        $contentReader['ContentUserReader']['name'] = $invite['name'];
                        $this->ContentUserReader->save($contentReader);
                    }
                    /* ******* Noti User ******** */
                    if ($this->request->data['Content']['is_notification'] == 'Y') {
                        $showdatanoti = $this->ContentUserReader->find('list', array(
                            'conditions' => array(
                                'ContentUserReader.content_id' => $this->Content->id,
                                'ContentUserReader.deleted ' => 'N',
                            ),
                            'fields' => array('ContentUserReader.user_id')
                        ));
                        $notiuser = implode(',', $showdatanoti);
                        
                        $notidate = Utility::addTime(0, 1,0);
                        $notidate = Utility::cdate($notidate, 'Y-m-d H:i:s', 'Y-m-d H:i');
                        $sendnotidetail = str_replace("&nbsp; ","",$sendnotidetail);    
                        $data_noti = array(
                            'time' => $notidate,
                            'to_id' => $notiuser,
                            'message' => $sendnotidetail,
                            'from_id' => $currentUser['AuthUser']['id'],
                            'iclass' => $iclass,
                            'action' => substr(Configure::read('Portal.Application'), 0, -1) . $this->PortalHelper->makeUrl($this->name, 'view', 'content_id=' . $this->Content->id)
                        );
                        $socket = new HttpSocket();
                        $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);
                    }
                 }
                /***************************************************************/
                $content = array();
                $content['ContentCategory']['content_id'] = $this->Content->id;
                $content['ContentCategory']['category_id'] = $this->request->data['Content']['category_id'];
                if (isset($this->request->data['Content']['sub_category_id'])) {
                    $content['ContentCategory']['sub_category_id'] = $this->request->data['Content']['sub_category_id'];
                }
                
                if($this->ContentCategory->save($content)) {
                    $this->redirect($this->PortalHelper->makeUrl($this->name, 'view', 'content_id=' . $content['ContentCategory']['content_id']));
                }else{
                    $this->redirect($this->PortalHelper->makeUrl($this->name, 'index'));
                }
                 
                 
            }else{
                $this->redirect($this->PortalHelper->makeUrl($this->name, 'index'));
            }
            // end Save Content

       }else{   
            //*********************** Not Post ***********************************
            $subcategory = '';
            $lang = 1;
            if(!empty($currentUser['AuthUserProfile'][0]['language'])){
                    $lang = $currentUser['AuthUserProfile'][0]['language'];
            }
            $description = $this->getCategoryName($this->request->query['category_id'], $lang);  

            $breadCrumbs = array(
                array(
                    'label' => $description,
                    'link' => array('controller' => $this->name, 'action' => 'viewall', 'query' => 'category_id='.$this->request->query['category_id'])
                ),
                array(
                    'label' => __('Add'),
                    'link' => false
                ),
            );
            $this->set('breadCrumbs', $breadCrumbs);

             //************************ Check Category *************************
            $chkcategory = $this->getCategory->find('first', array(
                'conditions' => array(
                    'getCategory.deleted' => 'N',
                    'getCategory.id' => $this->request->query['category_id']
                )
            ));

            //************************ Check subcategory *************************
           
                $subcategory = $this->getCategory->find('list', array(
                    'conditions' => array(
                        'getCategory.deleted' => 'N',
                        'getCategory.parent_id' => $this->request->query['category_id']
                    ),
                    'fields' => array('getCategory.category_name'),
                    'order' => array('getCategory.id ASC')
                ));

            

            //*******************************************************************
            $size = Utility::checkfilesize(Configure::read('Config.MaxFileSize'));
            $exten = Configure::read('Eportal.Extension.Allow');
            
            $this->request->data['Content']['category_id'] = $this->request->query['category_id'];
            $this->set('size', $size);
            $this->set('extension', $exten);
            $this->set('subcategory', $subcategory);
            $this->set('is_comment', $chkcategory['getCategory']['is_comment']);
            $this->set('chk_comment', $chkcategory['getCategory']['is_comment']);
            $this->set('modelClass', 'Content');
            $this->set('action', 'add');
            //pr($this->request->query);
            $this->render('/Contents/add');
       }
    }    
    /***********************************************************************/

    function edit(){
       $currentUser = $this->Session->read('AuthUser');  
       $lang = 1;
        if(!empty($currentUser['AuthUserProfile'][0]['language'])){
            $lang = $currentUser['AuthUserProfile'][0]['language'];
        }
       if (!empty($this->request->data)) {
           
           $is_globe = FALSE;
           $data_noti = array();
           $contentReader = array();
           
           if($this->request->data['Content']['notification_type'] == 'A'){
              $is_globe = TRUE;
           }
           
           if(isset($this->request->data['Content']['is_notification'])){
               if($this->request->data['Content']['is_notification'] == 1){
                  $this->request->data['Content']['is_notification'] = 'Y';
               }else{
                  $this->request->data['Content']['is_notification'] = 'N'; 
               }
           }else{
              $this->request->data['Content']['is_notification'] = 'N';
           }
           
           if(isset($this->request->data['Content']['is_comment'])){
               if($this->request->data['Content']['is_comment'] == 1){
                  $this->request->data['Content']['is_comment'] = 'Y'; 
               }else{
                  $this->request->data['Content']['is_comment'] = 'N'; 
               }
           }else{
               $this->request->data['Content']['is_comment'] = 'N';
           }
            $sendnotidetail = strip_tags($this->request->data['Content']['description']);
            $event_date = Utility::cdate($this->request->data['Content']['event_date'], 'd/m/Y', 'Y-m-d');
            $expiry_date = Utility::cdate($this->request->data['Content']['expiry_date'], 'd/m/Y', 'Y-m-d');
            
            $this->request->data['Content']['title'] = htmlspecialchars($this->request->data['Content']['title'], ENT_QUOTES);
            $this->request->data['Content']['description'] = htmlspecialchars($this->request->data['Content']['description'], ENT_QUOTES);
            $this->request->data['Content']['gist'] = htmlspecialchars($this->request->data['Content']['gist'], ENT_QUOTES);
            $this->request->data['Content']['event_date'] = $event_date;       
            $this->request->data['Content']['expiry_date'] = $expiry_date;  
            $this->request->data['Content']['is_comment'] = 'Y';
            /***********************************************************************************/    
           
            if(isset($this->request->data['Content']['attachment1'])){
                if (!empty($this->request->data['Content']['attachment1']['tmp_name'])) {
                    $post = $this->request->data['Content']['attachment1'];
                    $tmp_name = $post['tmp_name'];
                    $file_name = $post['name'];
                    $file_error = $post['error'];
                    $file_size = $post['size'];
                    $file_type = $post['type'];

                    $attach_key = $this->addAttachment($post, $tmp_name, $file_name, $file_error, $file_size, $file_type, $currentUser['AuthUser']['id']);
                    //pr($attach_key);
                    $this->request->data['Content']['attachment1_name'] = $attach_key['original_name'];
                    $this->request->data['Content']['attachment1'] = $attach_key['key'];
                }else{
                    $this->request->data['Content']['attachment1'] = '';
                    $this->request->data['Content']['attachment1_name'] = '';
                }
            }




            if(isset($this->request->data['Content']['attachment2'])){
                if (!empty($this->request->data['Content']['attachment2']['tmp_name'])) {
                    $post = $this->request->data['Content']['attachment2'];
                    $tmp_name = $post['tmp_name'];
                    $file_name = $post['name'];
                    $file_error = $post['error'];
                    $file_size = $post['size'];
                    $file_type = $post['type'];

                    $attach_key = $this->addAttachment($post, $tmp_name, $file_name, $file_error, $file_size, $file_type, $currentUser['AuthUser']['id']);

                    //$attach_key = $this->addAttachment($this->request->data['Content']['attachment2'], $currentUser['AuthUser']['id']);
                    //pr($attach_key);
                    $this->request->data['Content']['attachment2_name'] = $attach_key['original_name'];
                    $this->request->data['Content']['attachment2'] = $attach_key['key'];
                }else{
                    $this->request->data['Content']['attachment2'] = '';
                    $this->request->data['Content']['attachment2_name'] = '';
                }    
            }
            
            if(isset($this->request->data['Content']['attachment3'])){
                if (!empty($this->request->data['Content']['attachment3']['tmp_name'])){

                    $post = $this->request->data['Content']['attachment3'];
                    $tmp_name = $post['tmp_name'];
                    $file_name = $post['name'];
                    $file_error = $post['error'];
                    $file_size = $post['size'];
                    $file_type = $post['type'];

                    $attach_key = $this->addAttachment($post, $tmp_name, $file_name, $file_error, $file_size, $file_type, $currentUser['AuthUser']['id']);

                    //$attach_key = $this->addAttachment($this->request->data['Content']['attachment3'], $currentUser['AuthUser']['id']);
                    //pr($attach_key);
                    $this->request->data['Content']['attachment3_name'] = $attach_key['original_name'];
                    $this->request->data['Content']['attachment3'] = $attach_key['key'];
                }else{
                    $this->request->data['Content']['attachment3'] = '';
                    $this->request->data['Content']['attachment3_name'] = '';
                }                
            }
            
            $this->Content->id = $this->request->data['Content']['id'];

            switch ($this->request->data['Content']['category_id']) {
                case 17 : $iclass = '10601';
                    break;

                case 18 : $iclass = '10602';
                    break;
                
                default: $iclass = '00000';
                    break;
            }


            if ($this->Content->save($this->request->data)) {

                // CHANGE CATEGORY			
                if (!empty($this->request->data['Content']['sub_category_id'])) {
                    $chkcat = $this->ContentCategory->find('first', array(
                        'conditions' => array(
                            'ContentCategory.content_id' => $this->request->data['Content']['id'],
                            'ContentCategory.deleted' => 'N'
                        ),
                        'recursive' => -1
                    ));
                    //pr($chkcat['ContentCategory']['id']);
                    $this->ContentCategory->id = $chkcat['ContentCategory']['id'];
                    $array_cate = array();
                    $array_cate['ContentCategory']['sub_category_id'] = $this->request->data['Content']['sub_category_id'];
                    $this->ContentCategory->save($array_cate);
                }
                /*************************************************************** */
                /************ check Reader when Notification_type = A ********** */
                if ($this->request->data['Content']['notification_type'] == 'A') {
                    $dataOrgs = array();
                    $dataUsers = array();
                    $data = array();
                    // ContentOrganization
                    $organize = $this->ContentOrganization->find('all', array(
                        'conditions' => array(
                            'ContentOrganization.content_id' => $this->request->data['Content']['id'],
                            "ContentOrganization.deleted" => 'N',
                        )
                    ));

                    foreach ($organize as $organization) {
                        $dataOrgs['ContentOrganization']['deleted'] = 'Y';
                        $dataOrgs['ContentOrganization']['id'] = $organization['ContentOrganization']['id'];
                        $this->ContentOrganization->saveAll($dataOrgs);
                    }

                    // ContentUserReader
                    $userReader = $this->ContentUserReader->find('all', array(
                        'conditions' => array(
                            'ContentUserReader.content_id' => $this->request->data['Content']['id'],
                            "ContentUserReader.deleted != 'Y'",
                        )
                    ));

                    foreach ($userReader as $reader) {
                        $dataUsers['ContentUserReader']['deleted'] = 'Y';
                        $dataUsers['ContentUserReader']['id'] = $reader['ContentUserReader']['id'];
                        $this->ContentUserReader->saveAll($dataUsers);
                    }

                    if ($this->request->data['Content']['is_notification'] == 'Y') {
                        $notidate = Utility::addTime(0, 1,0);
                        $notidate = Utility::cdate($notidate, 'Y-m-d H:i:s', 'Y-m-d H:i');
                        
                        $sendnotidetail = str_replace("&nbsp; ","",$sendnotidetail);
                        
                        $data_noti = array(
                            'time' => $notidate,
                            'to_id13' => 0,
                            'message' => $sendnotidetail,
                            'from_id13' => $currentUser['AuthUserProfile'][0]['officer_code'],
                            'iclass' => '00000',
                            'action' => substr(Configure::read('Portal.Application'), 0, -1) . $this->PortalHelper->makeUrl($this->name, 'view', 'content_id=' . $this->Content->id)
                        );
                        $socket = new HttpSocket();
                        //pr($data_noti); die();
                        $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addAlert', $data_noti);
                    }
                }
                /* ************************************************************** */

                /* ********** check Reader when Notification_type != A ********** */
                if ($this->request->data['Content']['notification_type'] != 'A') {
                    /*********** Query Content Organization ************* */
                    $organize = $this->ContentOrganization->find('all', array(
                        'conditions' => array(
                            'ContentOrganization.content_id' => $this->request->data['Content']['id'],
                            'ContentOrganization.deleted' => 'N',
                        )
                    ));
                    // Check Invite Orgs

                    if (isset($this->request->data['Invite']['Orgs'])) {
                        sort($this->request->data['Invite']['Orgs']);
                        /* [+++++ DELETE ORGS LOOP +++++] */
                        foreach ($organize as $i => $organization) {
                            // Check Duplicate
                            $is_dup = false;
                            foreach ($this->request->data['Invite']['Orgs'] as $j => $organizes) {
                                if ($organization['ContentOrganization']['organization_id'] == $organizes['id']) {
                                    $is_dup = true;
                                    break;
                                }
                            }
                            if (!$is_dup) {
                                $this->ContentOrganization->updateAll(
                                        array(
                                    'ContentOrganization.deleted' => "'Y'"
                                        ), array(
                                    'ContentOrganization.id' => $organization['ContentOrganization']['id']
                                        )
                                );
                            }
                        }
                        /* [+++++ ADD Orgs LOOP +++++] */
                        // Add If Empty
                        if (isset($organize)) {
                            foreach ($this->request->data['Invite']['Orgs'] as $j => $organizes) {
                                $is_dup = false;
                                foreach ($organize as $i => $organization) {
                                    if ($organizes['id'] == $organization['ContentOrganization']['organization_id']) {
                                        $is_dup = true;
                                        break;
                                    }
                                }
                                if ($organizes != null && !$is_dup) {
                                    $org = split(' ', $organizes['organization_name']);
                                    if (count($org) == 1) {
                                        $org_name = $org[0];
                                    } else {
                                        $org_name = $org[1];
                                    }
                                    $data_arr = array();
                                    $this->ContentOrganization->create();
                                    $data_arr['ContentOrganization']['content_id'] = $this->request->data['Content']['id'];
                                    $data_arr['ContentOrganization']['organization_id'] = $organizes['id'];
                                    $data_arr['ContentOrganization']['organization_name'] = $org_name;
                                    $this->ContentOrganization->save($data_arr);
                                }
                            } // end foreach Invite Orgs 
                            
                        }
                        /************************************************ */
                        /****************************** Noti *********************** */
                        if ($this->request->data['Content']['is_notification'] == 'Y') {
                            $showdatanoti = $this->ContentOrganization->find('list', array(
                                'conditions' => array(
                                    'ContentOrganization.content_id' => $this->Content->id,
                                    'ContentOrganization.deleted = ' => 'N',
                                ),
                                'fields' => array('ContentOrganization.organization_id')
                            ));
                            $notiorg = '';

                            $notiorg = implode(',', $showdatanoti);    

                            // send Org to 
                        $notidate = Utility::addTime(0, 1,0);
                        $notidate = Utility::cdate($notidate, 'Y-m-d H:i:s', 'Y-m-d H:i');
                        $sendnotidetail = str_replace("&nbsp; ","",$sendnotidetail);                
                            $data_noti = array(
                            	'time' => $notidate,
                                'to_id' => $notiorg,
                                'message' => $sendnotidetail,
                                'from_id' => $currentUser['AuthUser']['id'],
                                'iclass' => $iclass,
                                'use_suborg' => 'Y',
                                'action' => substr(Configure::read('Portal.Application'), 0, -1) . $this->PortalHelper->makeUrl($this->name, 'view', 'content_id=' . $this->Content->id)
                            );
                            $socket = new HttpSocket();
                            $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addOrgAlert', $data_noti);

                        }
                        // End is notification Organization
                        /**************************************************** */
                    } else { // End Check Invite Orgs
                        $organize = $this->ContentOrganization->find('all', array(
                            'conditions' => array(
                                'ContentOrganization.content_id' => $this->request->data['Content']['id'],
                                'ContentOrganization.deleted' => 'N',
                            )
                        ));
                        if (!empty($organize)) {

                            foreach ($organize as $showorg) {
                                $this->ContentOrganization->updateAll(
                                        array(
                                    'ContentOrganization.deleted' => "'Y'"
                                        ), array(
                                    'ContentOrganization.id' => $showorg['ContentOrganization']['id']
                                        )
                                );
                            }
                        }
                    }

                    /****************************************************** */

                    /***************** Query Content User Reader ************** */
                    $userReader = $this->ContentUserReader->find('all', array(
                        'conditions' => array(
                            'ContentUserReader.content_id' => $this->request->data['Content']['id'],
                            'ContentUserReader.content_type_id' => 1,
                            "ContentUserReader.deleted" => 'N',
                        )
                    ));

                    if (isset($this->request->data['Invite']['Users'])) {
                        sort($this->request->data['Invite']['Users']);
                        /* [+++++ DELETE UserS LOOP +++++] */
                       

                       if(!empty($userReader)){
                            foreach ($userReader as $i => $userread) {
                                $is_dup = false;
                                foreach ($this->request->data['Invite']['Users'] as $j => $userreads) {
                                    if ($userread['ContentUserReader']['user_id'] == $userreads['id']) {
                                        $is_dup = true;
                                        break;
                                    }
                                }

                                if (!$is_dup) {
                                    $this->ContentUserReader->updateAll(
                                            array(
                                        'ContentUserReader.deleted' => "'Y'"
                                            ), array(
                                        'ContentUserReader.id' => $userread['ContentUserReader']['id']
                                            )
                                    );
                                }
                            }
                            /***************** Update User Read Have Duplicate ********************/
                            foreach ($this->request->data['Invite']['Users'] as $j => $userreads) {
                                $is_dup = false;
                                foreach ($userReader as $i => $userread) {

                                    if ($userreads['id'] == $userread['ContentUserReader']['user_id']) { 
                                        $is_dup = true;
                                        break;
                                    }
                                }

                                if ($userreads != null && !$is_dup) {
                                    $data_arr = array();
                                    $data_noti = array();
                                    $this->ContentUserReader->create();
                                    $data_arr['ContentUserReader']['content_id'] = $this->request->data['Content']['id'];
                                    $data_arr['ContentUserReader']['user_id'] = $userreads['id'];
                                    $data_arr['ContentUserReader']['content_type_id'] = 1;
                                    $data_arr['ContentUserReader']['message'] = $this->request->data['Content']['title'];
                                    $data_arr['ContentUserReader']['name'] = $userreads['name'];
                                    $this->ContentUserReader->save($data_arr);
                                    
                                    if ($this->request->data['Content']['is_notification'] == 'Y') {
                                        
                                        $showdatanoti = $this->ContentUserReader->find('list', array(
                                            'conditions' => array(
                                                'ContentUserReader.content_id' => $this->Content->id,
                                                'ContentUserReader.deleted ' => 'N',
                                            ),
                                            'fields' => array('ContentUserReader.user_id')
                                        ));
                                        $notiuser = '';
                                        $notiuser = implode(',', $showdatanoti);
                                        
                                        $notidate = Utility::addTime(0, 1,0);
                                        $notidate = Utility::cdate($notidate, 'Y-m-d H:i:s', 'Y-m-d H:i');
                                        $sendnotidetail = str_replace("&nbsp; ","",$sendnotidetail);
                                        $data_noti = array(
                                            'time' => $notidate,
                                            'to_id' => $notiuser,
                                            'message' => $sendnotidetail,
                                            'from_id' => $currentUser['AuthUser']['id'],
                                            'iclass' => $iclass,
                                            'action' => substr(Configure::read('Portal.Application'), 0, -1) . $this->PortalHelper->makeUrl($this->name, 'view', 'content_id=' . $this->Content->id)
                                        );
                                        $socket = new HttpSocket();
                                        $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);
                                        
                                        
                                    }
                                }else{
                                    if ($this->request->data['Content']['is_notification'] == 'Y') {
                                        $showdatanoti = $this->ContentUserReader->find('list', array(
                                            'conditions' => array(
                                                'ContentUserReader.content_id' => $this->Content->id,
                                                'ContentUserReader.deleted ' => 'N',
                                            ),
                                            'fields' => array('ContentUserReader.user_id')
                                        ));
                                        $notiuser = '';
                                        $notiuser = implode(',', $showdatanoti);
                                        
                                        $notidate = Utility::addTime(0, 1,0);
                                        $notidate = Utility::cdate($notidate, 'Y-m-d H:i:s', 'Y-m-d H:i');
                                        $sendnotidetail = str_replace("&nbsp; ","",$sendnotidetail);
                                        $data_noti = array(
                                            'time' => $notidate,
                                            'to_id' => $notiuser,
                                            'message' => $sendnotidetail,
                                            'from_id' => $currentUser['AuthUser']['id'],
                                            'iclass' => $iclass,
                                            'action' => substr(Configure::read('Portal.Application'), 0, -1) . $this->PortalHelper->makeUrl($this->name, 'view', 'content_id=' . $this->Content->id)
                                        );
                                        $socket = new HttpSocket();
                                        $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);
                                    }
                                }
                                
                            }
                            
                       }else{
                            if(isset($this->request->data['Invite']['Users'])) { 
                                sort($this->request->data['Invite']['Users']);
                                foreach ($this->request->data['Invite']['Users'] as $invite) {
                                    $this->ContentUserReader->create();
                                    $contentReader['ContentUserReader']['content_id'] = $this->Content->id;
                                    $contentReader['ContentUserReader']['content_type_id'] = '1';
                                    $contentReader['ContentUserReader']['message'] = $this->request->data['Content']['title'];
                                    $contentReader['ContentUserReader']['user_id'] = $invite['id'];
                                    $contentReader['ContentUserReader']['name'] = $invite['name'];
                                    $this->ContentUserReader->save($contentReader);
                                }
                                /****************************** Noti *********************** */
                                if ($this->request->data['Content']['is_notification'] == 'Y') {
                                    $showdatanoti = $this->ContentUserReader->find('list', array(
                                        'conditions' => array(
                                            'ContentUserReader.content_id' => $this->Content->id,
                                            'ContentUserReader.deleted ' => 'N',
                                        ),
                                        'fields' => array('ContentUserReader.user_id')
                                    ));
                                    $notiuser = '';
                                    $notiuser = implode(',', $showdatanoti);
                                    $notidate = Utility::addTime(0, 1,0);
                                    $notidate = Utility::cdate($notidate, 'Y-m-d H:i:s', 'Y-m-d H:i');
                                    $sendnotidetail = str_replace("&nbsp; ","",$sendnotidetail);
                                    $data_noti = array(
                                        'time' => $notidate,
                                        'to_id' => $notiuser,
                                        'message' => $sendnotidetail,
                                        'from_id' => $currentUser['AuthUser']['id'],
                                        'iclass' => $iclass,
                                        'action' => substr(Configure::read('Portal.Application'), 0, -1) . $this->PortalHelper->makeUrl($this->name, 'view', 'content_id=' . $this->Content->id)
                                    );
                                    $socket = new HttpSocket();
                                    $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);
                                }
                            }  
                       }    
                         

                        /* ******************************************************** */
                        // Check data['Invite']['Users']
                    } else {
                        // IF NO IT'S MEAN DELETE ALL!!!
                        foreach ($userReader as $userreads) {
                            $this->ContentUserReader->updateAll(
                                    array('ContentUserReader.deleted' => "'Y'"), array('ContentUserReader.id' => $userreads['ContentUserReader']['id'])
                            );
                        }
                    }
                    /* ************************************************************** */
                } // End Notification != A

                /* ************************************************************** */
                $this->redirect($this->PortalHelper->makeUrl($this->name, 'view', 'content_id=' . $this->request->data['Content']['id']));
            } else {
                $this->redirect($this->PortalHelper->makeUrl($this->name, 'index'));
            }
        } else {

            $subcategory = '';
            $lang = 1;
            if (!empty($currentUser['AuthUserProfile'][0]['language'])) {
                $lang = $currentUser['AuthUserProfile'][0]['language'];
            }
            $description = $this->getCategoryName($this->request->query['category_id'], $lang);

            $breadCrumbs = array(
                array(
                    'label' => $description,
                    'link' => array('controller' => $this->name, 'action' => 'viewall', 'query' => 'category_id=' . $this->request->query['category_id'])
                ),
                array(
                    'label' => __('Edit'),
                    'link' => false
                ),
            );
            $this->set('breadCrumbs', $breadCrumbs);


            if (empty($this->request->query['content_id'])) {
                //$this->Session->setFlash(__('Something went wrong you can do a thing like this, trust me'));
                $this->redirect($this->PortalHelper->makeUrl($this->name, 'index'));
            }
            //$content = $this->Content->findById($this->request->query['content_id']);
            $content = $this->ContentCategory->find('first', array(
                'conditions' => array(
                    'Content.deleted' => 'N',
                    'Content.id' => $this->request->query['content_id'],
                ),
            ));

            $publish_date = Utility::cdate($content['Content']['published_date'], 'Y-m-d H:i:s', 'd/m/Y');
            $content['Content']['published_time'] = Utility::cdate($content['Content']['published_date'], 'Y-m-d H:i:s', 'H:i');

            $event_date = Utility::cdate($content['Content']['event_date'], 'Y-m-d H:i:s', 'd/m/Y');
            $content['Content']['event_time'] = Utility::cdate($content['Content']['event_date'], 'Y-m-d H:i:s', 'H:i');

            $expiry_date = Utility::cdate($content['Content']['expiry_date'], 'Y-m-d H:i:s', 'd/m/Y');
            $content['Content']['expiry_time'] = Utility::cdate($content['Content']['expiry_date'], 'Y-m-d H:i:s', 'H:i');
            $content['Content']['title'] = htmlspecialchars_decode($content['Content']['title']);
            $content['Content']['description'] = htmlspecialchars_decode($content['Content']['description']);
            $content['Content']['gist'] = htmlspecialchars_decode($content['Content']['gist']);

            $content['Content']['published_date'] = $publish_date;
            $content['Content']['event_date'] = $event_date;
            $content['Content']['expiry_date'] = $expiry_date;
            if ($content['Content']['is_comment'] == 'N') {
                $content['Content']['is_comment'] = 0;
            } else {
                $content['Content']['is_comment'] = 1;
            }
            if ($content['Content']['is_notification'] == 'N') {
                $content['Content']['is_notification'] = 0;
            } else {
                $content['Content']['is_notification'] = 1;
            }

            $content['Content']['category_id'] = $content['ContentCategory']['category_id'];
            $content['Content']['sub_category_id'] = $content['ContentCategory']['sub_category_id'];

            /* *************************  get Reader *************************** */
            /* **************** Old UserReader ********************** */

            $userread = $this->ContentUserReader->find('all', array(
                'conditions' => array(
                    'ContentUserReader.content_id' => $this->request->query['content_id'],
                    'ContentUserReader.deleted = ' => 'N',
                ),
                'fields' => array('ContentUserReader.user_id', 'ContentUserReader.name')
            ));

            $this->set('OldUserReader', $userread);

            /* **************** Old Organization ********************** */
            $organize = $this->ContentOrganization->find('all', array(
                'conditions' => array(
                    'ContentOrganization.content_id' => $this->request->query['content_id'],
                    'ContentOrganization.deleted = ' => 'N',
                ),
                'fields' => array('ContentOrganization.organization_id', 'ContentOrganization.organization_name')
            ));
            $this->set('OldOrgReader', $organize);

            /* ************************************************************ */


            /************************ Check Category *************************/
            $chkcategory = $this->getCategory->find('first', array(
                'conditions' => array(
                    'getCategory.deleted' => 'N',
                    'getCategory.id' => $this->request->query['category_id']
                )
            ));

            if ($content['ContentCategory']['category_id'] == 3) {
                $subcategory = $this->getCategory->find('list', array(
                    'conditions' => array(
                        'getCategory.deleted' => 'N',
                        'getCategory.parent_id' => $content['ContentCategory']['category_id']
                    ),
                    'fields' => array('getCategory.category_name'),
                    'order' => array('getCategory.id ASC')
                ));
            }
            
            $this->request->data = $content;
            
            $size = Utility::checkfilesize(Configure::read('Config.MaxFileSize'));
            $exten = Configure::read('Eportal.Extension.Allow');
            $this->set('size', $size);
            $this->set('extension', $exten);
            $this->set('subcategory', $subcategory);
            $this->set('chk_comment', $chkcategory['getCategory']['is_comment']);
            $this->set('content', $content);
            $this->set('modelClass', 'Content');
            $this->set('action', 'edit');
            $this->render('/Contents/edit');
       }
    }

    /* ************************************************************************ */

    function delete() {
		
                if(isset($this->request->query['main_cate'])){
                    if(isset($this->request->query['content_id'])) {
                        $data_arr = array();
			$url = $this->PortalHelper->makeUrl($this->name,'viewallsub','main_cate='.$this->request->query['main_cate'].'&category_id='.$this->request->query['category_id']);
			$id = $this->request->query['content_id'];

			$this->ShowListContent->id = $id;
			$data_arr['ShowListContent']['deleted'] = 'Y';
			if($this->ShowListContent->save($data_arr)){
				$this->redirect($url);
			}
                    }else{
			$this->redirect('/main?pc=Contents&pa=index');
                    }
                }else{
                    if(isset($this->request->query['content_id'])) {
                            $data_arr = array();
                            $url = $this->PortalHelper->makeUrl($this->name,'viewall','category_id='.$this->request->query['category_id']);
                            $id = $this->request->query['content_id'];

                            $this->ShowListContent->id = $id;
                            $data_arr['ShowListContent']['deleted'] = 'Y';
                            if($this->ShowListContent->save($data_arr)){
                                    $this->redirect($url);
                            }

                    }else{
                            $this->redirect('/main?pc=Contents&pa=index');
                    }
                }
    }            
    function deleteAttachment(){
        $this->autoRender = false;
        $this->layout ='blank';
        $dataresource = array();
        $datacontent = array();
        
        $key = $this->request->data['id'];
        $content_id = $this->request->data['content_id'];
        $category_id = $this->request->data['category_id'];
        $type = $this->request->data['type'];
        $attach_name = $type.'_name';
        
         
        $this->Content->id = $content_id;
        $datacontent['Content'][$type] = '';
        $datacontent['Content'][$attach_name] = '';        
        if($this->Content->save($datacontent)){
            echo '1';
        }else{
            echo '0';
        }
 
    }
    


    /*
     * View
     * - have content_id ? next step : redirect to Content index;
     * - search for content from content_id;
     * - have content ? nextspec : redirect to Content index;
     * - save view log;
     * - is this content deleted? redirect to Content index : nextstep;
     * - is allow globe user to read? skipnext2step : is owner;
     * - is owner? skipnextstep : nexstep;
     * - is this user allow? nextstep : redirect to Content index ;
     * - view;
     */
    
    function view(){
        $currentUser = $this->Session->read('AuthUser');
        if (empty($this->request->query['content_id'])) {
            $this->redirect('/main?pc=Contents&pa=index');
        }
        
        
        $content = $this->ShowListContent->find('first', array(
            'conditions' => array(
                'ShowListContent.deleted' => 'N',
                'ShowListContent.id' => $this->request->query['content_id']
            ),
            'fields' => array('ShowListContent.id', 'ShowListContent.user_id', 'ShowListContent.title', 'ShowListContent.is_comment',
                'ShowListContent.count_view', 'ShowListContent.count_like', 'ShowListContent.description', 'ShowListContent.expiry_date',
                'ShowListContent.created_date', 'ShowListContent.event_date','ShowListContent.first_name', 'ShowListContent.last_name',
                'ShowListContent.organization_name', 'ShowListContent.belong_organization_name', 'ShowListContent.attachment1',
                'ShowListContent.attachment2', 'ShowListContent.attachment3', 'ShowListContent.attachment1_name', 'ShowListContent.attachment2_name',
                'ShowListContent.attachment3_name', 'ShowListContent.count_comment'
                
            )
        ));
        
        /******************* Add View When Open Contents ******************************/
        
        if(!$this->ContentView->findByContentIdAndUserId($this->request->query['content_id'], $this->Session->read('AuthUser.AuthUser.id'))){
            $this->ShowListContent->id = $this->request->query['content_id'];
            $this->ShowListContent->saveField('count_view', ++$content['ShowListContent']['count_view']);
        }
       
        // START CONTENTVIEW LOG
        $contentView['ContentView']['content_id'] = $this->request->query['content_id'];
        $contentView['ContentView']['user_id'] = $this->Session->read('AuthUser.AuthUser.id');
        $contentView['ContentView']['username'] = $this->Session->read('AuthUser.AuthUser.username');
        $this->ContentView->create();
        $this->ContentView->save($contentView);
        
        /******************************************************************************/
        
        
        if(empty($content)){
          $this->redirect('/main?pc=Contents&pa=index');  
        }

        $lang = 1;
        if(!empty($currentUser['AuthUserProfile'][0]['language'])){
                $lang = $currentUser['AuthUserProfile'][0]['language'];
        }
        $description = $this->getCategoryName($content['ContentCategory'][0]['category_id'], $lang);
        
        $breadCrumbs = array(
            array(
                //'label' => $main['getCategory']['category_name'],
                'label' => $description,
                'link' => array('controller' => $this->name, 'action' => 'viewall', 'query' => 'category_id='.$content['ContentCategory'][0]['category_id'], 'param' => '8')

            ),       
            array(
                'label' => __('View Detail'),
                'link' => false  
            )
        );
      
        $con_like = $this->ContentLike->find('first', array(
            'conditions' => array(
                'ContentLike.content_type_id' => 1,
                'ContentLike.deleted' => 'N',
                'ContentLike.reference_id' => $this->request->query['content_id'],
                'ContentLike.created_user_id' => $currentUser['AuthUser']['id']
            ),
            'fields' => array('ContentLike.id')
        ));
        $content['ShowListContent']['content_liked_id'] = '';
        if(!empty($con_like)){
            $content['ShowListContent']['content_liked_id'] =   $con_like['ContentLike']['id'];
        }
        
        if ($content['ShowListContent']['is_comment'] == 'Y'){
        /************************ Comment *************************************/
        $comments = $this->ContentComment->find('all', array(
            'conditions' => array(
                'ContentComment.content_id = ' . $content['ShowListContent']['id'],
            ),
            'order' => 'ContentComment.id ASC'
        ));
        $this->set('comments', $comments);
        /*********************************************************************/
        }   
        $size = Utility::checkfilesize(Configure::read('Config.MinFileSize'));
        $this->set('filesize', $size);
        $this->set('breadCrumbs', $breadCrumbs);
        $this->set('content', $content);
        $this->render('/Contents/view');
    }
    
    

    function comment() {
        $currentUser = $this->Session->read('AuthUser');
        $lang = 1;
        if(!empty($currentUser['AuthUserProfile'][0]['language'])){
            $lang = $currentUser['AuthUserProfile'][0]['language'];
        }
        if(!empty($this->request->data)){
            $updatecountcomment = $this->Content->find('first', array(
                    'fields' => 'Content.count_comment',
                    'conditions' => array(
                        'Content.id' => $this->request->data['content_id'],    
                    ),
            ));
            /************** Update Count Comment ***********************/
                $count_comment = $updatecountcomment['Content']['count_comment'];
                $count_comment = $count_comment+1;
                $this->Content->updateAll(
                    array('Content.count_comment' => $count_comment), array('Content.id' => $this->request->data['content_id'])
                );
            /************** End Update Count Comment ***********************/
            
            $this->request->data['user_id'] = $currentUser['AuthUser']['id'];
            $this->request->data['description'] = htmlspecialchars($this->request->data['description'], ENT_QUOTES);
            $this->request->data['username'] = $currentUser['AuthUser']['username'];
            $this->request->data['first_name'] = $currentUser['AuthUserProfile'][0]['first_name_th'];
            $this->request->data['last_name'] = $currentUser['AuthUserProfile'][0]['last_name_th'];
            $this->request->data['organization_name'] = $currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthOrganization']['AuthOrganizationDescription'][0]['description'];
            $this->request->data['position_name'] = htmlspecialchars($currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthPosition']['position_name'], ENT_QUOTES);
            /******************************* get Belong ****************************************/
            $data_detail = array();
            $data_detail['list_org_id'] = $currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['AuthOrganization']['parent_id'];
            
            $socket = new HttpSocket();
            $getBelongOrganization = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganizationName/.json', $data_detail);
            $getbelongOrganize = json_decode($getBelongOrganization, true);
            foreach($getbelongOrganize['Result']['OrganizationName'] as $j=>$BelongOrganization){
                if($lang == $BelongOrganization['OrganizationDescription']['language_id']){
                    $desc_belong = $BelongOrganization['OrganizationDescription']['description'];
                    $this->request->data['belong_organization_name'] = $desc_belong;
                    break;
                }
            }
            /***********************************************************************************/  
            
            if (!empty($this->request->data['attachment']['tmp_name'])) {
                $post = $this->request->data['attachment'];
                $tmp_name = $post['tmp_name'];
                $file_name = $post['name'];
                $file_error = $post['error'];
                $file_size = $post['size'];
                $file_type = $post['type'];
                        
                $attach_key = $this->addAttachment($post, $tmp_name, $file_name, $file_error, $file_size, $file_type, $currentUser['AuthUser']['id']);
                //pr($attach_key);
                $this->request->data['attachment_name'] = $attach_key['original_name'];
                $this->request->data['attachment'] = $attach_key['key'];
            }else{
                $this->request->data['attachment'] = '';
                $this->request->data['attachment_name'] = '';
            }
            //pr($this->request->data); die();
            $this->ContentComment->create();
            if($this->ContentComment->save($this->request->data)){
                $this->redirect(
                        $this->PortalHelper->makeURL(
                                $this->name, 'view', 'content_id=' . $this->request->data['content_id'])
                        . '#COMMENT.' . $this->request->data['content_id'] . '.' . $this->ContentComment->id
                );
            }
        }
          /*if ($this->request->is('post')) {
                $category_name = 'Comment';
                $content_type = $this->ContentType->find('first', array(
                      'conditions'=>array(
                          'ContentType.name ' => $category_name,
                          'ContentType.deleted '=> 'N',
                      )
                ));
                $updatecountcomment = $this->Content->find('first', array(
                        'fields' => 'Content.count_comment',
                        'conditions' => array(
                            'Content.id' => $this->request->data['content_id'],    
                        ),
                ));
                 /************** Update Count Comment ***********************/
              /*  $count_comment = $updatecountcomment['Content']['count_comment'];
                $count_comment = $count_comment+1;
                $this->Content->updateAll(
                    array('Content.count_comment' => $count_comment), array('Content.id' => $this->request->data['content_id'])
                );
                /************** End Update Count Comment ***********************/
                /*
                $this->request->data['user_id'] = $currentUser['AuthUser']['id'];
                $this->request->data['description'] = htmlspecialchars($this->request->data['description'], ENT_QUOTES);
                $this->ContentComment->create();
                $this->ContentComment->save($this->request->data);
                
                if (!empty($this->request->data['attachment']['tmp_name'])) {
                    
                        $post = $this->request->data['attachment'];
                        $original_name = $post['name'];
                        $pic_type = $post['type'];
                        $original_size = $post['size'];
                        $code = Utility::uuid();
                        $allowedExts = array(Configure::read('Eportal.Extension.Allow'));
                        $receieve_error = explode(".", $post["name"]);
                        $extension = strtolower(end($receieve_error));
                        if(is_uploaded_file($post['tmp_name']) && ($post['size'] < Configure::read('Config.MinFileSize')) && in_array($extension, $allowedExts)) { 
                                if ($post["error"] > 0) {
                                    echo __("Return Error Code: ") . $post["error"] . "<br>";
                                }else { 
                                    $path = 'attachment/comment/';
                                    if(!file_exists($path)) {
                                        mkdir($path, 0777, true);
                                    }
                                    $uploadSuccess = move_uploaded_file($post["tmp_name"], "../webroot/attachment/comment/" . $code . '.' . $extension);
                                    $filename = $code . '.' . $extension;
                                    if ($uploadSuccess) {
                                         $result = $this->FileStorageComponent->save($path, $code . '.' . $extension, $post['type']);
                                         if ($result) {
                                                $data =array();
                                                $data['ResourceContent']['resource_category_id'] = 0;
                                                $data['ResourceContent']['path'] = $path;
                                                $data['ResourceContent']['content_type'] = $pic_type;
                                                $data['ResourceContent']['original_name'] = $original_name;
                                                $data['ResourceContent']['file_size'] = $original_size;
                                                $data['ResourceContent']['user_id'] = $currentUser['AuthUser']['id'];
                                                $data['ResourceContent']['key'] = $result;
                                                $this->ResourceContent->create();
                                                $this->ResourceContent->save($data);

                                                $data_port = array();
                                                $data_port['PortalAttachment']['reference_id'] = $this->ContentComment->id;;
                                                $data_port['PortalAttachment']['content_type_id'] = $content_type['ContentType']['id'];
                                                $data_port['PortalAttachment']['resource_content_id'] = $this->ResourceContent->getLastInsertID();
                                                $this->PortalAttachment->create();
                                                $this->PortalAttachment->save($data_port);
                                         }
                                    }else{
                                        echo '-1';
                                    }
                                }                         
                        }  
                      
                }
                
                $this->redirect(
                        $this->PortalHelper->makeURL(
                                $this->name, 'view', 'content_id=' . $this->request->data['content_id'])
                        . '#COMMENT.' . $this->request->data['content_id'] . '.' . $this->ContentComment->id
                );
          } */ 

    }
    
    
    function like() {
        
        $this->layout = 'blank';
        $this->autoRender = false;
        $currentUser = $this->Session->read('AuthUser');
        $content_id = $this->request->data['content_id'];
        $comment_id = $this->request->data['comment_id'];
        
        if ($comment_id == 0 || !$comment_id) {
            if (!$content_id) {
                $this->redirect($this->name);
            }else{
               // IS ALREADY LIKE CONTENT!!!
                /*$category_name = 'Content';
                $content_type = $this->ContentType->find('first', array(
                      'conditions'=>array(
                          'ContentType.name ' => $category_name,
                          'ContentType.deleted '=> 'N',
                      )
                ));*/
                
                $content = $this->Content->findById($content_id);
                
                $countcontent_like = $content['Content']['count_like'];
                $countcontent_like = $countcontent_like+1;
               
                $contentLike = array();
                $contentLike['ContentLike']['user_id'] = $currentUser['AuthUser']['id'];
                $contentLike['ContentLike']['content_type_id'] = 1; // 1 for content
                $contentLike['ContentLike']['reference_id'] = $content_id;
                $this->ContentLike->create();
                
                if($this->ContentLike->save($contentLike)){
                    $content_like_last_id = $this->ContentLike->getLastInsertID(); 
                   //Save Like Content and DB
                    $detail = array();
                    $this->Content->id = $content_id;
                    $detail['Content']['count_like'] = $countcontent_like;
                    $this->Content->save($detail); 
//                    $this->Content->updateAll(
//                            array('Content.count_like' =>  $countcontent_like), array('Content.id' => $content_id)
//                    );   
                    
                    
                    //$this->redirect($this->PortalHelper->makeUrl($this->name, 'view', 'content_id=' . $content_id));   
                }
                    $showcountlike = $this->Content->findById($content_id);
                    
                    $returndata['Content']['count_like'] = $showcountlike['Content']['count_like']; 
                    $returndata['Content']['like_id'] = $content_like_last_id;
                    return json_encode($returndata);
            }
        }else{
            // IS ALREADY LIKE COMMENT!!!
            //$category_name = 'Comment';
            /*$content_type = $this->ContentType->find('first', array(
                  'conditions'=>array(
                      'ContentType.name ' => $category_name,
                      'ContentType.deleted '=> 'N',
                  )
            ));*/
            $comments = $this->ContentComment->findById($comment_id);
            $comment_like = $comments['ContentComment']['count_like'];
            $comment_like = $comment_like+1;
            
            $contentLike = array();
            $contentLike['ContentLike']['user_id'] = $currentUser['AuthUser']['id'];
            $contentLike['ContentLike']['content_type_id'] = 3; // 3 for comment
            $contentLike['ContentLike']['reference_id'] = $comment_id;
            if($this->ContentLike->save($contentLike)){
               $comment_like_last_id = $this->ContentLike->getLastInsertID(); 
               $this->ContentComment->updateAll(
             
                    array('ContentComment.count_like' => $comment_like), array('ContentComment.id' => $comment_id)
               );
                /*$this->redirect(
                        $this->PortalHelper->makeURL(
                                $this->name, 'view', 'content_id=' . $content_id)
                        . '#COMMENT.' . $content_id . '.' . $comment_id
                );*/
                
            }
                $showcountcommentlike = $this->ContentComment->findById($comment_id);
                $returndata['ContentComment']['count_like'] = $showcountcommentlike['ContentComment']['count_like']; 
                $returndata['ContentComment']['like_id'] = $comment_like_last_id;
                return json_encode($returndata);
        }
        
    }

    function unlike() {
        $this->layout = 'blank';
        $this->autoRender = false;
        $currentUser = $this->Session->read('AuthUser');
        $content_id = $this->request->data['content_id'];
        $contentLike_id = $this->request->data['like_id'];
        $comment_id = $this->request->data['comment_id'];
        
        if ($content_id == null || $contentLike_id == null) {
            $this->redirect($this->name);
        } elseif ($comment_id == null || $comment_id == 0) {
              $content = $this->Content->findById($content_id);
              $countcontent_like = $content['Content']['count_like'];
              $countcontent_like = $countcontent_like-1;
              $this->ContentLike->updateAll(
                     array('ContentLike.deleted' => "'Y'"), array('ContentLike.id' => $contentLike_id)
              );
              $this->Content->updateAll(
                     array('Content.count_like' => $countcontent_like), array('Content.id' => $content_id)
              );
              
                    $showcountlike = $this->Content->findById($content_id);
                    $returndata['Content']['count_like'] = $showcountlike['Content']['count_like'];
                    $returndata['Content']['deleted'] = $showcountlike['Content']['deleted'];
                    $returndata['Content']['like_id'] = $contentLike_id;
                    //return json_encode($returndata); 
              //$this->redirect($this->PortalHelper->makeUrl($this->name, 'view', 'content_id=' . $content_id));
        } elseif ($content_id != null && $contentLike_id != null && $comment_id != null) {
              $comments = $this->ContentComment->findById($comment_id);
              $comment_like = $comments['ContentComment']['count_like'];
              $comment_like = $comment_like-1;
              $this->ContentLike->updateAll(
                      array('ContentLike.deleted' => "'Y'"), array('ContentLike.id' => $contentLike_id)
              );
              $this->ContentComment->updateAll(
                 array('ContentComment.count_like' => $comment_like), array('ContentComment.id' => $comment_id)
              );
              
                    $showcountcommentlike = $this->ContentComment->findById($comment_id);
                    $returndata['ContentComment']['count_like'] = $showcountcommentlike['ContentComment']['count_like'];  
                    $returndata['ContentComment']['like_id'] = $contentLike_id;
              /*$this->redirect(
                  $this->PortalHelper->makeURL(
                          $this->name, 'view', 'content_id=' . $content_id)
                  . '#COMMENT.' . $content_id . '.' . $comment_id
              );*/
        } else {
           $this->redirect('/main?pc=' . $this->name); 
        }
        return json_encode($returndata);
    }

    function seenby($content_id = null) {
        $this->ContentComment->create();
        $this->ContentComment->data['ContentComment']['content_id'] = $content_id;
//        $this->ContentComment->data['ContentComment']['updated_user_id'] = 1;
//        pr($this->ContentComment->data);
//        $this->ContentComment['ContentComment']['content_id'] = $content_id;
        $this->ContentComment->save();
        $this->layout = 'blank';
    }
    
    
    function getFirstDatetimeInThisWeek(){
        $day_of_the_week = date("N");
        $first_datetime_in_this_week = date("Y-m-d 00:00:00", (time() - (60 * 60 * 24 * $day_of_the_week)));
        
        return $first_datetime_in_this_week;
    }
    
    function viewall(){
        
		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];
		$offset = 10;
		$limit = 10;
		$has_expire = 'N';
		$lang = 1;
		if(!empty($currentUser['AuthUserProfile'][0]['language'])){
			$lang = $currentUser['AuthUserProfile'][0]['language'];
		}
		$description = $this->getCategoryName($this->request->query['category_id'], $lang);
		$allow_content = $this->getAllowContentId();
		$checkallow = '';
		if(!empty($allow_content)){
			$checkallow = 'getContentCategory.content_id in (' . $allow_content . ')';
		}

                $breadCrumbs = array(
                    array(
                        'label' => $description,
                        'link' => false
                    ),
                );
		
		$countallcontent = $this->getContentCategory->find('count', array(
			'conditions' => array(
			   'getContentCategory.category_id'=>$this->request->query['category_id'],
			   'Content.deleted' => 'N',
			   'OR' => array(
					'Content.notification_type = \'A\'',
					'Content.user_id = ' . $this->Session->read('AuthUser.AuthUser.id'),
					$checkallow
			   ),
			),
			//'fields' => array('ContentCategory.content_id'),
			'order' => array('Content.event_date'=>'DESC'),
			'recursive' => 3
		));



		$getContentCategory = $this->getContentCategory->find('all', array(
			'conditions' => array(
				'getContentCategory.category_id'=>$this->request->query['category_id'],
				'Content.deleted' => 'N',
				'OR' => array(
					'Content.notification_type = \'A\'',
					'Content.user_id = ' . $user_id,
					$checkallow
				)
			),
                        'fields' => array('getContentCategory.category_id', 'Content.id', 'Content.event_date', 
                            'Content.title', 'Content.gist', 'Content.organization_name', 'Content.belong_organization_name',
                            'Content.created_date', 'Content.user_id', 'Content.count_view', 'Content.is_comment',
                            'Content.count_comment', 'Content.first_name', 'Content.last_name', 'Content.attachment1', 'Content.attachment2', 'Content.attachment3', 
                        'Content.attachment1_name', 'Content.attachment2_name', 'Content.attachment3_name'
                        ),
			'order' => array('Content.event_date'=>'DESC'),
			'recursive' => 3,
			'limit' => $limit
		));
		
		$this->request->data['category_id'] = $this->request->query['category_id'];
		$this->set('countContent', empty($countallcontent) ? 0 : $countallcontent);
		$this->set('contents', $getContentCategory);
		$this->set('offset', $offset);
		$this->set('limit', $limit);
                $this->set('has_expire', $has_expire);
		$this->set('breadCrumbs', $breadCrumbs);
                $this->render('/Contents/viewall');
	}

    function getCategoryName($category_id, $lang_id){
        $category = $this->CategoryDescription->find('first', array(
            'conditions'=>array('CategoryDescription.category_id'=>$category_id, 'CategoryDescription.language_id'=> $lang_id), 
            'fields'=>array('description')
        ));
        
        return @$category['CategoryDescription']['description'];
    }

   
    function search(){
        $this->layout = 'blank';
        $offset = 10;
        $limit = 10;
        $period_condition =  '';  
        $showlist_conditions = '';
        $currentUser = $this->Session->read('AuthUser');
        $user_id = $currentUser['AuthUser']['id'];
        $allow_content = $this->getAllowContentId();
        $checkallow = '';
        if(!empty($allow_content)){
          $checkallow = 'getContentCategory.content_id in (' . $allow_content . ')';
        }
        $data = $this->request->data;
        switch($data['sort']){
            case 0 :  $order = 'created_date'; break;
            case 1 :  $order = 'event_date'; break;
            default : $order = 'created_date'; break;
        }
        switch($data['period']){
                       // Last Month
            case 1 :  $start_period = date('Y-m-d', strtotime('first day of last month')); // sun to sat they said
                      $end_period = date('Y-m-d', strtotime('first day of this month'));
                      $start = $start_period." 00:00:00";
                      $end = $end_period." 00:00:00";
                      $period_condition =  "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      $showlist_conditions = "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      break;
                      
                       // Last Week
            case 2 :  $start_period = date('Y-m-d', strtotime('last week -1 day')); // sun to sat they said
                      $end_period = date('Y-m-d', strtotime('this week -1 day'));
                      $start = $start_period." 00:00:00";
                      $end = $end_period." 00:00:00";
                      $period_condition =  "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      $showlist_conditions = "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      break;
                      
                      // This Week
            case 3 :  $start_period = date('Y-m-d', strtotime('this week -1 day'));
                      $end_period = date('Y-m-d', strtotime('next week'));
                      $start = $start_period." 00:00:00";
                      $end = $end_period." 00:00:00";
                      $period_condition =  "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      $showlist_conditions = "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      break;
            
                      // This Month
            case 4 :  $start_period = date('Y-m-d', strtotime('first day of this month'));
                      $end_period = date('Y-m-d', strtotime('first day of next month'));
                      $start = $start_period." 00:00:00";
                      $end = $end_period." 00:00:00";
                      $period_condition =  "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      $showlist_conditions = "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      break;
                  
                      //Next Week
            case 5 :  $start_period = date('Y-m-d', strtotime('next week - 1 day'));
                      $end_period = date('Y-m-d', strtotime('next week + 1 week - 1 day'));
                      $start = $start_period." 00:00:00";
                      $end = $end_period." 00:00:00"; 
                      $period_condition =  "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      $showlist_conditions = "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      break;
                  
                      //Next Month
            case 6 :  $start_period = date('Y-m-d', strtotime('first day of next month'));
                      $end_period = date('Y-m-d', strtotime('first day of next month + 1 month'));
                      $start = $start_period." 00:00:00";
                      $end = $end_period." 00:00:00";
                      $period_condition =  "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      $showlist_conditions = "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      break;
                  
            default:  //All    
                      $period_condition =  '';  
                      $showlist_conditions = '';
                      break;    
        }
        
        $countallcontent = $this->getContentCategory->find('count', array(
                'conditions' => array(
                   'getContentCategory.category_id'=>$data['category_id'],
                   "Content.title ILIKE '%".trim($data['keyword'])."%'",
                   'Content.deleted' => 'N',
                   $period_condition,
                   $showlist_conditions, 
                   'OR' => array(
                        'Content.notification_type = \'A\'',
                        'Content.user_id = ' . $user_id,
                        $checkallow
                   ),
                ),
                //'fields' => array('ContentCategory.content_id'),
                'order' => array('Content.'.$order=>'DESC'),
                'recursive' => 3
        ));
        

        
        $getContentCategory = $this->getContentCategory->find('all', array(
                'conditions' => array(
                   'getContentCategory.category_id'=>$data['category_id'],
                   "Content.title ILIKE '%".trim($data['keyword'])."%'",
                   'Content.deleted' => 'N',
                   $period_condition,
                   $showlist_conditions, 
                   'OR' => array(
                        'Content.notification_type = \'A\'',
                        'Content.user_id = ' . $user_id,
                        $checkallow
                   ),
                ),
                'fields' => array('getContentCategory.category_id', 'Content.id', 'Content.event_date', 
                    'Content.title', 'Content.gist', 'Content.organization_name', 'Content.belong_organization_name',
                    'Content.created_date', 'Content.user_id', 'Content.count_view', 'Content.is_comment',
                    'Content.count_comment', 'Content.first_name', 'Content.last_name', 'Content.attachment1', 'Content.attachment2', 'Content.attachment3', 
                        'Content.attachment1_name', 'Content.attachment2_name', 'Content.attachment3_name'
                ),
                'order' => array('Content.'.$order=>'DESC'),
                'recursive' => 3,
                'limit' => $limit
        ));
       // pr($getContentCategory);
        $this->request->data['category_id'] = $data['category_id'];
        $this->set('category_id', $data['category_id']);
        $this->set('countContent', empty($countallcontent) ? 0 : $countallcontent);
        $this->set('contents', $getContentCategory);
        $this->set('period', $data['period']);
        $this->set('keyword', $data['keyword']);
        $this->set('sort', $order);
        $this->set('offset', $offset);
        $this->set('limit', $limit);
        $this->render('/Contents/search');
        
    }
    
    function viewmoresearch(){
        $this->layout = 'blank';
        
        $period_condition =  '';  
        $showlist_conditions = '';
        $currentUser = $this->Session->read('AuthUser');
        $user_id = $currentUser['AuthUser']['id'];
        $allow_content = $this->getAllowContentId();
        $checkallow = '';
        if(!empty($allow_content)){
          $checkallow = 'getContentCategory.content_id in (' . $allow_content . ')';
        }
        $data = $this->request->data;
        
        switch($data['order']){
            case 0 :  $order = 'created_date'; break;
            case 1 :  $order = 'event_date'; break;
            default : $order = 'created_date'; break;
        }
        switch($data['period']){
                       // Last Month
            case 1 :  $start_period = date('Y-m-d', strtotime('first day of last month')); // sun to sat they said
                      $end_period = date('Y-m-d', strtotime('first day of this month'));
                      $start = $start_period." 00:00:00";
                      $end = $end_period." 00:00:00";
                      $period_condition =  "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      $showlist_conditions = "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      break;
                      
                       // Last Week
            case 2 :  $start_period = date('Y-m-d', strtotime('last week -1 day')); // sun to sat they said
                      $end_period = date('Y-m-d', strtotime('this week -1 day'));
                      $start = $start_period." 00:00:00";
                      $end = $end_period." 00:00:00";
                      $period_condition =  "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      $showlist_conditions = "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      break;
                      
                      // This Week
            case 3 :  $start_period = date('Y-m-d', strtotime('this week -1 day'));
                      $end_period = date('Y-m-d', strtotime('next week'));
                      $start = $start_period." 00:00:00";
                      $end = $end_period." 00:00:00";
                      $period_condition =  "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      $showlist_conditions = "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      break;
            
                      // This Month
            case 4 :  $start_period = date('Y-m-d', strtotime('first day of this month'));
                      $end_period = date('Y-m-d', strtotime('first day of next month'));
                      $start = $start_period." 00:00:00";
                      $end = $end_period." 00:00:00";
                      $period_condition =  "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      $showlist_conditions = "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      break;
                  
                      //Next Week
            case 5 :  $start_period = date('Y-m-d', strtotime('next week - 1 day'));
                      $end_period = date('Y-m-d', strtotime('next week + 1 week - 1 day'));
                      $start = $start_period." 00:00:00";
                      $end = $end_period." 00:00:00"; 
                      $period_condition =  "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      $showlist_conditions = "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      break;
                  
                      //Next Month
            case 6 :  $start_period = date('Y-m-d', strtotime('first day of next month'));
                      $end_period = date('Y-m-d', strtotime('first day of next month + 1 month'));
                      $start = $start_period." 00:00:00";
                      $end = $end_period." 00:00:00";
                      $period_condition =  "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      $showlist_conditions = "Content.event_date >= '".$start."' AND Content.event_date < '".$end."'";
                      break;
                  
            default:  //All    
                      $period_condition =  '';  
                      $showlist_conditions = '';
                      break;    
        }
        $getContentCategory = $this->getContentCategory->find('all', array(
                'conditions' => array(
                   'getContentCategory.category_id'=>$data['category_id'],
                   "Content.title ILIKE '%".trim($data['keyword'])."%'",
                   'Content.deleted' => 'N',
                   $period_condition,
                   $showlist_conditions, 
                   'OR' => array(
                        'Content.notification_type = \'A\'',
                        'Content.user_id = ' . $user_id,
                        $checkallow
                   ),
                ),
                'fields' => array('getContentCategory.category_id', 'Content.id', 'Content.event_date', 
                    'Content.title', 'Content.gist', 'Content.organization_name', 'Content.belong_organization_name',
                    'Content.created_date', 'Content.user_id', 'Content.count_view', 'Content.is_comment',
                    'Content.count_comment', 'Content.first_name', 'Content.last_name', 'Content.attachment1', 'Content.attachment2', 'Content.attachment3', 
                        'Content.attachment1_name', 'Content.attachment2_name', 'Content.attachment3_name'
                ),
                'order' => array('Content.'.$data['order']=>'DESC'),
                'recursive' => 3,
                'offset' => $data['offset'],
                'limit' => $data['limit']
        ));
        $this->request->data['category_id'] = $data['category_id'];
        $this->set('category_id', $data['category_id']);
        $this->set('contents', $getContentCategory);
        $this->render('/Contents/viewmoresearch');
    }
    
    

        /******************* View more Click ******************************/
        public function viewmore(){
            $this->layout = 'blank';
            $currentUser = $this->Session->read('AuthUser');
            $user_id = $currentUser['AuthUser']['id'];
            $offset = $_POST['offset'];
            $limit = $_POST['limit'];
            $category_id = $_POST['category_id'];
            $chk_expire = $_POST['expire'];

            $expire = '';
            if($chk_expire == 'Y'){
                $expire = 'Content.expiry_date >= \'' . date('Y-m-d') . ' \'';
            }
            $checkallow = '';
            $allow_content = $this->getAllowContentId();
            if(!empty($allow_content)){
                $checkallow = 'getContentCategory.content_id in (' . $allow_content . ')';
            }
            $getContentCategory = $this->getContentCategory->find('all', array(
                    'conditions' => array(
                            'getContentCategory.category_id'=>$category_id,
                            'Content.deleted' => 'N',
                            $expire,
                            'OR' => array(
                                    'Content.notification_type = \'A\'',
                                    'Content.user_id = ' . $user_id,
                                    $checkallow,
                                    
                            )
                    ),
                    'fields' => array('getContentCategory.category_id', 'Content.id', 'Content.event_date', 
                        'Content.title', 'Content.gist', 'Content.organization_name', 'Content.belong_organization_name',
                        'Content.created_date', 'Content.user_id', 'Content.count_view', 'Content.is_comment',
                        'Content.count_comment', 'Content.first_name', 'Content.last_name', 'Content.attachment1', 'Content.attachment2', 'Content.attachment3', 
                        'Content.attachment1_name', 'Content.attachment2_name', 'Content.attachment3_name'
                    ),
                    'order' => array('Content.event_date'=>'DESC'),
                    'recursive' => 3,
                    'offset' => $offset,
                    'limit' => $limit
            ));
            
            //$this->set('countContent', empty($countallcontent) ? 0 : $countallcontent);
            $this->set('category_id', $category_id);
            $this->set('contents', $getContentCategory);
            $this->set('offset', $offset);
            $this->set('limit', $limit);
            //$this->set('breadCrumbs', $breadCrumbs);
            $this->render('/Contents/viewmore');
        }
        /******************************************************************/
    
	/***************************** Add Attachment *********************/
        function addAttachment($attachment, $tmp_name, $file_name, $file_error, $file_size, $file_type, $user_id){

            $result['original_name'] = $file_name;
            $result['pic_type'] = $file_type;
            $result['key'] = '';
            
            $code = Utility::uuid();
            $allowedExts = array('doc', 'docx', 'txt', 'pdf', 'jpg', 'jpeg', 'png', 'gif', 'xls', 'xlsx', 'ppt', 'pptx', 'bmp', 'zip', 'rar', '7z', 'swf');
            $receieve_error = explode(".", $file_name);
            $extension = strtolower(end($receieve_error));
            if(is_uploaded_file($tmp_name) && ($file_size < Configure::read('Config.MaxFileSize')) && in_array($extension, $allowedExts)) { 
                    if ($file_error > 0) {
                        echo __("Return Error Code: ") . $file_error . "<br>";
                    }else { 
                        $path = 'attachment/content/';
                        if(!file_exists($path)) {
                            mkdir($path, 0777, true);
                        }
                        $uploadSuccess = move_uploaded_file($tmp_name, $path . $code . '.' . $extension);
                        $filename = $code . '.' . $extension;
                        if ($uploadSuccess) {
                             $results = $this->FileStorageComponent->save($path, $code . '.' . $extension, $file_type);
                             if ($results) {
                                    /*$data =array();
                                    $data['ResourceContent']['resource_category_id'] = 0;
                                    $data['ResourceContent']['path'] = $path;
                                    $data['ResourceContent']['content_type'] = $file_type;
                                    $data['ResourceContent']['original_name'] = $file_name;
                                    $data['ResourceContent']['file_size'] = $file_size;
                                    $data['ResourceContent']['user_id'] = $user_id;
                                    $data['ResourceContent']['key'] = $results;
                                    $this->ResourceContent->create();
                                    $this->ResourceContent->save($data);*/
                                        
                                    $result['key'] = $results;
                             }
                        }else{
                            echo  '-1';
                           $result['key'] = '';
                        }
                    }                         
            } 
            
            return $result;
        }
        /******************************************************************/

	
	
}