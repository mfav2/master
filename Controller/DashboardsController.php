<?php
App::uses('AppController', 'Controller');

class DashboardsController extends AppController {

	public $name = 'Dashboards';
	public $uses = array();
	public $components = array('PortalHelper');
	public $helpers = array('Portal', 'Session');

	public function index() {
            $this->layout = 'dashboard';
            //$url = $this->PortalHelper->makeUrl('Contents', 'index');
            //$this->PortalHelper->PortalRedirect($url);
	}

	public function under() {
            $this->layout = 'dashboard';
            //$url = $this->PortalHelper->makeUrl('Contents', 'index');
            //$this->PortalHelper->PortalRedirect($url);
	}
}
