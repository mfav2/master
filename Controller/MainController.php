<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');

class MainController extends AppController {

	public $components = array("PortalHelper");
	public $helpers = array('Portal');
	public $uses = array('Language');
        
	function index() {

		$currentUser = $this->Session->read('AuthUser');
                $socket = new HttpSocket();
                $data = array();
                $data['language_id'] = $currentUser['AuthUserProfile'][0]['language'];
                $languageResult = $socket->post(Configure::read('Config.CenterBaseAPI.EndPoint') . 'getLanguage/.json', $data);
		$langResult = json_decode($languageResult, true);
                //pr($langResult);
                if (empty($langResult['Result']['Error'])) {
                    $language = $langResult['Result']['Language'];
                }
                
		if(empty($currentUser)){
                    $this->logout();
		}
                
                
                if(!empty($language)){
                    $this->Session->write('Config.language', $language['Language']['directory']);
                }
                
                
		$requestParams = array(
			"portalController"=>(!empty($this->request->query["pc"])? $this->request->query["pc"]: "Contents"),
			"portalAction"=>(!empty($this->request->query["pa"])? $this->request->query["pa"]: "index"),
			"portalQuery"=>(!empty($this->request->query["pq"])? base64_decode($this->request->query["pq"]): ""),
			"portalParam"=>(!empty($this->request->query["pr"])? base64_decode($this->request->query["pr"]): "")
		);

		$control_name = (!empty($this->request->query["pc"])? $this->request->query["pc"]: "Contents");
		$action_name = (!empty($this->request->query["pa"])? $this->request->query["pa"]: "index");
		$this->auditUser($currentUser,$control_name,$action_name);
                
		$content = $this->PortalHelper->render($requestParams);
//		$this->layout = 'main';
		if($this->request->query["pc"] == 'Dashboards'){
			$this->request->query['layout'] = 'dashboard';
		}
		$this->layout = (!empty($this->request->query['layout'])? $this->request->query['layout']: 'main');
		$this->set("content", $content);
		
	}

	function token($token_code = ''){

		$this->layout = 'blank';
		$this->autoRender = false;

		$data["token_code"] = $token_code;
		$data["server_id"]  = Configure::read('Application.ServerId');
		$data["application_id"] = Configure::read('Application.ApplicationId');
		$data["client_ip"] = $this->request->clientIp();

		$socket = new HttpSocket();
		$authenticateResult = $socket->post(Configure::read('Config.AuthBaseAPI.EndPoint') . 'authenticateToken/.json', $data);
		$authResult = json_decode($authenticateResult, true);

		if (empty($authResult['Result']['Error'])) {
 
			$authUser = $authResult['Result']['User'];
			
			$data_appt['user_id'] = $authUser['AuthUser']['id'];
			$appointmentResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getAppointment/.json', $data_appt);
			$appointment =  json_decode($appointmentResult, true);
			//pr($appointment);
			$authUser['Appointment'] = $appointment['Result']['Appointment'];
			$this->Session->Write('AuthUser', $authUser);
			$currentUser = $this->Session->read('AuthUser');
		
			$url = $this->PortalHelper->makeUrl('Dashboards', 'index');
//			$this->PortalHelper->PortalRedirect($url);
                        echo '<script type="text/javascript">window.location=\'' . $url . '\'</script>';

		}else{
			$this->logout();
		//	$url = $this->PortalHelper->makeUrl('logins', '');
		//	$this->PortalHelper->PortalRedirect($url);
		//	pr($authResult['Result']['Error']);
		
		}
	
	}

	function logout($module=''){
		
		$currentUser = $this->Session->read('AuthUser');
		$control_name = 'Logout';
		$action_name = 'index';
		$this->auditUser($currentUser,$control_name,$action_name);
		$this->_clear();
		$this->_logout($module);
	}

	function auditUser($currentUser,$control_name,$action_name){

		$data["server_id"]  = Configure::read('Application.ServerId');
		$data["user_id"] = $currentUser['AuthUser']['id'];
		//$data["organization_id"]  = Configure::read('Application.ServerId');
		$data["module_name"]  = Configure::read('Application.ModuleName');
		$data["control_name"] = $control_name;
		$data["action_name"] = $action_name;
		$data["token_id"] = $currentUser['AuditToken']['token_id'];

		$socket = new HttpSocket();
		$auditResult = $socket->post(Configure::read('Config.AuthBaseAPI.EndPoint') . 'auditUser/.json', $data);

	}

	function _clear() {

		$this->Session->delete('AuthUser');
		$this->Session->destroy();

	}

	function _logout($module=''){
	
		//pr($this->Session->read('AuthUser'));
		//die();
		$url = Configure::read('Gateway.Application').'logout/remoteLogout/'.$module;
		$this->PortalHelper->PortalRedirect($url);

	}

	function remoteLogout(){

		$this->_logout();

	}	
}
?>
