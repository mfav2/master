<?php

App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('Utility', 'mfa');
/**
 * Highlevels Controller
 *
 * @propertyHighlevel $Highlevel
 */
class HighlevelsController extends AppController {

    public $name = 'Highlevels';
    public $uses = array("Appointment", 'AppointmentUserReader', "Highlevel", "HighlevelGroup");
    public $components = array('PortalHelper');
    public $helpers = array('Form', 'Html', 'Portal', 'Session');
    
    /*
     * This function will return $user[$user_id] = $first_name_th $last_name_th
     */
    public function getUserNameFromList($user_list){ // array $user_list
        
        if(!is_array($user_list)){
            $user_list = array($user_list);
        }
        
        $socket = new HttpSocket();
        $ulist['list_user_id'] = implode(",", $user_list);
        $getUserProfileResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserHighLevel/.json', $ulist);
        $getUserProfile = json_decode($getUserProfileResult, true);

        $user = array();
        if(empty($getUserProfile)){ 
            return false; 
        }else{
            if(!empty($getUserProfile['Result']['Error'])){
                return false;
            }
           
            foreach($getUserProfile['Result']['UserProfile'] AS $i => $u){
                $uid = $u['UserProfile']['user_id'];
                    $position = '';
		    $organization = '';
                if(!empty($u['UserOrganizationPosition'][0]['OrganizationPosition']['Position']['position_name'])){
                    $position = $u['UserOrganizationPosition'][0]['OrganizationPosition']['Position']['position_name'];
                }
                if(!empty($u['UserOrganizationPosition'][0]['OrganizationPosition']['Organization'])){
                    $organization = $u['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['organization_short_name'];
                }
                
                $uname = $u['UserProfile']['first_name_th'] . " " . $u['UserProfile']['last_name_th'] . " [{$position}]";
                
                $user[$uid]['id'] = $uid;
                $user[$uid]['detail'] = $uname;
		$user[$uid]['first_name_th'] = $u['UserProfile']['first_name_th'];
		$user[$uid]['last_name_th'] = $u['UserProfile']['last_name_th'];
		$user[$uid]['position'] = $position;
                $user[$uid]['organization'] = $organization;
                
            }
            
            return $user;
        }
    }
    
    
    /*public function highlevelindex() {
        $this->Session->write('pc', $this->request->query['pc']);
        $this->Session->write('pa', $this->request->query['pa']);
        if(!empty($this->request->query['pq'])){
            $this->Session->write('pq', base64_decode($this->request->query['pq']));
        }else{
            $this->Session->delete('pq');
        }
        if(!empty($this->request->query['pr'])){
           $this->Session->write('pr', base64_decode($this->request->query['pr'])); 
        }else{
            $this->Session->delete('pr');
        }
        //$this->layout = 'form';
        $breadCrumbs = array(
            array(
                'label' => __('High Level Schedule'),
                'link' => false
            )
        );
        $this->set("breadCrumbs", $breadCrumbs);
        
        $highlevel_group = $this->HighlevelGroup->find('list', array( 
                'fields' => array('HighlevelGroup.id', 'HighlevelGroup.highlevel_group_name'),
                'conditions' => array('HighlevelGroup.deleted' => 'N'), 
        ));
        $this->set('highlevel_group', $highlevel_group);
        
//        include 'Metadata/Highlevels/highlevelform.ctp';
//        $this->set("viewDefs", $viewDefs);pr()


        if(empty($this->request->data)){
                $highlevel_group_id = $this->request->query['high_level_group'];
                $selected_year = date('Y');
                $tmp_selected_month = date('m');
                $selected_month = $tmp_selected_month < 10 ? "{$tmp_selected_month}" : $tmp_selected_month;
        }else{
                $highlevel_group_id = $this->request->data['Highlevels']['high_level_group'];
                $selected_year = $this->request->data['Highlevels']['selected_year'];
                $tmp_selected_month = $this->request->data['Highlevels']['selected_month'];
                $selected_month = $tmp_selected_month < 10 ? "{$tmp_selected_month}" : $tmp_selected_month;
        }
		/***************************************************/
	/*	$days_in_month = date("t", mktime(0, 0, 0, $selected_month, 1, $selected_year));
		
                $begin_date = $selected_year."-".$selected_month."-01 00:00";
                $last_date  = $selected_year."-".$selected_month."-".$days_in_month." 23:59";
                $date_range = array("$begin_date", "$last_date");
          
            //$date_range = array("$selected_year-$selected_month-01", "$selected_year-$selected_month-$days_in_month");

            $ulist = $this->Highlevel->find('list', array(
                'conditions'=>array('Highlevel.highlevel_group_id'=>$highlevel_group_id, 'Highlevel.deleted'=>'N'),
                'fields'=>array('Highlevel.user_id'),
                'order' => 'Highlevel.sort_order ASC',
            ));
            
            $user_list = array();
            foreach($ulist AS $id => $user_id){
                $user_list[] = $user_id;
            }
            $this->set('user_list', $user_list);
            $user = $this->getUserNameFromList($user_list);
            
            $this->set('user_detail', $user);
            //// =========================
            $tmp_appointments = $this->Appointment->find('all', array(
                'conditions'=>array(
                    'Appointment.privacy_type_id'=>2,
                    'Appointment.deleted'=>'N',
                    'Appointment.user_id'=>$user_list,
                    'Appointment.user_id <>'=>0,
                    'OR'=>array(
                        'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                        'Appointment.end_date BETWEEN ? AND ?'=>$date_range
                    )
                ),
                'fields'=>array(
                    'Appointment.id', 'Appointment.start_date', 'Appointment.end_date', 'Appointment.location', 
                    'Appointment.location_type_id', 'Appointment.detail', 'Appointment.user_id', 'Appointment.name'
                ),
                'order'=>array('Appointment.user_id'=>'ASC', 'Appointment.start_date'=>'ASC')
            ));
            
            $appointments = array();
            foreach($tmp_appointments AS $i => $app){
                $app = $app['Appointment'];
                $id = $app['id'];
                $uid = $app['user_id'];

                if(empty($last_user_id)){ 
                    $last_user_id = $app['user_id']; 
                    
                    $appointments[$uid]['id'] = $id;
                    $appointments[$uid]['user_full_name'] = $user[$uid]['detail'];
                    $appointments[$uid]['user_id'] = $uid;
                    $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                }else{              
                    if($last_user_id == $app['user_id']){ /// same as last loop
                        $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                        $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                        $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                        $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                        $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                        //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                    }else{ /// new user
                        $appointments[$uid]['id'] = $id;
                        $appointments[$uid]['user_id'] = $uid;
                        $appointments[$uid]['user_full_name'] = $user[$uid]['detail'];;
                        $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                        $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                        $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                        $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                        //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                        $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                    }
                }
            }
            
            $this->set('appointments', $appointments);
            $this->set('selected_month', $selected_month);
            $this->set('selected_year', $selected_year);
		/***************************************************/
   /* }*/
    
    public function highlevelindex(){
        $currentUser = $this->Session->read('AuthUser');
        //pr($currentUser);
        $permisssubscribe = $currentUser['PermissionSubscribe'];
        $chk_hi_group = 'HighlevelGroup.id != 2';
        if(!empty($permisssubscribe)){
            foreach($permisssubscribe as $subscrib){
                if($subscrib['GroupSubscribe']['function_id'] == 2){
                    if(isset($subscrib['GroupSubscribe']['detail'])){
                       $chk_hi_group = '';
                    }
                }
            }
        }else{
            $chk_hi_group = 'HighlevelGroup.id != 2';
        } 
        
        

        $breadCrumbs = array(
            array(
                'label' => __('High Level Schedule'),
                'link' => false
            )
        );
        
        $highlevel_group = $this->HighlevelGroup->find('list', array( 
                'fields' => array('HighlevelGroup.id', 'HighlevelGroup.highlevel_group_name'),
                'conditions' => array(
                    'HighlevelGroup.deleted' => 'N',
                    $chk_hi_group,
                ),
                'recursive' => -1
        ));
        if(empty($this->request->data)){
                $highlevel_group_id = $this->request->query['high_level_group'];
                $selected_year = date('Y');
                $tmp_selected_month = date('m');
                $selected_month = $tmp_selected_month < 10 ? "{$tmp_selected_month}" : $tmp_selected_month;
        }else{
                $highlevel_group_id = $this->request->data['Highlevels']['high_level_group'];
                $selected_year = $this->request->data['Highlevels']['selected_year'];
                $tmp_selected_month = $this->request->data['Highlevels']['selected_month'];
                $selected_month = $tmp_selected_month < 10 ? "{$tmp_selected_month}" : $tmp_selected_month;
        }
        /************************** Date Range *******************************/
        $days_in_month = date("t", mktime(0, 0, 0, $selected_month, 1, $selected_year));

        $begin_date = $selected_year."-".$selected_month."-01 00:00";
        $last_date  = $selected_year."-".$selected_month."-".$days_in_month." 23:59";
        $date_range = array("$begin_date", "$last_date");
        
        $ulist = $this->Highlevel->find('all', array(
            'conditions'=>array(
                'Highlevel.highlevel_group_id'=>$highlevel_group_id, 
                'Highlevel.deleted'=>'N',
                'Highlevel.user_id != '=> 0 
            ),
            'fields'=>array('Highlevel.user_id', 'Highlevel.display_name', 'Highlevel.position_name', 'Highlevel.organization_name'),
            'order' => 'Highlevel.sort_order ASC',
            'recursive' => -1
        ));
        
        $user_list = array();
        foreach($ulist AS $highLevel){
            $user_list[] = $highLevel['Highlevel']['user_id'];
        }
        $reader = $this->AppointmentUserReader->find('list', array(
            'conditions' => array(
                'AppointmentUserReader.user_id' => $user_list,
                'AppointmentUserReader.user_id <>' => 0,
                'AppointmentUserReader.confirmed_status' => 'Accepted',
                'AppointmentUserReader.deleted = ' => 'N',
                'AppointmentUserReader.privacy_type_id' => 2,
                'OR'=>array(
                        'AppointmentUserReader.start_date BETWEEN ? AND ?'=>$date_range,
                        'AppointmentUserReader.end_date BETWEEN ? AND ?'=>$date_range
                )
            ),
            'fields' => array('AppointmentUserReader.appointment_id')
        ));
        
        $get_invite = implode(',', $reader);
        $chkreader = '';
        if(!empty($get_invite)){
            $chkreader = 'Appointment.id in ('.$get_invite.')';
        }
        /**********************************************************************/
        $tmp_appointments = $this->Appointment->find('all', array(
            'conditions'=>array(
                'Appointment.privacy_type_id'=>2,
                'Appointment.deleted'=>'N',
                'Appointment.user_id' => $user_list,
                'Appointment.user_id <>'=>0,
                'OR'=>array(
                    
                    'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                    'Appointment.end_date BETWEEN ? AND ?'=>$date_range,
                    $chkreader,
                )
            ),
            'fields'=>array(
                'Appointment.id', 'Appointment.start_date', 'Appointment.end_date', 'Appointment.location', 
                'Appointment.location_type_id', 'Appointment.detail', 'Appointment.user_id', 'Appointment.name'
            ),
            'order'=>array('Appointment.user_id'=>'ASC', 'Appointment.start_date'=>'ASC')
        )); 
        /**********************************************************************/
        //pr($tmp_appointments);
        $appointments = array();
        
        foreach($tmp_appointments as $app){
            $app = $app['Appointment'];
            $id = $app['id'];
            $uid = $app['user_id'];   
            if(empty($last_user_id)){ 
                $last_user_id = $app['user_id']; 
                $appointments[$uid]['id'] = $id;
                //$appointments[$uid]['user_full_name'] = $user[$uid]['detail'];
                $appointments[$uid]['user_id'] = $uid;
                $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
            }else{
                if($last_user_id == $app['user_id']){ /// same as last loop
                    $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                }else{ /// new user
                    $appointments[$uid]['id'] = $id;
                    $appointments[$uid]['user_id'] = $uid;
                    //$appointments[$uid]['user_full_name'] = $user[$uid]['detail'];;
                    $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                }
            } 
        }
        
        
        $this->set('user_list', $ulist);
        $this->set('appointments', $appointments);
        $this->set('selected_month', $selected_month);
        $this->set('selected_year', $selected_year);
        $this->set('highlevel_group', $highlevel_group);
        $this->set("breadCrumbs", $breadCrumbs);
    }
    
    public function highlevelajax(){
        $this->layout = 'blank';
        if(!empty($this->request->data)){
            $highlevel_group_id = $this->request->data['high_level_group'];
            $selected_year = $this->request->data['selected_year'];
            $tmp_selected_month = $this->request->data['selected_month'];
            $selected_month = $tmp_selected_month < 10 ? "0{$tmp_selected_month}" : $tmp_selected_month;
            /***************************************************/
            $days_in_month = date("t", mktime(0, 0, 0, $selected_month, 1, $selected_year));
            $begin_date = $selected_year."-".$selected_month."-01 00:00";
            $last_date  = $selected_year."-".$selected_month."-".$days_in_month." 23:59";
            $date_range = array("$begin_date", "$last_date");
            $ulist = $this->Highlevel->find('all', array(
                'conditions'=>array(
                    'Highlevel.highlevel_group_id'=>$highlevel_group_id, 
                    'Highlevel.deleted'=>'N',
                    'Highlevel.user_id != '=> 0 
                ),
                'fields'=>array('Highlevel.user_id', 'Highlevel.display_name', 'Highlevel.position_name'),
                'order' => 'Highlevel.sort_order ASC',
                'recursive' => -1
            ));

            $user_list = array();
            foreach($ulist AS $highLevel){
                $user_list[] = $highLevel['Highlevel']['user_id'];
            }
            $reader = $this->AppointmentUserReader->find('list', array(
                'conditions' => array(
                    'AppointmentUserReader.user_id' => $user_list,
                    'AppointmentUserReader.user_id <>' => 0,
                    'AppointmentUserReader.confirmed_status' => 'Accepted',
                    'AppointmentUserReader.deleted = ' => 'N',
                    'AppointmentUserReader.privacy_type_id'=>2,

                    'OR'=>array(
                            'AppointmentUserReader.start_date BETWEEN ? AND ?'=>$date_range,
                            'AppointmentUserReader.end_date BETWEEN ? AND ?'=>$date_range
                    )
                ),
                'fields' => array('AppointmentUserReader.appointment_id')
            ));

            $get_invite = implode(',', $reader);
            $chkreader = '';
            if(!empty($get_invite)){
                $chkreader = 'Appointment.id in ('.$get_invite.')';
            }
            /**********************************************************************/
            $tmp_appointments = $this->Appointment->find('all', array(
               'conditions'=>array(
                   'Appointment.privacy_type_id'=>2,
                   'Appointment.deleted'=>'N',
                   'Appointment.user_id' => $user_list,
                   'Appointment.user_id <>'=>0,
                   'OR'=>array(

                       'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                       'Appointment.end_date BETWEEN ? AND ?'=>$date_range,
                       $chkreader,
                   )
               ),
               'fields'=>array(
                   'Appointment.id', 'Appointment.start_date', 'Appointment.end_date', 'Appointment.location', 
                   'Appointment.location_type_id', 'Appointment.detail', 'Appointment.user_id', 'Appointment.name'
               ),
               'order'=>array('Appointment.user_id'=>'ASC', 'Appointment.start_date'=>'ASC')
            )); 
        /**********************************************************************/
            $appointments = array();

            foreach($tmp_appointments as $app){
                $app = $app['Appointment'];
                $id = $app['id'];
                $uid = $app['user_id'];   
                if(empty($last_user_id)){ 
                    $last_user_id = $app['user_id']; 
                    $appointments[$uid]['id'] = $id;
                    //$appointments[$uid]['user_full_name'] = $user[$uid]['detail'];
                    $appointments[$uid]['user_id'] = $uid;
                    $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                }else{
                    if($last_user_id == $app['user_id']){ /// same as last loop
                        $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                        $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                        $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                        $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                        $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                        //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                    }else{ /// new user
                        $appointments[$uid]['id'] = $id;
                        $appointments[$uid]['user_id'] = $uid;
                        //$appointments[$uid]['user_full_name'] = $user[$uid]['detail'];;
                        $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                        $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                        $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                        $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                        //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                        $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                    }
                } 
            }            
            
            $this->set('user_list', $ulist);
            $this->set('appointments', $appointments);
            $this->set('selected_month', $selected_month);
            $this->set('selected_year', $selected_year);
        }
    }
    
    function getNumMonthFromStr($str){
        switch ($str) {
            case "Jan" : $NumMonth = "01";
                break;
            case "Feb" : $NumMonth = "02";
                break;
            case "Mar" : $NumMonth = "03";
                break;
            case "Apr" : $NumMonth = "04";
                break;
            case "May" : $NumMonth = "05";
                break;
            case "Jun" : $NumMonth = "06";
                break;
            case "Jul" : $NumMonth = "07";
                break;
            case "Aug" : $NumMonth = "08";
                break;
            case "Sep" : $NumMonth = "09";
                break;
            case "Oct" : $NumMonth = "10";
                break;
            case "Nov" : $NumMonth = "11";
                break;
            case "Dec" : $NumMonth = "12";
                break;
            default : $NumMonth = date("m");
                break;
        }
        
        return $NumMonth;
    }
    
    public function highlevelview(){
        $highlevel_group = $this->request->query['highlevel_group_id'];
        $grouphighlevel = $this->HighlevelGroup->find('first', array(
            'conditions'=>array(
                'HighlevelGroup.id'=>$highlevel_group, 
                'HighlevelGroup.deleted'=>'N'
            )
        ));
        
        $highlevel = $this->Highlevel->find('all',array(
                'conditions' => array(
                        'Highlevel.highlevel_group_id' =>  $highlevel_group,
                        'Highlevel.user_id != ' => 0,
                        'Highlevel.deleted' => 'N'
                ),
                'order' => array('Highlevel.sort_order ASC')
        ));
        
        $breadCrumbs = array(
            array(
                'label' => __('High Level Schedule'),
                'link' => array('controller' => 'Highlevels', 'action' => 'highlevelindex', 'query' => 'high_level_group=3'),
            ),array(
                'label' => __('High Level Group'),
                'link' => array('controller' => 'Highlevels', 'action' => 'highlevelgroupindex', 'query' => false),
            ),
            array(
                'label' => $grouphighlevel['HighlevelGroup']['highlevel_group_name'],
                'link' => false
            ),
        );
        
        $this->set("highlevel", $highlevel);
        $this->set("breadCrumbs", $breadCrumbs);
        $this->set('highlevel_group', $grouphighlevel['HighlevelGroup']);
        
    }
    
    /*public function highlevelview(){
        $highlevel_group = $this->request->query['highlevel_group_id'];
        
        $highlevel = $this->HighlevelGroup->find('first', array(
           'conditions'=>array('HighlevelGroup.id'=>$highlevel_group, 'HighlevelGroup.deleted'=>'N')
        ));
        
        $this->set('highlevel_group', $highlevel['HighlevelGroup']);
        
        $user_list = array();
        if(!empty($highlevel['Highlevel'])){
            foreach($highlevel['Highlevel'] AS $i => $h){
                $user_list[] = $h['user_id'];
				
            }
        }
       
        
        if(!empty($user_list)){
            $ulist['list_user_id'] = implode(",", $user_list);
            $socket = new HttpSocket();
            $getUserProfileResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserHighLevel/.json', $ulist);
            $userProfile = json_decode($getUserProfileResult, true);
            
            $users = array();
           if(isset($userProfile['Result']['UserProfile'])){
                
                foreach($userProfile['Result']['UserProfile'] AS $i => $u){
                    $user_id = $u['UserProfile']['user_id'];
                    $first_name = $u['UserProfile']['first_name_th'];
                    $last_name = $u['UserProfile']['last_name_th'];
                    $position = @$u['UserOrganizationPosition'][0]['OrganizationPosition']['Position']['position_name'];

                    $users[$user_id]['user_id'] = $user_id;
                    $users[$user_id]['first_name_th'] = $first_name;
                    $users[$user_id]['last_name_th'] = $last_name;
                    $users[$user_id]['position'] = $position;
                }
           } 
            $this->set('users', $users);
        }
        
        $breadCrumbs = array(
            array(
                'label' => __('High Level Schedule'),
                'link' => array('controller' => 'Highlevels', 'action' => 'highlevelindex', 'query' => false),
            ),array(
                'label' => __('High Level Group'),
                'link' => array('controller' => 'Highlevels', 'action' => 'highlevelgroupindex', 'query' => false),
            ),
            array(
                'label' => $highlevel['HighlevelGroup']['highlevel_group_name'],
                'link' => false
            ),
        );
        $this->set("breadCrumbs", $breadCrumbs);
    }*/
    
    
    public function highleveladdusertogroup(){
        $highlevel_group_id = @$_POST['highlevel_group_id'];
        $users = explode(',', @$_POST['user_id']);
        
        if(is_array($users)){
            foreach($users AS $user_id){
               
                $user = $this->getUserNameFromList($user_id);
               
                $name = $user[$user_id]['first_name_th']." ".$user[$user_id]['last_name_th'];
                
                $this->Highlevel->create();
                $data['highlevel_group_id'] = $highlevel_group_id;
                $data['user_id'] = $user_id;
                $data['display_name'] = $name;
                $data['position_name'] = $user[$user_id]['position'];
                $data['organization_name'] = $user[$user_id]['organization'];
                
                
                $this->Highlevel->save($data);
                
            }
        }else{
                $user_id = $_POST['user_id'];
                $user = $this->getUserNameFromList($user_id);
                $name = $user[$user_id]['first_name_th']." ".$user[$user_id]['last_name_th'];
                $this->Highlevel->create();    
                
                $data['highlevel_group_id'] = $highlevel_group_id;
                $data['user_id'] = $user_id;
                $data['display_name'] = $name;
                $data['position_name'] = $user[$user_id]['position'];
                $data['organization_name'] = $user[$user_id]['organization'];
                $this->Highlevel->save($data);
                
        }
       
        /*if(is_array($users)){
            foreach($users AS $user_id){
                
                if(!is_numeric($user_id)){ continue; }
                
                $this->Highlevel->create();
                
                $data['highlevel_group_id'] = $highlevel_group_id;
                $data['user_id'] = $user_id;
                $data['display_name'] = $getname; 
                
                $this->Highlevel->save($data);
            }
        }else{
            $this->Highlevel->create();
            
            $data['highlevel_group_id'] = $highlevel_group_id;
            $data['user_id'] = $_POST['user_id'];
            $data['display_name'] = @$_POST['display_name']; 
            
            $this->Highlevel->save($data);
        }*/

    }
    
    
    public function highlevelgroupindex() {
        $this->layout = 'dashboard';
        $allgroup = $this->HighlevelGroup->find('all', array(
            'conditions' => array('HighlevelGroup.deleted' => 'N')
        ));
        //pr($showallgroup);
        $this->set('allgroup', $allgroup);
        $breadCrumbs = array(
            array(
                'label' => __('High Level Schedule'),
                'link' => array('controller' => 'Highlevels', 'action' => 'highlevelindex', 'query' => 'high_level_group=3'),
            ),array(
                'label' => __('High Level Group'),
                'link' => false
            )
        );
        $this->set("breadCrumbs", $breadCrumbs);
    }
    public function addgroup(){
        //pr($this->request->data); die();
        $group = $this->request->data;
        //pr($group['Highlevels']['highlevel_group_name']); die();
        $this->HighlevelGroup->save( $this->request->data);
        $this->HighlevelGroup->saveField('highlevel_group_name',$group['group']);
        
        $url = $this->PortalHelper->makeUrl('highlevels','highlevelgroupindex');
        $this->redirect($url);
         
    }
    public function editgroup() {
//                pr($this->request->data);
//                pr($this->HighlevelGroup->findById($this->request->data['idgroup'])); die();
                $editgroup = $this->request->data['editgroup'];
               // pr($editgroup); die();
                $this->HighlevelGroup->updateAll(
                      //  array('HighlevelGroup.highlevel_group_name =\''.$editgroup.'\'' ),
                         array("HighlevelGroup.highlevel_group_name" =>"'".$editgroup."'"),
                        array('HighlevelGroup.id'=>$this->request->data['idgroup'])
                );
                $url = $this->PortalHelper->makeUrl('highlevels','highlevelgroupindex');
                $this->redirect($url);
            }
     
     public function deletegroup(){

            $this->HighlevelGroup->updateAll(
                         //  array('HighlevelGroup.highlevel_group_name =\''.$editgroup.'\'' ),
                           array("HighlevelGroup.deleted" =>"'Y'"),
                           array('HighlevelGroup.id'=>$this->request->data['idgroup'])
                   );
            $url = $this->PortalHelper->makeUrl('highlevels','highlevelgroupindex');
            $this->redirect($url);
     }
    
     public function view(){
        
        $chkreader = '';
        $invite = array();
        $appointments = array(); 
         
        if(!empty($this->request->data)){
            $selected_year = $this->request->data['selected_year'];
            $tmp_selected_month = $this->request->data['selected_month'];
            $selected_month = $tmp_selected_month < 10 ? "{$tmp_selected_month}" : $tmp_selected_month;
            $user_id = $this->request->data['user_id'];
        }else{
            $selected_year = $this->request->query['selected_year'];
            $selected_month = $this->request->query['selected_month'];
            $user_id = $this->request->query['user_id'];
        }
        
            $days_in_month = date("t", mktime(0, 0, 0, $selected_month, 1, $selected_year));

            $begin_date = $selected_year."-".$selected_month."-01 00:00";
            $last_date  = $selected_year."-".$selected_month."-".$days_in_month." 23:59";
            $date_range = array("$begin_date", "$last_date");
            //$date_range = array("$selected_year-$selected_month-01", "$selected_year-$selected_month-$days_in_month");

            $user = $this->getUserNameFromList(array($user_id));
            
            $reader = $this->AppointmentUserReader->find('list', array(
                'conditions' => array(
                    'AppointmentUserReader.user_id' => $user_id,
                    'AppointmentUserReader.user_id <>' => 0,
                    'AppointmentUserReader.confirmed_status' => 'Accepted',
                    'AppointmentUserReader.deleted = ' => 'N',
                    'AppointmentUserReader.privacy_type_id'=>2,
                    'OR'=>array(
                            'AppointmentUserReader.start_date BETWEEN ? AND ?'=>$date_range,
                            'AppointmentUserReader.end_date BETWEEN ? AND ?'=>$date_range
                    )
                ),
                'fields' => array('AppointmentUserReader.appointment_id')
            ));      
            
            $get_invite = implode(',', $reader);
        
            if(!empty($get_invite)){
                $chkreader = 'Appointment.id in ('.$get_invite.')';
            }
            $tmp_appointments = $this->Appointment->find('all', array(
                'conditions'=>array(
                    'Appointment.privacy_type_id' => '2',
                    'Appointment.deleted'=>'N',
                    'Appointment.user_id'=>$user_id,
                    'OR'=>array(

                        'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                        'Appointment.end_date BETWEEN ? AND ?'=>$date_range,
                        $chkreader,
                    )
                ),
                'order'=>array('Appointment.user_id'=>'ASC', 'Appointment.start_date'=>'ASC')
            ));
            foreach($tmp_appointments as $app){
                $appointment = $app['Appointment'];
                $id = $appointment['id'];
                $uid = $appointment['user_id'];
                if($appointment['user_id'] != $user_id){
                    $uid = $user_id; 
                }
                if(empty($last_user_id)){ 
                    $last_user_id = $uid;
                    $appointments[$uid]['id'] = $id;
                    $appointments[$uid]['user_full_name'] = @$user[$uid]['detail'];
                    $appointments[$uid]['user_id'] = $uid;
                    $appointments[$uid]['event_date'][$id]['start_date'] = $appointment['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $appointment['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $appointment['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $appointment['name'];
                    //$appointments[$uid]['location_type_id'][$id] = $appointment['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $appointment['detail'];
                }else{
                    if($last_user_id == $uid){
                        $appointments[$uid]['event_date'][$id]['start_date'] = $appointment['start_date'];
                        $appointments[$uid]['event_date'][$id]['end_date'] = $appointment['end_date'];
                        $appointments[$uid]['event_date'][$id]['location_type_id'] = $appointment['location_type_id'];
                        $appointments[$uid]['event_date'][$id]['appointment_name'] = $appointment['name'];
                        $appointments[$uid]['event_date'][$id]['detail'] = $appointment['detail'];
                        //$appointments[$uid]['location_type_id'][$id] = $appointment['location_type_id']; 
                    }else{
                        $appointments[$uid]['id'] = $id;
                        $appointments[$uid]['user_id'] = $uid;
                        $appointments[$uid]['user_full_name'] = @$user[$uid]['detail'];
                        $appointments[$uid]['event_date'][$id]['start_date'] = $appointment['start_date'];
                        $appointments[$uid]['event_date'][$id]['end_date'] = $appointment['end_date'];
                        $appointments[$uid]['event_date'][$id]['location_type_id'] = $appointment['location_type_id'];
                        $appointments[$uid]['event_date'][$id]['appointment_name'] = $appointment['name'];
                        //$appointments[$uid]['location_type_id'][$id] = $appointment['location_type_id'];
                        $appointments[$uid]['event_date'][$id]['detail'] = $appointment['detail'];
                    }
                }
            }
            
            foreach($appointments AS $user_id => $appointment){
                $this->set('appointments', $appointment);
            }
            
            $this->set('selected_month', $selected_month);
            $this->set('selected_year', $selected_year);
            $this->set('user_id', $user_id);
            $this->set('user',$user);
           
     }
     
     
    
    public function highlevelapi($id=null, $userId = null, $select_date =null, $month=null, $year=null) {
        $this->layout ='blank';
        
        if(!empty($this->request->data)){
            
            $selected_date = $this->request->data['day'];
            $selected_year = $this->request->data['year'];
            $selected_month = $this->request->data['month'];
            //$tmp_selected_month = $this->request->data['month'];
            //$selected_month = $tmp_selected_month < 10 ? "0{$tmp_selected_month}" : $tmp_selected_month;
            $user_id = $this->request->data['user_id'];
        }else{
            $selected_date = $select_date;
            $selected_year = $year;
            $selected_month = $month;
            /*$tmp_selected_month  = $month;
            $selected_month = $tmp_selected_month < 10 ? "0{$tmp_selected_month}" : $tmp_selected_month;*/
            $user_id = $userId;
           
        }
        
		$days_in_month = date("t", mktime(0, 0, 0, $selected_month, 1, $selected_year));
		
		$begin_date = $selected_year."-".$selected_month."-01 00:00";
		$last_date  = $selected_year."-".$selected_month."-".$days_in_month." 23:59";
		$date_range = array("$begin_date", "$last_date");

        //$date_range = array("$selected_year-$selected_month-01", "$selected_year-$selected_month-$days_in_month");

        $user = $this->getUserNameFromList(array($user_id));
        //// =========================

        $tmp_appointments = $this->Appointment->find('all', array(
            'conditions'=>array(
                'Appointment.privacy_type_id'=>2,
                'Appointment.deleted'=>'N',
                'Appointment.user_id'=>$user_id,
                'OR'=>array(
                    'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                    'Appointment.end_date BETWEEN ? AND ?'=>$date_range
                )
            ),
            'fields'=>array(
                'Appointment.id', 'Appointment.start_date', 'Appointment.end_date', 'Appointment.location', 
                'Appointment.location_type_id', 'Appointment.detail', 'Appointment.user_id', 'Appointment.name'
            ),
            'order'=>array('Appointment.user_id'=>'ASC', 'Appointment.start_date'=>'ASC')
        ));
        
        //pr($tmp_appointments);

        $appointments = array();
        foreach($tmp_appointments AS $i => $app){
            $app = $app['Appointment'];
            $id = $app['id'];
            $uid = $app['user_id'];

            if(empty($last_user_id)){ 
                $last_user_id = $app['user_id']; 

                $appointments[$uid]['id'] = $id;
                $appointments[$uid]['user_full_name'] = @$user[$uid];
                $appointments[$uid]['user_id'] = $uid;
                $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
            }else{              
                if($last_user_id == $app['user_id']){ /// same as last loop
                    $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                }else{ /// new user
                    $appointments[$uid]['id'] = $id;
                    $appointments[$uid]['user_id'] = $uid;
                    $appointments[$uid]['user_full_name'] = @$user[$uid];
                    $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                }
            }
        }
        
        foreach($appointments AS $user_id => $appointment){
            $this->set('appointments', $appointment);
        }
        
        $this->set('selected_month', $selected_month);
        $this->set('selected_year', $selected_year);
        $this->set('selected_date', $selected_date);
        $this->set('user_id', $user_id);
        $this->set('user', $user);
    }
    
    public function timeline(){
        $this->layout ='blank';
        if(!empty($this->request->data)){
            $highlevel_group_id = $this->request->data['high_level_group'];
            $selected_date = $this->request->data['selecteddate'];
            $selected_year = $this->request->data['selectedyear'];
            $selected_month = $this->request->data['selectedmonth'];
            
            $days_in_month = date("t", mktime(0, 0, 0, $selected_month, 1, $selected_year));
            $begin_date = $selected_year."-".$selected_month."-01 00:00";
            $last_date  = $selected_year."-".$selected_month."-".$days_in_month." 23:59";
            $date_range = array("$begin_date", "$last_date");
            
            /***********************************************************/
            $ulist = $this->Highlevel->find('all', array(
               'conditions'=>array(
                   'Highlevel.highlevel_group_id'=>$highlevel_group_id, 
                   'Highlevel.deleted'=>'N',
                   'Highlevel.user_id != '=> 0 
               ),
               'fields'=>array('Highlevel.user_id', 'Highlevel.display_name', 'Highlevel.position_name'),
               'order' => 'Highlevel.sort_order ASC',
               'recursive' => -1
            ));

            $user_list = array();
            foreach($ulist AS $highLevel){
               $user_list[] = $highLevel['Highlevel']['user_id'];
            }
            
            /***********************************************************/
            $reader = $this->AppointmentUserReader->find('list', array(
                'conditions' => array(
                    'AppointmentUserReader.user_id' => $user_list,
                    'AppointmentUserReader.user_id <>' => 0,
                    'AppointmentUserReader.confirmed_status' => 'Accepted',
                    'AppointmentUserReader.deleted = ' => 'N',
                    'AppointmentUserReader.privacy_type_id'=>2,

                    'OR'=>array(
                            'AppointmentUserReader.start_date BETWEEN ? AND ?'=>$date_range,
                            'AppointmentUserReader.end_date BETWEEN ? AND ?'=>$date_range
                    )
                ),
                'fields' => array('AppointmentUserReader.appointment_id')
            ));

            $get_invite = implode(',', $reader);
            $chkreader = '';
            if(!empty($get_invite)){
                $chkreader = 'Appointment.id in ('.$get_invite.')';
            }
            /**********************************************************************/
            $tmp_appointments = $this->Appointment->find('all', array(
                'conditions'=>array(
                    'Appointment.privacy_type_id'=>2,
                    'Appointment.deleted'=>'N',
                    'Appointment.user_id' => $user_list,
                    'Appointment.user_id <>'=>0,
                    'OR'=>array(

                        'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                        'Appointment.end_date BETWEEN ? AND ?'=>$date_range,
                        $chkreader,
                    )
                ),
                'fields'=>array(
                    'Appointment.id', 'Appointment.start_date', 'Appointment.end_date', 'Appointment.location', 
                    'Appointment.location_type_id', 'Appointment.detail', 'Appointment.user_id', 'Appointment.name'
                ),
                'order'=>array('Appointment.user_id'=>'ASC', 'Appointment.start_date'=>'ASC')
            )); 
        /**********************************************************************/
            $appointments = array();
            foreach($tmp_appointments AS $i => $app){
                $app = $app['Appointment'];
                $id = $app['id'];
                $uid = $app['user_id']; 
                if(empty($last_user_id)){ 
                    $last_user_id = $app['user_id']; 
                    
                    $appointments[$uid]['id'] = $id;
                    //$appointments[$uid]['user_full_name'] = $user[$uid]['detail'];
                    $appointments[$uid]['user_id'] = $uid;
                    $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['user_id'] = $uid;
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                }else{              
                    if($last_user_id == $app['user_id']){ /// same as last loop
                        $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                        $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                        $appointments[$uid]['event_date'][$id]['user_id'] = $uid;
                        $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                        $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                        $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                        //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                    }else{ /// new user
                        $appointments[$uid]['id'] = $id;
                        $appointments[$uid]['user_id'] = $uid;
                        //$appointments[$uid]['user_full_name'] = $user[$uid]['detail'];
                        $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                        $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                        $appointments[$uid]['event_date'][$id]['user_id'] = $uid;
                        $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                        $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                        //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                        $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                    }
                }
                
            }
            $this->set('user_list', $ulist);
            $this->set('appointments', $appointments);
            $this->set('selected_month', $selected_month);
            $this->set('selected_year', $selected_year);
            $this->set('selected_date', $selected_date);
        }
    } 
    

    public function chkredirect($group_id){
            $this->layout="blank";
            $this->autoRender = false;
            $url = $this->PortalHelper->makeUrl('Highlevels','mission','group_id='.$group_id);
            //echo $url;
            $this->redirect($url);
    }

    public function mission(){
        $highlevel_group_id = 1;
        if(!empty($this->request->query['group_id'])){
              $highlevel_group_id = $this->request->query['group_id'];
        }
        $breadCrumbs = array(
            array(
                'label' => __('High Level Schedule'),
                'link' => array('controller' => 'Highlevels', 'action' => 'highlevelindex', 'query' => 'high_level_group='.$this->request->query['group_id'], 'param' => '')
            ),
            array(
                'label' => __('Mission Mode'),
                'link' => false
            )
        );
        
        /*****************************************************************/
        $high_conditions = '';
        $selected_month = date('m');
        $selected_year = date('Y');
        $selected_date = date('d');

        $days_in_month = date("t", mktime(0, 0, 0, $selected_month, 1, $selected_year));

        $begin_date = $selected_year."-".$selected_month."-01 00:00";
        $last_date  = $selected_year."-".$selected_month."-".$days_in_month." 23:59";
        $date_range = array("$begin_date", "$last_date");
        /*****************************************************************/
        if(!empty($this->request->data)){
                /**************** Check user when choose config ***************/

                        if(!empty($this->request->data['chooseuser'])){
                                $choose = $this->request->data['chooseuser'];
                                foreach($choose as $user){
                                        $getchoose[$user] = $user;
                                }

                                $getuser = implode(',', $getchoose);
                                $high_conditions = "Highlevel.user_id in (".$getuser.") ";
                        }
                /************************************************************/
                /*********************** Check Date For Query ***************/
                        //pr($this->request->data);
                        if(!empty($this->request->data['from_date']) && !empty($this->request->data['to_date'])){
                                $from_dat = $this->request->data['from_date'];
                                $to_dat = $this->request->data['to_date'];

                                $start_date = Utility::cdate($from_dat, 'd/m/Y', 'Y-m-d');
                                $end_date = Utility::cdate($to_dat, 'd/m/Y', 'Y-m-d');
                                $date_range = array($start_date, $end_date);

                        }
                /************************************************************/
        }
        $from_date = $date_range[0];
        $to_date = $date_range[1];
        
        /*****************************************************************/
         /***********************************************************/
            $ulist = $this->Highlevel->find('all', array(
               'conditions'=>array(
                   'Highlevel.highlevel_group_id'=>$highlevel_group_id, 
                   'Highlevel.deleted'=>'N',
                   'Highlevel.user_id != '=> 0,
                   $high_conditions
                   
               ),
               'fields'=>array('Highlevel.user_id', 'Highlevel.display_name', 'Highlevel.position_name'),
               'order' => 'Highlevel.sort_order ASC',
               'recursive' => -1
            ));

            $user_list = array();
            foreach($ulist AS $highLevel){
               $user_list[] = $highLevel['Highlevel']['user_id'];
            }
            
        /***********************************************************/
            $reader = $this->AppointmentUserReader->find('list', array(
                'conditions' => array(
                    'AppointmentUserReader.user_id' => $user_list,
                    'AppointmentUserReader.user_id <>' => 0,
                    'AppointmentUserReader.confirmed_status' => 'Accepted',
                    'AppointmentUserReader.deleted = ' => 'N',

                    'OR'=>array(
                            'AppointmentUserReader.start_date BETWEEN ? AND ?'=>$date_range,
                            'AppointmentUserReader.end_date BETWEEN ? AND ?'=>$date_range
                    )
                ),
                'fields' => array('AppointmentUserReader.appointment_id')
            ));

            $get_invite = implode(',', $reader);
            $chkreader = '';
            if(!empty($get_invite)){
                $chkreader = 'Appointment.id in ('.$get_invite.')';
            }
            /**********************************************************************/
            $tmp_appointments = $this->Appointment->find('all', array(
                'conditions'=>array(
                    'Appointment.privacy_type_id'=>2,
                    'Appointment.deleted'=>'N',
                    'Appointment.user_id'=>$user_list,
                    'Appointment.user_id <>'=>0,
                    'OR'=>array(
                        'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                        'Appointment.end_date BETWEEN ? AND ?'=>$date_range
                    )
                ),
                'fields'=>array(
                    'Appointment.id', 'Appointment.start_date', 'Appointment.end_date', 'Appointment.location', 
                    'Appointment.location_type_id', 'Appointment.detail', 'Appointment.private_note', 'Appointment.is_all_day', 'Appointment.user_id', 'Appointment.name'
                ),
                'order'=>array('Appointment.start_date'=>'ASC')
            )); 
        /**********************************************************************/  
        $this->set('appointments', $tmp_appointments);
        $this->set('user_list', $ulist);    
        $this->set('from_dat',$from_date);
        $this->set('to_dat',$to_date);    
        $this->set("breadCrumbs", $breadCrumbs);
    }
   
    public function configmission(){
        $highlevel_group_id = $this->request->query['group_id'];
        $breadCrumbs = array(
            array(
                'label' => __('High Level Schedule'),
                'link' => array('controller' => 'Highlevels', 'action' => 'highlevelindex', 'query' => 'high_level_group='.$highlevel_group_id, 'param' => '')
            ),
            array(
                'label' => __('Config Mission Mode'),
                'link' => false
            )
        );
            $ulist = $this->Highlevel->find('all', array(
                'conditions'=>array(
                    'Highlevel.highlevel_group_id'=>$highlevel_group_id, 
                    'Highlevel.deleted'=>'N',
                    'Highlevel.user_id != '=> 0 
                ),
                'fields'=>array('Highlevel.user_id', 'Highlevel.display_name', 'Highlevel.position_name'),
                'order' => 'Highlevel.sort_order ASC',
                'recursive' => -1
             ));

             $user_list = array();
             /*foreach($ulist AS $highLevel){
                $user_list[] = $highLevel['Highlevel']['user_id'];
             }*/
             $this->set('user_list', $ulist);
        
             $this->set("breadCrumbs", $breadCrumbs);
        
    }
    

	public function edithighlevel($id = null, $group = null){
            if(empty($id)){
                    $id = $this->request->query['id'];
            }
            
           
            
            if(!empty($this->request->data)){
                //pr($this->request->data); die();
                $user_id = $this->request->data['Highlevel']['user_id'];
		$user = $this->getUserNameFromList($user_id);
                $position =  $user[$user_id]['position'];
                $organization =  $user[$user_id]['organization'];
                $group = $this->request->data['Highlevel']['highlevel_group_id'];
                
                if(empty($this->request->data['Highlevel']['new_name'])){
                    $name = $this->request->data['Highlevel']['name'];
                }else{
                    $name = $this->request->data['Highlevel']['new_name'];
                }


                $this->Highlevel->id = $id;
                $data = array();
                $data['Highlevel']['user_id'] = $this->request->data['Highlevel']['user_id'];
                $data['Highlevel']['display_name'] = $name;
                $data['Highlevel']['position_name'] = $position;
                $data['Highlevel']['organization_name'] = $organization;
                $data['Highlevel']['sort_order'] = $this->request->data['Highlevel']['sort_order'];
                $data['Highlevel']['highlevel_group_id'] = $this->request->data['Highlevel']['highlevel_group_id'];
                //pr($data); die();
                
                $this->Highlevel->save($data); 
                $url = $this->PortalHelper->makeUrl('Highlevels','highlevelview','highlevel_group_id='.$this->request->data['Highlevel']['highlevel_group_id']);
                $this->redirect($url);
                
            }else{
                if(empty($group)){
                    $group = $this->request->query['group_id'];
                }
                //pr($id);
                $highlevel = $this->Highlevel->find('first', array(
                        'conditions' => array(
                                'Highlevel.id' => $id,
                                'Highlevel.deleted' => 'N'
                        ),
                        'recursive' => -1,
                ));
                $user_id = $highlevel['Highlevel']['user_id'];
                $user = $this->getUserNameFromList($highlevel['Highlevel']['user_id']);

                $highlevel['Highlevel']['name'] = $user[$user_id]['first_name_th']." ".$user[$user_id]['last_name_th'];

                $this->request->data = $highlevel;
                //pr($highlevel);
                $highlevelgroup = $this->HighlevelGroup->find('first', array(
                        'conditions' => array(
                                'HighlevelGroup.id' => $group,
                                'HighlevelGroup.deleted' => 'N'
                        ),
                        'recursive' => -1,
                ));

                $breadCrumbs = array(
                    array(
                        'label' => __('High Level Schedule'),
                        'link' => array('controller' => 'Highlevels', 'action' => 'highlevelindex', 'query' => false),
                    ),array(
                        'label' => __('High Level Group'),
                        'link' => array('controller' => 'Highlevels', 'action' => 'highlevelgroupindex', 'query' => false),
                    ),
                    array(
                            'label' => $highlevelgroup['HighlevelGroup']['highlevel_group_name'] ,
                            'link' => array('controller' => 'Highlevels', 'action' => 'highlevelview', 'query' => 'highlevel_group_id='.$group),

                    ),
                    array(
                        'label' => __('edit'),
                        'link' => false
                    ),
                );
                $this->set('model', 'Highlevel');
                $this->set("id", $id);
                $this->set("groupid", $group);
                $this->set("breadCrumbs", $breadCrumbs);
            }
	}

	function deleteuserinhighlevel(){
		$this->layout="blank";
		$this->autoRender = false;
		if($this->request->data){
			$id = $this->request->data['id'];
			$data = array();
			$this->Highlevel->id = $id;
			$data['deleted'] = 'Y';
			$this->Highlevel->save($data);
			echo 1;
		}else{
			echo 0;
		}
	}
}

?>
