<?php

App::uses('AppController', 'Controller');
App::uses('Utility', 'mfa');
App::uses('HttpSocket', 'Network/Http');

class AppointmentsController extends AppController {
	public $name = 'Appointments';
	public $uses = array('Appointment', 'PortalAttachment', 'AppointmentColor', 'ContentType',
                             'ContentUserReader', 'AppointmentHandle', 'AppointmentLocation', 
                             'AppointmentPriority', 'AppointmentStatus', 'AppointmentUserReader', 
                             'AppointmentPrivacy','AppointmentAlert','PortalAttachment', 'ResourceContent');
     
	public $components = array("PortalHelper", "FileStorageComponent");
	public $helpers = array('Form', 'Html', 'Portal','Session');

	public function index() {
		//$this->layout = 'dashboard';
		//$this->layout = 'searchview';
		//$this->layout = 'detailview';
		//$this->layout = 'mailbox';
		$this->layout = 'calendar';
		//$this->layout = 'listview';
		//$this->layout = 'form';
		//$this->layout = 'map';
		//$this->layout = 'blank';

		$this->set("content", 'dddd');

	}

    public function getUserNameFromList($user_list){ // array $user_list
        
        if(!is_array($user_list)){
            $user_list = array($user_list);
        }
        
        $socket = new HttpSocket();
        $ulist['list_user_id'] = implode(",", $user_list);
        $getUserProfileResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserHighLevel/.json', $ulist);
        $getUserProfile = json_decode($getUserProfileResult, true);

        $user = array();
        if(empty($getUserProfile)){ 
            return false; 
        }else{
            if(!empty($getUserProfile['Result']['Error'])){
                return false;
            }
            
            foreach($getUserProfile['Result']['UserProfile'] AS $i => $u){
                $uid = $u['UserProfile']['user_id'];
                    $position = '';
                if(!empty($u['UserOrganizationPosition'][0]['OrganizationPosition']['Position']['position_name'])){
                    $position = $u['UserOrganizationPosition'][0]['OrganizationPosition']['Position']['position_name'];
                }
                $uname = $u['UserProfile']['first_name_th'] . " " . $u['UserProfile']['last_name_th'] . " [{$position}]";
                
                $user[$uid]['id'] = $uid;
                $user[$uid]['detail'] = $uname;
                
            }
            
            return $user;
        }
    }

	public function calendar($id = null) {
               $currentUser = $this->Session->read('AuthUser');
                /*$this->Session->write('pc', $this->request->query['pc']);
                $this->Session->write('pa', $this->request->query['pa']);
                if(!empty($this->request->query['pq'])){
                    $this->Session->write('pq', base64_decode($this->request->query['pq']));
                }else{
                    $this->Session->delete('pq');
                }
                if(!empty($this->request->query['pr'])){
                   $this->Session->write('pr', base64_decode($this->request->query['pr'])); 
                }else{
                    $this->Session->delete('pr');
                }*/
               if(empty($id)){
                   $id = $currentUser['AuthUser']['id'];
               }
               
               
               $this->layout = 'calendar';
               $breadCrumbs = array(
                        array(
                                'label' => __('My Appointment'), 
                                'link'  => false
                        ),

               ); 
               $showowner = $this->AppointmentHandle->find('all', array(
                        'conditions' => array(
                            'AppointmentHandle.handle_user_id' =>  $id,
                            'AppointmentHandle.deleted = ' => 'N',
                            'AppointmentHandle.user_id != ' => '0',
                        ),
                        
                    )
               );
              
              /*
                $socket = new HttpSocket();
                $showuser = array();
                $countdata = count($showowner);
                $i = 1 ;
                $manyid = '';
                $userid = '';
                $chkname = '';
                //pr($countdata);
                foreach($showowner as $key => $user){
                
                  if($i == $countdata){
                      $manyid .= $user['AppointmentHandle']['user_id'];
                      $data['list_user_id'] = $manyid;

                      $dataUser = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', $data);
                      $showdatauser = json_decode($dataUser, true);
                      $showuser = $showdatauser['Result']['UserProfile'];
                     
                      
                     
                  }else{
                      $manyid .= $user['AppointmentHandle']['user_id'].', ';
                  }  
                  $i++;
                }
                $chkname['AppointmentHandle'] = $showuser;*/
               
               $this->set('showowner',$showowner);
               //$this->set('chkname',$chkname);
               $this->set('userid', $id);
               $this->set("breadCrumbs", $breadCrumbs);

	}
        
        public function addminicalendar ($id = null){
               $currentUser = $this->Session->read('AuthUser');
               
               if(empty($id)){
                   $id = $currentUser['AuthUser']['id'];
               }
                $this->request->data['Appointment']['user_id'] = $id;
                //pr($currentUser);
                
                
                $list = $this->AppointmentPriority->find('list', array(
                            'fields' => array('AppointmentPriority.id', 'AppointmentPriority.priority_name'),
                            'order'=>array('AppointmentPriority.id ASC'),
                            'conditions'=>array(
                                'AppointmentPriority.deleted = ' => 'N',
                            )
                        )
                );
                $location = $this->AppointmentLocation->find('list', array(
                            'fields' => array('AppointmentLocation.id', 'AppointmentLocation.location_name'),
                            'order'=>array('AppointmentLocation.id ASC'),
                            'conditions'=>array(
                                 'AppointmentLocation.deleted = ' => 'N',
                            ),
                        )
                );
                $color = $this->AppointmentColor->find('all', array(
                            'fields' => array('AppointmentColor.id', 'AppointmentColor.color_code'),
                            'order'=>array('AppointmentColor.id ASC'),
                            'conditions'=>array(
                                 'AppointmentColor.deleted = ' => 'N',
                            ),
                        )
                );
                $defaultlist = $this->AppointmentPriority->find('list', array(
                            'fields' => array('AppointmentPriority.id', 'AppointmentPriority.priority_name'),
                            'conditions'=>array(
                                "AppointmentPriority.priority_name = 'none' ",
                                'AppointmentPriority.deleted = ' => 'N',
                            )
                        )
                );
             
                $status = $this->AppointmentStatus->find('list', array(
                            'fields' => array('AppointmentStatus.id', 'AppointmentStatus.status_name'),
                            'order'=>array('AppointmentStatus.id ASC'),
                            'conditions'=>array(
                                 'AppointmentStatus.deleted = ' => 'N',
                            ),
                    
                        )
                );
                $privacy_list = $this-> AppointmentPrivacy->find('list', array(
                            'fields' => array('AppointmentPrivacy.privacy_name'),
                            'order'=>array('AppointmentPrivacy.id ASC'),
                            'conditions'=>array(
                                 'AppointmentPrivacy.deleted = ' => 'N',
                            ),
                        )
                );
                $alert_list = $this->AppointmentAlert->find('list', array(
                        'fields' => array('AppointmentAlert.alert_name'),
                        'order'=>array('AppointmentAlert.id ASC'),                        
                        'conditions'=>array(
                            'AppointmentAlert.deleted = ' => 'N'
                        )
                    )  
            );
                $showowner = $this->AppointmentHandle->find('all', array(
                        'conditions' => array(
                            'AppointmentHandle.handle_user_id' =>  $id,
                            'AppointmentHandle.deleted = ' => 'N',
                            'AppointmentHandle.user_id != ' =>  '0',
                        ),
                        
                    )
                );
                
                $socket = new HttpSocket();
                $showuser = array();
                $countdata = count($showowner);
                $i = 1 ;
                $manyid = '';
                $userid = '';
                $chkname = '';
                //pr($countdata);
                foreach($showowner as $key => $user){
                  
                  if($i == $countdata){
                      $manyid .= $user['AppointmentHandle']['user_id'];
                      $data['list_user_id'] = $manyid;

                      $dataUser = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', $data);
                      $showdatauser = json_decode($dataUser, true);
                      $showuser = $showdatauser['Result']['UserProfile'];
                     
                  }else{
                      $manyid .= $user['AppointmentHandle']['user_id'].', ';
                  }  
                  $i++;
                }
                $chkname['AppointmentHandle'] = $showuser;
                
                $size = Utility::checkfilesize(Configure::read('Config.MaxFileSize'));
                include 'Metadata/Appointment/miniform.ctp';
              
                $breadCrumbs = array(
                    array(
                        'label' => __('Appointments'),
                        'link' => array('controller' => $this->name, 'action' => 'calendar', 'query' => '', 'param' => '')
                    ),
                    array(
                        'label' => __('Add Appointment'),
                        'link' => false
                    ),
                );
                $this->set("viewDefs", $viewDefs);
                $this->set("breadCrumbs", $breadCrumbs);
                $this->set("chkname",$chkname);
                $this->set("chkcolor", '');
                $this->set("all", '');
                $this->set("alway", '');
                $this->layout = "blank";  
        }
       
       public function appointmentuseradd($id = null){
           $currentUser = $this->Session->read('AuthUser');
           if(empty($id)){
               $id = $currentUser['AuthUser']['id'];
           }
            $url = $this->PortalHelper->makeUrl('appointments', 'addappointment', 'user_id='.$id);
            $this->redirect($url);
       }
        
       public function addappointment(){
           $currentUser = $this->Session->read('AuthUser');
           
           if ($this->PortalHelper->isPost()) {
              
              $datas = $this->data; 
              
              if(!empty($datas['AppointmentUserReader'])){
                        sort($datas['AppointmentUserReader']);               
              }
              
              if(!isset($datas['Appointment']['alert_type_id'])){
                  $datas['Appointment']['alert_type_id'] = 1;
              }
              
              if(!isset($datas['Appointment']['alert_repeat_type_id'])){
                  $datas['Appointment']['alert_repeat_type_id'] = 1;
              }
              
              $remind_time = $this->AppointmentAlert->find('first', array(
                    'conditions'=>array(
                        'AppointmentAlert.id'=>$datas['Appointment']['alert_type_id']
                    )
              ));
              $remind_repeat_time = $this->AppointmentAlert->find('first', array(
                    'conditions'=>array(
                        'AppointmentAlert.id'=>$datas['Appointment']['alert_repeat_type_id']
                    )
              ));
               $email = 'N';
               $sms = 'N';
               $isall = 'N';
               $isalway = 'N';
               $remind_time_value = '';
               $remind_time_unit = '';
               $remind_time_repeat_value = '';
               $remind_time_repeat_unit = '';
               
              switch($remind_time['AppointmentAlert']['alert_name']){
                   case 'None' :  break;
                   case 'At Time of event' :   $remind_time_value = '0';
                                               $remind_time_unit = 'M'; 
                                               break;
                   case '5 minutes before' :   $remind_time_value = '5';
                                               $remind_time_unit = 'M'; 
                                               break;
                   case '15 minutes before' :  $remind_time_value = '15';
                                               $remind_time_unit = 'M'; 
                                               break;
                   case '30 minutes before' :  $remind_time_value = '30';
                                               $remind_time_unit = 'M'; 
                                               break;
                   case '1 hour before' :      $remind_time_value = '60';
                                               $remind_time_unit = 'H'; 
                                               break;
                   case '2 hours before' :     $remind_time_value = '120';
                                               $remind_time_unit = 'H'; 
                                               break;
                   case '1 day before' :       $remind_time_value = '1440';
                                               $remind_time_unit = 'D'; 
                                               break;
                   case '2 days before' :      $remind_time_value = '2880';
                                               $remind_time_unit = 'D'; 
                                               break;
                   default:                                         
                                               break;           
               }
               
               switch($remind_repeat_time['AppointmentAlert']['alert_name']){
                   case 'None' :  break;
                   case 'At Time of event' :   $remind_time_repeat_value = '0';
                                               $remind_time_repeat_unit = 'M'; 
                                               break;
                   case '5 minutes before' :   $remind_time_repeat_value = '5';
                                               $remind_time_repeat_unit = 'M'; 
                                               break;
                   case '15 minutes before' :  $remind_time_repeat_value = '15';
                                               $remind_time_repeat_unit = 'M'; 
                                               break;
                   case '30 minutes before' :  $remind_time_repeat_value = '30';
                                               $remind_time_repeat_unit = 'M'; 
                                               break;
                   case '1 hour before' :      $remind_time_repeat_value = '60';
                                               $remind_time_repeat_unit = 'H'; 
                                               break;
                   case '2 hours before' :     $remind_time_repeat_value = '120';
                                               $remind_time_repeat_unit = 'H'; 
                                               break;
                   case '1 day before' :       $remind_time_repeat_value = '1440';
                                               $remind_time_repeat_unit = 'D'; 
                                               break;
                   case '2 days before' :      $remind_time_repeat_value = '2880';
                                               $remind_time_repeat_unit = 'D'; 
                                               break;
                   default:                    break;           
               }
               
               /***************************************************************/
               
               
               $data = array();
              $repeat_alert = 'N';
               /*if($datas['Appointment']['is_alert_repeat'] == 0){
                   $repeat_alert = 'N';
               }else{
                   $repeat_alert = 'Y';
               }*/
               if($datas['Appointment']['is_alert'] == 0){
                   $alert = 'N';
               }else{
                   $alert = 'Y';
               }
               
               if($remind_time['AppointmentAlert']['alert_name'] == 'None'){
                   $alert = 'N';
                   $repeat_alert = 'N';
               }
               
               if($remind_repeat_time['AppointmentAlert']['alert_name'] == 'None'){
                   $repeat_alert = 'N';
               }
               
               
                if(empty($datas['Appointment']['color_id'])){
                    $datas['Appointment']['color_id'] = '1';
                }
             
                if(isset($datas['Appointment']['remind'])){
                    if($datas['Appointment']['remind'] == 'email'){
                        $email = 'Y';
                    }else if($datas['Appointment']['remind'] == 'sms'){
                        $sms = 'Y';
                    }
                }else{
                    $email = 'N';
                    $sms = 'N';
                }
                
                if(!empty($datas['Appointment']['is_all_day'])){
                    $isall = 'Y';
                }
                if(!empty($datas['Appointment']['is_always'])){
                    $isalway = 'Y';
                }
                if($datas['Appointment']['user_id'] == $currentUser['AuthUser']['id']){
                    $handle = '';
                }else{
                    $handle =  $currentUser['AuthUser']['id'];
                }
               /***************************************************************/
               
               /********************** Start - End Date  **********************/
                
               $start_date = Utility::cdate($datas['Appointment']['start_date'], 'd/m/Y', 'Y-m-d');
               $end_date = Utility::cdate($datas['Appointment']['end_date'], 'd/m/Y', 'Y-m-d');
               
               
               $datas['Appointment']['fhour'] = str_pad($datas['Appointment']['fhour'], 2, "0", STR_PAD_LEFT);
               $datas['Appointment']['thour'] = str_pad($datas['Appointment']['thour'], 2, "0", STR_PAD_LEFT);
               
               
               if($start_date == $end_date){
               /************* check case start date same end date  ************/
                if($datas['Appointment']['fhour'] == 'all'){
                    $start_time = '08:30:00';
                    $end_time = '16:30:00';
                    $isall = 'Y';  
                }else if($datas['Appointment']['fhour'] == 'morning'){
                    $start_time = '08:30:00';
                    $end_time = '12:00:00';  
                }else if($datas['Appointment']['fhour'] == 'afternoon'){
                    $start_time = '13:00:00';
                    $end_time = '16:30:00';
                }else{
                    $start_time = $datas['Appointment']['fhour'].':'.$datas['Appointment']['fmin'].':00';
                    if($datas['Appointment']['thour'] == 'all'){
                        $end_time = '16:30:00'; 

                    }else if($datas['Appointment']['thour'] == 'morning'){
                        $end_time = '12:00:00';

                    }else{
                        $end_time = $datas['Appointment']['thour'].':'.$datas['Appointment']['tmin'].':00';  
                    }
                }
               /********* End check case start date same end date  ************/
               }else{
               /************* check case start date Diff end date  ************/
                 if($datas['Appointment']['fhour'] == 'all'){
                         $start_time = '08:30:00';

                         if($datas['Appointment']['thour'] == 'all'){
                            $end_time = '16:30:00'; 
                            $isall = 'Y';

                         }else if($datas['Appointment']['thour'] == 'morning'){
                            $end_time = '12:00:00';

                         }else{
                            $end_time = $datas['Appointment']['thour'].':'.$datas['Appointment']['tmin'].':00';  
                         }

                 }else if($datas['Appointment']['fhour'] == 'morning'){
                               $start_time = '08:30:00';

                           if($datas['Appointment']['thour'] == 'all'){
                               $end_time = '16:30:00';

                           }else if($datas['Appointment']['thour'] == 'morning'){
                               $end_time = '12:00:00';

                           }else{
                               $end_time = $datas['Appointment']['thour'].':'.$datas['Appointment']['tmin'].':00';  

                           }

                 }else if($datas['Appointment']['fhour'] == 'afternoon'){
                               $start_time = '13:00:00';

                           if($datas['Appointment']['thour'] == 'all'){
                               $end_time = '16:30:00'; 

                           }else if($datas['Appointment']['thour'] == 'morning'){
                               $end_time = '12:00:00'; 

                           }else{
                               $end_time = $datas['Appointment']['thour'].':'.$datas['Appointment']['tmin'].':00';  

                           }
                    }else{
                        $start_time = $datas['Appointment']['fhour'].':'.$datas['Appointment']['fmin'].':00'; 
                        if($datas['Appointment']['thour'] == 'all'){
                               $end_time = '16:30:00'; 

                           }else if($datas['Appointment']['thour'] == 'morning'){
                               $end_time = '12:00:00'; 

                           }else{
                               $end_time = $datas['Appointment']['thour'].':'.$datas['Appointment']['tmin'].':00';  

                           }
                    }
               /********* End check case start date Diff end date  ************/
               }
               /****************** End Start - End Date ***********************/
               $startshow = $start_date.' '.$start_time;
               $endshow = $end_date.' '.$end_time;

               /***********************************************************************************/    
             //Add Attachment1   
                if (!empty($datas['Appointment']['attach1']['tmp_name'])) {
                    $post = $datas['Appointment']['attach1'];
                    $tmp_name = $post['tmp_name'];
                    $file_name = $post['name'];
                    $file_error = $post['error'];
                    $file_size = $post['size'];
                    $file_type = $post['type'];

                    $attach_key = $this->addAttachment($post, $tmp_name, $file_name, $file_error, $file_size, $file_type, $currentUser['AuthUser']['id']);

                    $data['Appointment']['attachment_name'] = $attach_key['original_name'];
                    $data['Appointment']['attachment'] = $attach_key['key'];
                }else{
                    $data['Appointment']['attachment'] = '';
                    $data['Appointment']['attachment_name'] = '';
                }
               /***************************************************************/
               $data['Appointment']['name'] = $datas['Appointment']['name'];
               $data['Appointment']['start_date'] = $startshow;
               $data['Appointment']['end_date'] = $endshow;
               $data['Appointment']['is_all_day'] = $isall;
               $data['Appointment']['is_always'] = $isalway;
               $data['Appointment']['location'] = $datas['Appointment']['location'];
               $data['Appointment']['location_type_id'] = $datas['Appointment']['location_type_id'];
               $data['Appointment']['detail'] = $datas['Appointment']['detail'];
               $data['Appointment']['private_note'] = $datas['Appointment']['private_note'];
               $data['Appointment']['color_id'] = $datas['Appointment']['color_id'];
               $data['Appointment']['priority_type_id'] = $datas['Appointment']['priority_type_id'];
               $data['Appointment']['status_type_id'] = '';
               $data['Appointment']['is_alert'] = $alert;
               $data['Appointment']['alert_email'] = $email;
               $data['Appointment']['alert_sms'] = $sms;
               $data['Appointment']['alert_type_id'] = $datas['Appointment']['alert_type_id'];
               $data['Appointment']['is_alert_repeat'] = $repeat_alert;
               $data['Appointment']['alert_repeat_type_id'] = $datas['Appointment']['alert_repeat_type_id'];
               $data['Appointment']['privacy_type_id'] = $datas['Appointment']['privacy_type_id'];
               $data['Appointment']['user_id'] = $datas['Appointment']['user_id'];
               $data['Appointment']['handle_user_id'] = $handle;
               $data['Appointment']['remind_time_value'] = $remind_time_value;
               $data['Appointment']['remind_time_unit'] = $remind_time_unit;
               $data['Appointment']['remind_time_repeat_value'] = $remind_time_repeat_value;
               $data['Appointment']['remind_time_repeat_unit'] = $remind_time_repeat_unit;
             
               $sendnotidetail = strip_tags($datas['Appointment']['detail']);
              /*$socket = new HttpSocket();   
              foreach($datas['AppointmentUserReader'] as $key => $userId){
                    $data['list_user_id'] = $userId['user_id'];
                    $dataUser = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', $data);
                    $showdatauser = json_decode($dataUser, true);
                    $showownerappt = $showdatauser['Result']['UserProfile'];
                    
              }
              pr($showownerappt[0]['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['OrganizationDescription'][0]['description']);
               die();*/
              //pr($showownerappt[0]['UserOrganizationPosition']['OrganizationPosition']['Position']['position_name']); die();
               
               $data_noti = array();
               $this->Appointment->create();
               $this->AppointmentUserReader->create();
               
               if($this->Appointment->save($data)){ 
                    $appt_id = $this->Appointment->getLastInsertId();
                    if(!empty($appt_id)){
                      /***********************************************/   
                        if(!empty($datas['AppointmentUserReader'])){
                            sort($datas['AppointmentUserReader']);
                            
                            $socket = new HttpSocket();   
                            $countreader = count($datas['AppointmentUserReader']);
                            $i = 1;
                            $manyid = '';
                            $lang = 1;
                            foreach($datas['AppointmentUserReader'] as $key => $userId){
                                $data['list_user_id'] = $userId['user_id'];
                                $dataUser = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', $data);
                                $showdatauser = json_decode($dataUser, true);
                                $showownerappt = $showdatauser['Result']['UserProfile'];

                                /******************************* get Belong ****************************************/
                                $data_detail = array();
                                $data_detail['list_org_id'] = $showownerappt[0]['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['parent_id'];

                                $getBelongOrganization = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganizationName/.json', $data_detail);
                                $getbelongOrganize = json_decode($getBelongOrganization, true);
                                foreach($getbelongOrganize['Result']['OrganizationName'] as $j=>$BelongOrganization){
                                    if($lang == $BelongOrganization['OrganizationDescription']['language_id']){
                                        $desc_belong = $BelongOrganization['OrganizationDescription']['description'];
                                        //$this->request->data['Content']['belong_organization_name'] = $desc_belong;
                                        break;
                                    }
                                }
                                /***********************************************************************************/
                                $this->AppointmentUserReader->query("INSERT INTO pg_ab_appointment_users (created_date, created_user_id, updated_date, updated_user_id, confirmed_status, appointment_id, user_id, published, first_name, last_name, position_name, organization_name, belong_organization_name, start_date, end_date, privacy_type_id) 
                                VALUES ('".date('Y-m-d H:i:s')."', '".$currentUser['AuthUser']['id']."', '".date('Y-m-d H:i:s')."', '".$currentUser['AuthUser']['id']."', 'Wait for confirm', '".$appt_id."', '".$userId['user_id']."', 'N', '".$showownerappt[0]['UserProfile']['first_name_th']."', '".$showownerappt[0]['UserProfile']['last_name_th']."', '".$showownerappt[0]['UserOrganizationPosition'][0]['OrganizationPosition']['Position']['position_name']."', '".$showownerappt[0]['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['OrganizationDescription'][0]['description']."', '".$desc_belong."', '".$startshow."', '".$endshow."', '".$datas['Appointment']['privacy_type_id']."'  )");
                                
                            }        
                        }
                        //set noti
                            $iclass = '10201';
                            $showdatanoti = $this->AppointmentUserReader->find('list', array(
                                'conditions'=>array(
                                    'AppointmentUserReader.deleted' => 'N',
                                    'AppointmentUserReader.appointment_id'=> $appt_id
                                ),
                                'fields' => array('AppointmentUserReader.user_id')
                            ));                            
                            $notiuser = implode(',', $showdatanoti);
                            if(!empty($notiuser)){
                                    $data_noti = array(
                                        'time' => date('Y-m-d H:i:s'),
                                        'to_id' => $notiuser,
                                        'message' => $sendnotidetail,
                                        'from_id' => $currentUser['AuthUser']['id'],
                                        'iclass' => $iclass,
                                        'action' => substr(Configure::read('Portal.Application'),0, -1).$this->PortalHelper->makeUrl($this->name, 'view', 'id=' . $appt_id),
                                    );
                                    $socket = new HttpSocket();
                                    $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);

                                
                                if($alert == 'Y'){
                                    if(!empty($remind_time_value)){
                                        $notidate = date('Y-m-d H:i:s', strtotime($startshow.' - '.$remind_time_value.' minutes')); 
                                        $notiuser_remind = $notiuser.",".$currentUser['AuthUser']['id'];
                                        $data_noti = array(
                                            'time' => $notidate,
                                            'to_id' => $notiuser_remind,
                                            'message' => $sendnotidetail,
                                            'from_id' => $currentUser['AuthUser']['id'],
                                            'iclass' => $iclass,
                                            'action' => substr(Configure::read('Portal.Application'),0, -1).$this->PortalHelper->makeUrl($this->name, 'view', 'id=' . $appt_id),
                                        );
                                        $socket = new HttpSocket();
                                        $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);                                        
                                    }
                                }

                                if($repeat_alert == 'Y'){
                                    if(!empty($remind_time_repeat_value)){
                                        $notirepeatdate = date('Y-m-d H:i:s', strtotime($startshow.' - '.$remind_time_repeat_value.' minutes')); 
                                        $notiuser_remind_repeat = $notiuser.",".$datas['Appointment']['user_id'];
                                        $data_noti = array(
                                            'time' => $notirepeatdate,
                                            'to_id' => $notiuser_remind_repeat,
                                            'message' => $sendnotidetail,
                                            'from_id' => $currentUser['AuthUser']['id'],
                                            'iclass' => $iclass,
                                            'action' => substr(Configure::read('Portal.Application'),0, -1).$this->PortalHelper->makeUrl($this->name, 'view', 'id=' . $appt_id),
                                        );
                                        $socket = new HttpSocket();
                                        $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);                                               
                                    }
                                }
                            }else{
                                if($alert == 'Y'){
                                    if(!empty($remind_time_value)){
                                        $notidate = date('Y-m-d H:i:s', strtotime($startshow.' - '.$remind_time_value.' minutes')); 
                                        
                                        $data_noti = array(
                                            'time' => $notidate,
                                            'to_id' => $currentUser['AuthUser']['id'],
                                            'message' => $sendnotidetail,
                                            'from_id' => $currentUser['AuthUser']['id'],
                                            'iclass' => $iclass,
                                            'action' => substr(Configure::read('Portal.Application'),0, -1).$this->PortalHelper->makeUrl($this->name, 'view', 'id=' . $appt_id),
                                        );
                                        $socket = new HttpSocket();
                                        $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);                                        
                                    }
                                }
                                
                                if($repeat_alert == 'Y'){
                                    if(!empty($remind_time_repeat_value)){
                                        $notirepeatdate = date('Y-m-d H:i:s', strtotime($startshow.' - '.$remind_time_repeat_value.' minutes')); 
                                        
                                        $data_noti = array(
                                            'time' => $notirepeatdate,
                                            'to_id' => $currentUser['AuthUser']['id'],
                                            'message' => $sendnotidetail,
                                            'from_id' => $currentUser['AuthUser']['id'],
                                            'iclass' => $iclass,
                                            'action' => substr(Configure::read('Portal.Application'),0, -1).$this->PortalHelper->makeUrl($this->name, 'view', 'id=' . $appt_id),
                                        );
                                        $socket = new HttpSocket();
                                        $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);                                               
                                    }
                                }
                            }
                    /**********************************************/  
                        
                    }
                    $url = $this->PortalHelper->makeUrl('appointments','calendar');
                    $this->redirect($url);
               }else{
                   $this->Session->setFlash(__('Failed'));
               }
               
           }else{
               
                $this->request->data['Appointment']['user_id'] = $this->request->query['user_id']; 

                $breadCrumbs = array(
                    array(
                        'label' => __('My Appointments'),
                        'link' => array('controller' => $this->name, 'action' => 'calendar', 'query' => '', 'param' => '')
                    ),
                    array(
                        'label' => __('New Appointment'),
                        'link' => false
                    ),
                );

                $textlabel = __('New Appointment');  
                $size = Utility::checkfilesize(Configure::read('Config.MaxFileSize'));
                
                
                $this->set("breadCrumbs", $breadCrumbs);
                $this->set("size", $size);
                $this->set("chkcolor", '');
                $this->set("all", '');
                $this->set("alway", '');    
           }
       } 
       
       public function edit(){
            $currentUser = $this->Session->read('AuthUser');
            $getdata = $this->request->query; 
            $email = 'N';
            $sms = 'N';
            $isall = 'N';
            $isalway = 'N';
            $remind_time_value = '';
            $remind_time_unit = '';
            $remind_time_repeat_value = '';
            $remind_time_repeat_unit = '';  
           if($this->request->data){
               
            $this->request->data['Appointment']['alert_email'] = 'N';
            $this->request->data['Appointment']['alert_sms'] = 'N';  
            
                if(!empty($this->request->data['Appointment']['is_all_day'])){
                  $this->request->data['Appointment']['is_all_day'] ='Y';
                }

                if(!empty($this->request->data['Appointment']['is_always'])){
                  $this->request->data['Appointment']['is_always'] ='Y'; 
                }

                if($this->request->data['Appointment']['is_alert_repeat'] == 0){
                  $repeat_alert = 'N';
                }else{
                  $repeat_alert = 'Y';
                }

                if($this->request->data['Appointment']['is_alert'] == 0){
                  $alert = 'N';
                }else{
                  $alert = 'Y';
                }

                $start_date = Utility::cdate($this->request->data['Appointment']['start_date'], 'd/m/Y', 'Y-m-d');
                $end_date = Utility::cdate($this->request->data['Appointment']['end_date'], 'd/m/Y', 'Y-m-d');

                $this->request->data['Appointment']['fhour'] = str_pad($this->request->data['Appointment']['fhour'], 2, "0", STR_PAD_LEFT);
                $this->request->data['Appointment']['thour'] = str_pad($this->request->data['Appointment']['thour'], 2, "0", STR_PAD_LEFT);
                        
                    /******************** check case start date same end date  ****************/
                    if($start_date == $end_date){
                        if($this->request->data['Appointment']['fhour'] == 'all'){
                            $start_time = '08:30:00';
                            $end_time = '16:30:00';
                            $isall = 'Y';  
                        }else if($this->request->data['Appointment']['fhour'] == 'morning'){
                            $start_time = '08:30:00';
                            $end_time = '12:00:00';  
                        }else if($this->request->data['Appointment']['fhour'] == 'afternoon'){
                            $start_time = '13:00:00';
                            $end_time = '16:30:00';
                        }else{
                            $start_time = $this->request->data['Appointment']['fhour'].':'.$this->request->data['Appointment']['fmin'].':00';
                                if($this->request->data['Appointment']['thour'] == 'all'){
                                   $end_time = '16:30:00'; 

                                }else if($this->request->data['Appointment']['thour'] == 'morning'){
                                   $end_time = '12:00:00';

                                }else{
                                   $end_time = $this->request->data['Appointment']['thour'].':'.$this->request->data['Appointment']['tmin'].':00';  
                                }
                        }
                    /******************** End check case start date same end date  ****************/
                    }else{
                        /************** check case start date diff end date ***************/ 
                        if($this->request->data['Appointment']['fhour'] == 'all'){
                                $start_time = '08:30:00';
                                if($this->request->data['Appointment']['thour'] == 'all'){
                                   $end_time = '16:30:00'; 
                                   $isall = 'Y';

                                }else if($this->request->data['Appointment']['thour'] == 'morning'){
                                   $end_time = '12:00:00';

                                }else{
                                   $end_time = $this->request->data['Appointment']['thour'].':'.$this->request->data['Appointment']['tmin'].':00';  
                                }

                        }else if($this->request->data['Appointment']['fhour'] == 'morning'){
                               $start_time = '08:30:00';
                               if($this->request->data['Appointment']['thour'] == 'all'){
                                   $end_time = '16:30:00';

                               }else if($this->request->data['Appointment']['thour'] == 'morning'){
                                   $end_time = '12:00:00';

                               }else{
                                   $end_time = $this->request->data['Appointment']['thour'].':'.$this->request->data['Appointment']['tmin'].':00';  

                               }

                        }else if($this->request->data['Appointment']['fhour'] == 'afternoon'){
                               $start_time = '13:00:00';
                               if($this->request->data['Appointment']['thour'] == 'all'){
                                   $end_time = '16:30:00'; 

                               }else if($this->request->data['Appointment']['thour'] == 'morning'){
                                   $end_time = '12:00:00'; 

                               }else{
                                   $end_time = $this->request->data['Appointment']['thour'].':'.$this->request->data['Appointment']['tmin'].':00';  

                               }
                        }else{
                               $start_time = $this->request->data['Appointment']['fhour'].':'.$this->request->data['Appointment']['fmin'].':00'; 
                               if($this->request->data['Appointment']['thour'] == 'all'){
                                   $end_time = '16:30:00'; 

                               }else if($this->request->data['Appointment']['thour'] == 'morning'){
                                   $end_time = '12:00:00'; 

                               }else{
                                   $end_time = $this->request->data['Appointment']['thour'].':'.$this->request->data['Appointment']['tmin'].':00';  

                               }
                        }
                        /************** End check case start date diff end date ***************/  
                    }
                    
                    $this->request->data['Appointment']['start_date'] = $start_date.' '.$start_time;
                    $this->request->data['Appointment']['end_date'] = $end_date.' '.$end_time;
                    
                    $remind_time = $this->AppointmentAlert->find('first', array(
                        'conditions'=>array(
                            'AppointmentAlert.id'=>$this->request->data['Appointment']['alert_type_id']
                        )
                    ));
                    
                    $remind_repeat_time = $this->AppointmentAlert->find('first', array(
                        'conditions'=>array(
                            'AppointmentAlert.id'=>$this->request->data['Appointment']['alert_repeat_type_id']
                        )
                    ));
                    
                    switch($remind_time['AppointmentAlert']['alert_name']){
                        case 'None' :  break;
                        
                        case 'At Time of event' :  $remind_time_value = '0';
                                                   $remind_time_unit = 'M'; 
                                                   break;
                                               
                        case '5 minutes before' :  $remind_time_value = '5';
                                                   $remind_time_unit = 'M'; 
                                                   break;
                                               
                        case '15 minutes before' : $remind_time_value = '15';
                                                   $remind_time_unit = 'M'; 
                                                   break;
                                               
                        case '30 minutes before' : $remind_time_value = '30';
                                                   $remind_time_unit = 'M'; 
                                                   break;
                                               
                        case '1 hour before' :     $remind_time_value = '60';
                                                   $remind_time_unit = 'H'; 
                                                   break;
                                               
                        case '2 hours before' :    $remind_time_value = '120';
                                                   $remind_time_unit = 'H'; 
                                                   break;
                                               
                        case '1 day before' :      $remind_time_value = '1440';
                                                   $remind_time_unit = 'D'; 
                                                   break;
                                               
                        case '2 days before' :     $remind_time_value = '2880';
                                                   $remind_time_unit = 'D'; 
                                                   break;
                                               
                        default:		   break;           
                    }

                    switch($remind_repeat_time['AppointmentAlert']['alert_name']){
                        case 'None' :  break;
                        
                        case 'At Time of event' :   $remind_time_repeat_value = '0';
                                                     $remind_time_repeat_unit = 'M'; 
                                                    break;
                                                
                        case '5 minutes before' :    $remind_time_repeat_value = '5';
                                                     $remind_time_repeat_unit = 'M'; 
                                                    break;
                                                
                        case '15 minutes before' :   $remind_time_repeat_value = '15';
                                                     $remind_time_repeat_unit = 'M'; 
                                                    break;
                                                
                        case '30 minutes before' :   $remind_time_repeat_value = '30';
                                                     $remind_time_repeat_unit = 'M'; 
                                                    break;
                                                
                        case '1 hour before' :       $remind_time_repeat_value = '60';
                                                     $remind_time_repeat_unit = 'H'; 
                                                    break;
                                                
                        case '2 hours before' :      $remind_time_repeat_value = '120';
                                                     $remind_time_repeat_unit = 'H'; 
                                                    break;
                                                
                        case '1 day before' :        $remind_time_repeat_value = '1440';
                                                     $remind_time_repeat_unit = 'D'; 
                                                    break;
                                                
                        case '2 days before' :       $remind_time_repeat_value = '2880';
                                                     $remind_time_repeat_unit = 'D'; 
                                                    break;
                                                
                        default:		    break;                
                    }
                    
                    if (!isset($this->request->data['Appointment']['attachment'])) {
	                    if (isset($this->request->data['Appointment']['attach1'])) {
	                        if (!empty($this->request->data['Appointment']['attach1']['tmp_name'])) {
	                            $post = $this->request->data['Appointment']['attach1'];
	                            $tmp_name = $post['tmp_name'];
	                            $file_name = $post['name'];
	                            $file_error = $post['error'];
	                            $file_size = $post['size'];
	                            $file_type = $post['type'];

	                            $attach_key = $this->addAttachment($post, $tmp_name, $file_name, $file_error, $file_size, $file_type, $currentUser['AuthUser']['id']);
				    //pr($attach_key);
	                            $this->request->data['Appointment']['attachment_name'] = $attach_key['original_name'];
	                            $this->request->data['Appointment']['attachment'] = $attach_key['key'];
	                        }else{
	                            $this->request->data['Appointment']['attachment'] = '';
	                            $this->request->data['Appointment']['attachment_name'] = '';
	                        }
	                    }
		    }
                    
                    $this->request->data['Appointment']['alert_email'] = $email;
                    $this->request->data['Appointment']['alert_sms'] = $sms;
                    $this->request->data['Appointment']['is_all_day'] = $isall;
                    $this->request->data['Appointment']['is_always'] = $isalway;   
                    $this->request->data['Appointment']['is_alert'] = $alert;
                    $this->request->data['Appointment']['is_alert_repeat'] = $repeat_alert;
                    $this->request->data['Appointment']['remind_time_value'] = $remind_time_value;
                    $this->request->data['Appointment']['remind_time_unit'] = $remind_time_unit;
                    $this->request->data['Appointment']['remind_time_repeat_value'] = $remind_time_repeat_value;
                    $this->request->data['Appointment']['remind_time_repeat_unit'] = $remind_time_repeat_unit;
                    
		    $data_array = array();
                    $this->Appointment->id = $this->request->data['Appointment']['id'];
                    $data_array['Appointment']['deleted'] = 'Y';
                    
                    // Delete Appointment
                    if($this->Appointment->save($data_array)){
                        $showuserforupdate = $this->AppointmentUserReader->find('list', array(
                           'conditions'=>array(
                               'AppointmentUserReader.deleted' => 'N',
                               'AppointmentUserReader.appointment_id'=> $this->request->data['Appointment']['id']
                           ),
                           'fields' => array('AppointmentUserReader.user_id')
                        ));  
                        
                        // check Reader
                        if(!empty($showuserforupdate)){
 
                            $iclass_edit = '10202';
                            $updated_word = __('This Appointment: '.strip_tags($this->request->data['Appointment']['title']).' was updated');

                            $noti_update_user = implode(',', $showuserforupdate);
                            $data_noti = array(
                                'time' => date('Y-m-d H:i:s'),
                                'to_id' => $noti_update_user,
                                'message' => $updated_word,
                                'from_id' => $currentUser['AuthUser']['id'],
                                'iclass' => $iclass_edit,
                                'action' => substr(Configure::read('Portal.Application'),0, -1).$this->PortalHelper->makeUrl($this->name, 'calendar'),
                            );
                            $socket = new HttpSocket();
                            $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);  
                        }
                        $this->AppointmentUserReader->query("UPDATE eportal.pg_ab_appointment_users SET deleted='Y' WHERE (appointment_id=".$this->request->data['Appointment']['id'].")");
                         
                    }
                    // End Delete Appointment
                    $this->Appointment->create();
                    $this->request->data['Appointment']['id'] = '';
                    if($this->Appointment->save($this->request->data)){
                        $appt_id = $this->Appointment->getLastInsertId();
                        if(!empty($appt_id)){
                           if(!empty($this->request->data['AppointmentUserReader'])){
                                    sort($this->request->data['AppointmentUserReader']);
                                    $socket = new HttpSocket();   
                                    $countreader = count($this->request->data['AppointmentUserReader']);
                                    $i = 1;
                                    $manyid = '';
                                    $lang = 1;
                                    foreach($this->request->data['AppointmentUserReader'] as $key => $userId){
                                        $data['list_user_id'] = $userId['user_id'];
                                        $dataUser = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', $data);
                                        $showdatauser = json_decode($dataUser, true);
                                        $showownerappt = $showdatauser['Result']['UserProfile'];

                                        /******************************* get Belong ****************************************/
                                        $data_detail = array();
                                        $data_detail['list_org_id'] = $showownerappt[0]['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['parent_id'];

                                        $getBelongOrganization = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganizationName/.json', $data_detail);
                                        $getbelongOrganize = json_decode($getBelongOrganization, true);
                                        foreach($getbelongOrganize['Result']['OrganizationName'] as $j=>$BelongOrganization){
                                            if($lang == $BelongOrganization['OrganizationDescription']['language_id']){
                                                $desc_belong = $BelongOrganization['OrganizationDescription']['description'];
                                                //$this->request->data['Content']['belong_organization_name'] = $desc_belong;
                                                break;
                                            }
                                        }
                                        /***********************************************************************************/
                                        $this->AppointmentUserReader->query("INSERT INTO pg_ab_appointment_users (created_date, created_user_id, updated_date, updated_user_id, confirmed_status, appointment_id, user_id, published, first_name, last_name, position_name, organization_name, belong_organization_name, start_date, end_date, privacy_type_id) 
                                        VALUES ('".date('Y-m-d H:i:s')."', '".$currentUser['AuthUser']['id']."', '".date('Y-m-d H:i:s')."', '".$currentUser['AuthUser']['id']."', 'Wait for confirm', '".$appt_id."', '".$userId['user_id']."', 'N', '".$showownerappt[0]['UserProfile']['first_name_th']."', '".$showownerappt[0]['UserProfile']['last_name_th']."', '".$showownerappt[0]['UserOrganizationPosition'][0]['OrganizationPosition']['Position']['position_name']."', '".$showownerappt[0]['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['OrganizationDescription'][0]['description']."', '".$desc_belong."', '".$this->request->data['Appointment']['start_date']."', '".$this->request->data['Appointment']['end_date']."', '".$this->request->data['Appointment']['privacy_type_id']."' )");

                                    }      
                           }
                           //set noti
                            $iclass = '10201';
                            $sendnotidetail = strip_tags($this->request->data['Appointment']['detail']);

                            $showdatanoti = $this->AppointmentUserReader->find('list', array(
                                'conditions'=>array(
                                    'AppointmentUserReader.deleted' => 'N',
                                    'AppointmentUserReader.appointment_id'=> $appt_id
                                ),
                                'fields' => array('AppointmentUserReader.user_id')
                            ));                            
                            $notiuser = implode(',', $showdatanoti);
                            if(!empty($notiuser)){
                                $data_noti = array(
                                    'time' => date('Y-m-d H:i:s'),
                                    'to_id' => $notiuser,
                                    'message' => $sendnotidetail,
                                    'from_id' => $currentUser['AuthUser']['id'],
                                    'iclass' => $iclass,
                                    'action' => substr(Configure::read('Portal.Application'),0, -1).$this->PortalHelper->makeUrl($this->name, 'view', 'id=' . $appt_id),
                                );
                                $socket = new HttpSocket();
                                $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);
                                
                                if($alert == 'Y'){
                                    if(!empty($remind_time_value)){
                                        $notidate = date('Y-m-d H:i:s', strtotime($this->request->data['Appointment']['start_date'].' - '.$remind_time_value.' minutes')); 
                                        $notiuser_remind = $notiuser.",".$currentUser['AuthUser']['id'];
                                        $data_noti = array(
                                            'time' => $notidate,
                                            'to_id' => $notiuser_remind,
                                            'message' => $sendnotidetail,
                                            'from_id' => $currentUser['AuthUser']['id'],
                                            'iclass' => $iclass,
                                            'action' => substr(Configure::read('Portal.Application'),0, -1).$this->PortalHelper->makeUrl($this->name, 'view', 'id=' . $appt_id),
                                        );
                                        $socket = new HttpSocket();
                                        $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);                                        
                                    }
                                }
                            
                                if($repeat_alert == 'Y'){
                                    if(!empty($remind_time_repeat_value)){
                                        $notirepeatdate = date('Y-m-d H:i:s', strtotime($this->request->data['Appointment']['start_date'].' - '.$remind_time_repeat_value.' minutes')); 
                                        $notiuser_remind_repeat = $notiuser.",".$currentUser['AuthUser']['id'];
                                        $data_noti = array(
                                            'time' => $notirepeatdate,
                                            'to_id' => $notiuser_remind_repeat,
                                            'message' => $sendnotidetail,
                                            'from_id' => $currentUser['AuthUser']['id'],
                                            'iclass' => $iclass,
                                            'action' => substr(Configure::read('Portal.Application'),0, -1).$this->PortalHelper->makeUrl($this->name, 'view', 'id=' . $appt_id),
                                        );
                                        $socket = new HttpSocket();
                                        $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);                                               
                                    }
                                } 
                            }else{
                                if($alert == 'Y'){
                                    if(!empty($remind_time_value)){
                                        $notidate = date('Y-m-d H:i:s', strtotime($this->request->data['Appointment']['start_date'].' - '.$remind_time_value.' minutes')); 

                                        $data_noti = array(
                                            'time' => $notidate,
                                            'to_id' => $currentUser['AuthUser']['id'],
                                            'message' => $sendnotidetail,
                                            'from_id' => $currentUser['AuthUser']['id'],
                                            'iclass' => $iclass,
                                            'action' => substr(Configure::read('Portal.Application'),0, -1).$this->PortalHelper->makeUrl($this->name, 'view', 'id=' . $appt_id),
                                        );
                                        $socket = new HttpSocket();
                                        $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);                                        
                                    }
                                } 

                                if($repeat_alert == 'Y'){
                                    if(!empty($remind_time_repeat_value)){
                                        $notirepeatdate = date('Y-m-d H:i:s', strtotime($this->request->data['Appointment']['start_date'].' - '.$remind_time_repeat_value.' minutes')); 

                                        $data_noti = array(
                                            'time' => $notirepeatdate,
                                            'to_id' => $currentUser['AuthUser']['id'],
                                            'message' => $sendnotidetail,
                                            'from_id' => $currentUser['AuthUser']['id'],
                                            'iclass' => $iclass,
                                            'action' => substr(Configure::read('Portal.Application'),0, -1).$this->PortalHelper->makeUrl($this->name, 'view', 'id=' . $appt_id),
                                        );
                                        $socket = new HttpSocket();
                                        $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);                                               
                                    }
                                }      
                            }
                        }
                        
                        
                        $url = $this->PortalHelper->makeUrl('appointments','calendar');
                        $this->redirect($url);
                    }else{
                        $this->Session->setFlash(__('Failed'));
                    }
                    
           }else{
                
                $breadCrumbs = array(
                      array(
                          'label' => __('My Appointments'),
                          'link' => array('controller' => $this->name, 'action' => 'calendar', 'query' => '', 'param' => '')
                      ),
                      array(
                          'label' => __('Edit Appointment'),
                          'link' => false
                      ),
                );
                $appointment = $this->Appointment->find('first', array(
                    'conditions' => array(
                        'Appointment.id' => $getdata['id'],
                        'Appointment.deleted' =>'N'
                    )
                ));  
                
                if(empty($appointment)){
                    $url = $this->PortalHelper->makeUrl('appointments','calendar');
                    $this->redirect($url);
                }
                
                /****************** Alert Remind *********************/
                if($appointment['Appointment']['alert_email'] == 'Y'){
                    $appointment['Appointment']['remind'] = 'email';
                }else if($appointment['Appointment']['alert_sms'] == 'Y'){
                    $appointment['Appointment']['remind'] = 'sms';
                }else{
                    $appointment['Appointment']['remind'] = '';
                }
                /*****************************************************/
                
                /*************** Check Alert & Repeat ****************/
                if($appointment['Appointment']['is_alert'] == 'Y'){
                    $appointment['Appointment']['is_alert'] = 1;
                }else{
                    $appointment['Appointment']['is_alert'] = 0;
                }
                
                if($appointment['Appointment']['is_alert_repeat'] == 'Y'){
                    $appointment['Appointment']['is_alert_repeat'] = 1;
                }else{
                    $appointment['Appointment']['is_alert_repeat'] = 0;
                }
                /*****************************************************/
                
                /************** Change Format Date Time **************/
                $start = Utility::cdate($appointment['Appointment']['start_date'], 'Y-m-d H:i:s', 'd/m/Y');
                $fhour = Utility::cdate($appointment['Appointment']['start_date'], 'Y-m-d H:i:s', 'H');
                $fmin = Utility::cdate($appointment['Appointment']['start_date'], 'Y-m-d H:i:s', 'i');


                $end = Utility::cdate($appointment['Appointment']['end_date'], 'Y-m-d H:i:s', 'd/m/Y');   
                $thour = Utility::cdate($appointment['Appointment']['end_date'], 'Y-m-d H:i:s', 'H');
                $tmin = Utility::cdate($appointment['Appointment']['end_date'], 'Y-m-d H:i:s', 'i');

                $appointment['Appointment']['start_date'] = $start;
                //$appointment['Appointment']['ftime'] = $ftime;
                $appointment['Appointment']['fhour'] = $fhour;
                $appointment['Appointment']['fmin'] = $fmin;
                $appointment['Appointment']['end_date'] = $end;
                //$appointment['Appointment']['ttime'] = $ttime;
                $appointment['Appointment']['thour'] = $thour;
                $appointment['Appointment']['tmin'] = $tmin;
                
                $appointment['Appointment']['title'] = $appointment['Appointment']['name'];
                /*****************************************************/
                $chkuser = $this->AppointmentUserReader->find('all', array(
                    'conditions' => array(
                        'AppointmentUserReader.appointment_id' =>$this->request->query['id'],
                        'AppointmentUserReader.deleted = ' => 'N'
                    ),
                    'fields' => array('AppointmentUserReader.first_name', 'AppointmentUserReader.last_name', 'AppointmentUserReader.user_id')
                ));
               
                $this->request->data = $appointment;  
                
                $textlabel = __('Edit Appointment');    
                $size = Utility::checkfilesize(Configure::read('Config.MaxFileSize'));
                
                $this->set("breadCrumbs", $breadCrumbs);
                $this->set('size', $size);
                $this->request->data = $appointment;
                $this->set('userread', $chkuser);
                $this->set("chkcolor", $appointment['Appointment']['color_id']);
                $this->set("all", $appointment['Appointment']['is_all_day']);
                $this->set("alway", $appointment['Appointment']['is_always']);
               
           }     
       } 
       
       
       public function insertappointment(){
             $currentUser = $this->Session->read('AuthUser');
             $is_chk = '';
             if ($this->PortalHelper->isPost()) {
                  $datas = $this->data;
                    if(empty($datas['color_id'])){
                        $datas['color_id'] = '1';
                    }
                    if($datas['user_id'] == $datas['handle']){
                        $handle = '';
                    }else{
                        $handle =  $datas['handle'];
                    }
                    $start_date = Utility::cdate($datas['start_date'], 'd/m/Y', 'Y-m-d');
                    $end_date = Utility::cdate($datas['end_date'], 'd/m/Y', 'Y-m-d');
                    $data = array();

                    $data['Appointment']['name'] = $datas['title'] ;
                    $data['Appointment']['detail'] = $datas['detail'];
                    $data['Appointment']['private_note'] = $datas['private_note'];
                    $data['Appointment']['start_date'] = $start_date.' '.$datas['start_time'].':00';
                    $data['Appointment']['end_date'] = $end_date.' '.$datas['end_time'].':00';
                    $data['Appointment']['priority_type_id'] = $datas['priority_type_id'];  
                    $data['Appointment']['location_type_id'] = $datas['location_type_id']; 
                    $data['Appointment']['privacy_type_id'] = $datas['privacy_type_id']; 
                    $data['Appointment']['location'] = $datas['location'];
                    $data['Appointment']['is_all_day'] = $datas['is_all_day'];
                    $data['Appointment']['is_always'] = $datas['is_always'];
                    //$data['Appointment']['status_type_id'] = $datas['status_type_id']; 
                    $data['Appointment']['color_id'] = $datas['color_id'];
                    $data['Appointment']['user_id'] = $datas['user_id'];
                    $data['Appointment']['handle_user_id'] = $handle;
                  
                  $this->Appointment->create();
                  if($this->Appointment->save($data['Appointment'])){
                      $is_chk = $this->Appointment->getLastInsertId();
                      echo $is_chk;
                  }
               
                                                              
                 //$this->set("showflg", $is_chk);  
             }   
                     
             $this->autoLayout =false;
             $this->autoRender = false;
             $this->layout = "blank";

       } 
       public function view($id = null){
           //pr($this->request->query);
           $currentUser = $this->Session->read('AuthUser');
           if ($this->PortalHelper->isPost()) {
              /*************** Post Data ***************************/ 
               $appt_id = $this->request->data['Appointment']['id'];
               $userread_confirmed = $this->request->data['AppointmentUserReader']['confirmed_status'];
               $reason = $this->request->data['AppointmentUserReader']['reason'];
               $user_id = $currentUser['AuthUser']['id'];
               $chkid =  $this->AppointmentUserReader->find('first', array(
                    'conditions'=> array(
                        'AppointmentUserReader.appointment_id ' => $appt_id,
                        'AppointmentUserReader.user_id ' =>  $user_id,
                        'AppointmentUserReader.deleted = ' =>  'N',
                    ),
                    'fields' => array('AppointmentUserReader.id')
                ));
               
              $update = $this->AppointmentUserReader->updateAll(
                      array(
                        'AppointmentUserReader.confirmed_status' => "'".$userread_confirmed."'",
                        'AppointmentUserReader.reason' => "'".$reason."'",
                        'AppointmentUserReader.confirmed_date' => "'".date('Y-m-d H:i:s')."'",
                      ),
                      array('AppointmentUserReader.id' => $chkid['AppointmentUserReader']['id'])
              );
              
              $url = $this->PortalHelper->makeUrl('appointments','view','id='.$appt_id);
              $this->redirect($url); 
           }else{
              /*************** Show data in page view **************/
            $breadCrumbs = array(
                array(
                    'label' => 'My Appointments',
                    'link' => array('controller' => $this->name, 'action' => 'calendar', 'query' => '', 'param' => '')
                ),
                array(
                    'label' => 'View Appointment',
                    'link' => false
                ),
            );
              if(empty($id)){
                $id = $this->request->query['id']; 
              }
                $viewappoint = $this->Appointment->find('first',array(
                    'conditions' => array('Appointment.id'=> $id, 'Appointment.deleted'=> 'N'  )
                )); 
                
                if(empty($viewappoint)){
                    $url = $this->PortalHelper->makeUrl('appointments','calendar');
                    $this->redirect($url);  
                }
                
                $getuser = $viewappoint['Appointment']['user_id']; 

                $dataUser = array();
                $dataUser['list_user_id'] = $getuser;
                $socket = new HttpSocket();
                $dataUser = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', $dataUser);
                $showdatauser = json_decode($dataUser, true);
                $showuser = $showdatauser['Result']['UserProfile'];

                
                $created_by_user = $showuser[0]['UserProfile']['first_name_th'].'&nbsp;'.$showuser[0]['UserProfile']['last_name_th'];

                $chkuser = $this->AppointmentUserReader->find('all', array(
                    'conditions' => array(
                        'AppointmentUserReader.appointment_id' => $id,
                        'AppointmentUserReader.deleted = ' => 'N',
                    ),
                    'fields' =>array('AppointmentUserReader.user_id','AppointmentUserReader.reason','AppointmentUserReader.confirmed_status','AppointmentUserReader.confirmed_date', 'AppointmentUserReader.first_name', 'AppointmentUserReader.last_name', 'AppointmentUserReader.organization_name', 'AppointmentUserReader.belong_organization_name' ),
                ));

                foreach($chkuser as $key=>$reader){

                  $chkuser[$key]['confirmdate'] = '';
                  $chkuser[$key]['confirmtime'] = '';

                  if(!empty($chkuser['AppointmentUserReader']['confirmed_date'])){
                    $chkuser_date = Utility::cdate($chkuser['AppointmentUserReader']['confirmed_date'], 'Y-m-d H:i:s', 'Y-m-d');
                    $chkuser_time = Utility::cdate($chkuser['AppointmentUserReader']['confirmed_date'], 'Y-m-d H:i:s', 'H:i:s');

                    $chkuser[$key]['confirmdate'] = $chkuser_date;
                    $chkuser[$key]['confirmtime'] = $chkuser_time;
                  }



                }
                


               //$this->request->data['Appointment']['id'] = $id;     
               $this->set("breadCrumbs", $breadCrumbs); 
               $this->set("userappt", $viewappoint);
               $this->set("chkuser", $chkuser);
               $this->set("create_by", $created_by_user);
               /*if(!empty($id)){
                
                pr($viewappoint);
              }else{
                $url = $this->PortalHelper->makeUrl('appointments','calendar');
                $this->redirect($url);
              }*/
              /****************************************************/
              
           }
       } 
       
       public function delete($id = null){
           $currentUser = $this->Session->read('AuthUser');
           if ($this->PortalHelper->isPost()) {
               $url = $this->PortalHelper->makeUrl('appointments','calendar');
               
               if(empty($id)){
                    if(isset($this->data['id'])){
                        $id = $this->data['id'];
                    }else{
                        $this->redirect($url);
                    }
               }
               $showdatanoti = $this->Appointment->find('first', array(
                   'conditions' => array(
                       'Appointment.id' => $id,
                       'Appointment.deleted' => 'N'
                   )
               ));
               
               $update = $this->Appointment->updateAll(
                    array('Appointment.deleted' => "'Y'"),
                    array('Appointment.id' => $id)
               );
               
               $showuserforupdate = $this->AppointmentUserReader->find('list', array(
                   'conditions'=>array(
                       'AppointmentUserReader.deleted' => 'N',
                       'AppointmentUserReader.appointment_id'=> $showdatanoti['Appointment']['id']
                   ),
                   'fields' => array('AppointmentUserReader.user_id')
               )); 
               
               if(!empty($showuserforupdate)){
                    $iclass = '10203';
                    $deleted_word = 'This Appointment: '.strip_tags($showdatanoti['Appointment']['name']).' was deleted';

                    $notiuser = implode(',', $showuserforupdate);
                    $data_noti = array(
                        'time' => date('Y-m-d H:i:s'),
                        'to_id' => $notiuser,
                        'message' => $deleted_word,
                        'from_id' => $currentUser['AuthUser']['id'],
                        'iclass' => $iclass,
                        'action' => substr(Configure::read('Portal.Application'),0, -1).$this->PortalHelper->makeUrl($this->name, 'calendar'),
                    );
                    $socket = new HttpSocket();
                    $getUserProfileResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'addUserAlert', $data_noti);  
               }
               
               $delReader = $this->AppointmentUserReader->query("UPDATE eportal.pg_ab_appointment_users SET deleted='Y' WHERE (appointment_id=".$id.")");
                
               if($update){
                   echo "1";
               }
//               $url = $this->PortalHelper->makeUrl('Appointments','calendar');
//               $this->redirect($url);
                
           }
            $this->autoLayout =false;
            $this->autoRender = false;
            $this->layout = "blank";
       }
       
 

    public function indexhandle() {
        //pr($this->request->data()); die();
        $currentUser = $this->Session->read('AuthUser');
       
        $breadCrumbs = array(
            array(
                'label' => __('My Appointment'),
                'link' => array('controller' => 'appointments', 'action' => 'calendar', 'query' => '', 'param' => '')
            ),
            array(
                'label' => __('My Appointment Handler'),
                'link' => false,
            ),
        );
        $this->set("breadCrumbs", $breadCrumbs);
		//pr($this->AppointmentHandle->find('all'));
		//die(); 
        $findhandle = $this->AppointmentHandle->find('all', array(
            'conditions' =>
            array(
                'AppointmentHandle.deleted' => 'N',
                'AppointmentHandle.user_id' => $currentUser['AuthUser']['id'],
                'AppointmentHandle.handle_user_id != ' =>  '0',
            )
          )
        );
        
        /*$socket = new HttpSocket();
        $showuser = array();
        $countdata = count($findhandle);
        $i = 1 ;
        $manyid = '';
        $userid = '';
        $chkname = '';
        //pr($countdata);
        foreach($findhandle as $key => $user){

          if($i == $countdata){
              $manyid .= $user['AppointmentHandle']['handle_user_id'];
              $data['list_user_id'] = $manyid;

              $dataUser = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', $data);
              $showdatauser = json_decode($dataUser, true);
              $showuser = $showdatauser['Result']['UserProfile'];

          }else{
              $manyid .= $user['AppointmentHandle']['handle_user_id'].', ';
          }  
          $i++;
        }*/
        //$chkname['AppointmentHandle'] = $showuser;
        
        $this->set("findhandle", $findhandle);
        //$this->set('chkname', $chkname);
    }
    
    public function deletedhandle($id = null) {
        $this->AppointmentHandle->updateAll(array('AppointmentHandle.deleted' => "'Y'"), array('AppointmentHandle.id' => $id));
        $url = $this->PortalHelper->makeUrl('appointments','indexhandle');
        $this->redirect($url);
    }

    public function addhandle($id = null, $name = null) {
        $this->layout = 'blank';
        $this->autoRender = false;
        $currentUser = $this->Session->read('AuthUser');
        $chk = 'N';
        $url = $this->PortalHelper->makeUrl('appointments', 'indexhandle');
        $data = array();
        
        $owner_name = $currentUser['AuthUserProfile'][0]['first_name_th']." ".$currentUser['AuthUserProfile'][0]['last_name_th'];
       
        $chkhandle = $this->AppointmentHandle->find('all', array(
                'conditions' => array(
                    'AppointmentHandle.user_id' => $currentUser['AuthUser']['id'],
                    'AppointmentHandle.deleted' => 'N',
                )
        ));
        
        if(empty($chkhandle) && $id != $currentUser['AuthUser']['id']){
            $data['AppointmentHandle']['user_id'] = $currentUser['AuthUser']['id'];
            $data['AppointmentHandle']['handle_user_id'] = $id;
            $data['AppointmentHandle']['handle_name'] = $name;
            $data['AppointmentHandle']['owner_name'] = $owner_name;
            $this->AppointmentHandle->create();
            $this->AppointmentHandle->save($data);   
            $this->redirect($url);
        }else{
           if($id == $currentUser['AuthUser']['id']){
                    $chk = 'Y';
           }
            foreach($chkhandle as $handle){
                if($handle['AppointmentHandle']['handle_user_id'] == $id ){
                       $chk = 'Y';
                       break;                        
                }
            }
           
            if($chk == 'N'){
                $data['AppointmentHandle']['user_id'] = $currentUser['AuthUser']['id'];
                $data['AppointmentHandle']['handle_user_id'] = $id;
                $data['AppointmentHandle']['handle_name'] = $name;
                $data['AppointmentHandle']['owner_name'] = $owner_name;
                $this->AppointmentHandle->create();
                $this->AppointmentHandle->save($data);   
                $this->redirect($url);
            }else{
                $this->redirect($url);
            }
        }
    }

    public function addmanyhandle($getdata = null ) {
        $this->autoRender = false;
        $currentUser = $this->Session->read('AuthUser');
        $user = $currentUser['AuthUser']['id'];
        $array_come = explode(',', $getdata);
        $owner_name = $currentUser['AuthUserProfile'][0]['first_name_th']." ".$currentUser['AuthUserProfile'][0]['last_name_th'];
        
        //pr($array_come);
        $chkhandle = $this->AppointmentHandle->find('all', array(
                'conditions' => array(
                    'AppointmentHandle.user_id' => $currentUser['AuthUser']['id'],
                    'AppointmentHandle.deleted' => 'N',
                )
        ));
        
        $is_dup = false;
        foreach($array_come as  $name_data){
          $getdetail = explode('=>', $name_data);
          
           foreach ($chkhandle as $i => $check) {
                if($getdetail[0] == $check['AppointmentHandle']['handle_user_id']){
                    $is_dup = true;
                    break;
                }
           }
            if ($getdetail[0] != null && !$is_dup && $getdetail[0] != $currentUser['AuthUser']['id']) {

                $data = array();
                $data['AppointmentHandle']['user_id'] = $currentUser['AuthUser']['id'];
                $data['AppointmentHandle']['handle_user_id'] = $getdetail[0];
                $data['AppointmentHandle']['handle_name'] = $getdetail[1];
                $data['AppointmentHandle']['owner_name'] = $owner_name;
                $this->AppointmentHandle->create();
                $this->AppointmentHandle->save($data); 
            }
         
        }
        $url = $this->PortalHelper->makeUrl('appointments', 'indexhandle');
        $this->redirect($url);
    }

    public function redirector() {
        $url = $this->PortalHelper->makeUrl('appointments', 'indexhandle');
        $this->redirect($url);
    }
    public function deleteAttachment($id=null){
            $this->autoRender = false;
            $this->layout = 'blank';
            if ($this->PortalHelper->isPost()) {
                $data = $this->data;
                $chkdata = array();
                $chkdata['Appointment']['attachment'] = '';
                $chkdata['Appointment']['attachment_name'] = '';
                
                $this->Appointment->id = $data['id'];
                if($this->Appointment->save($chkdata)){
                    echo '1';
                }else{
                    echo '0';
                }
            }
    }
    
    /************************* Agenda **********************/
    public function agenda($user_id = null){
        $this->layout = 'blank';
        $currentUser = $this->Session->read('AuthUser');
        $mode = '';
        $chkreader = '';
        $chkprivacy = '';
        $invite = array();
        $appointments = array();
        if(empty($user_id)){
            $user_id = $currentUser['AuthUser']['id'];
        }
        
        if(!empty($this->request->data)){
            $mode          = $this->request->data['mode'];
            $selected_year = $this->request->data['selected_year'];
            $tmp_selected_month = $this->request->data['selected_month'];
            $selected_month = $tmp_selected_month < 10 ? "0{$tmp_selected_month}" : $tmp_selected_month;
            $user_id = $this->request->data['user_id'];
        }else{
            $selected_year = date('Y');
            $selected_month = date('m');
            //$id = $this->request->query['user_id'];
        }
        
        /******************* Check Period Time *******************************/
        if(!empty($mode)){
             switch($mode){
                    case 1 :    $current_date = date('Y-m-d');
                                $alldate = array('0' =>$current_date);
                                $begin_date = $current_date." 00:00:00";
                                $last_date = $current_date." 23:59:59";
                                break;

                    case 2 :    $tomorrow = date('Y-m-d', strtotime("tomorrow"));
                                $alldate = array('0' =>$tomorrow);
                                $begin_date = $tomorrow." 00:00:00";
                                $last_date = $tomorrow." 23:59:59";
                                break; 

                    case 3 :    $start_period = date('Y-m-d', strtotime('this week -1 day'));
                                $end_period = date('Y-m-d', strtotime('next week -2 days'));
                                $addtime = 0;
                                for($i=0;$i<7;$i++){
                                       $alldate[] = Utility::addTime($addtime, 0, 0, $start_period);	
                                       $addtime = $addtime+24;
                                }
                                $begin_date = $start_period." 00:00:00";
                                $last_date = $end_period." 23:59:59";	

                                break;  


                    case 4 :    $start_period = date('Y-m-d', strtotime('first day of this month'));
                                $end_period = date("t", mktime(0, 0, 0, $selected_month, 1, $selected_year));
                                $addtime = 0;
                                for($i=0;$i<$end_period;$i++){
                                       $alldate[] = Utility::addTime($addtime, 0, 0, $start_period);	
                                       $addtime = $addtime+24;
                                }
                                $begin_date = $start_period." 00:00:00";
                                $last_date = $selected_year."-".$selected_month."-".$end_period." 23:59:59";
                                break;

                    case 5  :   $start_period = date('Y-m-d', strtotime('next week - 1 day'));
                                $end_period = date('Y-m-d', strtotime('next week + 1 week - 2 days'));

                                $addtime = 0;
                                for($i=0;$i<7;$i++){
                                       $alldate[] = Utility::addTime($addtime, 0, 0, $start_period);	
                                       $addtime = $addtime+24;
                                }
                                $begin_date = $start_period." 00:00:00";
                                $last_date = $end_period." 23:59:59";	
                                break;

                    case 6  :   $current_date = date('Y-m-d');
                                $addtime = 0;

                                for($i=0;$i<=7;$i++){
                                       $alldate[] = Utility::addTime($addtime, 0, 0, $current_date);	
                                       $addtime = $addtime+24;
                                }	
                                $end_period = Utility::cdate($alldate[7], 'Y-m-d H:i:s', 'Y-m-d');

                                $begin_date = $current_date." 00:00:00";
                                $last_date = $end_period. " 23:59:59";
                                break;

                    case 7  :   $current_date = date('Y-m-d');
                                $addtime = 0;

                                for($i=0;$i<=14;$i++){
                                       $alldate[] = Utility::addTime($addtime, 0, 0, $current_date);	
                                       $addtime = $addtime+24;
                                }	
                                $end_period = Utility::cdate($alldate[14], 'Y-m-d H:i:s', 'Y-m-d');

                                $begin_date = $current_date." 00:00:00";
                                $last_date = $end_period. " 23:59:59";
                                break;

                    case 8  :   $current_date = date('Y-m-d');
                                $addtime = 0;

                                for($i=0;$i<=30;$i++){
                                       $alldate[] = Utility::addTime($addtime, 0, 0, $current_date);	
                                       $addtime = $addtime+24;
                                }	
                                $end_period = Utility::cdate($alldate[30], 'Y-m-d H:i:s', 'Y-m-d');

                                $begin_date = $current_date." 00:00:00";
                                $last_date = $end_period. " 23:59:59";  
                                break;

                    default :   $start_period = date('Y-m-d', strtotime('first day of this month'));
                                $end_period = date("t", mktime(0, 0, 0, $selected_month, 1, $selected_year));
                                $addtime = 0;

                                for($i=0;$i<$end_period;$i++){
                                       $alldate[] = Utility::addTime($addtime, 0, 0, $start_period);	
                                       $addtime = $addtime+24;
                                }
                                $begin_date = $start_period." 00:00:00";
                                $last_date = $selected_year."-".$selected_month."-".$end_period." 23:59:59";
                                break;                           
                            
             }
        }else{		
            $start_period = $selected_year.'-'.$selected_month.'-01';	
            $end_period = date("t", mktime(0, 0, 0, $selected_month, 1, $selected_year));	

            $addtime = 0;
            for($i=0;$i<$end_period;$i++){
                   $alldate[] = Utility::addTime($addtime, 0, 0, $start_period);	
                   $addtime = $addtime+24;
            }

            $begin_date = $start_period." 00:00:00";
            $last_date = $selected_year."-".$selected_month."-".$end_period." 23:59:59";
        }
        /**********************************************************************/
        $date_range = array($begin_date, $last_date);
        $user = $this->getUserNameFromList(array($user_id));
        $reader = $this->AppointmentUserReader->find('list', array(
            'conditions' => array(
                'AppointmentUserReader.user_id' => $user_id,
                "AppointmentUserReader.confirmed_status ilike '%Accepted%'",
                'AppointmentUserReader.deleted = ' => 'N',
                'OR'=>array(
                        'AppointmentUserReader.start_date BETWEEN ? AND ?'=>$date_range,
                        'AppointmentUserReader.end_date BETWEEN ? AND ?'=>$date_range
                )
            ),
            'fields' => array('AppointmentUserReader.appointment_id')
        ));
        $get_invite = implode(',', $reader);
        
        if(!empty($get_invite)){
            $chkreader = 'Appointment.id in ('.$get_invite.')';
        }
        
        if($user_id != $currentUser['AuthUser']['id']){
            $chkprivacy = 'Appointment.privacy_type_id = 2';
        }
        
        $tmp_appointments = $this->Appointment->find('all', array(
            'conditions'=>array(
                //'Appointment.privacy_type_id' => '2',
                'Appointment.deleted'=>'N',
                'Appointment.user_id'=>$user_id,
                $chkprivacy,
                'OR'=>array(
                    
                    'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                    'Appointment.end_date BETWEEN ? AND ?'=>$date_range,
                    $chkreader,
                )
            ),
            'order'=>array('Appointment.user_id'=>'ASC', 'Appointment.start_date'=>'ASC')
        ));
        
        foreach($tmp_appointments as $app){
            $appointment = $app['Appointment'];
            $id = $appointment['id'];
            $uid = $appointment['user_id'];
            if($appointment['user_id'] != $user_id){
                $uid = $user_id; 
            }
            if(empty($last_user_id)){ 
                $last_user_id = $uid;
                $appointments[$uid]['id'] = $id;
                $appointments[$uid]['user_full_name'] = @$user[$uid]['detail'];
                $appointments[$uid]['user_id'] = $uid;
                $appointments[$uid]['event_date'][$id]['start_date'] = $appointment['start_date'];
                $appointments[$uid]['event_date'][$id]['end_date'] = $appointment['end_date'];
                $appointments[$uid]['event_date'][$id]['location_type_id'] = $appointment['location_type_id'];
                $appointments[$uid]['event_date'][$id]['appointment_name'] = $appointment['name'];
                //$appointments[$uid]['location_type_id'][$id] = $appointment['location_type_id'];
                $appointments[$uid]['event_date'][$id]['detail'] = $appointment['detail'];
            }else{
                if($last_user_id == $uid){
                    $appointments[$uid]['event_date'][$id]['start_date'] = $appointment['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $appointment['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $appointment['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $appointment['name'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $appointment['detail'];
                    //$appointments[$uid]['location_type_id'][$id] = $appointment['location_type_id']; 
                }else{
                    $appointments[$uid]['id'] = $id;
                    $appointments[$uid]['user_id'] = $uid;
                    $appointments[$uid]['user_full_name'] = @$user[$uid]['detail'];
                    $appointments[$uid]['event_date'][$id]['start_date'] = $appointment['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $appointment['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $appointment['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $appointment['name'];
                    //$appointments[$uid]['location_type_id'][$id] = $appointment['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $appointment['detail'];
                }
            }
        }
        foreach($appointments AS $user_id => $appointment){
            $this->set('appointments', $appointment);
        }
        $this->set('selected_month', $selected_month);
        $this->set('selected_year', $selected_year);
        $this->set('user_id', $user_id);
        $this->set('user',$user);
        $this->set('alldate',$alldate);
        
    }
   
				
    /*******************************************************/
        public function test(){
            $data = array();
            $this->layout = 'blank';
            $data['user_id'] = 188;
            $socket = new HttpSocket();
            $getAppt = $socket->post('http://eportal1.mfa.uat/AppointmentsAPI/show/.json', $data);
            $Appt = json_decode($getAppt, true);	
            pr($Appt);
        }
        public function test2(){
            $data = array();
            $this->layout = 'blank';
            $data['list_user_id'] = 188;
            $socket = new HttpSocket();
            $getAppt = $socket->post('http://gateway.mfa.sit/WidgetBaseAPI/getUserProfile/.json', $data);
            $Appt = json_decode($getAppt, true);	
            pr($Appt);
            $this->render(test);
        }
	/***************************** Add Attachment *********************/
        function addAttachment($attachment, $tmp_name, $file_name, $file_error, $file_size, $file_type, $user_id){
            /*$post = $attachment;
           
            $original_name = $post['name'];
            $pic_type = $post['type'];
            $original_size = $post['size'];*/
            
            $result['original_name'] = $file_name;
            $result['pic_type'] = $file_type;
            $result['key'] = '';
            
            $code = Utility::uuid();
            $allowedExts = array('doc', 'docx', 'txt', 'pdf', 'jpg', 'jpeg', 'png', 'gif', 'xls', 'xlsx', 'ppt', 'pptx', 'bmp', 'zip', 'rar', '7z', 'swf');
            $receieve_error = explode(".", $file_name);
            $extension = strtolower(end($receieve_error));
            if(is_uploaded_file($tmp_name) && ($file_size < Configure::read('Config.MaxFileSize')) && in_array($extension, $allowedExts)) { 
                    if ($file_error > 0) {
                        echo __("Return Error Code: ") . $file_error . "<br>";
                    }else { 
                        $path = 'attachment/appointment/';
                        if(!file_exists($path)) {
                            mkdir($path, 0777, true);
                        }
                        $uploadSuccess = move_uploaded_file($tmp_name, $path . $code . '.' . $extension);
                        $filename = $code . '.' . $extension;
                        if ($uploadSuccess) {
                             $results = $this->FileStorageComponent->save($path, $code . '.' . $extension, $file_type);
                             if ($results) {
                                   
                                        
                                    $result['key'] = $results;
                             }
                        }else{
                            echo  '-1';
                           $result['key'] = '';
                        }
                    }                         
            } 
            
            return $result;
        }
        /******************************************************************/

}
