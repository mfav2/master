<?php

App::uses('Utility', 'mfa');
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');

class ContactsController extends AppController {

    public $name = 'Contacts';  //Real Name Of Controller for this->$name
    public $uses = array("AuthUser", "Contact", "PortalAttachment", "ContentType", "ResourceContent", "ContactOrganization");
    public $components = array("PortalHelper", "FileStorageComponent");
    public $helpers = array("Form", "Html","Portal", "FileStorageHelper",'Session');

    public function searchcontact() {
		  
	$not_share = array();
	$all_shareorganization = array();
	$all_sharecontact = array();

	$this->layout = "blank";
			
		if($this->request->is('Post')){
                    $currentUser = $this->Session->read('AuthUser');
                    $test = $this->request->data;
                    $organi_id = $currentUser['AuthUserOrganizationPosition']['0']['AuthOrganizationPosition']['organization_id'];


		
					$value = $test['text'];
                    $type = $test['share'];

                    $this->set('type', $type);
                    if($type == 0){
                        $mycontact = $this->Contact->find('all', array(
								'conditions'=>array(
									'Contact.deleted' => 'N',
									'Contact.user_id' => $currentUser['AuthUser']['id'],
									array('OR' => array(
										"Contact.contact_name ILIKE '%".trim($value)."%'",
										"Contact.company ILIKE '%".trim($value)."%'",
										"Contact.job_title ILIKE '%".trim($value)."%'",
										"Contact.phone1 ILIKE '%".trim($value)."%'",
										"Contact.phone2 ILIKE '%".trim($value)."%'",
										"Contact.phone3 ILIKE '%".trim($value)."%'",
										"Contact.phone4 ILIKE '%".trim($value)."%'",
										"Contact.con_mfa ILIKE '%".trim($value)."%'",
									)),
								),
								'order' => array('Contact.id Desc'),
								'recursive'=>-1,
                        )); 
                        include 'Metadata/Contacts/mycontact.ctp';
                        $this->set("listViewDefs", $listViewDefs);
						$this->set('listViewDatas',$listdata);
                        //$this->set('contact', $mycontact);
                        
                    }else{
					//--------------------------------GET CHILD-------------------------------------
					$get_contact_arr = array();
					$getShareDepart = array();
					$getallShare = array();
					$data = array();
                    $data['organization_id'] = $organi_id; 

                    $socket = new HttpSocket($data);
                    $getResult = $socket->post(Configure::read('Config.CenterBaseAPI.EndPoint') . 'getOrganizationMain/.json',$data);		
                    $Organization = json_decode($getResult, true);
                    $OrganizationsAllMain = $Organization['Result']['OrganizationMain'];
                    //pr($OrganizationsAllMain);
                    
					if(!empty($OrganizationsAllMain)){
						$getallorg = implode(',', $OrganizationsAllMain);
						$getContactOrg = $this->ContactOrganization->find('all',array(
							'conditions' => array(
								'ContactOrganization.organization_id in ('.$getallorg.')',
								'ContactOrganization.deleted'=>'N',
							),
							'recursive'=>-1,
						));
						foreach($getContactOrg as $OrgContactId){
							$getcontact[] = $OrgContactId['ContactOrganization']['contact_id'];
						}
						if(!empty($getcontact)){
							$get_contact_arr = implode(',', $getcontact);
						}
						
						if(!empty($get_contact_arr)){
							$getShareDepart = $this->Contact->find('all', array(
								'conditions'=> array(
									'Contact.deleted'=>'N',
									'Contact.id in ('.$get_contact_arr.')',
									array('OR' => array(
										"Contact.contact_name ILIKE '%".trim($value)."%'",
										"Contact.company ILIKE '%".trim($value)."%'",
										"Contact.job_title ILIKE '%".trim($value)."%'",
										"Contact.phone1 ILIKE '%".trim($value)."%'",
										"Contact.phone2 ILIKE '%".trim($value)."%'",
										"Contact.phone3 ILIKE '%".trim($value)."%'",
										"Contact.phone4 ILIKE '%".trim($value)."%'",
										"Contact.con_mfa ILIKE '%".trim($value)."%'",
										
									)),
								),
								'recursive'=>-1,		
							));
						}
						
							
					}
						
						$getShareAllContact = $this->Contact->find('all',array(
							'conditions'=> array(
								'Contact.deleted'=>'N',
								"Contact.share_type ILIKE '%".trim('Share All')."%'",
								//'Contact.is_shared'=>'Y',
								array('OR' => array(
									"Contact.contact_name ILIKE '%".trim($value)."%'",
									"Contact.company ILIKE '%".trim($value)."%'",
									"Contact.job_title ILIKE '%".trim($value)."%'",
									"Contact.phone1 ILIKE '%".trim($value)."%'",
									"Contact.phone2 ILIKE '%".trim($value)."%'",
									"Contact.phone3 ILIKE '%".trim($value)."%'",
									"Contact.phone4 ILIKE '%".trim($value)."%'",
									"Contact.con_mfa ILIKE '%".trim($value)."%'",
									
								)),
							),
							'recursive'=>-1,		
						));
						
						$getallShare = array_merge($getShareDepart, $getShareAllContact);
						//pr($getShareDepart);
							

                        include 'Metadata/Contacts/sharedcontact.ctp';
                        $this->set("listViewDefs", $listViewDefs);
						$this->set('listViewDatas',$listdata);
                        
                    }
                    
                }
		

	}
	/*public function searchcontact() {
		  
	$not_share = array();
	$all_shareorganization = array();
	$all_sharecontact = array();

	$this->layout = "blank";
			
			if($this->request->is('Post'))
		{
				$currentUser = $this->Session->read('AuthUser');
				$test = $this->request->data;

				$organi_id = $currentUser['AuthUserOrganizationPosition']['0']['AuthOrganizationPosition']['organization_id'];


		//--------------------------------GET CHILD-------------------------------------
		
		$data = array();
		$data['organization_id'] = $organi_id; 
		
		$socket = new HttpSocket($data);
		$getResult = $socket->post(Configure::read('Config.CenterBaseAPI.EndPoint') . 'getOrganizationChild/.json',$data);		
		$Organization = json_decode($getResult, true);
		$OrganizationsAllChild = $Organization['Result']['OrganizationChild'];
		//pr($OrganizationsAllChild);
		//------------------------------------------------------------------------------


				$value = $test['text'];
				$type = $test['share'];
				
				
				
				if($type == 'Not Shares' || $type == 'All')
			{
				$not_share = $this->Contact->find('all', array(

					'conditions' => array(
								'Contact.deleted' => 'N',
								'Contact.share_type' => 'Not Share',
							    'Contact.user_id' => $currentUser['AuthUser']['id'],
								'(Contact.contact_name ILIKE \'%'.$value.'%\' 
								OR Contact.company ILIKE \'%'.$value.'%\'
								OR Contact.job_title ILIKE \'%'.$value.'%\'
								)'),

					
					'order' => array('Contact.id Desc')
								
						)
				);
				
				

			}

				if($type == 'Share Department' || $type == 'All')
			{		
				$all_shareorganization = $this->ContactOrganization->find('all', array(
				'conditions' => array(
					'ContactOrganization.organization_id' => $OrganizationsAllChild,
					'Contact.share_type' => 'Share Department',
					'ContactOrganization.deleted' => 'N',
					'( Contact.contact_name ILIKE \'%'.$value.'%\' 
					OR Contact.company ILIKE \'%'.$value.'%\'
					OR Contact.job_title ILIKE \'%'.$value.'%\'
					)
					OR
					( Contact.user_id ='.$currentUser['AuthUser']['id'].' )
					'
					)
					)
				);
				
			}
			
				if($type == 'Share All' || $type == 'All')
			{
				$all_sharecontact = $this->Contact->find('all', array(
					'conditions' =>
					array(
						'Contact.share_type' => 'Share All',
						'Contact.deleted' => 'N',
						'( Contact.contact_name ILIKE \'%'.$value.'%\' 
						OR Contact.company ILIKE \'%'.$value.'%\'
						OR Contact.job_title ILIKE \'%'.$value.'%\')'
					),
						)
				);
				
			}	

			$this->set('not_share', $not_share);
			$this->set('share_all', $all_sharecontact);
			$this->set('share_org', $all_shareorganization);
		}
		

	}*/

    public function indexcontact() {
        /*$this->Session->write('pc', $this->request->query['pc']);
        $this->Session->write('pa', $this->request->query['pa']);
        if(!empty($this->request->query['pq'])){
            $this->Session->write('pq', base64_decode($this->request->query['pq']));
        }else{
            $this->Session->delete('pq');
        }
        if(!empty($this->request->query['pr'])){
           $this->Session->write('pr', base64_decode($this->request->query['pr'])); 
        }else{
            $this->Session->delete('pr');
        }*/
        $currentUser = $this->Session->read('AuthUser');

		//pr($currentUser);
        
	$organization_code = $currentUser['AuthUserOrganizationPosition']['0']
			['AuthOrganizationPosition']['AuthOrganization']['organization_code'];

        $organi_id = $currentUser['AuthUserOrganizationPosition']['0']['AuthOrganizationPosition']['organization_id'];

		//pr($organi_id);

		//--------------------------------GET CHILD-------------------------------------
		$data = array();
		$data['organization_id'] = $organi_id; 
		
		$socket = new HttpSocket($data);
		$getResult = $socket->post(Configure::read('Config.CenterBaseAPI.EndPoint') . 'getOrganizationChild/.json',$data);		
		$Organization = json_decode($getResult, true);
		$OrganizationsAllChild = $Organization['Result']['OrganizationChild'];
		//pr($OrganizationsAllChild);
		//------------------------------------------------------------------------------	

        $mycontactall_notdel = $this->Contact->find('all', array(
            'conditions' => array(
                'Contact.user_id' => $currentUser['AuthUser']['id'],
                'Contact.deleted' => 'N',	
            ),
            'order' => array('Contact.id Desc')
                )
        );
        $this->set('contactall', $mycontactall_notdel);
		//pr($Organization);
		
        /*$all_shareorganization = $this->ContactOrganization->find('all', array(
            'conditions' => array(
                'ContactOrganization.organization_id' => $OrganizationsAllChild,
                'Contact.user_id !=' => $currentUser['AuthUser']['id'],
                'Contact.share_type' => 'Share Department',
                'ContactOrganization.deleted' => 'N',
            )
                )
        );
		

        $this->set('allsharegani', $all_shareorganization);
		

        $all_sharecontact = $this->Contact->find('all', array(
            'conditions' =>
            array(
                'Contact.share_type' => 'Share All',
                'Contact.deleted' => 'N',
                'Contact.user_id !=' => $currentUser['AuthUser']['id'],
            ),
                )
        );
        $this->set('allshare', $all_sharecontact);*/

		

        $breadCrumbs = array(
            array('label' => __('My Contact'), 'link' => array('controller' => 'Contacts', 'action' => 'indexcontact', 'query' => false))
        );
        $this->set("breadCrumbs", $breadCrumbs);

        include 'Metadata/Contacts/IndexContactForm.ctp';
        $this->set("TabView", $TabView);

		
    }

    public function addcontact() {
        $currentUser = $this->Session->read('AuthUser');  //pr($currentUser);
        $this->request->data['deleted'] = 'N'; // default in database T^T
        if ($this->request->data['share_type'] == 'Not Share') {
            $this->request->data['is_shared'] = 'N';
        } else {
            $this->request->data['is_shared'] = 'Y';
        }
        //pr($this->request->data);
        //die();
        
        if (!empty($this->request->data['Contacts']['conpic']['tmp_name'])) {
            $post = $this->request->data['Contacts']['conpic'];
            $tmp_name = $post['tmp_name'];
            $file_name = $post['name'];
            $file_error = $post['error'];
            $file_size = $post['size'];
            $file_type = $post['type'];

            $attach_key = $this->addAttachment($post, $tmp_name, $file_name, $file_error, $file_size, $file_type, $currentUser['AuthUser']['id']);

            $this->request->data['attachment_name'] = $attach_key['original_name'];
            $this->request->data['attachment'] = $attach_key['key'];
        }else{
            $this->request->data['attachment'] = '';
            $this->request->data['attachment_name'] = '';
        }
        
        $data_all['Contacts'] = $this->request->data;
        $data_all['Contacts']['user_id'] = $currentUser['AuthUser']['id'];
        if ($this->request->data['title'] == 'Etc' && $this->request->data['title'] != null) {
            $this->request->data['title'] = $this->request->data['title2'];
        }
        if ($this->Contact->save($data_all['Contacts'])) {
            if ($this->request->data['organization_id'] != "") {
                $this->ContactOrganization->create();
                $this->ContactOrganization->save($data_all['Contacts']);
                $this->ContactOrganization->saveField('contact_id', $this->Contact->getLastInsertID());
                $this->ContactOrganization->saveField('organization_id', $data_all['Contacts']['organization_id']);
            } else {
                $this->Contact->save($data_all['Contacts']);
            }
            $url = $this->PortalHelper->makeUrl('Contacts', 'indexcontact');
            $this->redirect($url);
            
        }

        
    }

    public function editcontact() {
        $currentUser = $this->Session->read('AuthUser');  //pr($currentUser);
        $getdata = $this->request->query;
        $contact_old = '';
        //pr($getdata); die();
        /*$content_type = $this->ContentType->find('first',
            array('conditions' => 
                array(
                'ContentType.name' => 'Contact')
                )
            );*/  
        
        $breadCrumbs = array(
            array('label' => __('My Contact'), 'link' => array('controller' => 'Contacts', 'action' => 'indexcontact', 'query' => false))
        );
        $this->set("breadCrumbs", $breadCrumbs);
        include 'Metadata/Contacts/EditContactForm.ctp';
        $this->set("View", $View);
        //get ค่ามา post ค่าไป
        if ($this->request->is('get') && !$this->request->data) {
            $contact = $this->Contact->find('first', array('conditions' => array('id' => $getdata['id'])));
            $this->request->data = $contact; // this use for place

            $share_type = $contact['Contact']['share_type'];
            $this->set('share_type', $share_type);
            if ($share_type == 'Share Department') {
                $organi_id = $contact['ContactOrganization']['0']['organization_id'];
                $this->set('organi_id', $organi_id);
            }
           
            
            if(empty($contact['Contact']['attachment'])){
                $url_old_pic = '../webroot/attachment/contact/no_image.jpg';
            }else{
                $url_old_pic = '/fileProviders/index/'.$contact['Contact']['attachment'];
            }

           
           
           $this->set('url_old_pic', $url_old_pic);
           
            /*$find_old_pic = $this->PortalAttachment->find('all', array('conditions' =>
                array(
                    'reference_id' => $getdata['id'],
                    'content_type_id' => $content_type['ContentType']['id'],
                    'PortalAttachment.deleted' => "N",
                )
                    )
            );
            //pr($find_old_pic);

            if ($find_old_pic == null) {
                $find_old_pic['0']['ResourceContent']['key'] = '';
                $old_key_pic = '';
            } else {
                $old_key_pic = $find_old_pic['0']['ResourceContent']['key'];
            }

            $url_old_pic = $this->FileStorageComponent->urlByKey($old_key_pic);


            $this->set('old_key_pic', $old_key_pic);
            $this->set('url_old_pic', $url_old_pic);*/
        }
        
        if ($this->request->is('post')) {
            $id = $this->request->data['Contact']['id'];
            $find_old = $this->Contact->find('all', array(
                'conditions' => array(
                    'Contact.id' => $id,
                ))
            );
            $contact_old = $find_old['0']['Contact'];
            if (!isset($find_old['0']['ContactOrganization']['0'])) {
                $organi_id = '';
                $organi_old['organization_id'] = '';
            } else {
                $organi_old = $find_old['0']['ContactOrganization']['0'];
            }
            if ($contact_old['share_type'] == 'Share Department' && $this->request->data['share_type'] == 'Share Department') {
                if ($organi_old['organization_id'] != $this->request->data['organization_id']) {
                    $this->ContactOrganization->updateAll(
                    array('ContactOrganization.organization_id' => $this->request->data['organization_id']), array('ContactOrganization.contact_id' => $id));
                }
            }
            if ($contact_old['share_type'] != $this->request->data['share_type']) {
                if ($contact_old['share_type'] == 'Share All' && $this->request->data['share_type'] == 'Not Share') {
                    $this->Contact->updateAll(array('Contact.share_type' => "'Not Share'"), array('Contact.id' => $id));
                    $this->Contact->updateAll(array('Contact.is_shared' => "'N'"), array('Contact.id' => $id));
                }
                if ($contact_old['share_type'] == 'Share All' && $this->request->data['share_type'] == 'Share Department') {
                    $this->Contact->updateAll(array('Contact.share_type' => "'Share Department'"), array('Contact.id' => $id));
                    $this->ContactOrganization->create();
                    $this->ContactOrganization->save($this->request->data);
                    $this->ContactOrganization->saveField('contact_id', $id);
                    $this->ContactOrganization->saveField('organization_id', $this->request->data['organization_id']);
                }
                if ($contact_old['share_type'] == 'Share Department' && $this->request->data['share_type'] == 'Share All') {
                    $this->Contact->updateAll(array('Contact.share_type' => "'Share All'"), array('Contact.id' => $id));
                    $this->ContactOrganization->updateAll(array('ContactOrganization.deleted' => "'Y'"), array('ContactOrganization.contact_id' => $id));
                }
                if ($contact_old['share_type'] == 'Share Department' && $this->request->data['share_type'] == 'Not Share') {
                    $this->Contact->updateAll(array('Contact.share_type' => "'Not Share'"), array('Contact.id' => $id));
                    $this->Contact->updateAll(array('Contact.is_shared' => "'N'"), array('Contact.id' => $id));
                    $this->ContactOrganization->updateAll(array('ContactOrganization.deleted' => "'Y'"), array('ContactOrganization.contact_id' => $id));
                }
                if ($contact_old['share_type'] == 'Not Share' && $this->request->data['share_type'] == 'Share All') {
                    $this->Contact->updateAll(array('Contact.share_type' => "'Share All'"), array('Contact.id' => $id));
                    $this->Contact->updateAll(array('Contact.is_shared' => "'Y'"), array('Contact.id' => $id));
                }
                if ($contact_old['share_type'] == 'Not Share' && $this->request->data['share_type'] == 'Share Department') {
                    $this->Contact->updateAll(array('Contact.share_type' => "'Share Department'"), array('Contact.id' => $id));
                    $this->Contact->updateAll(array('Contact.is_shared' => "'Y'"), array('Contact.id' => $id));
                    $this->ContactOrganization->create();
                    $this->ContactOrganization->save($this->request->data);
                    $this->ContactOrganization->saveField('contact_id', $id);
                    $this->ContactOrganization->saveField('organization_id', $this->request->data['organization_id']);
                }
            }
            
            
            if (!empty($this->request->data['Contact']['conpic']['tmp_name'])) {
                $post = $this->request->data['Contact']['conpic'];
                $tmp_name = $post['tmp_name'];
                $file_name = $post['name'];
                $file_error = $post['error'];
                $file_size = $post['size'];
                $file_type = $post['type'];

                $attach_key = $this->addAttachment($post, $tmp_name, $file_name, $file_error, $file_size, $file_type, $currentUser['AuthUser']['id']);

                $this->request->data['Contact']['attachment_name'] = $attach_key['original_name'];
                $this->request->data['Contact']['attachment'] = $attach_key['key'];
            }
            
            
            if ($this->Contact->save($this->request->data)) {
                $this->Contact->saveField('contact_name', $this->request->data['contact_name']);
                $this->Contact->saveField('phone1', $this->request->data['phone1']);
                $url = $this->PortalHelper->makeUrl('Contacts', 'indexcontact');
                $this->redirect($url);
            }
            
            
        }
    }

    public function viewcontact($id = null /* Don't Use */) {
        //$this->layout = 'form';
        $getdata = $this->request->query;
        // pr($getdata);
            /*$content_type = $this->ContentType->find('first',
            array('conditions' => 
                array(
                'ContentType.name' => 'Contact')
                )
            );*/  
        $contact = $this->Contact->find('first', array('conditions' => array('id' => $getdata['id'])));

        $this->set('contact', $contact); /* For Future */

        $breadCrumbs = array(
            array('label' => __('My Contact'), 'link' => array('controller' => 'Contacts', 'action' => 'indexcontact', 'query' => false))
        );
        $this->set("breadCrumbs", $breadCrumbs);

            /*$find_old_pic = $this->PortalAttachment->find('all', array('conditions' =>
                array(
                    'reference_id' => $getdata['id'],
                    'content_type_id' => $content_type['ContentType']['id'],
                    'PortalAttachment.deleted' => "N",
                )
                    )
            );

            if ($find_old_pic == null) {
                $find_old_pic['0']['ResourceContent']['key'] = '';
                $old_key_pic = '';
            } else {
                $old_key_pic = $find_old_pic['0']['ResourceContent']['key'];
            }

            $url_old_pic = $this->FileStorageComponent->urlByKey($old_key_pic);*/


        //$this->set('old_key_pic', $old_key_pic);
        //$this->set('url_old_pic', $url_old_pic);
        include 'Metadata/Contacts/ViewContactForm.ctp';
        $this->set("viewDefs", $viewDefs);
        $this->set("viewDatas", $viewDatas);
        
        
    }

    //public function deletecontact() {
        // $this->layout = 'form';
        //$getdata = $this->request->query;
        //$this->Contact->updateAll(array('Contact.deleted' => "'Y'"), array('Contact.id' => $getdata['id']));
        /* BIG BUG FOR POSTGRES FIELD TYPE CHARACTER CANNOT USE "Y" MUST USE "'Y'" */
        //$url = $this->PortalHelper->makeUrl('Contacts', 'indexcontact');
        //$this->redirect($url);
    //}

	public function deletecontact(){
		$this->layout = 'blank';
		$this->autoRender = false;
		$data =array();
		//pr($this->request->data['id']);
		$this->Contact->id = $this->request->data['id'];
		$data['Contact']['deleted'] = 'Y';
		if($this->Contact->save($data)){
			echo 1;
		}else{
			echo 0;
		}


	}
        
	/***************************** Add Attachment *********************/
        function addAttachment($attachment, $tmp_name, $file_name, $file_error, $file_size, $file_type, $user_id){
            /*$post = $attachment;
           
            $original_name = $post['name'];
            $pic_type = $post['type'];
            $original_size = $post['size'];*/
            
            $result['original_name'] = $file_name;
            $result['pic_type'] = $file_type;
            $result['key'] = '';
            
            $code = Utility::uuid();
            $allowedExts = array('doc', 'docx', 'txt', 'pdf', 'jpg', 'jpeg', 'png', 'gif', 'xls', 'xlsx', 'ppt', 'pptx', 'bmp', 'zip', 'rar', '7z', 'swf');
            $receieve_error = explode(".", $file_name);
            $extension = strtolower(end($receieve_error));
            if(is_uploaded_file($tmp_name) && ($file_size < Configure::read('Config.MaxFileSize')) && in_array($extension, $allowedExts)) { 
                    if ($file_error > 0) {
                        echo __("Return Error Code: ") . $file_error . "<br>";
                    }else { 
                        $path = 'attachment/contact/';
                        if(!file_exists($path)) {
                            mkdir($path, 0777, true);
                        }
                        $uploadSuccess = move_uploaded_file($tmp_name, $path . $code . '.' . $extension);
                        $filename = $code . '.' . $extension;
                        if ($uploadSuccess) {
                             $results = $this->FileStorageComponent->save($path, $code . '.' . $extension, $file_type);
                             if ($results) {
                                    /*$data =array();
                                    $data['ResourceContent']['resource_category_id'] = 0;
                                    $data['ResourceContent']['path'] = $path;
                                    $data['ResourceContent']['content_type'] = $file_type;
                                    $data['ResourceContent']['original_name'] = $file_name;
                                    $data['ResourceContent']['file_size'] = $file_size;
                                    $data['ResourceContent']['user_id'] = $user_id;
                                    $data['ResourceContent']['key'] = $results;
                                    $this->ResourceContent->create();
                                    $this->ResourceContent->save($data);*/
                                        
                                    $result['key'] = $results;
                             }
                        }else{
                            echo  '-1';
                           $result['key'] = '';
                        }
                    }                         
            } 
            
            return $result;
        }
        /******************************************************************/        
        
}