<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('Utility', 'mfa');

class WidgetController extends AppController {

	public $components = array("PortalHelper");
	public $name = 'Widget';
	public $uses = array();

	public function index() {
		$this->layout = 'blank';
		//$this->render($widget);
	}

	//-------------------------------------------------------------------------------------------------------------------
	//Core Engine
	public function userProfile($widget='user_profile',$user_id='') {
		$this->disableCache();
		$this->layout = 'blank';

		if(empty($user_id)){
			$currentUser = $this->Session->read('AuthUser');
			$user_id = $currentUser['AuthUser']['id'];
		}

		$data["list_user_id"] = $user_id;

		$socket = new HttpSocket();
		$getUserProfileResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', $data);
		$userProfile = json_decode($getUserProfileResult, true);

		$userProfiles = $userProfile['Result']['UserProfile'];
		$this->set('userProfiles', $userProfiles);
		$this->set('event', $widget);
		$this->render($widget);
	}

    //ok
    public function changePassword($event = '') {
        if ($this->request->data) {
            $data = $this->request->data;
            $currentUser = $this->Session->read('AuthUser');
            $user_id = $currentUser['AuthUser']['id'];
            $data['user_id'] = $user_id;
            $socket = new HttpSocket();
            $setPasswordResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'setPassword/.json', $data);
            $userPassword = json_decode($setPasswordResult, true);

            $userPasswords = $userPassword['Result']['User'][0];
            $old_pass = md5($data['password']);
            if ($userPasswords['User']['password'] == $old_pass) {
                if ($data['new_password']==$data['re_new_password']&&$data['new_password']!='') {
                    $data['password'] = md5($data['new_password']);
                    $savePassword = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'setNewPassword/.json', $data);
                    $newPassword = json_decode($savePassword, true);
                    $this->autoRender = false;
                    print_r('saved');
                } else {
                    $this->autoRender = false;
                    print_r('incurrectpass');
                }
            } else {
                $this->autoRender = false;
                print_r('incurrect');
            }
        }
        $this->layout = 'blank';
        $this->set('event', $event);
    }

   	 //ok
    	public function userDirectory($event = '') {
	        $this->disableCache();
	        $this->layout = 'blank';

		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];

		$data["user_id"] = $user_id;

		$socket = new HttpSocket();
		$getGroupResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getContactGroup/.json', $data);
		$contactGroup = json_decode($getGroupResult, true);
		//pr($contactGroup);

		$publicGroups = $contactGroup['Result']['PublicGroup'];
		$privateGroups = $contactGroup['Result']['PrivateGroup'];
		
		$this->set('publicGroups', $publicGroups);
		$this->set('privateGroups', $privateGroups);
		$this->set('event',$event);
		//$this->render('get_result');

		//---------------------------------------------------------------------------------------------

		/*
		$data["contact_group_id"] = 1;

		$contactGroupUserResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getContactGroupUser/.json', $data);
		$contactUser = json_decode($contactGroupUserResult, true);
		//pr($contactUser);


		$this->set('contactUsers', $contactUser);
		*/
	}
        
        // In case  Add New Contact group
        public function showcontactgroup($event = ''){
                $this->disableCache();
                $this->layout = 'blank';

		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];

		$data["user_id"] = $user_id;

		$socket = new HttpSocket();
		$getGroupResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getContactGroup/.json', $data);
		$contactGroup = json_decode($getGroupResult, true);
		//pr($contactGroup);

		$publicGroups = $contactGroup['Result']['PublicGroup'];
		$privateGroups = $contactGroup['Result']['PrivateGroup'];
		
		$this->set('publicGroups', $publicGroups);
		$this->set('privateGroups', $privateGroups);
		$this->set('event',$event);            
        }
        
	
	//ok
	public function organizationDirectory($event='') {
		$this->layout = 'blank';
		//echo "<script>alert('".$event."');</script>";
		$this->set('event',$event);
	}

	//ok
	public function getOrganizationDescription($organization_id=0){
		$this->disableCache();
		$this->layout = 'blank';

		$data["organization_id"] = $organization_id;
		$data["language_id"] = 1;

		$socket = new HttpSocket();
		$getOrganizationResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganizationDescription/.json', $data);
		$organization = json_decode($getOrganizationResult, true);

		$organizations = $organization['Result']['Organization'];
		$this->set('result', $organizations);
		$this->render('get_result');
	}

	//ok
	public function getOrganization($organization_id=-1){
		$this->disableCache();
		$this->layout = 'blank';

		$data["organization_id"] = $organization_id;
		$data["language_id"] = 1;

		$socket = new HttpSocket();
		$getOrganizationResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganization/.json', $data);
		$organization = json_decode($getOrganizationResult, true);

		$organizations = $organization['Result']['Organization'];
		$this->set('result', $organizations);
		$this->render('get_result');
	}

	//ok v1
	public function getContactGroupUser_($contact_group_id=0) {
		$this->disableCache();
		$this->layout = 'blank';

		$data["contact_group_id"] = $contact_group_id;

		$socket = new HttpSocket();
		$getContactGroupUserResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getContactGroupUser/.json', $data);
		$contactGroupUser = json_decode($getContactGroupUserResult, true);

		$contactGroupUsers = $contactGroupUser['Result']['ContactGroupUser'];
		//pr($contactGroupUsers);

		$result = array();
		foreach ($contactGroupUsers as $contactGroupUser) {
			$contactGroupUser['User']['ContactGroup']['id'] = $contactGroupUser['ContactGroupUser']['contact_group_id'];
			$contactGroupUser['User']['ContactGroupUser']['id'] = $contactGroupUser['ContactGroupUser']['id'];
			
			$result[] = $contactGroupUser['User'];
		}
		//pr($result);
		$this->set('result', $result);
		$this->render('get_result');
	}

	//ok v2
	public function getContactGroupUser($contact_group_id=0,$event='publicGroup') {
		$this->disableCache();
		$this->layout = 'blank';

		$data["contact_group_id"] = $contact_group_id;

		$socket = new HttpSocket();
		$getContactGroupUserResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getContactGroupUser/.json', $data);
		$contactGroupUser = json_decode($getContactGroupUserResult, true);

		$contactGroupUsers = $contactGroupUser['Result']['ContactGroupUser'];
		//pr($contactGroupUsers);

		//$result = array();
		//foreach ($contactGroupUsers as $contactGroupUser) {
		//	$contactGroupUser['User']['ContactGroup']['id'] = $contactGroupUser['ContactGroupUser']['contact_group_id'];
		//	$contactGroupUser['User']['ContactGroupUser']['id'] = $contactGroupUser['ContactGroupUser']['id'];
		//	
		//	$result[] = $contactGroupUser['User'];
		//}
		//pr($result);
		//$result = $this->_sortUserDirectory($result);
		$result = $contactGroupUsers;

		$this->set('action', 'getContactGroupUser');
		$this->set('event', $event);
		$this->set('users', $result);
		$this->render('get_user_list');
	}

	//ok
	public function setContactGroup($contact_group_name='',$is_public='N',$contact_user_id=''){
		$this->layout = 'blank';

		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];

		$browser = $this->PortalHelper->checkBrowserUserAgent();
		
		$data["contact_group_name"] = Utility::valueConvert($browser,$contact_group_name);
		$data["user_id"] = $user_id;
		$data["is_public"] = $is_public;
		$data["contact_user_id"] = $contact_user_id;

		$socket = new HttpSocket();
		$getContactGroupResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'setContactGroup/.json', $data);
		$contactGroup = json_decode($getContactGroupResult, true);

		$contactGroup = $contactGroup['Result']['ContactGroup'];
		$this->set('result', $contactGroup);
		$this->render('get_result');
	}

	//ok
	public function editContactGroup($contact_id=0,$contact_group_name='',$is_public='N'){
		$this->layout = 'blank';

		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];

		$data["contact_id"] = $contact_id;
		$data["contact_group_name"] = $contact_group_name;
		$data["is_public"] = $is_public;

		$socket = new HttpSocket();
		$getContactGroupResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'editContactGroup/.json', $data);
		$contactGroup = json_decode($getContactGroupResult, true);

		$contactGroup = $contactGroup['Result']['ContactGroup'];
		$this->set('result', $contactGroup);
		$this->render('get_result');
	}

	//ok
	public function deleteContactGroup($contact_group_id=0) {
		$this->layout = 'blank';

		$data["contact_group_id"] = $contact_group_id;

		$socket = new HttpSocket();
		$getContactGroupResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'deleteContactGroup/.json', $data);
		$contactGroup = json_decode($getContactGroupResult, true);

		$contactGroup = $contactGroup['Result']['ContactGroupUser'];
		$this->set('result', $contactGroup);
		$this->render('get_result');
	}

	//ok
	public function deleteContactGroupUser($contact_user_id=0) {
		$this->layout = 'blank';

		$data["contact_user_id"] = $contact_user_id;

		$socket = new HttpSocket();
		$getContactGroupUserResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'deleteContactGroupUser/.json', $data);
		$contactGroupUser = json_decode($getContactGroupUserResult, true);

		$contactGroupUser = $contactGroupUser['Result']['ContactGroupUser'];
		$this->set('result', $contactGroupUser);
		$this->render('get_result');
	}

	public function setContactGroupUser($contact_group_id=0,$contact_user_id=0) {
		$this->layout = 'blank';

		$data["contact_group_id"] = $contact_group_id;
		$data["contact_user_id"] = $contact_user_id;

		$socket = new HttpSocket();
		$getContactGroupUserResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'setContactGroupUser/.json', $data);
		$contactGroupUser = json_decode($getContactGroupUserResult, true);

		$contactGroup = $contactGroupUser['Result']['ContactGroups'];
		$this->set('result', $contactGroup);
		$this->render('get_result');
	}

	//ok v1
	public function getOrganizationUser_($organization_id=0) {
		$this->disableCache();
		$this->layout = 'blank';

		$data["organization_id"] = $organization_id;

		$socket = new HttpSocket();
		$getOrganizationUserResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganizationUser/.json', $data);
		$organizationUser = json_decode($getOrganizationUserResult, true);
		$organizationUsers = $organizationUser['Result']['OrganizationUser'];
		//pr($organizationUsers);

		/*
		$result = array();
		$user = array();
		foreach ($organizationUsers as $organizationUser) {
			//pr($organizationUser);
			$user = $organizationUser['User'];
			$user['UserProfile'] = $organizationUser['UserProfile'];
			$user['AuthUserAudit'] = $organizationUser['AuthUserAudit'];
			//if(empty($organizationUser['AuthUserAudit']['is_online'])) $user['AuthUserAudit']['is_online'] = 'N';
			
			$result[] = $user;
		}
		*/

		$result = $organizationUsers;
		//pr($result);
		$this->set('result', $result);
		$this->render('get_result');
	}

	//ok v2
	/*public function getOrganizationUser($organization_id=0,$event='publicGroup') {
		$this->disableCache();
		$this->layout = 'blank';

		$cache_type = 'getOrganizationUser';
		$cache_key = $cache_type.'-'.$organization_id;
		Cache::set(array('duration' => '+1 days'));
		$organizationUsers = Cache::read($cache_key);
		if(empty($organizationUsers)){

			$data["organization_id"] = $organization_id;

			$socket = new HttpSocket();
			$getOrganizationUserResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganizationUser/.json', $data);
			$organizationUser = json_decode($getOrganizationUserResult, true);
			$organizationUsers = $organizationUser['Result']['OrganizationUser'];

			//$organizationUsers = $this->_sortUserDirectory($organizationUsers);
			$cache_type = 'getOrganizationUser';
			$cache_key = $cache_type.'-'.$organization_id;
			Cache::set(array('duration' => '+1 days'));
			Cache::write($cache_key,$organizationUsers);

		}

		$this->set('action', 'getOrganizationUser');
		$this->set('event', $event);
		$this->set('users', $organizationUsers);
		$this->render('get_user_list');
	}*/

	public function getOrganizationUser($organization_id=0,$event='publicGroup') {
		$this->disableCache();
		$this->layout = 'blank';

		$cache_type = 'getOrganizationUser';
		$cache_key = $cache_type.'-'.$organization_id;
		Cache::set(array('duration' => '+1 days'));
		$organizationUsers = Cache::read($cache_key);
		if(empty($organizationUsers)){
			$data["organization_id"] = $organization_id;

			$socket = new HttpSocket();
			$getOrganizationUserResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganizationUser/.json', $data);
			$organizationUser = json_decode($getOrganizationUserResult, true);
			$organizationUsers = $organizationUser['Result']['OrganizationUser'];
			$cache_type = 'getOrganizationUser';
			$cache_key = $cache_type.'-'.$organization_id;
			Cache::set(array('duration' => '+1 days'));
			Cache::write($cache_key,$organizationUsers);

		}
			
			/*$getOrgDesc = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganizationUser/.json', $data);
			pr($organizationUsers);*/
		/*$cache_type = 'getOrganizationUser';
		$cache_key = $cache_type.'-'.$organization_id;
		Cache::set(array('duration' => '+1 days'));
		$organizationUsers = Cache::read($cache_key);
		if(empty($organizationUsers)){

			$data["organization_id"] = $organization_id;

			$socket = new HttpSocket();
			$getOrganizationUserResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getOrganizationUser/.json', $data);
			$organizationUser = json_decode($getOrganizationUserResult, true);
			$organizationUsers = $organizationUser['Result']['OrganizationUser'];

			//$organizationUsers = $this->_sortUserDirectory($organizationUsers);
			$cache_type = 'getOrganizationUser';
			$cache_key = $cache_type.'-'.$organization_id;
			Cache::set(array('duration' => '+1 days'));
			Cache::write($cache_key,$organizationUsers);

		}*/

		$this->set('action', 'getOrganizationUser');
		$this->set('event', $event);
		$this->set('users', $organizationUsers);
		$this->render('get_user_list');
	}

	//ok
	public function setUserProfile() {
		$this->layout = 'blank';

		$data = $this->request->data;
               
		$socket = new HttpSocket();
		$setUserProfileResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'setUserProfile/.json', $data);
		$userProfile = json_decode($setUserProfileResult, true);
                //pr($userProfile);
		$userProfiles = $userProfile['Result']['UserProfile'];
		$this->set('result', $userProfiles);
		$this->render('get_result');
	}

	// v1
	public function searchUserProfile_($search='',$event='publicGroup') {
		$this->disableCache();
		$this->layout = 'blank';

		$browser = $this->PortalHelper->checkBrowserUserAgent();
		$data["search"] = Utility::valueConvert($browser,$search);

		$socket = new HttpSocket();
		$userProfileResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'searchUserProfile/.json', $data);
		$userProfile = json_decode($userProfileResult, true);
		$userProfiles = $userProfile['Result']['UserProfile'];

		//pr($userProfiles);
		$this->set('result', $userProfiles);
		$this->render('get_result');
	}

	// v2
	public function searchUserProfile($search='',$event='publicGroup') {
		$this->disableCache();
		$this->layout = 'blank';
		//$browser = $this->PortalHelper->checkBrowserUserAgent();
		//$data["search"] = Utility::valueConvert($browser,$search);
		$data["search"] = $search; 

		$socket = new HttpSocket();
		$userProfileResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'searchUserProfile/.json', $data);
		$userProfile = json_decode($userProfileResult, true);
		$userProfiles = $userProfile['Result']['UserProfile'];

		//$userProfiles = $this->_sortUserDirectory($userProfiles);
			
		$this->set('action', 'searchUserProfile');
		$this->set('event', $event);
		$this->set('users', $userProfiles);
		$this->render('get_user_list');
	}

	//-------------------------------------------------------------------------------------------------------------------
	//Language
	public function languageIcon(){
		$this->layout = 'blank';
	}

	public function languageSelect($language_id,$language_flag,$language_directory){
		$this->layout = 'blank';
		$this->autoRender = false;
		//echo '<i class="flag-'. $language_flag .'"></i> <b class="caret"></b>';

		$currentUser = $this->Session->read('AuthUser');
		$currentUser['AuthUserAudit']['select_language'] = $language_id;
                $currentUser['AuthUserAudit']['UserProfile']['language'] = $language_id;
                $currentUser['AuthUserProfile'][0]['language'] = $language_id;
                
                $data = array();
                $data['language'] = $language_id;
                $data['user_id'] = $currentUser['AuthUser']['id'];
                $data['token_code'] = $currentUser['AuditToken']['token_code'];            
                $socket = new HttpSocket();
		$userProfileResult = $socket->post(Configure::read('Config.CenterBaseAPI.EndPoint') . 'updateProfile/.json', $data);
		$userProfile = json_decode($userProfileResult, true);
                
		//$userProfiles = $userProfile['Result']['UserProfile'];
                
		$this->Session->write('AuthUser', $currentUser);
		$this->Session->write('Config.language', $language_directory);
	}

	//-------------------------------------------------------------------------------------------------------------------
	//Notification
	//ok
	public function notificationSetting(){
		$this->layout = 'blank';
	}

	//ok
	public function notificationIcon(){
		$this->layout = 'blank';
	}

	//ok
	public function notificationMessage($application_id=0){
		$this->disableCache();
		$this->layout = 'blank';

		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];
		//$user_id = 2;

		$data["user_id"] = $user_id;
		$data["application_id"] = $application_id;

		$socket = new HttpSocket();
		$getNotificationResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'getNotificationMessage/.json', $data);
		$notification = json_decode($getNotificationResult, true);
		//pr($notification);

		$notifications = $notification['Result']['NotificationAlertMessages'];
		$users = $notification['Result']['Users'];
		$this->set('notifications', $notifications);		
		$this->set('users', $users);	
	}

	//no
	public function notificationPopup(){
		$this->layout = 'blank';
	}

	//ok
	public function getNotificationAlert($notification_type_value=0){
		$this->disableCache();
		$this->layout = 'blank';

		$currentUser = $this->Session->read('AuthUser');
		$user_id = $currentUser['AuthUser']['id'];
		
		$data["user_id"] = $user_id;
		$data["notification_type_value"] = $notification_type_value;

		$socket = new HttpSocket();
		$getNotificationAlertResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'getNotificationAlert/.json', $data);
		$notificationAlert = json_decode($getNotificationAlertResult, true);

		$notificationAlerts = $notificationAlert['Result']['NotificationAlert'];
		//if(!empty($notificationAlerts['NotificationAlert'])){
			$currentUser = $this->Session->read('AuthUser');
			//$current_updated_date = $notificationAlerts['userNotification']['updated_date'];
			//$updated_date = $currentUser['AuthUserAudit']['UserNotification']['updated_date'];
			//if($notificationAlerts['userNotification']['updated_date']!=$currentUser['UserNotification']['updated_date']){
			if($notificationAlerts['userNotification']['notification_time']!=$currentUser['UserNotification']['notification_time']){
				//echo $notificationAlerts['userNotification']['updated_date'].'<br>';
				//echo $currentUser['UserNotification']['updated_date'].'<br>';
				//echo '!==<br>';
				//pr($notificationAlerts['userNotification']);
				//echo "--------------------------";

				$currentUser['UserNotification']['notification_time'] = $notificationAlerts['userNotification']['notification_time'];
				//pr($currentUser);
				$this->Session->write('AuthUser', $currentUser);
			}
			
		//}

		$this->set('result', $notificationAlerts);
		$this->render('get_result');
	}

	//ok
	public function setNotificationAlert($user_id=0,$notification_mode=0,$notification_type=1,$notification_sound='Y',$notification_song='default.mp3',$notification_id='') {
		$this->layout = 'blank';
		//$this->autoRender = false;
		//echo file_get_contents("http://center.mfa.local/");

		$data["user_id"] = $user_id;
		$data["id"] = $notification_id;
		$data["notification_mode"] = $notification_mode;
		$data["notification_type"] = $notification_type;
		$data["notification_sound"] = $notification_sound;
		$data["notification_song"] = $notification_song;

		$currentUser = $this->Session->read('AuthUser');
		$currentUser['UserNotification']['notification_mode'] = $notification_mode;
		$currentUser['UserNotification']['notification_type'] = $notification_type;
		$currentUser['UserNotification']['notification_sound'] = $notification_sound;
		$currentUser['UserNotification']['notification_song'] = $notification_song;
		$this->Session->write('AuthUser', $currentUser);

		$socket = new HttpSocket();
		$setNotificationResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'setNotificationAlert/.json', $data);
		//echo $contactGroupUserResult;
		$setNotification = json_decode($setNotificationResult, true);
		//pr($organizationUser);

		$userNotifications = $setNotification['Result']['UserNotification'];
		$this->set('result', $userNotifications);
		$this->render('get_result');
	}

	
	//no
	public function getNotification($user_id=0) {
		$this->disableCache();
		$this->layout = 'blank';
		//$this->autoRender = false;
		//echo file_get_contents("http://center.mfa.local/");

		$data["user_id"] = $user_id;

		$socket = new HttpSocket();
		$organizationUserResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'getNotification/.json', $data);
		//echo $contactGroupUserResult;
		$organizationUser = json_decode($organizationUserResult, true);
		//pr($organizationUser);

		$organizationUsers = $organizationUser['Result']['OrganizationUser'];
		$this->set('result', $organizationUsers);
		$this->render('get_result');
	}

	//ok
	public function deleteNotification($notification_id=0) {
		$this->layout = 'blank';
		//$this->autoRender = false;
		//echo file_get_contents("http://center.mfa.local/");

		$data["notification_id"] = $notification_id;

		$socket = new HttpSocket();
		$deleteNotificationResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'deleteNotification/.json', $data);
		//echo $contactGroupUserResult;
		$deleteNotification = json_decode($deleteNotificationResult, true);
		//pr($organizationUser);

		$deleteNotification = $deleteNotification['Result']['OrganizationUser'];
		$this->set('result', $deleteNotification);
		$this->render('get_result');
	}

	public function deleteAllNotification(){
		$this->layout = 'blank';
		$this->autoRender = false;
		$data["notification_id"] = $this->request->data['noti_id'];
		//$data["notification_id"] = $notification_id;
		//pr($data);
		$socket = new HttpSocket();
		$deleteNotificationResult = $socket->post(Configure::read('Config.NotificationBaseAPI.EndPoint') . 'deleteAllNotification/.json', $data);
		$deleteNotification = json_decode($deleteNotificationResult, true);
		$this->set('result', $deleteNotification);
		$this->render('get_result');
	}

	//-------------------------------------------------------------------------------------------------------------------
	//Portal Connect
	public function chat($user_id=0) {
		$this->layout = 'blank';
	}

	//-------------------------------------------------------------------------------------------------------------------
	//Central Filling
	public function folderHandle($event = '') {
		$this->disableCache();
		$this->layout = 'blank';
		$this->set('event',$event);
		//$this->autoRender = false;
		//echo file_get_contents("http://center.mfa.local/");
                
                if (!empty($this->request->data)) {
                        $data = $this->request->data;
                        $currentUser = $this->Session->read('AuthUser');
                        $user_id = $currentUser['AuthUser']['id'];
                        $data['user_id'] = $user_id;
                        //print_r($data['folder_list']);
                        
                        //$socket = new HttpSocket();
                        //$folderHandleResult = $socket->post(Configure::read('Config.CentralFillingBaseAPI.EndPoint') . 'addToFolder/.json', $data);
                        
                        $this->autoRender = false;
                        //print_r('saved');
                } else {
                        $currentUser = $this->Session->read('AuthUser');
                        $organization_id = $currentUser['AuthUserOrganizationPosition'][0]['AuthOrganizationPosition']['organization_id'];
                        //pr($currentUser);
                        $data["organization_id"] = $organization_id;

                        $socket = new HttpSocket();
                        $folderHandleResult = $socket->post(Configure::read('Config.CentralFillingBaseAPI.EndPoint') . 'getFolderHandle/.json', $data);
                        //echo $contactGroupUserResult;
                        $folderHandle = json_decode($folderHandleResult, true);
                        //pr($folderHandle);

                        $folderHandles = $folderHandle['Result']['FolderHandle'];
                        $this->set('result', $folderHandles);
                }
	}
        
//        public function addDocument($folder_id = null, $document_id = null) {
//                $this->set('folder_id',$folder_id);
//                $this->set('model_name','Document');
//                $this->layout = 'blank';
//                //$this->autoRender = false;
//
//                $list_document_type = array(); //get from eSubBaseAPI
//                $list_priority_level = array(); //get from eSubBaseAPI
//                $list_security_level = array(); //get from eSubBaseAPI
//                
//                if(empty($this->data)) {  
//                        if(!empty($document_id)) {  //chose document template (Favourite Message)
//                            //echo $document_id;
//                            //$template_document = $this->Document->find('first',array('conditions'=>array('id'=>$document_id)));
//                            //$this->set('template_document',$template_document);
//
//							$data["subject"] = $organization_id;
//							$data["disss"] = $organization_id;
//							$data["priorirty"] = $organization_id;
//							$data["organization_id"] = $organization_id;
//
//
//							$socket = new HttpSocket();
//							$folderHandleResult = $socket->post(Configure::read('Config.EsubmissionBaseAPI.EndPoint') . 'importDocument/.json', $data);
//							//echo $contactGroupUserResult;
//							$folderHandle = json_decode($folderHandleResult, true);
//							//pr($folderHandle);
//                        }
//                        include 'Metadata/DryPopup/import_document.ctp';
//                        $this->set("viewDefs", $viewDefs);
//
//                }else {
//                        debug($this->data); die();
//
//                        
//                        
//                }
//                
//        }

                public function gettest(){
                    $this->disableCache();
                    $this->layout = 'blank';
                    $user_id = 188;
                    $data["list_user_id"] = $user_id;
                    
                    $socket = new HttpSocket();
                    $getUserProfileResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserProfile/.json', $data);
                    $userProfile = json_decode($getUserProfileResult, true);
                    
                    $userProfiles = $userProfile['Result']['UserProfile'];
                    $this->set('result', $userProfiles);
                    $this->render('get_result');
                   
                }

		public function shortcut($application){
			$this->layout = 'blank';
			$this->autoRender = false;
			echo $application;
		}


		/*
		function _sortUserDirectory($organizationUsers){

			$sort_organizationUsers = array();
			$order_sort_organization = '';
			$order_sort_position = '';
			$arr_organizationUsers = array();
			//pr($organizationUsers);
			if(!empty($organizationUsers)){
				foreach($organizationUsers as $key=>$organizationUser){
					
					$order_sort_organization = !empty($organizationUser['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['order_sort'])!='' ? $organizationUser['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['order_sort']:0;
					$order_sort_position = !empty($organizationUser['UserOrganizationPosition'][0]['OrganizationPosition']['order_sort'])!='' ? $organizationUser['UserOrganizationPosition'][0]['OrganizationPosition']['order_sort']:0;;

					//$order_sort_organization = $order_sort_organization!='' ? $order_sort_organization:0;
					//$order_sort_position = $order_sort_position!='' ? $order_sort_position:0;

					$sort_organizationUsers[$order_sort_organization][$order_sort_position][$key] = $key;

				}

				
				ksort($sort_organizationUsers);
				foreach($sort_organizationUsers as $key=>$sort_organizationUser){

					ksort($sort_organizationUser);
					foreach($sort_organizationUser as $values){
						foreach($values as $value){
							$arr_organizationUsers[] = $organizationUsers[$value];
						}
					}
				}

			}

			return $arr_organizationUsers;
		}
		*/

}
