<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'Validate');
/**
 * Main Controller
 *
 * @property Main $Main
 */
class MenuController extends AppController {
        public $name = 'Menu';
	public $uses = array("Menu", 'Layout', 'AuthSystem', 'AuthObject');
	//public $helpers = array("Portal", "AuthBase", "Theme");
	//public $components = array("RequestHandler", "PortalHelper");
        public $helpers = array("Portal");
        public $components = array("PortalHelper");
        
	function index() {
             
                
	}
	
        public function getMenu(){
              
                $orgTs = $this->Menu->find('all', array(
                        'conditions' => array(
                            'Menu.deleted' => 'N'
                        ),
                        'order'=>'Menu.lft'
                    )
                );
		$orgList = array();
				
		$rootNode = null;
		foreach ($orgTs as $iOrgT=>$orgT) {
			if ($rootNode == null) {
				$rootNode = &$orgTs[$iOrgT];
				$orgList[$rootNode['Menu']['id']] = &$rootNode;
			} else {
				$currentNode = &$orgTs[$iOrgT];
				$parentId = $currentNode['Menu']['parent_id'];
				$parentNode = &$orgList[$parentId];
				$parentNode['child'][] = &$currentNode;
				$orgList[$currentNode['Menu']['id']] = &$currentNode;
			}

		}
                $orgs[] = $rootNode;
                $menus = json_encode($orgs,true);
                return $menus;
            
		
                
                
        }
        
	function menuRedirect($id) {
		$menu = $this->Menu->find('all', array('conditions'=>array('id'=>$id)));
		$view = new View($this);
		$portalHelper = $view->loadHelper("Portal");
		$controller = $menu[0]['Menu']['controller'];
		$action = $menu[0]['Menu']['action'];
		$containerId = $menu[0]['Menu']['layout_id'];
		$param = $menu[0]['Menu']['param'];
		//error_reporting(E_ALL);
		$this->redirect($portalHelper->makeUrl($controller, $action, $param, $containerId));
	}
	
	function ajaxMenus() {
		$orgTs = $this->Menu->find('all', array('order'=>'Menu.lft'));
		$orgList = array();
		
		$rootNode = null;
		foreach ($orgTs as $iOrgT=>$orgT) {
			if ($rootNode == null) {
				$rootNode = &$orgTs[$iOrgT];
				$orgList[$rootNode['Menu']['id']] = &$rootNode;
			} else {
				$currentNode = &$orgTs[$iOrgT];
				$parentId = $currentNode['Menu']['parent_id'];
				$parentNode = &$orgList[$parentId];
				$parentNode['child'][] = &$currentNode;
				$orgList[$currentNode['Menu']['id']] = &$currentNode;
			}

		}

		$orgs[] = $rootNode;

		$sOrgs = $this->printTree($orgs);
		
		$this->set('menus', $sOrgs);
		//$this->set('_serialize', 'menus');
	}

	function jsPopupMenu() {
		$orgTs = $this->Menu->find('all', array('order'=>'Menu.lft'));
		$orgList = array();
				
		$rootNode = null;
		foreach ($orgTs as $iOrgT=>$orgT) {
			if ($rootNode == null) {
				$rootNode = &$orgTs[$iOrgT];
				$orgList[$rootNode['Menu']['id']] = &$rootNode;
			} else {
				$currentNode = &$orgTs[$iOrgT];
				$parentId = $currentNode['Menu']['parent_id'];
				$parentNode = &$orgList[$parentId];
				$parentNode['child'][] = &$currentNode;
				$orgList[$currentNode['Menu']['id']] = &$currentNode;
			}

		}

		$orgs[] = $rootNode;

		$sOrgs = $this->printTree($orgs);
		
		$this->set('menus', $orgs);
//print_r($orgs);
		//$this->set('_serialize', 'menus');
		//$this->render('blank');
	}

	function printTree($tree) {
		$str = $this->_printTree($tree);
		return substr($str, 0, -1);
	}
	
	function _printTree($tree) {
		$view = new View($this);
		$authBase = $view->loadHelper('AuthBase');
		
		$str = '';
		for ($i=0; $i<count($tree); $i++) {
			if ($tree[$i]['Menu']['system_name'] != '' && $tree[$i]['Menu']['object_name'] != '') {
				if (!$authBase->canAccess($tree[$i]['Menu']['system_name'], $tree[$i]['Menu']['object_name'])) {
					continue;
				}
			}
			$str .= '{"data": "' . $tree[$i]['Menu']['menu_label'] . '", "metadata":{ "id" : "' . $tree[$i]['Menu']['id'] . '" }';
			if (!empty($tree[$i]['child'])) {
				$retStr = $this->_printTree($tree[$i]['child']);
				$retStr = substr($retStr, 0, -1);
				$str .= ', "children": [' . $retStr . ']';
			}

			$str .= '},';
		}

		return $str;
	}

	function adminIndex() {
               $breadCrumbs = array(
                        array(
                                'label' => 'Index', 
                                'link'  => ''
                        ),

               );
                $topicname = array(
                     'panel' =>array(
                         'label' =>__('Menu Management'),
                     )
                );  
               include 'Metadata/Admin/menulist.ctp';
               
		$menuTs = $this->Menu->generateTreeList(null, '{n}.Menu.id', '{n}.Menu.menu_label', '&nbsp;&nbsp;&nbsp;');
		$menus = $this->Menu->find('all', array(
                    'conditions' =>array("Menu.deleted = 'N' "),
                    'fields' => array('Menu.menu_label', 'Menu.controller', 'Menu.action', 'Menu.id'),
                    'order'=>'Menu.lft'
                ));
               
  
		foreach ($menus as $menuIdx=>$menu) {
			$menus[$menuIdx]['Menu']['indent'] = $menuTs[$menu['Menu']['id']];
                        $menus[$menuIdx]['Menu']['manage'] = $menu['Menu']['id'];
		}
               
                $this->Set("topic",$topicname);
                $this->set("listViewDefs",$listViewDefs);
                $this->set("breadCrumbs", $breadCrumbs);
		//$this->set('menus', $menus);
                $this->set('listViewDatas', $menus);
	}
	
	function adminAdd() {
                $breadCrumbs = array(
                    array(
                            'label' => __('Menu Management'), 
                            'link'  => ''
                    ),
                    array(
                            'label' => __('Administrator'), 
                            'link'  => ''
                    )

                );
                
		if ($this->PortalHelper->isPost()) {
			if (!empty($this->request->data['SUB_ACTION']) && $this->request->data['SUB_ACTION'] == 'REFRESH') {
				$this->request->data['Menu']['object_name'] = '';
				$authSystem = $this->AuthSystem->find('first', array('conditions'=>array('AuthSystem.system_name'=>$this->request->data['Menu']['system_name'])));
				$authObjects = $this->AuthObject->find('list', array('fields'=>array('AuthObject.object_name', 'AuthObject.object_name'), 'conditions'=>array('AuthObject.system_id'=>$authSystem['AuthSystem']['id']), 'order'=>array('AuthObject.object_name')));
				$this->set('authObjects', $authObjects);
			} else {
				if(empty($this->request->data["Menu"]["menu_label"])){
					$this->Session->setFlash('กรุณาใส่ Menu Label');
				}
				else if(!Validate::isNumber($this->request->data["Menu"]["layout_id"])){
					$this->Session->setFlash('กรุณาใส่ Container ID เป็นตัวเลข');
				}
				else{
					$this->Menu->save($this->request->data);
					
					$url = $this->PortalHelper->makeUrl('Menu', 'adminIndex');
					$this->PortalHelper->PortalRedirect($url);
				}
			}
		}
		$parentId = (!empty($this->request->query['parent_id'])) ? $this->request->query['parent_id'] : $this->request->data['Menu']['parent_id'];
		$parentMenu = $this->Menu->findById($parentId);
		$this->set('parentMenu', $parentMenu);
		
		$this->Layout->virtualFields['full_name'] = "concat(Layout.layout_type, ' | ', Layout.name)";
		$layouts = $this->Layout->find('list', array('fields'=>array('Layout.id', "Layout.full_name"), 'order'=>array('Layout.full_name')));
		$this->set('layouts', $layouts);
		
		$authSystems = $this->AuthSystem->find('list', array('fields'=>array('AuthSystem.system_name', 'AuthSystem.system_name'), 'order'=>array('AuthSystem.system_name')));
		$this->set('authSystems', $authSystems);
		
		if (empty($authObjects)) {
			$authObjects = array();
		}
		$this->set('authObjects', $authObjects);
		
		$this->request->data['Menu']['parent_id'] = $parentId;
	}

	function adminEdit() {
		if ($this->PortalHelper->isPost()) {
			if (!empty($this->request->data['SUB_ACTION']) && $this->request->data['SUB_ACTION'] == 'REFRESH') {
				$this->request->data['Menu']['object_name'] = '';
				$authSystem = $this->AuthSystem->find('first', array('conditions'=>array('AuthSystem.system_name'=>$this->request->data['Menu']['system_name'])));
				$authObjects = $this->AuthObject->find('list', array('fields'=>array('AuthObject.object_name', 'AuthObject.object_name'), 'conditions'=>array('AuthObject.system_id'=>$authSystem['AuthSystem']['id']), 'order'=>array('AuthObject.object_name')));
				$this->set('authObjects', $authObjects);
			} else {
				if(empty($this->request->data["Menu"]["menu_label"])){
					$this->Session->setFlash('กรุณาใส่ Menu Label');
				} else if(!Validate::isNumber($this->request->data["Menu"]["layout_id"])){
					$this->Session->setFlash('กรุณาใส่ Container ID เป็นตัวเลข');
				} else{
					$this->Menu->save($this->request->data);
				
					$url = $this->PortalHelper->makeUrl('Menu', 'adminIndex');
					$this->PortalHelper->PortalRedirect($url);
				}
			}
		}
		
		if (!empty($this->request->query['id'])) {
			$parentId = $this->request->query['id'];
			$menu = $this->Menu->findById($parentId);
			$this->request->data = $menu;
			$parentMenu = $this->Menu->findById($menu['Menu']['parent_id']);
			$this->set('parentMenu', $parentMenu);
		} else {
			$parentId = $this->request->data['Menu']['parent_id'];
			$parentMenu = $this->Menu->findById($parentId);
			$this->set('parentMenu', $parentMenu);
		}
		

		$this->Layout->virtualFields['full_name'] = "concat(Layout.layout_type, ' | ', Layout.name)";
		$layouts = $this->Layout->find('list', array('fields'=>array('Layout.id', "Layout.full_name"), 'order'=>array('Layout.full_name')));
		$this->set('layouts', $layouts);
		
		$authSystems = $this->AuthSystem->find('list', array('fields'=>array('AuthSystem.system_name', 'AuthSystem.system_name'), 'order'=>array('AuthSystem.system_name')));
		$this->set('authSystems', $authSystems);
		
		if (empty($authObjects) && !empty($this->request->data['Menu']['system_name'])) {
			$authSystem = $this->AuthSystem->find('first', array('conditions'=>array('AuthSystem.system_name'=>$this->request->data['Menu']['system_name'])));
			$authObjects = $this->AuthObject->find('list', array('fields'=>array('AuthObject.object_name', 'AuthObject.object_name'), 'conditions'=>array('AuthObject.system_id'=>$authSystem['AuthSystem']['id']), 'order'=>array('AuthObject.object_name')));
		}
		$this->set('authObjects', $authObjects);
			
		
	}

	function adminDelete() {
		$this->Menu->delete($this->request->query['id']);
		
		$url = $this->PortalHelper->makeUrl('Menu', 'adminIndex');
		$this->PortalHelper->PortalRedirect($url);
	}

	function adminUp() {
		$this->Menu->moveUp($this->request->query['id'], 1);
		
		$url = $this->PortalHelper->makeUrl('Menu', 'adminIndex');
		$this->PortalHelper->PortalRedirect($url);
	}

	function adminDown() {
		$this->Menu->moveDown($this->request->query['id'], 1);
		
		$url = $this->PortalHelper->makeUrl('Menu', 'adminIndex');
		$this->PortalHelper->PortalRedirect($url);
	}
        
        public function show($page, $area){

                $socket = new HttpSocket();

                $msgResult = $socket->post(Configure::read('Pakgon.Banner.EndPoint').$page.'/'.$area);
                $msgResult = json_decode($msgResult,true);

                return $msgResult;       
        }
    
}
?>