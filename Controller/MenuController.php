<?php
App::uses('AppController', 'Controller');
App::import('Vendor', 'Validate');
/**
 * Main Controller
 *
 * @property Main $Main
 */
class MenuController extends AppController {
    public $name = 'Menu';
	public $uses = array("Menu", 'Layout', 'AuthSystem', 'AuthObject');
	//public $helpers = array("Portal", "AuthBase", "Theme");
	//public $components = array("RequestHandler", "PortalHelper");
        public $helpers = array("Portal");
        public $components = array("PortalHelper");
        
	public function index() {
             
                
	}

	public function admin_index() {
             
                
	}
	
    public function showmenu($parent_id = NULL){

    	$this->disableCache();
        $this->autoRender = false;

        if(empty($parent_id)){
        	$parent_id  = -1;
        }
        
        //$condition[] = array('Menu.parent_id' => $parent_id);
        //$condition['and'] = array('Menu.deleted' => "N");

        $children = $this->Menu->find('all', array(
        	'conditions' => array(
        		'Menu.parent_id' => $parent_id,
        	), 
        	'order' => array('Menu.id' => 'asc')


        ));

        pr($children);
        $i = 0;
        //pr($children);
         if ($children != NULL) {
            foreach ($children as $child) {
                foreach ($child['MenuDescription'] as $des) {
                    if ($des['language_id'] == 2) {
                        $data_array[$i]['data']['title'] = $des['description'];
                        $data_array[$i]['data']['attr']['id'] = $child['Menu']['id']; //id ของ <a>
                        $data_array[$i]['data']['attr']['level'] = $child['Menu']['level'];
                        break;
                    }
                } $data_array[$i]['state'] = 'closed';
                $data_array[$i]['attr']['id'] = $child['Menu']['id']; //id ของ <li>
                $data_array[$i]['attr']['parent_id'] = $child['Menu']['parent_id'];
                $data_array[$i++]['attr']['level'] = $child['Menu']['level'];
            }
        } else {
            $data_array = array();
        }
        print_r(json_encode($data_array));

    }

    public function add(){
        $this->layout= 'blank';
        
    }

    public function edit(){

    }

    public function delete(){

    }

    public function preview(){
    	
    }


}
?>