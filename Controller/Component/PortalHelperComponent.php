<?php
class PortalHelperComponent extends Component {

	public $components = array("AuthenticationBase");

	private $controller;
	
	function initialize(Controller $controller) {
		$this->controller = $controller;
	}

	function isPost() {
		if (empty($this->controller->request->params["method"])) {
			return $this->controller->request->isPost();
		}
		if ($this->controller->request->params["method"] == 'post') {
			return true;
		} else {
			return false;
		}
	}

	function render($requestParams) {
		$method = ($this->controller->request->isPost() || $this->controller->request->isPut()? "post": "get");
		$url = "/" . $requestParams["portalController"] . "/" . $requestParams["portalAction"] . (empty($requestParams['portalParam'])? '': '/'.$requestParams['portalParam']) . "?" . $requestParams["portalQuery"];
		//$url = "/StaticPage/index?page=unauthorized";
		return $this->controller->requestAction($url,array("return", "data"=>$this->controller->request->data, "method"=>$method));
	}

	function render_($requestParams) {
		$content = "";
				
		$hasPermission = $this->AuthenticationBase->checkAuthentication($requestParams['portalController'], $requestParams['portalAction']);
		if ($hasPermission) {
		
			$method = ($this->controller->request->isPost() || $this->controller->request->isPut()? "post": "get");
			$url = "/" . $requestParams["portalController"] . "/" . $requestParams["portalAction"] . (empty($requestParams['portalParam'])? '': '/'.$requestParams['portalParam']) . "?" . $requestParams["portalQuery"];
					
			//$this->log("sending $method to containerId " . $requestParams["containerId"], "debug");
			//$this->log("query=" . $requestParams["portalQuery"], "debug");
			//$this->log("url=" . $url, "debug");
			//pr($url);
					
			$content = $this->controller->requestAction($url,
				array("return", "data"=>$this->controller->request->data, "containerId"=>$requestParams["containerId"], "method"=>$method)
			);
			
		} else {
			$url = "/StaticPage/index?page=unauthorized";
			$content = $this->controller->requestAction($url);
		}
		
		//$layout = $this->loadLayout(false, $this->controller->Session->read("Pagkon.Portal.LayoutType"));
		return $this->_renderLayout($layout, $requestParams, 0, $content);
		
	}
	
    function makeUrl($controller, $action, $query="") {

        $url = "pc=" . $controller . "&pa=" . $action . ($query==""? "": "&pq=" . base64_encode($query));
        return "/main?" . $url;

    }
	
	function portalRedirect($url) {
		$this->controller->redirect($url);		
	}
        
        
	function checkAccessPermission() {
		$currentUser = $this->Session->read('AuthUser');
		debug($currentUser['UserCentralFillingPermission']);
	}
    
	function checkBrowserUserAgent() {

		$agent = $_SERVER['HTTP_USER_AGENT'];

		if(preg_match('/Firefox/i',$agent)) $browser = 'Firefox'; 
		elseif(preg_match('/Mac/i',$agent)) $browser = 'Mac';
		elseif(preg_match('/Chrome/i',$agent)) $browser = 'Chrome'; 
		elseif(preg_match('/Opera/i',$agent)) $browser = 'Opera'; 
		elseif(preg_match('/MSIE/i',$agent)) $browser = 'IE'; 
		else $browser = 'Unknown';

		return $browser;

	}

}
?>