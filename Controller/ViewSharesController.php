<?php
App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');

class ViewSharesController extends AppController {

    public $name = 'ViewShares';
    public $uses = array("Appointment", 'AppointmentUserReader', "Highlevel", "HighlevelGroup");
    public $components = array('PortalHelper');
    public $helpers = array('Form', 'Html', 'Portal','Session');
    
    
    public function getUserNameFromList($user_list){ // array $user_list
        if(!is_array($user_list)){
            $user_list = array($user_list);
        }
        $socket = new HttpSocket();
        $ulist['list_user_id'] = implode(",", $user_list);
        $getUserProfileResult = $socket->post(Configure::read('Config.WidgetBaseAPI.EndPoint') . 'getUserHighLevel/.json', $ulist);
        $getUserProfile = json_decode($getUserProfileResult, true);
        $user = array();
        if(empty($getUserProfile)){ 
            return false; 
        }else{
            if(!empty($getUserProfile['Result']['Error'])){
                return false;
            }
            $j=0;
            foreach($getUserProfile['Result']['UserProfile'] AS $i => $u){
				$position = '';
				$org_id = '';
				$org_sort = '';
				$pos_id = '';
				$user_org_pos_id = '';
				$sort = '';
				if(!empty($u['UserOrganizationPosition'][0]['OrganizationPosition']['Position']['position_name'])){
					$position = $u['UserOrganizationPosition'][0]['OrganizationPosition']['Position']['position_name'];
				}

				if(!empty($u['UserOrganizationPosition'])){
					if(isset($u['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['id'])){
						$org_id = $u['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['id'];
					}
					if(isset($u['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['order_sort'])){
						$org_sort = $u['UserOrganizationPosition'][0]['OrganizationPosition']['Organization']['order_sort'];
					}
					 
					
					$pos_id = $u['UserOrganizationPosition'][0]['OrganizationPosition']['position_id'];
					$user_org_pos_id = $u['UserOrganizationPosition'][0]['OrganizationPosition']['organization_id'];
					$sort   = $u['UserOrganizationPosition'][0]['OrganizationPosition']['order_sort'];
				}

                $uid = $u['UserProfile']['user_id'];
                $uname = $u['UserProfile']['first_name_th'] . " " . $u['UserProfile']['last_name_th'] . "<br />[{$position}]";
                
                
                
                $user['UserProfile'][$uid]['id'] = $uid;
                $user['UserProfile'][$uid]['detail'] = $uname;
                
                
                $user['Organization'][$j]['org_id'] = $org_id;
                $user['Organization'][$j]['organization_sort'] = $org_sort;
                $user['Organization'][$j]['user_id'] = $uid;
                $user['Organization'][$j]['detail'] = $uname;
                $user['Organization'][$j]['Position_sort'] = $sort;

               $j++;
            }
           
            return $user;
        }
    }
    
    
    
    public function index() {
        $this->Session->write('pc', $this->request->query['pc']);
        $this->Session->write('pa', $this->request->query['pa']);
        if(!empty($this->request->query['pq'])){
            $this->Session->write('pq', base64_decode($this->request->query['pq']));
        }else{
            $this->Session->delete('pq');
        }
        if(!empty($this->request->query['pr'])){
           $this->Session->write('pr', base64_decode($this->request->query['pr'])); 
        }else{
            $this->Session->delete('pr');
        }        
        $this->layout = 'form';
        $breadCrumbs = array(
            array(
                'label' => __('View Shared Schedule'),
                'link' => array('controller' => 'ViewShares', 'action' => 'index', 'query' => '', 'param' => '')
            )
        );
        $this->set("breadCrumbs", $breadCrumbs);
        
        if(!empty($this->request->data)){
            $selected_year = $this->request->data['Highlevels']['selected_year'];
            $tmp_selected_month = $this->request->data['Highlevels']['selected_month'];
            $selected_month = $tmp_selected_month < 10 ? "0{$tmp_selected_month}" : $tmp_selected_month;
        }else{
            $selected_year = date("Y");
            $selected_month = date("m");
        }
            
        $days_in_month = date("t", mktime(0, 0, 0, $selected_month, 1, $selected_year));
		
		$begin_date = $selected_year."-".$selected_month."-01 00:00";
		$last_date  = $selected_year."-".$selected_month."-".$days_in_month." 23:59";
		$date_range = array("$begin_date", "$last_date");
        //$date_range = array("$selected_year-$selected_month-01", "$selected_year-$selected_month-$days_in_month");

        $ulist = $this->Appointment->find('list', array(
            'conditions'=>array(
                'Appointment.privacy_type_id'=>2,
                'Appointment.deleted'=>'N',
                'Appointment.user_id <>'=>0,
                'OR'=>array(
                    'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                    'Appointment.end_date BETWEEN ? AND ?'=>$date_range
                )),
            'fields'=>array('Appointment.user_id'),
            'limit'=>10,
        ));

        $user_list = array();
        foreach($ulist AS $id => $user_id){
            $user_list[] = $user_id;
        }
        
        
       
        $user = $this->getUserNameFromList($user_list);
         
		$list_user_id = '';
		if(!empty($user)){
			foreach($user['UserProfile'] as $userid=>  $getuid){
				$list_user[] = $getuid['id'];
			}
			$list_user_id = implode(',', $list_user);
		}
        
        
        $this->set('alluser_id', $list_user_id);
                
        $this->set('user_list', $user['Organization']);
        //$this->set('sort_pos', $user['Position']);
        //// =========================

        $tmp_appointments = $this->Appointment->find('all', array(
            'conditions'=>array(
                'Appointment.privacy_type_id'=>2,
                'Appointment.deleted'=>'N',
                'Appointment.user_id <>'=>0,
                'OR'=>array(
                    'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                    'Appointment.end_date BETWEEN ? AND ?'=>$date_range
                )
            ),
            'fields'=>array(
                'Appointment.id', 'Appointment.start_date', 'Appointment.end_date', 'Appointment.location', 
                'Appointment.location_type_id', 'Appointment.detail', 'Appointment.user_id', 'Appointment.name'
            ),
            'order'=>array('Appointment.user_id'=>'ASC', 'Appointment.start_date'=>'ASC'),
           
        ));

        $appointments = array();
        foreach($tmp_appointments AS $i => $app){
            $app = $app['Appointment'];
            $id = $app['id'];
            $uid = $app['user_id'];

            if(empty($last_user_id)){ 
                $last_user_id = $app['user_id']; 
                
                $appointments[$uid]['id'] = $id;
                $appointments[$uid]['user_full_name'] = @$user['UserProfile'][$uid]['detail'];
                $appointments[$uid]['user_id'] = $uid;
                $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
            }else{              
                if($last_user_id == $app['user_id']){ /// same as last loop
                    $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                }else{ /// new user
                    $appointments[$uid]['id'] = $id;
                    $appointments[$uid]['user_id'] = $uid;
                    $appointments[$uid]['user_full_name'] = @$user['UserProfile'][$uid]['detail'];
                    $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                }
            }
        }
        
        $this->set('appointments', $appointments);
        $this->set('selected_month', $selected_month);
        $this->set('selected_year', $selected_year);
        
    }
    
	public function chooseuser(){
       $this->layout = 'blank';

			$filter = '';
			$selected_year = date("Y");
            $selected_month = date("m");
			$filter_con = '';
        if(!empty($this->request->data['filter'])){
            $filter = $this->request->data['filter'];
        }
		if(!empty($this->request->data['selected_year'])){
			$selected_year = $this->request->data['selected_year'];
		}
		if(!empty($this->request->data['selected_month'])){
			$selected_month = $this->request->data['selected_month'];
			$tmp_selected_month = $selected_month;
            $selected_month = $tmp_selected_month < 10 ? "0{$tmp_selected_month}" : $tmp_selected_month;
		}
		
		
        $days_in_month = date("t", mktime(0, 0, 0, $selected_month, 1, $selected_year));
		
		$begin_date = $selected_year."-".$selected_month."-01 00:00";
		$last_date  = $selected_year."-".$selected_month."-".$days_in_month." 23:59";
		$date_range = array("$begin_date", "$last_date");

		$ulist = explode(", ", $filter);
		
        /*$ulist = $this->Appointment->find('list', array(
            'conditions'=>array(
                'Appointment.privacy_type_id'=>2,
                'Appointment.deleted'=>'N',
                'Appointment.user_id <>'=>0,
				$filter_con,
                'OR'=>array(
                    'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                    'Appointment.end_date BETWEEN ? AND ?'=>$date_range
                )),
            'fields'=>array('Appointment.user_id')
        ));*/


        $user_list = array();
        foreach($ulist AS $id => $user_id){
            $user_list[] = $user_id;
        }
        
        
       
        $user = $this->getUserNameFromList($user_list);
         
		$list_user_id = '';
		if(!empty($user)){
			foreach($user['UserProfile'] as $userid=>  $getuid){
				$list_user[] = $getuid['id'];
			}
			$list_user_id = implode(',', $list_user);
			$filter_con = 'Appointment.user_id in ('.$list_user_id.')';
		}
       
			
		
        
        $this->set('alluser_id', $list_user_id);
                
        $this->set('user_list', $user['Organization']);
        //$this->set('sort_pos', $user['Position']);
        //// =========================

        $tmp_appointments = $this->Appointment->find('all', array(
            'conditions'=>array(
                'Appointment.privacy_type_id'=>2,
                'Appointment.deleted'=>'N',
                'Appointment.user_id <>'=>0,
			    $filter_con,
                'OR'=>array(
                    'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                    'Appointment.end_date BETWEEN ? AND ?'=>$date_range
                )
            ),
            'fields'=>array(
                'Appointment.id', 'Appointment.start_date', 'Appointment.end_date', 'Appointment.location', 
                'Appointment.location_type_id', 'Appointment.detail', 'Appointment.user_id', 'Appointment.name'
            ),
            'order'=>array('Appointment.user_id'=>'ASC', 'Appointment.start_date'=>'ASC')
        ));

        $appointments = array();
        foreach($tmp_appointments AS $i => $app){
            $app = $app['Appointment'];
            $id = $app['id'];
            $uid = $app['user_id'];

            if(empty($last_user_id)){ 
                $last_user_id = $app['user_id']; 
                
                $appointments[$uid]['id'] = $id;
                $appointments[$uid]['user_full_name'] = @$user['UserProfile'][$uid]['detail'];
                $appointments[$uid]['user_id'] = $uid;
                $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
            }else{              
                if($last_user_id == $app['user_id']){ /// same as last loop
                    $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                }else{ /// new user
                    $appointments[$uid]['id'] = $id;
                    $appointments[$uid]['user_id'] = $uid;
                    $appointments[$uid]['user_full_name'] = @$user['UserProfile'][$uid]['detail'];
                    $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                }
            }
        }
        
        $this->set('appointments', $appointments);
        $this->set('selected_month', $selected_month);
        $this->set('selected_year', $selected_year);
		$this->set('filter', $filter);
        
	}
	
	public function showschedule(){
		$this->layout = 'blank';

		$filter = '';
		$selected_year = date("Y");
		$selected_month = date("m");

        if(!empty($this->request->data['alluser'])){
            $filter = $this->request->data['alluser'];
        }
		if(!empty($this->request->data['selected_year'])){
			$selected_year = $this->request->data['selected_year'];
		}
		if(!empty($this->request->data['selected_month'])){
			$selected_month = $this->request->data['selected_month'];
			$tmp_selected_month = $selected_month;
            $selected_month = $tmp_selected_month < 10 ? "0{$tmp_selected_month}" : $tmp_selected_month;
		}
		
        $days_in_month = date("t", mktime(0, 0, 0, $selected_month, 1, $selected_year));
		
		$begin_date = $selected_year."-".$selected_month."-01 00:00";
		$last_date  = $selected_year."-".$selected_month."-".$days_in_month." 23:59";
		$date_range = array("$begin_date", "$last_date");

		$ulist = explode(",", $filter);
		
        /*$ulist = $this->Appointment->find('list', array(
            'conditions'=>array(
                'Appointment.privacy_type_id'=>2,
                'Appointment.deleted'=>'N',
                'Appointment.user_id <>'=>0,
				$filter_con,
                'OR'=>array(
                    'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                    'Appointment.end_date BETWEEN ? AND ?'=>$date_range
                )),
            'fields'=>array('Appointment.user_id')
        ));*/

		
        $user_list = array();
        foreach($ulist AS $id => $user_id){
            $user_list[] = $user_id;
        }
        
        
       
        $user = $this->getUserNameFromList($user_list);
        $filter_con = ''; 
		$list_user_id = '';
		if(!empty($user)){
			foreach($user['UserProfile'] as $userid=>  $getuid){
				$list_user[] = $getuid['id'];
			}
			$list_user_id = implode(',', $list_user);
			$filter_con = 'Appointment.user_id in ('.$list_user_id.')';
		}
       
			
		
        
        $this->set('alluser_id', $list_user_id);
                
        $this->set('user_list', $user['Organization']);
        //$this->set('sort_pos', $user['Position']);
        //// =========================

        $tmp_appointments = $this->Appointment->find('all', array(
            'conditions'=>array(
                'Appointment.privacy_type_id'=>2,
                'Appointment.deleted'=>'N',
                'Appointment.user_id <>'=>0,
			    $filter_con,
                'OR'=>array(
                    'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                    'Appointment.end_date BETWEEN ? AND ?'=>$date_range
                )
            ),
            'fields'=>array(
                'Appointment.id', 'Appointment.start_date', 'Appointment.end_date', 'Appointment.location', 
                'Appointment.location_type_id', 'Appointment.detail', 'Appointment.user_id', 'Appointment.name'
            ),
            'order'=>array('Appointment.user_id'=>'ASC', 'Appointment.start_date'=>'ASC')
        ));
		
        $appointments = array();
        foreach($tmp_appointments AS $i => $app){
            $app = $app['Appointment'];
            $id = $app['id'];
            $uid = $app['user_id'];

            if(empty($last_user_id)){ 
                $last_user_id = $app['user_id']; 
                
                $appointments[$uid]['id'] = $id;
                $appointments[$uid]['user_full_name'] = @$user['UserProfile'][$uid]['detail'];
                $appointments[$uid]['user_id'] = $uid;
                $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
            }else{              
                if($last_user_id == $app['user_id']){ /// same as last loop
                    $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                }else{ /// new user
                    $appointments[$uid]['id'] = $id;
                    $appointments[$uid]['user_id'] = $uid;
                    $appointments[$uid]['user_full_name'] = @$user['UserProfile'][$uid]['detail'];
                    $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                }
            }
        }
       
        $this->set('appointments', $appointments);
        $this->set('selected_month', $selected_month);
        $this->set('selected_year', $selected_year);
		$this->set('filter', $filter);		
	}
    
    
    /*function view(){
        if(!empty($this->request->data)){
            $selected_year = $this->request->data['selected_year'];
            $tmp_selected_month = $this->request->data['selected_month'];
            $selected_month = $tmp_selected_month < 10 ? "0{$tmp_selected_month}" : $tmp_selected_month;
            $user_id = $this->request->data['user_id'];
        }else{
            $selected_year = $this->request->query['selected_year'];
            $selected_month = $this->request->query['selected_month'];
            $user_id = $this->request->query['user_id'];
        }
        
        $this->layout = 'form';
        $breadCrumbs = array(
            array(
                'label' => __('View Shared Schedule'),
                'link' => array('controller' => 'ViewShares', 'action' => 'index', 'query' => '', 'param' => '')
            ),
            array(
                'label' => __('Detail'),
                'link' => false
            )
        );
        $this->set("breadCrumbs", $breadCrumbs);
        
            
        $days_in_month = date("t", mktime(0, 0, 0, $selected_month, 1, $selected_year));
		
		$begin_date = $selected_year."-".$selected_month."-01 00:00";
		$last_date  = $selected_year."-".$selected_month."-".$days_in_month." 23:59";
		$date_range = array("$begin_date", "$last_date");
        //$date_range = array("$selected_year-$selected_month-01", "$selected_year-$selected_month-$days_in_month");

        $user = $this->getUserNameFromList(array($user_id));

        //// =========================

        $tmp_appointments = $this->Appointment->find('all', array(
            'conditions'=>array(
                'Appointment.privacy_type_id'=>2,
                'Appointment.deleted'=>'N',
                'Appointment.user_id'=>$user_id,
                'OR'=>array(
                    'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                    'Appointment.end_date BETWEEN ? AND ?'=>$date_range
                )
            ),
            'fields'=>array(
                'Appointment.id', 'Appointment.start_date', 'Appointment.end_date', 'Appointment.location', 
                'Appointment.location_type_id', 'Appointment.detail', 'Appointment.user_id', 'Appointment.name'
            ),
            'order'=>array('Appointment.user_id'=>'ASC', 'Appointment.start_date'=>'ASC')
        ));
        
        //pr($tmp_appointments);
		
        $appointments = array();
        foreach($tmp_appointments AS $i => $app){
			
            $app = $app['Appointment'];
            $id = $app['id'];
            $uid = $app['user_id'];

            if(empty($last_user_id)){ 
                $last_user_id = $app['user_id']; 

                $appointments[$uid]['id'] = $id;
                $appointments[$uid]['user_full_name'] = @$user['UserProfile'][$uid]['detail'];
                $appointments[$uid]['user_id'] = $uid;
                $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
            }else{              
                if($last_user_id == $app['user_id']){ /// same as last loop
                    $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                }else{ /// new user
                    $appointments[$uid]['id'] = $id;
                    $appointments[$uid]['user_id'] = $uid;
                    $appointments[$uid]['user_full_name'] = @$user['UserProfile'][$uid]['detail'];
                    $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                    $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                    $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                    $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                }
            }
			
        }
        
        foreach($appointments AS $user_id => $appointment){
            $this->set('appointments', $appointment);
        }
        
        $this->set('selected_month', $selected_month);
        $this->set('selected_year', $selected_year);
        $this->set('user_id', $user_id);
        $this->set('user', $user);
    }*/
    
    public function sharedtimeline(){
       $this->layout ='blank'; 
	  
	   $filter = '';
       if(!empty($this->request->data)){
		$filter = $this->request->data['alluser'];
		$selected_date = $this->request->data['selecteddate'];
		$selected_year = $this->request->data['selectedyear'];
		$selected_month = $this->request->data['selectedmonth'];
       }
        $days_in_month = date("t", mktime(0, 0, 0, $selected_month, 1, $selected_year));
		
		$begin_date = $selected_year."-".$selected_month."-01 00:00";
		$last_date  = $selected_year."-".$selected_month."-".$days_in_month." 23:59";
		$date_range = array("$begin_date", "$last_date");
		$filter_con = '';
		if(!empty($filter)){
			$filter_con = 'Appointment.user_id in ('.$filter.')';
			$ulist = explode(", ", $filter);
		}else{
		
			$ulist = $this->Appointment->find('list', array(
				'conditions'=>array(
					'Appointment.privacy_type_id'=>2,
					'Appointment.deleted'=>'N',
					'Appointment.user_id <>'=>0,
					'OR'=>array(
						'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
						'Appointment.end_date BETWEEN ? AND ?'=>$date_range
					)),
				'fields'=>array('Appointment.user_id')
			));
		}
        $user_list = array();
        foreach($ulist AS $id => $user_id){
            $user_list[] = $user_id;
        }
        
        
        
        $user = $this->getUserNameFromList($user_list);
        
        foreach($user['UserProfile'] as $userid=>  $getuid){
            $list_user[] = $getuid['id'];
        }
        $list_user_id = implode(',', $list_user);
        $this->set('alluser_id', $list_user_id);
                
        $this->set('user_list', $user['Organization']);
        //$this->set('sort_pos', $user['Position']);
        //// =========================       
        $tmp_appointments = $this->Appointment->find('all', array(
             'conditions'=>array(
                 'Appointment.privacy_type_id'=>2,
                 'Appointment.deleted'=>'N',
                 'Appointment.user_id <>'=>0,
				 $filter_con,
                 'OR'=>array(
                     'Appointment.start_date BETWEEN ? AND ?'=>$date_range,
                     'Appointment.end_date BETWEEN ? AND ?'=>$date_range
                 )
             ),
             'fields'=>array(
                 'Appointment.id', 'Appointment.start_date', 'Appointment.end_date', 'Appointment.location', 
                 'Appointment.location_type_id', 'Appointment.detail', 'Appointment.user_id', 'Appointment.name'
             ),
             'order'=>array('Appointment.user_id'=>'ASC', 'Appointment.start_date'=>'ASC')
         ));

         $appointments = array();
         foreach($tmp_appointments AS $i => $app){
             $app = $app['Appointment'];
             $id = $app['id'];
             $uid = $app['user_id'];

       $appointments[$uid]['event_date'][$id]['user_id'] = $uid;
       $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                if(empty($last_user_id)){ 
                     $last_user_id = $app['user_id']; 

                     $appointments[$uid]['id'] = $id;
                     $appointments[$uid]['user_full_name'] = @$user['UserProfile'][$uid]['detail'];
                     $appointments[$uid]['user_id'] = $uid;
                     $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                     $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                     $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                    //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                     $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                }else{              
                    if($last_user_id == $app['user_id']){ /// same as last loop
                        $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                        $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                        $appointments[$uid]['event_date'][$id]['user_id'] = $uid;
                        $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                        $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                        $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                        //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                    }else{ /// new user
                        $appointments[$uid]['id'] = $id;
                        $appointments[$uid]['user_id'] = $uid;
                        $appointments[$uid]['user_full_name'] = @$user['UserProfile'][$uid]['detail'];;
                        $appointments[$uid]['event_date'][$id]['start_date'] = $app['start_date'];
                        $appointments[$uid]['event_date'][$id]['end_date'] = $app['end_date'];
                        $appointments[$uid]['event_date'][$id]['user_id'] = $uid;
                        $appointments[$uid]['event_date'][$id]['location_type_id'] = $app['location_type_id'];
                        $appointments[$uid]['event_date'][$id]['appointment_name'] = $app['name'];
                        //$appointments[$uid]['location_type_id'][$id] = $app['location_type_id'];
                        $appointments[$uid]['event_date'][$id]['detail'] = $app['detail'];
                    }
                }
         }
       $this->set('appointments', $appointments);
       $this->set('selected_month', $selected_month);
       $this->set('selected_year', $selected_year);
       $this->set('selected_date', $selected_date);
      
    }
}

?>
