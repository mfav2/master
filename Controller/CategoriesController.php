<?php

App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('Utility', 'mfa');
class CategoriesController extends AppController {

    public $name = 'Categories';
    public $uses = array('getCategory', 'Category', 'CategoryDescription', 'ContentCategory','Language');
    public $components = array("PortalHelper", "FileStorageComponent", "ProfileHelper");
    public $helpers = array('Portal', 'Form','Session');
	
	public function addsubgroup($MainCateId=null, $CurrentCateId=null){
		$currentUser = $this->Session->read('AuthUser');
		$this->layout='blank';
		$getcat = array();
		$get_cat_id = array();
		
		$this->set('main_category',$MainCateId);

		$getCate = $this->getCategory->find('all',array(
			 'conditions' => array(
				 'getCategory.deleted '=>'N',
				 'getCategory.parent_id '=>$MainCateId,
				 'getCategory.is_add_subcategory'=>'Y'	
		     ),
			 'order'=>array('getCategory.id'=>'ASC')
		));
		

		foreach ($getCate as $i => $cate) {
			$cat_lang = $this->ProfileHelper->CheckLanguage($cate['CategoryDescription'], $currentUser['AuthUserAudit']['UserProfile']['language']); 
			$getcat[$i]['category'] = $cat_lang;
			//pr($cat_lang);
		}

		$this->set('default', $CurrentCateId);
		$this->set('getcategory', $getcat);
		
		foreach($getcat as $getallcat){
			
			$get_cat_id['MainCategory'][$getallcat['category']['category_id']] = $getallcat['category']['category_id'];
			$get_cat_id['MainCategoryDescription'][$getallcat['category']['category_id']] = $getallcat['category'];
			
		}

		
		if(!empty($get_cat_id['MainCategory'])){
		  $getarr = implode(',', $get_cat_id['MainCategory']);
		  $conditions = 'getCategory.parent_id in ('.$getarr.')';
		  //pr($getarr);
		  $getParent = $this->getCategory->find('all',array(
			 'conditions' => array(
				 'getCategory.deleted '=>'N',
				 $conditions
		     ),
			 'order'=>array('getCategory.id'=>'ASC')
		  ));

		  foreach($getParent as $parent){
				
				$sub_cat_lang = $this->ProfileHelper->CheckLanguage($parent['CategoryDescription'], $currentUser['AuthUserAudit']['UserProfile']['language']); 
				
				$get_cat_id['SubCategory'][$parent['getCategory']['id']] = $parent['getCategory'];
				$get_cat_id['SubCategoryDescription'][$parent['getCategory']['id']] = $sub_cat_lang;
				$get_cat_id['SubCategoryDescription'][$parent['getCategory']['id']]['parent_id'] = $parent['getCategory']['parent_id'];

		  }
		  $this->set('getCateId',$get_cat_id);
		}
	}
	public function saveGroup(){
		$this->layout='blank';
		$this->autoRender = false;
		$chk = 0; 
		
		$countLanguage = $this->Language->find('count');
	   
		if(!empty($this->request->data)){
			$data = array();
			$data_desc = array();
			$data['Category']['category_name'] = $this->request->data['category_name'];
			$data['Category']['parent_id'] = $this->request->data['parent_id'];

			
			
			$this->Category->create();
			
			if($this->Category->save($data)){
				$category = $this->Category->getLastInsertID();
				for($i=1;$i<=$countLanguage;$i++){
					$this->CategoryDescription->create();
					$data_desc['CategoryDescription']['language_id'] = $i;
					$data_desc['CategoryDescription']['description'] = $this->request->data['category_name'];
					$data_desc['CategoryDescription']['category_id'] = $category;
					$this->CategoryDescription->save($data_desc);

					$cache_type = 'getCountContents';
					$cache_key = $cache_type.'-'.$this->request->data['parent_id'];

					$cache_type2 = 'getContentCategory';
					$cache_key2 = $cache_type2.'-'.$this->Session->read('AuthUser.AuthUser.id').'-'.$this->request->data['parent_id'];
					Cache::delete($cache_key);
					Cache::delete($cache_key2);

				}
				$chk= 1; 
			}
			
			
		}
		echo $chk;

	}

	public function showsubcategory($MainCateId=null, $CurrentCateId=null){
		$currentUser = $this->Session->read('AuthUser');
		$this->layout='blank';
		$getcat = array();
		$get_cat_id = array();
		
		$this->set('main_category',$MainCateId);

		$getCate = $this->getCategory->find('all',array(
			 'conditions' => array(
				 'getCategory.deleted '=>'N',
				 'getCategory.parent_id '=>$MainCateId,
				 
		     ),
			 'order'=>array('getCategory.id'=>'ASC')
		));
		

		foreach ($getCate as $i => $cate) {
			$cat_lang = $this->ProfileHelper->CheckLanguage($cate['CategoryDescription'], $currentUser['AuthUserAudit']['UserProfile']['language']); 
			$getcat[$i]['category'] = $cat_lang;
			//pr($cat_lang);
		}

		$this->set('default', $CurrentCateId);
		$this->set('getcategory', $getcat);
		
		foreach($getcat as $getallcat){
			
			$get_cat_id['MainCategory'][$getallcat['category']['category_id']] = $getallcat['category']['category_id'];
			$get_cat_id['MainCategoryDescription'][$getallcat['category']['category_id']] = $getallcat['category'];
			
		}

		
		if(!empty($get_cat_id['MainCategory'])){
		  $getarr = implode(',', $get_cat_id['MainCategory']);
		  $conditions = 'getCategory.parent_id in ('.$getarr.')';
		  //pr($getarr);
		  $getParent = $this->getCategory->find('all',array(
			 'conditions' => array(
				 'getCategory.deleted '=>'N',
				 $conditions
		     ),
			 'order'=>array('getCategory.id'=>'ASC')
		  ));

		  foreach($getParent as $parent){
				
				$sub_cat_lang = $this->ProfileHelper->CheckLanguage($parent['CategoryDescription'], $currentUser['AuthUserAudit']['UserProfile']['language']); 
				
				$get_cat_id['SubCategory'][$parent['getCategory']['id']] = $parent['getCategory'];
				$get_cat_id['SubCategoryDescription'][$parent['getCategory']['id']] = $sub_cat_lang;
				$get_cat_id['SubCategoryDescription'][$parent['getCategory']['id']]['parent_id'] = $parent['getCategory']['parent_id'];

		  }
		  $this->set('getCateId',$get_cat_id);
		}
	}
	public function categorydeleted(){
		$this->layout = 'blank';
		$this->autoRender = false;
		
		$parent_cate = $this->getCategory->find('first', array(
			'conditions' => array(
				'getCategory.id' => $this->request->data['id'],
				'getCategory.deleted' => 'N'
			)
		));
		
		$main_category = $parent_cate['getCategory']['parent_id'];
		
		$cate_desc = $this->CategoryDescription->find('all',array(
			'conditions'=>array(
				'CategoryDescription.category_id'=>$this->request->data['id'],
				'CategoryDescription.deleted'=>'N'
			),
			'recursive'=>-1,
		));	
		
		$data =array();
		$this->getCategory->id = $this->request->data['id'];
		$data['getCategory']['deleted'] = 'Y';
		
		


		if($this->getCategory->save($data)){
			$array_desc = array();
			foreach($cate_desc as $catedesc){
				//pr($catedesc);
				$this->CategoryDescription->id = $catedesc['CategoryDescription']['id'];
				$array_desc['CategoryDescription']['deleted'] = 'Y';
				$this->CategoryDescription->save($array_desc);

				$cache_type = 'getCountContents';
				$cache_key = $cache_type.'-'.$main_category;

				$cache_type2 = 'getContentCategory';
				$cache_key2 = $cache_type2.'-'.$this->Session->read('AuthUser.AuthUser.id').'-'.$main_category;
				Cache::delete($cache_key);
				Cache::delete($cache_key2);

			}
			echo 1;
		}else{
			echo 0;
		}
	}


	public function index(){
		
	}
	
	public function showallcategory($parent_id = NULL){

		$this->disableCache();
        $this->autoRender = false;
        //$condition[] = array('Category.parent_id' => $parent_id);
        $parent = '';
        if(!empty($parent_id)){
        	$parent = 'Category.parent_id = '.$parent_id;
        }
        //$condition['and'] = array('Organization.is_normal' => "Y");
        $condition['and'] = array('Category.deleted' => "N");
        $children = $this->Category->find('all', array(
        	'conditions' => array(
        		'Category.deleted' => 'N',
        		$parent
        	), 
        	'order' => array('Category.sort_order' => 'asc')
        ));
        $i = 0;
        //pr($children);
         if ($children != NULL) {
            foreach ($children as $child) {
                foreach ($child['CategoryDescription'] as $des) {
                    if ($des['language_id'] == 2) {
                        $data_array[$i]['data']['title'] = $des['description'];
                        $data_array[$i]['data']['attr']['id'] = $child['Category']['id']; //id ของ <a>
                        $data_array[$i]['data']['attr']['level'] = $child['Category']['level'];
                        break;
                    }
                } $data_array[$i]['state'] = 'closed';
                $data_array[$i]['attr']['id'] = $child['Category']['id']; //id ของ <li>
                $data_array[$i]['attr']['parent_id'] = $child['Category']['parent_id'];
                $data_array[$i++]['attr']['level'] = $child['Category']['level'];
            }
        } else {
            $data_array = array();
        }
        print_r(json_encode($data_array));
	}

}