<?php
App::uses('AppModel', 'Model');

class MsgGroup extends AppModel{
    public $useTable = "msg_groups";
    
    public $hasMany =array(
            "MsgGroupMember"=>array(
                "className"=>"MsgGroupMember",
                "foreignKey"=>"msg_group_id"
            )
        );
}
?>