<?php

App::uses('AppModel', 'Model');

/**
 * Layout Model
 *
 * @property Layout $Layout
 */
class ContentTag extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    public $useTable = 'mb_content_tags';

    //public $tablePrefix = 'pg_mb_';
    public function beforeSave($options = array()) {

        /* [++++++++++ ADD BY BAIPHAI 2013-03-06 ++++++++++] */
        // STRIP HTML TAG AS DISCUSSION
//        $this->data[$this->name]['tag_name'] = strip_tags($this->data[$this->name]['tag_name']);
        /* [---------- END ADD BY BAIPHAI 2013-03-06 -----------] */

        $currentUser = $this->getCurrentUser();
        $user_id = $currentUser['AuthUser']['id'];

        if (empty($this->data[$this->name]['id'])) {
            $this->data[$this->name]['created_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['created_user_id'] = $user_id;
            $this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['updated_user_id'] = $user_id;
            //$this->data[$this->name]['assigned_user_id'] = $user_id;	
        } else {
            $this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['updated_user_id'] = $user_id;
        }
        return true;
    }

}

?>
