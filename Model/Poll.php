<?php
App::uses('AppModel', 'Model');
/**
 * Layout Model
 *
 * @property Layout $Layout
 */
class Poll extends AppModel {
/**
 * Display field
 *
 * @var string
 */

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	public $useTable = "mb_questions";
	//public $tablePrefix = "pg_mb_";
        
        public $hasMany = array(
                "PollAnswer" => array(
                        "className"=>"PollAnswer",
                        "foreignKey"=>"question_id" 
                ),
                "PollResult" => array(
                        "className"=>"PollResult",
                        "foreignKey"=>"question_id" 
                ),
                "PortalAttachment" => array(
                        "className"=>"PortalAttachment",
                        "foreignKey"=>"reference_id" 
                )
            
        ); 
        public function beforeSave($options = array()) {

		$currentUser = $this->getCurrentUser();
		$user_id = $currentUser['AuthUser']['id'];

		if (empty($this->data[$this->name]['id'])) {
			$this->data[$this->name]['created_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['created_user_id'] = $user_id;
			$this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['updated_user_id'] = $user_id;
			//$this->data[$this->name]['assigned_user_id'] = $user_id;	
		} else {
			$this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['updated_user_id'] = $user_id;
		}
		return true;
	}        
	
}
?>
