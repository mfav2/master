<?php

App::uses('AppModel', 'Model');
App::import('Component', 'Session');

class Question extends AppModel {
    var $name = 'Question';
    var $useTable = 'mb_questions';
    
    var $hasMany = array(
        'QuestionAnswer'=>array(
            'className'=>'QuestionAnswer', 
            'foreignKey'=>'question_id',
            'fields'=>array(
              'QuestionAnswer.id',  'QuestionAnswer.question_id', 'QuestionAnswer.answer_name'
            ),
            'conditions' => array(
                 'QuestionAnswer.deleted' => 'N'  
            ),  
        ),
        'QuestionResult'=>array(
            'className'=>'QuestionResult', 
            'foreignKey'=>'question_id',
            'fields'=>array(
              'QuestionResult.id',  'QuestionResult.answer_id', 'QuestionResult.question_id', 'QuestionResult.open_text',
              'QuestionResult.vote_date', 'QuestionResult.vote_date', 'QuestionResult.first_name', 'QuestionResult.last_name', 
              'QuestionResult.organization_name', 'QuestionResult.position_name',
            ),
            'conditions' => array(
                 'QuestionResult.deleted' => 'N'  
            ),  
        ),
    );
    
    
    public function beforeSave($options = array()) {

		$currentUser = $this->getCurrentUser();
		$user_id = $currentUser['AuthUser']['id'];

		if (empty($this->data[$this->name]['id'])) {
			$this->data[$this->name]['created_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['created_user_id'] = $user_id;
			$this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['updated_user_id'] = $user_id;
			//$this->data[$this->name]['assigned_user_id'] = $user_id;	
		} else {
			$this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['updated_user_id'] = $user_id;
		}
		return true;
	}
    
}

?>