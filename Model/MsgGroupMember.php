<?php
App::uses('AppModel', 'Model');

class MsgGroupMember extends AppModel{
    public $useTable = "msg_group_members";
    
    public $belongsTo =array(
            "MsgGroup"=>array(
                "className"=>"MsgGroup"
            )
        );
}
?>