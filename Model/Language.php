<?php
App::uses('AppModel', 'Model');
/**
 * Layout Model
 *
 * @property Layout $Layout
 */
class Language extends AppModel {
/**
 * Display field
 *
 * @var string
 */

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	public $useTable = "sys_languages";
	//public $tablePrefix = "pg_sys_";
	/*public $hasMany = array(
		'CategoryDescription' => array(
		   'className' =>'CategoryDescription',
		   'foreignKey'=>'language_id'
		),
		'MenuDescription' =>array(
		  'className'=>'MenuDescription',
		  'foreignKey'=>'language_id'
		)	
        );*/
        public function beforeSave($options = array()) {

		$currentUser = $this->getCurrentUser();
		$user_id = $currentUser['AuthUser']['id'];

		if (empty($this->data[$this->name]['id'])) {
			$this->data[$this->name]['created_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['created_user_id'] = $user_id;
			$this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['updated_user_id'] = $user_id;
			//$this->data[$this->name]['assigned_user_id'] = $user_id;	
		} else {
			$this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['updated_user_id'] = $user_id;
		}
		return true;
	}
}
?>