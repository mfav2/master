<?php
App::uses('AppModel', 'Model');

class MsgLog extends AppModel{
    public $useTable = "msg_logs";
    
    public $belongsTo =array(
            "MsgRegister"=>array(
                "className"=>"MsgRegister",
                'foreignKey'=>'msg_register_id'
            )
        );
}
?>