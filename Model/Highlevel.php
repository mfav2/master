<?php

App::uses('AppModel', 'Model');

/**
 * PgAbHighlevel Model
 *
 * @property User $User
 */
class Highlevel extends AppModel {

    public $useTable = "ab_highlevels";
    //public $tablePrefix = "pg_ab_";

    public $belongsTo = array(
        /* 'AuthUser'=>array(
          'className'=>'AuthUser',
          'foreignKey'=>'user_id'
          ), */
        'HighlevelGroup' => array(
            'className' => 'HighlevelGroup',
            'foreignKey' => 'highlevel_group_id'
        )
    );

    public function beforeSave($options = array()) {

		$currentUser = $this->getCurrentUser();
		$user_id = $currentUser['AuthUser']['id'];

        if (empty($this->data[$this->name]['id'])) {
            $this->data[$this->name]['created_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['created_user_id'] = $user_id;
            $this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['updated_user_id'] = $user_id;
            //$this->data[$this->name]['assigned_user_id'] = $user_id;	
        } else {
            $this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['updated_user_id'] = $user_id;
        }
        return true;
    }

}
