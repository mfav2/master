<?php
App::uses('AppModel', 'Model');

class MsgRegister extends AppModel{
    public $useTable = "msg_registers";
    
    public $hasMany =array(
            'MsgMember'=>array(
                'className'=>'MsgMember'
            ),
            'MsgGroup'=>array(
                'className'=>'MsgGroup'
            ),
            'MsgLog'=>array(
                'className'=>'MsgLog'
            )
        );
}
?>