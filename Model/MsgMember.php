<?php
App::uses('AppModel', 'Model');

class MsgMember extends AppModel{
    public $useTable = "msg_members";
    
    public $belongsTo =array(
            "MsgRegister"=>array(
                "className"=>"MsgRegister",
                'foreignKey'=>'msg_register_id'
            )
        );
}
?>