<?php

App::uses('AppModel', 'Model');
App::import('Component', 'Session');

class getQuestion extends AppModel {
    var $name = 'getQuestion';
    var $useTable = 'mb_questions';
    
    var $hasMany = array(
        'QuestionAnswer'=>array(
            'className'=>'QuestionAnswer', 
            'foreignKey'=>'question_id',
            'fields'=>array(
              'QuestionAnswer.id',  'QuestionAnswer.question_id', 'QuestionAnswer.answer_name'
            ),
            'conditions' => array(
                 'QuestionAnswer.deleted' => 'N'  
            ), 
        ),
       
    );
    
    
    public function beforeSave($options = array()) {

		$currentUser = $this->getCurrentUser();
		$user_id = $currentUser['AuthUser']['id'];

		if (empty($this->data[$this->name]['id'])) {
			$this->data[$this->name]['created_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['created_user_id'] = $user_id;
			$this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['updated_user_id'] = $user_id;
			//$this->data[$this->name]['assigned_user_id'] = $user_id;	
		} else {
			$this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['updated_user_id'] = $user_id;
		}
		return true;
	}
    
}

?>