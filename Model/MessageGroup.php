<?php
App::uses('AppModel', 'Model');

class MessageGroup extends AppModel{
    public $useTable = "mb_message_groups";
    
    public $hasMany =array(
            "MessageGroupUser"=>array(
                "className"=>"MessageGroupUser",
                "foreignKey"=>"group_id"
            )
    );
    public function beforeSave($options = array()) {

            $currentUser = $this->getCurrentUser();
            $user_id = $currentUser['AuthUser']['id'];

            if (empty($this->data[$this->name]['id'])) {
                    $this->data[$this->name]['created_date'] = date('Y-m-d H:i:s');
                    $this->data[$this->name]['created_user_id'] = $user_id;
                    $this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
                    $this->data[$this->name]['updated_user_id'] = $user_id;
                    //$this->data[$this->name]['assigned_user_id'] = $user_id;	
            } else {
                    $this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
                    $this->data[$this->name]['updated_user_id'] = $user_id;
            }
            return true;
    }        
}
?>