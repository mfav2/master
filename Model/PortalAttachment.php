<?php
App::uses('AppModel', 'Model');
/**
 * Layout Model
 *
 * @property Layout $Layout
 */
class PortalAttachment extends AppModel {
/**
 * Display field
 *
 * @var string
 */

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	public $useTable = "attachments";
	public $tablePrefix = "pg_";
        
        public $belongsTo = array(
                'ContentType' => array(
                      'className'=>'ContentType',
                      'foreignKey'=>'content_type_id'
                ),
                'ResourceContent'=>array(
                      'className'=>'ResourceContent',
                      'foreignKey'=>'resource_content_id'
                ),
        );
        
        
        
        public function beforeSave($options = array()) {

		$currentUser = $this->getCurrentUser();
		$user_id = $currentUser['AuthUser']['id'];

		if (empty($this->data[$this->name]['id'])) {
			$this->data[$this->name]['created_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['created_user_id'] = $user_id;
			$this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['updated_user_id'] = $user_id;
			//$this->data[$this->name]['assigned_user_id'] = $user_id;	
		} else {
			$this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['updated_user_id'] = $user_id;
		}
		return true;
	}	
}
?>
