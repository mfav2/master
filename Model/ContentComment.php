<?php

App::uses('AppModel', 'Model');

/**
 * Layout Model
 *
 * @property Layout $Layout
 */
class ContentComment extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    public $useTable = 'mb_content_comments';
    public $hasMany = array(
        'ContentLike' => array(
            'className' => 'ContentLike',
            'foreignKey' => 'reference_id',
            'conditions' => array(
                'ContentLike.deleted = \'N\'',
                'ContentLike.content_type_id = 3' // 3 is Content type comment
//             'ContentLike.user_id = ' . $this->Session->read('AuthUser.AuthUser.id'),
////             ถ้าดึงมาแค่เจ้าของได้จะดีมาก เพราะใช้แค่ของเจ้าของ
            )
        )
    );
    
    /*public $hasOne = array(
        'PortalAttachment'=>array(
            'className'=>'PortalAttachment',
            'foreignKey'=>'reference_id',
            'conditions'=>array(
                'PortalAttachment.deleted'=>'N',
                'PortalAttachment.content_type_id'=>3
            )
        )
    );*/

    //public $tablePrefix = 'pg_mb_';
    public function beforeSave($options = array()) {

        $currentUser = $this->getCurrentUser();
        $user_id = $currentUser['AuthUser']['id'];
        // ADD BY BAIPHAI
        // $user_id = 1; ** Comment By PKWARLOCK
        // END ADD BY BAIPHAI

        if (empty($this->data[$this->name]['id'])) {
            $this->data[$this->name]['created_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['created_user_id'] = $user_id;
            $this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['updated_user_id'] = $user_id;
            //$this->data[$this->name]['assigned_user_id'] = $user_id;	
        } else {
            $this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['updated_user_id'] = $user_id;
        }
        return true;
    }

}

?>
