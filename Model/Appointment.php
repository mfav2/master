<?php

App::uses('AppModel', 'Model');

/**
 * Layout Model
 *
 * @property Layout $Layout
 */
class Appointment extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    public $useTable = "ab_appointments";
    //public $tablePrefix = "pg_ab_";


    /*public $belongsTo = array(
        "AppointmentColor" => array(
            "className" => "AppointmentColor",
            "foreignKey" => "color_id",
            "conditions" => array(
                'AppointmentColor.deleted' => 'N',
            ),
            "fields" => array(
                'AppointmentColor.id', 'AppointmentColor.color_code', 'AppointmentColor.color_name'
            )
        ),
        "AppointmentLocation" => array(
            "className" => "AppointmentLocation",
            "foreignKey" => "location_type_id",
            "conditions" => array(
                'AppointmentLocation.deleted' => 'N',
            ),
            "fields" => array(
                'AppointmentLocation.id', 'AppointmentLocation.location_name'
            )
        ),
        "AppointmentPriority" => array(
            "className" => "AppointmentPriority",
            "foreignKey" => "priority_type_id",
            "conditions" => array(
                'AppointmentPriority.deleted' => 'N',
            ),  
          "fields" => array(
                'AppointmentPriority.id', 'AppointmentPriority.priority_name'
            )
        ),
        "AppointmentStatus" => array(
            "className" => "AppointmentStatus",
            "foreignKey" => "status_type_id"
        ),
        "AppointmentPrivacy" => array(
            "className" => "AppointmentPrivacy",
            "foreignKey" => "privacy_type_id",
            "conditions" => array(
                'AppointmentPrivacy.deleted' => 'N',
            ),
            "fields" => array(
                'AppointmentPrivacy.id', 'AppointmentPrivacy.privacy_name'
            )
        ),
        "AppointmentHandle" => array(
            "className" => "AppointmentHandle",
            "foreignKey" => "handle_user_id"
        ),
    );*/
    /*public $hasMany = array(
        "AppointmentUserReader" => array(
            "className" => "AppointmentUserReader",
            "foreignKey" => "appointment_id",
            "conditions" => array(
                'AppointmentUserReader.deleted' => 'N',
            ),
        ),
       
    );*/

    public function beforeSave($options = array()) {

        $currentUser = $this->getCurrentUser();
        $user_id = $currentUser['AuthUser']['id'];

        if (empty($this->data[$this->name]['id'])) {
            $this->data[$this->name]['created_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['created_user_id'] = $user_id;
            $this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['updated_user_id'] = $user_id;
            //$this->data[$this->name]['assigned_user_id'] = $user_id;	
        } else {
            $this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['updated_user_id'] = $user_id;
        }
        return true;
    }

}

?>
