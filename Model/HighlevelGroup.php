<?php
App::uses('AppModel', 'Model');
/**
 * PgAbHighlevel Model
 *
 * @property User $User
 */
class HighlevelGroup extends AppModel {

	public $useTable = "ab_highlevel_groups";
	//public $tablePrefix = "pg_ab_";
        /*public $hasMany = array(
            'Highlevel' => array(
                'className' => 'Highlevel',
                'foreignKey' => 'highlevel_group_id'
            )
        );*/
        
        
        public function beforeSave($options = array()) {

		$currentUser = $this->getCurrentUser();
		$user_id = $currentUser['AuthUser']['id'];

		if (empty($this->data[$this->name]['id'])) {
			$this->data[$this->name]['created_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['created_user_id'] = $user_id;
			$this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['updated_user_id'] = $user_id;
			//$this->data[$this->name]['assigned_user_id'] = $user_id;	
		} else {
			$this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
			$this->data[$this->name]['updated_user_id'] = $user_id;
		}
		return true;
	}	
	
}
