<?php

App::uses('AppModel', 'Model');

/**
 * Layout Model
 *
 * @property Layout $Layout
 */
class Category extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    public $useTable = 'mb_categories';
    //public $tablePrefix = 'pg_mb_';
    public $actsAs = array('Tree');

    public $hasMany = array(
//        'CategoryDescription' => array(
//            'className' => 'CategoryDescription',
//            'foreignKey' => 'category_id'
//        ),
        /*'ContentCategory' => array(
            'className' => 'ContentCategory',
            'foreignKey' => 'category_id',
//            'conditions' => array('ContentCategory.sub_category_id = Category.id')
        ),*/
        /*'Subscrible' => array(
            'className' => 'Subscrible',
            'foreignKey' => 'category_id',
        ),*/
        'CategoryDescription' => array(
            'className' => 'CategoryDescription',
            'foreignKey' => 'category_id',
            'conditions' => array(
                'CategoryDescription.deleted' =>'N'
            ),
            'order' => array('CategoryDescription.id ASC')
        ),
    );

    public function beforeSave($options = array()) {

        $currentUser = $this->getCurrentUser();
        $user_id = $currentUser['AuthUser']['id'];

        if (empty($this->data[$this->name]['id'])) {
            $this->data[$this->name]['created_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['created_user_id'] = $user_id;
            $this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['updated_user_id'] = $user_id;
            //$this->data[$this->name]['assigned_user_id'] = $user_id;	
        } else {
            $this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['updated_user_id'] = $user_id;
        }
        return true;
    }

}

?>
