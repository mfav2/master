<?php
App::uses('AppModel', 'Model');
App::import('Component', 'Session');

/**
 * Layout Model
 *
 * @property Layout $Layout
 */
class Content extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
//The Associations below have been created with all possible keys, those that are not needed can be removed
    public $useTable = 'mb_contents';
//public $tablePrefix = 'pg_mb_';
    public $hasMany = array(
        /*'ContentLike' => array(
            'className' => 'ContentLike',
            'foreignKey' => 'reference_id',
            'conditions' => array(
//                'ContentLike.deleted != \'Y\'',
                'ContentLike.content_type_id = 1', // 1 is Content type content
//             'ContentLike.user_id = ' . $this->Session->read('AuthUser.AuthUser.id'),
////             ถ้าดึงมาแค่เจ้าของได้จะดีมาก เพราะใช้แค่ของเจ้าของ
            ),
        ),*/
	/*	'PortalAttachment' => array(
            'className' => 'PortalAttachment',
            'foreignKey' => 'reference_id',
            'conditions' => array(
                'PortalAttachment.deleted != \'Y\'',
            ),
        ),*/
        /*'ContentView' => array(
            'className' => 'ContentView',
            'foreignKey' => 'content_id',
//            'conditions' => array(
//                'ContentView.deleted != \'Y\'',
//            ),
            'fields' => array('DISTINCT created_user_id'),
        ),
//        'PortalAttachment' => array(
//            'className' => 'PortalAttachment',
//            'foreignKey' => 'reference_id',
//        ),
        /*'ContentTag' => array(
            'className' => 'ContentTag',
            'foreignKey' => 'content_id',
            'conditions' => array(
                'ContentTag.deleted != \'Y\'',
            ),
            'order' => 'ContentTag.tag_name ASC',
        ),*/
        /*'ContentComment' => array(
            'className' => 'ContentComment',
            'foreignKey' => 'content_id',
//            'order' => 'id ASC',
        ),*/
//        'ContentFavorite' => array(
//            'className' => 'ContentFavorite',
//            'foreignKey' => 'content_id'
//        ),
//        'Poll' => array(
//            'className' => 'Poll',
//            'foreignKey' => 'content_id'
//        ),
        /*'ContentCategory' => array(
            'className' => 'ContentCategory',
            'foreignKey' => 'content_id',
        ),*/
        /*'ContentUserReader' => array(
            'className' => 'ContentUserReader',
            'foreignKey' => 'content_id',
            'fields' => array('DISTINCT user_id'),
        ),
        'ContentOrganization' => array(
            'className' => 'ContentOrganization',
            'foreignKey' => 'content_id',
            'fields' => array('DISTINCT organization_id'),
        ),*/
    );

//    public $hasAndBelongsToMany = array(
//        'ContentType' => array(
//            'className' => 'ContentType',
//            'joinTable' => 'mb_content_likes',
//            'foreignKey' => 'reference_id',
//            'associationForeignKey' => 'content_type_id'
//        )
//    );


    public function beforeSave($options = array()) {

        /* [++++++++++ ADD BY BAIPHAI 2013-03-06 ++++++++++] */
        // STRIP HTML TAG AS DISCUSSION
//        $this->data[$this->name]['title'] = strip_tags($this->data[$this->name]['title']);
//        $this->data[$this->name]['description'] = strip_tags($this->data[$this->name]['description']);
        /* [---------- END ADD BY BAIPHAI 2013-03-06 -----------] */

        $currentUser = $this->getCurrentUser();
        $user_id = $currentUser['AuthUser']['id'];

        if (empty($this->data[$this->name]['id'])) {
            $this->data[$this->name]['created_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['created_user_id'] = $user_id;
            $this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['updated_user_id'] = $user_id;
//$this->data[$this->name]['assigned_user_id'] = $user_id;	
        } else {
            $this->data[$this->name]['updated_date'] = date('Y-m-d H:i:s');
            $this->data[$this->name]['updated_user_id'] = $user_id;
        }
        return true;
    }

//    Be sure that beforeDelete() returns true, or your delete is going to fail.
    public function beforeDelete($cascade = true) {
        return true;
    }

}